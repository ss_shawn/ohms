package com.lanware.ehs.common.exception;

import org.apache.shiro.authc.AuthenticationException;

/**
 * 系统维护异常
 */
public class SysMaintainceException extends AuthenticationException {
    public SysMaintainceException() {
    }

    public SysMaintainceException(String message) {
        super(message);
    }

    public SysMaintainceException(String message, Throwable cause) {
        super(message, cause);
    }

    public SysMaintainceException(Throwable cause) {
        super(cause);
    }
}
