package com.lanware.ehs.common.utils;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.UUID;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
public class OSSTools {

    @Value("${alioss.endpoint}")
    private String endpoint;
    @Value("${alioss.accessKeyId}")
    private String accessKeyId;
    @Value("${alioss.accessKeySecret}")
    private String accessKeySecret;
    @Value("${alioss.bucketName}")
    private String bucketName;
    @Value("${alioss.parentPath}")
    private String parentPath;

    /**
     * 上传文件到OSS服务器
     * @param path oss上文件存储路径，根目录zyjkxt下级
     * @param file 需要上传的文件
     */
    public void uploadOSS(String path,File file) {
        // 创建OSSClient实例。
        OSSClient ossClient=null;
        try {
            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            //上传
            PutObjectResult result = ossClient.putObject(bucketName,parentPath+path, file);

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            ossClient.shutdown();
        }
    }

    /**
     * 上传文件流到OSS服务器
     * @param path oss上文件存储路径，根目录zyjkxt下级
     * @param inputStream 文件字节输入流
     */
    public void uploadStream(String path,InputStream inputStream ) {
        // 创建OSSClient实例。
        OSSClient ossClient=null;
        try {
            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            // 上传文件流。
            ossClient.putObject(bucketName,parentPath+path, inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            ossClient.shutdown();
        }
    }

    /**
     * 上传附件
     * @param filePath
     * @param file
     * @return
     */
    public String upload(String filePath,MultipartFile file) throws IOException {
        String fileName =null,realNames = "",returnURL;
        fileName = file.getOriginalFilename();
        //上传文件
        StringBuffer realName = new StringBuffer(UUID.randomUUID().toString());
        String fileExtension = fileName.substring(fileName.lastIndexOf("."));
        realName.append(fileExtension);


        InputStream is = file.getInputStream();

        Long fileSize = file.getSize();

        //创建上传Object的Metadata
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(is.available());
        metadata.setCacheControl("no-cache");
        metadata.setHeader("Pragma", "no-cache");
        metadata.setContentEncoding("utf-8");
        metadata.setContentType(getContentType(fileName));
        StringBuffer description = new StringBuffer("filename/filesize=");
        description.append(realNames).append("/").append(fileSize).append("Byte.");
        metadata.setContentDisposition(description.toString());
        returnURL = filePath+"/"+realName;
        String key = parentPath+returnURL;
        getOSSClient().putObject(bucketName,key,is, metadata);
        getOSSClient().shutdown();
        is.close();
        return returnURL;
    }


    /**
     * 获取OSS客户端实例
     * @return
     */
    private OSSClient getOSSClient(){
        return new OSSClient(endpoint, accessKeyId, accessKeySecret);
    }


    /**
     * @description 多文件批量打包成zip包下载
     * @param fileName 要下载的zip文件名
     * @param pathList 文件路径数组
     * @param response
     */
    public void downloadZip(String fileName, String[] pathList, HttpServletResponse response) throws IOException {
        OSSClient ossClient = null;
        BufferedOutputStream bos = null;
        BufferedInputStream bis = null;
        try {
            // 初始化
            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            File zipFile = File.createTempFile(fileName, ".zip");
            fileName = fileName + ".zip";
            FileOutputStream fos = new FileOutputStream(zipFile);
            CheckedOutputStream cos = new CheckedOutputStream(fos, new Adler32());
            ZipOutputStream zos = new ZipOutputStream(cos);
            for (String path : pathList) {
                OSSObject ossObject = ossClient.getObject(bucketName, parentPath + path);
                InputStream is = ossObject.getObjectContent();
                zos.putNextEntry(new ZipEntry(path.substring(path.lastIndexOf("/"))));
                int bytesRead = 0;
                while ((bytesRead = is.read()) != -1) {
                    zos.write(bytesRead);
                }
                is.close();
                zos.closeEntry();;
            }
            zos.close();
            fileName = URLEncoder.encode(fileName, "UTF-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition", "attachment;fileName="+fileName);
            FileInputStream fis = new FileInputStream(zipFile);
            bis = new BufferedInputStream(fis);
            bos = new BufferedOutputStream(response.getOutputStream());
            int n = -1;
            byte[] buffer = new byte[4096];
            while ((n = bis.read(buffer, 0, 4096)) > -1) {
                bos.write(buffer, 0, n);
            }
            bos.flush();
            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
            // 关闭流，不可少
            if (bis != null)
                bis.close();
            if (bos != null)
                bos.close();
        }
    }


    /**
     * 用流的方式从OSS服务器下载文件
     * @param fileName 下载文件名
     * @param path oss上文件存储路径，根目录zyjkxt下级
     * @param response
     */
    public void downloadFile(String fileName, String path, HttpServletResponse response) throws IOException {
        OSSClient ossClient = null;
        BufferedOutputStream output = null;
        BufferedInputStream input = null;
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8");
            fileName = fileName.replaceAll("\\+","%20");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition", "attachment;fileName="+fileName);
            byte[] buffer = new byte[4096];// 缓冲区

            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
            OSSObject ossObject = ossClient.getObject(bucketName,parentPath+path);
            output = new BufferedOutputStream(response.getOutputStream());
            input = new BufferedInputStream(ossObject.getObjectContent());
            int n = -1;
            // 遍历，开始下载
            while ((n = input.read(buffer, 0, 4096)) > -1) {
                output.write(buffer, 0, n);
            }
            output.flush();
            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
            // 关闭流，不可少
            if (input != null)
                input.close();
            if (output != null)
                output.close();
        }
    }

    /**
     * 从OSS服务器下载文件
     * @param path oss上文件存储路径，根目录zyjkxt下级
     * @param localPath 下载到本地的路径包含文件名
     */
    public void downloadToLocal(String path,String localPath) {
//        String endpoint = "oss-cn-hangzhou.aliyuncs.com";
//        String accessKeyId = "LTAI3QupNo30N1Pq";
//        String accessKeySecret ="dT5IW1beMbpX8kOZFzUB5hzelAJy2C";
//        String bucketName = "ohms-archive1";
//        String parentPath = "zyjkxt";
        // 创建OSSClient实例。
        OSSClient ossClient=null;
        try {
            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            //下载
            ossClient.getObject(new GetObjectRequest(bucketName, parentPath+path), new File(localPath));

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            ossClient.shutdown();
        }
    }

    /**
     * 删除OSS服务器上的文件
     * @param path oss上文件存储路径
     */
    public void deleteOSS(String path) {
        // 创建OSSClient实例。
        OSSClient ossClient=null;
        try {
            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            ossClient.deleteObject(bucketName, parentPath+path);

        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            ossClient.shutdown();
        }

    }




    /**
     * 通过文件名判断并获取OSS服务文件上传时文件的contentType
     *
     * @param fileName 文件名
     * @return 文件的contentType
     */
    public static final String getContentType(String fileName) {
        String FilenameExtension = fileName.substring(fileName.lastIndexOf("."));
        if (FilenameExtension.equalsIgnoreCase(".bmp")) {
            return "application/x-bmp";
        }
        if (FilenameExtension.equalsIgnoreCase(".gif")) {
            return "image/gif";
        }
        if (FilenameExtension.equalsIgnoreCase(".jpeg") ||
                FilenameExtension.equalsIgnoreCase(".jpg") ||
                FilenameExtension.equalsIgnoreCase(".png")) {
            return "image/jpeg";
        }
        if (FilenameExtension.equalsIgnoreCase(".html")) {
            return "text/html";
        }
        if (FilenameExtension.equalsIgnoreCase(".txt")) {
            return "text/plain";
        }
        if (FilenameExtension.equalsIgnoreCase(".vsd")) {
            return "application/vnd.visio";
        }
        if (FilenameExtension.equalsIgnoreCase(".pptx") ||
                FilenameExtension.equalsIgnoreCase(".ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (FilenameExtension.equalsIgnoreCase(".docx") ||
                FilenameExtension.equalsIgnoreCase(".doc")) {
            return "application/msword";
        }
        if (FilenameExtension.equalsIgnoreCase(".xla") ||
                FilenameExtension.equalsIgnoreCase(".xlc")||
                FilenameExtension.equalsIgnoreCase(".xlm")||
                FilenameExtension.equalsIgnoreCase(".xls")||
                FilenameExtension.equalsIgnoreCase(".xlt")||
                FilenameExtension.equalsIgnoreCase(".xlw")) {
            return "application/vnd.ms-excel";
        }
        if (FilenameExtension.equalsIgnoreCase(".xml")) {
            return "text/xml";
        }
        if (FilenameExtension.equalsIgnoreCase(".pdf")) {
            return "application/pdf";
        }
        if (FilenameExtension.equalsIgnoreCase(".zip")) {
            return "application/zip";
        }
        if (FilenameExtension.equalsIgnoreCase(".tar")) {
            return "application/x-tar";
        }
        if (FilenameExtension.equalsIgnoreCase(".avi")) {
            return "video/avi";
        }
        if (FilenameExtension.equalsIgnoreCase(".mp4")) {
            return "video/mpeg4";
        }
        if (FilenameExtension.equalsIgnoreCase(".mp3")) {
            return "audio/mp3";
        }
        if (FilenameExtension.equalsIgnoreCase(".mp2")) {
            return "audio/mp2";
        }
        return "application/octet-stream";
    }

}
