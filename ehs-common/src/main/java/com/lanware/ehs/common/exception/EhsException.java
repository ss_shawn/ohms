package com.lanware.ehs.common.exception;

public class EhsException extends RuntimeException {
    public EhsException() {
    }

    public EhsException(String message) {
        super(message);
    }

    public EhsException(String message, Throwable cause) {
        super(message, cause);
    }

    public EhsException(Throwable cause) {
        super(cause);
    }

    public EhsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
