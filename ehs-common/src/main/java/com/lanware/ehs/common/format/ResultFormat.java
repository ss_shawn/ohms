package com.lanware.ehs.common.format;

public class ResultFormat {

    public final static String STATUS_SUCCESS = "success";

    public final static String STATUS_ERROR = "error";

    private String status;

    private String message;

    private Object data;

    public ResultFormat(String status, String message, Object data){
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public static ResultFormat success(){
        return success(null);
    }

    public static ResultFormat success(String message){
        return success(message, null);
    }

    public static ResultFormat success(Object data){
        return success(null, data);
    }

    public static ResultFormat success(String message, Object data){
        return bind(STATUS_SUCCESS, message, data);
    }

    public static ResultFormat error(){
        return error(null);
    }

    public static ResultFormat error(String message){
        return error(message, null);
    }

    public static ResultFormat error(Object data){
        return error(null, data);
    }

    public static ResultFormat error(String message, Object data){
        return bind(STATUS_ERROR, message, data);
    }

    public static ResultFormat bind(String status, String message, Object data){
        return new ResultFormat(status, message, data);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
