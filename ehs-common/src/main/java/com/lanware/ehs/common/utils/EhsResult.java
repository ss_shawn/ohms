package com.lanware.ehs.common.utils;

import java.util.List;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 自定义响应结构
 *
 * @author zhuxiao
 * @date 2018年9月17日
 */
public class EhsResult {
	
	//定义jackson对象
	private static final ObjectMapper MAPPER = new ObjectMapper();
	
	//响应业务状态
	private Integer status;

	//兼容layuiTable
	private Integer code;
	
	//响应消息
	private String message;
	
	//响应中的数据
	private Object data;
	
	public EhsResult(){
		
	}
	
	public EhsResult(Object data){
		this.status = 200;
		this.message = "OK";
		this.data = data;
		this.code = 200;
	}
	
	public EhsResult(Integer status,String message,Object data){
		this.status = status;
		this.code = status;
		this.message = message;
		this.data = data;
	}


	public static  EhsResult build(Integer status,String message){
		return new EhsResult(status,message,null);
	}

	public static  EhsResult build(Integer status,String message,Object data){
		return new EhsResult(status,message,data);
	}
	
	public static EhsResult ok(){
		return new EhsResult(null);
	}
	
	public static EhsResult ok(Object data){
		return new EhsResult(data);
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getmessage() {
		return message;
	}

	public void setmessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * 将json结果集转化为EhsResult对象
	 * @param jsonData
	 * @param clazz
	 * @return
	 */
	public static EhsResult formatToPojo(String jsonData,Class<?> clazz){
		try {
			if(clazz == null){
				return MAPPER.readValue(jsonData, EhsResult.class);
			}
			JsonNode jsonNode = MAPPER.readTree(jsonData);
			JsonNode data = jsonNode.get("data");
			Object obj = null;
			if(clazz != null){
				if(data.isObject()){
					obj = MAPPER.readValue(data.traverse(), clazz);
				}else if(data.isTextual()){
					obj = MAPPER.readValue(data.asText(), clazz);
				}
			}
			return build(jsonNode.get("status").intValue(), jsonNode.get("message").asText(), obj);
		} catch (Exception e) {
			return null;
		} 
	}
	
	/**
	 * 
	 * @param json
	 * @return
	 */
	public static EhsResult format(String json){
		try {
			return MAPPER.readValue(json, EhsResult.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static EhsResult formatToList(String jsonData, Class<?> clazz){
		try {
			JsonNode jsonNode = MAPPER.readTree(jsonData);
			JsonNode data = jsonNode.get("data");
			Object obj = null;
			if(data.isArray() && data.size() > 0){
				obj = MAPPER.readValue(data.traverse(), 
						MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
			}
			return build(jsonNode.get("status").intValue(), jsonNode.get("message").asText(), obj); 
		} catch (Exception e) {
			return null;
		}
	}
	
}
