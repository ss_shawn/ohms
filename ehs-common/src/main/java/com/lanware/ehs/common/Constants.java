package com.lanware.ehs.common;

/**
 * 系统全局常量
 */
public interface Constants {
    /**
     * 当前用户
     */
    String CURRENT_USER = "user";
    String CURRRENT_USERNAME = "username";

    /**
     * 验证码前缀
     */
    String CODE_PREFIX = "ehs_captcha_";

    // 前端页面路径前缀
    String VIEW_PREFIX = "ehs/views/";

    // 排序规则：降序
    String ORDER_DESC = "desc";
    // 排序规则：升序
    String ORDER_ASC = "asc";

    //各级责任制文件上传数量
    Integer DUTY_FILE_COUNT = 5;

    //防范制度文件上传数量
    Integer PREVENTIVE_SYSTEM_FILE_COUNT = 10;

    //年度计划文件上传数量
    Integer YEAR_PLAN_FILE_COUNT = 7;
}
