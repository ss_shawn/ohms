package com.lanware.ehs.common.utils;

public class StringUtil {

    public static String humpToUnderscore(String value){
        if (value == null){
            return null;
        }
        StringBuilder resultBuilder = new StringBuilder("");
        for (int i = 0; i < value.length(); i ++){
            char c = value.charAt(i);
            if (Character.isUpperCase(c)){
                resultBuilder.append("_");
                c = Character.toLowerCase(c);
            }
            resultBuilder.append(c);
        }
        return resultBuilder.toString();
    }

    public static String underscoreToHump(String value){
        if (value == null){
            return null;
        }
        StringBuilder resultBuilder = new StringBuilder("");
        boolean isConvert = false;
        for (int i = 0; i < value.length(); i ++){
            char c = value.charAt(i);
            if (c == '_' && i != value.length() - 1){
                isConvert = true;
                continue;
            }
            if (isConvert){
                c = Character.toUpperCase(c);
                isConvert = false;
            }
            resultBuilder.append(c);
        }
        return resultBuilder.toString();
    }
}
