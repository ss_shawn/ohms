package com.lanware.ehs.common.function;

import com.lanware.ehs.common.exception.RedisConnectException;

/**
 *
 * @param <T>
 * @param <R>
 */
@FunctionalInterface
public interface JedisExecutor<T, R> {
    R excute(T t) throws RedisConnectException;
}