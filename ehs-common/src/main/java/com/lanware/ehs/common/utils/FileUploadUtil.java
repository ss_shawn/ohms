package com.lanware.ehs.common.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
/**
 * @description 文件上传到服务器工具类
 */
public class FileUploadUtil {
    @Value("${upload.tempPath}")
    /** 上传到服务器的临时文件路径 */
    private String tempPath;

    /**
     * @description 文件上传（服务器临时文件）
     * @param file 请求的MultipartFile文件
     * @return java.io.File 生成的临时File文件
     */
    public File fileupload(@RequestParam("file") MultipartFile file) {
        // 生成的临时文件路径
        File tempFile = new File(tempPath + File.separator + file.getOriginalFilename());
        try {
            if(!tempFile.getParentFile().exists()){
                tempFile.getParentFile().mkdirs();
            }
            // 文件复制
            file.transferTo(tempFile);
        } catch(IOException e) {
            e.printStackTrace();
        }
        return tempFile;
    }
}
