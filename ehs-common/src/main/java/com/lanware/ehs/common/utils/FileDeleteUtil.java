package com.lanware.ehs.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class FileDeleteUtil {

    private OSSTools ossTools;

    @Autowired
    public FileDeleteUtil(OSSTools ossTools) {
        this.ossTools = ossTools;
    }

    public void deleteFilesAsync(Set<String> filePathSet){
        Thread thread = new Thread(() -> {
            deleteFiles(filePathSet);
        });
        thread.start();
    }

    public void deleteFiles(Set<String> filePathSet){
        for (String filePath : filePathSet){
            ossTools.deleteOSS(filePath);
        }
    }
}
