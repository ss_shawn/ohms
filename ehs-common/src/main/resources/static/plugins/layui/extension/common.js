layui.define(["jquery", "layer","xadmin"], function (exports) {
    let $ = layui.$;
    let layer = layui.layer;
    var xadmin = layui.xadmin;

    $(document).on("change", ":file", function (event) {
        if (this.files.length > 0 && this.files[0].size > 1024 * 1024 * 500) {
            layer.msg("文件上传限制500MB", {icon: 5});
            $(this).val("");
            event.stopImmediatePropagation();
        }
    });

    let common = {
        layerOpen: function (url, parameters, options) {
            let loadingIndex = layer.load(2);
            $.post(url, parameters, function (r) {
                var result;
                try {
                   var code=r.code;
                    result=r;
                } catch (e) {
                    result = {'code': 'err'};
                }
                if (result.code === 401) {
                    xadmin.view.notify('登录失效', '登录已失效，请重新登录', function () {
                        window.location.reload();
                        window.location.hash = '';
                    });
                    return;
                }
                if (result.code === 403) {
                    xadmin.view.tab.change('/403');

                    return;
                }
                if (result.code === 404) {
                    xadmin.view.tab.change('/404');

                    return;
                }
                if (result.code === 500) {
                    xadmin.view.tab.change('/500');

                    return;
                }


                layer.close(loadingIndex);
                layer.open($.extend({
                    type: 1,
                    content: r,
                    maxmin: true
                }, options));
            });
        },
        verifyNumber: function(selector, exclude){
            var reg = "[^\\d";
            if (exclude) {
                for (var index in exclude) {
                    reg += exclude[index];
                }
            }
            reg +="]";
            $(selector).on("input" , function (event) {
                $(this).val($(this).val().replace(new RegExp(reg, "g"),""));
            });
        }
    };
    exports("common", common);
});