class InputVerification{

    constructor(){

    }

    static verifyNumber(selector, exclude){
        var reg = "[^\\d";
        if (exclude) {
            for (var index in exclude) {
                reg += exclude[index];
            }
        }
        reg +="]";
        $(selector).on("input" , function (event) {
            $(this).val($(this).val().replace(new RegExp(reg, "g"),""));
        });
    }
}

function verifyNumber(selector, exclude){
    var reg = "[^\\d";
    if (exclude) {
        for (var index in exclude) {
            reg += exclude[index];
        }
    }
    reg +="]";
    $(selector).on("input" , function (event) {
        $(this).val($(this).val().replace(new RegExp(reg, "g"),""));
    });
}