class Format{

    constructor(){
        console.log("aaaa")
    }

    test(){
        console.log("没问题");
    }

    static formatDate(value) {
        if (typeof value === "string") {
            value = new Date(value);
        }
        if (!value) {
            return "";
        }
        var year = value.getFullYear();
        var month = value.getMonth() + 1;
        var date = value.getDate();
        return year + "-" + (month > 9 ? month : "0" + month) + "-" + (date > 9 ? date : "0" + date);
    }

    static formatDateTime(value) {
        if (typeof value === "string") {
            value = new Date(value);
        }
        if (!value) {
            return "";
        }
        var year = value.getFullYear();
        var month = value.getMonth() + 1;
        var date = value.getDate();
        var hours = value.getHours();
        var minutes = value.getMinutes();
        var seconds = value.getSeconds();
        return year + "-" + (month > 9 ? month : "0" + month) + "-" + (date > 9 ? date : "0" + date) + " " + (hours > 9 ? hours : "0" + hours) + ":" + (minutes > 9 ? minutes : "0" + minutes) + ":"  + (seconds > 9 ? seconds : "0" + seconds);
    }
}
console.log(new Format());
