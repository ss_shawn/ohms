//设置默认异常处理
$.ajaxSetup({
    "error": function (xhr, status, error) {
        if (xhr.status == 600) {
            //未登录或超时
            window.top.location.href = "/login";
        }else if (xhr.status == 700) {
            //没有权限
            layer.msg("抱歉，你没有相应的权限", {icon: 5});
        }else {
            layer.msg("网络异常", {icon: 5});
        }
    }
});

$(document).on("change", ":file", function (event) {
    if (this.files.length > 0 && this.files[0].size > 1024 * 1024 * 500) {
        layer.msg("文件上传限制500MB", {icon: 5});
        $(this).val("");
        event.stopImmediatePropagation();
    }
});