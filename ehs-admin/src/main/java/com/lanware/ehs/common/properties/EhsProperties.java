package com.lanware.ehs.common.properties;

import lombok.Data;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

/**
 * 加载配置文件
 */
@Data
@SpringBootConfiguration
@PropertySource(value = {"classpath:ehs.properties"})
@ConfigurationProperties(prefix = "ehs")
public class EhsProperties {
    private ShiroProperties shiro = new ShiroProperties();
    private boolean openAopLog = true;
}
