package com.lanware.ehs.admin.account.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.account.service.AccountService;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 账号管理视图
 */
@Controller("accountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入accountService的bean */
    @Autowired
    private AccountService accountService;

    /**
     * @description 新增超级管理账号
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("account/editSysUser")
    public ModelAndView addSysUser() {
        JSONObject data = new JSONObject();
        SysUser sysUser = new SysUser();
        data.put("sysUser", sysUser);
        return new ModelAndView("account/editSysUser", data);
    }

    /**
     * @description 编辑超级管理账号
     * @param id 超级管理账号id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("account/editSysUser/{id}")
    public ModelAndView editSysUser(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        SysUser sysUser = accountService.getSysUserById(id);
        data.put("sysUser", sysUser);
        return new ModelAndView("account/editSysUser", data);
    }

    /**
     * @description 新增监管账号
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("account/editMonitorUser")
    public ModelAndView addMonitorUser() {
        JSONObject data = new JSONObject();
        MonitorUser monitorUser = new MonitorUser();
        data.put("monitorUser", monitorUser);
        return new ModelAndView("account/editMonitorUser", data);
    }

    /**
     * @description 编辑监管账号
     * @param id 监管账号id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("account/editMonitorUser/{id}")
    public ModelAndView editMonitorUser(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        MonitorUser monitorUser = accountService.getMonitorUserById(id);
        data.put("monitorUser", monitorUser);
        return new ModelAndView("account/editMonitorUser", data);
    }

    /**
     * @description 新增企业账号
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("account/editEnterpriseUser")
    public ModelAndView addEnterpriseUser() {
        JSONObject data = new JSONObject();
        EnterpriseUser enterpriseUser = new EnterpriseUser();
        data.put("enterpriseUser", enterpriseUser);
        return new ModelAndView("account/editEnterpriseUser", data);
    }

    /**
     * @description 编辑企业账号
     * @param id 企业账号id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("account/editEnterpriseUser/{id}")
    public ModelAndView editEnterpriseUser(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseUser enterpriseUser = accountService.getEnterpriseUserById(id);
        data.put("enterpriseUser", enterpriseUser);
        return new ModelAndView("account/editEnterpriseUser", data);
    }
}
