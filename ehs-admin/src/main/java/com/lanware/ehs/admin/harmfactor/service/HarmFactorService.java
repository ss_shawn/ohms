package com.lanware.ehs.admin.harmfactor.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.BaseHarmFactor;

import java.util.List;
import java.util.Map;

/**
 * @description 危害因素管理interface
 * @author WangYumeng
 * @date 2019-07-03 13:43
 */
public interface HarmFactorService {
    /**
     * @description 查询危害因素list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含baseHarmFactors和totalNum的result
     */
    JSONObject listHarmFactors(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:23
     * @param id 危害因素id
     * @return com.lanware.management.pojo.BaseHarmFactor 返回BaseHarmFactor对象
     */
    BaseHarmFactor getHarmFactorById(Long id);

    /**
     * @description 根据name查询危害因素
     * @param name 危害因素名称
     * @return com.lanware.management.pojo.BaseHarmFactor 返回BaseHarmFactor对象
     */
    BaseHarmFactor getHarmFactorByName(String name);

    /**
     * @description 根据condition查询危害因素
     * @param condition 查询条件
     * @return com.lanware.management.pojo.BaseHarmFactor 返回BaseHarmFactor对象
     */
    BaseHarmFactor getHarmFactorByCondition(Map<String, Object> condition);

    /**
     * @description 新增危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:25
     * @param baseHarmFactor 危害因素pojo
     * @return int 插入成功的数目
     */
    int insertHarmFactor(BaseHarmFactor baseHarmFactor);

    /**
     * @description 批量插入危害因素信息
     * @param baseHarmFactorList 危害因素list
     * @return int 插入成功的数目
     */
    int insertHarmFactors(List<BaseHarmFactor> baseHarmFactorList);

    /**
     * @description 更新危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:26
     * @param baseHarmFactor 危害因素pojo
     * @return int 更新成功的数目
     */
    int updateHarmFactor(BaseHarmFactor baseHarmFactor);

    /**
     * @description 删除危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:35
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteHarmFactor(Long id);

    /**
     * @description 批量删除危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:37
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteHarmFactors(Long[] ids);

    /**
     *
     * 导入危害因素基础数据
     * @param list
     */
    void importExcelData(List<BaseHarmFactor> list);
}
