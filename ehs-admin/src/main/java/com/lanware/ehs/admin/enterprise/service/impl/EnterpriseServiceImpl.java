package com.lanware.ehs.admin.enterprise.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.admin.ModuleName;
import com.lanware.ehs.admin.enterprise.service.EnterpriseService;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.mapper.EnterpriseBaseinfoMapper;
import com.lanware.ehs.mapper.custom.EnterpriseBaseinfoCustomMapper;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseBaseinfoExample;
import com.lanware.ehs.pojo.custom.EnterpriseBaseinfoCustom;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EnterpriseServiceImpl implements EnterpriseService {

    private EnterpriseBaseinfoMapper enterpriseBaseinfoMapper;

    private EnterpriseBaseinfoCustomMapper enterpriseBaseinfoCustomMapper;

    private OSSTools ossTools;

    @Value("${enterprise.api.address}")
    private String enterpriseAPIAddress;

    @Autowired
    public EnterpriseServiceImpl(EnterpriseBaseinfoMapper enterpriseBaseinfoMapper, EnterpriseBaseinfoCustomMapper enterpriseBaseinfoCustomMapper, OSSTools ossTools){
        this.enterpriseBaseinfoMapper = enterpriseBaseinfoMapper;
        this.enterpriseBaseinfoCustomMapper = enterpriseBaseinfoCustomMapper;
        this.ossTools = ossTools;
    }

    @Override
    public Page<EnterpriseBaseinfoCustom> queryEnterpriseBaseinfoByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<EnterpriseBaseinfoCustom> page = PageHelper.startPage(pageNum, pageSize);
        enterpriseBaseinfoCustomMapper.queryEnterpriseBaseinfoByCondition(condition);
        return page;
    }


    @Override
    public List<EnterpriseBaseinfoCustom> queryEnterpriseBaseinfoByCondition(Map<String, Object> condition) {
        return enterpriseBaseinfoCustomMapper.queryEnterpriseBaseinfoByCondition(condition);
    }

    @Override
    @Transactional
    public int saveEnterpriseBaseinfo(EnterpriseBaseinfo enterpriseBaseinfo) {
        //新增数据先保存，以便获取id
        if (enterpriseBaseinfo.getId() == null){
            enterpriseBaseinfo.setGmtModified(new Date());
            enterpriseBaseinfo.setGmtCreate(new Date());
            enterpriseBaseinfo.setStatus(0);
            return  enterpriseBaseinfoMapper.insert(enterpriseBaseinfo);
        }else{
            //更新企业基础信息
            EnterpriseBaseinfo targetBaseinfoSource = enterpriseBaseinfoMapper.selectByPrimaryKey(enterpriseBaseinfo.getId());
            BeanUtils.copyProperties(enterpriseBaseinfo,targetBaseinfoSource,"lastUseTime","lastUpdateTime","status","gmtCreate");
            targetBaseinfoSource.setGmtModified(new Date());
            return enterpriseBaseinfoMapper.updateByPrimaryKey(targetBaseinfoSource);
        }
    }

    @Override
    public EnterpriseBaseinfo queryEnterpriseBaseinfoById(Long id) {
        return enterpriseBaseinfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public int enableEnterpriseBaseinfo(List<Long> ids, Boolean enable) {
        EnterpriseBaseinfo enterpriseBaseinfo = new EnterpriseBaseinfo();
        enterpriseBaseinfo.setStatus(enable ? 0 : 1);
        enterpriseBaseinfo.setLastUpdateTime(new Date());
        enterpriseBaseinfo.setGmtModified(enterpriseBaseinfo.getLastUpdateTime());
        EnterpriseBaseinfoExample enterpriseBaseinfoExample = new EnterpriseBaseinfoExample();
        EnterpriseBaseinfoExample.Criteria criteria = enterpriseBaseinfoExample.createCriteria();
        criteria.andIdIn(ids);
        return enterpriseBaseinfoMapper.updateByExampleSelective(enterpriseBaseinfo, enterpriseBaseinfoExample);
    }

    @Override
    public EhsResult transferData(List<Long> ids) {
        RestTemplate restTemplate = new RestTemplate();
        EhsResult result = EhsResult.ok();
        for(Long id : ids){
            String url = enterpriseAPIAddress+"/uploadGovernment/"+id;
            ResponseEntity<EhsResult> responseEntity = restTemplate.getForEntity(url,EhsResult.class);
            result = responseEntity.getBody();
            if(result.getStatus() == 500){
                break;
            }
        }
        return result;
    }
}
