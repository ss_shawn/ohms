package com.lanware.ehs.admin.consult.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.admin.consult.service.ConsultService;
import com.lanware.ehs.mapper.SysConsultMapper;
import com.lanware.ehs.mapper.custom.SysConsultCustomMapper;
import com.lanware.ehs.pojo.SysConsult;
import com.lanware.ehs.pojo.SysConsultExample;
import com.lanware.ehs.pojo.custom.SysConsultCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 系统咨询interface实现类
 */
@Service
public class ConsultServiceImpl implements ConsultService {
    @Autowired
    /** 注入sysConsultMapper的bean */
    private SysConsultMapper sysConsultMapper;
    @Autowired
    /** 注入sysConsultCustomMapper的bean */
    private SysConsultCustomMapper sysConsultCustomMapper;

    @Override
    /**
     * @description 查询系统咨询list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含sysRecommendInstitutions和totalNum的result
     */
    public JSONObject listConsults(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<SysConsultCustom> sysConsults = sysConsultCustomMapper.listConsults(condition);
        result.put("sysConsults", sysConsults);
        // 分页数据装进result
        if (pageNum != null && pageSize != null) {
            PageInfo<SysConsultCustom> pageInfo = new PageInfo<SysConsultCustom>(sysConsults);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 更新系统咨询
     * @param sysConsult 系统咨询pojo
     * @return int 更新成功的数目
     */
    public int updateSysConsult(SysConsult sysConsult) {
        return sysConsultMapper.updateByPrimaryKeySelective(sysConsult);
    }

    @Override
    /**
     * @description 删除系统咨询
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    public int deleteSysConsult(Long id) {
        return sysConsultMapper.deleteByPrimaryKey(id);
    }

    @Override
    /**
     * @description 批量删除系统咨询
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    public int deleteSysConsults(Long[] ids) {
        SysConsultExample sysConsultExample = new SysConsultExample();
        SysConsultExample.Criteria criteria = sysConsultExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return sysConsultMapper.deleteByExample(sysConsultExample);
    }
}
