package com.lanware.ehs.admin.account.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.SysUser;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.MonitorUserCustom;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Map;

/**
 * @description 账号管理interface
 * @author WangYumeng
 * @date 2019-07-01 13:55
 */
public interface AccountService {
    /**
     * @description 查询超级管理账号list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含sysUsers和totalNum的result
     */
    JSONObject listSysUsers(Integer pageNum, Integer pageSize, Map<String, Object> condition);
    /**
     * @description 查询监管账号list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含monitorUsers和totalNum的result
     */
    JSONObject listMonitorUsers(Integer pageNum, Integer pageSize, Map<String, Object> condition);
    /**
     * @description 查询企业账号list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseUsers和totalNum的result
     */
    JSONObject listEnterpriseUsers(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 14:45
     * @param id 超级管理账号id
     * @return com.high.stand.management.pojo.SysUser 返回SysUser对象
     */
    SysUser getSysUserById(Long id);
    /**
     * @description 根据id查询监管账号信息
     * @author WangYumeng
     * @date 2019/7/1 14:45
     * @param id 监管账号id
     * @return com.high.stand.management.pojo.MonitorUser 返回MonitorUser对象
     */
    MonitorUser getMonitorUserById(Long id);
    /**
     * @description 根据id查询企业账号信息
     * @author WangYumeng
     * @date 2019/7/1 14:45
     * @param id 企业账号id
     * @return com.high.stand.management.pojo.EnterpriseUser 返回EnterpriseUser对象
     */
    EnterpriseUser getEnterpriseUserById(Long id);

    /**
     * @description 根据id查询监管账号扩展信息
     * @author WangYumeng
     * @date 2019/7/3 16:24
     * @param id 监管账号id
     * @return com.lanware.management.pojo.custom.MonitorUserCustom
     */
    MonitorUserCustom getMonitorUserCustomById(Long id);
    /**
     * @description 根据id查询企业账号扩展信息
     * @author WangYumeng
     * @date 2019/7/3 16:24
     * @param id 企业账号id
     * @return com.lanware.management.pojo.custom.EnterpriseUserCustom
     */
    EnterpriseUserCustom getEnterpriseUserCustomById(Long id);

    /**
     * @description 根据username查询超级管理账号信息
     * @param username 用户名
     * @return com.high.stand.management.pojo.SysUser 返回SysUser对象
     */
    SysUser getSysUserByUsername(String username);
    /**
     * @description 根据username查询监管账号信息
     * @param username 用户名
     * @return com.high.stand.management.pojo.MonitorUser 返回MonitorUser对象
     */
    MonitorUser getMonitorUserByUsername(String username);
    /**
     * @description 根据username查询企业账号信息
     * @param username 用户名
     * @return com.high.stand.management.pojo.EnterpriseUser 返回EnterpriseUser对象
     */
    EnterpriseUser getEnterpriseUserByUsername(String username);

    /**
     * @description 新增超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:21
     * @param sysUser 超级管理账号pojo
     * @return int 插入成功的数目
     */
    void insertSysUser(SysUser sysUser) throws MessagingException;
    /**
     * @description 新增监管账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:21
     * @param monitorUser 监管账号pojo
     * @return int 插入成功的数目
     */
    void insertMonitorUser(MonitorUser monitorUser) throws MessagingException;
    /**
     * @description 新增企业账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:23
     * @param enterpriseUser 企业账号pojo
     * @return int 插入成功的数目
     */
    void insertEnterpriseUser(EnterpriseUser enterpriseUser) throws MessagingException;

    /**
     * @description 更新超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:24
     * @param sysUser 超级管理账号pojo
     * @return int 更新成功的数目
     */
    int updateSysUser(SysUser sysUser);
    /**
     * @description 更新监管账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:24
     * @param monitorUser 监管账号pojo
     * @return int 更新成功的数目
     */
    int updateMonitorUser(MonitorUser monitorUser);
    /**
     * @description 更新企业账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:25
     * @param enterpriseUser 企业账号pojo
     * @return int 更新成功的数目
     */
    int updateEnterpriseUser(EnterpriseUser enterpriseUser);

    /**
     * @description 删除超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 17:07
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteSysUser(Long id);

    /**
     * @description 批量删除超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 17:17
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteSysUsers(Long[] ids);

    /**
     * @description 批量禁用超级管理账号
     * @author WangYumeng
     * @date 2019/7/2 17:17
     * @param ids 要禁用的数组ids
     * @return int 禁用成功的数目
     */
    int disableSysUsers(Long[] ids);

    /**
     * @description 批量禁用监管账号
     * @author WangYumeng
     * @date 2019/7/2 17:37
     * @param ids 要禁用的数组ids
     * @return int 禁用成功的数目
     */
    int disableMonitorUsers(Long[] ids);

    /**
     * @description 批量禁用企业账号
     * @author WangYumeng
     * @date 2019/7/2 17:57
     * @param ids 要禁用的数组ids
     * @return int 禁用成功的数目
     */
    int disableEnterpriseUsers(Long[] ids);

    /**
     * @description 批量启用超级管理账号
     * @author WangYumeng
     * @date 2019/7/2 18:07
     * @param ids 要启用的数组ids
     * @return int 启用成功的数目
     */
    int enableSysUsers(Long[] ids);

    /**
     * @description 批量启用监管账号
     * @author WangYumeng
     * @date 2019/7/2 18:17
     * @param ids 要启用的数组ids
     * @return int 启用成功的数目
     */
    int enableMonitorUsers(Long[] ids);

    /**
     * @description 批量启用企业账号
     * @author WangYumeng
     * @date 2019/7/2 18:27
     * @param ids 要启用的数组ids
     * @return int 启用成功的数目
     */
    int enableEnterpriseUsers(Long[] ids);

    /**
     * @description 批量重置超级管理账号密码
     * @author WangYumeng
     * @date 2019/7/3 08:37
     * @param sysUsers 要重置密码的超级管理账号list
     * @return int 重置密码成功的数目
     */
    int resetSysUsers(List<SysUser> sysUsers);

    /**
     * @description 批量重置监管账号密码
     * @author WangYumeng
     * @date 2019/7/3 08:47
     * @param monitorUsers 要重置密码的监管账号list
     * @return int 重置密码成功的数目
     */
    int resetMonitorUsers(List<MonitorUser> monitorUsers);

    /**
     * @description 批量重置企业账号密码
     * @author WangYumeng
     * @date 2019/7/3 08:57
     * @param enterpriseUsers 要重置密码的企业账号list
     * @return int 重置密码成功的数目
     */
    int resetEnterpriseUsers(List<EnterpriseUser> enterpriseUsers);

    /**
     * @description 发送html邮件
     * @author WangYumeng
     * @date 2019/7/2 14:04
     * @param toMail 收件人地址
     * @param subject 主题
     * @param content 内容，包含html
     * @return void
     */
    void sendHtmlMail(String toMail, String subject, String content) throws MessagingException;
}
