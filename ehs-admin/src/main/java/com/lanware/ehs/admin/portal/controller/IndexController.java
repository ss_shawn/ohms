package com.lanware.ehs.admin.portal.controller;

import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.OSSTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class IndexController {

    private OSSTools ossTools;

    @Autowired
    public IndexController(OSSTools ossTools){
        this.ossTools = ossTools;
    }

    /**
     * 管理端门户页面
     * @return 管理端门户model
     */
    @RequestMapping("/")
    public ModelAndView index(){
        return new ModelAndView("portal/index");
    }

    /**
     * 文件下载
     * @param filePath 文件路径
     * @param response response对象
     * @return 失败信息
     */
    @RequestMapping("/downloadFile")
    @ResponseBody
    public ResultFormat downloadFile(@RequestParam("filePath") String filePath, HttpServletResponse response) {
        try {
            ossTools.downloadFile(filePath.substring(filePath.lastIndexOf("/") + 1), filePath, response);
        } catch (IOException e) {
            e.printStackTrace();
            return ResultFormat.error("下载文件失败");
        }
        return null;
    }
}
