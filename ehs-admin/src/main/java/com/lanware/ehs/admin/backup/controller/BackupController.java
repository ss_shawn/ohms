package com.lanware.ehs.admin.backup.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.admin.backup.service.BackupService;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.pojo.EnterpriseDocumentBackup;
import com.lanware.ehs.pojo.custom.EnterpriseDocumentBackupCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/backup")
@Slf4j
public class BackupController extends BaseController {

    @Autowired
    private OSSTools ossTools;

    @Autowired
    BackupService backupService;
    @RequestMapping("")
    public String list(){
        return "backup/list";
    }

    /**
     * 文件备份分页
     */
    @RequestMapping("/getList")
    @ResponseBody
    public EhsResult getList(Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<EnterpriseDocumentBackupCustom> page = backupService.queryEnterpriseDocumentBackupByCondition(pageNum, pageSize, condition);
        Map<String, Object> dataTable = getDataTable(page);
        return EhsResult.ok(dataTable);
    }

    /**
     * 下载文件
     */
    @RequestMapping(value = "/downloadFile")
    public String downloadFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fileName = request.getParameter("name");
        String path =  request.getParameter("path");
        ossTools.downloadFile(fileName,path,response);
        return null;
    }

    /**
     * 删除备份数据
     * @param id
     * @return
     */
    @RequestMapping("delete/{id}")
    @ResponseBody
    public EhsResult delete(@PathVariable  Long id) throws EhsException {
        try {
            backupService.delete(id);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "删除备份数据失败";
            log.error(message,e);
            throw new EhsException(message);
        }
    }


}
