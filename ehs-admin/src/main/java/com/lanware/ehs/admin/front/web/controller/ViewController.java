package com.lanware.ehs.admin.front.web.controller;

import com.lanware.ehs.common.Constants;
import com.lanware.ehs.pojo.custom.SysUserCustom;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * 视图显示Controller
 */
@Controller("systemView")
public class ViewController {

    @GetMapping(Constants.VIEW_PREFIX + "404")
    public String error404() {
        return "error/404";
    }

    @GetMapping(Constants.VIEW_PREFIX + "403")
    public String error403() {
        return "error/403";
    }

    @GetMapping(Constants.VIEW_PREFIX + "500")
    public String error500() {
        return "error/500";
    }

    @RequestMapping(Constants.VIEW_PREFIX + "index")
    public String pageIndex() {
        return "index/index";
    }

    /**
     * 系统布局页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "layout")
    public String layout() {
        return "index/layout";
    }

    /**
     * 账号管理页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "account")
    public String account() {
        return "account/list";
    }

    /**
     * 企业基础信息列表页面
     * @return 企业基础信息列表model
     */
    @RequestMapping(Constants.VIEW_PREFIX + "enterprise")
    public ModelAndView enterprise(){
        return new ModelAndView("enterprise/list");
    }

    /**
     * @description 返回危害因素管理页面
     * @param
     * @return java.lang.String
     */
    @RequestMapping(Constants.VIEW_PREFIX + "harmFactor")
    public String harmfactor() {
        return "harmfactor/list";
    }


    /**
     * 系统维护主页
     */
    @RequestMapping(Constants.VIEW_PREFIX + "maintain")
    public String main(){
        return "redirect:/maintain";
    }


    /**
     * 监管区域列表页面
     * @return 监管区域列表model
     */
    @RequestMapping(Constants.VIEW_PREFIX + "monitor")
    public ModelAndView monitor(){
        return new ModelAndView("monitor/list");
    }


    /**
     * @description 返回推荐机构管理页面
     */
    @RequestMapping(Constants.VIEW_PREFIX + "recommendInstitution")
    public String recommendInstitution() {
        return "recommendinstitution/list";
    }

    /**
     * 企业文档备份页面
     * @return
     */
    @RequestMapping(Constants.VIEW_PREFIX + "backup")
    public String list(){
        return "backup/list";
    }

    /**
     * 系统咨询页面
     * @return
     */
    @RequestMapping(Constants.VIEW_PREFIX + "consult")
    public String consult(){
        return "consult/list";
    }

    /**
     * 修改密码
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "password/update")
    public String passwordUpdate() {
        return "user/passwordUpdate";
    }
}
