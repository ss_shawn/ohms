package com.lanware.ehs.admin.recommendinstitution.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.admin.recommendinstitution.service.RecommendInstitutionService;
import com.lanware.ehs.mapper.SysRecommendInstitutionMapper;
import com.lanware.ehs.mapper.custom.SysRecommendInstitutionCustomMapper;
import com.lanware.ehs.pojo.SysRecommendInstitution;
import com.lanware.ehs.pojo.SysRecommendInstitutionExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 推荐机构管理interface实现类
 * @author WangYumeng
 * @date 2019-07-03 16:43
 */
@Service
public class RecommendInstitutionServiceImpl implements RecommendInstitutionService {
    @Autowired
    /** 注入sysRecommendInstitutionMapper的bean */
    private SysRecommendInstitutionMapper sysRecommendInstitutionMapper;
    @Autowired
    /** 注入sysRecommendInstitutionCustomMapper的bean */
    private SysRecommendInstitutionCustomMapper sysRecommendInstitutionCustomMapper;

    @Override
    /**
     * @description 查询推荐机构list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含sysRecommendInstitutions和totalNum的result
     */
    public JSONObject listRecommendInstitutions(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<SysRecommendInstitution> sysRecommendInstitutions = sysRecommendInstitutionCustomMapper
                .listRecommendInstitutions(condition);
        // 查询数据和分页数据装进result
        result.put("sysRecommendInstitutions", sysRecommendInstitutions);
        if (pageNum != null && pageSize != null) {
            PageInfo<SysRecommendInstitution> pageInfo = new PageInfo<SysRecommendInstitution>(sysRecommendInstitutions);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 根据id查询推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:23
     * @param id 推荐机构id
     * @return com.lanware.management.pojo.SysRecommendInstitution 返回SysRecommendInstitution对象
     */
    public SysRecommendInstitution getRecommendInstitutionById(Long id) {
        return sysRecommendInstitutionMapper.selectByPrimaryKey(id);
    }

    @Override
    /**
     * @description 新增推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:25
     * @param sysRecommendInstitution 推荐机构pojo
     * @return int 插入成功的数目
     */
    public int insertRecommendInstitution(SysRecommendInstitution sysRecommendInstitution) {
        return sysRecommendInstitutionMapper.insert(sysRecommendInstitution);
    }

    @Override
    /**
     * @description 更新危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 17:26
     * @param sysRecommendInstitution 推荐机构pojo
     * @return int 更新成功的数目
     */
    public int updateRecommendInstitution(SysRecommendInstitution sysRecommendInstitution) {
        return sysRecommendInstitutionMapper.updateByPrimaryKeySelective(sysRecommendInstitution);
    }

    @Override
    /**
     * @description 删除推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:35
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    public int deleteRecommendInstitution(Long id) {
        return sysRecommendInstitutionMapper.deleteByPrimaryKey(id);
    }

    @Override
    /**
     * @description 批量删除推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:37
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    public int deleteRecommendInstitutions(Long[] ids) {
        SysRecommendInstitutionExample sysRecommendInstitutionExample = new SysRecommendInstitutionExample();
        SysRecommendInstitutionExample.Criteria criteria = sysRecommendInstitutionExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return sysRecommendInstitutionMapper.deleteByExample(sysRecommendInstitutionExample);
    }
}
