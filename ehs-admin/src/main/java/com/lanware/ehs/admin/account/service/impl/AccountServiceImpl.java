package com.lanware.ehs.admin.account.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.admin.account.service.AccountService;
import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.mapper.EnterpriseUserMapper;
import com.lanware.ehs.mapper.MonitorUserMapper;
import com.lanware.ehs.mapper.SysUserMapper;
import com.lanware.ehs.mapper.custom.EnterpriseUserCustomMapper;
import com.lanware.ehs.mapper.custom.MonitorUserCustomMapper;
import com.lanware.ehs.mapper.custom.SysUserCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.MonitorUserCustom;
import com.lanware.ehs.pojo.custom.SysUserCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description 账号管理interface实现类
 * @author WangYumeng
 * @date 2019-07-01 13:59
 */
@Slf4j
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class AccountServiceImpl implements AccountService {
    @Autowired
    /** 注入sysUserMapper的bean */
    private SysUserMapper sysUserMapper;
    @Autowired
    /** 注入monitorUserMapper的bean */
    private MonitorUserMapper monitorUserMapper;
    @Autowired
    /** 注入enterpriseUserMapper的bean */
    private EnterpriseUserMapper enterpriseUserMapper;
    @Autowired
    /** 注入sysUserCustomMapper的bean */
    private SysUserCustomMapper sysUserCustomMapper;
    @Autowired
    /** 注入enterpriseUserCustomMapper的bean */
    private EnterpriseUserCustomMapper enterpriseUserCustomMapper;
    @Autowired
    /** 注入monitorUserCustomMapper的bean */
    private MonitorUserCustomMapper monitorUserCustomMapper;

    @Value("${spring.mail.username}")
    /** 发件人邮箱的用户名 */
    private String formMail;
    @Autowired
    /** 注入javaMailSender的bean，用于邮件发送 */
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.subject}")
    /** 发送邮件的主题 */
    private String subject;

    @Override
    /**
     * @description 查询超级管理账号list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含sysUsers和totalNum的result
     */
    public JSONObject listSysUsers(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<SysUser> sysUsers = sysUserCustomMapper.listSysUsers(condition);
        result.put("sysUsers", sysUsers);
        if (pageNum != null && pageSize != null) {
            PageInfo<SysUser> pageInfo = new PageInfo<SysUser>(sysUsers);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 查询监管账号list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含monitorUsers和totalNum的result
     */
    public JSONObject listMonitorUsers(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<MonitorUserCustom> monitorUserCustoms = monitorUserCustomMapper.listMonitorUsers(condition);
        // 查询数据和分页数据装进result
        result.put("monitorUserCustoms", monitorUserCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<MonitorUserCustom> pageInfo = new PageInfo<MonitorUserCustom>(monitorUserCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 查询企业账号list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseUsers和totalNum的result
     */
    public JSONObject listEnterpriseUsers(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseUserCustom> enterpriseUserCustoms = enterpriseUserCustomMapper.listEnterpriseUsers(condition);
        result.put("enterpriseUserCustoms", enterpriseUserCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseUserCustom> pageInfo = new PageInfo<EnterpriseUserCustom>(enterpriseUserCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 根据id查询超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 14:45
     * @param id 超级管理账号id
     * @return com.high.stand.management.pojo.SysUser 返回SysUser对象
     */
    public SysUser getSysUserById(Long id) {
        return sysUserMapper.selectByPrimaryKey(id);
    }

    @Override
    /**
     * @description 根据id查询监管账号信息
     * @author WangYumeng
     * @date 2019/7/1 14:45
     * @param id 监管账号id
     * @return com.high.stand.management.pojo.MonitorUser 返回MonitorUser对象
     */
    public MonitorUser getMonitorUserById(Long id) {
        return monitorUserMapper.selectByPrimaryKey(id);
    }

    @Override
    /**
     * @description 根据id查询企业账号信息
     * @author WangYumeng
     * @date 2019/7/1 14:45
     * @param id 企业账号id
     * @return com.high.stand.management.pojo.EnterpriseUser 返回EnterpriseUser对象
     */
    public EnterpriseUser getEnterpriseUserById(Long id) {
        return enterpriseUserMapper.selectByPrimaryKey(id);
    }

    @Override
    /**
     * @description 根据id查询监管账号扩展信息
     * @author WangYumeng
     * @date 2019/7/3 16:24
     * @param id 监管账号id
     * @return com.lanware.management.pojo.custom.MonitorUserCustom
     */
    public MonitorUserCustom getMonitorUserCustomById(Long id) {
        return monitorUserCustomMapper.getMonitorUserCustomById(id);
    }

    @Override
    /**
     * @description 根据id查询企业账号扩展信息
     * @author WangYumeng
     * @date 2019/7/3 16:24
     * @param id 企业账号id
     * @return com.lanware.management.pojo.custom.EnterpriseUserCustom
     */
    public EnterpriseUserCustom getEnterpriseUserCustomById(Long id) {
        return enterpriseUserCustomMapper.getEnterpriseUserCustomById(id);
    }

    @Override
    /**
     * @description 根据username查询超级管理账号信息
     * @param username 用户名
     * @return com.high.stand.management.pojo.SysUser 返回SysUser对象
     */
    public SysUser getSysUserByUsername(String username) {
        return sysUserCustomMapper.findByName(username);
    }

    @Override
    /**
     * @description 根据username查询监管账号信息
     * @param username 用户名
     * @return com.high.stand.management.pojo.MonitorUser 返回MonitorUser对象
     */
    public MonitorUser getMonitorUserByUsername(String username) {
        return monitorUserCustomMapper.findByName(username);
    }

    @Override
    /**
     * @description 根据username查询企业账号信息
     * @param username 用户名
     * @return com.high.stand.management.pojo.EnterpriseUser 返回EnterpriseUser对象
     */
    public EnterpriseUser getEnterpriseUserByUsername(String username) {
        return enterpriseUserCustomMapper.findByName(username);
    }


    /**
     * @description 新增超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:21
     * @param sysUser 超级管理账号pojo
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public void insertSysUser(SysUser sysUser) throws MessagingException {
        sysUser.setPassword(MD5Util.encrypt(sysUser.getUsername().toLowerCase()
                , SysUserCustom.DEFAULT_PASSWORD));
        // 默认有效
        sysUser.setStatus(SysUserCustom.STATUS_VALID);
        // 默认删除
        sysUser.setIsDeleted(SysUserCustom.DELETED_YES);
        // 默认tab开启
        sysUser.setIsTab(SysUserCustom.TAB_OPEN);
        // 默认黑色主题
        sysUser.setTheme(SysUserCustom.THEME_BLACK);
        sysUser.setGmtCreate(new Date());

         sysUserMapper.insert(sysUser);

        sendHtmlMail(sysUser.getEmail(), subject, sysUser.getUsername());
    }


    /**
     * @description 新增监管账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:21
     * @param monitorUser 监管账号pojo
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public void insertMonitorUser(MonitorUser monitorUser) throws MessagingException {
        monitorUser.setPassword(MD5Util.encrypt(monitorUser.getUsername().toLowerCase()
                , MonitorUserCustom.DEFAULT_PASSWORD));
        // 默认有效
        monitorUser.setStatus(MonitorUserCustom.STATUS_VALID);
        // 默认不删除
        monitorUser.setIsDeleted(MonitorUserCustom.DELETED_NO);
        // 默认tab开启
        monitorUser.setIsTab(MonitorUserCustom.TAB_OPEN);
        // 默认黑色主题
        monitorUser.setTheme(MonitorUserCustom.THEME_BLACK);
        monitorUser.setGmtCreate(new Date());

        monitorUserMapper.insert(monitorUser);

        sendHtmlMail(monitorUser.getEmail(), subject, monitorUser.getUsername());
    }

    /**
     * @description 新增企业账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:23
     * @param enterpriseUser 企业账号pojosendHtmlMail
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public void insertEnterpriseUser(EnterpriseUser enterpriseUser) throws MessagingException {
        enterpriseUser.setPassword(MD5Util.encrypt(enterpriseUser.getUsername().toLowerCase()
                , EnterpriseUserCustom.DEFAULT_PASSWORD));
        // 默认启用
        enterpriseUser.setStatus(EnterpriseUserCustom.STATUS_VALID);
        // 默认不删除
        enterpriseUser.setIsDeleted(EnterpriseUserCustom.DELETED_NO);
        // 默认管理员权限
        enterpriseUser.setIsAdmin(EnterpriseUserCustom.ADMIN_YES);
        // 默认tab开启
        enterpriseUser.setIsTab(EnterpriseUserCustom.TAB_OPEN);
        // 默认黑色主题
        enterpriseUser.setTheme(EnterpriseUserCustom.THEME_BLACK);
        enterpriseUser.setGmtCreate(new Date());

        enterpriseUserMapper.insert(enterpriseUser);

        //发送邮件
        sendHtmlMail(enterpriseUser.getEmail(), subject, enterpriseUser.getUsername());



    }

    @Override
    /**
     * @description 更新超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:24
     * @param sysUser 超级管理账号pojo
     * @return int 更新成功的数目
     */
    public int updateSysUser(SysUser sysUser) {
        return sysUserMapper.updateByPrimaryKeySelective(sysUser);
    }

    @Override
    /**
     * @description 更新监管账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:24
     * @param monitorUser 监管账号pojo
     * @return int 更新成功的数目
     */
    public int updateMonitorUser(MonitorUser monitorUser) {
        return monitorUserMapper.updateByPrimaryKeySelective(monitorUser);
    }

    @Override
    /**
     * @description 更新企业账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:25
     * @param enterpriseUser 企业账号pojo
     * @return int 更新成功的数目
     */
    public int updateEnterpriseUser(EnterpriseUser enterpriseUser) {
        return enterpriseUserMapper.updateByPrimaryKeySelective(enterpriseUser);
    }

    @Override
    /**
     * @description 删除超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 17:07
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    public int deleteSysUser(Long id) {
        return sysUserMapper.deleteByPrimaryKey(id);
    }

    @Override
    /**
     * @description 批量删除超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 17:17
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    public int deleteSysUsers(Long[] ids) {
        SysUserExample sysUserExample = new SysUserExample();
        SysUserExample.Criteria criteria = sysUserExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return sysUserMapper.deleteByExample(sysUserExample);
    }

    @Override
    /**
     * @description 批量禁用超级管理账号
     * @author WangYumeng
     * @date 2019/7/2 17:17
     * @param ids 要禁用的数组ids
     * @return int 禁用成功的数目
     */
    public int disableSysUsers(Long[] ids) {
        SysUser sysUser = new SysUser();
        sysUser.setStatus(1);
        sysUser.setGmtModified(new Date());
        SysUserExample sysUserExample = new SysUserExample();
        SysUserExample.Criteria criteria = sysUserExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return sysUserMapper.updateByExampleSelective(sysUser, sysUserExample);
    }

    @Override
    /**
     * @description 批量禁用监管账号
     * @author WangYumeng
     * @date 2019/7/2 17:37
     * @param ids 要禁用的数组ids
     * @return int 禁用成功的数目
     */
    public int disableMonitorUsers(Long[] ids) {
        MonitorUser monitorUser = new MonitorUser();
        monitorUser.setStatus(1);
        monitorUser.setGmtModified(new Date());
        MonitorUserExample monitorUserExample = new MonitorUserExample();
        MonitorUserExample.Criteria criteria = monitorUserExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return monitorUserMapper.updateByExampleSelective(monitorUser, monitorUserExample);
    }

    @Override
    /**
     * @description 批量禁用企业账号
     * @author WangYumeng
     * @date 2019/7/2 17:57
     * @param ids 要禁用的数组ids
     * @return int 禁用成功的数目
     */
    public int disableEnterpriseUsers(Long[] ids) {
        EnterpriseUser enterpriseUser = new EnterpriseUser();
        enterpriseUser.setStatus(1);
        enterpriseUser.setGmtModified(new Date());
        EnterpriseUserExample enterpriseUserExample = new EnterpriseUserExample();
        EnterpriseUserExample.Criteria criteria = enterpriseUserExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseUserMapper.updateByExampleSelective(enterpriseUser, enterpriseUserExample);
    }

    @Override
    /**
     * @description 批量启用超级管理账号
     * @author WangYumeng
     * @date 2019/7/2 18:07
     * @param ids 要启用的数组ids
     * @return int 启成功的数目
     */
    public int enableSysUsers(Long[] ids) {
        SysUser sysUser = new SysUser();
        sysUser.setStatus(0);
        sysUser.setGmtModified(new Date());
        SysUserExample sysUserExample = new SysUserExample();
        SysUserExample.Criteria criteria = sysUserExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return sysUserMapper.updateByExampleSelective(sysUser, sysUserExample);
    }

    @Override
    /**
     * @description 批量启用监管账号
     * @author WangYumeng
     * @date 2019/7/2 18:17
     * @param ids 要启用的数组ids
     * @return int 启用成功的数目
     */
    public int enableMonitorUsers(Long[] ids) {
        MonitorUser monitorUser = new MonitorUser();
        monitorUser.setStatus(0);
        monitorUser.setGmtModified(new Date());
        MonitorUserExample monitorUserExample = new MonitorUserExample();
        MonitorUserExample.Criteria criteria = monitorUserExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return monitorUserMapper.updateByExampleSelective(monitorUser, monitorUserExample);
    }

    @Override
    /**
     * @description 批量启用企业账号
     * @author WangYumeng
     * @date 2019/7/2 18:27
     * @param ids 要启用的数组ids
     * @return int 启用成功的数目
     */
    public int enableEnterpriseUsers(Long[] ids) {
        EnterpriseUser enterpriseUser = new EnterpriseUser();
        enterpriseUser.setStatus(0);
        enterpriseUser.setGmtModified(new Date());
        EnterpriseUserExample enterpriseUserExample = new EnterpriseUserExample();
        EnterpriseUserExample.Criteria criteria = enterpriseUserExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseUserMapper.updateByExampleSelective(enterpriseUser, enterpriseUserExample);
    }

    @Override
    /**
     * @description 批量重置超级管理账号密码
     * @author WangYumeng
     * @date 2019/7/3 08:37
     * @param sysUsers 要重置密码的超级管理账号list
     * @return int 重置密码成功的数目
     */
    public int resetSysUsers(List<SysUser> sysUsers) {
        return sysUserCustomMapper.updateSysUsers(sysUsers);
    }

    @Override
    /**
     * @description 批量重置监管账号密码
     * @author WangYumeng
     * @date 2019/7/3 08:47
     * @param monitorUsers 要重置密码的监管账号list
     * @return int 重置密码成功的数目
     */
    public int resetMonitorUsers(List<MonitorUser> monitorUsers) {
        return monitorUserCustomMapper.updateMonitorUsers(monitorUsers);
    }

    @Override
    /**
     * @description 批量重置企业账号密码
     * @author WangYumeng
     * @date 2019/7/3 08:57
     * @param enterpriseUsers 要重置密码的企业账号list
     * @return int 重置密码成功的数目
     */
    public int resetEnterpriseUsers(List<EnterpriseUser> enterpriseUsers) {
        return enterpriseUserCustomMapper.updateEnterpriseUsers(enterpriseUsers);
    }

    @Override
    /**
     * @description 发送html邮件
     * @author WangYumeng
     * @date 2019/7/2 14:04
     * @param toMail 收件人地址
     * @param subject 主题
     * @param content 内容，包含html
     * @return void
     */
    public void sendHtmlMail(String toMail, String subject, String username) throws MessagingException {
        // 账号注册成功的邮件发送html模板
        String content = "<div style='background-color:#ECECEC; padding: 35px;'>" +
                "<table cellpadding='0' align='center' " +
                "style='width: 600px; margin: 0px auto; text-align: left; position: relative; " +
                "border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; " +
                "border-bottom-left-radius: 5px; font-size: 14px; font-family:微软雅黑, 黑体; line-height: 1.5; " +
                "box-shadow: rgb(153, 153, 153) 0px 0px 5px; border-collapse: collapse; background-position: " +
                "initial initial; background-repeat: initial initial;background:#fff;'>" +
                "<tbody><tr>" +
                "<th valign='middle' style='height: 25px; line-height: 25px; padding: 15px 35px; " +
                "border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #42a3d3; " +
                "background-color: #49bcff; border-top-left-radius: 5px; border-top-right-radius: 5px; " +
                "border-bottom-right-radius: 0px; border-bottom-left-radius: 0px;'>" +
                "<font face='微软雅黑' size='5' style='color: rgb(255, 255, 255); '>注册成功！（职业健康智慧管理系统）</font>" +
                "</th></tr><tr><td>" +
                "<div style='padding:25px 35px 40px; background-color:#fff;'>" +
                "<h2 style='margin: 5px 0px; '><font color='#333333' style='line-height: 20px; '>" +
                "<font style='line-height: 22px; ' size='4'>亲爱的" + username + "</font></font></h2>" +
                "<p>感谢您加入职业健康智慧管理系统！下面是您的账号信息</p>" +
                "您的账号：<b>" + username + "</b><br>" +
                "您的密码：<b>123456</b><br>" +
                "您注册时的时间：<b>" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "</b><br>" +
                "当您在使用本系统时，遵守当地法律法规。<br>" +
                "<p align='right'>职业健康智慧管理系统</p>" +
                "<p align='right'>" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "</p>" +
                "<div style='width:700px;margin:0 auto;'>" +
                "<div style='padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;" +
                "line-height:1.3em;font-size:12px;'>" +
                "<p>此为系统邮件，请勿回复<br>" +
                "请保管好您的邮箱，避免账号被他人盗用" +
                "</p></div></div></div></td></tr></tbody></table></div>";
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            // 邮件helper类
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setFrom(formMail);
            mimeMessageHelper.setTo(toMail);
            mimeMessageHelper.setSubject(subject);
            mimeMessageHelper.setText(content, true);
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            log.error(e.getMessage(),e);
            throw e;
        }
    }
}
