package com.lanware.ehs.admin.maintain.service;

import com.lanware.ehs.pojo.SysMaintain;
import com.lanware.ehs.pojo.SysUser;

import java.util.List;

public interface MaintainService {
    /**
     * 获取系统配置列表数据
     */
    List<SysMaintain> listMaintain();
    /**
     * 更新sysMaintain
     */
    int update(SysMaintain maintain);
    /**
     * 插入sysMaintain
     */
    int insert(SysMaintain maintain);
    /**
     * 根据id删除sysMaintain
     */
    int delete(Long id);
}
