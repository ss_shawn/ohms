package com.lanware.ehs.admin.maintain.service.impl;

import com.lanware.ehs.admin.maintain.service.MaintainService;
import com.lanware.ehs.mapper.SysMaintainMapper;
import com.lanware.ehs.pojo.SysMaintain;
import com.lanware.ehs.pojo.SysMaintainExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaintainServiceImpl implements MaintainService {
    @Autowired
    SysMaintainMapper sysMaintainMapper;
    @Override
    public List<SysMaintain> listMaintain() {
        SysMaintainExample sysMaintainExample = new SysMaintainExample();
        SysMaintainExample.Criteria criteria = sysMaintainExample.createCriteria();
        return  sysMaintainMapper.selectByExample(sysMaintainExample);
    }

    @Override
    public int update(SysMaintain maintain) {
        return sysMaintainMapper.updateByPrimaryKey(maintain);
    }

    @Override
    public int insert(SysMaintain maintain) {
        return sysMaintainMapper.insert(maintain);
    }

    @Override
    public int delete(Long id) {
        return sysMaintainMapper.deleteByPrimaryKey(id);
    }


}
