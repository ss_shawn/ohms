package com.lanware.ehs.admin.harmfactor.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.admin.harmfactor.service.HarmFactorService;
import com.lanware.ehs.mapper.BaseHarmFactorMapper;
import com.lanware.ehs.mapper.custom.BaseHarmFactorCustomMapper;
import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.BaseHarmFactorExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description 危害因素管理interface实现类
 * @author WangYumeng
 * @date 2019-07-03 13:43
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class HarmFactorServiceImpl implements HarmFactorService {
    @Autowired
    /** 注入baseHarmFactorMapper的bean */
    private BaseHarmFactorMapper baseHarmFactorMapper;
    @Autowired
    /** 注入baseHarmFactorCustomMapper的bean */
    private BaseHarmFactorCustomMapper baseHarmFactorCustomMapper;

    @Override
    /**
     * @description 查询危害因素list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含baseHarmFactors和totalNum的result
     */
    public JSONObject listHarmFactors(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<BaseHarmFactor> baseHarmFactors = baseHarmFactorCustomMapper.listHarmFactors(condition);
        // 查询数据和分页数据装进result
        result.put("baseHarmFactors", baseHarmFactors);
        if (pageNum != null && pageSize != null) {
            PageInfo<BaseHarmFactor> pageInfo = new PageInfo<BaseHarmFactor>(baseHarmFactors);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 根据id查询危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:23
     * @param id 危害因素id
     * @return com.lanware.management.pojo.BaseHarmFactor 返回BaseHarmFactor对象
     */
    public BaseHarmFactor getHarmFactorById(Long id) {
        return baseHarmFactorMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 根据name查询危害因素
     * @param name 危害因素名称
     * @return com.lanware.management.pojo.BaseHarmFactor 返回BaseHarmFactor对象
     */
    @Override
    public BaseHarmFactor getHarmFactorByName(String name) {
        return baseHarmFactorCustomMapper.getHarmFactorByName(name);
    }

    /**
     * @description 根据condition查询危害因素
     * @param condition 查询条件
     * @return com.lanware.management.pojo.BaseHarmFactor 返回BaseHarmFactor对象
     */
    @Override
    public BaseHarmFactor getHarmFactorByCondition(Map<String, Object> condition) {
        return baseHarmFactorCustomMapper.getHarmFactorByCondition(condition);
    }

    @Override
    /**
     * @description 新增危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:25
     * @param baseHarmFactor 危害因素pojo
     * @return int 插入成功的数目
     */
    public int insertHarmFactor(BaseHarmFactor baseHarmFactor) {
        return baseHarmFactorMapper.insert(baseHarmFactor);
    }

    @Override
    /**
     * @description 批量插入危害因素信息
     * @param baseHarmFactorList 危害因素list
     * @return int 插入成功的数目
     */
    public int insertHarmFactors(List<BaseHarmFactor> baseHarmFactorList) {
        return baseHarmFactorCustomMapper.insertBatch(baseHarmFactorList);
    }

    @Override
    /**
     * @description 更新危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:26
     * @param baseHarmFactor 危害因素pojo
     * @return int 更新成功的数目
     */
    public int updateHarmFactor(BaseHarmFactor baseHarmFactor) {
        return baseHarmFactorCustomMapper.updateHarmFactor(baseHarmFactor);
    }

    @Override
    /**
     * @description 删除危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:35
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    public int deleteHarmFactor(Long id) {
        return baseHarmFactorMapper.deleteByPrimaryKey(id);
    }

    @Override
    /**
     * @description 批量删除危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:37
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    public int deleteHarmFactors(Long[] ids) {
        BaseHarmFactorExample baseHarmFactorExample = new BaseHarmFactorExample();
        BaseHarmFactorExample.Criteria criteria = baseHarmFactorExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return baseHarmFactorMapper.deleteByExample(baseHarmFactorExample);
    }

    @Override
    @Transactional
    public void importExcelData(List<BaseHarmFactor> list){
        //根据危害因素名称查询数据库中是否有重复的数据，重复的更新
        BaseHarmFactorExample baseHarmFactorExample = new BaseHarmFactorExample();
        for(BaseHarmFactor factor : list){
            baseHarmFactorExample.clear();
            String name = factor.getName();
            BaseHarmFactorExample.Criteria criteria = baseHarmFactorExample.createCriteria();
            criteria.andNameEqualTo(name);
            List<BaseHarmFactor> factors = baseHarmFactorMapper.selectByExample(baseHarmFactorExample);
            if(factors.size() > 0){
                //存在危害因素同名数据，更新数据
                factor.setId(factors.get(0).getId());
                factor.setGmtModified(new Date());
                baseHarmFactorMapper.updateByPrimaryKey(factor);
            }else{
                //不存在危害因素同名数据，新增数据
                baseHarmFactorMapper.insertSelective(factor);
            }
        }
    }
}
