package com.lanware.ehs.admin.consult.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.consult.service.ConsultService;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.pojo.SysConsult;
import com.lanware.ehs.pojo.custom.SysConsultCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/consult")
/**
 * @description 系统咨询controller
 */
public class ConsultController {
    @Autowired
    /** 注入consultService的bean */
    private ConsultService consultService;

    @RequestMapping("")
    /**
     * @description 返回系统咨询页面
     * @param
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "consult/list";
    }


    @RequestMapping("/listConsults")
    @ResponseBody
    /**
     * @description 获取系统咨询result
     * @param pageNum 请求的是页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat listConsults(Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = consultService.listConsults(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/reply")
    @ResponseBody
    /**
     * @description 回复咨询的问题
     * @param sysConsult 系统咨询pojo
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat reply(SysConsult sysConsult) {
        sysConsult.setAnswerStatus(SysConsultCustom.STATUS_ANSWER_NEW);
        sysConsult.setAnswerTime(new Date());
        sysConsult.setGmtModified(new Date());
        if (consultService.updateSysConsult(sysConsult) > 0) {
            return ResultFormat.success("回复成功");
        } else {
            return ResultFormat.error("回复失败");
        }
    }

    @RequestMapping("/updateReadStatus")
    @ResponseBody
    /**
     * @description 更新咨询阅读状态
     * @param sysConsult 系统咨询pojo
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat updateReadStatus(SysConsult sysConsult) {
        sysConsult.setReadStatus(SysConsultCustom.STATUS_READ_YES);
        sysConsult.setGmtModified(new Date());
        if (consultService.updateSysConsult(sysConsult) > 0) {
            return ResultFormat.success();
        } else {
            return ResultFormat.error();
        }
    }

    @RequestMapping("/deleteSysConsult")
    @ResponseBody
    /**
     * @description 删除系统咨询
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat deleteSysConsult(Long id) {
        if (consultService.deleteSysConsult(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteSysConsults")
    @ResponseBody
    /**
     * @description 批量删除系统咨询
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat deleteHarmFactors(Long[] ids) {
        if (consultService.deleteSysConsults(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
