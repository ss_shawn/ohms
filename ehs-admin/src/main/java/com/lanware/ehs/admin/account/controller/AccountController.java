package com.lanware.ehs.admin.account.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.account.service.AccountService;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.SysUser;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.MonitorUserCustom;
import com.lanware.ehs.pojo.custom.SysUserCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/account")
@Slf4j
/**
 * @description 账号管理controller
 * @author WangYumeng
 * @date 2019-07-01 13:46
 */
public class AccountController {
    @Autowired
    /** 注入accountService的bean */
    private AccountService accountService;

    @RequestMapping("")
    /**
     * @description 返回账号管理页面
     * @author WangYumeng
     * @date 2019/7/1 13:53
     * @param
     * @return java.lang.String
     */
    public String list() {
        return "account/list";
    }


    @RequestMapping("/listSysUsers")
    @ResponseBody
    /**
     * @description 获取超级管理账号result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat listSysUsers(Integer pageNum, Integer pageSize
            , String keyword, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = accountService.listSysUsers(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }
    
    @RequestMapping("/listMonitorUsers")
    @ResponseBody
    /**
     * @description 获取监管账号result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat listMonitorUsers(Integer pageNum, Integer pageSize
            , String keyword, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = accountService.listMonitorUsers(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listEnterpriseUsers")
    @ResponseBody
    /**
     * @description 获取企业账号result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat listEnterpriseUsers(Integer pageNum, Integer pageSize
            , String keyword, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = accountService.listEnterpriseUsers(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editSysUser/save")
    @ResponseBody
    /**
     * @description 保存超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:25
     * @param sysUser 超级管理账号pojo
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat save(SysUser sysUser) {
        try {
            if (sysUser.getId() == null) {
                if (accountService.getSysUserByUsername(sysUser.getUsername()) == null) {
                    accountService.insertSysUser(sysUser);
                    return ResultFormat.success("新增成功");
                } else {
                    return ResultFormat.error("新增账号失败，用户名已存在");
                }
            } else {
                sysUser.setGmtModified(new Date());
                if (accountService.updateSysUser(sysUser) > 0) {
                    return ResultFormat.success("更新成功");
                } else {
                    return ResultFormat.error("更新失败");
                }
            }
        }catch(MailSendException e){
            throw new EhsException("发送邮箱失败,请确认邮箱是否正确",e);
        } catch (Exception e) {
            String message = "新增账号失败";
            log.error(message,e);
            throw new EhsException(message);
        }
    }

    @RequestMapping("/editMonitorUser/save")
    @ResponseBody
    /**
     * @description 保存监管账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:27
     * @param monitorUser 监管账号pojo
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat save(MonitorUser monitorUser) {
        try {
            if (monitorUser.getId() == null) {
                if (accountService.getMonitorUserByUsername(monitorUser.getUsername()) == null) {
                    accountService.insertMonitorUser(monitorUser);
                    return ResultFormat.success("新增成功");
                } else {
                    return ResultFormat.error("新增账号失败，用户名已存在");
                }
            } else {
                monitorUser.setGmtModified(new Date());
                if (accountService.updateMonitorUser(monitorUser) > 0) {
                    return ResultFormat.success("更新成功");
                } else {
                    return ResultFormat.error("更新失败");
                }
            }
        }catch(MailSendException e){
            throw new EhsException("发送邮箱失败,请确认邮箱是否正确",e);
        } catch (Exception e) {
            String message = "新增账号失败";
            log.error(message,e);
            throw new EhsException(message);
        }
    }

    @RequestMapping("/editEnterpriseUser/save")
    @ResponseBody
    /**
     * @description 保存企业账号信息
     * @author WangYumeng
     * @date 2019/7/1 15:29
     * @param enterpriseUser 企业账号pojo
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat save(EnterpriseUser enterpriseUser) {
        try {
            if (enterpriseUser.getId() == null) {
                if (accountService.getEnterpriseUserByUsername(enterpriseUser.getUsername()) == null) {
                    accountService.insertEnterpriseUser(enterpriseUser);
                    return ResultFormat.success("新增账号成功");
                } else {
                    return ResultFormat.error("新增账号失败，用户名已存在");
                }
            }else{
                enterpriseUser.setGmtModified(new Date());
                if (accountService.updateEnterpriseUser(enterpriseUser) > 0) {
                    return ResultFormat.success("更新成功");
                } else {
                    return ResultFormat.error("更新失败");
                }
            }
        }catch(MailSendException e){
            throw new EhsException("发送邮箱失败,请确认邮箱是否正确",e);
        } catch (Exception e) {
            String message = "新增账号失败";
            log.error(message,e);
            throw new EhsException(message);
        }
    }

    @RequestMapping("/viewSysUser")
    /**
     * @description 查看超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/3 16:00
     * @param id 超级管理账号id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewSysUser(Long id) {
        JSONObject data = new JSONObject();
        SysUser sysUser = accountService.getSysUserById(id);
        data.put("sysUser", sysUser);
        return new ModelAndView("account/viewSysUser", data);
    }

    @RequestMapping("/viewMonitorUser")
    /**
     * @description 查看监管账号信息
     * @author WangYumeng
     * @date 2019/7/3 16:10
     * @param id 监管账号id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewMonitorUser(Long id) {
        JSONObject data = new JSONObject();
        MonitorUserCustom monitorUserCustom = accountService.getMonitorUserCustomById(id);
        data.put("monitorUserCustom", monitorUserCustom);
        return new ModelAndView("account/viewMonitorUser", data);
    }

    @RequestMapping("/viewEnterpriseUser")
    /**
     * @description 查看企业账号信息
     * @author WangYumeng
     * @date 2019/7/3 16:20
     * @param id 企业账号id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewEnterpriseUser(Long id) {
        JSONObject data = new JSONObject();
        EnterpriseUserCustom enterpriseUserCustom = accountService.getEnterpriseUserCustomById(id);
        data.put("enterpriseUserCustom", enterpriseUserCustom);
        return new ModelAndView("account/viewEnterpriseUser", data);
    }

    @RequestMapping("/deleteSysUser")
    @ResponseBody
    /**
     * @description 删除超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 17:06
     * @param id 要删除的id
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat deleteSysUser(Long id) {
        if (accountService.deleteSysUser(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteSysUsers")
    @ResponseBody
    /**
     * @description 批量删除超级管理账号信息
     * @author WangYumeng
     * @date 2019/7/1 17:16
     * @param ids 要删除的数组ids
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat deleteSysUsers(Long[] ids) {
        if (accountService.deleteSysUsers(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/disableSysUsers")
    @ResponseBody
    /**
     * @description 批量禁用超级管理账号
     * @author WangYumeng
     * @date 2019/7/2 17:16
     * @param ids 要禁用的数组ids
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat disableSysUsers(Long[] ids) {
        if (accountService.disableSysUsers(ids) == ids.length) {
            return ResultFormat.success("禁用成功");
        }else{
            return ResultFormat.error("禁用失败");
        }
    }

    @RequestMapping("/disableMonitorUsers")
    @ResponseBody
    /**
     * @description 批量禁用监管账号
     * @author WangYumeng
     * @date 2019/7/2 17:36
     * @param ids 要禁用的数组ids
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat disableMonitorUsers(Long[] ids) {
        if (accountService.disableMonitorUsers(ids) == ids.length) {
            return ResultFormat.success("禁用成功");
        }else{
            return ResultFormat.error("禁用失败");
        }
    }

    @RequestMapping("/disableEnterpriseUsers")
    @ResponseBody
    /**
     * @description 批量禁用企业账号
     * @author WangYumeng
     * @date 2019/7/2 17:56
     * @param ids 要禁用的数组ids
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat disableEnterpriseUsers(Long[] ids) {
        if (accountService.disableEnterpriseUsers(ids) == ids.length) {
            return ResultFormat.success("禁用成功");
        }else{
            return ResultFormat.error("禁用失败");
        }
    }

    @RequestMapping("/enableSysUsers")
    @ResponseBody
    /**
     * @description 批量启用超级管理账号
     * @author WangYumeng
     * @date 2019/7/2 18:06
     * @param ids 要启用的数组ids
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat enableSysUsers(Long[] ids) {
        if (accountService.enableSysUsers(ids) == ids.length) {
            return ResultFormat.success("启用成功");
        }else{
            return ResultFormat.error("启用失败");
        }
    }

    @RequestMapping("/enableMonitorUsers")
    @ResponseBody
    /**
     * @description 批量启用监管账号
     * @author WangYumeng
     * @date 2019/7/2 18:16
     * @param ids 要启用的数组ids
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat enableMonitorUsers(Long[] ids) {
        if (accountService.enableMonitorUsers(ids) == ids.length) {
            return ResultFormat.success("启用成功");
        }else{
            return ResultFormat.error("启用失败");
        }
    }

    @RequestMapping("/enableEnterpriseUsers")
    @ResponseBody
    /**
     * @description 批量启用企业账号
     * @author WangYumeng
     * @date 2019/7/2 18:26
     * @param ids 要启用的数组ids
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat enableEnterpriseUsers(Long[] ids) {
        if (accountService.enableEnterpriseUsers(ids) == ids.length) {
            return ResultFormat.success("启用成功");
        }else{
            return ResultFormat.error("启用失败");
        }
    }

    @RequestMapping("/resetSysUsers")
    @ResponseBody
    /**
     * @description 批量重置超级管理账号密码
     * @param dataJSON 要重置密码的dataJSON
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat resetSysUsers(String dataJSON) {
        // json转成list
        List<SysUser> sysUsers = JSONArray.parseArray(dataJSON, SysUser.class);
        // 遍历sysUsers修改密码
        for (SysUser sysUser: sysUsers) {
            sysUser.setGmtModified(new Date());
            sysUser.setPassword(MD5Util.encrypt(sysUser.getUsername().toLowerCase(), SysUserCustom.DEFAULT_PASSWORD));
        }
        // 执行批量更新
        if (accountService.resetSysUsers(sysUsers) > 0) {
            return ResultFormat.success("重置密码成功");
        } else {
            return ResultFormat.error("重置密码失败");
        }
    }

    @RequestMapping("/resetMonitorUsers")
    @ResponseBody
    /**
     * @description 批量重置监管账号密码
     * @param dataJSON 要重置密码的dataJSON
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat resetMonitorUsers(String dataJSON) {
        // json转成list
        List<MonitorUser> monitorUsers = JSONArray.parseArray(dataJSON, MonitorUser.class);
        // 遍历monitorUsers修改密码
        for (MonitorUser monitorUser: monitorUsers) {
            monitorUser.setGmtModified(new Date());
            monitorUser.setPassword(MD5Util.encrypt(monitorUser.getUsername().toLowerCase()
                    , MonitorUserCustom.DEFAULT_PASSWORD));
        }
        // 执行批量更新
        if (accountService.resetMonitorUsers(monitorUsers) > 0) {
            return ResultFormat.success("重置密码成功");
        }else{
            return ResultFormat.error("重置密码失败");
        }
    }

    @RequestMapping("/resetEnterpriseUsers")
    @ResponseBody
    /**
     * @description 批量重置企业账号密码
     * @param dataJSON 要重置密码的dataJSON
     * @return com.high.stand.management.common.format.ResultFormat
     */
    public ResultFormat resetEnterpriseUsers(String dataJSON) {
        // json转成list
        List<EnterpriseUser> enterpriseUsers = JSONArray.parseArray(dataJSON, EnterpriseUser.class);
        // 遍历enterpriseUsers修改密码
        for (EnterpriseUser enterpriseUser: enterpriseUsers) {
            enterpriseUser.setGmtModified(new Date());
            enterpriseUser.setPassword(MD5Util.encrypt(enterpriseUser.getUsername().toLowerCase()
                    , EnterpriseUserCustom.DEFAULT_PASSWORD));
        }
        // 执行批量更新
        if (accountService.resetEnterpriseUsers(enterpriseUsers) > 0) {
            return ResultFormat.success("重置密码成功");
        } else {
            return ResultFormat.error("重置密码失败");
        }
    }
}
