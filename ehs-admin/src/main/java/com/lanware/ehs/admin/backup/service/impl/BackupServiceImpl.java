package com.lanware.ehs.admin.backup.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.admin.backup.service.BackupService;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.mapper.EnterpriseDocumentBackupMapper;
import com.lanware.ehs.mapper.custom.EnterpriseDocumentBackupCustomMapper;
import com.lanware.ehs.pojo.EnterpriseDocumentBackup;
import com.lanware.ehs.pojo.custom.EnterpriseDocumentBackupCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BackupServiceImpl implements BackupService {
    @Autowired
    private EnterpriseDocumentBackupCustomMapper enterpriseDocumentBackupCustomMapper;

    @Autowired
    private OSSTools ossTools;

    @Override
    public Page<EnterpriseDocumentBackupCustom> queryEnterpriseDocumentBackupByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<EnterpriseDocumentBackupCustom> page = PageHelper.startPage(pageNum, pageSize);
        enterpriseDocumentBackupCustomMapper.queryDocumentBackupByCondition(condition);
        return page;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        EnterpriseDocumentBackup backup = enterpriseDocumentBackupCustomMapper.selectByPrimaryKey(id);
        //删除备份数据
        enterpriseDocumentBackupCustomMapper.deleteByPrimaryKey(id);
        //删除备份文件
        ossTools.deleteOSS(backup.getFilePath());
    }
}
