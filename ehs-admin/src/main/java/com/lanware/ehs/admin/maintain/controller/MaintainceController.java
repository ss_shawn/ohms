package com.lanware.ehs.admin.maintain.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.maintain.service.MaintainService;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.pojo.SysMaintain;
import com.lanware.ehs.pojo.SysUser;
import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/maintain")
public class MaintainceController {
    @Autowired
    MaintainService maintainService;

    /**
     * 自动转换日期类型的字段格式
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, false));
    }

    /**
     * 系统维护主页
     */
    @RequestMapping("")
    public ModelAndView main(){
        List<SysMaintain> list = maintainService.listMaintain();
        SysMaintain maintain = new SysMaintain();
        if(list.size() > 0){
            maintain = list.get(0);
        }
        JSONObject data = new JSONObject();
        data.put("maintain", maintain);
        return new ModelAndView("maintain/main", data);
    }

    /**
     * 新增或更新系统维护表数据
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultFormat save(SysMaintain maintain){
        List<SysMaintain> list = maintainService.listMaintain();
        if (maintain.getId() == null && list.size() == 0) {  //新增
            Date date = new Date();
            maintain.setGmtCreate(date);
            maintain.setGmtModified(date);
            if (maintainService.insert(maintain) > 0) {
                return ResultFormat.success("保存成功");
            } else {
                return ResultFormat.error("保存失败");
            }
        } else {
            maintain.setGmtModified(new Date());
            if (maintainService.update(maintain) > 0) {
                return ResultFormat.success("保存成功");
            } else {
                return ResultFormat.error("保存失败");
            }
        }
    }

    /**
     * 根据id删除数据
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultFormat delete(Long id){
        if (maintainService.delete(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
