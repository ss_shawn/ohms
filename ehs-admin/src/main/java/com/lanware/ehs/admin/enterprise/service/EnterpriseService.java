package com.lanware.ehs.admin.enterprise.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.custom.EnterpriseBaseinfoCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface EnterpriseService {

    /**
     * 分页查询企业基础信息
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<EnterpriseBaseinfoCustom> queryEnterpriseBaseinfoByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);


    /**
     * 查询企业信息
     * @param condition
     * @return
     */
    List<EnterpriseBaseinfoCustom> queryEnterpriseBaseinfoByCondition(Map<String, Object> condition);

    /**
     * 保存企业基础信息
     * @param enterpriseBaseinfo 企业对象
     * @return 更新数量
     */
    int saveEnterpriseBaseinfo(EnterpriseBaseinfo enterpriseBaseinfo);

    /**
     * 查询企业基础信息
     * @param id 企业id
     * @return 企业对象
     */
    EnterpriseBaseinfo queryEnterpriseBaseinfoById(Long id);

    /**
     * 启用（禁用）企业
     * @param ids 企业id集合
     * @param enable 操作标识 true 启用 false 禁用
     * @return 更新数量
     */
    int enableEnterpriseBaseinfo(List<Long> ids, Boolean enable);

    /**
     * 同步企业数据到局端
     * @param ids
     */
    EhsResult transferData(List<Long> ids);
}
