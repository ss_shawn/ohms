package com.lanware.ehs.admin.recommendinstitution.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.recommendinstitution.service.RecommendInstitutionService;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.pojo.SysRecommendInstitution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 推荐机构视图
 */
@Controller("recommendInstitutionView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入recommendInstitutionService的bean */
    @Autowired
    private RecommendInstitutionService recommendInstitutionService;

    /**
     * @description 新增推荐机构信息
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("recommendInstitution/edit")
    public ModelAndView add() {
        JSONObject data = new JSONObject();
        SysRecommendInstitution sysRecommendInstitution = new SysRecommendInstitution();
        data.put("sysRecommendInstitution", sysRecommendInstitution);
        return new ModelAndView("recommendinstitution/edit", data);
    }

    /**
     * @description 编辑推荐机构信息
     * @param id 推荐机构id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("recommendInstitution/edit/{id}")
    public ModelAndView edit(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        SysRecommendInstitution sysRecommendInstitution = recommendInstitutionService.getRecommendInstitutionById(id);
        data.put("sysRecommendInstitution", sysRecommendInstitution);
        return new ModelAndView("recommendinstitution/edit", data);
    }
}
