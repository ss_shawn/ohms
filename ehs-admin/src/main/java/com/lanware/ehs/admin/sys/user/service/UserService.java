package com.lanware.ehs.admin.sys.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lanware.ehs.pojo.SysUser;
import com.lanware.ehs.pojo.custom.SysUserCustom;

public interface UserService{
    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return 用户
     */
    SysUserCustom findByName(String username);

    /**
     * 更新用户登录时间
     *
     * @param username 用户名
     */
    void updateLoginTime(String username);


    /**
     * 修改密码
     *
     * @param username 用户名
     * @param password 新密码
     */
    void updatePassword(String username, String password);


}
