package com.lanware.ehs.admin.harmfactor.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.harmfactor.service.HarmFactorService;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.pojo.BaseHarmFactor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 危害因素视图
 */
@Controller("harmfactorView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入harmFactorService的bean */
    @Autowired
    private HarmFactorService harmFactorService;

    /**
     * @description 新增危害因素信息
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("harmFactor/editHarmFactor")
    public ModelAndView addHarmFactor() {
        JSONObject data = new JSONObject();
        BaseHarmFactor baseHarmFactor = new BaseHarmFactor();
        data.put("baseHarmFactor", baseHarmFactor);
        return new ModelAndView("harmfactor/edit", data);
    }

    /**
     * @description 编辑危害因素信息
     * @param id 危害因素id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("harmFactor/editHarmFactor/{id}")
    public ModelAndView editHarmFactor(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        BaseHarmFactor baseHarmFactor = harmFactorService.getHarmFactorById(id);
        data.put("baseHarmFactor", baseHarmFactor);
        return new ModelAndView("harmfactor/edit", data);
    }

    /**
     * @description 返回批量导入危害因素信息页面
     * @param
     * @return java.lang.String 页面路径
     */
    @RequestMapping("harmFactor/importHarmFactors")
    public String importHarmFactors() {
        return "harmfactor/import";
    }
}
