package com.lanware.ehs.admin.consult.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.SysConsult;

import java.util.Map;

/**
 * @description 系统咨询interface
 */
public interface ConsultService {
    /**
     * @description 查询系统咨询list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含sysRecommendInstitutions和totalNum的result
     */
    JSONObject listConsults(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 更新系统咨询
     * @param sysConsult 系统咨询pojo
     * @return int 更新成功的数目
     */
    int updateSysConsult(SysConsult sysConsult);

    /**
     * @description 删除系统咨询
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteSysConsult(Long id);

    /**
     * @description 批量删除系统咨询
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteSysConsults(Long[] ids);
}
