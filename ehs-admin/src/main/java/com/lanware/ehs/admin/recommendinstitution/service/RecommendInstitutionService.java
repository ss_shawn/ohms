package com.lanware.ehs.admin.recommendinstitution.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.SysRecommendInstitution;

import java.util.Map;

/**
 * @description 推荐机构管理interface
 * @author WangYumeng
 * @date 2019-07-03 16:43
 */
public interface RecommendInstitutionService {
    /**
     * @description 查询推荐机构list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含sysRecommendInstitutions和totalNum的result
     */
    JSONObject listRecommendInstitutions(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:23
     * @param id 推荐机构id
     * @return com.lanware.management.pojo.SysRecommendInstitution 返回SysRecommendInstitution对象
     */
    SysRecommendInstitution getRecommendInstitutionById(Long id);

    /**
     * @description 新增推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:25
     * @param sysRecommendInstitution 推荐机构pojo
     * @return int 插入成功的数目
     */
    int insertRecommendInstitution(SysRecommendInstitution sysRecommendInstitution);

    /**
     * @description 更新危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 17:26
     * @param sysRecommendInstitution 推荐机构pojo
     * @return int 更新成功的数目
     */
    int updateRecommendInstitution(SysRecommendInstitution sysRecommendInstitution);

    /**
     * @description 删除推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:35
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteRecommendInstitution(Long id);

    /**
     * @description 批量删除推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:37
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteRecommendInstitutions(Long[] ids);
}
