package com.lanware.ehs.admin.backup.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.EnterpriseDocumentBackup;
import com.lanware.ehs.pojo.custom.EnterpriseDocumentBackupCustom;

import java.util.Map;

public interface BackupService {

    Page<EnterpriseDocumentBackupCustom> queryEnterpriseDocumentBackupByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 根据ID删除用户
     * @param id
     */
    void delete(Long id);
}
