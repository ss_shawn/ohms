package com.lanware.ehs.admin;

public enum ModuleName {

    ENTERPRISE("enterprise");

    ModuleName(String value){
        this.value = value;
    }

    private String value;

    public String getValue(){
        return this.value;
    }
}
