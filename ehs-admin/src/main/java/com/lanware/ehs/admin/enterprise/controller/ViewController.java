package com.lanware.ehs.admin.enterprise.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.enterprise.service.EnterpriseService;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(Constants.VIEW_PREFIX)
public class ViewController {
    @Autowired
    private EnterpriseService enterpriseService;

    /**
     * 编辑企业基础信息页面
     * @param id 企业id
     * @return 编辑企业基础信息model
     */
    @RequestMapping("enterprise/edit/{id}")
    public ModelAndView edit(@PathVariable  Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            EnterpriseBaseinfo enterpriseBaseinfo = enterpriseService.queryEnterpriseBaseinfoById(id);
            data.put("enterpriseBaseinfo", enterpriseBaseinfo);
        }
        return new ModelAndView("enterprise/edit", data);
    }

    /**
     * 添加企业信息
     * @return
     */
    @RequestMapping("enterprise/add")
    public ModelAndView add(){
        JSONObject data = new JSONObject();
        return new ModelAndView("enterprise/edit", data);
    }

}
