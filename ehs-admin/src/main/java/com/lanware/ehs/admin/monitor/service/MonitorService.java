package com.lanware.ehs.admin.monitor.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.MonitorArea;
import com.lanware.ehs.pojo.custom.EnterpriseBaseinfoCustom;
import com.lanware.ehs.pojo.custom.MonitorAreaCustom;
import com.lanware.ehs.pojo.custom.MonitorEnterpriseCustom;

import java.util.List;
import java.util.Map;

public interface MonitorService {

    /**
     * 分页查询监管区域
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<MonitorAreaCustom> queryMonitorAreaByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    List<MonitorAreaCustom> queryMonitorAreaByCondition(Map<String, Object> condition);

    /**
     * 保存监管区域
     * @param monitorArea 监管区域对象
     * @param enterpriseIds 监管企业id集合
     * @return 更新数量
     */
    int saveMonitorArea(MonitorArea monitorArea, List<Long> enterpriseIds);

    /**
     * 查询监管区域
     * @param id 监管区域id
     * @return 监管区域对象
     */
    MonitorArea queryMonitorAreaById(Long id);

    /**
     * 分页查询监管企业
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<MonitorEnterpriseCustom> queryMonitorEnterpriseByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 分页查询未选择的监管企业
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<MonitorEnterpriseCustom> queryUnselectedMonitorEnterpriseByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 启用（禁用）监管区域
     * @param ids 监管区域id集合
     * @param enable 操作标识 true 启用 false 禁用
     * @return 更新数量
     */
    int enableMonitorArea(List<Long> ids, Boolean enable);

    /**
     * 删除监管企业
     * @param areaId 监管区域id
     * @return 更新结果
     */
    int deleteMonitorEnterpriseByAreaId(Long areaId);

    /**
     * 删除监管区域
     * @param ids 监管区域id集合
     * @return 更新结果
     */
    int deleteMonitorAreaByIds(List<Long> ids);
}
