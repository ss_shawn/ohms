package com.lanware.ehs.admin.sys.user.controller;

import com.lanware.ehs.admin.sys.user.service.UserService;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.pojo.custom.SysUserCustom;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

@Slf4j
@RestController
@RequestMapping("user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;
    /**
     * 修改密码
     * @param oldPassword
     * @param newPassword
     * @return
     * @throws EhsException
     */
    @PostMapping("password/update")
    public EhsResult updatePassword(
            @NotBlank(message = "{required}") String oldPassword,
            @NotBlank(message = "{required}") String newPassword) throws EhsException {
        try {

            SysUserCustom user = (SysUserCustom)getSubject().getPrincipal();
            user = userService.findByName(user.getUsername());
            if (!StringUtils.equals(user.getPassword(), MD5Util.encrypt(user.getUsername(), oldPassword))) {
                throw new EhsException("原密码不正确");
            }
            userService.updatePassword(user.getUsername(), newPassword);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "修改密码失败，" + e.getMessage();
            log.error(message, e);
            throw new EhsException(message);
        }
    }
    @GetMapping("password/verify")
    public EhsResult verifyPassword(){
        SysUserCustom userCustom = (SysUserCustom)getSubject().getPrincipal();
        String defaultPassword = SysUserCustom.DEFAULT_PASSWORD;
        if (StringUtils.equals(userCustom.getPassword(), MD5Util.encrypt(userCustom.getUsername(), defaultPassword))) {
            return EhsResult.build(500,"您的登录密码与默认密码一致，请尽快修改您的登录密码！");
        }
        return EhsResult.ok();
    }
}
