package com.lanware.ehs.admin.monitor.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.admin.monitor.service.MonitorService;
import com.lanware.ehs.mapper.MonitorAreaMapper;
import com.lanware.ehs.mapper.MonitorEnterpriseMapper;
import com.lanware.ehs.mapper.custom.MonitorAreaCustomMapper;
import com.lanware.ehs.mapper.custom.MonitorEnterpriseCustomMapper;
import com.lanware.ehs.pojo.MonitorArea;
import com.lanware.ehs.pojo.MonitorEnterprise;
import com.lanware.ehs.pojo.MonitorEnterpriseExample;
import com.lanware.ehs.pojo.custom.MonitorAreaCustom;
import com.lanware.ehs.pojo.custom.MonitorEnterpriseCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class MonitorServiceImpl implements MonitorService {

    private MonitorAreaMapper monitorAreaMapper;

    private MonitorAreaCustomMapper monitorAreaCustomMapper;

    private MonitorEnterpriseMapper monitorEnterpriseMapper;

    private MonitorEnterpriseCustomMapper monitorEnterpriseCustomMapper;

    @Autowired
    public MonitorServiceImpl(MonitorAreaMapper monitorAreaMapper, MonitorAreaCustomMapper monitorAreaCustomMapper, MonitorEnterpriseMapper monitorEnterpriseMapper, MonitorEnterpriseCustomMapper monitorEnterpriseCustomMapper){
        this.monitorAreaMapper = monitorAreaMapper;
        this.monitorAreaCustomMapper = monitorAreaCustomMapper;
        this.monitorEnterpriseMapper = monitorEnterpriseMapper;
        this.monitorEnterpriseCustomMapper = monitorEnterpriseCustomMapper;
    }

    @Override
    public Page<MonitorAreaCustom> queryMonitorAreaByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<MonitorAreaCustom> page = PageHelper.startPage(pageNum, pageSize);
        monitorAreaCustomMapper.queryMonitorAreaByCondition(condition);
        return page;
    }


    @Override
    public List<MonitorAreaCustom> queryMonitorAreaByCondition(Map<String, Object> condition) {
        return monitorAreaCustomMapper.queryMonitorAreaByCondition(condition);
    }

    @Override
    @Transactional
    public int saveMonitorArea(MonitorArea monitorArea, List<Long> enterpriseIds) {
        //保存监管区域
        int num;
        if (monitorArea.getId() == null){
            monitorArea.setStatus(0);
            monitorArea.setIsDeleted(0);
            monitorArea.setGmtCreate(new Date());
            num = monitorAreaMapper.insert(monitorArea);
        }else {
            monitorArea.setGmtModified(new Date());
            num = monitorAreaMapper.updateByPrimaryKeySelective(monitorArea);
        }
        //删除旧监管企业
        this.deleteMonitorEnterpriseByAreaId(monitorArea.getId());
        if (enterpriseIds != null && !enterpriseIds.isEmpty()){
            //保存新监管企业
            List<MonitorEnterprise> monitorEnterprises = new ArrayList<MonitorEnterprise>();
            for (Long enterpriseId : enterpriseIds){
                MonitorEnterprise monitorEnterprise = new MonitorEnterprise();
                monitorEnterprise.setEnterpriseId(enterpriseId);
                monitorEnterprise.setAreaId(monitorArea.getId());
                monitorEnterprise.setGmtCreate(new Date());
                monitorEnterprises.add(monitorEnterprise);
            }
            monitorEnterpriseCustomMapper.insertBatch(monitorEnterprises);
        }
        return num;
    }

    @Override
    public MonitorArea queryMonitorAreaById(Long id) {
        return monitorAreaMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<MonitorEnterpriseCustom> queryMonitorEnterpriseByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<MonitorEnterpriseCustom> page = PageHelper.startPage(pageNum, pageSize);
        monitorEnterpriseCustomMapper.queryMonitorEnterpriseByCondition(condition);
        return page;
    }

    @Override
    public Page<MonitorEnterpriseCustom> queryUnselectedMonitorEnterpriseByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<MonitorEnterpriseCustom> page = PageHelper.startPage(pageNum, pageSize);
        List<MonitorEnterpriseCustom> list = monitorEnterpriseCustomMapper.queryUnselectedMonitorEnterpriseByCondition(condition);
        return page;
    }

    @Override
    public int enableMonitorArea(List<Long> ids, Boolean enable) {
        return monitorAreaCustomMapper.enableMonitorAreaByIds(ids, enable ? 0l : 1l, new Date());
    }

    @Override
    public int deleteMonitorEnterpriseByAreaId(Long areaId) {
        MonitorEnterpriseExample monitorEnterpriseExample = new MonitorEnterpriseExample();
        MonitorEnterpriseExample.Criteria criteria = monitorEnterpriseExample.createCriteria();
        criteria.andAreaIdEqualTo(areaId);
        return monitorEnterpriseMapper.deleteByExample(monitorEnterpriseExample);
    }

    @Override
    public int deleteMonitorAreaByIds(List<Long> ids) {
        return monitorAreaCustomMapper.deleteMonitorAreaByIds(ids);
    }
}
