package com.lanware.ehs.admin.recommendinstitution.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.recommendinstitution.service.RecommendInstitutionService;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.pojo.SysRecommendInstitution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/recommendInstitution")
/**
 * @description 推荐机构管理controller
 * @author WangYumeng
 * @date 2019-07-03 16:39
 */
public class RecommendInstitutionController {
    @Autowired
    /** 注入recommendInstitutionService的bean */
    private RecommendInstitutionService recommendInstitutionService;

    @RequestMapping("")
    /**
     * @description 返回推荐机构管理页面
     * @author WangYumeng
     * @date 2019/7/3 16:40
     * @param
     * @return java.lang.String
     */
    public String list() {
        return "recommendinstitution/list";
    }


    @RequestMapping("/list")
    @ResponseBody
    /**
     * @description 获取推荐机构result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat list(Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = recommendInstitutionService.listRecommendInstitutions(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/edit/save")
    @ResponseBody
    /**
     * @description 保存推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:19
     * @param sysRecommendInstitution 推荐机构pojo
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat save(SysRecommendInstitution sysRecommendInstitution) {
        if (sysRecommendInstitution.getId() == null) {
            sysRecommendInstitution.setGmtCreate(new Date());
            if (recommendInstitutionService.insertRecommendInstitution(sysRecommendInstitution) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            sysRecommendInstitution.setGmtModified(new Date());
            if (recommendInstitutionService.updateRecommendInstitution(sysRecommendInstitution) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/view")
    /**
     * @description 查看推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 18:09
     * @param id 推荐机构id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView vidw(Long id) {
        JSONObject data = new JSONObject();
        SysRecommendInstitution sysRecommendInstitution = recommendInstitutionService.getRecommendInstitutionById(id);
        data.put("sysRecommendInstitution", sysRecommendInstitution);
        return new ModelAndView("recommendinstitution/view", data);
    }

    @RequestMapping("/deleteRecommendInstitution")
    @ResponseBody
    /**
     * @description 删除推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:33
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat deleteRecommendInstitution(Long id) {
        if (recommendInstitutionService.deleteRecommendInstitution(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteRecommendInstitutions")
    @ResponseBody
    /**
     * @description 批量删除推荐机构信息
     * @author WangYumeng
     * @date 2019/7/3 17:34
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat deleteRecommendInstitutions(Long[] ids) {
        if (recommendInstitutionService.deleteRecommendInstitutions(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
