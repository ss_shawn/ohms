package com.lanware.ehs.admin.harmfactor.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.admin.harmfactor.service.HarmFactorService;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUploadUtil;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.BaseHarmFactorExample;
import com.lanware.ehs.service.BaseHarmFactorService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

@Controller
@RequestMapping("/harmFactor")
/**
 * @description 危害因素管理controller
 * @author WangYumeng
 * @date 2019-07-03 13:39
 */
@Slf4j
public class HarmFactorController {
    @Autowired
    /** 注入harmFactorService的bean */
    private HarmFactorService harmFactorService;
    @Autowired
    /** 注入fileUploadUtil的bean */
    private FileUploadUtil fileUploadUtil;

    @Autowired
    private BaseHarmFactorService baseHarmFactorService;

    @RequestMapping("")
    /**
     * @description 返回危害因素管理页面
     * @author WangYumeng
     * @date 2019/7/3 13:40
     * @param
     * @return java.lang.String
     */
    public String list() {
        return "harmfactor/list";
    }


    @RequestMapping("/listHarmFactors")
    @ResponseBody
    /**
     * @description 获取危害因素result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listHarmFactors(Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = harmFactorService.listHarmFactors(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editHarmFactor/save")
    @ResponseBody
    /**
     * @description 保存危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:19
     * @param baseHarmFactor 危害因素pojo
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat save(BaseHarmFactor baseHarmFactor) {
        if (baseHarmFactor.getId() == null) {
            if (harmFactorService.getHarmFactorByName(baseHarmFactor.getName()) == null) {
                baseHarmFactor.setGmtCreate(new Date());
                if (harmFactorService.insertHarmFactor(baseHarmFactor) > 0) {
                    return ResultFormat.success("添加成功");
                } else {
                    return ResultFormat.error("添加失败");
                }
            } else {
                return ResultFormat.error("添加失败，该危害因素名称已存在");
            }
        } else {
            Map<String, Object> condition = new HashMap<>();
            condition.put("id", baseHarmFactor.getId());
            condition.put("name", baseHarmFactor.getName());
            if (harmFactorService.getHarmFactorByCondition(condition) == null) {
                baseHarmFactor.setGmtModified(new Date());
                if (harmFactorService.updateHarmFactor(baseHarmFactor) > 0) {
                    return ResultFormat.success("更新成功");
                } else {
                    return ResultFormat.error("更新失败");
                }
            } else {
                return ResultFormat.error("更新失败，该危害因素名称已存在");
            }
        }
    }

    @RequestMapping("/viewHarmFactor")
    /**
     * @description 查看危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 17:09
     * @param id 危害因素id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView vidwHarmFactor(Long id) {
        JSONObject data = new JSONObject();
        BaseHarmFactor baseHarmFactor = harmFactorService.getHarmFactorById(id);
        data.put("baseHarmFactor", baseHarmFactor);
        return new ModelAndView("harmfactor/view", data);
    }

    @RequestMapping("/deleteHarmFactor")
    @ResponseBody
    /**
     * @description 删除危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:33
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat deleteHarmFactor(Long id) {
        if (harmFactorService.deleteHarmFactor(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteHarmFactors")
    @ResponseBody
    /**
     * @description 批量删除危害因素信息
     * @author WangYumeng
     * @date 2019/7/3 14:34
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat deleteHarmFactors(Long[] ids) {
        if (harmFactorService.deleteHarmFactors(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/importExcelData")
    @ResponseBody
    /**
     * @description 导入危害因素Excel数据
     * @param request HttpServlet请求，包含上传文件信息
     * @return com.lanware.ehs.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat importExcelData(HttpServletRequest request) {
        MultipartHttpServletRequest mrequest = (MultipartHttpServletRequest)request;
        MultipartFile file = mrequest.getFile("file");
        // 创建临时文件
        File tempFile = null;
        try {
            // 通过上传的文件得到服务器上的临时文件
            tempFile = fileUploadUtil.fileupload(file);
            String fileName = tempFile.getName();
            InputStream is = new FileInputStream(tempFile);
            Workbook workbook = null;
            // 判断文件类型：Excel 2007，Excel 2003
            if (fileName.endsWith("xlsx")) {
                workbook = new XSSFWorkbook(is);
            } else if (fileName.endsWith("xls")) {
                workbook = new HSSFWorkbook(is);
            }
            // 判断导入excel是否有不匹配数据的flag
            boolean flag = false;
            // 创建危害因素list
            List<BaseHarmFactor> baseHarmFactorList = new ArrayList<BaseHarmFactor>();
            List<String> nameList = new ArrayList<>();
            Sheet sheet = workbook.getSheetAt(0);
            // 从excel第二行开始读取数据
            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
                Row row = sheet.getRow(rowNum);
                if (row != null) {
                    // 读取一行中每列的数据
                    String name = "";
                    if (row.getCell(1) != null) {
                        row.getCell(1).setCellType(CellType.STRING);
                        name = row.getCell(1).getStringCellValue();
                        //如果危害因素名称为空，则跳出继续执行下一行。
                        if(org.apache.commons.lang3.StringUtils.isEmpty(name)){
                            continue;
                        }
                    } else {
                        flag = true;
                        break;
                    }
                    String cas = "";
                    if (row.getCell(2) != null) {
                        row.getCell(2).setCellType(CellType.STRING);
                        cas = row.getCell(2).getStringCellValue();
                    }
                    String category = "";
                    if (row.getCell(3) != null) {
                        row.getCell(3).setCellType(CellType.STRING);
                        category = row.getCell(3).getStringCellValue();
                    } else {
                        flag = true;
                        break;
                    }
                    Integer isHighlyToxic = null;
                    if (row.getCell(4) != null) {
                        row.getCell(4).setCellType(CellType.STRING);
                        String isHighlyToxicStr = row.getCell(4).getStringCellValue();
                        if(org.apache.commons.lang3.StringUtils.isNotEmpty(isHighlyToxicStr)){
                            isHighlyToxic = Integer.parseInt(isHighlyToxicStr);
                        }
                    } else {
                        flag = true;
                        break;
                    }
                    if (isHighlyToxic.intValue() != 0 && isHighlyToxic.intValue() != 1) {
                        flag = true;
                        break;
                    }
                    BigDecimal mac = null;
                    if (row.getCell(5) != null) {
                        row.getCell(5).setCellType(CellType.STRING);
                        String macStr = row.getCell(5).getStringCellValue();
                        if(org.apache.commons.lang3.StringUtils.isNotEmpty(macStr)){
                            mac = new BigDecimal(macStr);
                        }

                    }
                    BigDecimal twa = null;
                    if (row.getCell(6) != null) {
                        row.getCell(6).setCellType(CellType.STRING);
                        String twaStr = row.getCell(6).getStringCellValue();
                        if(org.apache.commons.lang3.StringUtils.isNotEmpty(twaStr)){
                            twa = new BigDecimal(twaStr);
                        }
                    }

                    BigDecimal stel = null;
                    if (row.getCell(7) != null) {
                        row.getCell(7).setCellType(CellType.STRING);
                        String stelStr = row.getCell(7).getStringCellValue();
                        if(org.apache.commons.lang3.StringUtils.isNotEmpty(stelStr)){
                            stel = new BigDecimal(stelStr);
                        }
                    }
                    BigDecimal overLimitMultiple = null;
                    if (row.getCell(8) != null) {
                        row.getCell(8).setCellType(CellType.STRING);
                        String overLimitMultipleStr = row.getCell(8).getStringCellValue();
                        if(org.apache.commons.lang3.StringUtils.isNotEmpty(overLimitMultipleStr)){
                            overLimitMultiple = new BigDecimal(overLimitMultipleStr);
                        }

                    }
                    String remark = "";
                    if (row.getCell(9) != null) {
                        row.getCell(9).setCellType(CellType.STRING);
                        remark = row.getCell(9).getStringCellValue();
                    }
                    // 创建危害因素对象，把excel每行读取数据set进对象
                    BaseHarmFactor baseHarmFactor = new BaseHarmFactor();
                    baseHarmFactor.setName(name);
                    baseHarmFactor.setCas(cas);
                    baseHarmFactor.setCategory(category);
                    baseHarmFactor.setIsHighlyToxic(isHighlyToxic);
                    baseHarmFactor.setMac(mac);
                    baseHarmFactor.setTwa(twa);
                    baseHarmFactor.setStel(stel);
                    baseHarmFactor.setOverLimitMultiple(overLimitMultiple);
                    baseHarmFactor.setRemark(remark);
                    baseHarmFactor.setGmtCreate(new Date());
                    // 危害因素对象添加进危害因素list
                    baseHarmFactorList.add(baseHarmFactor);
                }
            }
            // 如果flag为true，直接返回"导入失败"
            if (flag) {
                return ResultFormat.error("导入失败，请与模板进行对比检查是否有不匹配的数据");
            }
            // 批量插入危害因素
            harmFactorService.importExcelData(baseHarmFactorList);
            workbook.close();
            return ResultFormat.success("导入成功");
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return ResultFormat.error("导入失败，请与模板进行对比检查是否有不匹配的数据");

        } finally {
            // 删除临时文件
            if (tempFile != null && tempFile.exists()) {
                tempFile.delete();
            }
        }
    }
}
