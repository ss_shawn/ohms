package com.lanware.ehs.admin.sys.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lanware.ehs.admin.sys.user.service.UserService;
import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.mapper.custom.SysUserCustomMapper;
import com.lanware.ehs.pojo.SysUser;
import com.lanware.ehs.pojo.SysUserExample;
import com.lanware.ehs.pojo.custom.SysUserCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 *
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {
    @Autowired
    private SysUserCustomMapper sysUserCustomMapper;
    @Override
    public SysUserCustom findByName(String username) {
        return this.sysUserCustomMapper.findByName(username);
    }

    @Override
    @Transactional
    public void updateLoginTime(String username) {
        SysUser user = new SysUser();
        user.setLastLoginTime(new Date());

        SysUserExample example = new SysUserExample();
        SysUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        sysUserCustomMapper.updateByExampleSelective(user,example);
    }

    @Override
    @Transactional
    public void updatePassword(String username, String password) {
        SysUser user = new SysUser();
        user.setGmtModified(new Date());
        user.setPassword(MD5Util.encrypt(username, password));
        SysUserExample example = new SysUserExample();
        SysUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        sysUserCustomMapper.updateByExampleSelective(user,example);
    }
}
