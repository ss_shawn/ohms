package com.lanware.ehs.admin.enterprise.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.google.common.collect.Maps;
import com.lanware.ehs.admin.enterprise.service.EnterpriseService;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.custom.EnterpriseBaseinfoCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.SysUserCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/enterprise")
public class EnterpriseController extends BaseController {

    private EnterpriseService enterpriseService;
    @Autowired
    private OSSTools ossTools;


    @Autowired
    public EnterpriseController(EnterpriseService enterpriseService){
        this.enterpriseService = enterpriseService;
    }

    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * 企业基础信息列表页面
     * @return 企业基础信息列表model
     */
    @RequestMapping("")
    public ModelAndView enterprise(){
        return new ModelAndView("enterprise/list");
    }

    /**
     * 分页查询企业基础信息
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public ResultFormat getList(Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("currentDate", new Date());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<EnterpriseBaseinfoCustom> page = enterpriseService.queryEnterpriseBaseinfoByCondition(pageNum, pageSize, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }

    /**
     * 查询企业数据
     * @return
     */
    @RequestMapping("/find")
    @ResponseBody
    public EhsResult find(){
        Map<String,Object> condition = Maps.newHashMap();
        List<EnterpriseBaseinfoCustom> list = enterpriseService.queryEnterpriseBaseinfoByCondition(condition);
        return EhsResult.ok(list);
    }



    /**
     * 查看企业基础信息页面
     * @param id 企业id
     * @return 查看企业基础信息model
     */
    @RequestMapping("/view")
    public ModelAndView view(Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            EnterpriseBaseinfo enterpriseBaseinfo = enterpriseService.queryEnterpriseBaseinfoById(id);
            data.put("enterpriseBaseinfo", enterpriseBaseinfo);
        }
        return new ModelAndView("enterprise/view", data);
    }

    /**
     * 保存企业基础信息
     * @param enterpriseBaseinfo 企业基础信息JSON
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(EnterpriseBaseinfo enterpriseBaseinfo){
        try {
            int num = enterpriseService.saveEnterpriseBaseinfo(enterpriseBaseinfo);
            if (num == 0){
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存企业信息失败");
            }
        } catch (EhsException e){
            throw e;
        }
        return EhsResult.ok();
    }

    /**
     * 启用（禁用）企业
     * @param idsJSON 企业id集合JSON
     * @param enable 操作标识 true 启用 false 禁用
     * @return 操作结果
     */
    @RequestMapping("/enable")
    @ResponseBody
    public ResultFormat enable(@RequestParam String idsJSON, @RequestParam Boolean enable){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        int num = enterpriseService.enableEnterpriseBaseinfo(ids, enable);
        if (num == 0){
            return ResultFormat.error((enable ? "启用" : "禁用") + "失败");
        }
        return ResultFormat.success();
    }

    /**
     * 同步局端数据
     * @param idsJSON
     * @return
     */
    @RequestMapping("/transfer")
    @ResponseBody
    public EhsResult transfer(@RequestParam String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        return enterpriseService.transferData(ids);
    }

    @PostMapping("file/upload")
    @ResponseBody
    public EhsResult uploadFile(@RequestParam("file") MultipartFile file) {
        if(file == null){
            return EhsResult.build(500,"上传文件为空");
        }
        String url;
        Map m = Maps.newHashMap();
        try {
            SysUserCustom userCustom = (SysUserCustom)getSubject().getPrincipal();
            String path = "/"+userCustom.getUsername()+"/enterprise";
            url = ossTools.upload(path,file);
            m.put("url", url);
            m.put("name", file.getOriginalFilename());
        } catch (Exception e) {
            throw new EhsException("上传文件失败");
        }
        return EhsResult.ok(m);
    }





}
