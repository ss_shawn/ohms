package com.lanware.ehs.admin.monitor.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.google.common.collect.Maps;
import com.lanware.ehs.admin.monitor.service.MonitorService;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.pojo.MonitorArea;
import com.lanware.ehs.pojo.custom.MonitorAreaCustom;
import com.lanware.ehs.pojo.custom.MonitorEnterpriseCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
@RequestMapping("/monitor")
public class MonitorController {

    private MonitorService monitorService;

    @Autowired
    public MonitorController(MonitorService monitorService){
        this.monitorService = monitorService;
    }

    /**
     * 监管区域列表页面
     * @return 监管区域列表model
     */
    @RequestMapping("")
    public ModelAndView monitor(){
        return new ModelAndView("monitor/list");
    }

    /**
     * 分页查询监管区域集合
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public ResultFormat getList(Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("currentDate", new Date());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<MonitorAreaCustom> page = monitorService.queryMonitorAreaByCondition(pageNum, pageSize, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }

    /**
     * 查找监管区域数据
     * @return
     */
    @RequestMapping("/find")
    @ResponseBody
    public EhsResult find(){
        Map<String,Object> condition = Maps.newHashMap();
        List<MonitorAreaCustom> list = monitorService.queryMonitorAreaByCondition(condition);
        return EhsResult.ok(list);
    }

    /**
     * 编辑监管区域页面
     * @param id 监管区域id
     * @return 编辑监管区域model
     */
    @RequestMapping("/edit")
    public ModelAndView edit(Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            MonitorArea monitorArea = monitorService.queryMonitorAreaById(id);
            data.put("monitorArea", monitorArea);
        }
        return new ModelAndView("monitor/edit", data);
    }

    /**
     * 查看监管区域页面
     * @param id 监管区域id
     * @return 查看监管区域model
     */
    @RequestMapping("/view")
    public ModelAndView view(Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            MonitorArea monitorArea = monitorService.queryMonitorAreaById(id);
            data.put("monitorArea", monitorArea);
        }
        return new ModelAndView("monitor/view", data);
    }

    /**
     * 查询监管企业集合
     * @param areaId 监管区域id
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getEnterpriseList")
    @ResponseBody
    public ResultFormat getEnterpriseList(Long areaId, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        List<MonitorEnterpriseCustom> list = new ArrayList<>();
        if (areaId != null){
            Map<String, Object> condition = new HashMap<>();
            condition.put("areaId", areaId);
            condition.put("keyword", keyword);
            condition.put("currentDate", new Date());
            if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
                condition.put("sortField", StringUtil.humpToUnderscore(sortField));
                condition.put("sortType", sortType);
            }
            list = monitorService.queryMonitorEnterpriseByCondition(0, 0, condition);
        }
        data.put("list", list);
        return ResultFormat.success(data);
    }

    /**
     * 选择企业页面
     * @return 选择企业model
     */
    @RequestMapping("/select")
    public ModelAndView select(){
        JSONObject data = new JSONObject();
        return new ModelAndView("monitor/select", data);
    }

    /**
     * 分页查询未选择的企业集合
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param areaId 监管区域id
     * @param selectedEnterpriseIdsJSON 已选企业id集合JSON
     * @param deletedEnterpriseIdsJSON 已删企业id集合JSON
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getUnselectedEnterpriseList")
    @ResponseBody
    public ResultFormat getUnselectedEnterpriseList(Integer pageNum, Integer pageSize, Long areaId, String selectedEnterpriseIdsJSON,  String deletedEnterpriseIdsJSON, String keyword, String sortField, String sortType) {
        JSONObject data = new JSONObject();
        List<Long> selectedEnterpriseIds = JSONArray.parseArray(selectedEnterpriseIdsJSON, Long.class);
        List<Long> deletedEnterpriseIds = JSONArray.parseArray(deletedEnterpriseIdsJSON, Long.class);
        Map<String, Object> condition = new HashMap<>();
        condition.put("areaId", areaId);
        condition.put("keyword", keyword);
        condition.put("selectedEnterpriseIds", selectedEnterpriseIds);
        condition.put("deletedEnterpriseIds", deletedEnterpriseIds);
        condition.put("currentDate", new Date());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)) {
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<MonitorEnterpriseCustom> page = monitorService.queryUnselectedMonitorEnterpriseByCondition(pageNum, pageSize, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }

    /**
     * 保存监管区域
     * @param monitorAreaJSON 监管区域JSON
     * @param enterpriseIdsJSON 监管企业id集合JSON
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultFormat save(String monitorAreaJSON, String enterpriseIdsJSON){
        MonitorArea monitorArea = JSONObject.parseObject(monitorAreaJSON, MonitorArea.class);
        List<Long> enterpriseIds = JSONArray.parseArray(enterpriseIdsJSON, Long.class);
        int num = monitorService.saveMonitorArea(monitorArea, enterpriseIds);
        if (num == 0){
            return ResultFormat.error("保存监管区域失败");
        }
        return ResultFormat.success();
    }

    /**
     * 启用（禁用）监管区域
     * @param idsJSON 监管区域id集合JSON
     * @param enable 操作标识 true 启用 false jiny
     * @return 操作结果
     */
    @RequestMapping("/enable")
    @ResponseBody
    public ResultFormat enable(@RequestParam String idsJSON, @RequestParam Boolean enable){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        int num = monitorService.enableMonitorArea(ids, enable);
        if (num == 0){
            return ResultFormat.error((enable ? "启用" : "禁用") + "失败");
        }
        return ResultFormat.success();
    }

    /**
     * 删除监管区域
     * @param idsJSON 监管区域id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultFormat delete(@RequestParam String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        int num = monitorService.deleteMonitorAreaByIds(ids);
        if (num == 0){
            return ResultFormat.error("删除监管区域失败");
        }
        return ResultFormat.success();
    }
}
