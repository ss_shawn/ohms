layui.extend({
    xadmin: 'lay/modules/xadmin',
    validate: 'lay/modules/validate'
}).define(['xadmin', 'conf'], function (exports) {
    layui.xadmin.initPage();
    //确认登录密码和默认密码是否一致
    layui.xadmin.verifyPassword();
    exports('index', {});
});