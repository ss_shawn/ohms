package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.EnterpriseDeclareCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 危害申报mapper扩展类
 */
public interface EnterpriseDeclareCustomMapper {
    /**
     * @description 查询作业场所危害申报list
     * @param condition 查询条件
     * @return 作业场所危害申报扩展list
     */
    List<EnterpriseDeclareCustom> listDeclares(Map<String, Object> condition);

    /**
     * @description 查询申报数据
     * @return 申报数据
     */
    Map<String, Object> queryDeclareData(Long enterpriseId);
}
