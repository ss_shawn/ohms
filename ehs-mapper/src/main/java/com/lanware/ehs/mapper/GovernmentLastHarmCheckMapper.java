package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.GovernmentLastHarmCheck;
import com.lanware.ehs.pojo.GovernmentLastHarmCheckExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GovernmentLastHarmCheckMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    long countByExample(GovernmentLastHarmCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    int deleteByExample(GovernmentLastHarmCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    int insert(GovernmentLastHarmCheck record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    int insertSelective(GovernmentLastHarmCheck record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    List<GovernmentLastHarmCheck> selectByExample(GovernmentLastHarmCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    GovernmentLastHarmCheck selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    int updateByExampleSelective(@Param("record") GovernmentLastHarmCheck record, @Param("example") GovernmentLastHarmCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    int updateByExample(@Param("record") GovernmentLastHarmCheck record, @Param("example") GovernmentLastHarmCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    int updateByPrimaryKeySelective(GovernmentLastHarmCheck record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_last_harm_check
     *
     * @mbg.generated Wed Oct 09 14:34:43 CST 2019
     */
    int updateByPrimaryKey(GovernmentLastHarmCheck record);
}