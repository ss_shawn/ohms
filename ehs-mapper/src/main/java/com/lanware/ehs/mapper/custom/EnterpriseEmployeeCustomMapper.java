package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.EnterpriseEmployeeCustom;

import java.util.List;
import java.util.Map;

public interface EnterpriseEmployeeCustomMapper {

    List<EnterpriseEmployeeCustom> queryEnterpriseEmployeeByCondition(Map<String, Object> condition);

    List<Map<String, String>> getAllFilesByIds(List<Long> ids);

    int deleteEnterpriseEmployeeByIds(List<Long> ids);

    String getEmployeeNameById(Long id);

    List<EnterpriseEmployeeCustom> findWorkEmployeesByEnterpriseId(Long enterpriseId);
}