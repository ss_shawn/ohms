package com.lanware.ehs.mapper.custom;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanware.ehs.mapper.SysUserMapper;
import com.lanware.ehs.pojo.SysUser;
import com.lanware.ehs.pojo.custom.SysUserCustom;

import java.util.List;
import java.util.Map;

/**
 * @description MonitorUserMapper扩展接口
 * @author WangYumeng
 * @date 2019-07-02 13:27
 */
public interface SysUserCustomMapper extends SysUserMapper {

    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return 用户
     */
    SysUserCustom findByName(String username);

    /**
     * @description 查询超级管理账号list
     * @param condition 查询条件
     * @return 超级管理账号list
     */
    List<SysUser> listSysUsers(Map<String, Object> condition);

    /**
     * @description 批量更新超级管理员账号
     * @author WangYumeng
     * @date 2019/7/3 08:37
     * @param sysUsers 要更新的超级管理账号list
     * @return int 更新成功的数目
     */
    int updateSysUsers(List<SysUser> sysUsers);
}
