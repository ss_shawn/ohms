package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.EmployeePostCustom;

import java.util.List;
import java.util.Map;

public interface EmployeePostCustomMapper {

    List<EmployeePostCustom> queryEmployeePostByCondition(Map<String, Object> condition);

    EmployeePostCustom queryEmployeePostById(Long id);

    List<EmployeePostCustom> listPostsByEmployeeId(Long employeeId);
    List<EmployeePostCustom> listEmployeePostByPostId(Map<String, Object> condition);
}