package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseWaitCheckMapper;
import com.lanware.ehs.pojo.EnterpriseWaitCheck;
import com.lanware.ehs.pojo.custom.EnterpriseWaitCheckCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 待检测项目扩展mapper
 */
public interface EnterpriseWaitCheckCustomMapper extends EnterpriseWaitCheckMapper {

    List<EnterpriseWaitCheckCustom> listWaitChecks(Map<String, Object> condition);

    List<EnterpriseWaitCheckCustom> listWaitCheckAccounts(Map<String, Object> condition);

    List<EnterpriseWaitCheckCustom> findEnterpriseWaitChecks(Map<String,Object> parameter);
}