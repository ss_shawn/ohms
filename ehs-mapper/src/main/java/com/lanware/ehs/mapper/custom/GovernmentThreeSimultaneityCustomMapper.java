package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.GovernmentThreeSimultaneityMapper;
import com.lanware.ehs.pojo.custom.GovernmentThreeSimultaneityCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 局端企业年度三同时信息扩展mapper
 */
public interface GovernmentThreeSimultaneityCustomMapper extends GovernmentThreeSimultaneityMapper {
    List<GovernmentThreeSimultaneityCustom> listGovernmentThreeSimultaneitys(Map<String, Object> condition);
}