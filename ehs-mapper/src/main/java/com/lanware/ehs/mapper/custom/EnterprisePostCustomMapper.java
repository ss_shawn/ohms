package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.custom.EnterprisePostCustom;
import com.lanware.ehs.pojo.custom.PostEmployeeResultCustom;

import java.util.List;
import java.util.Map;

public interface EnterprisePostCustomMapper {

    List<EnterprisePostCustom> queryEnterprisePostByCondition(Map<String, Object> condition);

    int deleteEnterprisePostByIds(List<Long> ids);

    EnterprisePostCustom queryEnterprisePostById(Long id);
    Page<PostEmployeeResultCustom> queryEnterprisePostEmployeeByCondition(Map<String, Object> condition);
}