package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.custom.EmployeeWorkWaitNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 防护用品mapper扩展类
 * @author
 * @date 2019-07-25 10:10
 */
public interface EmployeeWorkWaitNotifyCustomMapper {
    /**
     * @description 查询待岗前告知list
     * @param condition 查询条件
     * @return 待岗前告知list
     */
    Page<EmployeeWorkWaitNotifyCustom> findEmployeeWorkWaitNotifyList(Map<String, Object> condition);



    /**
     * @description 查询待岗前告知
     * @param id 待岗前告知主键
     * @return 待岗前告知
     */
    EmployeeWorkWaitNotifyCustom selectByPrimaryKey(long id);
}