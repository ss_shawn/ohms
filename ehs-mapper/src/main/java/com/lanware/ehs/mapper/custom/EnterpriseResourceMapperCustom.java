package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseResourceMapper;
import com.lanware.ehs.pojo.EnterpriseResource;
import com.lanware.ehs.pojo.EnterpriseResourceExample;
import com.lanware.ehs.pojo.custom.EnterpriseResourceCustom;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EnterpriseResourceMapperCustom extends EnterpriseResourceMapper {
    /**
     * 查找用户权限集
     *
     * @param username 用户名
     * @return 用户权限集合
     */
    List<EnterpriseResourceCustom> findUserPermissions(String username);

    /**
     * 查找用户菜单集合
     *
     * @param username 用户名
     * @return 用户菜单集合
     */
    List<EnterpriseResource> findUserMenus(@Param("username") String username);
}