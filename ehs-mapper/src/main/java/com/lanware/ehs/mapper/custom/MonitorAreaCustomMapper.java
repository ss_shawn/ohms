package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.MonitorAreaMapper;
import com.lanware.ehs.pojo.custom.MonitorAreaCustom;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface MonitorAreaCustomMapper extends MonitorAreaMapper {

    List<MonitorAreaCustom> queryMonitorAreaByCondition(Map<String, Object> condition);

    int enableMonitorAreaByIds(@Param("ids") List<Long> ids, @Param("status") Long status, @Param("updateTime") Date updateTime);

    int deleteMonitorAreaByIds(List<Long> ids);
}