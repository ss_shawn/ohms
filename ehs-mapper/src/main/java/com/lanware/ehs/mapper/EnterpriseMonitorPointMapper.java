package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.EnterpriseMonitorPoint;
import com.lanware.ehs.pojo.EnterpriseMonitorPointExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EnterpriseMonitorPointMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    long countByExample(EnterpriseMonitorPointExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    int deleteByExample(EnterpriseMonitorPointExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    int insert(EnterpriseMonitorPoint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    int insertSelective(EnterpriseMonitorPoint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    List<EnterpriseMonitorPoint> selectByExample(EnterpriseMonitorPointExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    EnterpriseMonitorPoint selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    int updateByExampleSelective(@Param("record") EnterpriseMonitorPoint record, @Param("example") EnterpriseMonitorPointExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    int updateByExample(@Param("record") EnterpriseMonitorPoint record, @Param("example") EnterpriseMonitorPointExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    int updateByPrimaryKeySelective(EnterpriseMonitorPoint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_monitor_point
     *
     * @mbg.generated Wed Sep 11 15:12:57 CST 2019
     */
    int updateByPrimaryKey(EnterpriseMonitorPoint record);
}