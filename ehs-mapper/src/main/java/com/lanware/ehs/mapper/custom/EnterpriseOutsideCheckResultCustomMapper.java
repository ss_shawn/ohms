package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseOutsideCheckResultMapper;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 外部检测结果扩展类
 */
public interface EnterpriseOutsideCheckResultCustomMapper extends EnterpriseOutsideCheckResultMapper {
    /**
     * @description 根据条件查询检测结果扩展
     * * @param condition 查询条件
     * @return 检测结果扩展list
     */
    List<EnterpriseOutsideCheckResultCustom> listOutsideCheckResultCustomsByOutsideCheckId(Map<String, Object> condition);

    /**
     * @description 根据id查询检测结果扩展
     * @param id 检测结果id
     * @return com.lanware.management.pojo.EnterpriseOutsideCheckResultCustom 返回EnterpriseOutsideCheckResultCustom对象
     */
    EnterpriseOutsideCheckResultCustom getCheckResultCustomById(Long id);

    /**
     * @description 根据条件更新检测结果
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateCheckResultConistenceByCondition(Map<String, Object> condition);

    /**
     * @description 获取检测结果包含的危害因素id
     * @param outsideCheckId 外部检测id
     * @return 检测结果list(只含有不重复的危害因素id)
     */
    List<EnterpriseOutsideCheckResultCustom> getHarmFactorIdsByOutsideCheckId(Long outsideCheckId);

    /**
     * @description 根据外部检测id查询检测结果扩展
     * @param outsideCheckId 外部检测id
     * @return java.util.List<com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom>
     */
    List<EnterpriseOutsideCheckResultCustom> listHarmFactorsByOutsideCheckId(Long outsideCheckId);

    List<EnterpriseOutsideCheckResultCustom> listCheckResultCustoms(Map<String, Object> condition);

    int updateCheckResultByCondition(Map<String, Object> condition);
}