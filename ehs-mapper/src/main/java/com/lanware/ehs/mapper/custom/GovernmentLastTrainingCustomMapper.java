package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.GovernmentLastTrainingMapper;
import com.lanware.ehs.pojo.GovernmentLastTraining;

import java.util.Map;

/**
 * @description 企业上次培训信息扩展mapper
 */
public interface GovernmentLastTrainingCustomMapper extends GovernmentLastTrainingMapper {
    GovernmentLastTraining getEnterpriseLastTraining(Map<String,Object> condition);
}