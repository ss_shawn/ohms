package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.GovernmentEnterpriseYear;
import com.lanware.ehs.pojo.GovernmentEnterpriseYearExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GovernmentEnterpriseYearMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    long countByExample(GovernmentEnterpriseYearExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    int deleteByExample(GovernmentEnterpriseYearExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    int insert(GovernmentEnterpriseYear record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    int insertSelective(GovernmentEnterpriseYear record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    List<GovernmentEnterpriseYear> selectByExample(GovernmentEnterpriseYearExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    GovernmentEnterpriseYear selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    int updateByExampleSelective(@Param("record") GovernmentEnterpriseYear record, @Param("example") GovernmentEnterpriseYearExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    int updateByExample(@Param("record") GovernmentEnterpriseYear record, @Param("example") GovernmentEnterpriseYearExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    int updateByPrimaryKeySelective(GovernmentEnterpriseYear record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_enterprise_year
     *
     * @mbg.generated Thu Aug 22 15:49:46 CST 2019
     */
    int updateByPrimaryKey(GovernmentEnterpriseYear record);
}