package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.PostHarmFactor;
import com.lanware.ehs.pojo.custom.PostHarmFactorCustom;

import java.util.List;
import java.util.Map;

public interface PostHarmFactorCustomMapper {

    List<PostHarmFactorCustom> queryPostHarmFactorByCondition(Map<String, Object> condition);

    List<PostHarmFactorCustom> queryUnSelectedPostHarmFactorByCondition(Map<String, Object> condition);

    int insertBatch(List<PostHarmFactor> postHarmFactorList);

    List<PostHarmFactorCustom> listTouchHarmNum(Map<String, Object> condition);
}