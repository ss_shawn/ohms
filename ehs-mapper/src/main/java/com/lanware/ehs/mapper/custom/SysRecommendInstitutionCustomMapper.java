package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.SysRecommendInstitution;

import java.util.List;
import java.util.Map;

/**
 * @description 推荐机构mapper扩展类
 * @author WangYumeng
 * @date 2019-07-04 11:10
 */
public interface SysRecommendInstitutionCustomMapper {
    /**
     * @description 查询推荐机构list
     * @param condition 查询条件
     * @return 推荐机构list
     */
    List<SysRecommendInstitution> listRecommendInstitutions(Map<String, Object> condition);
}