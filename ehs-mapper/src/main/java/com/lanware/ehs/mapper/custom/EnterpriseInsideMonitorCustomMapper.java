package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.EnterpriseInsideMonitorCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 日常监测扩展mapper
 */
public interface EnterpriseInsideMonitorCustomMapper {
    /**
     * @description 查询日常监测list
     * @param condition 查询条件
     * @return 日常监测扩展list
     */
    List<EnterpriseInsideMonitorCustom> listInsideMonitors(Map<String, Object> condition);
}
