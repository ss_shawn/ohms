package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 防护用品mapper扩展类
 * @author
 * @date 2019-07-25 10:10
 */
public interface EnterpriseProtectArticlesCustomMapper {
    /**
     * @description 查询防护用品list
     * @param condition 查询条件
     * @return 防护用品list
     */
    Page<EnterpriseProtectArticlesCustom> findProtectList(Map<String, Object> condition);
    /**
     * @description 根据岗位人员ID查询防护用品list
     * @param id 岗位人员ID
     * @return 防护用品list
     */
    List<EnterpriseProtectArticlesCustom> findProtectListByEmployeePostId(long id);
    /**
     * @description 根据岗位人员ID查询等待发放防护用品list
     * @param id 岗位人员ID
     * @return 防护用品list
     */
    List<EnterpriseProtectArticlesCustom> findWaitProtectListByEmployeePostId(long id);


    /**
     * @description 查询防护用品
     * @param id 防护用品主键
     * @return 防护用品
     */
    EnterpriseProtectArticlesCustom selectByPrimaryKey(long id);
}