package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.GovernmentProtectiveMeasuresMapper;
import com.lanware.ehs.pojo.GovernmentProtectiveMeasures;

import java.util.List;
import java.util.Map;

/**
 * @description 企业防护设施信息扩展mapper
 */
public interface GovernmentProtectiveMeasuresCustomMapper extends GovernmentProtectiveMeasuresMapper {
    List<GovernmentProtectiveMeasures> listEnterpriseProtectiveMeasures(Map<String, Object> condition);
}