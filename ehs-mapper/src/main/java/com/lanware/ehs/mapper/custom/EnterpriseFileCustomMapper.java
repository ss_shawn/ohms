package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseFileMapper;
import com.lanware.ehs.pojo.custom.EnterpriseFileCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 文件管理mapper扩展
 */
public interface EnterpriseFileCustomMapper extends EnterpriseFileMapper {
    /**
     * @description 查询文件管理list
     * @param condition 查询条件
     * @return 文件管理扩展list
     */
    List<EnterpriseFileCustom> listFiles(Map<String, Object> condition);
}
