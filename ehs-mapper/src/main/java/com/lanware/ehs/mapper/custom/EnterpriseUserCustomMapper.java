package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.mapper.EnterpriseUserMapper;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description EnterpriseUserMapper扩展接口
 * @author WangYumeng
 * @date 2019-07-02 11:27
 */
public interface EnterpriseUserCustomMapper extends EnterpriseUserMapper {
    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return 用户
     */
    EnterpriseUserCustom findByName(String username);


    /**
     * 查找用户详细信息
     *
     * @param page 分页对象
     * @param user 用户对象，用于传递查询条件
     * @return Ipage
     */
    Page<EnterpriseUserCustom> findUserDetailPage(@Param("user") EnterpriseUserCustom user);

    /**
     * 查找用户详细信息
     *
     * @param user 用户对象，用于传递查询条件
     * @return List<User>
     */
    List<EnterpriseUserCustom> findUserDetail(@Param("user") EnterpriseUserCustom user);



    /**
     * @description 查询企业账号扩展list
     * @param condition 查询条件
     * @return
     */
    List<EnterpriseUserCustom> listEnterpriseUsers(Map<String, Object> condition);

    /**
     * @description 根据id查询企业账号扩展信息
     * @author WangYumeng
     * @date 2019/7/3 16:54
     * @param id
     * @return
     */
    EnterpriseUserCustom getEnterpriseUserCustomById(Long id);

    /**
     * @description 批量重置企业账号密码
     * @author WangYumeng
     * @date 2019/7/3 08:57
     * @param enterpriseUsers 要重置密码的企业账号list
     * @return int 重置密码成功的数目
     */
    int updateEnterpriseUsers(List<EnterpriseUser> enterpriseUsers);
}
