package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultAllCustom;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;

import java.util.List;

/**
 * @description 工作场所职业病危害因素检测结果
 */
public interface EnterpriseOutsideCheckResultAllCustomMapper {
    /**
     * @description 根据人员id查询工作场所职业病危害因素检测结果
     * @param employeeId 人员ID
     * @return 工作场所职业病危害因素检测结果
     */
    List<EnterpriseOutsideCheckResultAllCustom> selectWorkplaceOutsideCheckByEmployeeId(Long employeeId);


}