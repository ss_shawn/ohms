package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 企业外部检测mapper扩展
 */
public interface EnterpriseOutsideCheckCustomMapper {
    /**
     * @description 查询危害因素检测list
     * @param condition 查询条件
     * @return 危害因素检测扩展list
     */
    List<EnterpriseOutsideCheckCustom> listOutsideChecks(Map<String, Object> condition);

    /**
     * @description 根据id查询危害因素检测扩展信息
     * @param id 危害因素检测id
     * @return com.lanware.management.pojo.EnterpriseOutsideCheck 返回EnterpriseOutsideCheck对象
     */
    EnterpriseOutsideCheckCustom getOutsideCheckCustomById(Long id);

    /**
     * @description 查询危害因素检测扩展list
     * @param condition 查询条件
     * @return List<EnterpriseOutsideCheckCustom>
     */
    List<EnterpriseOutsideCheckCustom> listOutsideCheckCustoms(Map<String, Object> condition);
}
