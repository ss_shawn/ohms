package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.PostProtectArticles;
import com.lanware.ehs.pojo.custom.PostProtectArticlesCustom;

import java.util.List;
import java.util.Map;

public interface PostProtectArticlesCustomMapper {

    List<PostProtectArticlesCustom> queryPostProtectArticlesByCondition(Map<String, Object> condition);

    List<PostProtectArticlesCustom> queryUnSelectedPostProtectArticlesByCondition(Map<String, Object> condition);

    int insertBatch(List<PostProtectArticles> postProtectArticlesList);
}