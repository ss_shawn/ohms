package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.GovernmentEnterpriseYear;

import java.util.Map;

/**
 * @description 企业年度统计信息扩展mapper
 */
public interface GovernmentEnterpriseYearCustomMapper {

    Map<String, Integer> getEnterpriseYearNum(Map<String, Object> condition);

    GovernmentEnterpriseYear getEnterpriseYearInfo(Map<String, Object> condition);
}