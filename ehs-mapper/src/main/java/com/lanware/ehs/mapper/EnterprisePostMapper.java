package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.EnterprisePost;
import com.lanware.ehs.pojo.EnterprisePostExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EnterprisePostMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    long countByExample(EnterprisePostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    int deleteByExample(EnterprisePostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    int insert(EnterprisePost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    int insertSelective(EnterprisePost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    List<EnterprisePost> selectByExample(EnterprisePostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    EnterprisePost selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    int updateByExampleSelective(@Param("record") EnterprisePost record, @Param("example") EnterprisePostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    int updateByExample(@Param("record") EnterprisePost record, @Param("example") EnterprisePostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    int updateByPrimaryKeySelective(EnterprisePost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_post
     *
     * @mbg.generated Thu Aug 29 13:15:47 CST 2019
     */
    int updateByPrimaryKey(EnterprisePost record);
}