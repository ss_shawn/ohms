package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseServiceRecordMapper;
import com.lanware.ehs.pojo.custom.EnterpriseServiceRecordCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 维修记录mapper扩展
 */
public interface EnterpriseServiceRecordCustomMapper extends EnterpriseServiceRecordMapper {
    /**
     * @description 查询维修记录list
     * @param condition 查询条件
     * @return 维修记录扩展list
     */
    List<EnterpriseServiceRecordCustom> listServiceRecords(Map<String, Object> condition);

    List<EnterpriseServiceRecordCustom> listServiceRecordAccounts(Map<String, Object> condition);
}