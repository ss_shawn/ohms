package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.mapper.EnterpriseUserRoleMapper;
import com.lanware.ehs.pojo.custom.EnterpriseUserRoleCustom;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EnterpriseUserRoleCustomMapper  extends EnterpriseUserRoleMapper {
    /**
     * 通过用户名查找用户角色
     *
     * @param username 用户名
     * @return 用户角色集合
     */
    List<EnterpriseUserRoleCustom> findUserRole(String username);

    /**
     * 查找角色详情
     *
     * @param role 角色
     * @return IPage<User>
     */
    Page<EnterpriseUserRoleCustom> findRolePage(@Param("role") EnterpriseUserRoleCustom role);

    /**
     * @description 根据id查询角色扩展pojo
     * @param id 角色id
     * @return EnterpriseUserRoleCustom 角色扩展pojo
     */
    EnterpriseUserRoleCustom getRoleCustomById(Long id);
}
