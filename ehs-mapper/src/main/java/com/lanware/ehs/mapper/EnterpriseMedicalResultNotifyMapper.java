package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.EnterpriseMedicalResultNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultNotifyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EnterpriseMedicalResultNotifyMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    long countByExample(EnterpriseMedicalResultNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    int deleteByExample(EnterpriseMedicalResultNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    int insert(EnterpriseMedicalResultNotify record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    int insertSelective(EnterpriseMedicalResultNotify record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    List<EnterpriseMedicalResultNotify> selectByExample(EnterpriseMedicalResultNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    EnterpriseMedicalResultNotify selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    int updateByExampleSelective(@Param("record") EnterpriseMedicalResultNotify record, @Param("example") EnterpriseMedicalResultNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    int updateByExample(@Param("record") EnterpriseMedicalResultNotify record, @Param("example") EnterpriseMedicalResultNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    int updateByPrimaryKeySelective(EnterpriseMedicalResultNotify record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_medical_result_notify
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    int updateByPrimaryKey(EnterpriseMedicalResultNotify record);
}