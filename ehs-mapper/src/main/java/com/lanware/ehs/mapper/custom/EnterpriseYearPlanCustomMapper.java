package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.custom.EnterpriseYearPlanCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 年度计划扩展mapper
 */
public interface EnterpriseYearPlanCustomMapper {
    /**
     * @description 根据企业id查询年度计划list
     * @param condition 查询条件
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseYearPlanCustom> 年度计划扩展list
     */
    List<EnterpriseYearPlanCustom> listYearplans(Map<String, Object> condition);

    /**
     * @description 根据条件查询年度计划
     * @param condition 查询条件
     * @return EnterpriseYearPlan 年度计划
     */
    EnterpriseYearPlan findYearplanByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件查询年度计划
     * @param condition 查询条件
     * @return EnterpriseYearPlan 年度计划
     */
    List<EnterpriseYearPlan> findYearplansByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件查询年度计划扩展
     * @param condition 查询条件
     * @return EnterpriseYearPlanCustom 年度计划扩展pojo
     */
    EnterpriseYearPlanCustom getYearplanByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件查询年度检测计划扩展
     * @param condition 查询条件
     * @return EnterpriseYearPlanCustom 年度计划扩展pojo
     */
    List<EnterpriseYearPlanCustom> listYearCheckPlans(Map<String, Object> condition);

    /**
     * @description 根据id查询年度检测计划
     * @param id 年度检测计划id
     * @return com.lanware.management.pojo.EnterpriseYearPlanCustom 返回EnterpriseYearPlanCustom对象
     */
    EnterpriseYearPlanCustom getYearCheckPlanById(Long id);

    EnterpriseYearPlan getTrainingPlan(Map<String, Object> condition);

    int getYearPlanCompletionNum(Map<String, Object> condition);
}
