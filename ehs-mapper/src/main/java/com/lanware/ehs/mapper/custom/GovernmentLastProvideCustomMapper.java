package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.GovernmentLastProvideMapper;
import com.lanware.ehs.pojo.GovernmentLastProvide;

import java.util.Map;

/**
 * @description 企业上次防护用品发放信息扩展mapper
 */
public interface GovernmentLastProvideCustomMapper extends GovernmentLastProvideMapper {
    GovernmentLastProvide getEnterpriseLastProvide(Map<String, Object> condition);
}