package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseTrainingMapper;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 培训管理mapper扩展
 */
public interface EnterpriseTrainingCustomMapper extends EnterpriseTrainingMapper {
    /**
     * @description 查询培训管理list
     * @param condition 查询条件
     * @return 培训管理扩展list
     */
    List<EnterpriseTrainingCustom> listTrainings(Map<String, Object> condition);

    /**
     * @description 根据id查询培训扩展信息
     * @param id 培训id
     * @return com.lanware.management.pojo.EnterpriseTrainingCustom 返回EnterpriseTrainingCustom对象
     */
    EnterpriseTrainingCustom getTrainingCustomById(Long id);


    /**
     *
     * 统计培训总时长
     * @param parameter
     * @return
     */
    String getTrainTime(Map<String, Object> parameter);

    /**
     * 获取年度每月培训人数
     * @param condition
     * @return
     */
    Map<String, Integer> getTrainNumByCondition(Map<String, Object> condition);
}