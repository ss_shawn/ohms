package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.EnterpriseDeclareWorkplace;

import java.util.List;
import java.util.Map;

/**
 * @description 申报涉及工作场所和危害因素扩展mapper
 */
public interface EnterpriseDeclareWorkplaceCustomMapper {
    /**
     * @description 根据条件查询申报涉及危害场所和危害因素集合
     * @param condition 查询条件
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseDeclareWorkplace>
     */
    List<EnterpriseDeclareWorkplace> listDeclareWorkplaces(Map<String, Object> condition);

    /**
     * @description 根据条件查询场所和危害因素集合
     * @param condition 查询条件
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseDeclareWorkplace>
     */
    List<EnterpriseDeclareWorkplace> listSelectDeclareWorkplaces(Map<String, Object> condition);

    /**
     * @description 根据企业id查询未申报的场所和危害因素集合
     * @param enterpriseId 企业id
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseDeclareWorkplace>
     */
    List<EnterpriseDeclareWorkplace> listUnselectedDeclareWorkplaces(Long enterpriseId);

    /**
     * @description 批量插入场所危害集合
     * @param enterpriseDeclareWorkplaces 场所危害集合
     * @return int 插入成功的数目
     */
    int insertBatch(List<EnterpriseDeclareWorkplace> enterpriseDeclareWorkplaces);
}