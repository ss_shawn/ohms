package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseBaseinfoMapper;
import com.lanware.ehs.pojo.custom.EnterpriseBaseinfoCustom;

import java.util.List;
import java.util.Map;

public interface EnterpriseBaseinfoCustomMapper extends EnterpriseBaseinfoMapper {

    List<EnterpriseBaseinfoCustom> queryEnterpriseBaseinfoByCondition(Map<String, Object> condition);
}
