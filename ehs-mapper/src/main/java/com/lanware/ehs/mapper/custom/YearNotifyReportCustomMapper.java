package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.custom.BaseHarmFactorCustom;
import com.lanware.ehs.pojo.custom.YearNotifyReportCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 危害因素mapper扩展类
 * @author WangYumeng
 * @date 2019-07-04 10:10
 */
public interface YearNotifyReportCustomMapper {
    /**
     * @description 查询检测结果告知年度统计
     * @param condition 查询条件
     * @return 检测结果告知年度统计
     */
    List<YearNotifyReportCustom> getWorkplaceWaitNotify(Map<String, Object> condition);

    /**
     * @description 查询职业病诊断结果告知年度统计
     * @param condition 查询条件
     * @return 职业病诊断结果告知年度统计
     */
    List<YearNotifyReportCustom> getDiseaseDiagnosiWaitNotify(Map<String, Object> condition);
    /**
     * @description 查询岗前告知年度统计
     * @param condition 查询条件
     * @return 岗前告知年度统计
     */
    List<YearNotifyReportCustom> getWorkWaitNotify(Map<String, Object> condition);
    /**
     * @description 查询体检结果告知年度统计
     * @param condition 查询条件
     * @return 体检结果告知年度统计
     */
    List<YearNotifyReportCustom> getMedicalResultNotify(Map<String, Object> condition);


}