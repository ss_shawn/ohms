package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.GovernmentHarmReport;
import com.lanware.ehs.pojo.GovernmentHarmReportExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GovernmentHarmReportMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_harm_report
     *
     * @mbg.generated Thu Aug 22 13:57:06 CST 2019
     */
    long countByExample(GovernmentHarmReportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_harm_report
     *
     * @mbg.generated Thu Aug 22 13:57:06 CST 2019
     */
    int deleteByExample(GovernmentHarmReportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_harm_report
     *
     * @mbg.generated Thu Aug 22 13:57:06 CST 2019
     */
    int insert(GovernmentHarmReport record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_harm_report
     *
     * @mbg.generated Thu Aug 22 13:57:06 CST 2019
     */
    int insertSelective(GovernmentHarmReport record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_harm_report
     *
     * @mbg.generated Thu Aug 22 13:57:06 CST 2019
     */
    List<GovernmentHarmReport> selectByExample(GovernmentHarmReportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_harm_report
     *
     * @mbg.generated Thu Aug 22 13:57:06 CST 2019
     */
    int updateByExampleSelective(@Param("record") GovernmentHarmReport record, @Param("example") GovernmentHarmReportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_harm_report
     *
     * @mbg.generated Thu Aug 22 13:57:06 CST 2019
     */
    int updateByExample(@Param("record") GovernmentHarmReport record, @Param("example") GovernmentHarmReportExample example);
}