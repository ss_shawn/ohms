package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.mapper.EmployeeDiseaseDiagnosiMapper;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.custom.EmployeeDiseaseDiagnosiCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 职业病诊断扩展mapper
 */
public interface EmployeeDiseaseDiagnosiCustomMapper extends EmployeeDiseaseDiagnosiMapper {
    /**
     * @description 根据体检id查询职业病诊断
     * @param medicalId 体检id
     * @return 职业病诊断list
     */
    List<EmployeeDiseaseDiagnosi> findDiseaseDiagnosiByMedicalId(Long medicalId);
    Page<EmployeeDiseaseDiagnosiCustom> queryEnterpriseDiseaseDiagnosiByCondition(Map<String, Object> condition);
}
