package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseAccidentHandleMapper;
import com.lanware.ehs.pojo.custom.EnterpriseAccidentHandleCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 事故调查处理mapper扩展
 */
public interface EnterpriseAccidentHandleCustomMapper extends EnterpriseAccidentHandleMapper {
    /**
     * @description 查询事故调查处理list
     * @param condition 查询条件
     * @return 事故调查处理list
     */
    List<EnterpriseAccidentHandleCustom> listAccidentHandles(Map<String, Object> condition);
}
