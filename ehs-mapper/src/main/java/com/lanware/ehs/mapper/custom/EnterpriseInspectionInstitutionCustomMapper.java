package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseInspectionInstitutionMapper;
import com.lanware.ehs.pojo.custom.EnterpriseInspectionInstitutionCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 检查机构扩展mapper
 */
public interface EnterpriseInspectionInstitutionCustomMapper extends EnterpriseInspectionInstitutionMapper {
    /**
     * @description 查询检查机构list
     * @param condition 查询条件
     * @return 检查机构list
     */
    List<EnterpriseInspectionInstitutionCustom> listInspectionInstitutions(Map<String, Object> condition);
}