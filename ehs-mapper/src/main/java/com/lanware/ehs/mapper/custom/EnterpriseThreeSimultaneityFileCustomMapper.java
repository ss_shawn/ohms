package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseThreeSimultaneityFileMapper;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;

import java.util.List;
import java.util.Map;

/**
 * @description 三同时文件mapper扩展
 */
public interface EnterpriseThreeSimultaneityFileCustomMapper extends EnterpriseThreeSimultaneityFileMapper {
    /**
     * @description 查询三同时文件list
     * @param condition 查询条件
     * @return 三同时文件list
     */
    List<EnterpriseThreeSimultaneityFile> listThreeSimultaneityFiles(Map<String, Object> condition);
}