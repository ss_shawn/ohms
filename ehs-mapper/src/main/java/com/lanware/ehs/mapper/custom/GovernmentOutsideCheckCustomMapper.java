package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.GovernmentOutsideCheckMapper;

import java.util.Map;

/**
 * @description 监管企业检测扩展mapper
 */
public interface GovernmentOutsideCheckCustomMapper extends GovernmentOutsideCheckMapper {
    Map<String, Integer> getEnterpriseCheckTimeDistribution(Map<String, Object> condition);
}