package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.EnterpriseHygieneOrgCustom;
import com.lanware.ehs.pojo.custom.EnterpriseRelationFileCustom;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EnterpriseHygieneOrgCustomMapper {

    List<EnterpriseHygieneOrgCustom> queryEnterpriseHygieneOrgByCondition(Map<String, Object> condition);

    List<EnterpriseRelationFileCustom> queryEnterpriseRelationFileByCondition(Map<String, Object> condition);

    int deleteEnterpriseHygieneOrgByIds(@Param("ids") List<Long> ids, @Param("module") String module);

    List<Map<String, String>> getAllFilesByIds(@Param("ids") List<Long> ids, @Param("module") String module);
}