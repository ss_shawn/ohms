package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseInspectionMapper;
import com.lanware.ehs.pojo.EnterpriseInspection;
import com.lanware.ehs.pojo.custom.EnterpriseInspectionCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 日常检查整改登记mapper扩展
 */
public interface EnterpriseInspectionCustomMapper extends EnterpriseInspectionMapper {
    /**
     * @description 查询日常检查整改list
     * @param condition 查询条件
     * @return 日常检查整改list
     */
    List<EnterpriseInspectionCustom> listInspections(Map<String, Object> condition);

    /**
     * @description 更新日常检查整改
     * @param enterpriseInspection 日常检查整改pojo
     * @return 更新成功的数目
     */
    int updateInspection(EnterpriseInspection enterpriseInspection);
}