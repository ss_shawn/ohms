package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.EmployeeMedicalHistory;
import com.lanware.ehs.pojo.custom.EmployeeMedicalHistoryCustom;

import java.util.List;
import java.util.Map;

public interface EmployeeMedicalHistoryCustomMapper {

    List<EmployeeMedicalHistoryCustom> queryEmployeeMedicalHistoryByCondition(Map<String, Object> condition);

    int insertBatch(List<EmployeeMedicalHistory> employeeMedicalHistories);

    int updateBatch(List<EmployeeMedicalHistory> employeeMedicalHistories);
}