package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.custom.BaseHarmFactorCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 监管端查询
 * @author
 * @date 2019-08-21 10:10
 */
public interface GovernmentCustomMapper {


    /**
     * @description 查询培训人数和培训时长
     * @param condition 查询条件
     * @return 培训人数和培训时长
     */
    Map getAllTrainTime(Map<String, Object> condition);
    /**
     * @description 查询危害因素统计信息和检查信息
     * @param condition 查询条件
     * @return 危害因素统计信息和检查信息
     */
    List<Map<String,Object>> getHarmReport(Map<String, Object> condition);

    /**
     * @description 查询健康档案人数
     * @param condition 查询条件
     * @return 健康档案人数
     */
    Map getArchiveNumber(Map<String, Object> condition);
    /**
     * @description 查询岗前体检总人数和已体检人数
     * @param condition 查询条件
     * @return 岗前体检总人数和已体检人数
     */
    Map getHeadMedical(Map<String, Object> condition);
    /**
     * @description 查询年度岗中体检
     * @param condition 查询条件
     * @return 岗前体检总人数和已体检人数
     */
    Map getMedical(Map<String, Object> condition);
    /**
     * @description 查询防护用品发放年度统计
     * @param condition 查询条件
     * @return 防护用品发放年度统计
     */
    List<Map> getProvideProtectArt(Map<String, Object> condition);
    /**
     * @description 查询防护用品发放
     * @param condition 查询条件
     * @return 防护用品发放
     */
    Map getLastProvide(Map<String, Object> condition);

    /**
     * @description 查询年度日常检查数量
     * @param condition 查询条件
     * @return 日常检查数量
     */
    Map getInspection(Map<String, Object> condition);
    /**
     * @description 查询职业病人数和职业禁忌症人数
     * @param condition 查询条件
     * @return 职业病人数和职业禁忌症人数
     */
    Map getTabooNumber(Map<String, Object> condition);
    /**
     * @description 查询年度最新体检信息
     * @param condition 查询条件
     * @return 最新体检信息
     */
    Map getLastMedical(Map<String, Object> condition);
    /**
     * @description 查询年度最新培训信息
     * @param condition 查询条件
     * @return 最新培训信息
     */
    Map getLastTraining(Map<String, Object> condition);

    /**
     * @description 查询防护设施年度统计
     * @param condition 查询条件
     * @return 防护设施年度统计
     */
    List<Map> getProtectiveMeasures(Map<String, Object> condition);
    /**
     * @description 查询年度最新日常检查
     * @param condition 查询条件
     * @return 最新日常检查
     */
    Map getLastServiceInspection(Map<String, Object> condition);
    /**
     * @description 查询年度最新整改
     * @param condition 查询条件
     * @return 最新整改
     */
    Map getLastServiceRectified(Map<String, Object> condition);
    /**
     * @description 查询外部检测年度统计
     * @param condition 查询条件
     * @return 外部检测年度统计
     */
    List<Map> getOutsideCheck(Map<String, Object> condition);

    /**
     * @description 查询年度最新外部检查
     * @param condition 查询条件
     * @return 最新外部检查
     */
    Map getLastHarmCheck(Map<String, Object> condition);
}