package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.SysConsultCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 系统咨询mapper扩展类
 */
public interface SysConsultCustomMapper {
    /**
     * @description 查询系统咨询list
     * @param condition 查询条件
     * @return 系统咨询list
     */
    List<SysConsultCustom> listConsults(Map<String, Object> condition);

    /**
     * @description 将状态为新回复的咨询更新为已回复
     * @param enterpriseId 企业id
     * @return 更新成功的数目
     */
    int updateAnswerStatusByEnterpriseId(Long enterpriseId);
}