package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.GovernmentProtectiveMeasures;
import com.lanware.ehs.pojo.GovernmentProtectiveMeasuresExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GovernmentProtectiveMeasuresMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    long countByExample(GovernmentProtectiveMeasuresExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    int deleteByExample(GovernmentProtectiveMeasuresExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    int insert(GovernmentProtectiveMeasures record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    int insertSelective(GovernmentProtectiveMeasures record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    List<GovernmentProtectiveMeasures> selectByExample(GovernmentProtectiveMeasuresExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    GovernmentProtectiveMeasures selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    int updateByExampleSelective(@Param("record") GovernmentProtectiveMeasures record, @Param("example") GovernmentProtectiveMeasuresExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    int updateByExample(@Param("record") GovernmentProtectiveMeasures record, @Param("example") GovernmentProtectiveMeasuresExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    int updateByPrimaryKeySelective(GovernmentProtectiveMeasures record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table government_protective_measures
     *
     * @mbg.generated Wed Aug 21 09:06:03 CST 2019
     */
    int updateByPrimaryKey(GovernmentProtectiveMeasures record);
}