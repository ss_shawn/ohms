package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseMonitorPointMapper;
import com.lanware.ehs.pojo.custom.EnterpriseMonitorPointCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 监测点管理扩展mapper
 */
public interface EnterpriseMonitorPointCustomMapper extends EnterpriseMonitorPointMapper {

    List<EnterpriseMonitorPointCustom> listMonitorPoints(Map<String, Object> condition);
}