package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseThreeSimultaneityMapper;
import com.lanware.ehs.pojo.custom.EnterpriseThreeSimultaneityCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 三同时mapper扩展
 */
public interface EnterpriseThreeSimultaneityCustomMapper extends EnterpriseThreeSimultaneityMapper {
    /**
     * @description 查询三同时扩展list
     * @param condition 查询条件
     * @return 三同时扩展list
     */
    List<EnterpriseThreeSimultaneityCustom> listThreeSimultaneitys(Map<String, Object> condition);

    /**
     * @description 根据条件更新危害程度和三同时阶段
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateHarmDegreeAndStateByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件更新三同时阶段
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateProjectStateByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件更新项目状态
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateStatusByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件更新项目更新时间
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateGmtModifiedByCondition(Map<String, Object> condition);
}