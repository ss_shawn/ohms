package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseEquipmentUseMapper;
import com.lanware.ehs.pojo.custom.EnterpriseEquipmentUseCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 运行使用mapper扩展
 */
public interface EnterpriseEquipmentUseCustomMapper extends EnterpriseEquipmentUseMapper {
    /**
     * @description 查询运行使用list
     * @param condition 查询条件
     * @return 运行使用扩展list
     */
    List<EnterpriseEquipmentUseCustom> listEquipmentUses(Map<String, Object> condition);

    List<EnterpriseEquipmentUseCustom> listEquipmentUseAccounts(Map<String, Object> condition);
}