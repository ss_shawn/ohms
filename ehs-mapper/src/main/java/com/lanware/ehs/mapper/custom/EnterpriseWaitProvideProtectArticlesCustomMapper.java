package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.custom.EnterpriseProvideProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.EnterpriseWaitProvideProtectArticlesCustom;

import java.util.Map;

/**
 * @description 防护用品mapper扩展类
 * @author
 * @date 2019-07-25 10:10
 */
public interface EnterpriseWaitProvideProtectArticlesCustomMapper {
    /**
     * @description 查询待发放防护用品发放list
     * @param condition 查询条件
     * @return 待发放防护用品list
     */
    Page<EnterpriseWaitProvideProtectArticlesCustom> findEnterpriseWaitProvideProtectArticles(Map<String, Object> condition);
    /**
     * @description 查询待发放防护用品发放
     * @param id 待发放防护用品发放主键
     * @return 待发放防护用品
     */
    EnterpriseWaitProvideProtectArticlesCustom selectEnterpriseWaitProvideProtectArticlesByPrimaryKey(long id);
}