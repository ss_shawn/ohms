package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.MonitorUserMapper;
import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.custom.MonitorUserCustom;

import java.util.List;
import java.util.Map;

/**
 * @description MonitorUserMapper扩展接口
 * @author WangYumeng
 * @date 2019-07-02 13:27
 */
public interface MonitorUserCustomMapper extends MonitorUserMapper {
    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return 用户
     */
    MonitorUser findByName(String username);

    /**
     * @description 查询监管账号扩展list
     * @param condition 查询条件
     * @return
     */
    List<MonitorUserCustom> listMonitorUsers(Map<String, Object> condition);

    /**
     * @description  根据id查询监管账号扩展信息
     * @author WangYumeng
     * @date 2019/7/3 16:53
     * @param id
     * @return
     */
    MonitorUserCustom getMonitorUserCustomById(Long id);

    /**
     * @description 批量重置监管账号密码
     * @author WangYumeng
     * @date 2019/7/3 08:47
     * @param monitorUsers 要重置密码的监管账号list
     * @return int 重置密码成功的数目
     */
    int updateMonitorUsers(List<MonitorUser> monitorUsers);
}
