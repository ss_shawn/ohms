package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.EnterpriseDiseaseDiagnosiNotify;
import com.lanware.ehs.pojo.EnterpriseDiseaseDiagnosiNotifyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EnterpriseDiseaseDiagnosiNotifyMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    long countByExample(EnterpriseDiseaseDiagnosiNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    int deleteByExample(EnterpriseDiseaseDiagnosiNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    int insert(EnterpriseDiseaseDiagnosiNotify record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    int insertSelective(EnterpriseDiseaseDiagnosiNotify record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    List<EnterpriseDiseaseDiagnosiNotify> selectByExample(EnterpriseDiseaseDiagnosiNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    EnterpriseDiseaseDiagnosiNotify selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    int updateByExampleSelective(@Param("record") EnterpriseDiseaseDiagnosiNotify record, @Param("example") EnterpriseDiseaseDiagnosiNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    int updateByExample(@Param("record") EnterpriseDiseaseDiagnosiNotify record, @Param("example") EnterpriseDiseaseDiagnosiNotifyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    int updateByPrimaryKeySelective(EnterpriseDiseaseDiagnosiNotify record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_disease_diagnosi_notify
     *
     * @mbg.generated Thu Aug 08 09:38:01 CST 2019
     */
    int updateByPrimaryKey(EnterpriseDiseaseDiagnosiNotify record);
}