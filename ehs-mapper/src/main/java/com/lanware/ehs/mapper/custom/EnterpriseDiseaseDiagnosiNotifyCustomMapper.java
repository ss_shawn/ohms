package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.custom.EnterpriseDiseaseDiagnosiNotifyCustom;

import java.util.Map;

/**
 * @description 作业场所告知mapper扩展类
 * @author
 * @date 2019-07-25 10:10
 */
public interface EnterpriseDiseaseDiagnosiNotifyCustomMapper {
    /**
     * @description 查询待作业场所告知list
     * @param condition 查询条件
     * @return 待作业场所告知list
     */
    Page<EnterpriseDiseaseDiagnosiNotifyCustom> findEnterpriseDiseaseDiagnosiNotifyList(Map<String, Object> condition);



    /**
     * @description 查询待作业场所告知
     * @param id 待作业场所告知主键
     * @return 待作业场所告知
     */
    EnterpriseDiseaseDiagnosiNotifyCustom selectByPrimaryKey(long id);


}