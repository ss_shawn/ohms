package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.EnterpriseOutsideCheck;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EnterpriseOutsideCheckMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    long countByExample(EnterpriseOutsideCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    int deleteByExample(EnterpriseOutsideCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    int insert(EnterpriseOutsideCheck record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    int insertSelective(EnterpriseOutsideCheck record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    List<EnterpriseOutsideCheck> selectByExample(EnterpriseOutsideCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    EnterpriseOutsideCheck selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    int updateByExampleSelective(@Param("record") EnterpriseOutsideCheck record, @Param("example") EnterpriseOutsideCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    int updateByExample(@Param("record") EnterpriseOutsideCheck record, @Param("example") EnterpriseOutsideCheckExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    int updateByPrimaryKeySelective(EnterpriseOutsideCheck record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_outside_check
     *
     * @mbg.generated Mon Sep 16 14:42:54 CST 2019
     */
    int updateByPrimaryKey(EnterpriseOutsideCheck record);
}