package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseCheckResultDetailMapper;
import com.lanware.ehs.pojo.EnterpriseCheckResultDetail;

import java.util.List;
import java.util.Map;

/**
 * @description 检测结果详细扩展mapper
 */
public interface EnterpriseCheckResultDetailCustomMapper extends EnterpriseCheckResultDetailMapper {

    List<EnterpriseCheckResultDetail> listCheckResultDetails(Map<String, Object> condition);

    int insertBatch(List<EnterpriseCheckResultDetail> checkResultDetailList);
}