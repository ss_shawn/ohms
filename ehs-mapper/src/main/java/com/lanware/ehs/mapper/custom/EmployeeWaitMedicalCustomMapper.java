package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EmployeeWaitMedicalMapper;
import com.lanware.ehs.pojo.EmployeeWaitMedical;
import com.lanware.ehs.pojo.custom.EmployeeWaitMedicalCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 待体检员工扩展mapper
 */
public interface EmployeeWaitMedicalCustomMapper extends EmployeeWaitMedicalMapper {
    /**
     * @description 查询待体检员工list
     * @param condition 查询条件
     * @return 待体检员工list
     */
    List<EmployeeWaitMedicalCustom> listWaitMedicals(Map<String, Object> condition);

    /**
     * @description 更新待体检状态
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateWaitMedicalByCondition(Map<String ,Object> condition);

    /**
     * @description 根据体检记录id恢复待体检记录(岗前和离岗和岗中)
     * @param condition 条件(体检记录id)
     * @return 更新成功的数目
     */
    int updateWaitMedicalByMedicalId(Map<String ,Object> condition);

    /**
     * @description 批量恢复待体检记录(岗前和离岗)
     * @return 更新成功的数目
     */
    int updateBatch( List<EmployeeWaitMedical> employeeWaitMedicalCustomList);

    /**
     * @description 更新待体检记录状态，并保存未体检原因
     * @param condition 条件(体检记录id)
     * @return 更新成功的数目
     */
    int updateWaitMedicalById(Map<String ,Object> condition);

    /**
     * @description 获取待体检年度报表
     * @param condition 查询条件
     * @return
     */
    List<EmployeeWaitMedicalCustom> listYearWaitMedicals(Map<String, Object> condition);

}
