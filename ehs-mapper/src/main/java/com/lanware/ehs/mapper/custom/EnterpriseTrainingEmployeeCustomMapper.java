package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseTrainingEmployeeMapper;
import com.lanware.ehs.pojo.EnterpriseTrainingEmployee;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingEmployeeCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 企业培训人员关系mapper扩展
 */
public interface EnterpriseTrainingEmployeeCustomMapper extends EnterpriseTrainingEmployeeMapper {
    /**
     * @description 批量插入参加培训的员工集合
     * @param enterpriseTrainingEmployeeList 参加培训的员工集合
     * @return int 插入成功的数目
     */
    int insertBatch(List<EnterpriseTrainingEmployee> enterpriseTrainingEmployeeList);

    /**
     * @description 查询管理员培训信息
     * @param condition 查询条件
     * @return 人员信息扩展list
     */
    List<EnterpriseTrainingEmployeeCustom> listTraingAdmins(Map<String, Object> condition);

    /**
     * @description 查询普通员工培训扩展信息
     * @param condition 查询条件
     * @return 人员信息扩展list
     */
    List<EnterpriseTrainingEmployeeCustom> listTraingEmployees(Map<String, Object> condition);

    /**
     * @description 根据培训id查询管理员培训信息
     * @param condition 查询条件
     * @return 人员信息扩展list
     */
    List<EnterpriseTrainingEmployeeCustom> listTrainingAdminsByTrainingId(Map<String, Object> condition);

    /**
     * @description 根据培训id查询普通员工培训信息
     * @param condition 查询条件
     * @return 人员信息扩展list
     */
    List<EnterpriseTrainingEmployeeCustom> listTraingEmployeesByTrainingId(Map<String, Object> condition);

    /**
     * @description 根据培训id过滤尚未培训的管理员
     * @param condition 查询条件
     * @return 人员信息扩展list
     */
    List<EnterpriseTrainingEmployeeCustom> listSelectTrainingAdminsByTrainingId(Map<String, Object> condition);

    /**
     * @description 根据培训id过滤尚未培训的普通员工
     * @param condition 查询条件
     * @return 人员信息扩展list
     */
    List<EnterpriseTrainingEmployeeCustom> listSelectTrainingEmployeesByTrainingId(Map<String, Object> condition);

    /**
     * @description 查询管理员培训信息(培训总时长和人数)
     * @return 培训数据map
     */
    Map<String, Object> getAdminTrainingData(Map<String, Object> condition);

    /**
     * @description 查询普通员工培训信息(在岗的普通员工培训总时长和人数)
     * @return 培训数据map
     */
    Map<String, Object> getEmployeeTrainingData(Map<String, Object> condition);

    /**
     * @description 查询管理员培训信息(培训总时长，培训人数，总人数)
     * @return 培训数据扩展list
     */
    List<EnterpriseTrainingEmployeeCustom> getYearAdminTrainingData(Map<String, Object> condition);

    /**
     * @description 查询管理员培训信息(培训总时长，培训人数，总人数)
     * @return 培训数据扩展list
     */
    List<EnterpriseTrainingEmployeeCustom> getYearEmployeeTrainingData(Map<String, Object> condition);
}