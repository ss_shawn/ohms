package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.GovernmentLastMedical;

import java.util.Map;

/**
 * @description 企业上次体检员工信息扩展mapper
 */
public interface GovernmentLastMedicalCustomMapper {
    GovernmentLastMedical getEnterpriseLastMedical(Map<String, Object> condition);
}