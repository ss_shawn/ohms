package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.WorkplaceHarmFactorCustom;

import java.util.List;
import java.util.Map;

public interface WorkplaceHarmFactorCustomMapper {
    /**
     * @description 查询作业场所危害因素list
     * @param workplaceId 作业场所id
     * @return 作业场所危害因素扩展list
     */
    List<WorkplaceHarmFactorCustom> listHarmFactorsByWorkplaceId(Long workplaceId);

    /**
     * @description 查询作业场所危害因素扩展list
     * @param workplaceId 作业场所id
     * @return 作业场所危害因素扩展list
     */
    List<WorkplaceHarmFactorCustom> listHarmFactorCustomsByWorkplaceId(Long workplaceId);

    /**
     * @description 根据id查询危害因素扩展信息
     * @param id 危害因素id
     * @return com.lanware.management.pojo.WorkplaceHarmFactor 返回WorkplaceHarmFactor对象
     */
    WorkplaceHarmFactorCustom getHarmfactorCustomById(Long id);

    /**
     * @description 查询作业场所危害因素扩展list
     * @param condition 查询条件
     * @return 危害因素扩展list
     */
    List<WorkplaceHarmFactorCustom> listWorkplaceHarmFactors(Map<String, Object> condition);

    /**
     * @description 获取危害因素result
     * @param condition 查询条件
     * @return 作业场所危害因素扩展list
     */
    List<WorkplaceHarmFactorCustom> listHarmFactorsByCondition(Map<String, Object> condition);

    /**
     * @description 获取该监测点中作业场所的危害因素
     * @param condition 查询条件
     * @return 作业场所危害因素扩展list
     */
    List<WorkplaceHarmFactorCustom> getSelectMonitorHarmFactors(Map<String, Object> condition);
}
