package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.GovernmentProvideProtectArtMapper;
import com.lanware.ehs.pojo.GovernmentProvideProtectArt;

import java.util.List;
import java.util.Map;

/**
 * @description 企业防护用品发放扩展mapper
 */
public interface GovernmentProvideProtectArtCustomMapper extends GovernmentProvideProtectArtMapper {
    List<GovernmentProvideProtectArt> listEnterpriseProvideProtectArts(Map<String, Object> condition);
}