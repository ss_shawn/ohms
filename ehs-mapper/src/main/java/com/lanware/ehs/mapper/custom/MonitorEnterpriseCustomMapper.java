package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.MonitorEnterpriseMapper;
import com.lanware.ehs.pojo.MonitorEnterprise;
import com.lanware.ehs.pojo.custom.MonitorEnterpriseCustom;

import java.util.List;
import java.util.Map;

public interface MonitorEnterpriseCustomMapper extends MonitorEnterpriseMapper {

    List<MonitorEnterpriseCustom> queryMonitorEnterpriseByCondition(Map<String, Object> condition);

    List<MonitorEnterpriseCustom> queryUnselectedMonitorEnterpriseByCondition(Map<String, Object> condition);

    int insertBatch(List<MonitorEnterprise> monitorEnterprises);

    Integer getMonitorEnterpriseNum(Long areaId);
}