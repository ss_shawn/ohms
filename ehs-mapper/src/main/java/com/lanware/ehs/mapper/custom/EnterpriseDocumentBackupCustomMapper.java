package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseDocumentBackupMapper;
import com.lanware.ehs.pojo.EnterpriseDocumentBackup;

import java.util.List;
import java.util.Map;

public interface EnterpriseDocumentBackupCustomMapper extends EnterpriseDocumentBackupMapper {

    List<EnterpriseDocumentBackup> queryDocumentBackupByCondition(Map<String, Object> condition);
}
