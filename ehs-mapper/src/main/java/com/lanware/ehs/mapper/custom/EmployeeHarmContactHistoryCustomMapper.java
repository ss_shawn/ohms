package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.EmployeeHarmContactHistory;
import com.lanware.ehs.pojo.custom.EmployeeHarmContactHistoryCustom;

import java.util.List;
import java.util.Map;

public interface EmployeeHarmContactHistoryCustomMapper {

    List<EmployeeHarmContactHistoryCustom> queryEmployeeHarmContactHistoryByCondition(Map<String, Object> condition);

    int insertBatch(List<EmployeeHarmContactHistory> employeeHarmContactHistories);

    int updateBatch(List<EmployeeHarmContactHistory> employeeHarmContactHistories);

    EmployeeHarmContactHistoryCustom queryEmployeeHarmContactHistoryById(Long id);
}