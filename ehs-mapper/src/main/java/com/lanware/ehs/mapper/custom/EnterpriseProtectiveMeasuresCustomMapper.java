package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseProtectiveMeasuresMapper;
import com.lanware.ehs.pojo.custom.EnterpriseProtectiveMeasuresCustom;

import java.util.List;
import java.util.Map;

public interface EnterpriseProtectiveMeasuresCustomMapper extends EnterpriseProtectiveMeasuresMapper {

    List<EnterpriseProtectiveMeasuresCustom> queryProtectEnterpriseProtectiveMeasuresByPostId(Long postId);

    /**
     * @description 查询防护措施list
     * @param condition 查询条件
     * @return 防护措施扩展list
     */
    List<EnterpriseProtectiveMeasuresCustom> listProtectiveMeasures(Map<String ,Object> condition);
}