package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.MonitorWorkplaceHarmFactorMapper;
import com.lanware.ehs.pojo.custom.MonitorWorkplaceHarmFactorCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 监测点关联的作业场所和监测危害因素扩展mapper
 */
public interface MonitorWorkplaceHarmFactorCustomMapper extends MonitorWorkplaceHarmFactorMapper {

    List<MonitorWorkplaceHarmFactorCustom> listWorkplaceHarmFactors(Map<String, Object> condition);

    MonitorWorkplaceHarmFactorCustom getMonitorWorkplaceHarmFactorCustom(Map<String, Object> condition);

    List<MonitorWorkplaceHarmFactorCustom> listCheckResults(Map<String, Object> condition);

    List<MonitorWorkplaceHarmFactorCustom> getCheckPlanHarmFactors(Map<String, Object> condition);

    int deleteByCondition(Map<String, Object> condition);

    List<MonitorWorkplaceHarmFactorCustom> listWorkplacesByMonitorId(Long monitorPointId);

    List<MonitorWorkplaceHarmFactorCustom> listHarmFactorsByMonitorWorkplaceId(Long monitorPointId, Long workplaceId);
}