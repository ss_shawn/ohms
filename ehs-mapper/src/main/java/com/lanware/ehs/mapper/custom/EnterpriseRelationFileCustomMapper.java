package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.custom.EnterpriseRelationFileCustom;

import java.util.List;
import java.util.Map;

public interface EnterpriseRelationFileCustomMapper {

    List<EnterpriseRelationFileCustom> queryEnterpriseRelationFileByCondition(Map<String, Object> condition);

    /**
     * @description 查询工作场所关联的文件信息
     * @param relationId 关联作业场所id
     * @return 关联文件扩展list
     */
    List<EnterpriseRelationFileCustom> listWorkplaceRelationFilesByRealtionId(Long relationId);

    /**
     * @description 删除工作场所关联的文件信息
     * @param relationId 关联作业场所id
     * @return 删除成功的数目
     */
    int deleteWorkplaceRelationFilesByRealtionId(Long relationId);

    /**
     * @description 查询工作场所关联的文件信息
     * @param relationIds 关联作业场所id数组
     * @return 关联文件扩展list
     */
    List<EnterpriseRelationFileCustom> listWorkplaceRelationFilesByRealtionIds(Long[] relationIds);

    /**
     * @description 删除工作场所关联的文件信息
     * @param relationIds 关联作业场所id数组
     * @return 删除成功的数目
     */
    int deleteWorkplaceRelationFilesByRealtionIds(Long[] relationIds);
}