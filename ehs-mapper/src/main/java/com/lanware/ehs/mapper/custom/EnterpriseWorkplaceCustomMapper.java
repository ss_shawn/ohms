package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.EnterpriseWorkplace;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceCustom;

import java.util.List;
import java.util.Map;

/**
 * @description EnterpriseWorkplace扩展接口
 */
public interface EnterpriseWorkplaceCustomMapper {
    /**
     * @description 查询作业场所list
     * @param condition 查询条件
     * @return 作业场所扩展list
     */
    List<EnterpriseWorkplaceCustom> listWorkplaces(Map<String, Object> condition);

    /**
     * @description 查询有职业病危害因素的作业场所list
     * @param condition 查询条件
     * @return 作业场所扩展list
     */
    List<EnterpriseWorkplaceCustom> listHarmWorkplaces(Map<String, Object> condition);

    /**
     * @description 查询该监测点未添加的作业场所(编辑的时候原作业场所也存在)
     * @param condition 查询条件(企业id,监测点id,作业场所id)
     * @return 作业场所list
     */
    List<EnterpriseWorkplace> getSelectMonitorWorkplaces(Map<String, Object> condition);
}