package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.EnterpriseManageFile;

import java.util.List;
import java.util.Map;

/**
 * @description 企业管理文件mapper扩展
 */
public interface EnterpriseManageFileCustomMapper {
    /**
     * @description 查询企业管理文件list
     * @param condition 查询条件
     * @return 各级责任制文件list
     */
    List<EnterpriseManageFile> listManageFiles(Map<String, Object> condition);
}