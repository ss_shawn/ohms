package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EquipmentServiceUseMapper;
import com.lanware.ehs.pojo.EquipmentServiceUse;
import com.lanware.ehs.pojo.custom.EquipmentServiceUseCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 维护、运行记录关联设备扩展mapper
 */
public interface EquipmentServiceUseCustomMapper extends EquipmentServiceUseMapper {

    List<EquipmentServiceUseCustom> listEquipmentsByRecordId(Map<String, Object> condition);

    List<EquipmentServiceUseCustom> listSelectEquipmentsByRecordId(Map<String, Object> condition);

    int insertBatch(List<EquipmentServiceUse> equipmentServiceUseList);
}