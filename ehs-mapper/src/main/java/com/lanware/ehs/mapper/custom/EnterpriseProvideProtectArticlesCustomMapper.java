package com.lanware.ehs.mapper.custom;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProvideProtectArticlesCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 防护用品mapper扩展类
 * @author
 * @date 2019-07-25 10:10
 */
public interface EnterpriseProvideProtectArticlesCustomMapper {
    /**
     * @description 查询防护用品发放list
     * @param condition 查询条件
     * @return 防护用品list
     */
    Page<EnterpriseProvideProtectArticlesCustom> findProtectList(Map<String, Object> condition);
    /**
     * @description 查询防护用品发放
     * @param id 防护用品发放主键
     * @return 防护用品
     */
    EnterpriseProvideProtectArticlesCustom selectByPrimaryKey(long id);

    /**
     * 根据人员岗位ID查询该人员岗位的防护用品没有发放数量
     * @param employeePostId 人员岗位ID
     * @return 没有发放数量
     */
    int selectCountByEmployeePostId(Long employeePostId);

    /**
     * 查询工作场所、岗位、应发放、人员、防护用品、数量
     * @param condition
     * @return
     */
    List<Map<String, Object>> findProvideProtectArticles(Map<String, Object> condition);

    /**
     * 查询岗位、人员、防护用品、数量
     * @param condition
     * @return
     */
    List<Map<String, Object>> findProvideProtectArticlesByPost(Map<String, Object> condition);
}