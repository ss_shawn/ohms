package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.BaseHarmFactor;

import java.util.List;
import java.util.Map;

/**
 * @description 危害因素mapper扩展类
 * @author WangYumeng
 * @date 2019-07-04 10:10
 */
public interface BaseHarmFactorCustomMapper {
    /**
     * @description 查询危害因素list
     * @param condition 查询条件
     * @return 危害因素list
     */
    List<BaseHarmFactor> listHarmFactors(Map<String, Object> condition);

    /**
     * @description 根据name查询危害因素
     * @param name 危害因素名称
     * @return BaseHarmFactor危害因素pojo
     */
    BaseHarmFactor getHarmFactorByName(String name);

    /**
     * @description 根据condition查询危害因素
     * @param condition 查询条件
     * @return com.lanware.management.pojo.BaseHarmFactor 返回BaseHarmFactor对象
     */
    BaseHarmFactor getHarmFactorByCondition(Map<String, Object> condition);

    /**
     * @description 取未选择的危害因素result
     * @param workplaceId 工作场所id
     * @param harmFactorId 危害因素id
     * @return 危害因素list
     */
    List<BaseHarmFactor> listUnselectedHarmFactors(Long workplaceId, Long harmFactorId);

    /**
     * @description 更新危害因素信息
     * @author WangYumeng
     * @date 2019/7/5 14:58
     * @param baseHarmFactor 危害因素pojo
     * @return
     */
    int updateHarmFactor(BaseHarmFactor baseHarmFactor);

    /**
     * @description 批量插入危害因素信息
     * @author WangYumeng
     * @date 2019/7/8 8:45
     * @param baseHarmFactorList 危害因素list
     * @return int 插入成功的数目
     */
    int insertBatch(List<BaseHarmFactor> baseHarmFactorList);
}