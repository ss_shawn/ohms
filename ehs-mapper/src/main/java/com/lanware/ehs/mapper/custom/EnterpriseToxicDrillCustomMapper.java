package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.EnterpriseToxicDrillMapper;
import com.lanware.ehs.pojo.EnterpriseToxicDrill;

import java.util.List;
import java.util.Map;

/**
 * @description 高毒演练记录mapper扩展
 */
public interface EnterpriseToxicDrillCustomMapper extends EnterpriseToxicDrillMapper {
    /**
     * @description 查询高毒演练记录list
     * @param condition 查询条件
     * @return 高毒演练记录list
     */
    List<EnterpriseToxicDrill> listToxicDrills(Map<String, Object> condition);
}
