package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.GovernmentEnterpriseBaseinfoMapper;
import com.lanware.ehs.pojo.GovernmentEnterpriseBaseinfo;
import com.lanware.ehs.pojo.custom.GovernmentEnterpriseBaseinfotCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 局端企业基本信息扩展mapper
 */
public interface GovernmentEnterpriseBaseinfoCustomMapper extends GovernmentEnterpriseBaseinfoMapper {
    Map<String, Integer> getEnterpriseHarmEmployeeNum(Long areaId);

    List<GovernmentEnterpriseBaseinfotCustom> getMonitorEnterpriseByMonitorId(Long monitorId);

    int insert(GovernmentEnterpriseBaseinfo record);

    List<GovernmentEnterpriseBaseinfotCustom> getMonitorEnterpriseByCondition(Map<String, Object> condition);

}