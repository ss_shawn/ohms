package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.pojo.EmployeeMedical;
import com.lanware.ehs.pojo.custom.EmployeeMedicalCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 员工体检扩展mapper
 */
public interface EmployeeMedicalCustomMapper {
    /**
     * @description 查询体检员工list
     * @param condition 查询条件
     * @return 体检员工list
     */
    List<EmployeeMedicalCustom> listMedicals(Map<String, Object> condition);

    /**
     * @description 根据id查询员工体检扩展信息
     * @param id 员工体检id
     * @return com.lanware.management.pojo.EmployeeMedical 返回EmployeeMedical对象
     */
    EmployeeMedicalCustom getMedicalCustomById(Long id);

    /**
     * @description 更新员工体检信息
     * @param employeeMedical 员工体检pojo
     * @return com.lanware.management.pojo.EmployeeMedical 返回EmployeeMedical对象
     */
    int updateMedical(EmployeeMedical employeeMedical);

    /**
     * @description 查询上年度体检人员各项人数
     * @param condition 查询条件
     * @return 体检员工扩展list
     */
    List<EmployeeMedicalCustom> listMedicalPersonNum(Map<String, Object> condition);

    /**
     * 获取年度每月体检人数
     * @param condition
     * @return
     */
    Map<String, Integer> getMedicalNumByCondition(Map<String, Object> condition);
}
