package com.lanware.ehs.mapper;

import com.lanware.ehs.pojo.WaitCheckHarmFactor;
import com.lanware.ehs.pojo.WaitCheckHarmFactorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WaitCheckHarmFactorMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    long countByExample(WaitCheckHarmFactorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    int deleteByExample(WaitCheckHarmFactorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    int insert(WaitCheckHarmFactor record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    int insertSelective(WaitCheckHarmFactor record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    List<WaitCheckHarmFactor> selectByExample(WaitCheckHarmFactorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    WaitCheckHarmFactor selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    int updateByExampleSelective(@Param("record") WaitCheckHarmFactor record, @Param("example") WaitCheckHarmFactorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    int updateByExample(@Param("record") WaitCheckHarmFactor record, @Param("example") WaitCheckHarmFactorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    int updateByPrimaryKeySelective(WaitCheckHarmFactor record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wait_check_harm_factor
     *
     * @mbg.generated Mon Sep 16 14:20:31 CST 2019
     */
    int updateByPrimaryKey(WaitCheckHarmFactor record);
}