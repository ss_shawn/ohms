package com.lanware.ehs.mapper.custom;

import com.lanware.ehs.mapper.GovernmentHarmReportMapper;
import com.lanware.ehs.pojo.GovernmentHarmReport;

import java.util.List;
import java.util.Map;

/**
 * @description 危害因素检测扩展mapper
 */
public interface GovernmentHarmReportCustomMapper extends GovernmentHarmReportMapper {
    List<GovernmentHarmReport> listEnterpriseYearHarmChecks(Map<String, Object> condition);
}