package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.PostHarmFactor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class PostHarmFactorCustom extends PostHarmFactor implements Serializable {

    private Long workplaceId;
    private String workplaceName;
    private String harmFactorName;

    private Integer monitorPeriod;

    private Integer monitorType;

    private String harmSource;

    private String operationWay;

    private String isPartition;

    private String deviceStatus;

    private Date harmFactorGmtCreate;

    private Date harmFactorGmtModified;

    /** 接触危害因素总人数 */
    private Integer totalNum;
    /** 接触危害因素女工数 */
    private Integer femaleNum;
    /** 接触危害因素农民工数 */
    private Integer migrantNum;

}