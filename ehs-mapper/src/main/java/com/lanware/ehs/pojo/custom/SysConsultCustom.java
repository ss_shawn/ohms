package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.SysConsult;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 系统咨询扩展类
 */
@Data
public class SysConsultCustom extends SysConsult implements Serializable {
    private static final long serialVersionUID = 2169845371652961764L;
    // 回复状态：未回复
    public static final Integer STATUS_ANSWER_NO = 0;
    // 回复状态：已回复
    public static final Integer STATUS_ANSWER_YES = 1;
    // 回复状态：最新回复(未被企业端查看过)
    public static final Integer STATUS_ANSWER_NEW = 2;
    // 阅读状态：未阅读
    public static final Integer STATUS_READ_NO = 0;
    // 阅读状态：已阅读
    public static final Integer STATUS_READ_YES = 1;

    /** 企业名称 */
    private String enterpriseName;
}
