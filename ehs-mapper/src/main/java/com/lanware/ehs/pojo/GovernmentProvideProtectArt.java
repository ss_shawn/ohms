package com.lanware.ehs.pojo;

public class GovernmentProvideProtectArt {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column government_provide_protect_art.id
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column government_provide_protect_art.enterprise_id
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    private Long enterpriseId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column government_provide_protect_art.year
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    private Integer year;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column government_provide_protect_art.name
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    private String name;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column government_provide_protect_art.model
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    private String model;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column government_provide_protect_art.number
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    private Integer number;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column government_provide_protect_art.id
     *
     * @return the value of government_provide_protect_art.id
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column government_provide_protect_art.id
     *
     * @param id the value for government_provide_protect_art.id
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column government_provide_protect_art.enterprise_id
     *
     * @return the value of government_provide_protect_art.enterprise_id
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public Long getEnterpriseId() {
        return enterpriseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column government_provide_protect_art.enterprise_id
     *
     * @param enterpriseId the value for government_provide_protect_art.enterprise_id
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column government_provide_protect_art.year
     *
     * @return the value of government_provide_protect_art.year
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public Integer getYear() {
        return year;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column government_provide_protect_art.year
     *
     * @param year the value for government_provide_protect_art.year
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column government_provide_protect_art.name
     *
     * @return the value of government_provide_protect_art.name
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column government_provide_protect_art.name
     *
     * @param name the value for government_provide_protect_art.name
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column government_provide_protect_art.model
     *
     * @return the value of government_provide_protect_art.model
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public String getModel() {
        return model;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column government_provide_protect_art.model
     *
     * @param model the value for government_provide_protect_art.model
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column government_provide_protect_art.number
     *
     * @return the value of government_provide_protect_art.number
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column government_provide_protect_art.number
     *
     * @param number the value for government_provide_protect_art.number
     *
     * @mbg.generated Mon Aug 26 17:48:49 CST 2019
     */
    public void setNumber(Integer number) {
        this.number = number;
    }
}