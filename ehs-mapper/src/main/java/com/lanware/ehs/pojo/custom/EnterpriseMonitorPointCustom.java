package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseMonitorPoint;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 监测点管理扩展类
 */
@Data
public class EnterpriseMonitorPointCustom extends EnterpriseMonitorPoint implements Serializable {
    private static final long serialVersionUID = -5293529896071776309L;
    /** 监测点关联的作业场所和监测危害因素数目 */
    private Integer monitorWorkplaceHarmFactorNum;
}