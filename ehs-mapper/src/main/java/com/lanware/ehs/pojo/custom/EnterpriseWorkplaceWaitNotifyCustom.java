package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseWorkplaceWaitNotify;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnterpriseWorkplaceWaitNotifyCustom extends EnterpriseWorkplaceWaitNotify implements Serializable {
    private static final long serialVersionUID = 8982120928298688264L;
    private String workplaceName;//作业场所名称
    private String checkOrganization;
    private Date checkDate;
    // 状态：未告知
    public static final Integer STATUS_NOTIFY_NO = 0;
    // 状态：已告知
    public static final Integer STATUS_NOTIFY_YES = 1;
}