package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterprisePost;
import lombok.Data;

import java.io.Serializable;
@Data
public class EnterprisePostCustom extends EnterprisePost implements Serializable {

    private String workplaceName;
    private String workplaceId;
    private Integer protectArticlesNum;
    private Integer harmFactorNum;


}