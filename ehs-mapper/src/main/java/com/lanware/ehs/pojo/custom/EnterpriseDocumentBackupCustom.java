package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseDocumentBackup;
import lombok.Data;

@Data
public class EnterpriseDocumentBackupCustom extends EnterpriseDocumentBackup {
    //企业名称
    private String enterpriseName;
}