package com.lanware.ehs.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EnterpriseAccidentHandleExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public EnterpriseAccidentHandleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNull() {
            addCriterion("enterprise_id is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNotNull() {
            addCriterion("enterprise_id is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdEqualTo(Long value) {
            addCriterion("enterprise_id =", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotEqualTo(Long value) {
            addCriterion("enterprise_id <>", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThan(Long value) {
            addCriterion("enterprise_id >", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("enterprise_id >=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThan(Long value) {
            addCriterion("enterprise_id <", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThanOrEqualTo(Long value) {
            addCriterion("enterprise_id <=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIn(List<Long> values) {
            addCriterion("enterprise_id in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotIn(List<Long> values) {
            addCriterion("enterprise_id not in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdBetween(Long value1, Long value2) {
            addCriterion("enterprise_id between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotBetween(Long value1, Long value2) {
            addCriterion("enterprise_id not between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andAccidentDateIsNull() {
            addCriterion("accident_date is null");
            return (Criteria) this;
        }

        public Criteria andAccidentDateIsNotNull() {
            addCriterion("accident_date is not null");
            return (Criteria) this;
        }

        public Criteria andAccidentDateEqualTo(Date value) {
            addCriterion("accident_date =", value, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateNotEqualTo(Date value) {
            addCriterion("accident_date <>", value, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateGreaterThan(Date value) {
            addCriterion("accident_date >", value, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateGreaterThanOrEqualTo(Date value) {
            addCriterion("accident_date >=", value, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateLessThan(Date value) {
            addCriterion("accident_date <", value, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateLessThanOrEqualTo(Date value) {
            addCriterion("accident_date <=", value, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateIn(List<Date> values) {
            addCriterion("accident_date in", values, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateNotIn(List<Date> values) {
            addCriterion("accident_date not in", values, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateBetween(Date value1, Date value2) {
            addCriterion("accident_date between", value1, value2, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentDateNotBetween(Date value1, Date value2) {
            addCriterion("accident_date not between", value1, value2, "accidentDate");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonIsNull() {
            addCriterion("accident_reason is null");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonIsNotNull() {
            addCriterion("accident_reason is not null");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonEqualTo(String value) {
            addCriterion("accident_reason =", value, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonNotEqualTo(String value) {
            addCriterion("accident_reason <>", value, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonGreaterThan(String value) {
            addCriterion("accident_reason >", value, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonGreaterThanOrEqualTo(String value) {
            addCriterion("accident_reason >=", value, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonLessThan(String value) {
            addCriterion("accident_reason <", value, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonLessThanOrEqualTo(String value) {
            addCriterion("accident_reason <=", value, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonLike(String value) {
            addCriterion("accident_reason like", value, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonNotLike(String value) {
            addCriterion("accident_reason not like", value, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonIn(List<String> values) {
            addCriterion("accident_reason in", values, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonNotIn(List<String> values) {
            addCriterion("accident_reason not in", values, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonBetween(String value1, String value2) {
            addCriterion("accident_reason between", value1, value2, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andAccidentReasonNotBetween(String value1, String value2) {
            addCriterion("accident_reason not between", value1, value2, "accidentReason");
            return (Criteria) this;
        }

        public Criteria andHandleProcessIsNull() {
            addCriterion("handle_process is null");
            return (Criteria) this;
        }

        public Criteria andHandleProcessIsNotNull() {
            addCriterion("handle_process is not null");
            return (Criteria) this;
        }

        public Criteria andHandleProcessEqualTo(String value) {
            addCriterion("handle_process =", value, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessNotEqualTo(String value) {
            addCriterion("handle_process <>", value, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessGreaterThan(String value) {
            addCriterion("handle_process >", value, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessGreaterThanOrEqualTo(String value) {
            addCriterion("handle_process >=", value, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessLessThan(String value) {
            addCriterion("handle_process <", value, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessLessThanOrEqualTo(String value) {
            addCriterion("handle_process <=", value, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessLike(String value) {
            addCriterion("handle_process like", value, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessNotLike(String value) {
            addCriterion("handle_process not like", value, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessIn(List<String> values) {
            addCriterion("handle_process in", values, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessNotIn(List<String> values) {
            addCriterion("handle_process not in", values, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessBetween(String value1, String value2) {
            addCriterion("handle_process between", value1, value2, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andHandleProcessNotBetween(String value1, String value2) {
            addCriterion("handle_process not between", value1, value2, "handleProcess");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisIsNull() {
            addCriterion("responsibility_analysis is null");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisIsNotNull() {
            addCriterion("responsibility_analysis is not null");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisEqualTo(String value) {
            addCriterion("responsibility_analysis =", value, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisNotEqualTo(String value) {
            addCriterion("responsibility_analysis <>", value, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisGreaterThan(String value) {
            addCriterion("responsibility_analysis >", value, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisGreaterThanOrEqualTo(String value) {
            addCriterion("responsibility_analysis >=", value, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisLessThan(String value) {
            addCriterion("responsibility_analysis <", value, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisLessThanOrEqualTo(String value) {
            addCriterion("responsibility_analysis <=", value, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisLike(String value) {
            addCriterion("responsibility_analysis like", value, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisNotLike(String value) {
            addCriterion("responsibility_analysis not like", value, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisIn(List<String> values) {
            addCriterion("responsibility_analysis in", values, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisNotIn(List<String> values) {
            addCriterion("responsibility_analysis not in", values, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisBetween(String value1, String value2) {
            addCriterion("responsibility_analysis between", value1, value2, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andResponsibilityAnalysisNotBetween(String value1, String value2) {
            addCriterion("responsibility_analysis not between", value1, value2, "responsibilityAnalysis");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleIsNull() {
            addCriterion("duty_person_handle is null");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleIsNotNull() {
            addCriterion("duty_person_handle is not null");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleEqualTo(String value) {
            addCriterion("duty_person_handle =", value, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleNotEqualTo(String value) {
            addCriterion("duty_person_handle <>", value, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleGreaterThan(String value) {
            addCriterion("duty_person_handle >", value, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleGreaterThanOrEqualTo(String value) {
            addCriterion("duty_person_handle >=", value, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleLessThan(String value) {
            addCriterion("duty_person_handle <", value, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleLessThanOrEqualTo(String value) {
            addCriterion("duty_person_handle <=", value, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleLike(String value) {
            addCriterion("duty_person_handle like", value, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleNotLike(String value) {
            addCriterion("duty_person_handle not like", value, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleIn(List<String> values) {
            addCriterion("duty_person_handle in", values, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleNotIn(List<String> values) {
            addCriterion("duty_person_handle not in", values, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleBetween(String value1, String value2) {
            addCriterion("duty_person_handle between", value1, value2, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andDutyPersonHandleNotBetween(String value1, String value2) {
            addCriterion("duty_person_handle not between", value1, value2, "dutyPersonHandle");
            return (Criteria) this;
        }

        public Criteria andMeasuresIsNull() {
            addCriterion("measures is null");
            return (Criteria) this;
        }

        public Criteria andMeasuresIsNotNull() {
            addCriterion("measures is not null");
            return (Criteria) this;
        }

        public Criteria andMeasuresEqualTo(String value) {
            addCriterion("measures =", value, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresNotEqualTo(String value) {
            addCriterion("measures <>", value, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresGreaterThan(String value) {
            addCriterion("measures >", value, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresGreaterThanOrEqualTo(String value) {
            addCriterion("measures >=", value, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresLessThan(String value) {
            addCriterion("measures <", value, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresLessThanOrEqualTo(String value) {
            addCriterion("measures <=", value, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresLike(String value) {
            addCriterion("measures like", value, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresNotLike(String value) {
            addCriterion("measures not like", value, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresIn(List<String> values) {
            addCriterion("measures in", values, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresNotIn(List<String> values) {
            addCriterion("measures not in", values, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresBetween(String value1, String value2) {
            addCriterion("measures between", value1, value2, "measures");
            return (Criteria) this;
        }

        public Criteria andMeasuresNotBetween(String value1, String value2) {
            addCriterion("measures not between", value1, value2, "measures");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated do_not_delete_during_merge Thu Jul 04 13:28:10 CST 2019
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table enterprise_accident_handle
     *
     * @mbg.generated Thu Jul 04 13:28:10 CST 2019
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}