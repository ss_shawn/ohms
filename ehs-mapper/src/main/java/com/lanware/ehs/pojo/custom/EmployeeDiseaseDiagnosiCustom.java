package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.EmployeePost;
import lombok.Data;

import java.io.Serializable;
@Data
public class EmployeeDiseaseDiagnosiCustom extends EmployeeDiseaseDiagnosi implements Serializable {

    private String employeeName;
    private String postName;

}