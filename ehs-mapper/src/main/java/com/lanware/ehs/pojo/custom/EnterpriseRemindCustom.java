package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseRemind;

public class EnterpriseRemindCustom extends EnterpriseRemind {
    // 提醒类别：防护设施维护检修计划
    public static final String TYPE_FHSS = "防护设施维护检修计划";

    // 提醒类别：应急救援演练计划
    public static final String TYPE_YJJY = "应急救援演练计划";

    //提醒状态：待提醒
    public static final Integer STATUS_DTX = 0;

    //提醒状态：已忽略
    public static final Integer STATUS_YHL = 1;

    //提醒状态：已完成
    public static final Integer STATUS_YWC = 2;
}
