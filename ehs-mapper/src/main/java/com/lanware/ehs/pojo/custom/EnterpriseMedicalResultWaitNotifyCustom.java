package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotify;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnterpriseMedicalResultWaitNotifyCustom extends EnterpriseMedicalResultWaitNotify implements Serializable {
    private static final long serialVersionUID = 5953750617026343843L;
    private String employeeName;//员工名称
    private String type;//体检类型
    private Date medicalDate;//体检时间
    private Long enterpriseId;//企业ID
    // 状态：未告知
    public static final Integer STATUS_NOTIFY_NO = 0;
    // 状态：已告知
    public static final Integer STATUS_NOTIFY_YES = 1;
}