package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description EnterpriseUser扩展类，添加字段enterpriseName
 * @author WangYumeng
 * @date 2019-07-02 11:24
 */
@Data
public class EnterpriseUserCustom extends EnterpriseUser implements Serializable {
    private static final long serialVersionUID = -9002541716218315739L;

    // 用户状态：有效
    public static final Integer STATUS_VALID = 0;
    // 用户状态：锁定
    public static final Integer STATUS_LOCK = 1;
    // 默认密码
    public static final String DEFAULT_PASSWORD = "123456";
    // 黑色主题
    public static final String THEME_BLACK = "black";
    // 白色主题
    public static final String THEME_WHITE = "white";
    // TAB开启
    public static final Integer TAB_OPEN = 1;
    // TAB关闭
    public static final Integer TAB_CLOSE = 0;


    // 是超级管理员
    public static final Integer ADMIN_YES = 1;
    // 不是超级管理员
    public static final Integer ADMIN_NO = 0;

    // 删除
    public static final Integer DELETED_YES = 1;
    // 未删除
    public static final Integer DELETED_NO = 0;




    private Long id;

    private String username;

    private String email;

    private String password;

    private Long enterpriseId;

    private Date lastLoginTime;

    private Integer status;

    private Integer isDeleted;

    private Integer isAdmin;

    private Integer isTab;

    private String theme;

    private Date gmtCreate;

    private Date gmtModified;

    private String remark;




    /********扩展*******/

    /** 企业名称 */
    private String enterpriseName;

    /** 企业是否同步局端 */
    private Integer syncAuth;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 创建开始时间
     */
    private String createTimeFrom;

    /**
     * 创建结束时间
     */
    private String createTimeTo;
}