package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysUserCustom extends SysUser implements Serializable {
    private static final long serialVersionUID = -8258257584299167577L;

    // 用户状态：有效
    public static final Integer STATUS_VALID = 0;
    // 用户状态：锁定
    public static final Integer STATUS_LOCK = 1;
    // 默认密码
    public static final String DEFAULT_PASSWORD = "123456";
    // 黑色主题
    public static final String THEME_BLACK = "black";
    // 白色主题
    public static final String THEME_WHITE = "white";
    // TAB开启
    public static final Integer TAB_OPEN = 1;
    // TAB关闭
    public static final Integer TAB_CLOSE = 0;
    // 删除
    public static final Integer DELETED_YES = 1;
    // 未删除
    public static final Integer DELETED_NO = 0;

    private Long id;

    private String username;

    private String email;

    private String password;

    private Date lastLoginTime;

    private Integer status;

    private Integer isDeleted;

    private Integer isTab;

    private String theme;

    private Date gmtCreate;

    private Date gmtModified;

    private String remark;


}
