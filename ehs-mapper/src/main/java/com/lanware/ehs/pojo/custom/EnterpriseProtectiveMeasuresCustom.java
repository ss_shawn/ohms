package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseProtectiveMeasures;

import java.io.Serializable;

/**
 * @description 防护措施扩展类
 */
public class EnterpriseProtectiveMeasuresCustom extends EnterpriseProtectiveMeasures implements Serializable {
    private static final long serialVersionUID = -3801236078215571178L;
    /** 工作场所名称 */
    private String workplaceName;
    /** 设备被添加的记录数 */
    private Integer recordNum;

    public String getWorkplaceName() {
        return workplaceName;
    }

    public void setWorkplaceName(String workplaceName) {
        this.workplaceName = workplaceName;
    }

    public Integer getRecordNum() {
        return recordNum;
    }

    public void setRecordNum(Integer recordNum) {
        this.recordNum = recordNum;
    }
}