package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseHygieneOrg;
import lombok.Data;

@Data
public class EnterpriseHygieneOrgCustom extends EnterpriseHygieneOrg {
    /** 资质证书数目 */
    private Integer certificateNum;
    /** 管理员培训记录数目 */
    private Integer managerTrainingNum;
}