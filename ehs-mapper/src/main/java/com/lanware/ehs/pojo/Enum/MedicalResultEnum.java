package com.lanware.ehs.pojo.Enum;

/**
 * 体检结论枚举类
 */
public enum MedicalResultEnum {
    WJYC("wjyc","未见异常"),
    FC("wjyc","复查"),
    YSZYB("wjyc","疑似职业病"),
    ZYJJZ("wjyc","职业禁忌症"),
    QTJH("wjyc","其它疾患");

    private String value;
    private String description;
    MedicalResultEnum(String value,String description){
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }}
