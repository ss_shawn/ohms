package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.GovernmentThreeSimultaneity;

import java.io.Serializable;

/**
 * @description 局端三同时扩展类
 */
public class GovernmentThreeSimultaneityCustom extends GovernmentThreeSimultaneity implements Serializable {
    private static final long serialVersionUID = 658518327181881382L;

    /** 距离上次更新天数 */
    private Long dayDiff;

    public Long getDayDiff() {
        return dayDiff;
    }

    public void setDayDiff(Long dayDiff) {
        this.dayDiff = dayDiff;
    }
}