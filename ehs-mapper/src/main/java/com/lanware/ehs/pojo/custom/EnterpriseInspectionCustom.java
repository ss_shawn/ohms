package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseInspection;

import java.io.Serializable;

/**
 * @description 日常检查整改登记扩展类
 */
public class EnterpriseInspectionCustom extends EnterpriseInspection implements Serializable {
    private static final long serialVersionUID = -3837958955538001264L;
    /** 整改记录数目 */
    private Integer rectificationNum;

    public Integer getRectificationNum() {
        return rectificationNum;
    }

    public void setRectificationNum(Integer rectificationNum) {
        this.rectificationNum = rectificationNum;
    }
}