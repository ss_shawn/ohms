package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.WorkplaceHarmFactor;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 工作场所危害因素pojo扩展类
 */
@Data
public class WorkplaceHarmFactorCustom extends WorkplaceHarmFactor implements Serializable {
    private static final long serialVersionUID = -3867943721657384320L;
    /** 危害因素名称 */
    private String harmFactorName;
    /** 岗位涉及的工作场所和危害因素集合数目 */
    private Integer postWorkplaceHarmFactorNum;
    /** 危害因素检测涉及的工作场所和危害因素集合数目 */
    private Integer outsideCheckWorkplaceHarmFactorNum;
    /** 日常监测涉及的工作场所和危害因素集合数目 */
    private Integer insideMonitorWorkplaceHarmFactorNum;

    /** 作业场所名称 */
    private String workplaceName;
    /** 工程防护设施 */
    private String ProtectiveMeasureName;
    /** 个体防护用品 */
    private String protectiveEquipmentName;
}
