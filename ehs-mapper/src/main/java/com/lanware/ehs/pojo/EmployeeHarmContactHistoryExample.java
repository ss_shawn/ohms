package com.lanware.ehs.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmployeeHarmContactHistoryExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public EmployeeHarmContactHistoryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameIsNull() {
            addCriterion("enterprise_name is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameIsNotNull() {
            addCriterion("enterprise_name is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameEqualTo(String value) {
            addCriterion("enterprise_name =", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameNotEqualTo(String value) {
            addCriterion("enterprise_name <>", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameGreaterThan(String value) {
            addCriterion("enterprise_name >", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameGreaterThanOrEqualTo(String value) {
            addCriterion("enterprise_name >=", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameLessThan(String value) {
            addCriterion("enterprise_name <", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameLessThanOrEqualTo(String value) {
            addCriterion("enterprise_name <=", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameLike(String value) {
            addCriterion("enterprise_name like", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameNotLike(String value) {
            addCriterion("enterprise_name not like", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameIn(List<String> values) {
            addCriterion("enterprise_name in", values, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameNotIn(List<String> values) {
            addCriterion("enterprise_name not in", values, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameBetween(String value1, String value2) {
            addCriterion("enterprise_name between", value1, value2, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameNotBetween(String value1, String value2) {
            addCriterion("enterprise_name not between", value1, value2, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdIsNull() {
            addCriterion("employee_id is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdIsNotNull() {
            addCriterion("employee_id is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdEqualTo(Long value) {
            addCriterion("employee_id =", value, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdNotEqualTo(Long value) {
            addCriterion("employee_id <>", value, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdGreaterThan(Long value) {
            addCriterion("employee_id >", value, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("employee_id >=", value, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdLessThan(Long value) {
            addCriterion("employee_id <", value, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdLessThanOrEqualTo(Long value) {
            addCriterion("employee_id <=", value, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdIn(List<Long> values) {
            addCriterion("employee_id in", values, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdNotIn(List<Long> values) {
            addCriterion("employee_id not in", values, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdBetween(Long value1, Long value2) {
            addCriterion("employee_id between", value1, value2, "employeeId");
            return (Criteria) this;
        }

        public Criteria andEmployeeIdNotBetween(Long value1, Long value2) {
            addCriterion("employee_id not between", value1, value2, "employeeId");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNull() {
            addCriterion("starttime is null");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNotNull() {
            addCriterion("starttime is not null");
            return (Criteria) this;
        }

        public Criteria andStarttimeEqualTo(Date value) {
            addCriterion("starttime =", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotEqualTo(Date value) {
            addCriterion("starttime <>", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThan(Date value) {
            addCriterion("starttime >", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("starttime >=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThan(Date value) {
            addCriterion("starttime <", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThanOrEqualTo(Date value) {
            addCriterion("starttime <=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIn(List<Date> values) {
            addCriterion("starttime in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotIn(List<Date> values) {
            addCriterion("starttime not in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeBetween(Date value1, Date value2) {
            addCriterion("starttime between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotBetween(Date value1, Date value2) {
            addCriterion("starttime not between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNull() {
            addCriterion("endtime is null");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNotNull() {
            addCriterion("endtime is not null");
            return (Criteria) this;
        }

        public Criteria andEndtimeEqualTo(Date value) {
            addCriterion("endtime =", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotEqualTo(Date value) {
            addCriterion("endtime <>", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThan(Date value) {
            addCriterion("endtime >", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("endtime >=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThan(Date value) {
            addCriterion("endtime <", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThanOrEqualTo(Date value) {
            addCriterion("endtime <=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIn(List<Date> values) {
            addCriterion("endtime in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotIn(List<Date> values) {
            addCriterion("endtime not in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeBetween(Date value1, Date value2) {
            addCriterion("endtime between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotBetween(Date value1, Date value2) {
            addCriterion("endtime not between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdIsNull() {
            addCriterion("employee_post_id is null");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdIsNotNull() {
            addCriterion("employee_post_id is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdEqualTo(Long value) {
            addCriterion("employee_post_id =", value, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdNotEqualTo(Long value) {
            addCriterion("employee_post_id <>", value, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdGreaterThan(Long value) {
            addCriterion("employee_post_id >", value, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdGreaterThanOrEqualTo(Long value) {
            addCriterion("employee_post_id >=", value, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdLessThan(Long value) {
            addCriterion("employee_post_id <", value, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdLessThanOrEqualTo(Long value) {
            addCriterion("employee_post_id <=", value, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdIn(List<Long> values) {
            addCriterion("employee_post_id in", values, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdNotIn(List<Long> values) {
            addCriterion("employee_post_id not in", values, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdBetween(Long value1, Long value2) {
            addCriterion("employee_post_id between", value1, value2, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andEmployeePostIdNotBetween(Long value1, Long value2) {
            addCriterion("employee_post_id not between", value1, value2, "employeePostId");
            return (Criteria) this;
        }

        public Criteria andPostNameIsNull() {
            addCriterion("post_name is null");
            return (Criteria) this;
        }

        public Criteria andPostNameIsNotNull() {
            addCriterion("post_name is not null");
            return (Criteria) this;
        }

        public Criteria andPostNameEqualTo(String value) {
            addCriterion("post_name =", value, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameNotEqualTo(String value) {
            addCriterion("post_name <>", value, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameGreaterThan(String value) {
            addCriterion("post_name >", value, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameGreaterThanOrEqualTo(String value) {
            addCriterion("post_name >=", value, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameLessThan(String value) {
            addCriterion("post_name <", value, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameLessThanOrEqualTo(String value) {
            addCriterion("post_name <=", value, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameLike(String value) {
            addCriterion("post_name like", value, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameNotLike(String value) {
            addCriterion("post_name not like", value, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameIn(List<String> values) {
            addCriterion("post_name in", values, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameNotIn(List<String> values) {
            addCriterion("post_name not in", values, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameBetween(String value1, String value2) {
            addCriterion("post_name between", value1, value2, "postName");
            return (Criteria) this;
        }

        public Criteria andPostNameNotBetween(String value1, String value2) {
            addCriterion("post_name not between", value1, value2, "postName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameIsNull() {
            addCriterion("harm_factor_name is null");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameIsNotNull() {
            addCriterion("harm_factor_name is not null");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameEqualTo(String value) {
            addCriterion("harm_factor_name =", value, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameNotEqualTo(String value) {
            addCriterion("harm_factor_name <>", value, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameGreaterThan(String value) {
            addCriterion("harm_factor_name >", value, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameGreaterThanOrEqualTo(String value) {
            addCriterion("harm_factor_name >=", value, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameLessThan(String value) {
            addCriterion("harm_factor_name <", value, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameLessThanOrEqualTo(String value) {
            addCriterion("harm_factor_name <=", value, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameLike(String value) {
            addCriterion("harm_factor_name like", value, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameNotLike(String value) {
            addCriterion("harm_factor_name not like", value, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameIn(List<String> values) {
            addCriterion("harm_factor_name in", values, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameNotIn(List<String> values) {
            addCriterion("harm_factor_name not in", values, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameBetween(String value1, String value2) {
            addCriterion("harm_factor_name between", value1, value2, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andHarmFactorNameNotBetween(String value1, String value2) {
            addCriterion("harm_factor_name not between", value1, value2, "harmFactorName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameIsNull() {
            addCriterion("protect_articles_name is null");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameIsNotNull() {
            addCriterion("protect_articles_name is not null");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameEqualTo(String value) {
            addCriterion("protect_articles_name =", value, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameNotEqualTo(String value) {
            addCriterion("protect_articles_name <>", value, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameGreaterThan(String value) {
            addCriterion("protect_articles_name >", value, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameGreaterThanOrEqualTo(String value) {
            addCriterion("protect_articles_name >=", value, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameLessThan(String value) {
            addCriterion("protect_articles_name <", value, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameLessThanOrEqualTo(String value) {
            addCriterion("protect_articles_name <=", value, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameLike(String value) {
            addCriterion("protect_articles_name like", value, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameNotLike(String value) {
            addCriterion("protect_articles_name not like", value, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameIn(List<String> values) {
            addCriterion("protect_articles_name in", values, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameNotIn(List<String> values) {
            addCriterion("protect_articles_name not in", values, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameBetween(String value1, String value2) {
            addCriterion("protect_articles_name between", value1, value2, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectArticlesNameNotBetween(String value1, String value2) {
            addCriterion("protect_articles_name not between", value1, value2, "protectArticlesName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameIsNull() {
            addCriterion("protective_measures_name is null");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameIsNotNull() {
            addCriterion("protective_measures_name is not null");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameEqualTo(String value) {
            addCriterion("protective_measures_name =", value, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameNotEqualTo(String value) {
            addCriterion("protective_measures_name <>", value, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameGreaterThan(String value) {
            addCriterion("protective_measures_name >", value, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameGreaterThanOrEqualTo(String value) {
            addCriterion("protective_measures_name >=", value, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameLessThan(String value) {
            addCriterion("protective_measures_name <", value, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameLessThanOrEqualTo(String value) {
            addCriterion("protective_measures_name <=", value, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameLike(String value) {
            addCriterion("protective_measures_name like", value, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameNotLike(String value) {
            addCriterion("protective_measures_name not like", value, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameIn(List<String> values) {
            addCriterion("protective_measures_name in", values, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameNotIn(List<String> values) {
            addCriterion("protective_measures_name not in", values, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameBetween(String value1, String value2) {
            addCriterion("protective_measures_name between", value1, value2, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andProtectiveMeasuresNameNotBetween(String value1, String value2) {
            addCriterion("protective_measures_name not between", value1, value2, "protectiveMeasuresName");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated do_not_delete_during_merge Fri Aug 02 10:40:18 CST 2019
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table employee_harm_contact_history
     *
     * @mbg.generated Fri Aug 02 10:40:18 CST 2019
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}