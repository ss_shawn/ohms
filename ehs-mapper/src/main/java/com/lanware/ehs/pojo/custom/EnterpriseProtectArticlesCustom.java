package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeePost;
import com.lanware.ehs.pojo.EnterpriseProtectArticles;
import lombok.Data;

import java.io.Serializable;
@Data
public class EnterpriseProtectArticlesCustom extends EnterpriseProtectArticles implements Serializable {
    private String harmFactorName;
    private String harmFactorId;
}