package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeeHarmContactHistory;
import lombok.Data;


import java.io.Serializable;
import java.util.Date;

@Data
public class EmployeeHarmContactHistoryCustom extends EmployeeHarmContactHistory implements Serializable {
    private String employeeName;
    private String enterpriseName2;
    private Date starttime2;
    private Date endtime2;
    private String  postName2;
    private String harmFactorName2;
    private String protectArticlesName2;
    private String protectiveMeasuresName2;

}