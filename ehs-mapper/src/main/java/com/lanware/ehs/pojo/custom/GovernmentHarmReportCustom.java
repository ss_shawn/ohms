package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.GovernmentHarmReport;

import java.io.Serializable;

/**
 * @description 局端企业年度危害因素扩展类
 */
public class GovernmentHarmReportCustom extends GovernmentHarmReport implements Serializable {
    private static final long serialVersionUID = -3038737368086317126L;
    private String harmFactorName;

    public String getHarmFactorName() {
        return harmFactorName;
    }

    public void setHarmFactorName(String harmFactorName) {
        this.harmFactorName = harmFactorName;
    }
}