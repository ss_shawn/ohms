package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EquipmentServiceUse;

import java.io.Serializable;

/**
 * @description 维护、运行记录关联设备扩展类
 */
public class EquipmentServiceUseCustom extends EquipmentServiceUse implements Serializable {
    private static final long serialVersionUID = -5328556296763393346L;

    // 记录类型：维护记录
    public static final Integer RECORD_SERVICE = 0;
    // 记录类型：运行记录
    public static final Integer RECORD_RUN = 1;

    /** 设备名称 */
    private String equipmentName;
    /** 工作场所 */
    private String workplaceName;

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getWorkplaceName() {
        return workplaceName;
    }

    public void setWorkplaceName(String workplaceName) {
        this.workplaceName = workplaceName;
    }
}