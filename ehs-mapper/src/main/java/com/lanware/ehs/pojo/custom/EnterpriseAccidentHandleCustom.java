package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseAccidentHandle;

import java.io.Serializable;

/**
 * @description 事故调查处理扩展类
 */
public class EnterpriseAccidentHandleCustom extends EnterpriseAccidentHandle implements Serializable {
    private static final long serialVersionUID = 2893968338805800512L;
    /** 事故调查处理相关文件数目 */
    private Integer accidentFileNum;

    public Integer getAccidentFileNum() {
        return accidentFileNum;
    }

    public void setAccidentFileNum(Integer accidentFileNum) {
        this.accidentFileNum = accidentFileNum;
    }
}
