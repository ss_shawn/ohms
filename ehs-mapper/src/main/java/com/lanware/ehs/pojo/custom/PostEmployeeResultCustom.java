package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeePost;
import lombok.Data;

import java.io.Serializable;
@Data
public class PostEmployeeResultCustom  implements Serializable {
    private long id;
    private String postName;
    private String employeeName;
    private String harmFactorName;
}