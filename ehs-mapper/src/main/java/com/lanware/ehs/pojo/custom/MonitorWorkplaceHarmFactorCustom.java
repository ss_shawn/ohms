package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.MonitorWorkplaceHarmFactor;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 监测点关联的作业场所和监测危害因素扩展类
 */
@Data
public class MonitorWorkplaceHarmFactorCustom extends MonitorWorkplaceHarmFactor implements Serializable {
    private static final long serialVersionUID = 2412023457600481596L;
    /** 工作场所名称 */
    private String workplaceName;
    /** 危害因素名称集合 */
    private String harmFactorName;
    /** 危害因素id集合 */
    private String tempHarmFactorId;
    /** 外部检测id */
    private Long outsideCheckId;
    /** 危害因素检测涉及的监测点的工作场所和危害因素集合数目 */
    private Integer outsideCheckMonitorWorkplaceHarmFactorNum;
}