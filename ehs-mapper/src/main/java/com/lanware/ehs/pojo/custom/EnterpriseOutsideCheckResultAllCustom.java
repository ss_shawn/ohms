package com.lanware.ehs.pojo.custom;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description 外部检测结果扩展类
 */
@Data
public class EnterpriseOutsideCheckResultAllCustom   implements Serializable {
    private Long id;//检测结果主键
    /** 岗位名称 */
    private String postName;
    /** 危害因素名称 */
    private String harmFactorName;
    private Date checkDate;//检测时间
    private String checkOrganization;//检测单位
    private String isQualified;//检测结果(是否合格)
    private String protectArticlesName;//防护用品
    private String protectiveMeasuresName;//防护设施


}
