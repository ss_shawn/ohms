package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeeMedical;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 员工体检扩展类
 */
@Data
public class EmployeeMedicalCustom extends EmployeeMedical implements Serializable {
    private static final long serialVersionUID = 6583650275340904104L;
    /** 员工姓名 */
    private String employeeName;
    /** 岗位名称 */
    private String postName;
    /** 机构名称 */
    private String institutionName;
    /** 职业病诊断数目 */
    private Integer diseaseNum;
    /** 体检结果告知数目 */
    private Integer medicalNotifyNum;
    /** 岗前体检人数 */
    private Integer beforePostNum;
    /** 岗中体检人数 */
    private Integer postingNum;
    /** 离岗体检人数 */
    private Integer afterPostNum;
    /** 体检发现职业病患者人数 */
    private Integer occupationalDiseaseNum;
    /** 调离岗位人数 */
    private Integer transferPostNum;

    //体检结论：复查
    public static final String RESULT_REVIEW = "复查";
    //体检结论：疑似职业病
    public static final String RESULT_SUSPECTED_OCCUPATIONAL_DISEASE = "疑似职业病";
    //体检结论：职业禁忌症
    public static final String RESULT_OCCUPATIONAL_CONTRAINDICATION = "职业禁忌症";
}
