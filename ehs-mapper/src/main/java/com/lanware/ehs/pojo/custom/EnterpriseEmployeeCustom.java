package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseEmployee;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnterpriseEmployeeCustom extends EnterpriseEmployee implements Serializable {
   private String postName;
   private Date workDate;
   private Long postId;
   /** 普通员工培训记录数目 */
   private Integer employeeTrainingNum;
}