package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseUserRole;
import lombok.Data;

import java.io.Serializable;

/**
 * 企业用户角色扩展类
 */
@Data
public class EnterpriseUserRoleCustom extends EnterpriseUserRole implements Serializable {
    private static final long serialVersionUID = 4555049169417565205L;

    /**
     * 显示
     */
    public static final Integer SHOW = 1;

    /**
     * 隐藏
     */
    public static final Integer HIDE = 0;

    /**
     * 角色对应的菜单ID
     */
    private String menuIds;

}
