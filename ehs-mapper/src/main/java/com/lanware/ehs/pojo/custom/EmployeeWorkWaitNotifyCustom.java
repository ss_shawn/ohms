package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeeWorkWaitNotify;
import com.lanware.ehs.pojo.EnterpriseWaitProvideProtectArticles;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EmployeeWorkWaitNotifyCustom extends EmployeeWorkWaitNotify implements Serializable {
    private String employeeName;//员工名称
    private Long postId;//岗位ID
    private String postName;//岗位名称
    private Date workDate;//上岗时间
    private Long enterpriseId;//企业ID
}