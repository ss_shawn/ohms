package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseResource;

import java.io.Serializable;
import java.util.Date;

public class EnterpriseResourceCustom extends EnterpriseResource implements Serializable {
    private static final long serialVersionUID = 3545537851240323367L;
    // 菜单
    public static final Integer TYPE_MENU = 0;
    // 按钮
    public static final Integer TYPE_BUTTON = 1;

    //显示
    public static final Integer SHOW_DISPLAY = 1;

    //不显示
    public static final Integer SHOW_HIDE = 0;
}