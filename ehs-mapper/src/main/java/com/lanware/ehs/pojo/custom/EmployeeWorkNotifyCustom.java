package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeeWorkNotify;
import com.lanware.ehs.pojo.EmployeeWorkWaitNotify;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EmployeeWorkNotifyCustom extends EmployeeWorkNotify implements Serializable {
    private String employeeName;//员工名称
    private Long postId;//岗位ID
    private String postName;//岗位名称
    private Date workDate;//上岗时间
    private Long enterpriseId;//企业ID
    private Long waitWorkNotifyId;//代待岗前告知ID
}