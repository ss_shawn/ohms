package com.lanware.ehs.pojo;

import java.util.Date;

public class EnterpriseResource {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.id
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.name
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private String name;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.url
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private String url;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.parent_id
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private Long parentId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.icon
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private String icon;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.weight
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private Integer weight;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.perms
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private String perms;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.is_show
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private Integer isShow;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.type
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private Integer type;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.gmt_create
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private Date gmtCreate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_resource.gmt_modified
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    private Date gmtModified;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.id
     *
     * @return the value of enterprise_resource.id
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.id
     *
     * @param id the value for enterprise_resource.id
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.name
     *
     * @return the value of enterprise_resource.name
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.name
     *
     * @param name the value for enterprise_resource.name
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.url
     *
     * @return the value of enterprise_resource.url
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public String getUrl() {
        return url;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.url
     *
     * @param url the value for enterprise_resource.url
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.parent_id
     *
     * @return the value of enterprise_resource.parent_id
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.parent_id
     *
     * @param parentId the value for enterprise_resource.parent_id
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.icon
     *
     * @return the value of enterprise_resource.icon
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public String getIcon() {
        return icon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.icon
     *
     * @param icon the value for enterprise_resource.icon
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.weight
     *
     * @return the value of enterprise_resource.weight
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.weight
     *
     * @param weight the value for enterprise_resource.weight
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.perms
     *
     * @return the value of enterprise_resource.perms
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public String getPerms() {
        return perms;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.perms
     *
     * @param perms the value for enterprise_resource.perms
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setPerms(String perms) {
        this.perms = perms == null ? null : perms.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.is_show
     *
     * @return the value of enterprise_resource.is_show
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public Integer getIsShow() {
        return isShow;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.is_show
     *
     * @param isShow the value for enterprise_resource.is_show
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.type
     *
     * @return the value of enterprise_resource.type
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public Integer getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.type
     *
     * @param type the value for enterprise_resource.type
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.gmt_create
     *
     * @return the value of enterprise_resource.gmt_create
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.gmt_create
     *
     * @param gmtCreate the value for enterprise_resource.gmt_create
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_resource.gmt_modified
     *
     * @return the value of enterprise_resource.gmt_modified
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_resource.gmt_modified
     *
     * @param gmtModified the value for enterprise_resource.gmt_modified
     *
     * @mbg.generated Wed Jul 10 10:16:46 CST 2019
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}