package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseWaitCheck;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description 待检测项目扩展类
 */
@Data
public class EnterpriseWaitCheckCustom extends EnterpriseWaitCheck implements Serializable {
    private static final long serialVersionUID = 4272414152775923957L;
    // 状态：未检测
    public static final Integer STATUS_CHECK_NO = 0;
    // 状态：已检测
    public static final Integer STATUS_CHECK_YES = 1;

    /** 危害因素名称 */
    private String harmFactorName;
    /** 危害因素备注 */
    private String remark;
    /** 危害因素mac */
    private BigDecimal mac;
    /** 危害因素twa */
    private BigDecimal twa;
    /** 危害因素stel */
    private BigDecimal stel;
    /** 危害因素超限倍数 */
    private BigDecimal overLimitMultiple;

    /** 岗位应测 */
    private Integer postShouldCheckNum;
    /** 岗位实测 */
    private Integer postActualCheckNum;
    /** 岗位合格 */
    private Integer postQualifiedNum;
    /** 监测点未测 */
    private Integer pointNotCheckNum;
    /** 监测点实测 */
    private Integer pointActualCheckNum;
    /** 监测点合格 */
    private Integer pointQualifiedNum;
}