package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseYearPlan;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 年度计划扩展类
 */
@Data
public class EnterpriseYearPlanCustom extends EnterpriseYearPlan implements Serializable {
    private static final long serialVersionUID = -4166258498081674414L;
    /** 危害因素名称集合 */
    private String harmFactorName;
    /** 危害因素id集合 */
    private String tempHarmFactorId;

    //防护设施维修计划
    public static String TYPE_FHSS = "fhssjc";

    //应急救援演练计划
    public static String TYPE_JYYL = "yjjyyl";

    //健康体检
    public static String TYPE_JKTJ = "jktj";
}
