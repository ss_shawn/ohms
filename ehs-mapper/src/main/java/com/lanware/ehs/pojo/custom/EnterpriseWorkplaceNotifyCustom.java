package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeeWorkNotify;
import com.lanware.ehs.pojo.EnterpriseWorkplaceNotify;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnterpriseWorkplaceNotifyCustom extends EnterpriseWorkplaceNotify implements Serializable {
    private String workplaceName;//作业场所名称
    private Long workplaceWaitNotifyId;//作业场所告知等待ID
    private String checkOrganization;
    private Date checkDate;
}