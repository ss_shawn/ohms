package com.lanware.ehs.pojo.custom;

import lombok.Data;

@Data
public class YearNotifyReportCustom {
    private String type;
    private int allNotifyNum;
    private int notifyNum;
    private double notifyStatistics;
}
