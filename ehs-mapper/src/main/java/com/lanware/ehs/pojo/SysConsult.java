package com.lanware.ehs.pojo;

import java.util.Date;

public class SysConsult {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.id
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.enterprise_id
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private Long enterpriseId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.title
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private String title;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.question
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private String question;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.mobile_phone
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private String mobilePhone;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.email
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private String email;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.answer
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private String answer;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.answer_time
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private Date answerTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.answer_status
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private Integer answerStatus;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.read_status
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private Integer readStatus;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.gmt_create
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private Date gmtCreate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_consult.gmt_modified
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    private Date gmtModified;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.id
     *
     * @return the value of sys_consult.id
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.id
     *
     * @param id the value for sys_consult.id
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.enterprise_id
     *
     * @return the value of sys_consult.enterprise_id
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public Long getEnterpriseId() {
        return enterpriseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.enterprise_id
     *
     * @param enterpriseId the value for sys_consult.enterprise_id
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.title
     *
     * @return the value of sys_consult.title
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public String getTitle() {
        return title;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.title
     *
     * @param title the value for sys_consult.title
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.question
     *
     * @return the value of sys_consult.question
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public String getQuestion() {
        return question;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.question
     *
     * @param question the value for sys_consult.question
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setQuestion(String question) {
        this.question = question == null ? null : question.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.mobile_phone
     *
     * @return the value of sys_consult.mobile_phone
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public String getMobilePhone() {
        return mobilePhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.mobile_phone
     *
     * @param mobilePhone the value for sys_consult.mobile_phone
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone == null ? null : mobilePhone.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.email
     *
     * @return the value of sys_consult.email
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public String getEmail() {
        return email;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.email
     *
     * @param email the value for sys_consult.email
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.answer
     *
     * @return the value of sys_consult.answer
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.answer
     *
     * @param answer the value for sys_consult.answer
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.answer_time
     *
     * @return the value of sys_consult.answer_time
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public Date getAnswerTime() {
        return answerTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.answer_time
     *
     * @param answerTime the value for sys_consult.answer_time
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setAnswerTime(Date answerTime) {
        this.answerTime = answerTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.answer_status
     *
     * @return the value of sys_consult.answer_status
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public Integer getAnswerStatus() {
        return answerStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.answer_status
     *
     * @param answerStatus the value for sys_consult.answer_status
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setAnswerStatus(Integer answerStatus) {
        this.answerStatus = answerStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.read_status
     *
     * @return the value of sys_consult.read_status
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public Integer getReadStatus() {
        return readStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.read_status
     *
     * @param readStatus the value for sys_consult.read_status
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setReadStatus(Integer readStatus) {
        this.readStatus = readStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.gmt_create
     *
     * @return the value of sys_consult.gmt_create
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.gmt_create
     *
     * @param gmtCreate the value for sys_consult.gmt_create
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_consult.gmt_modified
     *
     * @return the value of sys_consult.gmt_modified
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_consult.gmt_modified
     *
     * @param gmtModified the value for sys_consult.gmt_modified
     *
     * @mbg.generated Thu Sep 19 14:01:03 CST 2019
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}