package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseProtectArticles;
import com.lanware.ehs.pojo.EnterpriseProvideProtectArticles;
import lombok.Data;

import java.io.Serializable;

@Data
public class EnterpriseProvideProtectArticlesCustom extends EnterpriseProvideProtectArticles implements Serializable,Cloneable {
    private String employeeName;//员工名称
    private String allEmployeeId;//员工ID集合，临时使用
    private String postName;//岗位名称
    private String postId;//岗位Id
    private Long provideArticlesId;//发放物品ID
    private String provideArticlesName;//发放物品名称
    @Override
    public Object clone() {
        EnterpriseProvideProtectArticlesCustom stu = null;
        try{
            stu = (EnterpriseProvideProtectArticlesCustom)super.clone();
        }catch(CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return stu;
    }

}