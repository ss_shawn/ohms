package com.lanware.ehs.pojo;

import java.util.Date;

public class CheckPlanHarmFactor {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column check_plan_harm_factor.id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column check_plan_harm_factor.check_plan_id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    private Long checkPlanId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column check_plan_harm_factor.harm_factor_id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    private Long harmFactorId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column check_plan_harm_factor.gmt_create
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    private Date gmtCreate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column check_plan_harm_factor.gmt_modified
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    private Date gmtModified;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column check_plan_harm_factor.id
     *
     * @return the value of check_plan_harm_factor.id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column check_plan_harm_factor.id
     *
     * @param id the value for check_plan_harm_factor.id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column check_plan_harm_factor.check_plan_id
     *
     * @return the value of check_plan_harm_factor.check_plan_id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public Long getCheckPlanId() {
        return checkPlanId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column check_plan_harm_factor.check_plan_id
     *
     * @param checkPlanId the value for check_plan_harm_factor.check_plan_id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public void setCheckPlanId(Long checkPlanId) {
        this.checkPlanId = checkPlanId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column check_plan_harm_factor.harm_factor_id
     *
     * @return the value of check_plan_harm_factor.harm_factor_id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public Long getHarmFactorId() {
        return harmFactorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column check_plan_harm_factor.harm_factor_id
     *
     * @param harmFactorId the value for check_plan_harm_factor.harm_factor_id
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public void setHarmFactorId(Long harmFactorId) {
        this.harmFactorId = harmFactorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column check_plan_harm_factor.gmt_create
     *
     * @return the value of check_plan_harm_factor.gmt_create
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column check_plan_harm_factor.gmt_create
     *
     * @param gmtCreate the value for check_plan_harm_factor.gmt_create
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column check_plan_harm_factor.gmt_modified
     *
     * @return the value of check_plan_harm_factor.gmt_modified
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column check_plan_harm_factor.gmt_modified
     *
     * @param gmtModified the value for check_plan_harm_factor.gmt_modified
     *
     * @mbg.generated Mon Sep 16 09:13:26 CST 2019
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}