package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.MonitorUser;

import java.io.Serializable;

/**
 * @description MonitorUser扩展类，添加字段areaName
 * @author WangYumeng
 * @date 2019-07-02 13:24
 */
public class MonitorUserCustom extends MonitorUser implements Serializable {
    private static final long serialVersionUID = 1055684925094947187L;
    /** 监管区域名称 */
    private String areaName;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    // 用户状态：有效
    public static final Integer STATUS_VALID = 0;
    // 用户状态：锁定
    public static final Integer STATUS_LOCK = 1;
    // 默认密码
    public static final String DEFAULT_PASSWORD = "123456";
    // 黑色主题
    public static final String THEME_BLACK = "black";
    // 白色主题
    public static final String THEME_WHITE = "white";
    // TAB开启
    public static final Integer TAB_OPEN = 1;
    // TAB关闭
    public static final Integer TAB_CLOSE = 0;
    // 删除
    public static final Integer DELETED_YES = 1;
    // 未删除
    public static final Integer DELETED_NO = 0;
}