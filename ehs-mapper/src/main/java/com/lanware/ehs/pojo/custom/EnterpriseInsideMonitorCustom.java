package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseInsideMonitor;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 日常监测扩展类
 */
@Data
public class EnterpriseInsideMonitorCustom extends EnterpriseInsideMonitor implements Serializable {
    private static final long serialVersionUID = -8762259469563746407L;
    /** 工作场所名称 */
    private String workplaceName;
    /** 危害因素名称 */
    private String harmFactorName;
    /** 监测点名称 */
    private String monitorPointName;
}
