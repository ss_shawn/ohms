package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseWorkplace;

import java.io.Serializable;

/**
 * @description 作业场所扩展类
 */
public class EnterpriseWorkplaceCustom extends EnterpriseWorkplace implements Serializable {
    private static final long serialVersionUID = -8801374318999718032L;
    /** 作业场所危害因素数目 */
    private Integer harmfactorNum;
    /** 作业场所职业病危害警示数目 */
    private Integer identifyFileNum;
    /** 作业场所高毒物品告知卡数目 */
    private Integer informFileNum;
    /** 作业场所职业危害因素信息卡数目 */
    private Integer informationFileNum;
    /** 作业场所告知数目 */
    private Integer workplaceNotifyNum;
    /** 添加过作业场所的岗位数目 */
    private Integer workplacePostNum;


    public Integer getHarmfactorNum() {
        return harmfactorNum;
    }

    public void setHarmfactorNum(Integer harmfactorNum) {
        this.harmfactorNum = harmfactorNum;
    }

    public Integer getIdentifyFileNum() {
        return identifyFileNum;
    }

    public void setIdentifyFileNum(Integer identifyFileNum) {
        this.identifyFileNum = identifyFileNum;
    }

    public Integer getInformFileNum() {
        return informFileNum;
    }

    public void setInformFileNum(Integer informFileNum) {
        this.informFileNum = informFileNum;
    }

    public Integer getInformationFileNum() {
        return informationFileNum;
    }

    public void setInformationFileNum(Integer informationFileNum) {
        this.informationFileNum = informationFileNum;
    }

    public Integer getWorkplaceNotifyNum() {
        return workplaceNotifyNum;
    }

    public void setWorkplaceNotifyNum(Integer workplaceNotifyNum) {
        this.workplaceNotifyNum = workplaceNotifyNum;
    }

    public Integer getWorkplacePostNum() {
        return workplacePostNum;
    }

    public void setWorkplacePostNum(Integer workplacePostNum) {
        this.workplacePostNum = workplacePostNum;
    }
}
