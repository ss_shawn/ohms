package com.lanware.ehs.pojo;

import java.util.Date;

public class EnterpriseDocumentBackup {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_document_backup.id
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_document_backup.enterprise_id
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    private Long enterpriseId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_document_backup.backup_time
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    private Date backupTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_document_backup.file_name
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    private String fileName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_document_backup.file_path
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    private String filePath;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_document_backup.gmt_create
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    private Date gmtCreate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_document_backup.gmt_modified
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    private Date gmtModified;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_document_backup.id
     *
     * @return the value of enterprise_document_backup.id
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_document_backup.id
     *
     * @param id the value for enterprise_document_backup.id
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_document_backup.enterprise_id
     *
     * @return the value of enterprise_document_backup.enterprise_id
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public Long getEnterpriseId() {
        return enterpriseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_document_backup.enterprise_id
     *
     * @param enterpriseId the value for enterprise_document_backup.enterprise_id
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_document_backup.backup_time
     *
     * @return the value of enterprise_document_backup.backup_time
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public Date getBackupTime() {
        return backupTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_document_backup.backup_time
     *
     * @param backupTime the value for enterprise_document_backup.backup_time
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public void setBackupTime(Date backupTime) {
        this.backupTime = backupTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_document_backup.file_name
     *
     * @return the value of enterprise_document_backup.file_name
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_document_backup.file_name
     *
     * @param fileName the value for enterprise_document_backup.file_name
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_document_backup.file_path
     *
     * @return the value of enterprise_document_backup.file_path
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_document_backup.file_path
     *
     * @param filePath the value for enterprise_document_backup.file_path
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath == null ? null : filePath.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_document_backup.gmt_create
     *
     * @return the value of enterprise_document_backup.gmt_create
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_document_backup.gmt_create
     *
     * @param gmtCreate the value for enterprise_document_backup.gmt_create
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_document_backup.gmt_modified
     *
     * @return the value of enterprise_document_backup.gmt_modified
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_document_backup.gmt_modified
     *
     * @param gmtModified the value for enterprise_document_backup.gmt_modified
     *
     * @mbg.generated Tue Jul 23 13:47:40 CST 2019
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}