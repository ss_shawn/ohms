package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseDeclare;

import java.io.Serializable;

/**
 * @description 工作场所危害申报pojo扩展类
 */
public class EnterpriseDeclareCustom extends EnterpriseDeclare implements Serializable {
    private static final long serialVersionUID = 7356929951092226266L;
    /** 工作场所及危害因素数目 */
    private Integer declareWorkplaceNum;
    /** 申报涉及的作业长场所和危害因素组合 */
    private String declareHarmWorkplace;
    
    public Integer getDeclareWorkplaceNum() {
        return declareWorkplaceNum;
    }

    public void setDeclareWorkplaceNum(Integer declareWorkplaceNum) {
        this.declareWorkplaceNum = declareWorkplaceNum;
    }

    public String getDeclareHarmWorkplace() {
        return declareHarmWorkplace;
    }

    public void setDeclareHarmWorkplace(String declareHarmWorkplace) {
        this.declareHarmWorkplace = declareHarmWorkplace;
    }
}
