package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseManageFile;

/**
 * @description 企业管理文件扩展类
 */
public class EnterpriseManageFileCustom extends EnterpriseManageFile {
    // 文件模块：各级责任制
    public static final Integer MODULE_RESPONSIBILITY = 0;
    // 文件模块：职业健康防范制度
    public static final Integer MODULE_PREVENTION = 1;
}
