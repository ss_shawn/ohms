package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseEquipmentUse;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 运行使用扩展类
 */
@Data
public class EnterpriseEquipmentUseCustom extends EnterpriseEquipmentUse implements Serializable {
    private static final long serialVersionUID = 352137721316708877L;
    /** 设备名称 */
    private String equipmentName;
    /** 设备数目 */
    private Integer equipmentNum;
}