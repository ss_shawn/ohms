package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.BaseHarmFactor;

import java.io.Serializable;

/**
 * @description 危害因素扩展类
 */
public class BaseHarmFactorCustom extends BaseHarmFactor implements Serializable {
    private static final long serialVersionUID = 2413808898209584561L;
}