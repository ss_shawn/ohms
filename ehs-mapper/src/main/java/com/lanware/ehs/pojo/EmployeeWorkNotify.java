package com.lanware.ehs.pojo;

import java.util.Date;

public class EmployeeWorkNotify {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column employee_work_notify.id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column employee_work_notify.employee_id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    private Long employeeId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column employee_work_notify.employee_post_id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    private Long employeePostId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column employee_work_notify.notify_file
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    private String notifyFile;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column employee_work_notify.notify_date
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    private Date notifyDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column employee_work_notify.gmt_create
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    private Date gmtCreate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column employee_work_notify.gmt_modified
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    private Date gmtModified;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column employee_work_notify.id
     *
     * @return the value of employee_work_notify.id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column employee_work_notify.id
     *
     * @param id the value for employee_work_notify.id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column employee_work_notify.employee_id
     *
     * @return the value of employee_work_notify.employee_id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public Long getEmployeeId() {
        return employeeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column employee_work_notify.employee_id
     *
     * @param employeeId the value for employee_work_notify.employee_id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column employee_work_notify.employee_post_id
     *
     * @return the value of employee_work_notify.employee_post_id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public Long getEmployeePostId() {
        return employeePostId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column employee_work_notify.employee_post_id
     *
     * @param employeePostId the value for employee_work_notify.employee_post_id
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public void setEmployeePostId(Long employeePostId) {
        this.employeePostId = employeePostId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column employee_work_notify.notify_file
     *
     * @return the value of employee_work_notify.notify_file
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public String getNotifyFile() {
        return notifyFile;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column employee_work_notify.notify_file
     *
     * @param notifyFile the value for employee_work_notify.notify_file
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public void setNotifyFile(String notifyFile) {
        this.notifyFile = notifyFile == null ? null : notifyFile.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column employee_work_notify.notify_date
     *
     * @return the value of employee_work_notify.notify_date
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public Date getNotifyDate() {
        return notifyDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column employee_work_notify.notify_date
     *
     * @param notifyDate the value for employee_work_notify.notify_date
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public void setNotifyDate(Date notifyDate) {
        this.notifyDate = notifyDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column employee_work_notify.gmt_create
     *
     * @return the value of employee_work_notify.gmt_create
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column employee_work_notify.gmt_create
     *
     * @param gmtCreate the value for employee_work_notify.gmt_create
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column employee_work_notify.gmt_modified
     *
     * @return the value of employee_work_notify.gmt_modified
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column employee_work_notify.gmt_modified
     *
     * @param gmtModified the value for employee_work_notify.gmt_modified
     *
     * @mbg.generated Tue Jul 30 09:15:48 CST 2019
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}