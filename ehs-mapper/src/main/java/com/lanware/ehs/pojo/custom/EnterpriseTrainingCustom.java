package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseTraining;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 培训管理扩展类
 */
@Data
public class EnterpriseTrainingCustom extends EnterpriseTraining implements Serializable {
    private static final long serialVersionUID = -7700874138530229437L;

    //人员类型：管理员
    public static final Integer ADMIN_TYPE = 1;

    //人员类型：普通人员
    public static final Integer OPERATOR_TYPE = 0;

    /** 培训证明材料数目 */
    private Integer trainingFileNum;
    /** 参加培训的人员id */
    private String employeeId;
    /** 普通员工培训人数 */
    private Integer trainingEmployeeNum;
    /** 管理员培训人数 */
    private Integer trainingAdminNum;
}
