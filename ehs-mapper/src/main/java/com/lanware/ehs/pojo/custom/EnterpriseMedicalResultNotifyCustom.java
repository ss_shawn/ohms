package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseMedicalResultNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotify;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnterpriseMedicalResultNotifyCustom extends EnterpriseMedicalResultNotify implements Serializable {
    private String employeeName;//员工名称
    private Date medicalDate;//体检时间
    private String type;//体检类型
    private Long enterpriseId;//企业ID
    private Long waitMedicalNotifyId;//待体检ID
}