package com.lanware.ehs.pojo.Enum;

/**
 * 年度计划类型枚举类
 */
public enum YearPlanTypeEnum {
    ZYBFZ("zybfz","职业病防治计划"),
    JFTR("jftr","经费投入计划"),
    XCJY("xcjy","宣传教育计划"),
    JKTJ("jktj","健康体检计划"),
    FHSSJC("fhssjc","防护设施检测维护计划"),
    YJJYYL("yjjyyl","应急救援演练计划"),
    WHYSJC("whysjc","危害因素检测计划");

    private String value;

    private String description;

    YearPlanTypeEnum(String value,String description){
        this.description = description;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
