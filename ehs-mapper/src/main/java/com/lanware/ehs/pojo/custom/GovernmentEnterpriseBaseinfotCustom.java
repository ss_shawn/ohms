package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.GovernmentEnterpriseBaseinfo;
import lombok.Data;

@Data
public class GovernmentEnterpriseBaseinfotCustom extends GovernmentEnterpriseBaseinfo {
    //职业健康得分
    private Float score;

    private String lastUpdateDate;
}
