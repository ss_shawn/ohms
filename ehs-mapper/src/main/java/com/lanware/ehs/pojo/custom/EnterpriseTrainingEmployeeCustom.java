package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseTrainingEmployee;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description 培训人员扩展类
 */
@Data
public class EnterpriseTrainingEmployeeCustom extends EnterpriseTrainingEmployee implements Serializable {
    private static final long serialVersionUID = -1289498418014437758L;
    /** 电子档案号 */
    private String electronArchiveNumber;
    /** 姓名 */
    private String name;
    /** 年份 */
    private String year;
    /** 培训总时长 */
    private Integer totalDuration;
    /** 岗位名称 */
    private String postName;
    /** 部门名称 */
    private String deptName;
    /** 职务 */
    private String job;
    /** 部门 */
    private String department;

    /** 管理员年度培训报表 */
    private Integer adminNum;
    private BigDecimal totalAdminTrainingDuration;
    private BigDecimal actualTotalAdminTrainingDuration;
    private Integer adminTrainingNum;
    /** 普通员工年度培训报表 */
    private Integer involveNum;
    private Integer employeeNum;
    private BigDecimal totalEmployeeTrainingDuration;
    //实际培训总时长
    private BigDecimal actualTotalEmployeeTrainingDuration;
    private Integer employeeTrainingNum;
}
