package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseServiceRecord;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 维修记录扩展类
 */
@Data
public class EnterpriseServiceRecordCustom extends EnterpriseServiceRecord implements Serializable {
    private static final long serialVersionUID = 2949546290237162966L;
    /** 设备名称 */
    private String equipmentName;
    /** 设备数目 */
    private Integer equipmentNum;
}