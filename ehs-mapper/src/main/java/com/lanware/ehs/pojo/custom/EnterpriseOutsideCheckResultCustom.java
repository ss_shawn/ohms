package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseOutsideCheckResult;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description 外部检测结果扩展类
 */
@Data
public class EnterpriseOutsideCheckResultCustom extends EnterpriseOutsideCheckResult implements Serializable {
    private static final long serialVersionUID = -6875611370706894149L;
    // 状态：未取消检测
    public static final Integer STATUS_CANCEL_CHECK_NO = 0;
    // 状态：已取消检测
    public static final Integer STATUS_CANCEL_CHECK_YES = 1;
    /** 危害因素名称 */
    private String harmFactorName;
    /** 工作场所名称 */
    private String workplaceName;
    /** 监测点名称 */
    private String monitorPointName;
    /** 涉及岗位 */
    private String postName;
    private BigDecimal mac;
    private BigDecimal twa;
    private BigDecimal stel;
    /** 超限倍数 */
    private BigDecimal overLimitMultiple;
    /** 检测结果详细集合 */
    private String checkResultDetail;
}
