package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class EnterpriseBaseinfoCustom extends EnterpriseBaseinfo {
    // 用户状态：有效
    public static final Integer STATUS_VALID = 0;
    // 用户状态：锁定
    public static final Integer STATUS_LOCK = 1;

    // 同步权限：是
    public static final Integer SYNCAUTH_YES = 1;

    // 同步权限：否
    public static final Integer SYNCAUTH_NO = 0;

    private Integer isOverdue;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date buildDate;
}
