package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseThreeSimultaneity;

import java.io.Serializable;

/**
 * @description 三同时扩展类
 */
public class EnterpriseThreeSimultaneityCustom extends EnterpriseThreeSimultaneity implements Serializable {
    private static final long serialVersionUID = -2680461825382265000L;
    // 三同时项目阶段：0预评价 1防护设施设计 2竣工验收
    public static final Integer STAGE_PRE = 0;
    public static final Integer STAGE_DESIGN = 1;
    public static final Integer STAGE_ACCEPTANCE = 2;
    // 三同时危害程度：0一般 1较重 2严重
    public static final Integer DEGREE_LOW = 0;
    public static final Integer DEGREE_MIDDLE = 1;
    public static final Integer DEGREE_HIGH = 2;
    // 三同时完成状态：0未完成 1已完成
    public static final Integer STATUS_UNCOMPLETE = 0;
    public static final Integer STATUS_COMPLETE = 1;
    /** 预评价文件数目 */
    private Integer preEvaluationNum;
    /** 防护设施设计文件数目 */
    private Integer protectiveDesignNum;
    /** 竣工验收文件数目 */
    private Integer completionAcceptanceNum;
    /** 距离上次更新天数 */
    private Long dayDiff;

    public Integer getPreEvaluationNum() {
        return preEvaluationNum;
    }

    public void setPreEvaluationNum(Integer preEvaluationNum) {
        this.preEvaluationNum = preEvaluationNum;
    }

    public Integer getProtectiveDesignNum() {
        return protectiveDesignNum;
    }

    public void setProtectiveDesignNum(Integer protectiveDesignNum) {
        this.protectiveDesignNum = protectiveDesignNum;
    }

    public Integer getCompletionAcceptanceNum() {
        return completionAcceptanceNum;
    }

    public void setCompletionAcceptanceNum(Integer completionAcceptanceNum) {
        this.completionAcceptanceNum = completionAcceptanceNum;
    }

    public Long getDayDiff() {
        return dayDiff;
    }

    public void setDayDiff(Long dayDiff) {
        this.dayDiff = dayDiff;
    }
}