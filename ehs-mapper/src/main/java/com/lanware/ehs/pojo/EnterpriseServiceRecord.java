package com.lanware.ehs.pojo;

import java.util.Date;

public class EnterpriseServiceRecord {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.enterprise_id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private Long enterpriseId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.equipment_id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private Long equipmentId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.time_limit
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private String timeLimit;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.service_date
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private Date serviceDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.reason
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private String reason;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.result
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private String result;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.service_personnel
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private String servicePersonnel;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.service_leader
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private String serviceLeader;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.gmt_create
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private Date gmtCreate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column enterprise_service_record.gmt_modified
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    private Date gmtModified;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.id
     *
     * @return the value of enterprise_service_record.id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.id
     *
     * @param id the value for enterprise_service_record.id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.enterprise_id
     *
     * @return the value of enterprise_service_record.enterprise_id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public Long getEnterpriseId() {
        return enterpriseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.enterprise_id
     *
     * @param enterpriseId the value for enterprise_service_record.enterprise_id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.equipment_id
     *
     * @return the value of enterprise_service_record.equipment_id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public Long getEquipmentId() {
        return equipmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.equipment_id
     *
     * @param equipmentId the value for enterprise_service_record.equipment_id
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.time_limit
     *
     * @return the value of enterprise_service_record.time_limit
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public String getTimeLimit() {
        return timeLimit;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.time_limit
     *
     * @param timeLimit the value for enterprise_service_record.time_limit
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit == null ? null : timeLimit.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.service_date
     *
     * @return the value of enterprise_service_record.service_date
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public Date getServiceDate() {
        return serviceDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.service_date
     *
     * @param serviceDate the value for enterprise_service_record.service_date
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setServiceDate(Date serviceDate) {
        this.serviceDate = serviceDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.reason
     *
     * @return the value of enterprise_service_record.reason
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public String getReason() {
        return reason;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.reason
     *
     * @param reason the value for enterprise_service_record.reason
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.result
     *
     * @return the value of enterprise_service_record.result
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public String getResult() {
        return result;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.result
     *
     * @param result the value for enterprise_service_record.result
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.service_personnel
     *
     * @return the value of enterprise_service_record.service_personnel
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public String getServicePersonnel() {
        return servicePersonnel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.service_personnel
     *
     * @param servicePersonnel the value for enterprise_service_record.service_personnel
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setServicePersonnel(String servicePersonnel) {
        this.servicePersonnel = servicePersonnel == null ? null : servicePersonnel.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.service_leader
     *
     * @return the value of enterprise_service_record.service_leader
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public String getServiceLeader() {
        return serviceLeader;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.service_leader
     *
     * @param serviceLeader the value for enterprise_service_record.service_leader
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setServiceLeader(String serviceLeader) {
        this.serviceLeader = serviceLeader == null ? null : serviceLeader.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.gmt_create
     *
     * @return the value of enterprise_service_record.gmt_create
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.gmt_create
     *
     * @param gmtCreate the value for enterprise_service_record.gmt_create
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column enterprise_service_record.gmt_modified
     *
     * @return the value of enterprise_service_record.gmt_modified
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column enterprise_service_record.gmt_modified
     *
     * @param gmtModified the value for enterprise_service_record.gmt_modified
     *
     * @mbg.generated Tue Oct 08 13:18:34 CST 2019
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}