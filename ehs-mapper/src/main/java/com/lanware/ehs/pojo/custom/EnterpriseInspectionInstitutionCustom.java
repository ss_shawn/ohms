package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;

import java.io.Serializable;

public class EnterpriseInspectionInstitutionCustom extends EnterpriseInspectionInstitution implements Serializable {
    private static final long serialVersionUID = 5921857331226752090L;
    /** 机构相关证书数目 */
    private Integer institutionCertificateNum;
    /** 被用到的检测机构数目 */
    private Integer checkInstitutionNum;
    /** 被用到的体检机构数目 */
    private Integer medicalInstitutionNum;

    public Integer getInstitutionCertificateNum() {
        return institutionCertificateNum;
    }

    public void setInstitutionCertificateNum(Integer institutionCertificateNum) {
        this.institutionCertificateNum = institutionCertificateNum;
    }

    public Integer getCheckInstitutionNum() {
        return checkInstitutionNum;
    }

    public void setCheckInstitutionNum(Integer checkInstitutionNum) {
        this.checkInstitutionNum = checkInstitutionNum;
    }

    public Integer getMedicalInstitutionNum() {
        return medicalInstitutionNum;
    }

    public void setMedicalInstitutionNum(Integer medicalInstitutionNum) {
        this.medicalInstitutionNum = medicalInstitutionNum;
    }
}
