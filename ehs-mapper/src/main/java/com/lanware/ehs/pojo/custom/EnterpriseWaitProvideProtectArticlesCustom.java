package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseWaitProvideProtectArticles;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnterpriseWaitProvideProtectArticlesCustom extends EnterpriseWaitProvideProtectArticles implements Serializable {

    //未发放
    public static final Integer STATUS_UNISSUED = 0;

    //已发放
    public static final Integer STATUS_ISSUED = 1;


    private Long employeePostId;//员工ID
    private String employeeName;//员工名称
    private Long employeeId;//员工ID
    private Long postId;//岗位ID
    private String postName;//岗位名称
    private Date workDate;//上岗时间
    private String provideArticlesName;//发放物品名称
}