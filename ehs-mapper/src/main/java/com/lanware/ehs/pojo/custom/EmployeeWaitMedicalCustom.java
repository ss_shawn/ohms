package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EmployeeWaitMedical;

import java.io.Serializable;

/**
 * @description 待体检员工扩展pojo
 */
public class EmployeeWaitMedicalCustom extends EmployeeWaitMedical implements Serializable {
    private static final long serialVersionUID = -6666341680104931178L;
    // 状态：未发起体检
    public static final Integer STATUS_MEDICAL_NO = 0;
    // 状态：已发起体检
    public static final Integer STATUS_MEDICAL_YES = 1;
    // 状态：已删除体检
    public static final Integer STATUS_MEDICAL_DEL = 2;

    //类型：岗前
    public static final String TYPE_PRE_JOB = "岗前";

    //类型：离岗
    public static final String TYPE_LEAVE_POST = "离岗";

    //类型：复查
    public static final String TYPE_REVIEW = "复查";

    //类型：岗中
    public static final String TYPE_IN_POST = "岗中";



    /** 员工姓名 */
    private String employeeName;
    /** 岗位名称 */
    private String postName;

    /** 职工人数 */
    private Integer employeeNum;
    /** 应检人数 */
    private Integer shouldMedicalNum;
    /** 实检人数 */
    private Integer actualMedicalNum;
    /** 粉尘应检人数 */
    private Integer dustShouldMedicalNum;
    /** 粉尘实检人数 */
    private Integer dustActualMedicalNum;
    /** 化学因素应检人数 */
    private Integer chemicalShouldMedicalNum;
    /** 化学因素实检人数 */
    private Integer chemicalActualMedicalNum;
    /** 物理因素应检人数 */
    private Integer physicalShouldMedicalNum;
    /** 物理因素实检人数 */
    private Integer physicalActualMedicalNum;
    /** 放射性因素应检人数 */
    private Integer radiationShouldMedicalNum;
    /** 放射性因素实检人数 */
    private Integer radiationActualMedicalNum;
    /** 生物因素应检人数 */
    private Integer biologicalShouldMedicalNum;
    /** 生物因素实检人数 */
    private Integer biologicalActualMedicalNum;
    /** 其他因素应检人数 */
    private Integer otherShouldMedicalNum;
    /** 其他因素实检人数 */
    private Integer otherActualMedicalNum;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public Integer getEmployeeNum() {
        return employeeNum;
    }

    public void setEmployeeNum(Integer employeeNum) {
        this.employeeNum = employeeNum;
    }

    public Integer getShouldMedicalNum() {
        return shouldMedicalNum;
    }

    public void setShouldMedicalNum(Integer shouldMedicalNum) {
        this.shouldMedicalNum = shouldMedicalNum;
    }

    public Integer getActualMedicalNum() {
        return actualMedicalNum;
    }

    public void setActualMedicalNum(Integer actualMedicalNum) {
        this.actualMedicalNum = actualMedicalNum;
    }

    public Integer getDustShouldMedicalNum() {
        return dustShouldMedicalNum;
    }

    public void setDustShouldMedicalNum(Integer dustShouldMedicalNum) {
        this.dustShouldMedicalNum = dustShouldMedicalNum;
    }

    public Integer getDustActualMedicalNum() {
        return dustActualMedicalNum;
    }

    public void setDustActualMedicalNum(Integer dustActualMedicalNum) {
        this.dustActualMedicalNum = dustActualMedicalNum;
    }

    public Integer getChemicalShouldMedicalNum() {
        return chemicalShouldMedicalNum;
    }

    public void setChemicalShouldMedicalNum(Integer chemicalShouldMedicalNum) {
        this.chemicalShouldMedicalNum = chemicalShouldMedicalNum;
    }

    public Integer getChemicalActualMedicalNum() {
        return chemicalActualMedicalNum;
    }

    public void setChemicalActualMedicalNum(Integer chemicalActualMedicalNum) {
        this.chemicalActualMedicalNum = chemicalActualMedicalNum;
    }

    public Integer getPhysicalShouldMedicalNum() {
        return physicalShouldMedicalNum;
    }

    public void setPhysicalShouldMedicalNum(Integer physicalShouldMedicalNum) {
        this.physicalShouldMedicalNum = physicalShouldMedicalNum;
    }

    public Integer getPhysicalActualMedicalNum() {
        return physicalActualMedicalNum;
    }

    public void setPhysicalActualMedicalNum(Integer physicalActualMedicalNum) {
        this.physicalActualMedicalNum = physicalActualMedicalNum;
    }

    public Integer getRadiationShouldMedicalNum() {
        return radiationShouldMedicalNum;
    }

    public void setRadiationShouldMedicalNum(Integer radiationShouldMedicalNum) {
        this.radiationShouldMedicalNum = radiationShouldMedicalNum;
    }

    public Integer getRadiationActualMedicalNum() {
        return radiationActualMedicalNum;
    }

    public void setRadiationActualMedicalNum(Integer radiationActualMedicalNum) {
        this.radiationActualMedicalNum = radiationActualMedicalNum;
    }

    public Integer getBiologicalShouldMedicalNum() {
        return biologicalShouldMedicalNum;
    }

    public void setBiologicalShouldMedicalNum(Integer biologicalShouldMedicalNum) {
        this.biologicalShouldMedicalNum = biologicalShouldMedicalNum;
    }

    public Integer getBiologicalActualMedicalNum() {
        return biologicalActualMedicalNum;
    }

    public void setBiologicalActualMedicalNum(Integer biologicalActualMedicalNum) {
        this.biologicalActualMedicalNum = biologicalActualMedicalNum;
    }

    public Integer getOtherShouldMedicalNum() {
        return otherShouldMedicalNum;
    }

    public void setOtherShouldMedicalNum(Integer otherShouldMedicalNum) {
        this.otherShouldMedicalNum = otherShouldMedicalNum;
    }

    public Integer getOtherActualMedicalNum() {
        return otherActualMedicalNum;
    }

    public void setOtherActualMedicalNum(Integer otherActualMedicalNum) {
        this.otherActualMedicalNum = otherActualMedicalNum;
    }
}
