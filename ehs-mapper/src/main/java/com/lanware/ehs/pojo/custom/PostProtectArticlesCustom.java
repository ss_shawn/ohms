package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.PostProtectArticles;

import java.io.Serializable;
import java.util.Date;

public class PostProtectArticlesCustom extends PostProtectArticles implements Serializable {

    private Long enterpriseId;

    private String protectArticlesName;

    private String protectArticlesModel;

    private String protectArticlesHarmFactorId;

    private String protectArticlesDescription;

    private String protectArticlesImage;

    private Date protectArticlesGmtCreate;

    private Date protectArticlesGmtModified;

    public Long getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public String getProtectArticlesName() {
        return protectArticlesName;
    }

    public void setProtectArticlesName(String protectArticlesName) {
        this.protectArticlesName = protectArticlesName;
    }

    public String getProtectArticlesModel() {
        return protectArticlesModel;
    }

    public void setProtectArticlesModel(String protectArticlesModel) {
        this.protectArticlesModel = protectArticlesModel;
    }

    public String getProtectArticlesHarmFactorId() {
        return protectArticlesHarmFactorId;
    }

    public void setProtectArticlesHarmFactorId(String protectArticlesHarmFactorId) {
        this.protectArticlesHarmFactorId = protectArticlesHarmFactorId;
    }

    public String getProtectArticlesDescription() {
        return protectArticlesDescription;
    }

    public void setProtectArticlesDescription(String protectArticlesDescription) {
        this.protectArticlesDescription = protectArticlesDescription;
    }

    public String getProtectArticlesImage() {
        return protectArticlesImage;
    }

    public void setProtectArticlesImage(String protectArticlesImage) {
        this.protectArticlesImage = protectArticlesImage;
    }

    public Date getProtectArticlesGmtCreate() {
        return protectArticlesGmtCreate;
    }

    public void setProtectArticlesGmtCreate(Date protectArticlesGmtCreate) {
        this.protectArticlesGmtCreate = protectArticlesGmtCreate;
    }

    public Date getProtectArticlesGmtModified() {
        return protectArticlesGmtModified;
    }

    public void setProtectArticlesGmtModified(Date protectArticlesGmtModified) {
        this.protectArticlesGmtModified = protectArticlesGmtModified;
    }
}
