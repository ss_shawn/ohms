package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseOutsideCheck;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 企业外部检测扩展类
 */
@Data
public class EnterpriseOutsideCheckCustom extends EnterpriseOutsideCheck implements Serializable {
    private static final long serialVersionUID = -7971175843059150614L;
    /** 危害因素检测结果数目 */
    private Integer checkResultNum;
    /** 检测机构名称 */
    private String checkOrganizationName;
    /** 检测危害因素名称集合 */
    private String checkHarmFactorName;
}
