package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseDiseaseDiagnosiNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotify;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnterpriseDiseaseDiagnosiNotifyCustom extends EnterpriseDiseaseDiagnosiNotify implements Serializable {
    private static final long serialVersionUID = 5953750617026343843L;
    private String employeeName;//员工名称
    private Date diagnosiDate;//体检时间
    private String hospital;//医院名称
    private String name;//职业病名称
    private Long diseaseDiagnosiWaitId;//待告知ID


    // 状态：未告知
    public static final Integer STATUS_NOTIFY_NO = 0;
    // 状态：已告知
    public static final Integer STATUS_NOTIFY_YES = 1;
}