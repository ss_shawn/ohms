package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.EnterpriseFile;

import java.io.Serializable;

/**
 * @description 往来文件扩展类
 */
public class EnterpriseFileCustom extends EnterpriseFile implements Serializable {
    private static final long serialVersionUID = -4706068513786093136L;
    /** 往来文件数目 */
    private Integer correspondFileNum;

    public Integer getCorrespondFileNum() {
        return correspondFileNum;
    }

    public void setCorrespondFileNum(Integer correspondFileNum) {
        this.correspondFileNum = correspondFileNum;
    }
}