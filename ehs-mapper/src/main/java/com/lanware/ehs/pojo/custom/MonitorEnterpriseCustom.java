package com.lanware.ehs.pojo.custom;

import com.lanware.ehs.pojo.MonitorEnterprise;

import java.util.Date;

public class MonitorEnterpriseCustom extends MonitorEnterprise {

    private String enterpriseName;

    private String enterpriseHarmDegree;

    private Integer enterpriseIsHighlyToxic;

    private String enterpriseCity;

    private Date enterpriseBuildDate;

    private String enterpriseAddress;

    private String enterpriseRepresentName;

    private String enterpriseRepresentPhone;

    private String enterpriseDutyName;

    private String enterpriseDutyPhone;

    private Integer enterpriseHarmTotalNumber;

    private Integer enterpriseHarmWomanNumber;

    private Integer enterpriseHarmFarmerNumber;

    private String enterpriseRegisterType;

    private String enterpriseIcon;

    private String enterpriseHintPicture;

    private Integer enterpriseUpdateFrequency;

    private String enterpriseUpdateWay;

    private Date enterpriseLastUseTime;

    private Date enterpriseLastUpdateTime;

    private Integer enterpriseStatus;

    private Date enterpriseGmtCreate;

    private Date enterpriseGmtModified;

    private Integer enterpriseIsOverdue;

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getEnterpriseHarmDegree() {
        return enterpriseHarmDegree;
    }

    public void setEnterpriseHarmDegree(String enterpriseHarmDegree) {
        this.enterpriseHarmDegree = enterpriseHarmDegree;
    }

    public Integer getEnterpriseIsHighlyToxic() {
        return enterpriseIsHighlyToxic;
    }

    public void setEnterpriseIsHighlyToxic(Integer enterpriseIsHighlyToxic) {
        this.enterpriseIsHighlyToxic = enterpriseIsHighlyToxic;
    }

    public String getEnterpriseCity() {
        return enterpriseCity;
    }

    public void setEnterpriseCity(String enterpriseCity) {
        this.enterpriseCity = enterpriseCity;
    }

    public Date getEnterpriseBuildDate() {
        return enterpriseBuildDate;
    }

    public void setEnterpriseBuildDate(Date enterpriseBuildDate) {
        this.enterpriseBuildDate = enterpriseBuildDate;
    }

    public String getEnterpriseAddress() {
        return enterpriseAddress;
    }

    public void setEnterpriseAddress(String enterpriseAddress) {
        this.enterpriseAddress = enterpriseAddress;
    }

    public String getEnterpriseRepresentName() {
        return enterpriseRepresentName;
    }

    public void setEnterpriseRepresentName(String enterpriseRepresentName) {
        this.enterpriseRepresentName = enterpriseRepresentName;
    }

    public String getEnterpriseRepresentPhone() {
        return enterpriseRepresentPhone;
    }

    public void setEnterpriseRepresentPhone(String enterpriseRepresentPhone) {
        this.enterpriseRepresentPhone = enterpriseRepresentPhone;
    }

    public String getEnterpriseDutyName() {
        return enterpriseDutyName;
    }

    public void setEnterpriseDutyName(String enterpriseDutyName) {
        this.enterpriseDutyName = enterpriseDutyName;
    }

    public String getEnterpriseDutyPhone() {
        return enterpriseDutyPhone;
    }

    public void setEnterpriseDutyPhone(String enterpriseDutyPhone) {
        this.enterpriseDutyPhone = enterpriseDutyPhone;
    }

    public Integer getEnterpriseHarmTotalNumber() {
        return enterpriseHarmTotalNumber;
    }

    public void setEnterpriseHarmTotalNumber(Integer enterpriseHarmTotalNumber) {
        this.enterpriseHarmTotalNumber = enterpriseHarmTotalNumber;
    }

    public Integer getEnterpriseHarmWomanNumber() {
        return enterpriseHarmWomanNumber;
    }

    public void setEnterpriseHarmWomanNumber(Integer enterpriseHarmWomanNumber) {
        this.enterpriseHarmWomanNumber = enterpriseHarmWomanNumber;
    }

    public Integer getEnterpriseHarmFarmerNumber() {
        return enterpriseHarmFarmerNumber;
    }

    public void setEnterpriseHarmFarmerNumber(Integer enterpriseHarmFarmerNumber) {
        this.enterpriseHarmFarmerNumber = enterpriseHarmFarmerNumber;
    }

    public String getEnterpriseRegisterType() {
        return enterpriseRegisterType;
    }

    public void setEnterpriseRegisterType(String enterpriseRegisterType) {
        this.enterpriseRegisterType = enterpriseRegisterType;
    }

    public String getEnterpriseIcon() {
        return enterpriseIcon;
    }

    public void setEnterpriseIcon(String enterpriseIcon) {
        this.enterpriseIcon = enterpriseIcon;
    }

    public String getEnterpriseHintPicture() {
        return enterpriseHintPicture;
    }

    public void setEnterpriseHintPicture(String enterpriseHintPicture) {
        this.enterpriseHintPicture = enterpriseHintPicture;
    }

    public Integer getEnterpriseUpdateFrequency() {
        return enterpriseUpdateFrequency;
    }

    public void setEnterpriseUpdateFrequency(Integer enterpriseUpdateFrequency) {
        this.enterpriseUpdateFrequency = enterpriseUpdateFrequency;
    }

    public String getEnterpriseUpdateWay() {
        return enterpriseUpdateWay;
    }

    public void setEnterpriseUpdateWay(String enterpriseUpdateWay) {
        this.enterpriseUpdateWay = enterpriseUpdateWay;
    }

    public Date getEnterpriseLastUseTime() {
        return enterpriseLastUseTime;
    }

    public void setEnterpriseLastUseTime(Date enterpriseLastUseTime) {
        this.enterpriseLastUseTime = enterpriseLastUseTime;
    }

    public Date getEnterpriseLastUpdateTime() {
        return enterpriseLastUpdateTime;
    }

    public void setEnterpriseLastUpdateTime(Date enterpriseLastUpdateTime) {
        this.enterpriseLastUpdateTime = enterpriseLastUpdateTime;
    }

    public Integer getEnterpriseStatus() {
        return enterpriseStatus;
    }

    public void setEnterpriseStatus(Integer enterpriseStatus) {
        this.enterpriseStatus = enterpriseStatus;
    }

    public Date getEnterpriseGmtCreate() {
        return enterpriseGmtCreate;
    }

    public void setEnterpriseGmtCreate(Date enterpriseGmtCreate) {
        this.enterpriseGmtCreate = enterpriseGmtCreate;
    }

    public Date getEnterpriseGmtModified() {
        return enterpriseGmtModified;
    }

    public void setEnterpriseGmtModified(Date enterpriseGmtModified) {
        this.enterpriseGmtModified = enterpriseGmtModified;
    }

    public Integer getEnterpriseIsOverdue() {
        return enterpriseIsOverdue;
    }

    public void setEnterpriseIsOverdue(Integer enterpriseIsOverdue) {
        this.enterpriseIsOverdue = enterpriseIsOverdue;
    }
}
