/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50639
Source Host           : localhost:3306
Source Database       : zhjk

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2019-12-04 09:20:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_harm_factor
-- ----------------------------
DROP TABLE IF EXISTS `base_harm_factor`;
CREATE TABLE `base_harm_factor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `cas` varchar(100) DEFAULT NULL,
  `is_highly_toxic` int(11) DEFAULT NULL COMMENT '0:不是高毒  1：是高毒',
  `mac` decimal(10,3) DEFAULT NULL,
  `twa` decimal(10,3) DEFAULT NULL,
  `stel` decimal(10,3) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `over_limit_multiple` decimal(4,1) DEFAULT NULL COMMENT '超限倍数',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1217 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_harm_factor
-- ----------------------------

-- ----------------------------
-- Table structure for base_medical_type
-- ----------------------------
DROP TABLE IF EXISTS `base_medical_type`;
CREATE TABLE `base_medical_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_medical_type
-- ----------------------------
INSERT INTO `base_medical_type` VALUES ('1', '岗前');
INSERT INTO `base_medical_type` VALUES ('2', '离岗');
INSERT INTO `base_medical_type` VALUES ('3', '岗中');
INSERT INTO `base_medical_type` VALUES ('4', '离岗后医学随访');
INSERT INTO `base_medical_type` VALUES ('5', '医学观察');

-- ----------------------------
-- Table structure for check_plan_harm_factor
-- ----------------------------
DROP TABLE IF EXISTS `check_plan_harm_factor`;
CREATE TABLE `check_plan_harm_factor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `check_plan_id` bigint(20) DEFAULT NULL COMMENT '检测计划id',
  `harm_factor_id` bigint(20) DEFAULT NULL COMMENT '检测危害因素id',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of check_plan_harm_factor
-- ----------------------------

-- ----------------------------
-- Table structure for employee_disease_diagnosi
-- ----------------------------
DROP TABLE IF EXISTS `employee_disease_diagnosi`;
CREATE TABLE `employee_disease_diagnosi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `medical_id` bigint(20) DEFAULT NULL,
  `diagnosi_date` datetime DEFAULT NULL,
  `is_certain` int(11) DEFAULT NULL COMMENT '1：是 0：否',
  `is_taboo` int(11) DEFAULT NULL COMMENT '1：是 0：否',
  `is_transferred` int(11) DEFAULT NULL COMMENT '1：是 0：否',
  `name` varchar(200) DEFAULT NULL,
  `hospital` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `medical_certificate` varchar(200) DEFAULT NULL,
  `diagnosi_level` varchar(100) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_disease_diagnosi
-- ----------------------------

-- ----------------------------
-- Table structure for employee_harm_contact_history
-- ----------------------------
DROP TABLE IF EXISTS `employee_harm_contact_history`;
CREATE TABLE `employee_harm_contact_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_name` varchar(100) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `employee_post_id` bigint(20) DEFAULT NULL,
  `post_name` varchar(200) DEFAULT NULL,
  `harm_factor_name` varchar(200) DEFAULT NULL,
  `protect_articles_name` varchar(200) DEFAULT NULL,
  `protective_measures_name` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0:手动生成 1：自动生成',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_harm_contact_history
-- ----------------------------

-- ----------------------------
-- Table structure for employee_medical
-- ----------------------------
DROP TABLE IF EXISTS `employee_medical`;
CREATE TABLE `employee_medical` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `institution_id` bigint(20) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT '岗前，岗中，离岗，职业病诊断，其它，离岗后医学随访，复查，医学观察',
  `result` varchar(200) DEFAULT NULL,
  `medical_date` datetime DEFAULT NULL,
  `problem_description` varchar(200) DEFAULT NULL,
  `personnel_handling` varchar(200) DEFAULT NULL,
  `scene_handing` varchar(200) DEFAULT NULL,
  `medical_report` varchar(200) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `is_transferred` int(11) DEFAULT NULL COMMENT '1：是 0：否',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_medical
-- ----------------------------

-- ----------------------------
-- Table structure for employee_medical_history
-- ----------------------------
DROP TABLE IF EXISTS `employee_medical_history`;
CREATE TABLE `employee_medical_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `medical_id` bigint(20) DEFAULT NULL COMMENT '体检id',
  `disease_name` varchar(100) DEFAULT NULL,
  `diagnosi_date` datetime DEFAULT NULL,
  `diagnosi_hosipital` varchar(100) DEFAULT NULL,
  `diagnosi_report` varchar(100) DEFAULT NULL,
  `diagnosi_result` varchar(100) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `disease_type` int(11) DEFAULT NULL COMMENT '0职业病 1禁忌症',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_medical_history
-- ----------------------------

-- ----------------------------
-- Table structure for employee_post
-- ----------------------------
DROP TABLE IF EXISTS `employee_post`;
CREATE TABLE `employee_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `work_date` datetime DEFAULT NULL,
  `leave_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_post
-- ----------------------------

-- ----------------------------
-- Table structure for employee_wait_medical
-- ----------------------------
DROP TABLE IF EXISTS `employee_wait_medical`;
CREATE TABLE `employee_wait_medical` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `medical_id` bigint(20) DEFAULT NULL,
  `review_id` bigint(20) DEFAULT NULL COMMENT '生成这条待体检记录的复查类型的体检记录id',
  `work_date` datetime DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT '岗前、离岗',
  `status` int(11) DEFAULT '0' COMMENT '状态：0 未发起体检 1已发起体检',
  `wait_metical_date` varchar(50) DEFAULT NULL,
  `reason` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=537 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_wait_medical
-- ----------------------------

-- ----------------------------
-- Table structure for employee_work_notify
-- ----------------------------
DROP TABLE IF EXISTS `employee_work_notify`;
CREATE TABLE `employee_work_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `employee_post_id` bigint(100) DEFAULT NULL,
  `notify_file` varchar(100) DEFAULT NULL,
  `notify_date` datetime DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_work_notify
-- ----------------------------

-- ----------------------------
-- Table structure for employee_work_wait_notify
-- ----------------------------
DROP TABLE IF EXISTS `employee_work_wait_notify`;
CREATE TABLE `employee_work_wait_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `employee_post_id` bigint(100) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '状态（0，未告知，1，已告知）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee_work_wait_notify
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_accident_handle
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_accident_handle`;
CREATE TABLE `enterprise_accident_handle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `accident_date` datetime DEFAULT NULL,
  `accident_reason` varchar(500) DEFAULT NULL,
  `handle_process` varchar(500) DEFAULT NULL,
  `responsibility_analysis` varchar(500) DEFAULT NULL,
  `duty_person_handle` varchar(500) DEFAULT NULL,
  `measures` varchar(500) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_accident_handle
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_auth
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_auth`;
CREATE TABLE `enterprise_auth` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_auth
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_baseinfo
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_baseinfo`;
CREATE TABLE `enterprise_baseinfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `harm_degree` varchar(50) DEFAULT NULL,
  `is_highly_toxic` int(11) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `build_date` datetime DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `represent_name` varchar(50) DEFAULT NULL,
  `represent_phone` varchar(20) DEFAULT NULL,
  `duty_name` varchar(50) DEFAULT NULL,
  `duty_phone` varchar(20) DEFAULT NULL,
  `harm_total_number` int(11) DEFAULT NULL,
  `harm_woman_number` int(11) DEFAULT NULL,
  `harm_farmer_number` int(11) DEFAULT NULL,
  `register_type` varchar(100) DEFAULT NULL,
  `icon` varchar(500) DEFAULT NULL,
  `hint_picture` varchar(500) DEFAULT NULL,
  `update_frequency` int(11) DEFAULT NULL,
  `update_way` varchar(20) DEFAULT NULL,
  `last_use_time` datetime DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sync_auth` int(11) DEFAULT NULL COMMENT '同步局端  0：否  1：是',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_baseinfo
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_check_result_detail
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_check_result_detail`;
CREATE TABLE `enterprise_check_result_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `check_result_id` bigint(20) DEFAULT NULL,
  `contact_time` decimal(4,1) DEFAULT NULL COMMENT '接触时间',
  `check_result` decimal(20,6) DEFAULT NULL COMMENT '检测结果',
  `unit` varchar(20) DEFAULT NULL COMMENT '单位',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_check_result_detail
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_declare
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_declare`;
CREATE TABLE `enterprise_declare` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `declare_date` datetime DEFAULT NULL,
  `declare_attachment` varchar(200) DEFAULT NULL,
  `declare_type` varchar(200) DEFAULT NULL COMMENT '初次申报，变更申报',
  `receipt_date` datetime DEFAULT NULL,
  `receipt_attachment` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_declare
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_declare_workplace
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_declare_workplace`;
CREATE TABLE `enterprise_declare_workplace` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `declare_id` bigint(20) DEFAULT NULL,
  `harm_factor_id` bigint(20) DEFAULT NULL,
  `workplace_id` char(10) DEFAULT NULL,
  `workplace_name` varchar(100) DEFAULT NULL,
  `harm_factor_name` varchar(100) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_declare_workplace
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_disease_diagnosi_notify
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_disease_diagnosi_notify`;
CREATE TABLE `enterprise_disease_diagnosi_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `diagnosi_id` bigint(20) DEFAULT NULL,
  `notify_file` varchar(200) DEFAULT NULL,
  `notify_date` datetime DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_disease_diagnosi_notify
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_disease_diagnosi_wait_notify
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_disease_diagnosi_wait_notify`;
CREATE TABLE `enterprise_disease_diagnosi_wait_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `diagnosi_id` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1:已告知  0待告知',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_disease_diagnosi_wait_notify
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_document_backup
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_document_backup`;
CREATE TABLE `enterprise_document_backup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `backup_time` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `file_path` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_document_backup
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_emergency_plan
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_emergency_plan`;
CREATE TABLE `enterprise_emergency_plan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `file` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_emergency_plan
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_employee
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_employee`;
CREATE TABLE `enterprise_employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `electron_archive_number` varchar(20) DEFAULT NULL,
  `archive_number` varchar(20) DEFAULT NULL,
  `attend_work_time` datetime DEFAULT NULL,
  `build_time` datetime DEFAULT NULL,
  `hobby` varchar(100) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `native_place` varchar(20) DEFAULT NULL,
  `is_married` int(11) DEFAULT NULL COMMENT '1:是 0否',
  `is_migrant_worker` int(11) DEFAULT NULL COMMENT '1:是 0否',
  `education_degree` varchar(20) DEFAULT NULL,
  `id_card` varchar(20) DEFAULT NULL,
  `induction_date` datetime DEFAULT NULL,
  `leave_date` datetime DEFAULT NULL,
  `health_entrust_file` varchar(200) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `job_number` varchar(20) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL COMMENT '0:男 1:女',
  `onduty_id` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_employee
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_equipment_use
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_equipment_use`;
CREATE TABLE `enterprise_equipment_use` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `equipment_id` bigint(20) DEFAULT NULL,
  `use_date` datetime DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_equipment_use
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_file
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_file`;
CREATE TABLE `enterprise_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `file_number` varchar(100) DEFAULT NULL,
  `transm_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `transm_unit` varchar(100) DEFAULT NULL,
  `transm_person` varchar(100) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_file
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_hygiene_org
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_hygiene_org`;
CREATE TABLE `enterprise_hygiene_org` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `job` varchar(200) DEFAULT NULL,
  `duty` varchar(200) DEFAULT NULL,
  `department` varchar(200) DEFAULT NULL,
  `is_fulltime` int(11) DEFAULT NULL COMMENT '1:全职  0:不是全职',
  `employ_file` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_hygiene_org
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_inside_monitor
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_inside_monitor`;
CREATE TABLE `enterprise_inside_monitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `monitor_date` datetime DEFAULT NULL,
  `monitor_point_id` bigint(20) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL,
  `harm_factor_id` bigint(20) DEFAULT NULL,
  `limit_value` varchar(20) DEFAULT NULL,
  `monitor_period` varchar(20) DEFAULT NULL,
  `monitor_result` varchar(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_inside_monitor
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_inspection
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_inspection`;
CREATE TABLE `enterprise_inspection` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `check_date` datetime DEFAULT NULL,
  `check_nature` varchar(200) DEFAULT NULL,
  `check_dept` varchar(200) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `registration_sheet` varchar(200) DEFAULT NULL,
  `is_rectified` int(11) DEFAULT NULL COMMENT '1：是 0：否',
  `rectification_deadline` datetime DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_inspection
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_inspection_institution
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_inspection_institution`;
CREATE TABLE `enterprise_inspection_institution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `contacts` varchar(30) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  `contract` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_inspection_institution
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_inspection_rectification
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_inspection_rectification`;
CREATE TABLE `enterprise_inspection_rectification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inspection_id` bigint(20) DEFAULT NULL,
  `rectification_date` datetime DEFAULT NULL,
  `department` varchar(200) DEFAULT NULL,
  `content` varchar(200) DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `file_path` varchar(500) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_inspection_rectification
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_manage_file
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_manage_file`;
CREATE TABLE `enterprise_manage_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `module` int(11) DEFAULT NULL COMMENT '0：各级责任制  1：职业健康防范制度',
  `file_type` varchar(200) DEFAULT NULL COMMENT '对应的具体文件名称（初始化时固定好）',
  `file_name` varchar(200) DEFAULT NULL,
  `file_path` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `enterprise_id` (`enterprise_id`,`module`,`file_type`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COMMENT='用于存放各级责任制文件和健康防治制度文件';

-- ----------------------------
-- Records of enterprise_manage_file
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_medical_result_notify
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_medical_result_notify`;
CREATE TABLE `enterprise_medical_result_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `medical_id` bigint(20) DEFAULT NULL,
  `notify_file` varchar(200) DEFAULT NULL,
  `notify_date` datetime DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_medical_result_notify
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_medical_result_wait_notify
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_medical_result_wait_notify`;
CREATE TABLE `enterprise_medical_result_wait_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `medical_id` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '1:已告知  0待告知',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_medical_result_wait_notify
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_monitor_point
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_monitor_point`;
CREATE TABLE `enterprise_monitor_point` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '监测点名称',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_monitor_point
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_outside_check
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_outside_check`;
CREATE TABLE `enterprise_outside_check` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `check_date` datetime DEFAULT NULL,
  `check_organization_id` bigint(20) DEFAULT NULL,
  `check_organization` varchar(100) DEFAULT NULL,
  `check_report` varchar(100) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_outside_check
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_outside_check_result
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_outside_check_result`;
CREATE TABLE `enterprise_outside_check_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `outside_check_id` bigint(20) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL COMMENT '工作场所id',
  `monitor_point_id` bigint(20) DEFAULT NULL COMMENT '监测点id',
  `harm_factor_id` bigint(20) DEFAULT NULL COMMENT '检测危害因素id',
  `is_qualified` varchar(20) DEFAULT NULL COMMENT '合格/不合格',
  `status` int(11) DEFAULT NULL COMMENT '0:未取消检测  1:取消检测',
  `cancel_reason` varchar(200) DEFAULT NULL COMMENT '取消原因',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1057 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_outside_check_result
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_post
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_post`;
CREATE TABLE `enterprise_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `dept_name` varchar(200) DEFAULT NULL,
  `post_name` varchar(200) DEFAULT NULL,
  `operation_rule` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_post
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_post_workplace_relation
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_post_workplace_relation`;
CREATE TABLE `enterprise_post_workplace_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(11) DEFAULT NULL,
  `workplace_id` bigint(11) DEFAULT NULL,
  `post_id` bigint(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_post_workplace_relation
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_protective_measures
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_protective_measures`;
CREATE TABLE `enterprise_protective_measures` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `equipment_name` varchar(200) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL,
  `operation_rule` varchar(200) DEFAULT NULL,
  `acceptance_certificate` varchar(200) DEFAULT NULL,
  `qualification_record` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_protective_measures
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_protect_articles
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_protect_articles`;
CREATE TABLE `enterprise_protect_articles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `model` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `certificate_name` varchar(200) DEFAULT NULL,
  `certificate_path` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_protect_articles
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_protect_harm_factor_relation
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_protect_harm_factor_relation`;
CREATE TABLE `enterprise_protect_harm_factor_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `protect_id` bigint(20) DEFAULT NULL,
  `harm_factor_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_protect_harm_factor_relation
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_provide_protect_articles
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_provide_protect_articles`;
CREATE TABLE `enterprise_provide_protect_articles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `employee_post_id` bigint(20) DEFAULT NULL,
  `provide_articles_id` bigint(20) DEFAULT NULL,
  `provide_date` datetime DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `signature_record` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_provide_protect_articles
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_relation_file
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_relation_file`;
CREATE TABLE `enterprise_relation_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `relation_id` bigint(20) DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `file_path` varchar(200) DEFAULT NULL,
  `file_type` varchar(50) DEFAULT NULL,
  `relation_module` varchar(200) DEFAULT NULL COMMENT '主要针对各个模块',
  `description` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_relation_file
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_remind
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_remind`;
CREATE TABLE `enterprise_remind` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL COMMENT '企业id',
  `type` varchar(50) DEFAULT NULL COMMENT '提醒类别',
  `content` varchar(500) DEFAULT NULL COMMENT '提醒内容',
  `plan_date` varchar(20) DEFAULT NULL COMMENT '提醒日期',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_remind
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_resource
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_resource`;
CREATE TABLE `enterprise_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `icon` varchar(200) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `perms` varchar(100) DEFAULT NULL,
  `is_show` int(11) DEFAULT NULL COMMENT '1显示 0不显示',
  `type` int(11) DEFAULT NULL COMMENT '0:菜单  1功能',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40002 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_resource
-- ----------------------------
INSERT INTO `enterprise_resource` VALUES ('100', '企业信息', null, '0', 'layui-icon-home', '1', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('101', '基本信息', '/basicInfo', '100', null, '1', 'basicInfo:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('102', '年度计划', '/yearplan', '100', null, '2', 'yearplan:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('103', '管理组织', '/hygieneOrg', '100', null, '3', 'hygieneOrg:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('104', '防范制度', '/prevention', '100', null, '4', 'prevention:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('105', '责任制文件', '/responsibility', '100', null, '5', 'responsibility:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('106', '往来文件', '/file', '100', null, '6', 'file:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('200', '作业场所管理', null, '0', 'layui-icon-bank', '2', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('201', '作业场所信息', '/workplace', '200', null, '1', 'workplace:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('202', '危害申报', '/declare', '200', null, '2', 'declare:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('300', '防护用品管理', null, '0', 'layui-icon-template', '3', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('301', '防护用品信息', '/protect', '300', null, '1', 'protect:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('302', '发放记录', '/protectArticles', '300', null, '2', 'protectArticles:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('400', '防护设施管理', null, '0', 'layui-icon-component', '4', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('401', '防护设施信息', '/protectiveMeasure', '400', null, '1', 'protectiveMeasure:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('402', '维护记录', '/serviceRecord', '400', null, '2', 'serviceRecord:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('403', '运行记录', '/equipmentUse', '400', null, '3', 'equipmentUse:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('404', '防护设施维护检修计划', '/protectiveMeasurePlan', '400', '', '4', 'protectiveMeasurePlan:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('500', '服务机构信息', null, '0', 'layui-icon-auz', '5', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('501', '服务机构管理', '/inspectionInstitution', '500', null, '1', 'inspectionInstitution:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('600', '人员管理', null, '0', 'layui-icon-team', '6', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('601', '员工信息', '/employee', '600', null, '1', 'employee:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('602', '岗位管理', '/post', '600', null, '2', 'post:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('603', '岗前告知', '/workNotify', '600', null, '3', 'workNotify:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('700', '体检管理', null, '0', 'layui-icon-fileprotect', '7', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('701', '体检记录', '/medical', '700', null, '1', 'medical:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('702', '体检告知', '/medicalResultNotify', '700', null, '2', 'medicalResultNotify:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('703', '职业病诊断告知', '/diseaseDiagnosiNotify', '700', null, '3', 'diseaseDiagnosiNotify:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('704', '体检计划', '/medicalPlan', '700', '', '4', 'medicalPlan:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('800', '危害因素检测', null, '0', 'layui-icon-check-square', '8', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('801', '检测计划', '/checkPlan', '800', '', '1', 'checkPlan:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('802', '监测点管理', '/monitorPoint', '800', '', '2', 'monitorPoint:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('803', '检测记录', '/check', '800', null, '3', 'check:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('804', '检测告知', '/workplaceNotify', '800', null, '4', 'workplaceNotify:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('900', '培训管理', null, '0', 'layui-icon-edit-square', '9', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('901', '培训记录', '/training', '900', null, '1', 'training:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('902', '培训计划', '/trainingPlan', '900', '', '2', 'trainingPlan:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('1000', '三同时管理', null, '0', 'layui-icon-template-1', '10', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('1001', '三同时记录', '/threeSimultaneity', '1000', null, '1', 'threeSimultaneity:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('2000', '日常检查整改', null, '0', 'layui-icon-list', '11', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('2001', '检查整改记录', '/inspection', '2000', null, '1', 'inspection:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('3000', '应急救援管理', null, '0', 'layui-icon-app', '12', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('3001', '演练记录', '/toxicDrill', '3000', null, '1', 'toxicDrill:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('3002', '事故调查处理', '/accidentHandle', '3000', null, '2', 'accidentHandle:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('3003', '应急救援演练计划', '/rescuePlan', '3000', '', '3', 'rescuePlan:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('4000', '12本台账', null, '0', 'layui-icon-form', '13', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('4001', ' 12本台账', '/account', '4000', '', '1', 'account:*', '1', '0', '2019-08-26 19:47:38', '2019-08-26 19:47:38');
INSERT INTO `enterprise_resource` VALUES ('5000', '系统管理', null, '0', 'layui-icon-set', '14', null, '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('5001', '用户管理', '/user', '5000', null, '1', 'user:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('5002', '权限管理', '/role', '5000', null, '2', 'role:*', '1', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('5003', '系统首页', '/index', '5000', '', '3', 'index:*', '0', '0', '2019-07-09 22:55:01', '2019-07-09 22:55:01');
INSERT INTO `enterprise_resource` VALUES ('5004', '图标', '/others/icon', '5000', '', null, '', '1', '0', '2019-10-31 17:12:22', '2019-10-31 17:12:26');

-- ----------------------------
-- Table structure for enterprise_role_resource_relation
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_role_resource_relation`;
CREATE TABLE `enterprise_role_resource_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `resource_id` bigint(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_role_resource_relation
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_service_record
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_service_record`;
CREATE TABLE `enterprise_service_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `equipment_id` bigint(20) DEFAULT NULL,
  `time_limit` varchar(200) DEFAULT NULL,
  `service_date` datetime DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `result` varchar(500) DEFAULT NULL,
  `service_personnel` varchar(100) DEFAULT NULL,
  `service_leader` varchar(100) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_service_record
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_three_simultaneity
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_three_simultaneity`;
CREATE TABLE `enterprise_three_simultaneity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `project_name` varchar(500) DEFAULT NULL,
  `project_source` varchar(200) DEFAULT NULL,
  `project_time` datetime DEFAULT NULL,
  `project_state` int(11) DEFAULT NULL COMMENT '0:预评价阶段 1:防护设施设计 2:竣工验收',
  `harm_degree` int(11) DEFAULT NULL COMMENT '0:一般 1:较重 2:严重',
  `risk_class` int(11) DEFAULT NULL COMMENT '0:一般 1:较重 2:严重',
  `status` int(11) DEFAULT NULL COMMENT '0:未完成 1:已完成',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_three_simultaneity
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_three_simultaneity_file
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_three_simultaneity_file`;
CREATE TABLE `enterprise_three_simultaneity_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) DEFAULT NULL,
  `file_name` varchar(500) DEFAULT NULL,
  `file_path` varchar(200) DEFAULT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `project_state` int(11) DEFAULT NULL COMMENT '0:预评价阶段 1:防护设施设计 2:竣工验收',
  `opinion` varchar(500) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_three_simultaneity_file
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_toxic_drill
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_toxic_drill`;
CREATE TABLE `enterprise_toxic_drill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `prove_file` varchar(200) DEFAULT NULL,
  `drill_date` datetime DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_toxic_drill
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_training
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_training`;
CREATE TABLE `enterprise_training` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '0 普通员工 1管理员',
  `training_date` datetime DEFAULT NULL,
  `duration` decimal(10,2) DEFAULT NULL,
  `place` varchar(200) DEFAULT NULL,
  `theme` varchar(200) DEFAULT NULL,
  `lecturer` varchar(200) DEFAULT NULL,
  `attendance_sheet` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_training
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_training_employee
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_training_employee`;
CREATE TABLE `enterprise_training_employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `training_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_training_employee
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_user
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_user`;
CREATE TABLE `enterprise_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1封禁   0不封禁',
  `is_deleted` int(11) DEFAULT NULL COMMENT '1删除  0不删除',
  `is_admin` int(11) DEFAULT NULL COMMENT '1是 0 不是',
  `is_tab` int(11) DEFAULT NULL,
  `theme` varchar(50) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_user
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_user_role
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_user_role`;
CREATE TABLE `enterprise_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `is_show` int(11) DEFAULT NULL COMMENT '1显示 0不显示',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_wait_check
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_wait_check`;
CREATE TABLE `enterprise_wait_check` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `outside_check_id` bigint(20) DEFAULT NULL COMMENT '外部检测id',
  `check_plan_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL COMMENT '年度',
  `check_month` int(11) DEFAULT NULL COMMENT '检测月份',
  `status` int(11) DEFAULT NULL COMMENT '状态：0 未生成检测记录 1已生成检测记录',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_wait_check
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_wait_provide_protect_articles
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_wait_provide_protect_articles`;
CREATE TABLE `enterprise_wait_provide_protect_articles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `protect_articles_id` bigint(20) DEFAULT NULL,
  `employee_post_id` bigint(20) DEFAULT NULL,
  `delete_cause` varchar(2000) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0未发放 1已发放',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_wait_provide_protect_articles
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_wait_rectification
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_wait_rectification`;
CREATE TABLE `enterprise_wait_rectification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inspection_id` bigint(20) DEFAULT NULL,
  `rectification_date` datetime DEFAULT NULL,
  `rectification_deadline` datetime DEFAULT NULL,
  `content` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_wait_rectification
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_workplace
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_workplace`;
CREATE TABLE `enterprise_workplace` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_workplace
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_workplacet_notify
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_workplacet_notify`;
CREATE TABLE `enterprise_workplacet_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL,
  `harm_warn_image` varchar(200) DEFAULT NULL,
  `check_result_prove` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_workplacet_notify
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_workplace_notify
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_workplace_notify`;
CREATE TABLE `enterprise_workplace_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL,
  `check_id` bigint(20) DEFAULT NULL,
  `notify_date` datetime DEFAULT NULL,
  `harm_warn_image` varchar(200) DEFAULT NULL,
  `check_result_prove` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_workplace_notify
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_workplace_wait_notify
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_workplace_wait_notify`;
CREATE TABLE `enterprise_workplace_wait_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL,
  `check_id` bigint(20) DEFAULT NULL,
  `status` int(100) DEFAULT '0' COMMENT '1:已告知   0未告知',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_workplace_wait_notify
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_year_plan
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_year_plan`;
CREATE TABLE `enterprise_year_plan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` varchar(200) DEFAULT NULL COMMENT '每个文件具体对应',
  `plan_file` varchar(200) DEFAULT NULL,
  `plan_type` varchar(50) DEFAULT NULL COMMENT '包含内容参考需求文档上传的计划文件',
  `file_name` varchar(200) DEFAULT NULL,
  `admin_train_time` decimal(10,2) DEFAULT NULL,
  `employee_train_time` decimal(10,2) DEFAULT NULL,
  `plan_period` varchar(50) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_year_plan
-- ----------------------------

-- ----------------------------
-- Table structure for equipment_service_use
-- ----------------------------
DROP TABLE IF EXISTS `equipment_service_use`;
CREATE TABLE `equipment_service_use` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `record_id` bigint(20) DEFAULT NULL,
  `equipment_id` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '0：维护记录   1：运行记录',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of equipment_service_use
-- ----------------------------

-- ----------------------------
-- Table structure for government_enterprise_baseinfo
-- ----------------------------
DROP TABLE IF EXISTS `government_enterprise_baseinfo`;
CREATE TABLE `government_enterprise_baseinfo` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `harm_degree` varchar(50) DEFAULT NULL,
  `is_highly_toxic` int(11) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `build_date` datetime DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `harm_num` float DEFAULT NULL,
  `represent_name` varchar(50) DEFAULT NULL,
  `represent_phone` varchar(20) DEFAULT NULL,
  `duty_name` varchar(50) DEFAULT NULL,
  `duty_phone` varchar(20) DEFAULT NULL,
  `harm_total_number` int(11) DEFAULT NULL,
  `harm_woman_number` int(11) DEFAULT NULL,
  `harm_farmer_number` int(11) DEFAULT NULL,
  `register_type` varchar(100) DEFAULT NULL,
  `icon` varchar(500) DEFAULT NULL,
  `hint_picture` varchar(500) DEFAULT NULL,
  `update_frequency` int(11) DEFAULT NULL COMMENT '单位(天)',
  `update_way` varchar(20) DEFAULT NULL,
  `last_use_time` datetime DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1;封禁  0:不封禁',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_enterprise_baseinfo
-- ----------------------------

-- ----------------------------
-- Table structure for government_enterprise_year
-- ----------------------------
DROP TABLE IF EXISTS `government_enterprise_year`;
CREATE TABLE `government_enterprise_year` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `score` float DEFAULT NULL,
  `administration` int(11) DEFAULT NULL COMMENT '1.已建立，0:未建立',
  `responsibility_number` int(11) DEFAULT NULL,
  `prevention_number` int(11) DEFAULT NULL,
  `post_head_notify` int(11) DEFAULT NULL,
  `post_head_notify_all` int(11) DEFAULT NULL,
  `workplace_notify` int(11) DEFAULT NULL,
  `workplace_notify_all` int(11) DEFAULT NULL,
  `medical_result_notify` int(11) DEFAULT NULL,
  `medical_result_notify_all` int(11) DEFAULT NULL,
  `harm_declare_num_all` int(11) DEFAULT NULL,
  `harm_declare_num` int(11) DEFAULT NULL,
  `employee_number` int(11) DEFAULT NULL,
  `employee_training_number` int(11) DEFAULT NULL,
  `manager_number` int(11) DEFAULT NULL,
  `manager_training_number` int(11) DEFAULT NULL,
  `employee_average_length` int(11) DEFAULT NULL,
  `manager_average_length` int(11) DEFAULT NULL,
  `harm_check_number` int(11) DEFAULT NULL,
  `harm_number` int(11) DEFAULT NULL,
  `health_archive_number` int(11) DEFAULT NULL,
  `health_archive_woman_number` int(11) DEFAULT NULL,
  `health_archive_fatmer_number` int(11) DEFAULT NULL,
  `post_head_medical_all_num` int(11) DEFAULT NULL,
  `post_head_medical_num` int(11) DEFAULT NULL,
  `post_later_medical_all_num` int(11) DEFAULT NULL,
  `post_later_medical_num` int(11) DEFAULT NULL,
  `post_medical_all_num` int(11) DEFAULT NULL,
  `post_medical_num` int(11) DEFAULT NULL,
  `taboo_number` int(11) DEFAULT NULL,
  `occupational_disease_number` int(11) DEFAULT NULL,
  `suspected_disease_number` int(11) DEFAULT NULL,
  `provide_protect` int(11) DEFAULT NULL COMMENT '0，无;100,有',
  `service_record` int(11) DEFAULT NULL COMMENT '0，无;100,有',
  `toxic_drill` int(11) DEFAULT NULL COMMENT '0，无;100,有',
  `check_date` int(11) DEFAULT NULL,
  `check_nature` int(11) DEFAULT NULL,
  `rectified` double DEFAULT NULL,
  `three_simultaneity` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_enterprise_year
-- ----------------------------

-- ----------------------------
-- Table structure for government_harm_report
-- ----------------------------
DROP TABLE IF EXISTS `government_harm_report`;
CREATE TABLE `government_harm_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `workplace_name` varchar(200) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `harm_factor_id` bigint(20) DEFAULT NULL,
  `involved_number` int(11) DEFAULT NULL,
  `woman_number` int(11) DEFAULT NULL,
  `farmer_number` int(11) DEFAULT NULL,
  `test_number` int(11) DEFAULT NULL,
  `last_check_date` datetime DEFAULT NULL,
  `check_organizatio` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=641 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_harm_report
-- ----------------------------

-- ----------------------------
-- Table structure for government_last_harm_check
-- ----------------------------
DROP TABLE IF EXISTS `government_last_harm_check`;
CREATE TABLE `government_last_harm_check` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `harm_factor_id` bigint(20) DEFAULT NULL,
  `harm_factor_name` varchar(200) DEFAULT NULL,
  `check_organization` varchar(200) DEFAULT NULL,
  `is_qualified` varchar(20) DEFAULT NULL,
  `check_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_last_harm_check
-- ----------------------------

-- ----------------------------
-- Table structure for government_last_medical
-- ----------------------------
DROP TABLE IF EXISTS `government_last_medical`;
CREATE TABLE `government_last_medical` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `employee_name` varchar(50) DEFAULT NULL,
  `institution_name` varchar(200) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT '岗前，岗中，离岗，职业病诊断，其它，离岗后医学随访，复查，医学观察',
  `result` varchar(200) DEFAULT NULL,
  `medical_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_last_medical
-- ----------------------------

-- ----------------------------
-- Table structure for government_last_provide
-- ----------------------------
DROP TABLE IF EXISTS `government_last_provide`;
CREATE TABLE `government_last_provide` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `employee_name` varchar(20) DEFAULT NULL,
  `provide_articles_name` varchar(100) DEFAULT NULL,
  `provide_date` datetime DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_last_provide
-- ----------------------------

-- ----------------------------
-- Table structure for government_last_training
-- ----------------------------
DROP TABLE IF EXISTS `government_last_training`;
CREATE TABLE `government_last_training` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `training_date` datetime DEFAULT NULL,
  `theme` varchar(200) DEFAULT NULL,
  `lecturer` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_last_training
-- ----------------------------

-- ----------------------------
-- Table structure for government_outside_check
-- ----------------------------
DROP TABLE IF EXISTS `government_outside_check`;
CREATE TABLE `government_outside_check` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `post_medical_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_outside_check
-- ----------------------------

-- ----------------------------
-- Table structure for government_protective_measures
-- ----------------------------
DROP TABLE IF EXISTS `government_protective_measures`;
CREATE TABLE `government_protective_measures` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `equipment_name` varchar(200) DEFAULT NULL,
  `workplace_name` varchar(200) DEFAULT NULL,
  `service_number` int(11) DEFAULT NULL,
  `last_service_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=454 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_protective_measures
-- ----------------------------

-- ----------------------------
-- Table structure for government_provide_protect_art
-- ----------------------------
DROP TABLE IF EXISTS `government_provide_protect_art`;
CREATE TABLE `government_provide_protect_art` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `model` varchar(200) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_provide_protect_art
-- ----------------------------

-- ----------------------------
-- Table structure for government_service_inspection
-- ----------------------------
DROP TABLE IF EXISTS `government_service_inspection`;
CREATE TABLE `government_service_inspection` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `check_date` datetime DEFAULT NULL,
  `check_nature` varchar(200) DEFAULT NULL,
  `check_dept` varchar(200) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_service_inspection
-- ----------------------------

-- ----------------------------
-- Table structure for government_service_record
-- ----------------------------
DROP TABLE IF EXISTS `government_service_record`;
CREATE TABLE `government_service_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `equipment_name` varchar(100) DEFAULT NULL,
  `service_date` datetime DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `result` varchar(500) DEFAULT NULL,
  `service_personnel` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_service_record
-- ----------------------------

-- ----------------------------
-- Table structure for government_service_rectified
-- ----------------------------
DROP TABLE IF EXISTS `government_service_rectified`;
CREATE TABLE `government_service_rectified` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `rectification_date` datetime DEFAULT NULL,
  `department` varchar(200) DEFAULT NULL,
  `content2` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_service_rectified
-- ----------------------------

-- ----------------------------
-- Table structure for government_three_simultaneity
-- ----------------------------
DROP TABLE IF EXISTS `government_three_simultaneity`;
CREATE TABLE `government_three_simultaneity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `project_name` varchar(500) DEFAULT NULL,
  `project_source` varchar(200) DEFAULT NULL,
  `project_state` int(11) DEFAULT NULL COMMENT '0:预评价阶段 1:防护设施设计 2:竣工验收',
  `harm_degree` int(11) DEFAULT NULL COMMENT '0一般 1较重 2严重',
  `status` int(11) DEFAULT NULL COMMENT '1已完成 0未完成',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_three_simultaneity
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_area
-- ----------------------------
DROP TABLE IF EXISTS `monitor_area`;
CREATE TABLE `monitor_area` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(20) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1封禁   0不封禁',
  `is_deleted` int(11) DEFAULT NULL COMMENT '1删除  0不删除',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_area
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_enterprise
-- ----------------------------
DROP TABLE IF EXISTS `monitor_enterprise`;
CREATE TABLE `monitor_enterprise` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `area_id` bigint(20) DEFAULT NULL,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_enterprise
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_user
-- ----------------------------
DROP TABLE IF EXISTS `monitor_user`;
CREATE TABLE `monitor_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `area_id` bigint(20) DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1封禁   0不封禁',
  `is_deleted` int(11) DEFAULT NULL COMMENT '1删除  0不删除',
  `is_tab` int(11) DEFAULT NULL,
  `theme` varchar(50) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_user
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_workplace_harm_factor
-- ----------------------------
DROP TABLE IF EXISTS `monitor_workplace_harm_factor`;
CREATE TABLE `monitor_workplace_harm_factor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `monitor_point_id` bigint(20) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL,
  `harm_factor_id` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1049 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_workplace_harm_factor
-- ----------------------------

-- ----------------------------
-- Table structure for post_harm_factor
-- ----------------------------
DROP TABLE IF EXISTS `post_harm_factor`;
CREATE TABLE `post_harm_factor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `workplace_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `harm_factor_id` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `harm_factor_id` (`harm_factor_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1402 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_harm_factor
-- ----------------------------

-- ----------------------------
-- Table structure for post_protect_articles
-- ----------------------------
DROP TABLE IF EXISTS `post_protect_articles`;
CREATE TABLE `post_protect_articles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) DEFAULT NULL,
  `protect_articles_id` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_protect_articles
-- ----------------------------

-- ----------------------------
-- Table structure for sys_consult
-- ----------------------------
DROP TABLE IF EXISTS `sys_consult`;
CREATE TABLE `sys_consult` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` bigint(20) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `question` varchar(1000) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `answer` varchar(1000) DEFAULT NULL,
  `answer_time` datetime DEFAULT NULL,
  `answer_status` int(11) DEFAULT NULL COMMENT '1:已回复  0未回复',
  `read_status` int(11) DEFAULT NULL COMMENT '1:已阅读  0未阅读',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_consult
-- ----------------------------

-- ----------------------------
-- Table structure for sys_maintain
-- ----------------------------
DROP TABLE IF EXISTS `sys_maintain`;
CREATE TABLE `sys_maintain` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_maintain
-- ----------------------------

-- ----------------------------
-- Table structure for sys_recommend_institution
-- ----------------------------
DROP TABLE IF EXISTS `sys_recommend_institution`;
CREATE TABLE `sys_recommend_institution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `area` varchar(200) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '0:培训机构 1体检机构 2:检测机构',
  `contacts` varchar(30) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_recommend_institution
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1封禁   0不封禁',
  `is_deleted` int(11) DEFAULT NULL COMMENT '1删除  0不删除',
  `is_tab` int(11) DEFAULT NULL COMMENT '1Tab菜单  0单页面',
  `theme` varchar(50) DEFAULT NULL COMMENT 'white\\black',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('2', 'Scott', 'lanware@lanware.cn', 'acfd7637564d20d245449d486ddb1339', '2019-12-02 00:20:59', '0', null, '1', 'black', '2019-07-04 15:17:58', '2019-10-26 19:28:42', '超级系统管理员');

-- ----------------------------
-- Table structure for wait_check_harm_factor
-- ----------------------------
DROP TABLE IF EXISTS `wait_check_harm_factor`;
CREATE TABLE `wait_check_harm_factor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wait_check_id` bigint(20) DEFAULT NULL COMMENT '待检测项目id',
  `harm_factor_id` bigint(20) DEFAULT NULL COMMENT '检测危害因素id',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wait_check_harm_factor
-- ----------------------------

-- ----------------------------
-- Table structure for wait_medical_type
-- ----------------------------
DROP TABLE IF EXISTS `wait_medical_type`;
CREATE TABLE `wait_medical_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wait_medical_type
-- ----------------------------
INSERT INTO `wait_medical_type` VALUES ('1', '岗前');
INSERT INTO `wait_medical_type` VALUES ('2', '岗中');
INSERT INTO `wait_medical_type` VALUES ('3', '离岗');
INSERT INTO `wait_medical_type` VALUES ('4', '复查');

-- ----------------------------
-- Table structure for workplace_harm_factor
-- ----------------------------
DROP TABLE IF EXISTS `workplace_harm_factor`;
CREATE TABLE `workplace_harm_factor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `workplace_id` bigint(20) DEFAULT NULL,
  `harm_factor_id` bigint(20) DEFAULT NULL,
  `monitor_period` int(11) DEFAULT NULL,
  `monitor_type` int(11) DEFAULT NULL COMMENT '0内部检测 1外部检测',
  `harm_source` varchar(200) DEFAULT NULL COMMENT '手动输入',
  `operation_way` varchar(20) DEFAULT NULL COMMENT '12本台账提取',
  `is_partition` varchar(20) DEFAULT NULL COMMENT '12本台账提取',
  `device_status` varchar(20) DEFAULT NULL COMMENT '12本台账提取',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of workplace_harm_factor
-- ----------------------------
