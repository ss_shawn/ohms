package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeeMedicalMapper;
import com.lanware.ehs.pojo.EmployeeMedical;
import com.lanware.ehs.pojo.EmployeeMedicalExample;
import com.lanware.ehs.service.EmployeeMedicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeMedicalServiceImpl extends BaseServiceImpl<EmployeeMedical, EmployeeMedicalExample, Long> implements EmployeeMedicalService {

@Autowired
private EmployeeMedicalMapper employeeMedicalMapper;

@Override
protected Object getMapper() {
	return employeeMedicalMapper;
}
}