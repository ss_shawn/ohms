package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseOutsideCheckResultMapper;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckResult;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckResultExample;
import com.lanware.ehs.service.EnterpriseOutsideCheckResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseOutsideCheckResultServiceImpl extends BaseServiceImpl<EnterpriseOutsideCheckResult, EnterpriseOutsideCheckResultExample, Long> implements EnterpriseOutsideCheckResultService {

@Autowired
private EnterpriseOutsideCheckResultMapper enterpriseOutsideCheckResultMapper;

@Override
protected Object getMapper() {
	return enterpriseOutsideCheckResultMapper;
}
}