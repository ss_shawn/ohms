package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseRoleResourceRelation;
import com.lanware.ehs.pojo.EnterpriseRoleResourceRelationExample;

public interface EnterpriseRoleResourceRelationService extends BaseService<EnterpriseRoleResourceRelation, EnterpriseRoleResourceRelationExample, Long> {
}