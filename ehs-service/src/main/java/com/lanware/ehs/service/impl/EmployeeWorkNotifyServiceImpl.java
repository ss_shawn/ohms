package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeeWorkNotifyMapper;
import com.lanware.ehs.pojo.EmployeeWorkNotify;
import com.lanware.ehs.pojo.EmployeeWorkNotifyExample;
import com.lanware.ehs.service.EmployeeWorkNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class EmployeeWorkNotifyServiceImpl extends BaseServiceImpl<EmployeeWorkNotify, EmployeeWorkNotifyExample, Long> implements EmployeeWorkNotifyService {

@Autowired
private EmployeeWorkNotifyMapper employeeWorkNotifyMapper;

@Override
protected Object getMapper() {
	return employeeWorkNotifyMapper;
}
}