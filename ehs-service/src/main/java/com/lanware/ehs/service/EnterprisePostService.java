package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterprisePost;
import com.lanware.ehs.pojo.EnterprisePostExample;

public interface EnterprisePostService extends BaseService<EnterprisePost, EnterprisePostExample, Long> {
}