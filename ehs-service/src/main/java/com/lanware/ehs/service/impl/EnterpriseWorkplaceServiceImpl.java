package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseWorkplaceMapper;
import com.lanware.ehs.pojo.EnterpriseWorkplace;
import com.lanware.ehs.pojo.EnterpriseWorkplaceExample;
import com.lanware.ehs.service.EnterpriseWorkplaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseWorkplaceServiceImpl extends BaseServiceImpl<EnterpriseWorkplace, EnterpriseWorkplaceExample, Long> implements EnterpriseWorkplaceService {

@Autowired
private EnterpriseWorkplaceMapper enterpriseWorkplaceMapper;

@Override
protected Object getMapper() {
	return enterpriseWorkplaceMapper;
}
}