package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseWorkplaceNotify;
import com.lanware.ehs.pojo.EnterpriseWorkplaceNotifyExample;

public interface EnterpriseWorkplacetNotifyService extends BaseService<EnterpriseWorkplaceNotify, EnterpriseWorkplaceNotifyExample, Long> {
}