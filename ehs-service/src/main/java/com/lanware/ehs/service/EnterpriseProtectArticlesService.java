package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseProtectArticles;
import com.lanware.ehs.pojo.EnterpriseProtectArticlesExample;

public interface EnterpriseProtectArticlesService extends BaseService<EnterpriseProtectArticles, EnterpriseProtectArticlesExample, Long> {
}