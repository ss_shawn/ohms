package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseDeclare;
import com.lanware.ehs.pojo.EnterpriseDeclareExample;

public interface EnterpriseDeclareService extends BaseService<EnterpriseDeclare, EnterpriseDeclareExample, Long> {
}