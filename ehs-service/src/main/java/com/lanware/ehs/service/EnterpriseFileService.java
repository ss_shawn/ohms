package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseFile;
import com.lanware.ehs.pojo.EnterpriseFileExample;

public interface EnterpriseFileService extends BaseService<EnterpriseFile, EnterpriseFileExample, Long> {
}