package com.lanware.ehs.service;

import com.lanware.ehs.pojo.MonitorEnterprise;
import com.lanware.ehs.pojo.MonitorEnterpriseExample;

public interface MonitorEnterpriseService extends BaseService<MonitorEnterprise, MonitorEnterpriseExample, Long> {
}