package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseWaitProvideProtectArticles;
import com.lanware.ehs.pojo.EnterpriseWaitProvideProtectArticlesExample;

public interface EnterpriseWaitProvideProtectArticlesService extends BaseService<EnterpriseWaitProvideProtectArticles, EnterpriseWaitProvideProtectArticlesExample, Long> {
}