package com.lanware.ehs.service;

import org.apache.ibatis.annotations.Param;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface BaseService<P, E, K> {

    long countByExample(E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    int deleteByExample(E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    int deleteByPrimaryKey(K id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    int insert(P record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    int insertSelective(P record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    List<P> selectByExample(E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    P selectByPrimaryKey(K id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    int updateByExampleSelective(@Param("record") P record, @Param("example") E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    int updateByExample(@Param("record") P record, @Param("example") E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    int updateByPrimaryKeySelective(P record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    int updateByPrimaryKey(P record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;
}
