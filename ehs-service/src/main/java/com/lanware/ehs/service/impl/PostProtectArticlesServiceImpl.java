package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeePostMapper;
import com.lanware.ehs.mapper.EnterpriseWaitProvideProtectArticlesMapper;
import com.lanware.ehs.mapper.PostProtectArticlesMapper;
import com.lanware.ehs.mapper.custom.PostProtectArticlesCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.service.PostProtectArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PostProtectArticlesServiceImpl extends BaseServiceImpl<PostProtectArticles, PostProtectArticlesExample, Long> implements PostProtectArticlesService {

	private PostProtectArticlesMapper postProtectArticlesMapper;

	private PostProtectArticlesCustomMapper postProtectArticlesCustomMapper;
	@Autowired
	private EnterpriseWaitProvideProtectArticlesMapper enterpriseWaitProvideProtectArticlesMapper ;
	@Autowired
	private EmployeePostMapper employeePostMapper;
	@Autowired
	public PostProtectArticlesServiceImpl(PostProtectArticlesMapper postProtectArticlesMapper, PostProtectArticlesCustomMapper postProtectArticlesCustomMapper) {
		this.postProtectArticlesMapper = postProtectArticlesMapper;
		this.postProtectArticlesCustomMapper = postProtectArticlesCustomMapper;
	}

	@Override
	protected Object getMapper() {
	return postProtectArticlesMapper;
}

	@Override
	public int insertBatch(List<PostProtectArticles> postProtectArticlesList,Long enterpriseId) {
		//没有发放的防护用品插入
		for(PostProtectArticles postProtectArticles:postProtectArticlesList){
			EmployeePostExample employeePostExample=new EmployeePostExample();
			employeePostExample.createCriteria().andPostIdEqualTo(postProtectArticles.getPostId()).andLeaveDateIsNull();
			List<EmployeePost> employeePostList= employeePostMapper.selectByExample(employeePostExample);//查询岗位所有在岗员工
			for(EmployeePost employeePost:employeePostList){
				EnterpriseWaitProvideProtectArticlesExample enterpriseWaitProvideProtectArticlesExample=new EnterpriseWaitProvideProtectArticlesExample();
				enterpriseWaitProvideProtectArticlesExample.createCriteria().andEmployeePostIdEqualTo(employeePost.getId()).andProtectArticlesIdEqualTo(postProtectArticles.getProtectArticlesId());
				List ent=enterpriseWaitProvideProtectArticlesMapper.selectByExample(enterpriseWaitProvideProtectArticlesExample);//该员工防护用品是否发放
				if(ent.size()==0){//没发放，插入待发放防护用品
					EnterpriseWaitProvideProtectArticles enterpriseWaitProvideProtectArticles = new EnterpriseWaitProvideProtectArticles();
					enterpriseWaitProvideProtectArticles.setEmployeePostId(employeePost.getId());
					enterpriseWaitProvideProtectArticles.setEnterpriseId(enterpriseId);
					enterpriseWaitProvideProtectArticles.setStatus(0);
					enterpriseWaitProvideProtectArticles.setProtectArticlesId(postProtectArticles.getProtectArticlesId());
					enterpriseWaitProvideProtectArticles.setGmtCreate(new Date());
					enterpriseWaitProvideProtectArticlesMapper.insert(enterpriseWaitProvideProtectArticles);
				}
			}

		}

		return postProtectArticlesCustomMapper.insertBatch(postProtectArticlesList);
	}
	@Override
	public int deleteProtect(List<Long> ids){
		//删除待发放的防护用品
		for(Long id:ids){
			PostProtectArticles postProtectArticles=postProtectArticlesMapper.selectByPrimaryKey(id);
			EmployeePostExample employeePostExample=new EmployeePostExample();
			employeePostExample.createCriteria().andPostIdEqualTo(postProtectArticles.getPostId()).andLeaveDateIsNull();
			List<EmployeePost> employeePostList= employeePostMapper.selectByExample(employeePostExample);//查询岗位所有在岗员工
			for(EmployeePost employeePost:employeePostList){
				EnterpriseWaitProvideProtectArticlesExample enterpriseWaitProvideProtectArticlesExample=new EnterpriseWaitProvideProtectArticlesExample();
				enterpriseWaitProvideProtectArticlesExample.createCriteria().andEmployeePostIdEqualTo(employeePost.getId()).andProtectArticlesIdEqualTo(postProtectArticles.getProtectArticlesId());
				enterpriseWaitProvideProtectArticlesMapper.deleteByExample(enterpriseWaitProvideProtectArticlesExample);
			}
		}
		//删除岗位的防护用品
		PostProtectArticlesExample postProtectArticlesExample = new PostProtectArticlesExample();
		PostProtectArticlesExample.Criteria criteria = postProtectArticlesExample.createCriteria();
		criteria.andIdIn(ids);
		int n =postProtectArticlesMapper.deleteByExample(postProtectArticlesExample);

		return n;
	}

}