package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseEquipmentUseMapper;
import com.lanware.ehs.pojo.EnterpriseEquipmentUse;
import com.lanware.ehs.pojo.EnterpriseEquipmentUseExample;
import com.lanware.ehs.service.EnterpriseEquipmentUseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseEquipmentUseServiceImpl extends BaseServiceImpl<EnterpriseEquipmentUse, EnterpriseEquipmentUseExample, Long> implements EnterpriseEquipmentUseService {

@Autowired
private EnterpriseEquipmentUseMapper enterpriseEquipmentUseMapper;

@Override
protected Object getMapper() {
	return enterpriseEquipmentUseMapper;
}
}