package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFileExample;

public interface EnterpriseThreeSimultaneityFileService extends BaseService<EnterpriseThreeSimultaneityFile, EnterpriseThreeSimultaneityFileExample, Long> {
}