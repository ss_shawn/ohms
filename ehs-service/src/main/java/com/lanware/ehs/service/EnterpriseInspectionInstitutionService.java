package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitutionExample;

public interface EnterpriseInspectionInstitutionService extends BaseService<EnterpriseInspectionInstitution, EnterpriseInspectionInstitutionExample, Long> {
}