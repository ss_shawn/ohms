package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseEmergencyPlan;
import com.lanware.ehs.pojo.EnterpriseEmergencyPlanExample;

public interface EnterpriseEmergencyPlanService extends BaseService<EnterpriseEmergencyPlan, EnterpriseEmergencyPlanExample, Long> {
}