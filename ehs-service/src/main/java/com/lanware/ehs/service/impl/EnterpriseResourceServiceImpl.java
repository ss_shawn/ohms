package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseResourceMapper;
import com.lanware.ehs.pojo.EnterpriseResource;
import com.lanware.ehs.pojo.EnterpriseResourceExample;
import com.lanware.ehs.service.EnterpriseResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseResourceServiceImpl extends BaseServiceImpl<EnterpriseResource, EnterpriseResourceExample, Long> implements EnterpriseResourceService {

@Autowired
private EnterpriseResourceMapper enterpriseResourceMapper;

@Override
protected Object getMapper() {
	return enterpriseResourceMapper;
}
}