package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.PostHarmFactorMapper;
import com.lanware.ehs.mapper.custom.PostHarmFactorCustomMapper;
import com.lanware.ehs.pojo.PostHarmFactor;
import com.lanware.ehs.pojo.PostHarmFactorExample;
import com.lanware.ehs.service.PostHarmFactorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostHarmFactorServiceImpl extends BaseServiceImpl<PostHarmFactor, PostHarmFactorExample, Long> implements PostHarmFactorService {

	private PostHarmFactorMapper postHarmFactorMapper;

	private PostHarmFactorCustomMapper postHarmFactorCustomMapper;

	@Autowired
	public PostHarmFactorServiceImpl(PostHarmFactorMapper postHarmFactorMapper, PostHarmFactorCustomMapper postHarmFactorCustomMapper) {
		this.postHarmFactorMapper = postHarmFactorMapper;
		this.postHarmFactorCustomMapper = postHarmFactorCustomMapper;
	}

	@Override
	protected Object getMapper() {
		return postHarmFactorMapper;
	}

	@Override
	public int insertBatch(List<PostHarmFactor> postHarmFactorList) {
		return postHarmFactorCustomMapper.insertBatch(postHarmFactorList);
	}
}