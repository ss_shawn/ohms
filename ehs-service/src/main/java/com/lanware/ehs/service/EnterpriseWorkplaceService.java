package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseWorkplace;
import com.lanware.ehs.pojo.EnterpriseWorkplaceExample;

public interface EnterpriseWorkplaceService extends BaseService<EnterpriseWorkplace, EnterpriseWorkplaceExample, Long> {
}