package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseAuth;
import com.lanware.ehs.pojo.EnterpriseAuthExample;

public interface EnterpriseAuthService extends BaseService<EnterpriseAuth, EnterpriseAuthExample, Long> {
}