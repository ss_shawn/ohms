package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.pojo.EnterpriseEmployeeExample;

public interface EnterpriseEmployeeService extends BaseService<EnterpriseEmployee, EnterpriseEmployeeExample, Long> {
}