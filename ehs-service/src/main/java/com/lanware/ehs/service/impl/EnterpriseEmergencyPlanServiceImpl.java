package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseEmergencyPlanMapper;
import com.lanware.ehs.pojo.EnterpriseEmergencyPlan;
import com.lanware.ehs.pojo.EnterpriseEmergencyPlanExample;
import com.lanware.ehs.service.EnterpriseEmergencyPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseEmergencyPlanServiceImpl extends BaseServiceImpl<EnterpriseEmergencyPlan, EnterpriseEmergencyPlanExample, Long> implements EnterpriseEmergencyPlanService {

@Autowired
private EnterpriseEmergencyPlanMapper enterpriseEmergencyPlanMapper;

@Override
protected Object getMapper() {
	return enterpriseEmergencyPlanMapper;
}
}