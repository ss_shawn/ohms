package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseTrainingEmployeeMapper;
import com.lanware.ehs.pojo.EnterpriseTrainingEmployee;
import com.lanware.ehs.pojo.EnterpriseTrainingEmployeeExample;
import com.lanware.ehs.service.EnterpriseTrainingEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseTrainingEmployeeServiceImpl extends BaseServiceImpl<EnterpriseTrainingEmployee, EnterpriseTrainingEmployeeExample, Long> implements EnterpriseTrainingEmployeeService {

@Autowired
private EnterpriseTrainingEmployeeMapper enterpriseTrainingEmployeeMapper;

@Override
protected Object getMapper() {
	return enterpriseTrainingEmployeeMapper;
}
}