package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseProvideProtectArticles;
import com.lanware.ehs.pojo.EnterpriseProvideProtectArticlesExample;

public interface EnterpriseProvideProtectArticlesService extends BaseService<EnterpriseProvideProtectArticles, EnterpriseProvideProtectArticlesExample, Long> {
}