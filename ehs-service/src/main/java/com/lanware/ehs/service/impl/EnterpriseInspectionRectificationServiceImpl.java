package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseInspectionRectificationMapper;
import com.lanware.ehs.pojo.EnterpriseInspectionRectification;
import com.lanware.ehs.pojo.EnterpriseInspectionRectificationExample;
import com.lanware.ehs.service.EnterpriseInspectionRectificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseInspectionRectificationServiceImpl extends BaseServiceImpl<EnterpriseInspectionRectification, EnterpriseInspectionRectificationExample, Long> implements EnterpriseInspectionRectificationService {

@Autowired
private EnterpriseInspectionRectificationMapper enterpriseInspectionRectificationMapper;

@Override
protected Object getMapper() {
	return enterpriseInspectionRectificationMapper;
}
}