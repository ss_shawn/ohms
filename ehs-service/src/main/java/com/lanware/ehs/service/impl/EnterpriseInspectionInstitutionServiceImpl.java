package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseInspectionInstitutionMapper;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitutionExample;
import com.lanware.ehs.service.EnterpriseInspectionInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseInspectionInstitutionServiceImpl extends BaseServiceImpl<EnterpriseInspectionInstitution, EnterpriseInspectionInstitutionExample, Long> implements EnterpriseInspectionInstitutionService {

@Autowired
private EnterpriseInspectionInstitutionMapper enterpriseInspectionInstitutionMapper;

@Override
protected Object getMapper() {
	return enterpriseInspectionInstitutionMapper;
}
}