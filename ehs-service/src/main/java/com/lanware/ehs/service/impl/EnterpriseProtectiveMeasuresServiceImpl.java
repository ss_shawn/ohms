package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseProtectiveMeasuresMapper;
import com.lanware.ehs.pojo.EnterpriseProtectiveMeasures;
import com.lanware.ehs.pojo.EnterpriseProtectiveMeasuresExample;
import com.lanware.ehs.service.EnterpriseProtectiveMeasuresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseProtectiveMeasuresServiceImpl extends BaseServiceImpl<EnterpriseProtectiveMeasures, EnterpriseProtectiveMeasuresExample, Long> implements EnterpriseProtectiveMeasuresService {

@Autowired
private EnterpriseProtectiveMeasuresMapper enterpriseProtectiveMeasuresMapper;

@Override
protected Object getMapper() {
	return enterpriseProtectiveMeasuresMapper;
}
}