package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseDeclareWorkplace;
import com.lanware.ehs.pojo.EnterpriseDeclareWorkplaceExample;

public interface EnterpriseDeclareWorkplaceService extends BaseService<EnterpriseDeclareWorkplace, EnterpriseDeclareWorkplaceExample, Long> {
}