package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeeDiseaseDiagnosiMapper;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosiExample;
import com.lanware.ehs.service.EmployeeDiseaseDiagnosiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeDiseaseDiagnosiServiceImpl extends BaseServiceImpl<EmployeeDiseaseDiagnosi, EmployeeDiseaseDiagnosiExample, Long> implements EmployeeDiseaseDiagnosiService {

@Autowired
private EmployeeDiseaseDiagnosiMapper employeeDiseaseDiagnosiMapper;

@Override
protected Object getMapper() {
	return employeeDiseaseDiagnosiMapper;
}
}