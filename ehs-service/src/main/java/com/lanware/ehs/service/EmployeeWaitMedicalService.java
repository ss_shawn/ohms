package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EmployeeWaitMedical;
import com.lanware.ehs.pojo.EmployeeWaitMedicalExample;

public interface EmployeeWaitMedicalService extends BaseService<EmployeeWaitMedical, EmployeeWaitMedicalExample, Long> {
}