package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseFileMapper;
import com.lanware.ehs.pojo.EnterpriseFile;
import com.lanware.ehs.pojo.EnterpriseFileExample;
import com.lanware.ehs.service.EnterpriseFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseFileServiceImpl extends BaseServiceImpl<EnterpriseFile, EnterpriseFileExample, Long> implements EnterpriseFileService {

@Autowired
private EnterpriseFileMapper enterpriseFileMapper;

@Override
protected Object getMapper() {
	return enterpriseFileMapper;
}
}