package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.SysConsultMapper;
import com.lanware.ehs.pojo.SysConsult;
import com.lanware.ehs.pojo.SysConsultExample;
import com.lanware.ehs.service.SysConsultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysConsultServiceImpl extends BaseServiceImpl<SysConsult, SysConsultExample, Long> implements SysConsultService {

@Autowired
private SysConsultMapper sysConsultMapper;

@Override
protected Object getMapper() {
	return sysConsultMapper;
}
}