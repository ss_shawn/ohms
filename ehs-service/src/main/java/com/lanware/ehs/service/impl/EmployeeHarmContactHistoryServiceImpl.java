package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeeHarmContactHistoryMapper;
import com.lanware.ehs.pojo.EmployeeHarmContactHistory;
import com.lanware.ehs.pojo.EmployeeHarmContactHistoryExample;
import com.lanware.ehs.service.EmployeeHarmContactHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeHarmContactHistoryServiceImpl extends BaseServiceImpl<EmployeeHarmContactHistory, EmployeeHarmContactHistoryExample, Long> implements EmployeeHarmContactHistoryService {

@Autowired
private EmployeeHarmContactHistoryMapper employeeHarmContactHistoryMapper;

@Override
protected Object getMapper() {
	return employeeHarmContactHistoryMapper;
}
}