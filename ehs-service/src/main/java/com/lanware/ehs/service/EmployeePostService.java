package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EmployeePost;
import com.lanware.ehs.pojo.EmployeePostExample;

public interface EmployeePostService extends BaseService<EmployeePost, EmployeePostExample, Long> {
}