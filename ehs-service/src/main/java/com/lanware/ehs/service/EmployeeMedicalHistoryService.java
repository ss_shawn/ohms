package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EmployeeMedicalHistory;
import com.lanware.ehs.pojo.EmployeeMedicalHistoryExample;

public interface EmployeeMedicalHistoryService extends BaseService<EmployeeMedicalHistory, EmployeeMedicalHistoryExample, Long> {
}