package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.WorkplaceHarmFactorMapper;
import com.lanware.ehs.pojo.WorkplaceHarmFactor;
import com.lanware.ehs.pojo.WorkplaceHarmFactorExample;
import com.lanware.ehs.service.WorkplaceHarmFactorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkplaceHarmFactorServiceImpl extends BaseServiceImpl<WorkplaceHarmFactor, WorkplaceHarmFactorExample, Long> implements WorkplaceHarmFactorService {

@Autowired
private WorkplaceHarmFactorMapper workplaceHarmFactorMapper;

@Override
protected Object getMapper() {
	return workplaceHarmFactorMapper;
}
}