package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseOutsideCheckResult;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckResultExample;

public interface EnterpriseOutsideCheckResultService extends BaseService<EnterpriseOutsideCheckResult, EnterpriseOutsideCheckResultExample, Long> {
}