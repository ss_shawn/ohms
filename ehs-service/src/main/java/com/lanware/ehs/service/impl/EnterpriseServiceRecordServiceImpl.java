package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseServiceRecordMapper;
import com.lanware.ehs.pojo.EnterpriseServiceRecord;
import com.lanware.ehs.pojo.EnterpriseServiceRecordExample;
import com.lanware.ehs.service.EnterpriseServiceRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseServiceRecordServiceImpl extends BaseServiceImpl<EnterpriseServiceRecord, EnterpriseServiceRecordExample, Long> implements EnterpriseServiceRecordService {

@Autowired
private EnterpriseServiceRecordMapper enterpriseServiceRecordMapper;

@Override
protected Object getMapper() {
	return enterpriseServiceRecordMapper;
}
}