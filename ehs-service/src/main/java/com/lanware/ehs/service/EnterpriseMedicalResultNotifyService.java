package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseMedicalResultNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultNotifyExample;

public interface EnterpriseMedicalResultNotifyService extends BaseService<EnterpriseMedicalResultNotify, EnterpriseMedicalResultNotifyExample, Long> {
}