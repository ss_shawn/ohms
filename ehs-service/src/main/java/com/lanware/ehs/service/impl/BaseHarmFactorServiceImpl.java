package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.BaseHarmFactorMapper;
import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.BaseHarmFactorExample;
import com.lanware.ehs.service.BaseHarmFactorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BaseHarmFactorServiceImpl extends BaseServiceImpl<BaseHarmFactor, BaseHarmFactorExample, Long> implements BaseHarmFactorService {

@Autowired
private BaseHarmFactorMapper baseHarmFactorMapper;

@Override
protected Object getMapper() {
	return baseHarmFactorMapper;
}
}