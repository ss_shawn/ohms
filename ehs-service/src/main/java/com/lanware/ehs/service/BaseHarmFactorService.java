package com.lanware.ehs.service;

import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.BaseHarmFactorExample;

public interface BaseHarmFactorService extends BaseService<BaseHarmFactor, BaseHarmFactorExample, Long> {
}