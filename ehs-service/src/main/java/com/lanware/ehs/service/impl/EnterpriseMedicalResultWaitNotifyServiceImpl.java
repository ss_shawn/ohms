package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseMedicalResultWaitNotifyMapper;
import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotifyExample;
import com.lanware.ehs.service.EnterpriseMedicalResultWaitNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class EnterpriseMedicalResultWaitNotifyServiceImpl extends BaseServiceImpl<EnterpriseMedicalResultWaitNotify, EnterpriseMedicalResultWaitNotifyExample, Long> implements EnterpriseMedicalResultWaitNotifyService {

@Autowired
private EnterpriseMedicalResultWaitNotifyMapper enterpriseMedicalResultWaitNotifyMapper;

@Override
protected Object getMapper() {
	return enterpriseMedicalResultWaitNotifyMapper;
}
}