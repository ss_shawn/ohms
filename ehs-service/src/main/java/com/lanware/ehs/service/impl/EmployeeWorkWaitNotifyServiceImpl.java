package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeeWorkWaitNotifyMapper;
import com.lanware.ehs.pojo.EmployeeWorkWaitNotify;
import com.lanware.ehs.pojo.EmployeeWorkWaitNotifyExample;
import com.lanware.ehs.service.EmployeeWorkWaitNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class EmployeeWorkWaitNotifyServiceImpl extends BaseServiceImpl<EmployeeWorkWaitNotify, EmployeeWorkWaitNotifyExample, Long> implements EmployeeWorkWaitNotifyService {

@Autowired
private EmployeeWorkWaitNotifyMapper employeeWorkWaitNotifyMapper;

@Override
protected Object getMapper() {
	return employeeWorkWaitNotifyMapper;
}
}