package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseToxicDrillMapper;
import com.lanware.ehs.pojo.EnterpriseToxicDrill;
import com.lanware.ehs.pojo.EnterpriseToxicDrillExample;
import com.lanware.ehs.service.EnterpriseToxicDrillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseToxicDrillServiceImpl extends BaseServiceImpl<EnterpriseToxicDrill, EnterpriseToxicDrillExample, Long> implements EnterpriseToxicDrillService {

@Autowired
private EnterpriseToxicDrillMapper enterpriseToxicDrillMapper;

@Override
protected Object getMapper() {
	return enterpriseToxicDrillMapper;
}
}