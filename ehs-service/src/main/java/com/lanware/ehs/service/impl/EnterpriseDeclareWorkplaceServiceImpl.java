package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseDeclareWorkplaceMapper;
import com.lanware.ehs.pojo.EnterpriseDeclareWorkplace;
import com.lanware.ehs.pojo.EnterpriseDeclareWorkplaceExample;
import com.lanware.ehs.service.EnterpriseDeclareWorkplaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseDeclareWorkplaceServiceImpl extends BaseServiceImpl<EnterpriseDeclareWorkplace, EnterpriseDeclareWorkplaceExample, Long> implements EnterpriseDeclareWorkplaceService {

@Autowired
private EnterpriseDeclareWorkplaceMapper enterpriseDeclareWorkplaceMapper;

@Override
protected Object getMapper() {
	return enterpriseDeclareWorkplaceMapper;
}
}