package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.EnterpriseUserExample;

public interface EnterpriseUserService extends BaseService<EnterpriseUser, EnterpriseUserExample, Long> {
}