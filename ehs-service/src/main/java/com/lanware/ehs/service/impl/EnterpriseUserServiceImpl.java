package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseUserMapper;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.EnterpriseUserExample;
import com.lanware.ehs.service.EnterpriseUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseUserServiceImpl extends BaseServiceImpl<EnterpriseUser, EnterpriseUserExample, Long> implements EnterpriseUserService {

@Autowired
private EnterpriseUserMapper enterpriseUserMapper;

@Override
protected Object getMapper() {
	return enterpriseUserMapper;
}
}