package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseThreeSimultaneityFileMapper;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFileExample;
import com.lanware.ehs.service.EnterpriseThreeSimultaneityFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseThreeSimultaneityFileServiceImpl extends BaseServiceImpl<EnterpriseThreeSimultaneityFile, EnterpriseThreeSimultaneityFileExample, Long> implements EnterpriseThreeSimultaneityFileService {

@Autowired
private EnterpriseThreeSimultaneityFileMapper enterpriseThreeSimultaneityFileMapper;

@Override
protected Object getMapper() {
	return enterpriseThreeSimultaneityFileMapper;
}
}