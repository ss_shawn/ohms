package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseBaseinfoMapper;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseBaseinfoExample;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseBaseinfoServiceImpl extends BaseServiceImpl<EnterpriseBaseinfo, EnterpriseBaseinfoExample, Long> implements EnterpriseBaseinfoService {

@Autowired
private EnterpriseBaseinfoMapper enterpriseBaseinfoMapper;

@Override
protected Object getMapper() {
	return enterpriseBaseinfoMapper;
}
}