package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseThreeSimultaneity;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityExample;

public interface EnterpriseThreeSimultaneityService extends BaseService<EnterpriseThreeSimultaneity, EnterpriseThreeSimultaneityExample, Long> {
}