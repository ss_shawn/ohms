package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseInsideMonitor;
import com.lanware.ehs.pojo.EnterpriseInsideMonitorExample;

public interface EnterpriseInsideMonitorService extends BaseService<EnterpriseInsideMonitor, EnterpriseInsideMonitorExample, Long> {
}