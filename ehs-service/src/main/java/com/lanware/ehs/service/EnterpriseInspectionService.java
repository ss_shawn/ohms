package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseInspection;
import com.lanware.ehs.pojo.EnterpriseInspectionExample;

public interface EnterpriseInspectionService extends BaseService<EnterpriseInspection, EnterpriseInspectionExample, Long> {
}