package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseTraining;
import com.lanware.ehs.pojo.EnterpriseTrainingExample;

public interface EnterpriseTrainingService extends BaseService<EnterpriseTraining, EnterpriseTrainingExample, Long> {
}