package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.MonitorAreaMapper;
import com.lanware.ehs.pojo.MonitorArea;
import com.lanware.ehs.pojo.MonitorAreaExample;
import com.lanware.ehs.service.MonitorAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MonitorAreaServiceImpl extends BaseServiceImpl<MonitorArea, MonitorAreaExample, Long> implements MonitorAreaService {

@Autowired
private MonitorAreaMapper monitorAreaMapper;

@Override
protected Object getMapper() {
	return monitorAreaMapper;
}
}