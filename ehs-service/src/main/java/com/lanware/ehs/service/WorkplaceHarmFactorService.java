package com.lanware.ehs.service;

import com.lanware.ehs.pojo.WorkplaceHarmFactor;
import com.lanware.ehs.pojo.WorkplaceHarmFactorExample;

public interface WorkplaceHarmFactorService extends BaseService<WorkplaceHarmFactor, WorkplaceHarmFactorExample, Long> {
}