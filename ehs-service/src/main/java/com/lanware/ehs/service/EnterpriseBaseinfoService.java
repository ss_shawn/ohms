package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseBaseinfoExample;

public interface EnterpriseBaseinfoService extends BaseService<EnterpriseBaseinfo, EnterpriseBaseinfoExample, Long> {
}