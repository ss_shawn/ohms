package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseOutsideCheck;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckExample;

public interface EnterpriseOutsideCheckService extends BaseService<EnterpriseOutsideCheck, EnterpriseOutsideCheckExample, Long> {
}