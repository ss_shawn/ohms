package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseWorkplaceWaitNotify;
import com.lanware.ehs.pojo.EnterpriseWorkplaceWaitNotifyExample;

public interface EnterpriseWorkplaceWaitNotifyService extends BaseService<EnterpriseWorkplaceWaitNotify, EnterpriseWorkplaceWaitNotifyExample, Long> {
}