package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseProvideProtectArticlesMapper;
import com.lanware.ehs.pojo.EnterpriseProvideProtectArticles;
import com.lanware.ehs.pojo.EnterpriseProvideProtectArticlesExample;
import com.lanware.ehs.service.EnterpriseProvideProtectArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseProvideProtectArticlesServiceImpl extends BaseServiceImpl<EnterpriseProvideProtectArticles, EnterpriseProvideProtectArticlesExample, Long> implements EnterpriseProvideProtectArticlesService {

@Autowired
private EnterpriseProvideProtectArticlesMapper enterpriseProvideProtectArticlesMapper;

@Override
protected Object getMapper() {
	return enterpriseProvideProtectArticlesMapper;
}
}