package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseWaitRectification;
import com.lanware.ehs.pojo.EnterpriseWaitRectificationExample;

public interface EnterpriseWaitRectificationService extends BaseService<EnterpriseWaitRectification, EnterpriseWaitRectificationExample, Long> {
}