package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseThreeSimultaneityMapper;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneity;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityExample;
import com.lanware.ehs.service.EnterpriseThreeSimultaneityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseThreeSimultaneityServiceImpl extends BaseServiceImpl<EnterpriseThreeSimultaneity, EnterpriseThreeSimultaneityExample, Long> implements EnterpriseThreeSimultaneityService {

@Autowired
private EnterpriseThreeSimultaneityMapper enterpriseThreeSimultaneityMapper;

@Override
protected Object getMapper() {
	return enterpriseThreeSimultaneityMapper;
}
}