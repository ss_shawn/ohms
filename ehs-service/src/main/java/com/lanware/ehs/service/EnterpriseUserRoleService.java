package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseUserRole;
import com.lanware.ehs.pojo.EnterpriseUserRoleExample;

public interface EnterpriseUserRoleService extends BaseService<EnterpriseUserRole, EnterpriseUserRoleExample, Long> {
}