package com.lanware.ehs.service.impl;

import com.lanware.ehs.service.BaseService;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public abstract class BaseServiceImpl<P, E, K> implements BaseService<P, E, K> {

    protected abstract Object getMapper();

    @Override
    public long countByExample(E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("countByExample", example.getClass());
        return (long) method.invoke(getMapper(), example);
    }

    @Override
    public int deleteByExample(E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("deleteByExample", example.getClass());
        return (int) method.invoke(getMapper(), example);
    }

    @Override
    public int deleteByPrimaryKey(K id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("deleteByPrimaryKey", id.getClass());
        return (int) method.invoke(getMapper(), id);
    }

    @Override
    public int insert(P record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("insert", record.getClass());
        return (int) method.invoke(getMapper(), record);
    }

    @Override
    public int insertSelective(P record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("insertSelective", record.getClass());
        return (int) method.invoke(getMapper(), record);
    }

    @Override
    public List<P> selectByExample(E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("selectByExample", example.getClass());
        return (List<P>) method.invoke(getMapper(), example);
    }

    @Override
    public P selectByPrimaryKey(K id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("selectByPrimaryKey", id.getClass());
        return (P) method.invoke(getMapper(), id);
    }

    @Override
    public int updateByExampleSelective(P record, E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("updateByExampleSelective", record.getClass(), example.getClass());
        return (int) method.invoke(getMapper(), record, example);
    }

    @Override
    public int updateByExample(P record, E example) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("updateByExample", record.getClass(), example.getClass());
        return (int) method.invoke(getMapper(), record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(P record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("updateByPrimaryKeySelective", record.getClass());
        return (int) method.invoke(getMapper(), record);
    }

    @Override
    public int updateByPrimaryKey(P record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMapper().getClass().getMethod("updateByPrimaryKey", record.getClass());
        return (int) method.invoke(getMapper(), record);
    }
}
