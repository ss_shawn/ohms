package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.SysUserMapper;
import com.lanware.ehs.pojo.SysUser;
import com.lanware.ehs.pojo.SysUserExample;
import com.lanware.ehs.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser, SysUserExample, Long> implements SysUserService {

@Autowired
private SysUserMapper sysUserMapper;

@Override
protected Object getMapper() {
	return sysUserMapper;
}
}