package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseHygieneOrg;
import com.lanware.ehs.pojo.EnterpriseHygieneOrgExample;

public interface EnterpriseHygieneOrgService extends BaseService<EnterpriseHygieneOrg, EnterpriseHygieneOrgExample, Long> {
}