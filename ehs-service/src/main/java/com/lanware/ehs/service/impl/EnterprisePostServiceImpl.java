package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterprisePostMapper;
import com.lanware.ehs.pojo.EnterprisePost;
import com.lanware.ehs.pojo.EnterprisePostExample;
import com.lanware.ehs.service.EnterprisePostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterprisePostServiceImpl extends BaseServiceImpl<EnterprisePost, EnterprisePostExample, Long> implements EnterprisePostService {

@Autowired
private EnterprisePostMapper enterprisePostMapper;

@Override
protected Object getMapper() {
	return enterprisePostMapper;
}
}