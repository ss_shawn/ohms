package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseManageFileMapper;
import com.lanware.ehs.pojo.EnterpriseManageFile;
import com.lanware.ehs.pojo.EnterpriseManageFileExample;
import com.lanware.ehs.service.EnterpriseManageFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseManageFileServiceImpl extends BaseServiceImpl<EnterpriseManageFile, EnterpriseManageFileExample, Long> implements EnterpriseManageFileService {

@Autowired
private EnterpriseManageFileMapper enterpriseManageFileMapper;

@Override
protected Object getMapper() {
	return enterpriseManageFileMapper;
}
}