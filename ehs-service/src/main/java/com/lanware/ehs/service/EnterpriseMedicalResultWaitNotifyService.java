package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotifyExample;

public interface EnterpriseMedicalResultWaitNotifyService extends BaseService<EnterpriseMedicalResultWaitNotify, EnterpriseMedicalResultWaitNotifyExample, Long> {
}