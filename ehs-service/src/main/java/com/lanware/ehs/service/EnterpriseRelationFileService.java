package com.lanware.ehs.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import com.lanware.ehs.pojo.custom.EnterpriseRelationFileCustom;

import java.util.Map;

public interface EnterpriseRelationFileService extends BaseService<EnterpriseRelationFile, EnterpriseRelationFileExample, Long> {

    /**
     * 分页查询企业相关文件
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<EnterpriseRelationFileCustom> queryEnterpriseRelationFileByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);
}