package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseTrainingEmployee;
import com.lanware.ehs.pojo.EnterpriseTrainingEmployeeExample;

public interface EnterpriseTrainingEmployeeService extends BaseService<EnterpriseTrainingEmployee, EnterpriseTrainingEmployeeExample, Long> {
}