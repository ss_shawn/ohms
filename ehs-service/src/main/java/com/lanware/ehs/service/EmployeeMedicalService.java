package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EmployeeMedical;
import com.lanware.ehs.pojo.EmployeeMedicalExample;

public interface EmployeeMedicalService extends BaseService<EmployeeMedical, EmployeeMedicalExample, Long> {
}