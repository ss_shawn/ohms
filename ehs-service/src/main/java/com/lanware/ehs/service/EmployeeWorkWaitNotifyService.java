package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EmployeeWorkWaitNotify;
import com.lanware.ehs.pojo.EmployeeWorkWaitNotifyExample;

public interface EmployeeWorkWaitNotifyService extends BaseService<EmployeeWorkWaitNotify, EmployeeWorkWaitNotifyExample, Long> {
}