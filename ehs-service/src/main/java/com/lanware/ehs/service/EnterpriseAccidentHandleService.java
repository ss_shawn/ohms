package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseAccidentHandle;
import com.lanware.ehs.pojo.EnterpriseAccidentHandleExample;

public interface EnterpriseAccidentHandleService extends BaseService<EnterpriseAccidentHandle, EnterpriseAccidentHandleExample, Long> {
}