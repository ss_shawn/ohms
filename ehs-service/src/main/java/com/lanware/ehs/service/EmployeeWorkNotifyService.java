package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EmployeeWorkNotify;
import com.lanware.ehs.pojo.EmployeeWorkNotifyExample;

public interface EmployeeWorkNotifyService extends BaseService<EmployeeWorkNotify, EmployeeWorkNotifyExample, Long> {
}