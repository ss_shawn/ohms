package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseUserRoleMapper;
import com.lanware.ehs.pojo.EnterpriseUserRole;
import com.lanware.ehs.pojo.EnterpriseUserRoleExample;
import com.lanware.ehs.service.EnterpriseUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseUserRoleServiceImpl extends BaseServiceImpl<EnterpriseUserRole, EnterpriseUserRoleExample, Long> implements EnterpriseUserRoleService {

@Autowired
private EnterpriseUserRoleMapper enterpriseUserRoleMapper;

@Override
protected Object getMapper() {
	return enterpriseUserRoleMapper;
}
}