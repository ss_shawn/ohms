package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeePostMapper;
import com.lanware.ehs.pojo.EmployeePost;
import com.lanware.ehs.pojo.EmployeePostExample;
import com.lanware.ehs.service.EmployeePostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeePostServiceImpl extends BaseServiceImpl<EmployeePost, EmployeePostExample, Long> implements EmployeePostService {

@Autowired
private EmployeePostMapper employeePostMapper;

@Override
protected Object getMapper() {
	return employeePostMapper;
}
}