package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseInsideMonitorMapper;
import com.lanware.ehs.pojo.EnterpriseInsideMonitor;
import com.lanware.ehs.pojo.EnterpriseInsideMonitorExample;
import com.lanware.ehs.service.EnterpriseInsideMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseInsideMonitorServiceImpl extends BaseServiceImpl<EnterpriseInsideMonitor, EnterpriseInsideMonitorExample, Long> implements EnterpriseInsideMonitorService {

@Autowired
private EnterpriseInsideMonitorMapper enterpriseInsideMonitorMapper;

@Override
protected Object getMapper() {
	return enterpriseInsideMonitorMapper;
}
}