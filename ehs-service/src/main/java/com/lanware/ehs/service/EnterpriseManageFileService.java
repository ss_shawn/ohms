package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseManageFile;
import com.lanware.ehs.pojo.EnterpriseManageFileExample;

public interface EnterpriseManageFileService extends BaseService<EnterpriseManageFile, EnterpriseManageFileExample, Long> {
}