package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseProtectiveMeasures;
import com.lanware.ehs.pojo.EnterpriseProtectiveMeasuresExample;

public interface EnterpriseProtectiveMeasuresService extends BaseService<EnterpriseProtectiveMeasures, EnterpriseProtectiveMeasuresExample, Long> {
}