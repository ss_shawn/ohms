package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EmployeeHarmContactHistory;
import com.lanware.ehs.pojo.EmployeeHarmContactHistoryExample;

public interface EmployeeHarmContactHistoryService extends BaseService<EmployeeHarmContactHistory, EmployeeHarmContactHistoryExample, Long> {
}