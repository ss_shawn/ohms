package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseAuthMapper;
import com.lanware.ehs.pojo.EnterpriseAuth;
import com.lanware.ehs.pojo.EnterpriseAuthExample;
import com.lanware.ehs.service.EnterpriseAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseAuthServiceImpl extends BaseServiceImpl<EnterpriseAuth, EnterpriseAuthExample, Long> implements EnterpriseAuthService {

@Autowired
private EnterpriseAuthMapper enterpriseAuthMapper;

@Override
protected Object getMapper() {
	return enterpriseAuthMapper;
}
}