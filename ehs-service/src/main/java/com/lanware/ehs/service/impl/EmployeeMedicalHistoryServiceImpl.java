package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeeMedicalHistoryMapper;
import com.lanware.ehs.pojo.EmployeeMedicalHistory;
import com.lanware.ehs.pojo.EmployeeMedicalHistoryExample;
import com.lanware.ehs.service.EmployeeMedicalHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeMedicalHistoryServiceImpl extends BaseServiceImpl<EmployeeMedicalHistory, EmployeeMedicalHistoryExample, Long> implements EmployeeMedicalHistoryService {

@Autowired
private EmployeeMedicalHistoryMapper employeeMedicalHistoryMapper;

@Override
protected Object getMapper() {
	return employeeMedicalHistoryMapper;
}
}