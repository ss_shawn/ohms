package com.lanware.ehs.service;

import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.MonitorUserExample;

public interface MonitorUserService extends BaseService<MonitorUser, MonitorUserExample, Long> {
}