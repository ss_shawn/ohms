package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseDocumentBackup;
import com.lanware.ehs.pojo.EnterpriseDocumentBackupExample;

public interface EnterpriseDocumentBackupService extends BaseService<EnterpriseDocumentBackup, EnterpriseDocumentBackupExample, Long> {
}