package com.lanware.ehs.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.mapper.custom.EnterpriseRelationFileCustomMapper;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import com.lanware.ehs.pojo.custom.EnterpriseRelationFileCustom;
import com.lanware.ehs.service.EnterpriseRelationFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class EnterpriseRelationFileServiceImpl extends BaseServiceImpl<EnterpriseRelationFile, EnterpriseRelationFileExample, Long> implements EnterpriseRelationFileService {

@Autowired
private EnterpriseRelationFileMapper enterpriseRelationFileMapper;

@Autowired
private EnterpriseRelationFileCustomMapper enterpriseRelationFileCustomMapper;

@Override
protected Object getMapper() {
	return enterpriseRelationFileMapper;
}

	@Override
	public Page<EnterpriseRelationFileCustom> queryEnterpriseRelationFileByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
		Page<EnterpriseRelationFileCustom> page = PageHelper.startPage(pageNum, pageSize);
		enterpriseRelationFileCustomMapper.queryEnterpriseRelationFileByCondition(condition);
		return page;
	}
}