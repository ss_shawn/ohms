package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.MonitorUserMapper;
import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.MonitorUserExample;
import com.lanware.ehs.service.MonitorUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MonitorUserServiceImpl extends BaseServiceImpl<MonitorUser, MonitorUserExample, Long> implements MonitorUserService {

@Autowired
private MonitorUserMapper monitorUserMapper;

@Override
protected Object getMapper() {
	return monitorUserMapper;
}
}