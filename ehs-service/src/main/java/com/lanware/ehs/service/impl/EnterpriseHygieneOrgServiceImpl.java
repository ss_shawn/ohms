package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseHygieneOrgMapper;
import com.lanware.ehs.pojo.EnterpriseHygieneOrg;
import com.lanware.ehs.pojo.EnterpriseHygieneOrgExample;
import com.lanware.ehs.service.EnterpriseHygieneOrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class EnterpriseHygieneOrgServiceImpl extends BaseServiceImpl<EnterpriseHygieneOrg, EnterpriseHygieneOrgExample, Long> implements EnterpriseHygieneOrgService {

@Autowired
private EnterpriseHygieneOrgMapper enterpriseHygieneOrgMapper;

@Override
protected Object getMapper() {
	return enterpriseHygieneOrgMapper;
}
}