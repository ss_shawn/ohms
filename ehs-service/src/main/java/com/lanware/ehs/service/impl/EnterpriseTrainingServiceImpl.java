package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseTrainingMapper;
import com.lanware.ehs.pojo.EnterpriseTraining;
import com.lanware.ehs.pojo.EnterpriseTrainingExample;
import com.lanware.ehs.service.EnterpriseTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseTrainingServiceImpl extends BaseServiceImpl<EnterpriseTraining, EnterpriseTrainingExample, Long> implements EnterpriseTrainingService {

@Autowired
private EnterpriseTrainingMapper enterpriseTrainingMapper;

@Override
protected Object getMapper() {
	return enterpriseTrainingMapper;
}
}