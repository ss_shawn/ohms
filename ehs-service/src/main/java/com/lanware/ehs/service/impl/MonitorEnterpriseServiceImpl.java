package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.MonitorEnterpriseMapper;
import com.lanware.ehs.pojo.MonitorEnterprise;
import com.lanware.ehs.pojo.MonitorEnterpriseExample;
import com.lanware.ehs.service.MonitorEnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MonitorEnterpriseServiceImpl extends BaseServiceImpl<MonitorEnterprise, MonitorEnterpriseExample, Long> implements MonitorEnterpriseService {

@Autowired
private MonitorEnterpriseMapper monitorEnterpriseMapper;

@Override
protected Object getMapper() {
	return monitorEnterpriseMapper;
}
}