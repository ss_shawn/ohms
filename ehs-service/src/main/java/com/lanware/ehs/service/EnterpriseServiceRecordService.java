package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseServiceRecord;
import com.lanware.ehs.pojo.EnterpriseServiceRecordExample;

public interface EnterpriseServiceRecordService extends BaseService<EnterpriseServiceRecord, EnterpriseServiceRecordExample, Long> {
}