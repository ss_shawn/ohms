package com.lanware.ehs.service;

import com.lanware.ehs.pojo.SysRecommendInstitution;
import com.lanware.ehs.pojo.SysRecommendInstitutionExample;

public interface SysRecommendInstitutionService extends BaseService<SysRecommendInstitution, SysRecommendInstitutionExample, Long> {
}