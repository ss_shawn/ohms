package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseMedicalResultNotifyMapper;
import com.lanware.ehs.pojo.EnterpriseMedicalResultNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultNotifyExample;
import com.lanware.ehs.service.EnterpriseMedicalResultNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class EnterpriseMedicalResultNotifyServiceImpl extends BaseServiceImpl<EnterpriseMedicalResultNotify, EnterpriseMedicalResultNotifyExample, Long> implements EnterpriseMedicalResultNotifyService {

@Autowired
private EnterpriseMedicalResultNotifyMapper enterpriseMedicalResultNotifyMapper;

@Override
protected Object getMapper() {
	return enterpriseMedicalResultNotifyMapper;
}
}