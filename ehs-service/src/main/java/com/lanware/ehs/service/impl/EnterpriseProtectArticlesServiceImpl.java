package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseProtectArticlesMapper;
import com.lanware.ehs.pojo.EnterpriseProtectArticles;
import com.lanware.ehs.pojo.EnterpriseProtectArticlesExample;
import com.lanware.ehs.service.EnterpriseProtectArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseProtectArticlesServiceImpl extends BaseServiceImpl<EnterpriseProtectArticles, EnterpriseProtectArticlesExample, Long> implements EnterpriseProtectArticlesService {

@Autowired
private EnterpriseProtectArticlesMapper enterpriseProtectArticlesMapper;

@Override
protected Object getMapper() {
	return enterpriseProtectArticlesMapper;
}
}