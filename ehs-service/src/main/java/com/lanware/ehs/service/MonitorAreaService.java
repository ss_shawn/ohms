package com.lanware.ehs.service;

import com.lanware.ehs.pojo.MonitorArea;
import com.lanware.ehs.pojo.MonitorAreaExample;

public interface MonitorAreaService extends BaseService<MonitorArea, MonitorAreaExample, Long> {
}