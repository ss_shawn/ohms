package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseInspectionMapper;
import com.lanware.ehs.pojo.EnterpriseInspection;
import com.lanware.ehs.pojo.EnterpriseInspectionExample;
import com.lanware.ehs.service.EnterpriseInspectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseInspectionServiceImpl extends BaseServiceImpl<EnterpriseInspection, EnterpriseInspectionExample, Long> implements EnterpriseInspectionService {

@Autowired
private EnterpriseInspectionMapper enterpriseInspectionMapper;

@Override
protected Object getMapper() {
	return enterpriseInspectionMapper;
}
}