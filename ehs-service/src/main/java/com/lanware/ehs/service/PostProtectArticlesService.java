package com.lanware.ehs.service;

import com.lanware.ehs.pojo.PostProtectArticles;
import com.lanware.ehs.pojo.PostProtectArticlesExample;

import java.util.List;

public interface PostProtectArticlesService extends BaseService<PostProtectArticles, PostProtectArticlesExample, Long> {

    int insertBatch(List<PostProtectArticles> postProtectArticlesList,Long enterpriseId);

    /**
     * 删除岗位的防护用品时，同时删除待发放的防护用品
     * @param ids
     * @return
     */
    int deleteProtect(List<Long> ids);
}