package com.lanware.ehs.service;

import com.lanware.ehs.pojo.SysConsult;
import com.lanware.ehs.pojo.SysConsultExample;

public interface SysConsultService extends BaseService<SysConsult, SysConsultExample, Long> {
}