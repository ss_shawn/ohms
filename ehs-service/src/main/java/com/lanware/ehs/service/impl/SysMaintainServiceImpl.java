package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.SysMaintainMapper;
import com.lanware.ehs.pojo.SysMaintain;
import com.lanware.ehs.pojo.SysMaintainExample;
import com.lanware.ehs.service.SysMaintainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysMaintainServiceImpl extends BaseServiceImpl<SysMaintain, SysMaintainExample, Long> implements SysMaintainService {

@Autowired
private SysMaintainMapper sysMaintainMapper;

@Override
protected Object getMapper() {
	return sysMaintainMapper;
}
}