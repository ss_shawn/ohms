package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseWorkplaceWaitNotifyMapper;
import com.lanware.ehs.pojo.EnterpriseWorkplaceWaitNotify;
import com.lanware.ehs.pojo.EnterpriseWorkplaceWaitNotifyExample;
import com.lanware.ehs.service.EnterpriseWorkplaceWaitNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseWorkplaceWaitNotifyServiceImpl extends BaseServiceImpl<EnterpriseWorkplaceWaitNotify, EnterpriseWorkplaceWaitNotifyExample, Long> implements EnterpriseWorkplaceWaitNotifyService {

@Autowired
private EnterpriseWorkplaceWaitNotifyMapper enterpriseWorkplaceWaitNotifyMapper;

@Override
protected Object getMapper() {
	return enterpriseWorkplaceWaitNotifyMapper;
}
}