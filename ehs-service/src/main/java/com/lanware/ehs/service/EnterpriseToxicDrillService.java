package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseToxicDrill;
import com.lanware.ehs.pojo.EnterpriseToxicDrillExample;

public interface EnterpriseToxicDrillService extends BaseService<EnterpriseToxicDrill, EnterpriseToxicDrillExample, Long> {
}