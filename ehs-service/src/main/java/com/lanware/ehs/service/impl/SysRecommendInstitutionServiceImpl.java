package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.SysRecommendInstitutionMapper;
import com.lanware.ehs.pojo.SysRecommendInstitution;
import com.lanware.ehs.pojo.SysRecommendInstitutionExample;
import com.lanware.ehs.service.SysRecommendInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysRecommendInstitutionServiceImpl extends BaseServiceImpl<SysRecommendInstitution, SysRecommendInstitutionExample, Long> implements SysRecommendInstitutionService {

@Autowired
private SysRecommendInstitutionMapper sysRecommendInstitutionMapper;

@Override
protected Object getMapper() {
	return sysRecommendInstitutionMapper;
}
}