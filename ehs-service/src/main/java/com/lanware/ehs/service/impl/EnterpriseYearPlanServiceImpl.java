package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseYearPlanMapper;
import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.EnterpriseYearPlanExample;
import com.lanware.ehs.service.EnterpriseYearPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseYearPlanServiceImpl extends BaseServiceImpl<EnterpriseYearPlan, EnterpriseYearPlanExample, Long> implements EnterpriseYearPlanService {

@Autowired
private EnterpriseYearPlanMapper enterpriseYearPlanMapper;

@Override
protected Object getMapper() {
	return enterpriseYearPlanMapper;
}
}