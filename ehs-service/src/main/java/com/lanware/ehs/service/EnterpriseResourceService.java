package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseResource;
import com.lanware.ehs.pojo.EnterpriseResourceExample;

public interface EnterpriseResourceService extends BaseService<EnterpriseResource, EnterpriseResourceExample, Long> {
}