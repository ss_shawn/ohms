package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseOutsideCheckMapper;
import com.lanware.ehs.pojo.EnterpriseOutsideCheck;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckExample;
import com.lanware.ehs.service.EnterpriseOutsideCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseOutsideCheckServiceImpl extends BaseServiceImpl<EnterpriseOutsideCheck, EnterpriseOutsideCheckExample, Long> implements EnterpriseOutsideCheckService {

@Autowired
private EnterpriseOutsideCheckMapper enterpriseOutsideCheckMapper;

@Override
protected Object getMapper() {
	return enterpriseOutsideCheckMapper;
}
}