package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseEquipmentUse;
import com.lanware.ehs.pojo.EnterpriseEquipmentUseExample;

public interface EnterpriseEquipmentUseService extends BaseService<EnterpriseEquipmentUse, EnterpriseEquipmentUseExample, Long> {
}