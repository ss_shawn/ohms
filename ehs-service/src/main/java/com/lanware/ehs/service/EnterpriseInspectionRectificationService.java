package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseInspectionRectification;
import com.lanware.ehs.pojo.EnterpriseInspectionRectificationExample;

public interface EnterpriseInspectionRectificationService extends BaseService<EnterpriseInspectionRectification, EnterpriseInspectionRectificationExample, Long> {
}