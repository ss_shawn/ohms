package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseRoleResourceRelationMapper;
import com.lanware.ehs.pojo.EnterpriseRoleResourceRelation;
import com.lanware.ehs.pojo.EnterpriseRoleResourceRelationExample;
import com.lanware.ehs.service.EnterpriseRoleResourceRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseRoleResourceRelationServiceImpl extends BaseServiceImpl<EnterpriseRoleResourceRelation, EnterpriseRoleResourceRelationExample, Long> implements EnterpriseRoleResourceRelationService {

@Autowired
private EnterpriseRoleResourceRelationMapper enterpriseRoleResourceRelationMapper;

@Override
protected Object getMapper() {
	return enterpriseRoleResourceRelationMapper;
}
}