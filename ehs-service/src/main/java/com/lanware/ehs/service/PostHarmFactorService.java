package com.lanware.ehs.service;

import com.lanware.ehs.pojo.PostHarmFactor;
import com.lanware.ehs.pojo.PostHarmFactorExample;

import java.util.List;

public interface PostHarmFactorService extends BaseService<PostHarmFactor, PostHarmFactorExample, Long> {

    int insertBatch(List<PostHarmFactor> postHarmFactorList);
}