package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseWorkplaceNotifyMapper;
import com.lanware.ehs.pojo.EnterpriseWorkplaceNotify;
import com.lanware.ehs.pojo.EnterpriseWorkplaceNotifyExample;
import com.lanware.ehs.service.EnterpriseWorkplacetNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseWorkplacetNotifyServiceImpl extends BaseServiceImpl<EnterpriseWorkplaceNotify, EnterpriseWorkplaceNotifyExample, Long> implements EnterpriseWorkplacetNotifyService {

@Autowired
private EnterpriseWorkplaceNotifyMapper enterpriseWorkplacetNotifyMapper;

@Override
protected Object getMapper() {
	return enterpriseWorkplacetNotifyMapper;
}
}