package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EmployeeWaitMedicalMapper;
import com.lanware.ehs.pojo.EmployeeWaitMedical;
import com.lanware.ehs.pojo.EmployeeWaitMedicalExample;
import com.lanware.ehs.service.EmployeeWaitMedicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeWaitMedicalServiceImpl extends BaseServiceImpl<EmployeeWaitMedical, EmployeeWaitMedicalExample, Long> implements EmployeeWaitMedicalService {

@Autowired
private EmployeeWaitMedicalMapper employeeWaitMedicalMapper;

@Override
protected Object getMapper() {
	return employeeWaitMedicalMapper;
}
}