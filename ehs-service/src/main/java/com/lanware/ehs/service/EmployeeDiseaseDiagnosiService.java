package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosiExample;

public interface EmployeeDiseaseDiagnosiService extends BaseService<EmployeeDiseaseDiagnosi, EmployeeDiseaseDiagnosiExample, Long> {
}