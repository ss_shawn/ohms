package com.lanware.ehs.service;

import com.lanware.ehs.pojo.SysUser;
import com.lanware.ehs.pojo.SysUserExample;

public interface SysUserService extends BaseService<SysUser, SysUserExample, Long> {
}