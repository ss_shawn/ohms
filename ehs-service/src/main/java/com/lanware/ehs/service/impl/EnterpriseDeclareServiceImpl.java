package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseDeclareMapper;
import com.lanware.ehs.pojo.EnterpriseDeclare;
import com.lanware.ehs.pojo.EnterpriseDeclareExample;
import com.lanware.ehs.service.EnterpriseDeclareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseDeclareServiceImpl extends BaseServiceImpl<EnterpriseDeclare, EnterpriseDeclareExample, Long> implements EnterpriseDeclareService {

@Autowired
private EnterpriseDeclareMapper enterpriseDeclareMapper;

@Override
protected Object getMapper() {
	return enterpriseDeclareMapper;
}
}