package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseDocumentBackupMapper;
import com.lanware.ehs.pojo.EnterpriseDocumentBackup;
import com.lanware.ehs.pojo.EnterpriseDocumentBackupExample;
import com.lanware.ehs.service.EnterpriseDocumentBackupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseDocumentBackupServiceImpl extends BaseServiceImpl<EnterpriseDocumentBackup, EnterpriseDocumentBackupExample, Long> implements EnterpriseDocumentBackupService {

@Autowired
private EnterpriseDocumentBackupMapper enterpriseDocumentBackupMapper;

@Override
protected Object getMapper() {
	return enterpriseDocumentBackupMapper;
}
}