package com.lanware.ehs.service;

import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.EnterpriseYearPlanExample;

public interface EnterpriseYearPlanService extends BaseService<EnterpriseYearPlan, EnterpriseYearPlanExample, Long> {
}