package com.lanware.ehs.service;

import com.lanware.ehs.pojo.SysMaintain;
import com.lanware.ehs.pojo.SysMaintainExample;

public interface SysMaintainService extends BaseService<SysMaintain, SysMaintainExample, Long> {
}