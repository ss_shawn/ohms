package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseAccidentHandleMapper;
import com.lanware.ehs.pojo.EnterpriseAccidentHandle;
import com.lanware.ehs.pojo.EnterpriseAccidentHandleExample;
import com.lanware.ehs.service.EnterpriseAccidentHandleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseAccidentHandleServiceImpl extends BaseServiceImpl<EnterpriseAccidentHandle, EnterpriseAccidentHandleExample, Long> implements EnterpriseAccidentHandleService {

@Autowired
private EnterpriseAccidentHandleMapper enterpriseAccidentHandleMapper;

@Override
protected Object getMapper() {
	return enterpriseAccidentHandleMapper;
}
}