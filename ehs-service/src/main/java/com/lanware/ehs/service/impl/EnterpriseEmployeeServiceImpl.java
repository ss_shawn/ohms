package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseEmployeeMapper;
import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.pojo.EnterpriseEmployeeExample;
import com.lanware.ehs.service.EnterpriseEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseEmployeeServiceImpl extends BaseServiceImpl<EnterpriseEmployee, EnterpriseEmployeeExample, Long> implements EnterpriseEmployeeService {

@Autowired
private EnterpriseEmployeeMapper enterpriseEmployeeMapper;

@Override
protected Object getMapper() {
	return enterpriseEmployeeMapper;
}
}