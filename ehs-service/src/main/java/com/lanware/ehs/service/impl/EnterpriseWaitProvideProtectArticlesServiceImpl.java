package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseWaitProvideProtectArticlesMapper;
import com.lanware.ehs.pojo.EnterpriseWaitProvideProtectArticles;
import com.lanware.ehs.pojo.EnterpriseWaitProvideProtectArticlesExample;
import com.lanware.ehs.service.EnterpriseWaitProvideProtectArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseWaitProvideProtectArticlesServiceImpl extends BaseServiceImpl<EnterpriseWaitProvideProtectArticles, EnterpriseWaitProvideProtectArticlesExample, Long> implements EnterpriseWaitProvideProtectArticlesService {

@Autowired
private EnterpriseWaitProvideProtectArticlesMapper enterpriseWaitProvideProtectArticlesMapper;

@Override
protected Object getMapper() {
	return enterpriseWaitProvideProtectArticlesMapper;
}
}