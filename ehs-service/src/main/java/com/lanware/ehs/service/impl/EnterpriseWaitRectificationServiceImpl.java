package com.lanware.ehs.service.impl;

import com.lanware.ehs.mapper.EnterpriseWaitRectificationMapper;
import com.lanware.ehs.pojo.EnterpriseWaitRectification;
import com.lanware.ehs.pojo.EnterpriseWaitRectificationExample;
import com.lanware.ehs.service.EnterpriseWaitRectificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseWaitRectificationServiceImpl extends BaseServiceImpl<EnterpriseWaitRectification, EnterpriseWaitRectificationExample, Long> implements EnterpriseWaitRectificationService {

@Autowired
private EnterpriseWaitRectificationMapper enterpriseWaitRectificationMapper;

@Override
protected Object getMapper() {
	return enterpriseWaitRectificationMapper;
}
}