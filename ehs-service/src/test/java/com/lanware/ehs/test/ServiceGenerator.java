package com.lanware.ehs.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ServiceGenerator {

    public static void generate(String pojoPath, String servicetPath, String packageName) throws IOException {
        FileOutputStream fileOutputStream = null;
        try {
            File servicetDirectory = new File(servicetPath);
            if (!servicetDirectory.exists()){
                servicetDirectory.mkdirs();
            }
            File servicetImplDirectory = new File(servicetPath + File.separator + "impl");
            if (!servicetImplDirectory.exists()){
                servicetImplDirectory.mkdirs();
            }
            File pojoDirectory = new File(pojoPath);
            for (File pojo : pojoDirectory.listFiles((dir, name) -> !name.endsWith("Example.java"))){
                if (pojo.isDirectory()){
                    continue;
                }
                String name = pojo.getName().replace(".java", "");
                File serviceFile = new File(servicetDirectory, name + "Service.java");
                if (!serviceFile.exists()){
                    serviceFile.createNewFile();
                }
                String serviceContent = "package " + packageName + ".service;\n\n" +
                        "import " + packageName + ".pojo." + name + ";\n" +
                        "import " + packageName + ".pojo." + name + "Example;\n\n" +
                        "public interface " + name + "Service extends BaseService<" + name + ", " + name + "Example, Long> {\n" +
                        "}";
                fileOutputStream = new FileOutputStream(serviceFile);
                fileOutputStream.write(serviceContent.getBytes());
                File serviceImplFile = new File(servicetImplDirectory, name + "ServiceImpl.java");
                if (!serviceImplFile.exists()){
                    serviceImplFile.createNewFile();
                }
                String serviceImplContent = "package " + packageName + ".service.impl;\n\n" +
                        "import " + packageName + ".mapper." + name + "Mapper;\n" +
                        "import " + packageName + ".pojo." + name + ";\n" +
                        "import " + packageName + ".pojo." + name + "Example;\n" +
                        "import " + packageName + ".service." + name + "Service;\n" +
                        "import org.springframework.beans.factory.annotation.Autowired;\n" +
                        "import org.springframework.stereotype.Service;\n\n" +
                        "@Service\n" +
                        "public class " + name + "ServiceImpl extends BaseServiceImpl<" + name + ", " + name + "Example, Long> implements " + name + "Service {\n\n" +
                        "@Autowired\n" +
                        "private " + name + "Mapper " + FirstCharaterToLowerCase(name) + "Mapper;\n\n" +
                        "@Override\n" +
                        "protected Object getMapper() {\n" +
                        "\treturn " + FirstCharaterToLowerCase(name) + "Mapper;\n" +
                        "}\n" +
                        "}";
                fileOutputStream = new FileOutputStream(serviceImplFile);
                fileOutputStream.write(serviceImplContent.getBytes());
            }
        }finally {
            if (fileOutputStream != null){
                fileOutputStream.close();
            }
        }
    }

    public static String FirstCharaterToLowerCase(String value){
        char c = value.charAt(0);
        return value.replaceFirst(String.valueOf(c), String.valueOf(Character.toLowerCase(c)));
    }

    public static void main(String[] args) throws IOException {
        ServiceGenerator.generate("F:\\IntelliJ_workspace\\ehs\\ehs-mapper\\src\\main\\java\\com\\lanware\\ehs\\pojo", "F:\\IntelliJ_workspace\\ehs\\ehs-service\\src\\main\\java\\com\\lanware\\ehs\\service", "com.lanware.ehs");
    }
}
