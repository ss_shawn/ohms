package com.lanware.ehs.enterprise.notify.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.EnterpriseWorkplaceWaitNotify;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceWaitNotifyCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface EnterpriseWorkplaceNotifyService {
    /**
     * @description 根据id查询待作业场所告知
     * @param id 待作业场所告知id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseWorkplaceWaitNotify对象
     */
     EnterpriseWorkplaceWaitNotify getEnterpriseWorkplaceWaitNotifyById(Long id);
    /**
     * @description 根据id查询待作业场所告知
     * @param condition 待作业场所告知条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseWorkplaceWaitNotify对象
     */
     Page<EnterpriseWorkplaceWaitNotifyCustom> findEnterpriseWorkplaceWaitNotifyList(Map<String, Object> condition, QueryRequest request);
    /**
     * 保存作业场所告知,
     * @param enterpriseWorkplaceNotifyCustom 作业场所告知,notifyFile 检测结果公示证明  harmWarnImage 危害警示标示图片
     * @return成功与否 1成功 0不成功
     */
    int saveEnterpriseWorkplaceNotify(EnterpriseWorkplaceNotifyCustom enterpriseWorkplaceNotifyCustom, MultipartFile notifyFile,MultipartFile harmWarnImage);
    /**
     * 通过id获取作业场所告知
     * @param id 作业场所告知id
     * @return作业场所告知
     */
     EnterpriseWorkplaceNotifyCustom getEnterpriseWorkplaceNotifyCustomByID(long id);
    /**
     * 删除待作业场所告知,
     * @param id
     * @return成功与否 1成功 0不成功
     */
     int deleteEnterpriseWorkplaceNotify(long id);
    /**
     * 通过待作业场所告知ID获取待作业场所告知的人员和岗位信息,放入作业场所告知返回
     * @param id 待作业场所告知ID
     * @return 作业场所告知
     */
     EnterpriseWorkplaceNotifyCustom getEnterpriseWorkplaceNotifyCustomByWaitID(long id);
    /**
     * @description 根据条件查询作业场所告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    Page<EnterpriseWorkplaceNotifyCustom> findEnterpriseWorkplaceNotifyList(Map<String, Object> condition, QueryRequest request);
}
