package com.lanware.ehs.enterprise.basicinfo.service;

import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.custom.EnterpriseBaseinfoCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BasicInfoService {

    /**
     * 保存企业基础信息
     * @param enterpriseBaseinfo 企业对象
     * @return 更新数量
     */
    int saveEnterpriseBaseinfo(EnterpriseBaseinfo enterpriseBaseinfo);

    /**
     * 根据企业ID查找企业信息
     * @return
     */
    EnterpriseBaseinfo findById(Long id);

    /**
     * 查找企业数据
     * @return
     */
    List<EnterpriseBaseinfo> find(EnterpriseBaseinfo enterpriseBaseinfo);
}
