package com.lanware.ehs.enterprise.government.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.pojo.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.List;
import java.util.Map;

public interface GovernmentService {
    /**
     * 根据企业ID获取提交的企业信息
     * @param enterpriseId 企业ID
     * @return
     */
    GovernmentEnterpriseBaseinfo getGovernmentEnterpriseBaseinfoByEnterpriseId(Long enterpriseId);
    /**
     * 保存提交的企业信息
     * @param governmentEnterpriseBaseinfo
     * @return 成功数量
     */
    int getGovernmentEnterpriseBaseinfoByEnterpriseId(GovernmentEnterpriseBaseinfo governmentEnterpriseBaseinfo);
    /**
     * 提交企业信息
     * @param enterpriseId
     * @return 是否成功
     */
     int uploadGovernment(Long enterpriseId );
}
