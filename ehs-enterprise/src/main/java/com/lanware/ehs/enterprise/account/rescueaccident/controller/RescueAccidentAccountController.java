package com.lanware.ehs.enterprise.account.rescueaccident.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.accidenthandle.service.AccidentHandleService;
import com.lanware.ehs.enterprise.toxicdrill.service.ToxicDrillService;
import com.lanware.ehs.pojo.EnterpriseToxicDrill;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseAccidentHandleCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 应急救援台帐controller
 */
@Controller
@RequestMapping("/rescueAccidentAccount")
public class RescueAccidentAccountController {
    /** 注入toxicDrillService的bean */
    @Autowired
    private ToxicDrillService toxicDrillService;
    /** 注入accidentHandleService的bean */
    @Autowired
    private AccidentHandleService accidentHandleService;

    /** 下载word的临时路径 */
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;

    @RequestMapping("")
    /**
     * @description 返回应急救援台帐页面
     * @param enterpriseUser 当前登录企业
     * @return ModelAndView
     */
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/rescueAccident/list", data);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(RescueAccidentAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 事故调查处理word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadAccidentHandle")
    @ResponseBody
    public ResultFormat downloadAccidentHandle(Long enterpriseId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 事故调查处理
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        JSONObject result = accidentHandleService.listAccidentHandles(0, 0, condition);
        List<EnterpriseAccidentHandleCustom> enterpriseAccidentHandles
                = (List<EnterpriseAccidentHandleCustom>)result.get("enterpriseAccidentHandles");
        List accidentHandleList = new ArrayList();
        for (EnterpriseAccidentHandleCustom enterpriseAccidentHandleCustom : enterpriseAccidentHandles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("accidentDate", sdf.format(enterpriseAccidentHandleCustom.getAccidentDate()));
            temp.put("accidentReason", enterpriseAccidentHandleCustom.getAccidentReason());
            temp.put("handleProcess", enterpriseAccidentHandleCustom.getHandleProcess());
            temp.put("responsibilityAnalysis", enterpriseAccidentHandleCustom.getResponsibilityAnalysis());
            temp.put("dutyPersonHandle", enterpriseAccidentHandleCustom.getDutyPersonHandle());
            temp.put("measures", enterpriseAccidentHandleCustom.getMeasures());
            accidentHandleList.add(temp);
        }
        dataMap.put("accidentHandleList", accidentHandleList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(RescueAccidentAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("accidentHandleMould.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "事故调查处理.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("事故调查处理.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 应急救援台账zip下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadRescueAccident")
    @ResponseBody
    public ResultFormat downloadRescueAccident(Long enterpriseId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> dataMap1 = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 事故调查处理
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        JSONObject result = accidentHandleService.listAccidentHandles(0, 0, condition);
        List<EnterpriseAccidentHandleCustom> enterpriseAccidentHandles
                = (List<EnterpriseAccidentHandleCustom>)result.get("enterpriseAccidentHandles");
        List accidentHandleList = new ArrayList();
        for (EnterpriseAccidentHandleCustom enterpriseAccidentHandleCustom : enterpriseAccidentHandles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("accidentDate", sdf.format(enterpriseAccidentHandleCustom.getAccidentDate()));
            temp.put("accidentReason", enterpriseAccidentHandleCustom.getAccidentReason());
            temp.put("handleProcess", enterpriseAccidentHandleCustom.getHandleProcess());
            temp.put("responsibilityAnalysis", enterpriseAccidentHandleCustom.getResponsibilityAnalysis());
            temp.put("dutyPersonHandle", enterpriseAccidentHandleCustom.getDutyPersonHandle());
            temp.put("measures", enterpriseAccidentHandleCustom.getMeasures());
            accidentHandleList.add(temp);
        }
        dataMap.put("accidentHandleList", accidentHandleList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(RescueAccidentAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("accidentHandleMould.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "事故调查处理.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            //放入压缩列表
            files.add(uploadPath);
            String uploadZipPath = upload + File.separator + "应急救援台账.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            File uploadZipFile = new File(uploadZipPath);
            FileInputStream fis = new FileInputStream(uploadZipFile);
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("应急救援台账.zip", "utf-8"));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = fis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            fis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 应急救援台账文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadRescueAccidentAccount")
    @ResponseBody
    public void downloadRescueAccidentAccount(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            Map<String, Object> dataMap = getRescueAccidentAccountMap(enterpriseId,request);
            String uploadPath = upload + File.separator + "职业危害应急救援和事故台帐.doc";
            exeWord(dataMap,uploadPath,"rescueAccidentAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业危害应急救援和事故台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到应急救援台账文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getRescueAccidentAccountMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 应急救援演练记录
        JSONObject result = toxicDrillService.listToxicDrills(0, 0, condition);
        List<EnterpriseToxicDrill> enterpriseToxicDrills
                = (List<EnterpriseToxicDrill>)result.get("enterpriseToxicDrills");
        List drillList = new ArrayList();
        for (EnterpriseToxicDrill enterpriseToxicDrill : enterpriseToxicDrills) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("drillDate", sdf.format(enterpriseToxicDrill.getDrillDate()));
            temp.put("description", enterpriseToxicDrill.getDescription());
            String content  = enterpriseToxicDrill.getContent();
            temp.put("content", (content == null || content.equals("")) ? "-" : "已上传");
            String proveFile  = enterpriseToxicDrill.getProveFile();
            temp.put("proveFile", (proveFile == null || proveFile.equals("")) ? "-" : "已上传");
            drillList.add(temp);
        }
        dataMap.put("drillList", drillList);
        // 事故调查处理
        JSONObject result1 = accidentHandleService.listAccidentHandles(0, 0, condition);
        List<EnterpriseAccidentHandleCustom> enterpriseAccidentHandles
                = (List<EnterpriseAccidentHandleCustom>)result1.get("enterpriseAccidentHandles");
        List accidentHandleList = new ArrayList();
        for (EnterpriseAccidentHandleCustom enterpriseAccidentHandleCustom : enterpriseAccidentHandles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("accidentDate", sdf.format(enterpriseAccidentHandleCustom.getAccidentDate()));
            temp.put("accidentReason", enterpriseAccidentHandleCustom.getAccidentReason());
            temp.put("handleProcess", enterpriseAccidentHandleCustom.getHandleProcess());
            temp.put("responsibilityAnalysis", enterpriseAccidentHandleCustom.getResponsibilityAnalysis());
            temp.put("dutyPersonHandle", enterpriseAccidentHandleCustom.getDutyPersonHandle());
            temp.put("measures", enterpriseAccidentHandleCustom.getMeasures());
            accidentHandleList.add(temp);
        }
        dataMap.put("accidentHandleList", accidentHandleList);
        return dataMap;
    }
}
