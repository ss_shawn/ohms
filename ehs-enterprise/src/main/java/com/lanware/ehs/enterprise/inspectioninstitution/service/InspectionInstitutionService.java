package com.lanware.ehs.enterprise.inspectioninstitution.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @description 检查机构管理interface
 */
public interface InspectionInstitutionService {
    /**
     * @description 查询检查机构list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseInspectionInstitutions和totalNum的result
     */
    JSONObject listInspectionInstitutions(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据机构类型获取检查机构result
     * @param type 机构类型
     * @param enterpriseId 当前企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    JSONObject listInspectionInstitutionsByType(String type, Long enterpriseId);

    /**
     * @description 根据id查询检查机构信息
     * @param id 检查机构id
     * @return com.lanware.management.pojo.EnterpriseInspection 返回EnterpriseInspection对象
     */
    EnterpriseInspectionInstitution getInspectionInstitutionById(Long id);

    /**
     * @description 新增检查机构信息
     * @param enterpriseInspectionInstitution 检查机构pojo
     * @param contractFile 机构合同
     * @return int 插入成功的数目
     */
    int insertInspectionInstitution(EnterpriseInspectionInstitution enterpriseInspectionInstitution
            , MultipartFile contractFile);

    /**
     * @description 更新检查机构信息
     * @param enterpriseInspectionInstitution 检查机构pojo
     * @param contractFile 机构合同
     * @return int 更新成功的数目
     */
    int updateInspectionInstitution(EnterpriseInspectionInstitution enterpriseInspectionInstitution
            , MultipartFile contractFile);

    /**
     * @description 删除检查机构信息
     * @param id 要删除的id
     * @param contract 要删除的机构合同路径
     * @return int 删除成功的数目
     */
    int deleteInspectionInstitution(Long id, String contract);

    /**
     * @description 批量删除检查机构信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteInspectionInstitutions(Long[] ids);
}
