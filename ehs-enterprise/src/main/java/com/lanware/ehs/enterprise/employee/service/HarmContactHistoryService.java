package com.lanware.ehs.enterprise.employee.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.EmployeeHarmContactHistory;
import com.lanware.ehs.pojo.custom.EmployeeHarmContactHistoryCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectiveMeasuresCustom;

import java.util.List;
import java.util.Map;

public interface HarmContactHistoryService {

    /**
     * 分页查询职业危害接触史
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<EmployeeHarmContactHistoryCustom> queryEmployeeHarmContactHistoryByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 根据岗位id查询防护措施
     * @param postId 岗位id
     * @return 查询结果
     */
    List<EnterpriseProtectiveMeasuresCustom> queryProtectEnterpriseProtectiveMeasuresByPostId(Long postId);

    /**
     * 根据id查询职业危害接触史
     * @param id id
     * @return 查询结果
     */
    EmployeeHarmContactHistoryCustom queryEmployeeHarmContactHistoryById(Long id);
}
