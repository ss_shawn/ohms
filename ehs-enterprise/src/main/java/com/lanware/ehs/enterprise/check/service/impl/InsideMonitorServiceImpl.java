package com.lanware.ehs.enterprise.check.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.check.service.InsideMonitorService;
import com.lanware.ehs.mapper.EnterpriseInsideMonitorMapper;
import com.lanware.ehs.mapper.EnterpriseMonitorPointMapper;
import com.lanware.ehs.mapper.EnterpriseWorkplaceMapper;
import com.lanware.ehs.mapper.custom.EnterpriseInsideMonitorCustomMapper;
import com.lanware.ehs.mapper.custom.MonitorWorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.mapper.custom.WorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseInsideMonitorCustom;
import com.lanware.ehs.pojo.custom.MonitorWorkplaceHarmFactorCustom;
import com.lanware.ehs.pojo.custom.WorkplaceHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 企业内部监测interface实现类
 */
public class InsideMonitorServiceImpl implements InsideMonitorService {
    @Autowired
    /** 注入enterpriseInsideMonitorMapper的bean */
    private EnterpriseInsideMonitorMapper enterpriseInsideMonitorMapper;
    @Autowired
    /** 注入enterpriseInsideMonitorCustomMapper的bean */
    private EnterpriseInsideMonitorCustomMapper enterpriseInsideMonitorCustomMapper;
    @Autowired
    /** 注入enterpriseMonitorPointMapper的bean */
    private EnterpriseMonitorPointMapper enterpriseMonitorPointMapper;
    @Autowired
    /** 注入enterpriseWorkplaceMapper的bean */
    private EnterpriseWorkplaceMapper enterpriseWorkplaceMapper;
    @Autowired
    /** 注入workplaceHarmFactorCustomMapper的bean */
    private WorkplaceHarmFactorCustomMapper workplaceHarmFactorCustomMapper;
    @Autowired
    /** 注入monitorWorkplaceHarmFactorCustomMapper的bean */
    private MonitorWorkplaceHarmFactorCustomMapper monitorWorkplaceHarmFactorCustomMapper;
    
    @Override
    /**
     * @description 查询日常监测list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseOutsideChecks和totalNum的result
     */
    public JSONObject listInsideMonitors(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseInsideMonitorCustom> enterpriseInsideMonitors = enterpriseInsideMonitorCustomMapper.listInsideMonitors(condition);
        result.put("enterpriseInsideMonitors", enterpriseInsideMonitors);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseInsideMonitorCustom> pageInfo = new PageInfo<EnterpriseInsideMonitorCustom>(enterpriseInsideMonitors);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据企业id获取监测点result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseMonitorPoints的result
     */
    @Override
    public JSONObject listMonitorPointsByEnterpriseId(Long enterpriseId) {
        JSONObject result = new JSONObject();
        EnterpriseMonitorPointExample enterpriseMonitorPointExample = new EnterpriseMonitorPointExample();
        enterpriseMonitorPointExample.createCriteria().andEnterpriseIdEqualTo(enterpriseId);
        List<EnterpriseMonitorPoint> enterpriseMonitorPoints
                = enterpriseMonitorPointMapper.selectByExample(enterpriseMonitorPointExample);
        result.put("enterpriseMonitorPoints", enterpriseMonitorPoints);
        return result;
    }

    /**
     * @description 根据监测点id获取作业场所result
     * @param monitorPointId 监测点id
     * @return com.alibaba.fastjson.JSONObject 返回包含monitorWorkplaceHarmFactorCustoms的result
     */
    @Override
    public JSONObject listWorkplacesByMonitorId(Long monitorPointId) {
        JSONObject result = new JSONObject();
        List<MonitorWorkplaceHarmFactorCustom> monitorWorkplaces
                = monitorWorkplaceHarmFactorCustomMapper.listWorkplacesByMonitorId(monitorPointId);
        result.put("monitorWorkplaces", monitorWorkplaces);
        return result;
    }

    /**
     * @description 根据监测点id和工作场所id获取危害因素result
     * @param monitorPointId 监测点id
     * @param workplaceId 工作场所id
     * @return com.alibaba.fastjson.JSONObject 返回包含monitorWorkplaceHarmFactors的result
     */
    @Override
    public JSONObject listHarmFactorsByMonitorWorkplaceId(Long monitorPointId, Long workplaceId) {
        JSONObject result = new JSONObject();
        List<MonitorWorkplaceHarmFactorCustom> monitorWorkplaceHarmFactors
                = monitorWorkplaceHarmFactorCustomMapper.listHarmFactorsByMonitorWorkplaceId(monitorPointId, workplaceId);
        result.put("monitorWorkplaceHarmFactors", monitorWorkplaceHarmFactors);
        return result;
    }

    /**
     * @description 根据企业id获取工作场所result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含baseHarmFactors的result
     */
    @Override
    public JSONObject listWorkplacesByEnterpriseId(Long enterpriseId) {
        JSONObject result = new JSONObject();
        EnterpriseWorkplaceExample enterpriseWorkplaceExample = new EnterpriseWorkplaceExample();
        EnterpriseWorkplaceExample.Criteria criteria = enterpriseWorkplaceExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        List<EnterpriseWorkplace> enterpriseWorkplaces
                = enterpriseWorkplaceMapper.selectByExample(enterpriseWorkplaceExample);
        result.put("enterpriseWorkplaces", enterpriseWorkplaces);
        return result;
    }

    /**
     * @description 获取危害因素result
     * @param workplaceId 工作场所id
     * @return com.alibaba.fastjson.JSONObject 返回包含workplaceHarmFacotrCustoms的result
     */
    @Override
    public JSONObject listHarmfactorsByWorkplaceId(Long workplaceId) {
        JSONObject result = new JSONObject();
        List<WorkplaceHarmFactorCustom> workplaceHarmFactorCustoms
                = workplaceHarmFactorCustomMapper.listHarmFactorsByWorkplaceId(workplaceId);
        result.put("workplaceHarmFactorCustoms", workplaceHarmFactorCustoms);
        return result;
    }

    /**
     * @description 根据id查询日常监测信息
     * @param id 日常监测id
     * @return com.lanware.management.pojo.EnterpriseInsideMonitor 返回EnterpriseInsideMonitor对象
     */
    @Override
    public EnterpriseInsideMonitor getInsideMonitorById(Long id) {
        return enterpriseInsideMonitorMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 插入日常监测信息
     * @param enterpriseInsideMonitor 日常监测pojo
     * @return 插入成功的数目
     */
    @Override
    public int insertInsideMonitor(EnterpriseInsideMonitor enterpriseInsideMonitor) {
        return enterpriseInsideMonitorMapper.insert(enterpriseInsideMonitor);
    }

    /**
     * @description 更新日常监测信息
     * @param enterpriseInsideMonitor 日常监测pojo
     * @return 更新成功的数目
     */
    @Override
    public int updateInsideMonitor(EnterpriseInsideMonitor enterpriseInsideMonitor) {
        return enterpriseInsideMonitorMapper.updateByPrimaryKeySelective(enterpriseInsideMonitor);
    }

    /**
     * @description 删除日常监测信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    public int deleteInsideMonitor(Long id) {
        return enterpriseInsideMonitorMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除日常监测信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteInsideMonitors(Long[] ids) {
        EnterpriseInsideMonitorExample enterpriseInsideMonitorExample = new EnterpriseInsideMonitorExample();
        EnterpriseInsideMonitorExample.Criteria criteria = enterpriseInsideMonitorExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseInsideMonitorMapper.deleteByExample(enterpriseInsideMonitorExample);
    }
}
