package com.lanware.ehs.enterprise.employee.service.impl;

import com.github.pagehelper.Page;
import com.lanware.ehs.enterprise.employee.service.EmployeeArchivesService;
import com.lanware.ehs.mapper.*;
import com.lanware.ehs.mapper.custom.*;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EmployeeMedicalCustom;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultAllCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProvideProtectArticlesCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeArchivesServiceImpl implements EmployeeArchivesService {
    /** 注入employeeDiseaseDiagnosiMapper的bean */
    @Autowired
    private EmployeeDiseaseDiagnosiMapper employeeDiseaseDiagnosiMapper;

    /** 注入EmployeeMedicalCustomMapper的bean */
    @Autowired
    private EmployeeMedicalCustomMapper employeeMedicalCustomMapper;
    @Autowired
    private EnterpriseOutsideCheckResultAllCustomMapper enterpriseOutsideCheckResultAllCustomMapper;
    @Autowired
    private EnterpriseProvideProtectArticlesCustomMapper enterpriseProvideProtectArticlesCustomMapper;
    /**
     * 根据员工ID获取职业病诊断
     * @param employeeId 员工ID
     * @return 职业病诊断列表
     */
    @Override
    public List<EmployeeDiseaseDiagnosi> getEmployeeDiseaseDiagnosiByEmployeeId(Long employeeId){
        EmployeeDiseaseDiagnosiExample employeeDiseaseDiagnosiExample=new EmployeeDiseaseDiagnosiExample();
        employeeDiseaseDiagnosiExample.createCriteria().andEmployeeIdEqualTo(employeeId);
        employeeDiseaseDiagnosiExample.setOrderByClause("diagnosi_date desc");
        return employeeDiseaseDiagnosiMapper.selectByExample(employeeDiseaseDiagnosiExample);
    }

    /**
     * 根据员工ID获取历次职业健康检查结果及处理情况
     * @param employeeId 员工ID
     * @return 体检列表
     */
    @Override
    public List<EmployeeMedicalCustom> getEmployeeMedicalByEmployeeId(Long employeeId){
        Map<String, Object> condition=new HashMap();
        condition.put("employeeId",employeeId);
        List<EmployeeMedicalCustom> employeeMedicals = employeeMedicalCustomMapper.listMedicals(condition);
        return employeeMedicals;
    }
    /**
     * 根据员工ID获取工作场所职业病危害因素检测结果
     * @param employeeId 员工ID
     * @return 工作场所职业病危害因素检测结果列表
     */
    @Override
    public  List<EnterpriseOutsideCheckResultAllCustom> getWorkplaceOutsideCheckByEmployeeId(Long employeeId){

        List<EnterpriseOutsideCheckResultAllCustom> employeeMedicals = enterpriseOutsideCheckResultAllCustomMapper.selectWorkplaceOutsideCheckByEmployeeId(employeeId);
        return employeeMedicals;
    }
    /**
     * 根据员工ID获取防护用品发放记录
     * @param condition 员工ID
     * @return 工作场所职业病危害因素检测结果列表
     */
    @Override
    public Page<EnterpriseProvideProtectArticlesCustom> getEmployeeProvideProtectList(Map<String, Object> condition){

        Page<EnterpriseProvideProtectArticlesCustom> employeeMedicals = enterpriseProvideProtectArticlesCustomMapper.findProtectList(condition);
        return employeeMedicals;
    }
}
