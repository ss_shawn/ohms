package com.lanware.ehs.enterprise.prevention.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.prevention.service.PreventionService;
import com.lanware.ehs.pojo.EnterpriseManageFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseManageFileCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/prevention")
/**
 * @description 职业健康防治制度controller
 */
public class PreventionController {
    @Autowired
    /** 注入preventionService的bean */
    private PreventionService preventionService;
    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    @RequestMapping("")
    /**
     * @description 返回职业健康防范制度页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "prevention/list";
    }

    @RequestMapping("/listPreventions")
    @ResponseBody
    /**
     * @description 获取职业健康防范制度result
     * @param enterpriseUser 当前登录企业
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat listPreventions(@CurrentUser EnterpriseUser enterpriseUser) {
        Map<String ,Object> result = new HashMap<String, Object>();
        Map<String ,Object> condition = new HashMap<String, Object>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("module", EnterpriseManageFileCustom.MODULE_PREVENTION);
        List<EnterpriseManageFile> enterpriseManageFileList = preventionService.listPreventionFiles(condition);
        for (EnterpriseManageFile enterpriseManageFile : enterpriseManageFileList) {
            result.put(enterpriseManageFile.getFileType(), enterpriseManageFile);
        }
        return ResultFormat.success(result);
    }

    @RequestMapping("/listPrevention")
    @ResponseBody
    /**
     * @description 获取职业健康防范制度result(文件对象的list)
     * @param enterpriseUser 当前登录企业
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat listPrevention(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject result = new JSONObject();
        Map<String ,Object> condition = new HashMap<String, Object>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("module", EnterpriseManageFileCustom.MODULE_PREVENTION);
        List<EnterpriseManageFile> enterpriseManageFiles = preventionService.listPreventionFiles(condition);
        result.put("enterpriseManageFiles", enterpriseManageFiles);
        return ResultFormat.success(result);
    }

    @RequestMapping("/getRescue")
    @ResponseBody
    /**
     * @description 获取应急救援预案对象
     * @param enterpriseUser 当前登录企业
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat getRescue(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject result = new JSONObject();
        List<EnterpriseManageFile> enterpriseManageFiles
                = preventionService.getRescue(enterpriseUser.getEnterpriseId());
        result.put("enterpriseManageFiles", enterpriseManageFiles);
        return ResultFormat.success(result);
    }

    @RequestMapping("/uploadFile")
    @ResponseBody
    /**
     * @description
     * @param file 上传的文件对象
     * @param type 上传文件的类型（初始化时固定好）
     * @param filePath 上传文件的路径（如果不为空，就是更新上传文件）
     * @return com.lanware.ehs.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat uploadFile(MultipartFile file, String type, String filePath
            , @CurrentUser EnterpriseUser enterpriseUser) {
        String preventionPath = "/" + enterpriseUser.getEnterpriseId() + "/" + ModuleName.PREVENTION.getValue()
                + "/" + type + "/" +file.getOriginalFilename();
        if (filePath.isEmpty()) {
            // 创建文件数据
            EnterpriseManageFile enterpriseManageFile = new EnterpriseManageFile();
            enterpriseManageFile.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseManageFile.setModule(EnterpriseManageFileCustom.MODULE_PREVENTION);
            enterpriseManageFile.setFileName(file.getOriginalFilename());
            enterpriseManageFile.setFileType(type);
            enterpriseManageFile.setFilePath(preventionPath);
            enterpriseManageFile.setGmtCreate(new Date());
            // 文件上传到oss服务器，并保存文件数据
            try {
                ossTools.uploadStream(preventionPath, file.getInputStream());
                preventionService.insertEnterpriseManageFile(enterpriseManageFile);
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传职业健康防范制度文件失败", e);
            }
        } else {
            // 删除相应的oss上的文件
            ossTools.deleteOSS(filePath);
            // 更新文件数据
            EnterpriseManageFile enterpriseManageFile = new EnterpriseManageFile();
            enterpriseManageFile.setFileName(file.getOriginalFilename());
            enterpriseManageFile.setFilePath(preventionPath);
            enterpriseManageFile.setGmtModified(new Date());
            // 文件上传到oss服务器，并保存文件数据
            try {
                ossTools.uploadStream(preventionPath, file.getInputStream());
                preventionService.updateEnterpriseManageFile(enterpriseManageFile, type
                        , enterpriseUser.getEnterpriseId());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传职业健康防范制度文件失败", e);
            }
        }
        return ResultFormat.success("上传文件成功");
    }

    @RequestMapping("/deleteFile")
    @ResponseBody
    /**
     * @description
     * @param filePath 要删除文件的路径
     * @return com.lanware.ehs.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat deleteFile(String filePath) {
        // 删除数据库上对应的文件数据
        if (preventionService.deleteFileByFilePath(filePath) > 0) {
            // 删除相应的oss上对应的文件
            ossTools.deleteOSS(filePath);
            return ResultFormat.success("删除文件成功");
        }else{
            return ResultFormat.error("删除文件失败");
        }
    }
}
