package com.lanware.ehs.enterprise.job.task;

import com.google.common.collect.Maps;
import com.lanware.ehs.enterprise.check.service.WaitCheckService;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.pojo.EnterpriseWaitCheck;
import com.lanware.ehs.pojo.WaitCheckHarmFactor;
import com.lanware.ehs.pojo.custom.EnterpriseWaitCheckCustom;
import com.lanware.ehs.pojo.custom.EnterpriseYearPlanCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 检测计划任务
 */
@Service
@Slf4j
public class CheckPlanTask {
    @Autowired
    private YearplanService yearplanService;

    @Autowired
    private WaitCheckService waitCheckService;

    /**
     * 执行检测计划任务
     */
    public void executeCheckPlanTask() {
        //获取当前时间
        LocalDate localDate = LocalDate.now();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        try {
            //查询年度计划危害因素检测执行月份。
            Map<String, Object> condition = Maps.newHashMap();
            condition.put("year", year);
            condition.put("planType", "whysjc");
            List<EnterpriseYearPlanCustom> yearPlans = yearplanService.listYearCheckPlans(condition);
            for (EnterpriseYearPlanCustom yearPlan : yearPlans) {
                String planPeriod = yearPlan.getPlanPeriod();
                Integer checkMonth = Integer.parseInt(planPeriod);
                if (checkMonth == month) {
                    addWaitCheckHarmFactor(checkMonth, yearPlan);
                }
            }
        } catch (Exception e) {
            log.error("{}-{}定时生成危害因素检测提醒失败",year,month,e);
        }
    }

    @Transactional
    public void addWaitCheckHarmFactor(Integer checkMonth, EnterpriseYearPlanCustom yearPlan) {
        String year = yearPlan.getYear();
        //根据检测计划id查看待检测记录表是否已经生成待检测记录，如果未生成，则生成检测计划
        Map<String, Object> parameter = Maps.newHashMap();
        parameter.put("checkPlanId", yearPlan.getId());
        parameter.put("checkMonth", checkMonth);
        List<EnterpriseWaitCheckCustom> waitCheckCustoms = waitCheckService.findEnterpriseWaitChecks(parameter);
        if (waitCheckCustoms.size() > 0) {
            //已经存在待检测记录，不在更新

        } else {
            EnterpriseWaitCheck waitCheck = new EnterpriseWaitCheck();
            waitCheck.setEnterpriseId(yearPlan.getEnterpriseId());
            waitCheck.setYear(Integer.parseInt(year));
            waitCheck.setCheckMonth(checkMonth);
            waitCheck.setGmtCreate(new Date());
            waitCheck.setCheckPlanId(yearPlan.getId());
            waitCheck.setStatus(EnterpriseWaitCheckCustom.STATUS_CHECK_NO);
            waitCheckService.insert(waitCheck);

            String[] harmFactorIds = yearPlan.getTempHarmFactorId().split(",");
            for (String harmFactorId : harmFactorIds) {
                WaitCheckHarmFactor waitCheckHarmFactor = new WaitCheckHarmFactor();
                waitCheckHarmFactor.setWaitCheckId(waitCheck.getId());
                waitCheckHarmFactor.setHarmFactorId(Long.parseLong(harmFactorId));
                waitCheckHarmFactor.setGmtCreate(new Date());
                waitCheckService.insertWaitCheckHarmFactor(waitCheckHarmFactor);
            }
        }
    }
}