package com.lanware.ehs.enterprise.medical.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.medical.service.WaitMedicalService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EmployeeWaitMedicalCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 待体检管理controller
 */
@Controller
@RequestMapping("/waitMedical")
public class WaitMedicalController {
    @Autowired
    /** 注入waitMedicalService的bean */
    private WaitMedicalService waitMedicalService;

    @RequestMapping("")
    /**
     * @description 返回检测管理页面
     * @param
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "medical/list";
    }

    @RequestMapping("/listWaitMedicals")
    @ResponseBody
    /**
     * @description 获取待体检人员result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 当前登录用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listWaitMedicals(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("status", EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = waitMedicalService.listWaitMedicals(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /**
     * @description 获取岗中待体检人员result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 当前登录用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/listPostWaitMedicals")
    @ResponseBody
    public ResultFormat listPostWaitMedicals(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("status", EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
        condition.put("type", EmployeeWaitMedicalCustom.TYPE_IN_POST);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = waitMedicalService.listWaitMedicals(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /** @description 删除待体检记录，并保存未体检原因
     * @param id 待体检id
     * @param reason 未体检原因
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/deleteWaitMedical")
    @ResponseBody
    public ResultFormat deleteWaitMedical(Long id, String reason) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("id", id);
        condition.put("reason", reason);
        condition.put("status", EmployeeWaitMedicalCustom.STATUS_MEDICAL_DEL);
        condition.put("gmtModified", new Date());
        if (waitMedicalService.updateWaitMedicalById(condition) > 0) {
            return ResultFormat.success("删除成功");
        } else {
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/listYearWaitMedicals")
    @ResponseBody
    /**
     * @description 获取待体检年度报表
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @param enterpriseUser 当前登录用户
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listYearWaitMedicals(String startDate, String endDate, @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String, Object> condition = new HashMap<>();
        if (startDate != null && !startDate.equals("")) {
            condition.put("startDate", startDate);
        }
        if (endDate != null && !endDate.equals("")) {
            condition.put("endDate", endDate);
        }
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        JSONObject result = waitMedicalService.listYearWaitMedicals(condition);
        return ResultFormat.success(result);
    }
}
