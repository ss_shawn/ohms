package com.lanware.ehs.enterprise.check.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.check.service.MonitorWorkplaceHarmFactorService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.MonitorWorkplaceHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @description 监测点关联的作业场所和监测危害因素controller
 */
@Controller
@RequestMapping("/monitorPoint/workplaceHarmFactor")
public class WorkplaceHarmFactorController {
    /** 注入monitorWorkplaceHarmFactorService的bean */
    @Autowired
    private MonitorWorkplaceHarmFactorService monitorWorkplaceHarmFactorService;

    /**
     * @description 获取监测点关联的作业场所和监测危害因素result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param monitorPointId 监测点id
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/listWorkplaceHarmFactors")
    @ResponseBody
    public ResultFormat listWorkplaceHarmFactors(Integer pageNum, Integer pageSize, String keyword
            , Integer monitorPointId, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("monitorPointId", monitorPointId);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = monitorWorkplaceHarmFactorService.listWorkplaceHarmFactors(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /**
     * @description 获取该监测点未添加的作业场所(编辑的时候原作业场所也存在)
     * @param monitorPointId 监测点id
     * @param workplaceId 作业场所id
     * @param enterpriseUser 当前登录企业
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/getSelectWorkplaces")
    @ResponseBody
    public ResultFormat getSelectWorkplaces(Long monitorPointId, Long workplaceId
            , @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("monitorPointId", monitorPointId);
        condition.put("workplaceId", workplaceId);
        JSONObject result = monitorWorkplaceHarmFactorService.getSelectWorkplaces(condition);
        return ResultFormat.success(result);
    }

    /**
     * @description 获取该监测点中作业场所的危害因素
     * @param workplaceId 作业场所id
     * @param keyword 关键字
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/getSelectHarmFactors")
    @ResponseBody
    public ResultFormat getSelectHarmFactors(Long workplaceId, String keyword) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("workplaceId", workplaceId);
        condition.put("keyword", keyword);
        JSONObject result = monitorWorkplaceHarmFactorService.getSelectHarmFactors(condition);
        return ResultFormat.success(result);
    }

    /** @description 保存监测点关联的作业场所和监测危害因素
     * @param monitorWorkplaceHarmFactorCustom 监测点关联的作业场所和监测危害因素扩展pojo
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/edit/save")
    @ResponseBody
    public ResultFormat save(MonitorWorkplaceHarmFactorCustom monitorWorkplaceHarmFactorCustom) {
        if (monitorWorkplaceHarmFactorCustom.getId() == null) {
            if (monitorWorkplaceHarmFactorService.insert(monitorWorkplaceHarmFactorCustom) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            if (monitorWorkplaceHarmFactorService.update(monitorWorkplaceHarmFactorCustom) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    /**
     * @description 删除作业场所和监测危害因素
     * @param monitorPointId 监测点id
     * @param workplaceId 作业场所id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultFormat delete(Long monitorPointId, Long workplaceId) {
        if (monitorWorkplaceHarmFactorService.delete(monitorPointId, workplaceId) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    /**
     * @description 批量删除作业场所和监测危害因素
     * @param monitorPointId 监测点id
     * @param workplaceIds 作业场所id数组
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/deletes")
    @ResponseBody
    public ResultFormat deletes(Long monitorPointId, Long[] workplaceIds) {
        if (monitorWorkplaceHarmFactorService.deletes(monitorPointId, workplaceIds) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
