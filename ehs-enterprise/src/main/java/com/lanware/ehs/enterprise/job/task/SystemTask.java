package com.lanware.ehs.enterprise.job.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 系统定时任务
 */
@Component
@Slf4j
public class SystemTask {
    @Autowired
    private MedicalTask medicalTask;

    @Autowired
    private CheckPlanTask checkPlanTask;

    @Autowired
    private ProtectFacPlanTask protectFacPlanTask;

    @Autowired
    private RescueDrillPlanTask rescueDrillPlanTask;

    @Autowired
    private SyncGovTask syncGovTask;
    /**
     * 凌晨一点生成各类提醒
     * cron表达式：cron = "0 *\/1 * * * ?"
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void work(){
        //生成体检提醒
        medicalTask.executeTask();

        //生成危害因素检测提醒
        checkPlanTask.executeCheckPlanTask();

        //生成防护设施维护检修提醒
        protectFacPlanTask.executeProtectFacPlanTask();

        //生成应急救援演练计划提醒
        rescueDrillPlanTask.executeRescueDrillPlanTask();

        //同步局端数据
        syncGovTask.executeSyncGovTask();
    }
}
