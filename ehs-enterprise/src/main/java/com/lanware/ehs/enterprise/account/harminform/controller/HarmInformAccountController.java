package com.lanware.ehs.enterprise.account.harminform.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.employee.controller.EmployeeArchivesController;
import com.lanware.ehs.enterprise.notify.service.EmployeeWorkNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseDiseaseDiagnosiNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseMedicalResultNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseWorkplaceNotifyService;
import com.lanware.ehs.enterprise.workplace.service.WorkplaceService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 职业危害告知台帐controller
 */
@Controller
@RequestMapping("/harmInformAccount")
public class HarmInformAccountController {
    @Autowired
    /** 注入EmployeeWorkNotifyService的bean */
    private EmployeeWorkNotifyService employeeWorkNotifyService;
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EnterpriseWorkplaceNotifyService enterpriseWorkplaceNotifyService;
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EnterpriseMedicalResultNotifyService enterpriseMedicalResultNotifyService;
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EnterpriseDiseaseDiagnosiNotifyService enterpriseDiseaseDiagnosiNotifyService;
    @Autowired
    /** 注入workplaceService的bean */
    private WorkplaceService workplaceService;

    @Value(value="${upload.tempPath}")
    private String uploadTempPath;
    //年度告知报表
    @RequestMapping("/getYearReportNotifyList")
    @ResponseBody
    public EhsResult getYearReportNotifyList( String yearReportNotify_startDate,String yearReportNotify_endDate,@CurrentUser EnterpriseUser currentUser){
        Map<String, Object> condition = new JSONObject();
        condition.put("enterpriseId",currentUser.getEnterpriseId());

        condition.put("startDate",yearReportNotify_startDate);

        condition.put("endDate",yearReportNotify_endDate);

        List<YearNotifyReportCustom>  yearNotifyReportCustomList=enterpriseMedicalResultNotifyService.getYearReportNotifyList(condition);
        return EhsResult.ok(yearNotifyReportCustomList);
    }



    //下载岗前告知列表
    @RequestMapping("/workNotifyDownload")
    @ResponseBody
    public void workNotifyDownload(String work_notify_keyword,HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");


            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            request.setPageSize(0);//设置为0表示不分页
            Map<String, Object> dataMap =getWorkNotifyMap(currentUser.getEnterpriseId(),request,work_notify_keyword);
            String uploadPath = upload +  File.separator+"岗前告知列表.doc";
            exeWord(dataMap,uploadPath,"workNotify.ftl");

            File uploadFile=new File(uploadPath);

            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("岗前告知列表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    private Map getWorkNotifyMap(long enterpriseId, QueryRequest request,String work_notify_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        if(work_notify_keyword!=null){
            condition.put("keyword",work_notify_keyword);
        }
        //排序条件
        condition.put("orderByClause","gmt_create");
        //查询并返回
        Page<EmployeeWorkNotifyCustom> employeeWorkNotifyCustomPage  = employeeWorkNotifyService.findEmployeeWorkNotifyList(condition,request);
        List  employeeWorkNotifyList=new ArrayList();
        for(EmployeeWorkNotifyCustom employeeWorkNotifyCustom:employeeWorkNotifyCustomPage){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("employeeName",employeeWorkNotifyCustom.getEmployeeName());
            temp.put("postName",employeeWorkNotifyCustom.getPostName());
            temp.put("notifyDate",employeeWorkNotifyCustom.getNotifyDate()==null?"":sdf1.format(employeeWorkNotifyCustom.getNotifyDate()));
            temp.put("workDate",employeeWorkNotifyCustom.getWorkDate()==null?"":sdf1.format(employeeWorkNotifyCustom.getWorkDate()));
            temp.put("notifyFile",employeeWorkNotifyCustom.getNotifyFile()==null?"--":"已上传");
            employeeWorkNotifyList.add(temp);
        }
        dataMap.put("employeeWorkNotifyList",employeeWorkNotifyList);
        return dataMap;
    }
    //下载作业场所检测结果告知列表
    @RequestMapping("/workplaceNotifyDownload")
    @ResponseBody
    public void workplaceNotifyDownload(String workplaceNotify_keyword,HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            request.setPageSize(0);//设置不分页
            Map<String, Object> dataMap =getWorkplaceNotifyMap(currentUser.getEnterpriseId(),request,workplaceNotify_keyword);
            String uploadPath = upload +  File.separator+"作业场所检测结果告知列表.doc";
            exeWord(dataMap,uploadPath,"workplaceNotify.ftl");

            File uploadFile=new File(uploadPath);

            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("作业场所检测结果告知列表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    private Map getWorkplaceNotifyMap(long enterpriseId, QueryRequest request,String workplaceNotify_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);
        condition.put("keyword",workplaceNotify_keyword);
        //排序条件
        condition.put("orderByClause","gmt_create");
        //查询并返回
        Page<EnterpriseWorkplaceNotifyCustom> enterpriseWorkplaceNotifyCustoms  = this.enterpriseWorkplaceNotifyService.findEnterpriseWorkplaceNotifyList(condition,request);
        List  enterpriseWorkplaceNotifyList=new ArrayList();
        for(EnterpriseWorkplaceNotifyCustom enterpriseWorkplaceNotifyCustom:enterpriseWorkplaceNotifyCustoms){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("workplaceName",enterpriseWorkplaceNotifyCustom.getWorkplaceName());
            temp.put("checkDate",enterpriseWorkplaceNotifyCustom.getCheckDate()==null?"":sdf1.format(enterpriseWorkplaceNotifyCustom.getCheckDate()));
            temp.put("checkOrganization",enterpriseWorkplaceNotifyCustom.getCheckOrganization());
            temp.put("notifyDate",enterpriseWorkplaceNotifyCustom.getNotifyDate()==null?"":sdf1.format(enterpriseWorkplaceNotifyCustom.getNotifyDate()));
            temp.put("checkResultProve",enterpriseWorkplaceNotifyCustom.getCheckResultProve()==null?"--":"已上传");
            enterpriseWorkplaceNotifyList.add(temp);
        }
        dataMap.put("enterpriseWorkplaceNotifyList",enterpriseWorkplaceNotifyList);
        return dataMap;
    }
    //下载体检结果告知列表
    @RequestMapping("/medicalResultNotifyDownload")
    @ResponseBody
    public void medicalResultNotifyDownload(String medicalResultNotify_keyword,HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            request.setPageSize(0);//设置不分页
            Map<String, Object> dataMap =getMedicalResultNotifyMap(currentUser.getEnterpriseId(),request,medicalResultNotify_keyword);
            String uploadPath = upload +  File.separator+"体检结果告知列表.doc";
            exeWord(dataMap,uploadPath,"medicalResultNotify.ftl");

            File uploadFile=new File(uploadPath);

            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("体检结果告知列表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    private Map getMedicalResultNotifyMap(long enterpriseId, QueryRequest request,String medicalResultNotify_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);
        condition.put("keyword",medicalResultNotify_keyword);
        //排序条件
        condition.put("orderByClause","mrw.gmt_create");
        //查询并返回
        Page<EnterpriseMedicalResultNotifyCustom> enterpriseMedicalResultNotifyCustomPage  = this.enterpriseMedicalResultNotifyService.findEnterpriseMedicalResultNotifyList(condition,request);
        List  enterpriseMedicalResultNotifyList=new ArrayList();
        for(EnterpriseMedicalResultNotifyCustom enterpriseMedicalResultNotifyCustom:enterpriseMedicalResultNotifyCustomPage){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("employeeName",enterpriseMedicalResultNotifyCustom.getEmployeeName());
            temp.put("medicalDate",enterpriseMedicalResultNotifyCustom.getMedicalDate()==null?"":sdf1.format(enterpriseMedicalResultNotifyCustom.getMedicalDate()));
            temp.put("type",enterpriseMedicalResultNotifyCustom.getType());
            temp.put("notifyDate",enterpriseMedicalResultNotifyCustom.getNotifyDate()==null?"":sdf1.format(enterpriseMedicalResultNotifyCustom.getNotifyDate()));
            temp.put("notifyFile",enterpriseMedicalResultNotifyCustom.getNotifyFile()==null?"--":"已上传");

            enterpriseMedicalResultNotifyList.add(temp);
        }
        dataMap.put("enterpriseMedicalResultNotifyList",enterpriseMedicalResultNotifyList);
        return dataMap;
    }

    //下载职业病诊断结果告知列表
    @RequestMapping("/diseaseDiagnosiNotifyDownload")
    @ResponseBody
    public void diseaseDiagnosiNotifyDownload(String diseaseDiagnosiNotify_keyword,HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            request.setPageSize(0);//设置不分页
            //返回word的变量
            Map<String, Object> dataMap =getDiseaseDiagnosiNotifyMap(currentUser.getEnterpriseId(),request,diseaseDiagnosiNotify_keyword);
            String uploadPath = upload +  File.separator+"职业病诊断结果告知列表.doc";
            exeWord(dataMap,uploadPath,"diseaseDiagnosiNotify.ftl");

            File uploadFile=new File(uploadPath);

            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业病诊断结果告知列表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }



    private Map getDiseaseDiagnosiNotifyMap(long enterpriseId, QueryRequest request,String diseaseDiagnosiNotify_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);
        condition.put("keyword",diseaseDiagnosiNotify_keyword);
        //排序条件
        condition.put("orderByClause","mrw.gmt_create");
        //查询并返回
        Page<EnterpriseDiseaseDiagnosiNotifyCustom> enterpriseMedicalResultNotifyCustomPage = this.enterpriseDiseaseDiagnosiNotifyService.findEnterpriseDiseaseDiagnosiNotifyList(condition,request);
        List  enterpriseDiseaseDiagnosiNotifyList=new ArrayList();
        for(EnterpriseDiseaseDiagnosiNotifyCustom enterpriseDiseaseDiagnosiNotifyCustom:enterpriseMedicalResultNotifyCustomPage){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("employeeName",enterpriseDiseaseDiagnosiNotifyCustom.getEmployeeName());
            temp.put("name",enterpriseDiseaseDiagnosiNotifyCustom.getName());
            temp.put("diagnosiDate",enterpriseDiseaseDiagnosiNotifyCustom.getDiagnosiDate()==null?"":sdf1.format(enterpriseDiseaseDiagnosiNotifyCustom.getDiagnosiDate()));
            temp.put("notifyDate",enterpriseDiseaseDiagnosiNotifyCustom.getNotifyDate()==null?"":sdf1.format(enterpriseDiseaseDiagnosiNotifyCustom.getNotifyDate()));
            temp.put("notifyFile",enterpriseDiseaseDiagnosiNotifyCustom.getNotifyFile()==null?"--":"已上传");
            enterpriseDiseaseDiagnosiNotifyList.add(temp);
        }
        dataMap.put("enterpriseDiseaseDiagnosiNotifyList",enterpriseDiseaseDiagnosiNotifyList);
        return dataMap;
    }

    //下载作业场所检测结果告知列表
    @RequestMapping("/workplaceInfoNotifyDownload")
    @ResponseBody
    public void workplaceInfoNotifyDownload(HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            //设置不分页
            request.setPageSize(0);
            //返回word的变量
            Map<String, Object> dataMap =getWorkplaceInfoNotifyMap(currentUser.getEnterpriseId(),request);
            String uploadPath = upload +  File.separator+"作业场所危害告知列表.doc";
            exeWord(dataMap,uploadPath,"workplaceInfoNotify.ftl");

            File uploadFile=new File(uploadPath);

            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("作业场所危害告知列表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    private Map getWorkplaceInfoNotifyMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);
        //排序条件
        condition.put("orderByClause","gmt_create");
        //查询并返回
        JSONObject jsonObject=this.workplaceService.listWorkplacesByEnterpriseId(null,null,condition);
        List<EnterpriseWorkplaceCustom> enterpriseWorkplaceNotifys  = (List<EnterpriseWorkplaceCustom>)jsonObject.get("enterpriseWorkplaces");
        List  enterpriseWorkplaceNotifyList=new ArrayList();
        for(EnterpriseWorkplaceCustom enterpriseWorkplaceCustom:enterpriseWorkplaceNotifys){
            if(enterpriseWorkplaceCustom.getHarmfactorNum()==null||enterpriseWorkplaceCustom.getHarmfactorNum()==0){
                continue;
            }
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("workplaceName",enterpriseWorkplaceCustom.getName());
            temp.put("dentifyFileNum",enterpriseWorkplaceCustom.getIdentifyFileNum()==null||enterpriseWorkplaceCustom.getIdentifyFileNum()==0?"--":"已告知");
            temp.put("informFileNum",enterpriseWorkplaceCustom.getInformFileNum()==null||enterpriseWorkplaceCustom.getInformFileNum()==0?"--":"已告知");
            temp.put("informationFileNum",enterpriseWorkplaceCustom.getInformationFileNum()==null||enterpriseWorkplaceCustom.getInformationFileNum()==0?"--":"已告知");
            enterpriseWorkplaceNotifyList.add(temp);
        }
        dataMap.put("enterpriseWorkplaceNotifyList",enterpriseWorkplaceNotifyList);
        return dataMap;
    }

    //下载告知年度统计
    @RequestMapping("/yearReportNotifyDownload")
    @ResponseBody
    public void yearReportNotifyDownload(String yearReportNotify_startDate,String yearReportNotify_endDate,HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();


            //设置不分页
            request.setPageSize(0);
            //返回word的变量
            Map<String, Object> dataMap =getYearReportNotifyDownloadNotifyMap(currentUser.getEnterpriseId(),yearReportNotify_startDate,yearReportNotify_endDate,request);
            String uploadPath = upload +  File.separator+"年度告知统计表.doc";
            exeWord(dataMap,uploadPath,"yearReportNotify.ftl");

            File uploadFile=new File(uploadPath);

            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("年度告知统计表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    private Map getYearReportNotifyDownloadNotifyMap(long enterpriseId,String yearReportNotify_startDate,String yearReportNotify_endDate, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        condition.put("enterpriseId",enterpriseId);
        condition.put("startDate",yearReportNotify_startDate);
        condition.put("endDate",yearReportNotify_endDate);

        List<YearNotifyReportCustom>  yearNotifyReportCustomList=enterpriseMedicalResultNotifyService.getYearReportNotifyList(condition);
          List   yearNotifyList=new ArrayList();
        for(YearNotifyReportCustom yearNotifyReportCustom:yearNotifyReportCustomList){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("type",yearNotifyReportCustom.getType());
            temp.put("allNotifyNum",yearNotifyReportCustom.getAllNotifyNum()+"");
            temp.put("notifyNum",yearNotifyReportCustom.getNotifyNum()+"");
            if(yearNotifyReportCustom.getAllNotifyNum()==0){
                temp.put("notifyStatistics","100%");
            }else{
                temp.put("notifyStatistics",yearNotifyReportCustom.getNotifyStatistics()*100+"%");
            }

            yearNotifyList.add(temp);
        }
        dataMap.put("year",yearReportNotify_startDate+"");
        dataMap.put("yearNotifyList",yearNotifyList);
        return dataMap;
    }

    //下载职业危害告知台帐
    @RequestMapping("/harmInformAccountDownload")
    @ResponseBody
    public void harmInformAccountDownload( String yearReportNotify_startDate,String yearReportNotify_endDate,String work_notify_keyword,String workplaceNotify_keyword,String medicalResultNotify_keyword,String diseaseDiagnosiNotify_keyword, HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();//待压缩的文件
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

//岗前告知列表
            //返回word的变量
            Map<String, Object> dataMap =getWorkNotifyMap(currentUser.getEnterpriseId(),request,work_notify_keyword);
            String uploadPath = upload +  File.separator+"岗前告知列表.doc";
            exeWord(dataMap,uploadPath,"workNotify.ftl");
            files.add(uploadPath);
            //作业场所检测结果告知列表
            //返回word的变量

            dataMap =getWorkplaceNotifyMap(currentUser.getEnterpriseId(),request, workplaceNotify_keyword);
            uploadPath = upload +  File.separator+"作业场所检测结果告知列表.doc";
            exeWord(dataMap,uploadPath,"workplaceNotify.ftl");

            files.add(uploadPath);
//体检结果告知列表
            dataMap =getMedicalResultNotifyMap(currentUser.getEnterpriseId(),request,medicalResultNotify_keyword);
            uploadPath = upload +  File.separator+"体检结果告知列表.doc";
            exeWord(dataMap,uploadPath,"medicalResultNotify.ftl");
            files.add(uploadPath);
//职业病诊断结果告知列表
            dataMap =getDiseaseDiagnosiNotifyMap(currentUser.getEnterpriseId(),request,diseaseDiagnosiNotify_keyword);
            uploadPath = upload +  File.separator+"职业病诊断结果告知列表.doc";
            exeWord(dataMap,uploadPath,"diseaseDiagnosiNotify.ftl");
            files.add(uploadPath);
//作业场所危害告知列表
            dataMap =getWorkplaceInfoNotifyMap(currentUser.getEnterpriseId(),request);
            uploadPath = upload +  File.separator+"作业场所危害告知列表.doc";
            exeWord(dataMap,uploadPath,"workplaceInfoNotify.ftl");
            files.add(uploadPath);
            //职业病诊断结果告知列表
            dataMap =getYearReportNotifyDownloadNotifyMap(currentUser.getEnterpriseId(),yearReportNotify_startDate,yearReportNotify_endDate,request);
            uploadPath = upload +  File.separator+"年度告知统计表.doc";
            exeWord(dataMap,uploadPath,"yearReportNotify.ftl");
            files.add(uploadPath);


            //放入压缩列表



            String uploadZipPath = upload +  File.separator+"职业危害告知台帐.zip";
            FileUtil.toZip(files, uploadZipPath, false);


            //下载文件
            File uploadFile=new File(uploadZipPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业危害告知台帐.zip", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    //下载职业危害告知台帐
    @RequestMapping("/downloadHarmInformAccount")
    @ResponseBody
    public void downloadHarmInformAccount( String yearReportNotify_startDate,String yearReportNotify_endDate,String work_notify_keyword,String workplaceNotify_keyword,String medicalResultNotify_keyword,String diseaseDiagnosiNotify_keyword, HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){



        File upload=null;
        List<String> files=new ArrayList<String>();//待压缩的文件
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            Map<String, Object> dataMapAll=new HashMap<String, Object>();
//岗前告知列表
            Map<String, Object> dataMap =getWorkNotifyMap(currentUser.getEnterpriseId(),request,work_notify_keyword);
            dataMapAll.putAll(dataMap);
            //作业场所检测结果告知列表
            dataMap =getWorkplaceNotifyMap(currentUser.getEnterpriseId(),request,workplaceNotify_keyword);
            dataMapAll.put("enterpriseWorkplaceCheckNotifyList",dataMap.get("enterpriseWorkplaceNotifyList"));

//体检结果告知列表
            dataMap =getMedicalResultNotifyMap(currentUser.getEnterpriseId(),request,medicalResultNotify_keyword);
            dataMapAll.putAll(dataMap);
//职业病诊断结果告知列表
            dataMap =getDiseaseDiagnosiNotifyMap(currentUser.getEnterpriseId(),request,diseaseDiagnosiNotify_keyword);
            dataMapAll.putAll(dataMap);
//作业场所危害告知列表
            dataMap =getWorkplaceInfoNotifyMap(currentUser.getEnterpriseId(),request);
            dataMapAll.putAll(dataMap);
            //职业病诊断结果告知列表
            dataMap =getYearReportNotifyDownloadNotifyMap(currentUser.getEnterpriseId(),yearReportNotify_startDate,yearReportNotify_endDate,request);
            dataMapAll.putAll(dataMap);
            //放入压缩列表

            String uploadPath = upload +  File.separator+"职业危害告知台帐.doc";
            exeWord(dataMapAll,uploadPath,"harmInformAccount.ftl");

            //下载文件
            File uploadFile=new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业危害告知台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(EmployeeArchivesController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
