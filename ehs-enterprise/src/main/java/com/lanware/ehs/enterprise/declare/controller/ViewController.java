package com.lanware.ehs.enterprise.declare.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.declare.service.DeclareService;
import com.lanware.ehs.pojo.EnterpriseDeclare;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 工作场所危害申报视图
 */
@Controller("declareView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @Autowired
    /** 注入declareService的bean */
    private DeclareService declareService;

    @RequestMapping("declare/editDeclare")
    /**
     * @description 新增作业场所危害申报
     * @param enterpriseUser 企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editDeclare(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        EnterpriseDeclare enterpriseDeclare = new EnterpriseDeclare();
        enterpriseDeclare.setEnterpriseId(enterpriseUser.getEnterpriseId());
        data.put("enterpriseDeclare", enterpriseDeclare);
        return new ModelAndView("declare/edit", data);
    }

    /**
     * @description 编辑作业场所危害申报
     * @param id 作业场所危害申报id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("declare/editDeclare/{id}")
    public ModelAndView editWorkplace(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseDeclare enterpriseDeclare = declareService.getDeclareById(id);
        data.put("enterpriseDeclare", enterpriseDeclare);
        return new ModelAndView("declare/edit", data);
    }

    @RequestMapping("declare/declareWorkplace/{declareId}")
    /**
     * 涉及的场所和危害因素页面
     * @param declareId 申报id
     * @return 涉及的场所和危害因素model
     */
    public ModelAndView list(@PathVariable Long declareId){
        JSONObject data = new JSONObject();
        data.put("declareId", declareId);
        return new ModelAndView("declare/declareworkplace/list", data);
    }

    @RequestMapping("declare/declareWorkplace/select/{declareId}")
    /**
     * @description 选择涉及危害场所和危害因素
     * @param declareId 申报id
     * @return 涉及的场所和危害因素model
     */
    public ModelAndView select(@PathVariable Long declareId){
        JSONObject data = new JSONObject();
        data.put("declareId", declareId);
        return new ModelAndView("declare/declareworkplace/select", data);
    }

    @RequestMapping("declare/receipt/{id}")
    /**
     * @description 申报回执
     * @param id 申报信息id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView receipt(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseDeclare enterpriseDeclare = declareService.getDeclareById(id);
        data.put("enterpriseDeclare", enterpriseDeclare);
        return new ModelAndView("declare/receipt", data);
    }
}
