package com.lanware.ehs.enterprise.equipmentuse.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseEquipmentUse;
import com.lanware.ehs.pojo.EquipmentServiceUse;
import com.lanware.ehs.pojo.custom.EnterpriseEquipmentUseCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 运行使用interface
 */
public interface EquipmentUseService {
    /**
     * @description 查询运行使用list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    JSONObject listEquipmentUses(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询运行使用
     * @param id 运行使用id
     * @return com.lanware.management.pojo.EnterpriseEquipmentUse 返回EnterpriseEquipmentUse对象
     */
    EnterpriseEquipmentUse getEquipmentUseById(Long id);

    /**
     * @description 新增运行使用
     * @param enterpriseEquipmentUse 运行使用pojo
     * @return int 插入成功的数目
     */
    int insertEquipmentUse(EnterpriseEquipmentUse enterpriseEquipmentUse);

    /**
     * @description 更新运行使用
     * @param enterpriseEquipmentUse 运行使用pojo
     * @return int 更新成功的数目
     */
    int updateEquipmentUse(EnterpriseEquipmentUse enterpriseEquipmentUse);

    /**
     * @description 删除运行使用
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteEquipmentUse(Long id);

    /**
     * @description 批量删除运行使用
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteEquipmentUses(Long[] ids);

    /**
     * @description 根据记录id获取运行设备
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listRunEquipmentsByRecordId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据记录id过滤尚未运行的设备
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listSelectRunEquipmentsByRecordId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 批量插入运行的设备集合
     * @param equipmentServiceUseList 入运行的设备集合
     * @return int 插入成功的数目
     */
    int insertBatch(List<EquipmentServiceUse> equipmentServiceUseList);

    /**
     * @description 删除运行的设备
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteRunEquipment(Long id);

    /**
     * @description 批量删除运行的设备
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteRunEquipments(Long[] ids);

    List<EnterpriseEquipmentUseCustom> listEquipmentUseAccounts(Map<String, Object> condition);
}
