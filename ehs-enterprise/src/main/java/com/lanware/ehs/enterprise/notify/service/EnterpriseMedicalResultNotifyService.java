package com.lanware.ehs.enterprise.notify.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotify;
import com.lanware.ehs.pojo.custom.EnterpriseMedicalResultNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseMedicalResultWaitNotifyCustom;
import com.lanware.ehs.pojo.custom.YearNotifyReportCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface EnterpriseMedicalResultNotifyService {
    /**
     * @description 根据id查询待体检结果告知
     * @param id 待体检结果告知id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseMedicalResultWaitNotify对象
     */
     EnterpriseMedicalResultWaitNotify getEnterpriseMedicalResultWaitNotifyById(Long id);
    /**
     * @description 根据id查询待体检结果告知
     * @param condition 待体检结果告知条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseMedicalResultWaitNotify对象
     */
     Page<EnterpriseMedicalResultWaitNotifyCustom> findEnterpriseMedicalResultWaitNotifyList(Map<String, Object> condition, QueryRequest request);
    /**
     * 保存体检结果告知,
     * @param enterpriseMedicalResultNotifyCustom
     * @return成功与否 1成功 0不成功
     */
    int saveEnterpriseMedicalResultNotify(EnterpriseMedicalResultNotifyCustom enterpriseMedicalResultNotifyCustom, MultipartFile notifyFile);
    /**
     * 通过id获取体检结果告知
     * @param id 体检结果告知id
     * @return体检结果告知
     */
     EnterpriseMedicalResultNotifyCustom getEnterpriseMedicalResultNotifyCustomByID(long id);
    /**
     * 删除待体检结果告知,
     * @param id
     * @return成功与否 1成功 0不成功
     */
     int deleteEnterpriseMedicalResultNotify(long id);
    /**
     * 通过待体检结果告知ID获取待体检结果告知的人员和岗位信息,放入体检结果告知返回
     * @param id 待体检结果告知ID
     * @return 体检结果告知
     */
     EnterpriseMedicalResultNotifyCustom getEnterpriseMedicalResultNotifyCustomByWaitID(long id);
    /**
     * @description 根据条件查询体检结果告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    Page<EnterpriseMedicalResultNotifyCustom> findEnterpriseMedicalResultNotifyList(Map<String, Object> condition, QueryRequest request);
    /**
     * 获取告知年度统计报表
     * @param condition
     * @return告知年度统计报表
     */
     List<YearNotifyReportCustom> getYearReportNotifyList(Map<String, Object> condition);

}
