package com.lanware.ehs.enterprise.hygieneorg.service.impl;

import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.FileDeleteUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.hygieneorg.service.CertificateService;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CertificateServiceImpl implements CertificateService {

    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;

    private OSSTools ossTools;

    private FileDeleteUtil fileDeleteUtil;

    @Autowired
    public CertificateServiceImpl(EnterpriseRelationFileMapper enterpriseRelationFileMapper, OSSTools ossTools, FileDeleteUtil fileDeleteUtil){
        this.enterpriseRelationFileMapper = enterpriseRelationFileMapper;
        this.ossTools = ossTools;
        this.fileDeleteUtil = fileDeleteUtil;
    }

    @Override
    @Transactional
    public int saveCertificate(Long enterpriseId, EnterpriseRelationFile enterpriseRelationFile, MultipartFile file) {
        //新增数据先保存，以便获取id
        if (enterpriseRelationFile.getId() == null){
            enterpriseRelationFile.setRelationModule(ModuleName.HYGIENEORG.getValue());
            enterpriseRelationFile.setGmtCreate(new Date());
            enterpriseRelationFileMapper.insert(enterpriseRelationFile);
        }
        if (file != null){
            //上传了新的附件
            if (!StringUtils.isEmpty(enterpriseRelationFile.getFilePath())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
            }
            //上传并保存新文件
            String filePath = "/" + enterpriseId + "/" + ModuleName.HYGIENEORG.getValue() + "/" + enterpriseRelationFile.getId() + "/" + file.getOriginalFilename();
            try {
                ossTools.uploadStream(filePath, file.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传聘用书失败", e);
            }
            enterpriseRelationFile.setFileName(file.getOriginalFilename().lastIndexOf(".") == -1 ? file.getOriginalFilename() : file.getOriginalFilename().substring(0, file.getOriginalFilename().lastIndexOf(".")));
            enterpriseRelationFile.setFilePath(filePath);
        }
        //职业卫生组织信息
        enterpriseRelationFile.setGmtModified(new Date());
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
    }

    @Override
    public int deleteCertificateByIds(List<Long> ids) {
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria = enterpriseRelationFileExample.createCriteria();
        criteria.andIdIn(ids);
        List<EnterpriseRelationFile> EnterpriseRelationFiles = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        Set<String> filePathList = new HashSet<>();
        for (EnterpriseRelationFile EnterpriseRelationFile: EnterpriseRelationFiles) {
            filePathList.add(EnterpriseRelationFile.getFilePath());
        }
        int num = enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        if (num != 0){
            fileDeleteUtil.deleteFilesAsync(filePathList);
        }
        return num;
    }
}
