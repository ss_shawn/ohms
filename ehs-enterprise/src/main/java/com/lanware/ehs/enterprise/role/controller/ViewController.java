package com.lanware.ehs.enterprise.role.controller;

import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.role.service.RoleService;
import com.lanware.ehs.pojo.EnterpriseUserRole;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserRoleCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 角色视图控制器
 */
@Controller("roleView")
@RequestMapping(Constants.VIEW_PREFIX)
public class ViewController {
    @Autowired
    private RoleService roleService;
    /**
     * 添加角色页面
     * @return
     */
    @GetMapping("role/add")
    public String roleAddView() {
        return "role/roleAdd";
    }

    /**
     * 更新角色页面
     * @return
     */
    @GetMapping("role/update/{id}")
    public String roleUpdateView(@PathVariable Long id, Model model, @CurrentUser EnterpriseUserCustom userCustom) {
        EnterpriseUserRoleCustom role = new EnterpriseUserRoleCustom();
        role.setId(id);
        role.setEnterpriseId(userCustom.getEnterpriseId());
        role = roleService.getRoleCustomById(id);
        model.addAttribute("role",role);
        return "role/roleUpdate";
    }

}
