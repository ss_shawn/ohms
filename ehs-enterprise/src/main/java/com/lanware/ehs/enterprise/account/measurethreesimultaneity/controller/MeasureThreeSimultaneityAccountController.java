package com.lanware.ehs.enterprise.account.measurethreesimultaneity.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseThreeSimultaneityCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 三同时台帐controller
 */
@Controller
@RequestMapping("/measureThreeSimultaneityAccount")
public class MeasureThreeSimultaneityAccountController {
    /** 注入threeSimultaneityService的bean */
    @Autowired
    private ThreeSimultaneityService threeSimultaneityService;
    /** 下载word的临时路径 */
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;

    @RequestMapping("")
    /**
     * @description 返回三同时台帐页面
     * @return java.lang.String 返回页面路径
     */
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/measureThreeSimultaneity/list", data);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(MeasureThreeSimultaneityAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 三同时台账zip下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadThreeSimultaneity")
    @ResponseBody
    public ResultFormat downloadThreeSimultaneity(Long enterpriseId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> dataMap1 = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 三同时记录
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        JSONObject result = threeSimultaneityService.listThreeSimultaneitys(0, 0, condition);
        List<EnterpriseThreeSimultaneityCustom> enterpriseThreeSimultaneitys
                = (List<EnterpriseThreeSimultaneityCustom>)result.get("enterpriseThreeSimultaneitys");
        List threeSimultaneityList = new ArrayList();
        for (EnterpriseThreeSimultaneityCustom enterpriseThreeSimultaneityCustom : enterpriseThreeSimultaneitys) {
            Map<String, Object> temp = new HashMap<String, Object>();
            Integer harmDefree = enterpriseThreeSimultaneityCustom.getHarmDegree();
            Integer riskClass = enterpriseThreeSimultaneityCustom.getRiskClass();
            temp.put("projectName", enterpriseThreeSimultaneityCustom.getProjectName());
            temp.put("projectSource", enterpriseThreeSimultaneityCustom.getProjectSource());
            temp.put("projectTime", sdf.format(enterpriseThreeSimultaneityCustom.getProjectTime()));
            temp.put("harmDegree", harmDefree == null ? "" : (harmDefree == 0 ? "一般" : (harmDefree == 1 ? "较重" : "严重")));
            temp.put("riskClass", riskClass == null ? "" : (riskClass == 0 ? "一般" : (riskClass == 1 ? "较重" : "严重")));
            if (enterpriseThreeSimultaneityCustom.getStatus() == 1) {
                temp.put("projectState", "已完成");
            } else {
                Integer projectState = enterpriseThreeSimultaneityCustom.getProjectState();
                temp.put("projectState", projectState == 0 ? "预评价阶段" : (projectState == 1 ? "防护设施设计阶段" : "竣工验收阶段"));
            }
            threeSimultaneityList.add(temp);
        }
        dataMap.put("threeSimultaneityList", threeSimultaneityList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(MeasureThreeSimultaneityAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("threeSimultaneity.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "三同时记录.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            //放入压缩列表
            files.add(uploadPath);
            String uploadZipPath = upload + File.separator + "三同时台账.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            File uploadZipFile = new File(uploadZipPath);
            FileInputStream fis = new FileInputStream(uploadZipFile);
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("三同时台账.zip", "utf-8"));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = fis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            fis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 三同时台帐文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadThreeSimultaneityAccount")
    @ResponseBody
    public void downloadThreeSimultaneityAccount(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            Map<String, Object> dataMap = getThreeSimultaneityAccountMap(enterpriseId,request);
            String uploadPath = upload + File.separator + "建设项目职业危害防护设施“三同时”台帐.doc";
            exeWord(dataMap,uploadPath,"threeSimultaneityAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("建设项目职业危害防护设施“三同时”台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到三同时台帐文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getThreeSimultaneityAccountMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 三同时记录
        JSONObject result = threeSimultaneityService.listThreeSimultaneitys(0, 0, condition);
        List<EnterpriseThreeSimultaneityCustom> enterpriseThreeSimultaneitys
                = (List<EnterpriseThreeSimultaneityCustom>)result.get("enterpriseThreeSimultaneitys");
        List threeSimultaneityList = new ArrayList();
        for (EnterpriseThreeSimultaneityCustom enterpriseThreeSimultaneityCustom : enterpriseThreeSimultaneitys) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("projectName", enterpriseThreeSimultaneityCustom.getProjectName());
            temp.put("projectSource", enterpriseThreeSimultaneityCustom.getProjectSource());
            temp.put("projectTime", sdf.format(enterpriseThreeSimultaneityCustom.getProjectTime()));
            Integer harmDefree = enterpriseThreeSimultaneityCustom.getHarmDegree();
            temp.put("harmDegree", (harmDefree == null || harmDefree.equals("")) ? "-"
                    : (harmDefree == 0 ? "一般" : (harmDefree == 1 ? "较重" : "严重")));
            Integer riskClass = enterpriseThreeSimultaneityCustom.getRiskClass();
            temp.put("riskClass", (riskClass == null || riskClass.equals("")) ? "-"
                    : (riskClass == 0 ? "一般" : (riskClass == 1 ? "较重" : "严重")));
            if (enterpriseThreeSimultaneityCustom.getStatus() == 1) {
                temp.put("projectState", "已完成");
            } else {
                Integer projectState = enterpriseThreeSimultaneityCustom.getProjectState();
                temp.put("projectState", projectState == 0 ? "预评价阶段" : (projectState == 1 ? "防护设施设计阶段" : "竣工验收阶段"));
            }
            threeSimultaneityList.add(temp);
        }
        dataMap.put("threeSimultaneityList", threeSimultaneityList);
        return dataMap;
    }
}
