package com.lanware.ehs.enterprise.account.harmdeclare.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.declare.service.DeclareService;
import com.lanware.ehs.pojo.EnterpriseDeclare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 职业危害告知台账视图
 */
@Controller("harmDeclareAccountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @Autowired
    /** 注入declareService的bean */
    private DeclareService declareService;

    /**
     * @description 返回机构相关证书详细页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("harmDeclare/institutionCertificateDetail/{relationId}/{relationModule}")
    public ModelAndView institutionCertificateDetail(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/harmDeclare/inspectionCertificateDetail", data);
    }

    /**
     * @description 返回职业病危害警示详细页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("harmDeclare/identifyFileDetail/{relationId}/{relationModule}")
    public ModelAndView identifyFileDetail(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/harmDeclare/identifyFileDetail", data);
    }

    /**
     * @description 返回高毒物品告知卡详细页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("harmDeclare/informFileDetail/{relationId}/{relationModule}")
    public ModelAndView informFileDetail(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/harmDeclare/informFileDetail", data);
    }

    /**
     * @description 返回职业危害因素信息卡详细页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("harmDeclare/informationFileDetail/{relationId}/{relationModule}")
    public ModelAndView informationFileDetail(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/harmDeclare/informationFileDetail", data);
    }

    @RequestMapping("harmDeclare/harmfactorDetail/{workplaceId}")
    /**
     * @description 返回职业病危害因素页面
     * @param workplaceId 作业场所id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView harmfactorDetail(@PathVariable Long workplaceId){
        JSONObject data = new JSONObject();
        data.put("workplaceId", workplaceId);
        return new ModelAndView("account/harmDeclare/harmfactorDetail", data);
    }

    @RequestMapping("harmDeclare/declareWorkplaceDetail/{declareId}")
    /**
     * 申报涉及的场所和危害因素详细页面
     * @param declareId 申报id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView declareWorkplaceDetail(@PathVariable Long declareId){
        JSONObject data = new JSONObject();
        data.put("declareId", declareId);
        return new ModelAndView("account/harmDeclare/declareWorkplaceDetail", data);
    }

    @RequestMapping("harmDeclare/viewDeclareReceipt/{id}")
    /**
     * @description 申报回执页面
     * @param id 申报信息id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewDeclareReceipt(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseDeclare enterpriseDeclare = declareService.getDeclareById(id);
        data.put("enterpriseDeclare", enterpriseDeclare);
        return new ModelAndView("account/harmDeclare/declareReceipt", data);
    }

    /**
     * @description 返回检测机构相关证书详细页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorCheck/checkInstitutionCertificate/{relationId}/{relationModule}")
    public ModelAndView checkInstitutionCertificate(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/monitorCheck/checkInstitutionCertificate", data);
    }
}
