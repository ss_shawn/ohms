package com.lanware.ehs.enterprise.accidenthandle.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.accidenthandle.service.AccidentHandleService;
import com.lanware.ehs.pojo.EnterpriseAccidentHandle;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 事故调查处理controller
 */
@Controller
@RequestMapping("/accidentHandle")
public class AccidentHandleController {
    /** 注入accidentHandleService的bean */
    @Autowired
    private AccidentHandleService accidentHandleService;

    @RequestMapping("")
    /**
     * @description 返回事故调查处理页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "accidentHandle/list";
    }

    @RequestMapping("/listAccidentHandles")
    @ResponseBody
    /**
     * @description 获取事故调查处理result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listAccidentHandles(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = accidentHandleService.listAccidentHandles(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editAccidentHandle/save")
    @ResponseBody
    /** @description 保存事故调查处理信息，并上传机构合同到oss服务器
     * @param enterpriseAccidentHandleJSON 事故调查处理json
     * @param enterpriseUser 当前企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseAccidentHandleJSON, @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseAccidentHandle enterpriseAccidentHandle
                = JSONObject.parseObject(enterpriseAccidentHandleJSON, EnterpriseAccidentHandle.class);
        if (enterpriseAccidentHandle.getId() == null) {
            enterpriseAccidentHandle.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseAccidentHandle.setGmtCreate(new Date());
            if (accidentHandleService.insertAccidentHandle(enterpriseAccidentHandle) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseAccidentHandle.setGmtModified(new Date());
            if (accidentHandleService.updateAccidentHandle(enterpriseAccidentHandle) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteAccidentHandle")
    @ResponseBody
    /**
     * @description 删除事故调查处理信息
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteAccidentHandle(Long id) {
        if (accidentHandleService.deleteAccidentHandle(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteAccidentHandles")
    @ResponseBody
    /**
     * @description 批量删除事故调查处理信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteAccidentHandles(Long[] ids) {
        if (accidentHandleService.deleteAccidentHandles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
