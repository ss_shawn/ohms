package com.lanware.ehs.enterprise.hygieneorg.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.hygieneorg.service.EnterpriseHygieneOrgService;
import com.lanware.ehs.enterprise.hygieneorg.service.HygieneOrgService;
import com.lanware.ehs.pojo.EnterpriseHygieneOrg;
import com.lanware.ehs.pojo.EnterpriseHygieneOrgExample;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseHygieneOrgCustom;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/hygieneOrg")
public class HygieneOrgController {

    private EnterpriseHygieneOrgService enterpriseHygieneOrgService;

    private HygieneOrgService hygieneOrgService;

    public HygieneOrgController(EnterpriseHygieneOrgService enterpriseHygieneOrgService, HygieneOrgService hygieneOrgService){
        this.enterpriseHygieneOrgService = enterpriseHygieneOrgService;
        this.hygieneOrgService = hygieneOrgService;
    }

    /**
     * 职业卫生组织管理列表页面
     * @return 职业卫生组织管理列表model
     */
    @RequestMapping("")
    public ModelAndView hygienerOrg(){
        return new ModelAndView("hygieneOrg/list");
    }

    /**
     * 分页查询职业卫生组织
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public ResultFormat getList(@CurrentUser EnterpriseUser enterpriseUser, Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<EnterpriseHygieneOrgCustom> page = hygieneOrgService.queryEnterpriseHygieneOrgByCondition(pageNum, pageSize, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }

    /**
     * 编辑职业卫生组织信息页面
     * @param id 职业卫生组织id
     * @return 编辑职业卫生组织信息model
     */
    @RequestMapping("/edit")
    public ModelAndView edit(Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            try {
                EnterpriseHygieneOrg enterpriseHygieneOrg = enterpriseHygieneOrgService.selectByPrimaryKey(id);
                data.put("enterpriseHygieneOrg", enterpriseHygieneOrg);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return new ModelAndView("hygieneOrg/edit", data);
    }

    /**
     * 查看职业卫生组织信息页面
     * @param id 职业卫生组织id
     * @return 查看职业卫生组织信息model
     */
    @RequestMapping("/view")
    public ModelAndView view(Long id) {
        JSONObject data = new JSONObject();
        if (id != null) {
            try {
                EnterpriseHygieneOrg enterpriseHygieneOrg = enterpriseHygieneOrgService.selectByPrimaryKey(id);
                data.put("enterpriseHygieneOrg", enterpriseHygieneOrg);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return new ModelAndView("hygieneOrg/view", data);
    }

    /**
     * 保存职业卫生组织
     * @param enterpriseHygieneOrgJSON 职业卫生组织JSON
     * @param employFile 聘用书文件
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(@CurrentUser EnterpriseUser enterpriseUser, String enterpriseHygieneOrgJSON, MultipartFile employFile){
        EnterpriseHygieneOrg enterpriseHygieneOrg = JSONObject.parseObject(enterpriseHygieneOrgJSON, EnterpriseHygieneOrg.class);
        enterpriseHygieneOrg.setEnterpriseId(enterpriseUser.getEnterpriseId());
        try {
            int num = hygieneOrgService.saveEnterpriseHygieneOrg(enterpriseHygieneOrg, employFile);
            if (num == 0){
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存职业卫生组织失败");
            }
        } catch (EhsException e){
            e.printStackTrace();
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
        return EhsResult.ok();
    }

    /**
     * 删除职业卫生组织
     * @param idsJSON 职业卫生组织id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultFormat delete(@RequestParam String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        EnterpriseHygieneOrgExample enterpriseHygieneOrgExample = new EnterpriseHygieneOrgExample();
        EnterpriseHygieneOrgExample.Criteria criteria = enterpriseHygieneOrgExample.createCriteria();
        criteria.andIdIn(ids);
        int num = hygieneOrgService.deleteEnterpriseHygieneOrgByIds(ids);
        if (num == 0){
            return ResultFormat.error("删除职业卫生组织失败");
        }
        return ResultFormat.success();
    }

    /**
     * 上传聘用书
     * @param id 职业卫生组织id
     * @return 编辑职业卫生组织信息model
     */
    @RequestMapping("/addEmployFile")
    public ModelAndView addEmployFile(Long id){
        JSONObject data = new JSONObject();
        try {
            EnterpriseHygieneOrg enterpriseHygieneOrg = enterpriseHygieneOrgService.selectByPrimaryKey(id);
            data.put("enterpriseHygieneOrg", enterpriseHygieneOrg);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return new ModelAndView("hygieneOrg/addEmployFile", data);
    }
}
