package com.lanware.ehs.enterprise.yearplan.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.DateUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.check.service.WaitCheckService;
import com.lanware.ehs.enterprise.employee.service.EmployeeService;
import com.lanware.ehs.enterprise.medical.service.WaitMedicalService;
import com.lanware.ehs.enterprise.remind.service.RemindService;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.mapper.CheckPlanHarmFactorMapper;
import com.lanware.ehs.mapper.EnterpriseYearPlanMapper;
import com.lanware.ehs.mapper.custom.EnterpriseYearPlanCustomMapper;
import com.lanware.ehs.mapper.custom.MonitorWorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 年度计划interface实现类
 */
public class YearplanServiceImpl implements YearplanService {
    /** 注入enterpriseYearPlanMapper的bean */
    @Autowired
    private EnterpriseYearPlanMapper enterpriseYearPlanMapper;
    /** 注入enterpriseYearPlanCustomMapper的bean */
    @Autowired
    private EnterpriseYearPlanCustomMapper enterpriseYearPlanCustomMapper;
    /** 注入monitorWorkplaceHarmFactorCustomMapper的bean */
    @Autowired
    private MonitorWorkplaceHarmFactorCustomMapper monitorWorkplaceHarmFactorCustomMapper;
    /** 注入checkPlanHarmFactorMapper的bean */
    @Autowired
    private CheckPlanHarmFactorMapper checkPlanHarmFactorMapper;
    @Autowired
    private WaitCheckService waitCheckService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private WaitMedicalService waitMedicalService;

    @Autowired
    private RemindService remindService;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    @Override
    /**
     * @description 根据企业id查询年度计划list
     * @param year 年度
     * @param enterpriseId 企业id
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseYearPlanCustom> 年度计划扩展list
     */
    public List<EnterpriseYearPlanCustom> listYearplansByEnterpriseId(String year, Long enterpriseId) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("year", year);
        condition.put("enterpriseId", enterpriseId);
        return enterpriseYearPlanCustomMapper.listYearplans(condition);
    }

    @Override
    /**
     * @description 根据id查询年度计划
     * @param id 年度计划id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseYearPlan对象
     */
    public EnterpriseYearPlan getYearPlanById(Long id) {
        return enterpriseYearPlanMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增年度计划信息
     * @param enterpriseYearPlan 年度计划pojo
     * @param planFile 计划文件
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertYearplan(EnterpriseYearPlan enterpriseYearPlan, MultipartFile planFile) {
        // 新增数据先保存，以便获取id
        enterpriseYearPlanMapper.insert(enterpriseYearPlan);
        // 上传并保存年度计划文件
        if (planFile != null){
            String plan = "/" + enterpriseYearPlan.getEnterpriseId() + "/" + ModuleName.YEARPLAN.getValue()
                    + "/" + enterpriseYearPlan.getId() + "/" + planFile.getOriginalFilename();
            try {
                ossTools.uploadStream(plan, planFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传年度计划文件失败", e);
            }
            enterpriseYearPlan.setFileName(planFile.getOriginalFilename());
            enterpriseYearPlan.setPlanFile(plan);
        }


        //新增的计划如果在当前月份之前，则创建待办事项
        if(enterpriseYearPlan.getPlanType().equals(EnterpriseYearPlanCustom.TYPE_JKTJ)){
            //健康体检待办事项
            insertWaitMedical(enterpriseYearPlan);
        }else if(enterpriseYearPlan.getPlanType().equals(EnterpriseYearPlanCustom.TYPE_FHSS)){
            //防护设施维护检修计划
            insertProtectMeasureRemind(enterpriseYearPlan);
        }else if(enterpriseYearPlan.getPlanType().equals(EnterpriseYearPlanCustom.TYPE_JYYL)){
            //应急救援演练计划
            insertRescueRemind(enterpriseYearPlan);
        }

        return enterpriseYearPlanMapper.updateByPrimaryKeySelective(enterpriseYearPlan);
    }

    @Override
    /**
     * @description 更新年度计划信息
     * @param enterpriseYearPlan 年度计划pojo
     * @param planFile 计划文件
     * @return int 更新成功的数目
     */
    @Transactional
    public int updateYearplan(EnterpriseYearPlan enterpriseYearPlan, MultipartFile planFile) {
        // 上传并保存年度计划文件
        if (planFile != null){
            // 上传了新的年度计划文件
            if (!StringUtils.isEmpty(enterpriseYearPlan.getPlanFile())){
                // 删除旧文件
                ossTools.deleteOSS(enterpriseYearPlan.getPlanFile());
            }
            String plan = "/" + enterpriseYearPlan.getEnterpriseId() + "/" + ModuleName.YEARPLAN.getValue()
                    + "/" + enterpriseYearPlan.getId() + "/" + planFile.getOriginalFilename();
            try {
                ossTools.uploadStream(plan, planFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传年度计划文件失败", e);
            }
            enterpriseYearPlan.setFileName(planFile.getOriginalFilename());
            enterpriseYearPlan.setPlanFile(plan);
        }

        //修改的计划如果在当前月份之前，则创建待办事项
        if(enterpriseYearPlan.getPlanType().equals(EnterpriseYearPlanCustom.TYPE_JKTJ)){
            //健康体检待办事项
            insertWaitMedical(enterpriseYearPlan);
        }else if(enterpriseYearPlan.getPlanType().equals(EnterpriseYearPlanCustom.TYPE_FHSS)){
            //防护设施维护检修计划
            insertProtectMeasureRemind(enterpriseYearPlan);
        }else if(enterpriseYearPlan.getPlanType().equals(EnterpriseYearPlanCustom.TYPE_JYYL)){
            //应急救援演练计划
            insertRescueRemind(enterpriseYearPlan);
        }

        return enterpriseYearPlanMapper.updateByPrimaryKeySelective(enterpriseYearPlan);
    }

    //插入待岗中体检记录
    private void insertWaitMedical(EnterpriseYearPlan enterpriseYearPlan) {
        if(enterpriseYearPlan.getPlanType().equals("jktj")){
            LocalDate localDate = LocalDate.now();
            localDate = LocalDate.of(localDate.getYear(),localDate.getMonthValue(),1);

            String[] planPeriods = enterpriseYearPlan.getPlanPeriod().split(",");
            String year = enterpriseYearPlan.getYear();
            for(String planPeriod : planPeriods){
                if(org.apache.commons.lang3.StringUtils.isNotEmpty(planPeriod)){
                    int medicalMonth = Integer.parseInt(planPeriod);
                    String medicalDateStr = year + "-" + medicalMonth;
                    LocalDate medicalDate = LocalDate.of(Integer.parseInt(year),medicalMonth,1);
                    if(medicalDate.isBefore(localDate)){
                        //如果计划体检日期，在当前日期之前，则插入待体检记录
                        //查找所有在岗的员工
                        List<EnterpriseEmployeeCustom> employeeCustoms =  employeeService.findWorkEmployeesByEnterpriseId(enterpriseYearPlan.getEnterpriseId());
                        for(EnterpriseEmployeeCustom employeeCustom : employeeCustoms){
                            //查询该员工在待体检表中是否已经生成待体检记录
                            Map<String,Object> condition = Maps.newHashMap();
                            condition.put("type", EmployeeWaitMedicalCustom.TYPE_IN_POST);
                            condition.put("waitMeticalDate",medicalDateStr);
                            condition.put("employeeId",employeeCustom.getId());
                            List<EmployeeWaitMedicalCustom> waitMedicalCustoms =  waitMedicalService.queryList(condition);
                            if(waitMedicalCustoms.size() ==  0){
                                //该员工还未生成待体检记录，这里插入待体检记录
                                EmployeeWaitMedical employeeWaitMedical = new EmployeeWaitMedical();
                                employeeWaitMedical.setEmployeeId(employeeCustom.getId());
                                employeeWaitMedical.setPostId(employeeCustom.getPostId());
                                employeeWaitMedical.setWorkDate(DateUtil.localDate2Date(medicalDate));
                                employeeWaitMedical.setStatus(EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
                                employeeWaitMedical.setType(EmployeeWaitMedicalCustom.TYPE_IN_POST);
                                employeeWaitMedical.setWaitMeticalDate(medicalDateStr);
                                employeeWaitMedical.setGmtCreate(new Date());
                                waitMedicalService.addWaitMedical(employeeWaitMedical);
                            }

                        }

                    }
                }
            }
        }
    }

    /**
     * 新增防护设施维修计划
     * @param enterpriseYearPlan
     */
    private void insertProtectMeasureRemind(EnterpriseYearPlan enterpriseYearPlan){
        LocalDate localDate = LocalDate.now();
        localDate = LocalDate.of(localDate.getYear(),localDate.getMonthValue(),1);
        String[] planPeriods = enterpriseYearPlan.getPlanPeriod().split(",");
        String planYear = enterpriseYearPlan.getYear();
        for(String planPeriod : planPeriods){
            if(org.apache.commons.lang3.StringUtils.isNotEmpty(planPeriod)){
                int planMonth = Integer.parseInt(planPeriod);
                String planDateStr = planYear + "-" + planMonth;
                LocalDate planDate = LocalDate.of(Integer.parseInt(planYear),planMonth,1);
                if(planDate.isBefore(localDate)){
                    //如果计划日期，在当前日期之前，则插入待办事项
                    //查找数据库中是否已经存在该提醒，如果存在则不插入
                    EnterpriseRemind remind = new EnterpriseRemind();
                    remind.setEnterpriseId(enterpriseYearPlan.getEnterpriseId());
                    remind.setPlanDate(planDateStr);
                    remind.setType(EnterpriseRemindCustom.TYPE_FHSS);
                    List<EnterpriseRemind> remindList = remindService.findRemeind(remind);
                    if(remindList.size() == 0){
                        //该计划日期提醒不存在，则插入该计划提醒
                        remind.setStatus(EnterpriseRemindCustom.STATUS_DTX);
                        remind.setGmtCreate(new Date());
                        remind.setContent("您计划于"+enterpriseYearPlan.getYear()+"-"+planMonth+"月进行防护设施维护检修");
                        remindService.insert(remind);
                    }
                }
            }
        }

    }

    /**
     * 新增应急救援演练提醒
     * @param enterpriseYearPlan
     */
    private void insertRescueRemind(EnterpriseYearPlan enterpriseYearPlan){
        LocalDate localDate = LocalDate.now();
        localDate = LocalDate.of(localDate.getYear(),localDate.getMonthValue(),1);
        String[] planPeriods = enterpriseYearPlan.getPlanPeriod().split(",");
        String planYear = enterpriseYearPlan.getYear();
        for(String planPeriod : planPeriods){
            if(org.apache.commons.lang3.StringUtils.isNotEmpty(planPeriod)){
                int planMonth = Integer.parseInt(planPeriod);
                String planDateStr = planYear + "-" + planMonth;
                LocalDate planDate = LocalDate.of(Integer.parseInt(planYear),planMonth,1);
                if(planDate.isBefore(localDate)){
                    //如果计划日期，在当前日期之前，则插入待办事项
                    //查找数据库中是否已经存在该提醒，如果存在则不插入
                    EnterpriseRemind remind = new EnterpriseRemind();
                    remind.setEnterpriseId(enterpriseYearPlan.getEnterpriseId());
                    remind.setPlanDate(planDateStr);
                    remind.setType(EnterpriseRemindCustom.TYPE_YJJY);
                    List<EnterpriseRemind> remindList = remindService.findRemeind(remind);
                    if(remindList.size() == 0){
                        //该计划日期提醒不存在，则插入该计划提醒
                        remind.setStatus(EnterpriseRemindCustom.STATUS_DTX);
                        remind.setGmtCreate(new Date());
                        remind.setContent("您计划于"+enterpriseYearPlan.getYear()+"-"+planMonth+"月进行紧急救援演练");
                        remindService.insert(remind);
                    }
                }
            }
        }
    }

    @Override
    /**
     * @description 根据条件查询年度计划
     * @param condition 查询条件
     * @return EnterpriseYearPlan 年度计划
     */
    public EnterpriseYearPlan findYearplanByCondition(Map<String, Object> condition) {
        return enterpriseYearPlanCustomMapper.findYearplanByCondition(condition);
    }

    @Override
    public List<EnterpriseYearPlan> findYearplansByCondition(Map<String, Object> condition) {
        return enterpriseYearPlanCustomMapper.findYearplansByCondition(condition);
    }


    /**
     * @description 根据条件查询年度计划扩展
     * @param condition 查询条件
     * @return EnterpriseYearPlanCustom 年度计划扩展pojo
     */
    @Override
    public EnterpriseYearPlanCustom getYearplanByCondition(Map<String, Object> condition) {
        return enterpriseYearPlanCustomMapper.getYearplanByCondition(condition);
    }

    /**
     * @description 根据条件查询年度检测计划扩展
     * @param condition 查询条件
     * @return EnterpriseYearPlanCustom 年度计划扩展pojo
     */
    @Override
    public List<EnterpriseYearPlanCustom> listYearCheckPlans(Map<String, Object> condition) {
        return enterpriseYearPlanCustomMapper.listYearCheckPlans(condition);
    }

    /**
     * @description 获取该企业监测点涉及的危害因素
     * @param condition 查询条件(企业id,关键字)
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    public JSONObject getCheckPlanHarmFactors(Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        List<MonitorWorkplaceHarmFactorCustom> monitorWorkplaceHarmFactorCustoms
                = monitorWorkplaceHarmFactorCustomMapper.getCheckPlanHarmFactors(condition);
        result.put("monitorWorkplaceHarmFactorCustoms", monitorWorkplaceHarmFactorCustoms);
        return result;
    }

    /**
     * @description 新增年度检测计划和检测的危害因素集合
     * @param enterpriseYearPlanCustom 年度计划扩展pojo
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertYearCheckPlan(EnterpriseYearPlanCustom enterpriseYearPlanCustom) {
        // 保存年度检测计划
        EnterpriseYearPlan enterpriseYearPlan = new EnterpriseYearPlan();
        enterpriseYearPlan.setEnterpriseId(enterpriseYearPlanCustom.getEnterpriseId());
        enterpriseYearPlan.setYear(enterpriseYearPlanCustom.getYear());
        enterpriseYearPlan.setPlanType(enterpriseYearPlanCustom.getPlanType());
        enterpriseYearPlan.setPlanPeriod(enterpriseYearPlanCustom.getPlanPeriod());
        enterpriseYearPlan.setGmtCreate(new Date());
        enterpriseYearPlanMapper.insert(enterpriseYearPlan);
        enterpriseYearPlanCustom.setId(enterpriseYearPlan.getId());
        // 保存检测的危害因素集合
        String[] harmFactorIds = enterpriseYearPlanCustom.getTempHarmFactorId().split(",");
        for (int i = 0; i < harmFactorIds.length; i++) {
            Long harmFactorId = Long.parseLong(harmFactorIds[i]);
            CheckPlanHarmFactor checkPlanHarmFactor = new CheckPlanHarmFactor();
            checkPlanHarmFactor.setCheckPlanId(enterpriseYearPlan.getId());
            checkPlanHarmFactor.setHarmFactorId(harmFactorId);
            checkPlanHarmFactor.setGmtCreate(new Date());
            checkPlanHarmFactorMapper.insert(checkPlanHarmFactor);
        }

        //生成待检测记录
        addWaitCheckHarmFactor(enterpriseYearPlanCustom);
        return harmFactorIds.length;
    }

    /**
     * @description 更新年度检测计划和检测的危害因素集合(先删除,再新增)
     * @param enterpriseYearPlanCustom 年度计划扩展pojo
     * @return int 更新成功的数目
     */
    @Override
    @Transactional
    public int updateYearCheckPlan(EnterpriseYearPlanCustom enterpriseYearPlanCustom) {
        // 更新年度检测计划
        EnterpriseYearPlan enterpriseYearPlan = new EnterpriseYearPlan();
        enterpriseYearPlan.setId(enterpriseYearPlanCustom.getId());
        enterpriseYearPlan.setPlanPeriod(enterpriseYearPlanCustom.getPlanPeriod());
        enterpriseYearPlan.setGmtModified(new Date());
        enterpriseYearPlanMapper.updateByPrimaryKeySelective(enterpriseYearPlan);
        // 删除该检测计划的危害因素集合
        CheckPlanHarmFactorExample checkPlanHarmFactorExample = new CheckPlanHarmFactorExample();
        CheckPlanHarmFactorExample.Criteria criteria = checkPlanHarmFactorExample.createCriteria();
        criteria.andCheckPlanIdEqualTo(enterpriseYearPlanCustom.getId());
        checkPlanHarmFactorMapper.deleteByExample(checkPlanHarmFactorExample);
        // 保存新增的检测危害因素集合
        String[] harmFactorIds = enterpriseYearPlanCustom.getTempHarmFactorId().split(",");
        for (int i = 0; i < harmFactorIds.length; i++) {
            Long harmFactorId = Long.parseLong(harmFactorIds[i]);
            CheckPlanHarmFactor checkPlanHarmFactor = new CheckPlanHarmFactor();
            checkPlanHarmFactor.setCheckPlanId(enterpriseYearPlan.getId());
            checkPlanHarmFactor.setHarmFactorId(harmFactorId);
            checkPlanHarmFactor.setGmtCreate(new Date());
            checkPlanHarmFactorMapper.insert(checkPlanHarmFactor);
        }

        //添加危害因素待检测记录
        addWaitCheckHarmFactor(enterpriseYearPlanCustom);

        return harmFactorIds.length;
    }

    private void addWaitCheckHarmFactor(EnterpriseYearPlanCustom enterpriseYearPlanCustom) {
        LocalDate localDate = LocalDate.now();
        String year = enterpriseYearPlanCustom.getYear();
        int month = Integer.parseInt(enterpriseYearPlanCustom.getPlanPeriod());
        LocalDate checkDate = LocalDate.of(Integer.parseInt(year),month,1);
        if(checkDate.isBefore(localDate)){
            //根据检测计划id查看待检测记录表是否已经生成待检测记录，如果未生成，则生成检测计划
            Map<String,Object> parameter = Maps.newHashMap();
            parameter.put("checkPlanId",enterpriseYearPlanCustom.getId());
            parameter.put("checkMonth",month);
            List<EnterpriseWaitCheckCustom> waitCheckCustoms = waitCheckService.findEnterpriseWaitChecks(parameter);
            if(waitCheckCustoms.size() > 0){
                //已经存在待检测记录，不在更新

            }else{
                EnterpriseWaitCheck waitCheck = new EnterpriseWaitCheck();
                waitCheck.setEnterpriseId(enterpriseYearPlanCustom.getEnterpriseId());
                waitCheck.setYear(Integer.parseInt(year));
                waitCheck.setCheckMonth(month);
                waitCheck.setGmtCreate(new Date());
                waitCheck.setCheckPlanId(enterpriseYearPlanCustom.getId());
                waitCheck.setStatus(EnterpriseWaitCheckCustom.STATUS_CHECK_NO);
                waitCheckService.insert(waitCheck);

                String[] harmFactorIds = enterpriseYearPlanCustom.getTempHarmFactorId().split(",");
                for (String harmFactorId : harmFactorIds) {
                    WaitCheckHarmFactor waitCheckHarmFactor = new WaitCheckHarmFactor();
                    waitCheckHarmFactor.setWaitCheckId(waitCheck.getId());
                    waitCheckHarmFactor.setHarmFactorId(Long.parseLong(harmFactorId));
                    waitCheckHarmFactor.setGmtCreate(new Date());
                    waitCheckService.insertWaitCheckHarmFactor(waitCheckHarmFactor);
                }
            }



        }
    }

    /**
     * @description 根据id查询年度检测计划
     * @param id 年度检测计划id
     * @return com.lanware.management.pojo.EnterpriseYearPlanCustom 返回EnterpriseYearPlanCustom对象
     */
    @Override
    public EnterpriseYearPlanCustom getYearCheckPlanById(Long id) {
        return enterpriseYearPlanCustomMapper.getYearCheckPlanById(id);
    }

    /**
     * @description 查询完成的年度计划数量
     * @param condition 查询条件
     * @return int 查询到的数量
     */
    @Override
    public int getYearPlanCompletionNum(Map<String, Object> condition) {
        return enterpriseYearPlanCustomMapper.getYearPlanCompletionNum(condition);
    }

    @Override
    @Transactional
    public void deleteYearPlanById(Long id){
        enterpriseYearPlanMapper.deleteByPrimaryKey(id);
    }
}
