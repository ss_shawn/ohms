package com.lanware.ehs.enterprise.basicinfo.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.basicinfo.service.BasicInfoService;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 企业基础信息视图
 */
@Controller("basicinfoView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @Autowired
    private BasicInfoService basicInfoService;

    /**
     * 企业基础信息查看页面
     * @return
     */
    @GetMapping("basicInfo")
    public String basicInfo() {
        return "redirect:/basicInfo";
    }

    /**
     * 企业基础信息编辑页面
     * @return
     */
    @GetMapping("basicInfo/update/{id}")
    public ModelAndView baseicInfoUpdate(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        if (id != null){
            EnterpriseBaseinfo enterpriseBaseinfo =  basicInfoService.findById(id);
            data.put("enterpriseBaseinfo", enterpriseBaseinfo);
        }
        return new ModelAndView("basicInfo/edit", data);
    }
}
