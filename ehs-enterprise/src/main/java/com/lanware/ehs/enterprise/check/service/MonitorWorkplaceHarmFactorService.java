package com.lanware.ehs.enterprise.check.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.custom.MonitorWorkplaceHarmFactorCustom;

import java.util.Map;

/**
 * @description 监测点关联的作业场所和监测危害因素interface
 */
public interface MonitorWorkplaceHarmFactorService {

    JSONObject listWorkplaceHarmFactors(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    JSONObject getSelectWorkplaces(Map<String, Object> condition);

    JSONObject getSelectHarmFactors(Map<String, Object> condition);

    int insert(MonitorWorkplaceHarmFactorCustom monitorWorkplaceHarmFactorCustom);

    int update(MonitorWorkplaceHarmFactorCustom monitorWorkplaceHarmFactorCustom);

    MonitorWorkplaceHarmFactorCustom getMonitorWorkplaceHarmFactorCustom(Map<String, Object> condition);

    int delete(Long monitorPointId, Long workplaceId);

    int deletes(Long monitorPointId, Long[] workplaceIds);
}
