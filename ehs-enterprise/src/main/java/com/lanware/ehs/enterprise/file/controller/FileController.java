package com.lanware.ehs.enterprise.file.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.file.service.FileService;
import com.lanware.ehs.pojo.EnterpriseFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 文件管理controller
 */
@Controller
@RequestMapping("/file")
public class FileController {
    /** 注入fileService的bean */
    @Autowired
    private FileService fileService;

    @RequestMapping("")
    /**
     * @description 返回文件管理页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "file/list";
    }

    @RequestMapping("/listFiles")
    @ResponseBody
    /**
     * @description 获取文件管理result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listFiles(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = fileService.listFiles(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editFile/save")
    @ResponseBody
    /**
     * @description 保存文件信息
     * @param enterpriseFileJSON 文件信息json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseFileJSON, @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseFile enterpriseFile
                = JSONObject.parseObject(enterpriseFileJSON, EnterpriseFile.class);
        if (enterpriseFile.getId() == null) {
            enterpriseFile.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseFile.setGmtCreate(new Date());
            if (fileService.insertFile(enterpriseFile) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseFile.setGmtModified(new Date());
            if (fileService.updateFile(enterpriseFile) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteFile")
    @ResponseBody
    /**
     * @description 删除文件信息
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteFile(Long id) {
        if (fileService.deleteFile(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteFiles")
    @ResponseBody
    /**
     * @description 批量删除文件信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteFiles(Long[] ids) {
        if (fileService.deleteFiles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
