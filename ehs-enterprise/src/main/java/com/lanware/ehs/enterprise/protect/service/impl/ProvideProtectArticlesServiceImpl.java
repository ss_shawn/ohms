package com.lanware.ehs.enterprise.protect.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.protect.service.ProtectService;
import com.lanware.ehs.enterprise.protect.service.ProvideProtectArticlesService;
import com.lanware.ehs.mapper.*;
import com.lanware.ehs.mapper.EnterpriseProvideProtectArticlesMapper;
import com.lanware.ehs.mapper.custom.*;
import com.lanware.ehs.mapper.custom.EnterpriseProvideProtectArticlesCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.*;
import com.lanware.ehs.pojo.custom.EnterpriseProvideProtectArticlesCustom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 物品发放interface实现类
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ProvideProtectArticlesServiceImpl implements ProvideProtectArticlesService {
    @Autowired
    /** 注入EnterpriseProvideProtectArticlesMapper的bean */
    private EnterpriseProvideProtectArticlesMapper enterpriseProvideProtectArticlesMapper;
    @Autowired
    /** 注入EnterpriseProvideProtectArticlesMapper的bean */
    private EnterpriseProvideProtectArticlesCustomMapper enterpriseProvideProtectArticlesCustomMapper;
    @Autowired
    /** 注入EmployeePostCustomMapper的bean */
    private EmployeePostCustomMapper employeePostCustomMapper;
    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private EnterpriseProtectArticlesMapper enterpriseProtectArticlesMapper;
    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private EnterpriseProtectArticlesCustomMapper enterpriseProtectArticlesCustomMapper;

    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private EnterprisePostCustomMapper enterprisePostCustomMapper;

    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private EnterpriseWaitProvideProtectArticlesCustomMapper enterpriseWaitProvideProtectArticlesCustomMapper;
    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private EnterpriseWaitProvideProtectArticlesMapper enterpriseWaitProvideProtectArticlesMapper;


    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    

    @Override
    /**
     * @description 根据id查询防护用品发放
     * @param id 防护用品id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProvideProtectArticles对象
     */
    public EnterpriseProvideProtectArticles getEnterpriseProvideProtectArticlesById(Long id) {
        return enterpriseProvideProtectArticlesMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 根据条件查询防护用品发放
     * @param condition 模糊查询条件等
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProvideProtectArticles对象
     */
    @Override
    public  Page<EnterpriseProvideProtectArticlesCustom>  findProtectList(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseProvideProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return enterpriseProvideProtectArticlesCustomMapper.findProtectList(condition);
    }


    /**
     * 保存防护用品发放,
     * @param enterpriseProvideProtectArticles 防护用品发放 signatureRecord 签名记录文件
     * @return成功与否 1成功 0不成功
     */
    @Override
    public int saveEnterpriseProvideProtectArticles(EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticles, MultipartFile signatureRecord){
        int n=0;
        if(enterpriseProvideProtectArticles.getId()!=null){//修改
           // n=enterpriseProvideProtectArticlesMapper.updateByPrimaryKey(enterpriseProvideProtectArticles);
        }else{
            //新增防护用品发放
            enterpriseProvideProtectArticles.setGmtCreate(new Date());
            n=enterpriseProvideProtectArticlesMapper.insert(enterpriseProvideProtectArticles);
        }
        //查询该人员岗位是否发放完成

        EnterpriseWaitProvideProtectArticles enterpriseWaitProvideProtectArticles=new EnterpriseWaitProvideProtectArticles();
        enterpriseWaitProvideProtectArticles.setStatus(1);
        EnterpriseWaitProvideProtectArticlesExample example=new EnterpriseWaitProvideProtectArticlesExample();
        example.createCriteria().andEmployeePostIdEqualTo(enterpriseProvideProtectArticles.getEmployeePostId()).andProtectArticlesIdEqualTo(enterpriseProvideProtectArticles.getProvideArticlesId());
        enterpriseWaitProvideProtectArticlesMapper.updateByExampleSelective(enterpriseWaitProvideProtectArticles,example);


        //上传图片
        if (signatureRecord != null){
            //上传了新的聘用书
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseProvideProtectArticles.getSignatureRecord())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseProvideProtectArticles.getSignatureRecord());
            }
            //上传并保存新文件
            String imageFilePath = "/" + enterpriseProvideProtectArticles.getEnterpriseId() + "/" + ModuleName.PROTECTARTICLES.getValue() + "/" + enterpriseProvideProtectArticles.getId() + "/" + signatureRecord.getOriginalFilename();
            try {
                ossTools.uploadStream(imageFilePath, signatureRecord.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传签名记录失败", e);
            }
            enterpriseProvideProtectArticles.setSignatureRecord(imageFilePath);
        }
        enterpriseProvideProtectArticles.setGmtModified(new Date());
        n=enterpriseProvideProtectArticlesMapper.updateByPrimaryKey(enterpriseProvideProtectArticles);
        return n;
    }
    /**
     * 保存防护用品批量发放,
     * @param enterpriseProvideProtectArticles 防护用品发放 signatureRecord 签名记录文件
     * @return成功与否 1成功 0不成功
     */
    @Override
    public int saveEnterpriseProvideProtectArticlesAll(EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticles, MultipartFile signatureRecord){
        String allEmployeeId=enterpriseProvideProtectArticles.getAllEmployeeId();
        long postId=enterpriseProvideProtectArticles.getEmployeePostId();
        enterpriseProvideProtectArticles.setGmtCreate(new Date());
        int n=0;
        boolean isNotUploud=true;
        if(allEmployeeId!=null){//选中人的集合
            String[] allEmployeeIds=allEmployeeId.split(",");
            for(String employeePostIds:allEmployeeIds){
                String[] tempEmployeePostId=employeePostIds.split("\\|");
                long employeePostId=Long.parseLong(tempEmployeePostId[0]);
                long employeeId=Long.parseLong(tempEmployeePostId[1]);
                EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticlesCustom= (EnterpriseProvideProtectArticlesCustom) enterpriseProvideProtectArticles.clone();
                enterpriseProvideProtectArticlesCustom.setEmployeePostId(employeePostId);
                enterpriseProvideProtectArticlesCustom.setEmployeeId(employeeId);
                n=n+enterpriseProvideProtectArticlesMapper.insert(enterpriseProvideProtectArticlesCustom);
                if(isNotUploud){//没有上传附件，需先传
                    //上传图片
                    if (signatureRecord != null){
                        //上传了新的聘用书
                        if (!org.springframework.util.StringUtils.isEmpty(enterpriseProvideProtectArticles.getSignatureRecord())){
                            //删除旧文件
                            ossTools.deleteOSS(enterpriseProvideProtectArticles.getSignatureRecord());
                        }
                        //上传并保存新文件
                        String imageFilePath = "/" + enterpriseProvideProtectArticles.getEnterpriseId() + "/" + ModuleName.PROTECTARTICLES.getValue() + "/" + enterpriseProvideProtectArticlesCustom.getId() + "/" + signatureRecord.getOriginalFilename();
                        try {
                            ossTools.uploadStream(imageFilePath, signatureRecord.getInputStream());
                        } catch (IOException e) {
                            e.printStackTrace();
                            throw new EhsException("上传签名记录失败", e);
                        }
                        enterpriseProvideProtectArticles.setSignatureRecord(imageFilePath);
                        enterpriseProvideProtectArticlesCustom.setSignatureRecord(imageFilePath);
                        enterpriseProvideProtectArticlesMapper.updateByPrimaryKey(enterpriseProvideProtectArticlesCustom);
                    }
                    isNotUploud=false;
                }
                //修改待发放状态
                EnterpriseWaitProvideProtectArticles enterpriseWaitProvideProtectArticles=new EnterpriseWaitProvideProtectArticles();
                enterpriseWaitProvideProtectArticles.setStatus(1);
                EnterpriseWaitProvideProtectArticlesExample example=new EnterpriseWaitProvideProtectArticlesExample();
                example.createCriteria().andEmployeePostIdEqualTo(enterpriseProvideProtectArticlesCustom.getEmployeePostId()).andProtectArticlesIdEqualTo(enterpriseProvideProtectArticlesCustom.getProvideArticlesId());
                enterpriseWaitProvideProtectArticlesMapper.updateByExampleSelective(enterpriseWaitProvideProtectArticles,example);
            }

        }


        return n;
    }
    /**
     * 保存防护用品发放签名记录文件,
     * @param id 防护用品发放 signatureRecord 签名记录文件
     * @return成功与否 1成功 0不成功
     */
    @Override
    public int uploadSignatureRecord(Long id, MultipartFile signatureRecord){

        EnterpriseProvideProtectArticles enterpriseProvideProtectArticles=enterpriseProvideProtectArticlesMapper.selectByPrimaryKey(id);
        //上传图片
        if (signatureRecord != null){

            //上传并保存新文件
            String imageFilePath = "/" + enterpriseProvideProtectArticles.getEnterpriseId() + "/" + ModuleName.PROTECTARTICLES.getValue() + "/" + enterpriseProvideProtectArticles.getId() + "/" + signatureRecord.getOriginalFilename();
            try {
                ossTools.uploadStream(imageFilePath, signatureRecord.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传签名记录失败", e);
            }
            enterpriseProvideProtectArticles.setSignatureRecord(imageFilePath);
        }
        enterpriseProvideProtectArticles.setGmtModified(new Date());
        int n=enterpriseProvideProtectArticlesMapper.updateByPrimaryKey(enterpriseProvideProtectArticles);
        return n;
    }
    /**
     * 修改防护用品发放,//暂未使用
     * @param enterpriseProvideProtectArticles
     * @return成功与否 1成功 0不成功
     */
    @Override
    public int updateEnterpriseProvideProtectArticles(EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticles){
        int n=enterpriseProvideProtectArticlesMapper.insert(enterpriseProvideProtectArticles);
        return n;
    }

    /**
     * 通过id获取防护用品发放
     * @param id
     * @return防护用品发放
     */
    @Override
    public EnterpriseProvideProtectArticlesCustom getEnterpriseProvideProtectArticlesCustomByID(long id){
        return  enterpriseProvideProtectArticlesCustomMapper.selectByPrimaryKey(id);
    }
    /**
     * 删除防护用品发放,
     * @param id
     * @return成功与否 1成功 0不成功
     */
    @Override
    public int deleteEnterpriseProvideProtectArticles(long id){
        //查询该防护用品
        EnterpriseProvideProtectArticles enterpriseProvideProtectArticles=enterpriseProvideProtectArticlesMapper.selectByPrimaryKey(id);
        //删除防护用品发放
        int n=enterpriseProvideProtectArticlesMapper.deleteByPrimaryKey(id);
        if(n==1){//处理待发放防护用品
            //查询该人员岗位是否发放完成
            EnterpriseProvideProtectArticlesExample enterpriseProvideProtectArticlesExample=new EnterpriseProvideProtectArticlesExample();
            enterpriseProvideProtectArticlesExample.createCriteria().andEmployeePostIdEqualTo(enterpriseProvideProtectArticles.getEmployeePostId()).andProvideArticlesIdEqualTo(enterpriseProvideProtectArticles.getProvideArticlesId());
            List temp=enterpriseProvideProtectArticlesMapper.selectByExample(enterpriseProvideProtectArticlesExample);
            EnterpriseWaitProvideProtectArticles enterpriseWaitProvideProtectArticles=new EnterpriseWaitProvideProtectArticles();
            if(temp.size()==0){
                enterpriseWaitProvideProtectArticles.setStatus(0);
                EnterpriseWaitProvideProtectArticlesExample example=new EnterpriseWaitProvideProtectArticlesExample();
                example.createCriteria().andEmployeePostIdEqualTo(enterpriseProvideProtectArticles.getEmployeePostId()).andProtectArticlesIdEqualTo(enterpriseProvideProtectArticles.getProvideArticlesId());;
                enterpriseWaitProvideProtectArticlesMapper.updateByExampleSelective(enterpriseWaitProvideProtectArticles,example);
            }else{//没有未发放数量，更新带发放状态
                enterpriseWaitProvideProtectArticles.setStatus(1);
                EnterpriseWaitProvideProtectArticlesExample example=new EnterpriseWaitProvideProtectArticlesExample();
                example.createCriteria().andEmployeePostIdEqualTo(enterpriseProvideProtectArticles.getEmployeePostId()).andProtectArticlesIdEqualTo(enterpriseProvideProtectArticles.getProvideArticlesId());;
                enterpriseWaitProvideProtectArticlesMapper.updateByExampleSelective(enterpriseWaitProvideProtectArticles,example);
            }

        }
        return n;
    }

    /**
     * 根据员工ID获取员工岗位信息
     * @param condition
     * @return 员工岗位信息
     */
    @Override
    public List<EmployeePostCustom> getEmployeePostByEmployeeId(Map<String, Object> condition){
        return employeePostCustomMapper.queryEmployeePostByCondition(condition);
    }
    /**
     * 根据岗位ID获取员工岗位信息
     * @param condition
     * @return 员工岗位信息
     */
    @Override
    public List<EmployeePostCustom> listEmployeePostByPostId(Map<String, Object> condition){
        return employeePostCustomMapper.listEmployeePostByPostId(condition);
    }

    /**
     * 根据岗位信息
     * @param condition
     * @return 员工岗位信息
     */
    @Override
    public  List<EnterprisePostCustom>  getPost(Map<String, Object> condition){
        return enterprisePostCustomMapper.queryEnterprisePostByCondition(condition);
    }

    /**
     * 根据企业ID获取防护物品信息..暂时没用
     * @param enterpriseId 企业ID
     * @return 企业防护物品信息
     */
    @Override
    public List<EnterpriseProtectArticles> getProtectByEnterpriseId(Long enterpriseId){
        EnterpriseProtectArticlesExample enterpriseProtectArticlesExample=new EnterpriseProtectArticlesExample();
        enterpriseProtectArticlesExample.createCriteria().andEnterpriseIdEqualTo(enterpriseId);
        return enterpriseProtectArticlesMapper.selectByExample(enterpriseProtectArticlesExample);
    }
    /**
     * 根据人员岗位ID获取防护物品信息
     * @param employeePostId 人员岗位ID
     * @return 岗位防护用品信息
     */
    @Override
    public List<EnterpriseProtectArticlesCustom> getProtectByByEmployeePostId(Long employeePostId){

        return enterpriseProtectArticlesCustomMapper.findProtectListByEmployeePostId(employeePostId);
    }

    /**
     * 获取防护物品信息
     * @param
     * @return 岗位防护用品信息
     */
    @Override
    public List<EnterpriseProtectArticles> getProtect(Long enterpriseId){
        EnterpriseProtectArticlesExample enterpriseProtectArticlesExample=new EnterpriseProtectArticlesExample();
        enterpriseProtectArticlesExample.createCriteria().andEnterpriseIdEqualTo(enterpriseId);
        enterpriseProtectArticlesExample.setOrderByClause(" name");
        return enterpriseProtectArticlesMapper.selectByExample(enterpriseProtectArticlesExample);
    }

    /**
     * 根据人员岗位ID获取防护物品信息
     * @param employeePostId 人员岗位ID
     * @return 岗位防护用品信息
     */
    @Override
    public List<EnterpriseProtectArticlesCustom> getWaitProtectByByEmployeePostId(Long employeePostId){

        return enterpriseProtectArticlesCustomMapper.findWaitProtectListByEmployeePostId(employeePostId);
    }
    /**
     * @description 根据条件查询待发放防护用品发放
     * @param condition 模糊查询条件等
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProvideProtectArticles对象
     */
    @Override
    public  Page<EnterpriseWaitProvideProtectArticlesCustom>  findEnterpriseWaitProvideProtectArticles(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseWaitProvideProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return enterpriseWaitProvideProtectArticlesCustomMapper.findEnterpriseWaitProvideProtectArticles(condition);
    }
    /**
     * 通过待发放ID获取发放物品信息
     * @param id
     * @return防护用品发放
     */
    @Override
    public EnterpriseProvideProtectArticlesCustom getEnterpriseProvideProtectArticlesCustomByWaitID(long id){
        //查询出待发放物品信息
        EnterpriseWaitProvideProtectArticlesCustom enterpriseWaitProvideProtectArticlesCustom=enterpriseWaitProvideProtectArticlesCustomMapper.selectEnterpriseWaitProvideProtectArticlesByPrimaryKey(id);
        EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticlesCustom=new EnterpriseProvideProtectArticlesCustom();
        enterpriseProvideProtectArticlesCustom.setEmployeeId(enterpriseWaitProvideProtectArticlesCustom.getEmployeeId());//人员ID
        enterpriseProvideProtectArticlesCustom.setEmployeeName(enterpriseWaitProvideProtectArticlesCustom.getEmployeeName());
        enterpriseProvideProtectArticlesCustom.setEmployeePostId(enterpriseWaitProvideProtectArticlesCustom.getEmployeePostId());//人员岗位ID
        enterpriseProvideProtectArticlesCustom.setPostName(enterpriseWaitProvideProtectArticlesCustom.getPostName());
        enterpriseProvideProtectArticlesCustom.setProvideArticlesId(enterpriseWaitProvideProtectArticlesCustom.getProtectArticlesId());
        enterpriseProvideProtectArticlesCustom.setProvideArticlesName(enterpriseWaitProvideProtectArticlesCustom.getProvideArticlesName());
        return  enterpriseProvideProtectArticlesCustom;
    }

    /**
     * 返回个人防护用品发放情况
     * @param condition 条件 企业ID
     * @return 个人防护用品发放情况
     */
    @Override
    public List<Map<String, Object>> findProvideProtectArticles(Map<String,Object> condition){
        //注释的是按工作场所、岗位、应发放、人员、防护用品、数量
        //return enterpriseProvideProtectArticlesCustomMapper.findProvideProtectArticles(condition);
        return enterpriseProvideProtectArticlesCustomMapper.findProvideProtectArticlesByPost(condition);
    }
    /**
     * 返回个人防护用品发放情况
     * @param condition 条件 企业ID
     * @return 个人防护用品发放情况
     */
    @Override
    public List<Map<String, Object>> findProvideProtectArticlesByPost(Map<String,Object> condition){
        //按岗位、人员、防护用品、数量
        return enterpriseProvideProtectArticlesCustomMapper.findProvideProtectArticlesByPost(condition);
    }

    @Override
    public List<EnterpriseWaitProvideProtectArticlesCustom> queryWaitProvideProtectArticles(Map<String, Object> condition) {
        return enterpriseWaitProvideProtectArticlesCustomMapper.findEnterpriseWaitProvideProtectArticles(condition);
    }

    /**
     * 删除待发放防护用品记录
     * @param enterpriseWaitProvideProtectArticles
     * @return 修改条数
     */
    @Override
    public int delProvideProtectSubmit(EnterpriseWaitProvideProtectArticles enterpriseWaitProvideProtectArticles){
        return enterpriseWaitProvideProtectArticlesMapper.updateByPrimaryKeySelective(enterpriseWaitProvideProtectArticles);
    }

}
