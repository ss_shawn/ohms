package com.lanware.ehs.enterprise.toxicdrill.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.toxicdrill.service.ToxicDrillService;
import com.lanware.ehs.pojo.EnterpriseToxicDrill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 高毒演练记录视图
 */
@Controller("toxicDrillView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入toxicDrillService的bean */
    @Autowired
    private ToxicDrillService toxicDrillService;

    @RequestMapping("toxicDrill/editToxicDrill")
    /**
     * @description 新增高毒演练记录
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addToxicDrill() {
        JSONObject data = new JSONObject();
        EnterpriseToxicDrill enterpriseToxicDrill = new EnterpriseToxicDrill();
        data.put("enterpriseToxicDrill", enterpriseToxicDrill);
        return new ModelAndView("toxicDrill/edit", data);
    }

    /**
     * @description 编辑高毒演练记录
     * @param id 高毒演练记录id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("toxicDrill/editToxicDrill/{id}")
    public ModelAndView editToxicDrill(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseToxicDrill enterpriseToxicDrill = toxicDrillService.getToxicDrillById(id);
        data.put("enterpriseToxicDrill", enterpriseToxicDrill);
        return new ModelAndView("toxicDrill/edit", data);
    }

    /**
     * @description 返回新增记录文件页面
     * @param id 演练记录id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("toxicDrill/addRecordFile/{id}")
    public ModelAndView addRecordFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseToxicDrill enterpriseToxicDrill = toxicDrillService.getToxicDrillById(id);
        data.put("enterpriseToxicDrill", enterpriseToxicDrill);
        return new ModelAndView("toxicDrill/addRecordFile", data);
    }

    /**
     * @description 返回新增证明文件页面
     * @param id 演练记录id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("toxicDrill/addProveFile/{id}")
    public ModelAndView addProveFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseToxicDrill enterpriseToxicDrill = toxicDrillService.getToxicDrillById(id);
        data.put("enterpriseToxicDrill", enterpriseToxicDrill);
        return new ModelAndView("toxicDrill/addProveFile", data);
    }
}
