package com.lanware.ehs.enterprise.training.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.training.service.TrainingFileService;
import com.lanware.ehs.enterprise.training.service.TrainingService;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 培训管理视图
 */
@Controller("trainingView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入trainingService的bean */
    @Autowired
    private TrainingService trainingService;
    /** 注入trainingFileService的bean */
    @Autowired
    private TrainingFileService trainingFileService;
    @Autowired
    /** 注入yearplanService的bean */
    private YearplanService yearplanService;

    @RequestMapping("training/editTraining")
    /**
     * @description 新增培训信息
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addTraining() {
        JSONObject data = new JSONObject();
        EnterpriseTrainingCustom enterpriseTraining = new EnterpriseTrainingCustom();
        data.put("enterpriseTraining", enterpriseTraining);
        return new ModelAndView("training/edit", data);
    }

    /**
     * @description 编辑培训扩展信息
     * @param id 培训信息id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("training/editTraining/{id}")
    public ModelAndView editMedical(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseTrainingCustom enterpriseTraining = trainingService.getTrainingCustomById(id);
        data.put("enterpriseTraining", enterpriseTraining);
        return new ModelAndView("training/edit", data);
    }

    @RequestMapping("training/addAttendanceSheet/{id}")
    /**
     * @description 返回新增签到表页面
     * @param id 培训记录id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addAttendanceSheet(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseTrainingCustom enterpriseTraining = trainingService.getTrainingCustomById(id);
        data.put("enterpriseTraining", enterpriseTraining);
        return new ModelAndView("training/addAttendanceSheet", data);
    }

    @RequestMapping("training/trainingFile/{relationId}/{relationModule}")
    /**
     * @description 返回其他培训证明资料页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView trainingFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("training/trainingFile/list", data);
    }

    @RequestMapping("training/trainingFile/addTrainingFile/{relationId}/{relationModule}")
    /**
     * @description 新增培训证明资料
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addInformationFile(@PathVariable Long relationId, @PathVariable String relationModule) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(relationId);
        enterpriseRelationFile.setRelationModule(relationModule);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("training/trainingFile/edit", data);
    }

    @RequestMapping("training/trainingFile/editTrainingFile/{id}")
    /**
     * @description 编辑培训证明资料
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editInformationFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = trainingFileService.getTrainingFileById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("training/trainingFile/edit", data);
    }

    @RequestMapping("training/trainingEmployee/{trainingId}")
    /**
     * 参加培训的普通员工页面
     * @param trainingId 培训id
     * @return 参加培训的普通员工model
     */
    public ModelAndView trainingEmployee(@PathVariable Long trainingId){
        JSONObject data = new JSONObject();
        data.put("trainingId", trainingId);
        return new ModelAndView("training/trainingEmployee/list", data);
    }

    @RequestMapping("training/selectTrainingEmployee/{trainingId}")
    /**
     * @description 选择参加培训的普通员工
     * @param trainingId 培训id
     * @return 尚未参加培训的普通员工model
     */
    public ModelAndView selectTrainingEmployee(@PathVariable Long trainingId){
        JSONObject data = new JSONObject();
        data.put("trainingId", trainingId);
        return new ModelAndView("training/trainingEmployee/select", data);
    }

    @RequestMapping("training/trainingAdmin/{trainingId}")
    /**
     * 参加培训的管理员页面
     * @param trainingId 培训id
     * @return 参加培训的管理员model
     */
    public ModelAndView trainingAdmin(@PathVariable Long trainingId){
        JSONObject data = new JSONObject();
        data.put("trainingId", trainingId);
        return new ModelAndView("training/trainingAdmin/list", data);
    }

    @RequestMapping("training/selectTrainingAdmin/{trainingId}")
    /**
     * @description 选择参加培训的管理员
     * @param trainingId 培训id
     * @return 尚未参加培训的管理员工model
     */
    public ModelAndView selectTrainingAdmin(@PathVariable Long trainingId){
        JSONObject data = new JSONObject();
        data.put("trainingId", trainingId);
        return new ModelAndView("training/trainingAdmin/select", data);
    }
}
