package com.lanware.ehs.enterprise.hygieneorg.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.hygieneorg.service.CertificateService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseRelationFileCustom;
import com.lanware.ehs.service.EnterpriseRelationFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/hygieneOrg/certificate")
public class CertificateController {

    private EnterpriseRelationFileService enterpriseRelationFileService;

    private CertificateService certificateService;

    @Autowired
    public CertificateController(EnterpriseRelationFileService enterpriseRelationFileService, CertificateService certificateService){
        this.enterpriseRelationFileService = enterpriseRelationFileService;
        this.certificateService = certificateService;
    }

    /**
     * 职业卫生组织资质证书页面
     * @param hygieneOrgId 职业卫生组织id
     * @return 职业卫生组织资质证书model
     */
    @RequestMapping("")
    public ModelAndView certificate(Long hygieneOrgId){
        JSONObject data = new JSONObject();
        data.put("hygieneOrgId", hygieneOrgId);
        return new ModelAndView("hygieneOrg/certificate/list", data);
    }

    /**
     * 查询职业卫生组织资质证书集合
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public ResultFormat getList(@RequestParam Long hygieneOrgId, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("relationId", hygieneOrgId);
        condition.put("relationModule", ModuleName.HYGIENEORG.getValue());
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<EnterpriseRelationFileCustom> page = enterpriseRelationFileService.queryEnterpriseRelationFileByCondition(0, 0, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }

    /**
     * 编辑职业卫生组织资质证书页面
     * @param id 资质证书id
     * @return 编辑职业卫生组织资质证书model
     */
    @RequestMapping("/edit")
    public ModelAndView edit(Long id, Long hygieneOrgId) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = null;
        if (id != null) {
            try {
                enterpriseRelationFile = enterpriseRelationFileService.selectByPrimaryKey(id);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }else {
            enterpriseRelationFile = new EnterpriseRelationFile();
            enterpriseRelationFile.setRelationId(hygieneOrgId);
        }
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("hygieneOrg/certificate/edit", data);
    }

    /**
     * 删除职业卫生组织资质证书
     * @param idsJSON 职业卫生组织资质证书id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultFormat delete(@RequestParam String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        int num = certificateService.deleteCertificateByIds(ids);
        if (num == 0){
            return ResultFormat.error("删除资质证书失败");
        }
        return ResultFormat.success();
    }

    /**
     * 保存职业卫生组织资质证书
     * @param enterpriseRelationFileJSON 职业卫生组织资质证书JSON
     * @param file 附件
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(@CurrentUser EnterpriseUser enterpriseUser, String enterpriseRelationFileJSON, MultipartFile file){
        EnterpriseRelationFile enterpriseRelationFile = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        try {
            int num = certificateService.saveCertificate(enterpriseUser.getEnterpriseId(), enterpriseRelationFile, file);
            if (num == 0){
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存资质证书失败");
            }
        } catch (EhsException e){
            e.printStackTrace();
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
        return EhsResult.ok();
    }

    /**
     * 查看职业卫生组织资质证书页面
     * @param id 资质证书id
     * @return 查看职业卫生组织资质证书model
     */
    @RequestMapping("/view")
    public ModelAndView view(Long id) {
        JSONObject data = new JSONObject();
        if (id != null) {
            try {
                EnterpriseRelationFile enterpriseRelationFile = enterpriseRelationFileService.selectByPrimaryKey(id);
                data.put("enterpriseRelationFile", enterpriseRelationFile);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return new ModelAndView("hygieneOrg/certificate/view", data);
    }
}
