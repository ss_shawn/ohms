package com.lanware.ehs.enterprise.inspectioninstitution.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.inspectioninstitution.service.InspectionInstitutionService;
import com.lanware.ehs.enterprise.inspectioninstitution.service.InstitutionCertificateService;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 检查机构视图
 */
@Controller("inspectionInstitutionView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入inspectionInstitutionService的bean */
    @Autowired
    private InspectionInstitutionService inspectionInstitutionService;
    /** 注入institutionCertificateService的bean */
    @Autowired
    private InstitutionCertificateService institutionCertificateService;

    @RequestMapping("inspectionInstitution/editInspectionInstitution")
    /**
     * @description 新增检查机构信息
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addInspectionInstitution() {
        JSONObject data = new JSONObject();
        EnterpriseInspectionInstitution enterpriseInspectionInstitution = new EnterpriseInspectionInstitution();
        data.put("enterpriseInspectionInstitution", enterpriseInspectionInstitution);
        return new ModelAndView("inspectionInstitution/edit", data);
    }

    /**
     * @description 编辑检查机构信息
     * @param id 检查机构id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("inspectionInstitution/editInspectionInstitution/{id}")
    public ModelAndView editMedical(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseInspectionInstitution enterpriseInspectionInstitution
                = inspectionInstitutionService.getInspectionInstitutionById(id);
        data.put("enterpriseInspectionInstitution", enterpriseInspectionInstitution);
        return new ModelAndView("inspectionInstitution/edit", data);
    }

    @RequestMapping("inspectionInstitution/addContract/{id}")
    /**
     * @description 返回新增机构合同页面
     * @param id 机构id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addContract(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseInspectionInstitution enterpriseInspectionInstitution
                = inspectionInstitutionService.getInspectionInstitutionById(id);
        data.put("enterpriseInspectionInstitution", enterpriseInspectionInstitution);
        return new ModelAndView("inspectionInstitution/addContract", data);
    }

    @RequestMapping("inspectionInstitution/institutionCertificate/{relationId}/{relationModule}")
    /**
     * @description 返回机构相关证书页面
     * @param relationId 检查机构id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView institutionCertificate(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("inspectionInstitution/institutionCertificate/list", data);
    }
    @RequestMapping("inspectionInstitution/institutionCertificateView/{relationId}/{relationModule}")
    /**
     * @description 返回机构相关证书页面
     * @param relationId 检查机构id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView institutionCertificateView(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("inspectionInstitution/institutionCertificate/listView", data);
    }
    @RequestMapping("inspectionInstitution/institutionCertificate/addInstitutionCertificate/{relationId}/{relationModule}")
    /**
     * @description 新增机构相关证书
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addInstitutionCertificate(@PathVariable Long relationId, @PathVariable String relationModule) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(relationId);
        enterpriseRelationFile.setRelationModule(relationModule);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("inspectionInstitution/institutionCertificate/edit", data);
    }

    @RequestMapping("inspectionInstitution/institutionCertificate/editInstitutionCertificate/{id}")
    /**
     * @description 编辑机构相关证书
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editInstitutionCertificate(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = institutionCertificateService.getInstitutionCertificateById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("inspectionInstitution/institutionCertificate/edit", data);
    }
}
