package com.lanware.ehs.enterprise.post.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.post.service.ProtectArticlesService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.PostProtectArticles;
import com.lanware.ehs.pojo.PostProtectArticlesExample;
import com.lanware.ehs.pojo.custom.PostProtectArticlesCustom;
import com.lanware.ehs.service.PostProtectArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/post/protectArticles")
public class ProtectArticlesController {


    private PostProtectArticlesService postProtectArticlesService;

    private ProtectArticlesService protectArticlesService;

    @Autowired
    public ProtectArticlesController( PostProtectArticlesService postProtectArticlesService, ProtectArticlesService protectArticlesService){
        this.postProtectArticlesService = postProtectArticlesService;
        this.protectArticlesService = protectArticlesService;
    }

    /**
     * 防护用具页面
     * @param postId 岗位id
     * @return 防护用具model
     */
    @RequestMapping("")
    public ModelAndView protectArticles(Long postId){
        JSONObject data = new JSONObject();
        data.put("postId", postId);
        return new ModelAndView("post/protectArticles/list", data);
    }

    /**
     * 查询防护用具集合
     * @param postId 岗位id
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public ResultFormat getList(@RequestParam Long postId, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("postId", postId);
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<PostProtectArticlesCustom> page = protectArticlesService.queryPostProtectArticlesByCondition(0, 0, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }


    /**
     * 删除防护用具
     * @param idsJSON 防护用具id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultFormat delete(@RequestParam String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);

        try {
            int num = postProtectArticlesService.deleteProtect(ids);
            if (num == 0){
                return ResultFormat.error("删除防护用具失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultFormat.error("删除防护用具失败");
        }
        return ResultFormat.success();
    }

    /**
     * 保存防护用具
     * @param postProtectArticlesListJSON 防护用具关联对象集合
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultFormat save(String postProtectArticlesListJSON,@CurrentUser EnterpriseUser enterpriseUser){
        List<PostProtectArticles> postProtectArticlesList = JSONArray.parseArray(postProtectArticlesListJSON, PostProtectArticles.class);
        if (postProtectArticlesList == null || postProtectArticlesList.isEmpty()){
            return ResultFormat.error("选择防护用具失败");
        }
        Date createTime = new Date();
        for (PostProtectArticles postProtectArticles : postProtectArticlesList){
            postProtectArticles.setGmtCreate(createTime);
        }
        int num = postProtectArticlesService.insertBatch(postProtectArticlesList,enterpriseUser.getEnterpriseId());
        if (num == 0){
            return ResultFormat.error("选择防护用具失败");
        }
        return ResultFormat.success();
    }

    /**
     * 防护用具选择页面
     * @return 防护用具选择model
     */
    @RequestMapping("/select")
    public ModelAndView select(){
        return new ModelAndView("post/protectArticles/select");
    }

    /**
     * 查询未选择的防护用具集合
     * @param postId 岗位id
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getUnselectedProtectArticlesList")
    @ResponseBody
    public ResultFormat getUnselectedProtectArticlesList(@RequestParam Long postId, Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("postId", postId);
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<PostProtectArticlesCustom> page = protectArticlesService.queryUnSelectedPostProtectArticlesByCondition(pageNum, pageSize, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }

}
