package com.lanware.ehs.enterprise.protect.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.protect.service.ProtectService;
import com.lanware.ehs.enterprise.protect.service.ProvideProtectArticlesService;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProvideProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

/**
 * 防护用品管理视图
 */
@Controller("protectView")
public class ViewController {
    /** 注入enterpriseBaseinfoService的bean */
    @Autowired
    private ProtectService protectService;

    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private ProvideProtectArticlesService provideProtectArticlesService;
    /**
     * @description 防护用品管理
     * @param enterpriseUser 当前登录企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @GetMapping(Constants.VIEW_PREFIX + "protect")
    public String protect( @CurrentUser EnterpriseUser enterpriseUser) {
        return "protect/protect";
    }

    /**
     * 进入防护用品新建页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protect/add")
    public String protectAdd( @CurrentUser EnterpriseUserCustom currentUser) {
        return "protect/protectAdd";
    }

    /**
     * 进入防护用品修改页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protect/update/{id}")
    public String protectUpdate(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseProtectArticlesCustom enterpriseProtectArticlesCustom=protectService.getEnterpriseProtectArticlesCustomByID(id);
        model.addAttribute("protectArticles",enterpriseProtectArticlesCustom);
        return "protect/protectAdd";
    }
    /**
     * 进入防护用品查看页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protect/viewProtect/{id}")
    public String viewProtect(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseProtectArticlesCustom enterpriseProtectArticlesCustom=protectService.getEnterpriseProtectArticlesCustomByID(id);
        model.addAttribute("protectArticles",enterpriseProtectArticlesCustom);
        return "protect/protectView";
    }



    /**
     * @description 防护用品发放
     * @param enterpriseUser 当前登录企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @GetMapping(Constants.VIEW_PREFIX + "protectArticles")
    public ModelAndView provideProtectArticles( @CurrentUser EnterpriseUser enterpriseUser) {

        return new ModelAndView("protect/provide/protectArticles");
    }
    /**
     * 进入防护用品发放新建页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protectArticles/provide/add")
    public String protectArticles( Model model, @CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseProvideProtectArticlesCustom enterpriseProtectArticlesCustom=new EnterpriseProvideProtectArticlesCustom();
        enterpriseProtectArticlesCustom.setProvideDate(new Date());
        model.addAttribute("protectArticles",enterpriseProtectArticlesCustom);
        return "protect/provide/provideAdd";
    }
    /**
     * 进入防护用品发放新建页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protectArticles/provide/addAll")
    public String protectAllArticles( Model model, @CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseProvideProtectArticlesCustom enterpriseProtectArticlesCustom=new EnterpriseProvideProtectArticlesCustom();
        enterpriseProtectArticlesCustom.setProvideDate(new Date());
        model.addAttribute("protectArticles",enterpriseProtectArticlesCustom);
        return "protect/provide/provideAll";
    }

    /**
     * 进入防护用品发放修改页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protectArticles/provide/update/{id}")
    public String protectArticlesUpdate(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseProvideProtectArticlesCustom enterpriseProtectArticlesCustom=provideProtectArticlesService.getEnterpriseProvideProtectArticlesCustomByID(id);
        model.addAttribute("protectArticles",enterpriseProtectArticlesCustom);
        return "protect/provide/provideAdd";
    }
    /**
     * 从待发放页面进入防护用品发放修改页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protectArticles/waitProvide/{id}")
    public String waitProtectArticlesUpdate(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        //通过待发放ID获取发放物品信息
        EnterpriseProvideProtectArticlesCustom enterpriseProtectArticlesCustom=provideProtectArticlesService.getEnterpriseProvideProtectArticlesCustomByWaitID(id);
        model.addAttribute("protectArticles",enterpriseProtectArticlesCustom);
        return "protect/provide/waitProvideAdd";
    }
    /**
     * 从待发放页面进入防护用品发放修改页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protectArticles/delWaitProvide/{id}")
    public String delProvideProtect(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        model.addAttribute("id",id);
        return "protect/provide/delWaitProvide";
    }
    /**
     * 进入防护用品发放查看页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"protectArticles/provide/viewProtect/{id}")
    public String viewprotectArticles(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseProvideProtectArticlesCustom enterpriseProtectArticlesCustom=provideProtectArticlesService.getEnterpriseProvideProtectArticlesCustomByID(id);
        model.addAttribute("protectArticles",enterpriseProtectArticlesCustom);
        return "protect/provide/provideView";
    }

}
