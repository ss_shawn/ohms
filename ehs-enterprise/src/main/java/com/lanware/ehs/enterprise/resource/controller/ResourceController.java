package com.lanware.ehs.enterprise.resource.controller;

import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.resource.entity.MenuTree;
import com.lanware.ehs.enterprise.resource.service.ResourceService;
import com.lanware.ehs.enterprise.user.service.UserService;
import com.lanware.ehs.pojo.EnterpriseResource;
import com.lanware.ehs.pojo.custom.EnterpriseResourceCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

/**
 * 菜单
 */
@Slf4j
@RestController
@RequestMapping("menu")
public class ResourceController extends BaseController {
    @Autowired
    private ResourceService resourceService;

    @Autowired
    private UserService userService;

    @GetMapping("tree")
    public EhsResult getMenuTree(EnterpriseResourceCustom menu) throws EhsException {
        try {
            MenuTree<EnterpriseResourceCustom> menus = this.resourceService.findMenus(menu);
            return EhsResult.ok(menus.getChilds());
        } catch (Exception e) {
            String message = "获取菜单树失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }


    @GetMapping("{username}")
    public EhsResult getUserMenus(@NotBlank(message = "{required}") @PathVariable String username) throws EhsException {
        EnterpriseUserCustom userCustom = (EnterpriseUserCustom)getSubject().getPrincipal();
        String currentUserName = userCustom.getUsername();
        if (!StringUtils.equalsIgnoreCase(username, currentUserName))
            throw new EhsException("您无权获取别人的菜单");

        userCustom = userService.findByName(currentUserName);
        MenuTree<EnterpriseResourceCustom> userMenus = null;
        if(userCustom.getIsAdmin() == EnterpriseUserCustom.ADMIN_YES){
            userMenus = this.resourceService.findUserMenus("");
        }else{
            userMenus = this.resourceService.findUserMenus(username);
        }

        return EhsResult.ok(userMenus);
    }
}
