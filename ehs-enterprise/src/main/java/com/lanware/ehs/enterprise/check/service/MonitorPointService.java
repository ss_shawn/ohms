package com.lanware.ehs.enterprise.check.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseMonitorPoint;

import java.util.Map;

/**
 * @description 监测点关联interface
 */
public interface MonitorPointService {

    JSONObject listMonitorPoints(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    int insertMonitorPoint(EnterpriseMonitorPoint enterpriseMonitorPoint);

    int updateMonitorPoint(EnterpriseMonitorPoint enterpriseMonitorPoint);

    EnterpriseMonitorPoint getMonitorPointById(Long id);

    int deleteMonitorPoint(Long id);

    int deleteMonitorPoints(Long[] ids);
}
