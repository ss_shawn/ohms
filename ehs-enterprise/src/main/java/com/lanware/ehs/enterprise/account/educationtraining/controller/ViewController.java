package com.lanware.ehs.enterprise.account.educationtraining.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 职业健康宣传教育培训台帐视图
 */
@Controller("educationTrainingAccountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @RequestMapping("educationTraining/trainingFileDetail/{relationId}/{relationModule}")
    /**
     * @description 返回其他培训证明资料页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView trainingFileDetail(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/educationTraining/trainingFileDetail", data);
    }

    /**
     * @description 返回机构相关证书详细页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("educationTraining/institutionCertificateDetail/{relationId}/{relationModule}")
    public ModelAndView institutionCertificateDetail(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/educationTraining/institutionCertificateDetail", data);
    }

    @RequestMapping("educationTraining/trainingEmployeeDetail/{trainingId}")
    /**
     * 返回参加培训的员工页面
     * @param trainingId 培训id
     * @return 参加培训的员工model
     */
    public ModelAndView trainingEmployeeDetail(@PathVariable Long trainingId){
        JSONObject data = new JSONObject();
        data.put("trainingId", trainingId);
        return new ModelAndView("account/educationTraining/trainingEmployeeDetail", data);
    }

    @RequestMapping("educationTraining/trainingAdminDetail/{trainingId}")
    /**
     * 返回参加培训的管理员页面
     * @param trainingId 培训id
     * @return 参加培训的员工model
     */
    public ModelAndView trainingAdminDetail(@PathVariable Long trainingId){
        JSONObject data = new JSONObject();
        data.put("trainingId", trainingId);
        return new ModelAndView("account/educationTraining/trainingAdminDetail", data);
    }
}
