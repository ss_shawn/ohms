package com.lanware.ehs.enterprise.protectivemeasure.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.protectivemeasure.service.ProtectiveMeasureService;
import com.lanware.ehs.mapper.EnterpriseWorkplaceMapper;
import com.lanware.ehs.mapper.custom.EnterpriseProtectiveMeasuresCustomMapper;
import com.lanware.ehs.pojo.EnterpriseProtectiveMeasures;
import com.lanware.ehs.pojo.EnterpriseProtectiveMeasuresExample;
import com.lanware.ehs.pojo.EnterpriseWorkplace;
import com.lanware.ehs.pojo.EnterpriseWorkplaceExample;
import com.lanware.ehs.pojo.custom.EnterpriseProtectiveMeasuresCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 防护措施interface实现类
 */
@Service
public class ProtectiveMeasureServiceImpl implements ProtectiveMeasureService {
    /** 注入enterpriseProtectiveMeasuresCustomMapper的bean */
    @Autowired
    private EnterpriseProtectiveMeasuresCustomMapper enterpriseProtectiveMeasuresCustomMapper;
    /** 注入enterpriseWorkplaceMapper的bean */
    @Autowired
    private EnterpriseWorkplaceMapper enterpriseWorkplaceMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    /**
     * @description 查询防护措施list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseProtectiveMeasures和totalNum的result
     */
    @Override
    public JSONObject listProtectiveMeasures(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseProtectiveMeasuresCustom> enterpriseProtectiveMeasures
                = enterpriseProtectiveMeasuresCustomMapper.listProtectiveMeasures(condition);
        result.put("enterpriseProtectiveMeasures", enterpriseProtectiveMeasures);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseProtectiveMeasuresCustom> pageInfo
                    = new PageInfo<EnterpriseProtectiveMeasuresCustom>(enterpriseProtectiveMeasures);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 根据企业id获取工作场所result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含baseHarmFactors的result
     */
    public JSONObject listWorkplacesByEnterpriseId(Long enterpriseId) {
        JSONObject result = new JSONObject();
        EnterpriseWorkplaceExample enterpriseWorkplaceExample = new EnterpriseWorkplaceExample();
        EnterpriseWorkplaceExample.Criteria criteria = enterpriseWorkplaceExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        List<EnterpriseWorkplace> enterpriseWorkplaces
                = enterpriseWorkplaceMapper.selectByExample(enterpriseWorkplaceExample);
        result.put("enterpriseWorkplaces", enterpriseWorkplaces);
        return result;
    }

    /**
     * @description 根据id查询防护措施
     * @param id 防护措施id
     * @return com.lanware.management.pojo.EnterpriseProtectiveMeasures 返回EnterpriseProtectiveMeasures对象
     */
    @Override
    public EnterpriseProtectiveMeasures getProtectiveMeasureById(Long id) {
        return enterpriseProtectiveMeasuresCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增防护措施
     * @param enterpriseProtectiveMeasure 防护措施pojo
      * @param operationRuleFile 操作规程
     * @param qualificationRecordFile 验收记录
     * @param acceptanceCertificateFile 合格证书
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertProtectiveMeasure(EnterpriseProtectiveMeasures enterpriseProtectiveMeasure
            , MultipartFile operationRuleFile, MultipartFile qualificationRecordFile
            , MultipartFile acceptanceCertificateFile) {
        // 新增数据先保存，以便获取id
        enterpriseProtectiveMeasuresCustomMapper.insert(enterpriseProtectiveMeasure);
        // 上传并保存操作规程
        if (operationRuleFile != null){
            String operationRule = "/" + enterpriseProtectiveMeasure.getEnterpriseId()
                    + "/" + ModuleName.PROTECTIVEMEASURE.getValue() + "/" + enterpriseProtectiveMeasure.getId()
                    + "/" + operationRuleFile.getOriginalFilename();
            try {
                ossTools.uploadStream(operationRule, operationRuleFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传操作规程失败", e);
            }
            enterpriseProtectiveMeasure.setOperationRule(operationRule);
        }
        // 上传并保存验收记录
        if (qualificationRecordFile != null){
            String qualificationRecord = "/" + enterpriseProtectiveMeasure.getEnterpriseId()
                    + "/" + ModuleName.PROTECTIVEMEASURE.getValue() + "/" + enterpriseProtectiveMeasure.getId()
                    + "/" + qualificationRecordFile.getOriginalFilename();
            try {
                ossTools.uploadStream(qualificationRecord, qualificationRecordFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传验收记录失败", e);
            }
            enterpriseProtectiveMeasure.setQualificationRecord(qualificationRecord);
        }
        // 上传并保存合格证书
        if (acceptanceCertificateFile != null){
            String acceptanceCertificate = "/" + enterpriseProtectiveMeasure.getEnterpriseId()
                    + "/" + ModuleName.PROTECTIVEMEASURE.getValue() + "/" + enterpriseProtectiveMeasure.getId()
                    + "/" + acceptanceCertificateFile.getOriginalFilename();
            try {
                ossTools.uploadStream(acceptanceCertificate, acceptanceCertificateFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传合格证书失败", e);
            }
            enterpriseProtectiveMeasure.setAcceptanceCertificate(acceptanceCertificate);
        }
        return enterpriseProtectiveMeasuresCustomMapper.updateByPrimaryKeySelective(enterpriseProtectiveMeasure);
    }

    /**
     * @description 更新防护措施
     * @param enterpriseProtectiveMeasure 防护措施pojo
      * @param operationRuleFile 操作规程
     * @param qualificationRecordFile 验收记录
     * @param acceptanceCertificateFile 合格证书
     * @return int 更新成功的数目
     */
    @Override
    public int updateProtectiveMeasure(EnterpriseProtectiveMeasures enterpriseProtectiveMeasure
            , MultipartFile operationRuleFile, MultipartFile qualificationRecordFile
            , MultipartFile acceptanceCertificateFile) {
        if (operationRuleFile != null) {
            // 上传了新的操作规程
            if (!StringUtils.isEmpty(enterpriseProtectiveMeasure.getOperationRule())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseProtectiveMeasure.getOperationRule());
            }
            String operationRule = "/" + enterpriseProtectiveMeasure.getEnterpriseId()
                    + "/" + ModuleName.PROTECTIVEMEASURE.getValue() + "/" + enterpriseProtectiveMeasure.getId()
                    + "/" + operationRuleFile.getOriginalFilename();
            try {
                ossTools.uploadStream(operationRule, operationRuleFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传操作规程失败", e);
            }
            enterpriseProtectiveMeasure.setOperationRule(operationRule);
        }
        if (qualificationRecordFile != null) {
            // 上传了新的验收记录
            if (!StringUtils.isEmpty(enterpriseProtectiveMeasure.getQualificationRecord())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseProtectiveMeasure.getQualificationRecord());
            }
            String qualificationRecord = "/" + enterpriseProtectiveMeasure.getEnterpriseId()
                    + "/" + ModuleName.PROTECTIVEMEASURE.getValue() + "/" + enterpriseProtectiveMeasure.getId()
                    + "/" + qualificationRecordFile.getOriginalFilename();
            try {
                ossTools.uploadStream(qualificationRecord, qualificationRecordFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传验收记录失败", e);
            }
            enterpriseProtectiveMeasure.setQualificationRecord(qualificationRecord);
        }
        if (acceptanceCertificateFile != null) {
            // 上传了新的合格证书
            if (!StringUtils.isEmpty(enterpriseProtectiveMeasure.getAcceptanceCertificate())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseProtectiveMeasure.getAcceptanceCertificate());
            }
            String acceptanceCertificate = "/" + enterpriseProtectiveMeasure.getEnterpriseId()
                    + "/" + ModuleName.PROTECTIVEMEASURE.getValue() + "/" + enterpriseProtectiveMeasure.getId()
                    + "/" + acceptanceCertificateFile.getOriginalFilename();
            try {
                ossTools.uploadStream(acceptanceCertificate, acceptanceCertificateFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传合格证书失败", e);
            }
            enterpriseProtectiveMeasure.setAcceptanceCertificate(acceptanceCertificate);
        }
        return enterpriseProtectiveMeasuresCustomMapper.updateByPrimaryKeySelective(enterpriseProtectiveMeasure);
    }

    /**
     * @description 删除防护措施
     * @param id 要删除的id
     * @param operationRule 要删除的操作规程
     * @param qualificationRecord 要删除的验收记录
     * @param acceptanceCertificate 要删除的合格证书
     * @return int 删除成功的数目
     */
    @Override
    public int deleteProtectiveMeasure(Long id, String operationRule
            , String qualificationRecord, String acceptanceCertificate) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(operationRule);
        ossTools.deleteOSS(qualificationRecord);
        ossTools.deleteOSS(acceptanceCertificate);
        // 删除数据库中对应的数据
        return enterpriseProtectiveMeasuresCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除防护措施
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteProtectiveMeasures(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseProtectiveMeasures enterpriseProtectiveMeasures
                    = enterpriseProtectiveMeasuresCustomMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseProtectiveMeasures.getOperationRule());
            ossTools.deleteOSS(enterpriseProtectiveMeasures.getQualificationRecord());
            ossTools.deleteOSS(enterpriseProtectiveMeasures.getAcceptanceCertificate());
        }
        // 删除数据库中对应的数据
        EnterpriseProtectiveMeasuresExample enterpriseProtectiveMeasuresExample
                = new EnterpriseProtectiveMeasuresExample();
        EnterpriseProtectiveMeasuresExample.Criteria criteria = enterpriseProtectiveMeasuresExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseProtectiveMeasuresCustomMapper.deleteByExample(enterpriseProtectiveMeasuresExample);
    }
}
