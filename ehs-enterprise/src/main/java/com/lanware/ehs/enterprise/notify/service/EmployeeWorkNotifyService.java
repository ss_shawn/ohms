package com.lanware.ehs.enterprise.notify.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.pojo.EmployeeWorkNotify;
import com.lanware.ehs.pojo.EmployeeWorkWaitNotify;
import com.lanware.ehs.pojo.custom.EmployeeWorkNotifyCustom;
import com.lanware.ehs.pojo.custom.EmployeeWorkWaitNotifyCustom;
import com.lanware.ehs.pojo.custom.EmployeeWorkWaitNotifyCustom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface EmployeeWorkNotifyService {
    /**
     * @description 根据id查询待岗前告知
     * @param id 待岗前告知id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EmployeeWorkWaitNotify对象
     */
     EmployeeWorkWaitNotify getEmployeeWorkWaitNotifyById(Long id);
    /**
     * @description 根据id查询待岗前告知
     * @param condition 待岗前告知条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EmployeeWorkWaitNotify对象
     */
     Page<EmployeeWorkWaitNotifyCustom> findEmployeeWorkWaitNotifyList(Map<String, Object> condition, QueryRequest request);
    /**
     * 保存岗前告知,
     * @param employeeWorkNotifyCustom
     * @return成功与否 1成功 0不成功
     */
    int saveEmployeeWorkNotify(EmployeeWorkNotifyCustom employeeWorkNotifyCustom, MultipartFile notifyFile);
    /**
     * 通过id获取岗前告知
     * @param id 岗前告知id
     * @return岗前告知
     */
     EmployeeWorkNotifyCustom getEmployeeWorkNotifyCustomByID(long id);
    /**
     * 删除待岗前告知,
     * @param id
     * @return成功与否 1成功 0不成功
     */
     int deleteEmployeeWorkNotify(long id);
    /**
     * 通过待岗前告知ID获取待岗前告知的人员和岗位信息,放入岗前告知返回
     * @param id 待岗前告知ID
     * @return 岗前告知
     */
     EmployeeWorkNotifyCustom getEmployeeWorkNotifyCustomByWaitID(long id);
    /**
     * @description 根据条件查询岗前告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    Page<EmployeeWorkNotifyCustom> findEmployeeWorkNotifyList(Map<String, Object> condition, QueryRequest request);


    /**
     * 查找岗前待告知信息
     * @param condition
     * @return
     */
    List<EmployeeWorkWaitNotifyCustom> findWorkWaitNofity(Map<String, Object> condition);
}
