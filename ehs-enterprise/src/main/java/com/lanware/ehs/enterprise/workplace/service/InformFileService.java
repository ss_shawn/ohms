package com.lanware.ehs.enterprise.workplace.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 高毒物品告知卡interface
 */
public interface InformFileService {
    /**
     * @description 查询高毒物品告知卡list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    JSONObject listInformFiles(Long relationId, String relationModule);

    /**
     * @description 根据id查询高毒物品告知卡
     * @param id
     * @return EnterpriseRelationFile
     */
    EnterpriseRelationFile getInformFileById(Long id);

    /**
     * @description 新增高毒物品告知卡，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param informFile 高毒物品告知卡文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertInformFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile informFile, Long enterpriseId);

    /**
     * @description 更新高毒物品告知卡，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param informFile 高毒物品告知卡文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateInformFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile informFile, Long enterpriseId);

    /**
     * @description 删除高毒物品告知卡
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteInformFile(Long id, String filePath);

    /**
     * @description 批量删除高毒物品告知卡
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteInformFiles(Long[] ids);
}
