package com.lanware.ehs.enterprise.threesimultaneity.service;

import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;

import java.util.List;

/**
 * @description 三同时管理预评价阶段interface
 */
public interface PreEvaluationService {
    /**
     * @description 根据项目id和阶段查询预评价阶段list
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile> 三同时管理文件list
     */
    List<EnterpriseThreeSimultaneityFile> listPreEvaluations(Long projectId, Integer projectState);
}
