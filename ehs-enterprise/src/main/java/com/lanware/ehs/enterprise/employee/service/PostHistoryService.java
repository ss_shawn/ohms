package com.lanware.ehs.enterprise.employee.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.custom.EmployeePostCustom;

import java.util.List;
import java.util.Map;

public interface PostHistoryService {

    /**
     * 分页查询历史岗位
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<EmployeePostCustom> queryEmployeePostByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 根据id查询历史岗位
     * @param id id
     * @return 查询结果
     */
    EmployeePostCustom queryEmployeePostById(Long id);

    /**
     * 根据ID删除岗位历史记录
     * @param ids id集合
     * @return 是否成功
     */
    int deleteEmployeePostById( List<Long> ids);
}
