package com.lanware.ehs.enterprise.consult.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.consult.service.ConsultService;
import com.lanware.ehs.pojo.SysConsult;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 咨询提问视图
 */
@Controller("consultView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入consultService的bean */
    @Autowired
    private ConsultService consultService;

    @RequestMapping("consult/editConsult")
    /**
     * @description 新增咨询提问
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addConsult() {
        JSONObject data = new JSONObject();
        SysConsult sysConsult = new SysConsult();
        data.put("sysConsult", sysConsult);
        return new ModelAndView("consult/edit", data);
    }

    /**
     * @description 编辑咨询提问
     * @param id 咨询提问id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("consult/editConsult/{id}")
    public ModelAndView editConsult(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        SysConsult sysConsult = consultService.getConsultById(id);
        data.put("sysConsult", sysConsult);
        return new ModelAndView("consult/edit", data);
    }

    /**
     * @description 查看咨询回复内容
     * @param id 咨询提问id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("/consult/viewConsult/{id}")
    public ModelAndView viewConsult(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        SysConsult sysConsult = consultService.getConsultById(id);
        data.put("sysConsult", sysConsult);
        return new ModelAndView("consult/view", data);
    }

    /**
     * @description 有新回复，跳转页面并更新回复状态
     * @param userCustom 当前企业
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("/consult/{newReplyNum}")
    public String newReply(@CurrentUser EnterpriseUserCustom userCustom) {
        consultService.updateAnswerStatusByEnterpriseId(userCustom.getEnterpriseId());
        return "consult/list";
    }
}
