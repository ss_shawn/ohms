package com.lanware.ehs.enterprise.employee.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.employee.service.PostHistoryService;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EmployeePostCustom;
import com.lanware.ehs.service.EmployeePostService;
import com.lanware.ehs.service.EnterprisePostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("employee/postHistory")
@Controller
public class PostHistoryController {

    private String basePath = "employee/postHistory/";

    private EmployeePostService employeePostService;

    private EnterprisePostService enterprisePostService;

    private PostHistoryService postHistoryService;

    @Autowired
    public PostHistoryController(EmployeePostService employeePostService, EnterprisePostService enterprisePostService, PostHistoryService postHistoryService) {
        this.employeePostService = employeePostService;
        this.enterprisePostService = enterprisePostService;
        this.postHistoryService = postHistoryService;
    }

    /**
     * 历史岗位列表页面
     * @return 历史岗位列表model
     */
    @RequestMapping("")
    public ModelAndView list(Long employeeId){
        JSONObject data = new JSONObject();
        data.put("employeeId", employeeId);
        return new ModelAndView(basePath + "list", data);
    }

    /**
     * 分页查询历史岗位
     * @param employeeId 人员id
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public EhsResult getList(Long employeeId, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("employeeId", employeeId);
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        condition.put("isLG", 1);
        Page<EmployeePostCustom> page = postHistoryService.queryEmployeePostByCondition(0, 0, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return EhsResult.ok(data);
    }

    /**
     * 编辑历史岗位页面
     * @param id 历史岗位id
     * @param postHistoryJSON 历史岗位JSON
     * @return 编辑历史岗位model
     */
    @RequestMapping("/edit")
    public ModelAndView edit(@CurrentUser EnterpriseUser enterpriseUser, Long id, String postHistoryJSON){
        JSONObject data = new JSONObject();
        EmployeePost employeePost = null;
        if (id != null){
            try {
                employeePost = employeePostService.selectByPrimaryKey(id);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }else {
            employeePost = JSONObject.parseObject(postHistoryJSON, EmployeePost.class);
        }
        data.put("employeePost", employeePost);
        EnterprisePostExample enterprisePostExample = new EnterprisePostExample();
        EnterprisePostExample.Criteria criteria = enterprisePostExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseUser.getEnterpriseId());
        try {
            List<EnterprisePost> enterprisePosts = enterprisePostService.selectByExample(enterprisePostExample);
            data.put("enterprisePosts", enterprisePosts);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return new ModelAndView(basePath + "edit", data);
    }

    /**
     * 保存历史岗位
     * @param employeePostJSON 历史岗位JSON
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(String employeePostJSON){
        EmployeePost employeePost = JSONObject.parseObject(employeePostJSON, EmployeePost.class);
        if (employeePost.getId() == null){
            employeePost.setGmtCreate(new Date());
            employeePost.setStatus(1);
            try {
                employeePostService.insert(employeePost);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存历史岗位失败");
            }
        }else {
            employeePost.setGmtModified(new Date());
            try {
                employeePostService.updateByPrimaryKeySelective(employeePost);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存历史岗位失败");
            }
        }
        return EhsResult.ok();
    }

    /**
     * 删除历史岗位
     * @param idsJSON id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public EhsResult delete(String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);

        try {
            int n=postHistoryService.deleteEmployeePostById(ids);
            if(n==-8){
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "删除历史岗位失败,有人员岗位有防护用品发放信息，不能删除！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), " 删除历史岗位失败");
        }
        return EhsResult.ok();
    }

    /**
     * 查看历史岗位页面
     * @param id 历史岗位id
     * @return 查看历史岗位model
     */
    @RequestMapping("/view")
    public ModelAndView view(@RequestParam Long id){
        JSONObject data = new JSONObject();
        EmployeePost employeePost = postHistoryService.queryEmployeePostById(id);
        data.put("employeePost", employeePost);
        return new ModelAndView(basePath + "view", data);
    }
}
