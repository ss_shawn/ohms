package com.lanware.ehs.enterprise.check.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseInsideMonitor;

import java.util.Map;

/**
 * @description 企业内部监测interface
 */
public interface InsideMonitorService {
    /**
     * @description 查询日常监测list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseOutsideChecks和totalNum的result
     */
    JSONObject listInsideMonitors(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据企业id获取监测点result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseMonitorPoints的result
     */
    JSONObject listMonitorPointsByEnterpriseId(Long enterpriseId);

    /**
     * @description 根据监测点id获取作业场所result
     * @param monitorPointId 监测点id
     * @return com.alibaba.fastjson.JSONObject 返回包含monitorWorkplaces的result
     */
    JSONObject listWorkplacesByMonitorId(Long monitorPointId);

    /**
     * @description 根据监测点id和工作场所id获取危害因素result
     * @param monitorPointId 监测点id
     * @param workplaceId 工作场所id
     * @return com.alibaba.fastjson.JSONObject 返回包含monitorWorkplaceHarmFactors的result
     */
    JSONObject listHarmFactorsByMonitorWorkplaceId(Long monitorPointId, Long workplaceId);

    /**
     * @description 根据企业id获取工作场所result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces的result
     */
    JSONObject listWorkplacesByEnterpriseId(Long enterpriseId);

    /**
     * @description 获取危害因素result
     * @param workplaceId 工作场所id
     * @return com.alibaba.fastjson.JSONObject 返回包含workplaceHarmFacotrCustoms的result
     */
    JSONObject listHarmfactorsByWorkplaceId(Long workplaceId);

    /**
     * @description 根据id查询日常监测信息
     * @param id 日常监测id
     * @return com.lanware.management.pojo.EnterpriseInsideMonitor 返回EnterpriseInsideMonitor对象
     */
    EnterpriseInsideMonitor getInsideMonitorById(Long id);

    /**
     * @description 插入日常监测信息
     * @param enterpriseInsideMonitor 日常监测pojo
     * @return 插入成功的数目
     */
    int insertInsideMonitor(EnterpriseInsideMonitor enterpriseInsideMonitor);

    /**
     * @description 更新日常监测信息
     * @param enterpriseInsideMonitor 日常监测pojo
     * @return 更新成功的数目
     */
    int updateInsideMonitor(EnterpriseInsideMonitor enterpriseInsideMonitor);

    /**
     * @description 删除日常监测信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteInsideMonitor(Long id);

    /**
     * @description 批量删除日常监测信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteInsideMonitors(Long[] ids);
}
