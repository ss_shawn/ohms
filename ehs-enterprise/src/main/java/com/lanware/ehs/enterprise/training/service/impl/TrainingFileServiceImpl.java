package com.lanware.ehs.enterprise.training.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.training.service.TrainingFileService;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @description 培训证明资料interface实现类
 */
@Service
public class TrainingFileServiceImpl implements TrainingFileService {
    /** 注入enterpriseRelationFileMapper的bean */
    @Autowired
    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    /**
     * @description 查询培训证明资料list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含workHarmFactors的result
     */
    @Override
    public JSONObject listTrainingFiles(Long relationId, String relationModule) {
        JSONObject result = new JSONObject();
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria = enterpriseRelationFileExample.createCriteria();
        criteria.andRelationIdEqualTo(relationId);
        criteria.andRelationModuleEqualTo(relationModule);
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        result.put("enterpriseRelationFiles", enterpriseRelationFiles);
        return result;
    }

    /**
     * @description 根据id查询培训证明资料
     * @param id
     * @return 返回EnterpriseRelationFile
     */
    @Override
    public EnterpriseRelationFile getTrainingFileById(Long id) {
        return enterpriseRelationFileMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增培训证明资料，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param trainingFile 培训证明资料
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    @Transactional
    public int insertTrainingFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile trainingFile, Long enterpriseId) {
        // 新增数据先保存，以便获取id
        enterpriseRelationFileMapper.insert(enterpriseRelationFile);
        // 上传并保存培训证明资料
        if (trainingFile != null){
            String training = "/" + enterpriseId + "/" + ModuleName.TRAINING.getValue()
                    + "/" + enterpriseRelationFile.getRelationId() + "/" + enterpriseRelationFile.getId()
                    + "/" + trainingFile.getOriginalFilename();
            try {
                ossTools.uploadStream(training, trainingFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传培训证明资料失败", e);
            }
            enterpriseRelationFile.setFileName(trainingFile.getOriginalFilename());
            enterpriseRelationFile.setFilePath(training);
        }
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
}

    /**
     * @description 更新培训证明资料，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param trainingFile 培训证明资料
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    public int updateTrainingFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile trainingFile, Long enterpriseId) {
        // 上传并保存培训证明资料
        if (trainingFile != null) {
            // 上传了新的培训证明资料
            if (!StringUtils.isEmpty(enterpriseRelationFile.getFilePath())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
            }
            String training = "/" + enterpriseId + "/" + ModuleName.TRAINING.getValue()
                    + "/" + enterpriseRelationFile.getRelationId() + "/" + enterpriseRelationFile.getId()
                    + "/" + trainingFile.getOriginalFilename();
            try {
                ossTools.uploadStream(training, trainingFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传培训证明资料失败", e);
            }
            enterpriseRelationFile.setFileName(trainingFile.getOriginalFilename());
            enterpriseRelationFile.setFilePath(training);
        }
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
    }

    /**
     * @description 删除培训证明资料
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    @Override
    public int deleteTrainingFile(Long id, String filePath) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(filePath);
        // 删除数据库中对应的数据
        return enterpriseRelationFileMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除培训证明资料
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteTrainingFiles(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseRelationFile enterpriseRelationFile = enterpriseRelationFileMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria = enterpriseRelationFileExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
    }
}
