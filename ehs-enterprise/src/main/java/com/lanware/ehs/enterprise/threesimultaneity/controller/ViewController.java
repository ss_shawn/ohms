package com.lanware.ehs.enterprise.threesimultaneity.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.threesimultaneity.service.OtherFileService;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityFileService;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneity;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import com.lanware.ehs.pojo.custom.EnterpriseThreeSimultaneityCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 三同时管理视图
 */
@Controller("threeSimultaneityView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入threeSimultaneityService的bean */
    @Autowired
    private ThreeSimultaneityService threeSimultaneityService;
    /** 注入otherFileService的bean */
    @Autowired
    private OtherFileService otherFileService;
    /** 注入threeSimultaneityFileService的bean */
    @Autowired
    private ThreeSimultaneityFileService threeSimultaneityFileService;

    @RequestMapping("threeSimultaneity/editThreeSimultaneity")
    /**
     * @description 新增三同时
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addThreeSimultaneity() {
        JSONObject data = new JSONObject();
        EnterpriseThreeSimultaneity enterpriseThreeSimultaneity = new EnterpriseThreeSimultaneity();
        data.put("enterpriseThreeSimultaneity", enterpriseThreeSimultaneity);
        return new ModelAndView("threeSimultaneity/edit", data);
    }

    /**
     * @description 编辑三同时
     * @param id 三同时id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/editThreeSimultaneity/{id}")
    public ModelAndView editMedical(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseThreeSimultaneity enterpriseThreeSimultaneity = threeSimultaneityService.getThreeSimultaneityById(id);
        data.put("enterpriseThreeSimultaneity", enterpriseThreeSimultaneity);
        return new ModelAndView("threeSimultaneity/edit", data);
    }

    /**
     * @description 返回预评价阶段编辑页面
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/preEvaluation/{projectId}/{projectState}")
    public ModelAndView listPreEvaluations(@PathVariable Long projectId, @PathVariable Integer projectState) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        data.put("projectState", projectState);
        return new ModelAndView("threeSimultaneity/preEvaluation/list", data);
    }

    /**
     * @description 返回预评价阶段查看页面
     * @param projectId 项目id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/preEvaluation/{projectId}")
    public ModelAndView viewPreEvaluations(@PathVariable Long projectId) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        data.put("projectState", EnterpriseThreeSimultaneityCustom.STAGE_PRE);
        return new ModelAndView("threeSimultaneity/preEvaluation/view", data);
    }

    /**
     * @description 返回确认危害程度页面
     * @param projectId 项目id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/harmDegree/{projectId}")
    public ModelAndView harmDegree(@PathVariable Long projectId) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        return new ModelAndView("threeSimultaneity/preEvaluation/harmDegree", data);
    }

    /**
     * @description 返回防护设施设计阶段页面
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/protectiveDesign/{projectId}/{projectState}")
    public ModelAndView listProtectiveDesigns(@PathVariable Long projectId, @PathVariable Integer projectState) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        data.put("projectState", EnterpriseThreeSimultaneityCustom.STAGE_DESIGN);
        data.put("previousProjectState", projectState);
        return new ModelAndView("threeSimultaneity/protectiveDesign/list", data);
    }

    /**
     * @description 返回防护设施设计阶段查看页面
     * @param projectId 项目id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/protectiveDesign/{projectId}")
    public ModelAndView viewProtectiveDesigns(@PathVariable Long projectId) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        data.put("projectState", EnterpriseThreeSimultaneityCustom.STAGE_DESIGN);
        return new ModelAndView("threeSimultaneity/protectiveDesign/view", data);
    }

    /**
     * @description 返回竣工验收阶段页面
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/completionAcceptance/{harmDegree}/{projectId}/{projectState}")
    public ModelAndView listCompletionAcceptances(@PathVariable String harmDegree, @PathVariable Long projectId
            , @PathVariable Integer projectState) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        data.put("projectState", EnterpriseThreeSimultaneityCustom.STAGE_ACCEPTANCE);
        data.put("previousProjectState", projectState);
        // 根据危害程度返回不同的页面
        return new ModelAndView("threeSimultaneity/completionAcceptance/" + harmDegree + "/list", data);
    }

    /**
     * @description 返回竣工验收阶段查看页面
     * @param projectId 项目id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/completionAcceptance/{harmDegree}/{projectId}")
    public ModelAndView viewCompletionAcceptances(@PathVariable String harmDegree, @PathVariable Long projectId) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        data.put("projectState", EnterpriseThreeSimultaneityCustom.STAGE_ACCEPTANCE);
        // 根据危害程度返回不同的页面
        return new ModelAndView("threeSimultaneity/completionAcceptance/" + harmDegree + "/view", data);
    }

    @RequestMapping("threeSimultaneity/otherFile/{relationId}/{relationModule}/{fileType}")
    /**
     * @description 返回三同时其他文件页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @param fileType 文件类型
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView trainingFile(@PathVariable Long relationId, @PathVariable String relationModule
            , @PathVariable String fileType){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        data.put("fileType", fileType);
        return new ModelAndView("threeSimultaneity/otherFile/list", data);
    }

    @RequestMapping("threeSimultaneity/otherFile/addOtherFile/{relationId}/{relationModule}/{fileType}")
    /**
     * @description 新增三同时其他文件
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @param fileType 文件类型
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addOtherFile(@PathVariable Long relationId, @PathVariable String relationModule
            , @PathVariable String fileType) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(relationId);
        enterpriseRelationFile.setRelationModule(relationModule);
        enterpriseRelationFile.setFileType(fileType);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("threeSimultaneity/otherFile/edit", data);
    }

    @RequestMapping("threeSimultaneity/otherFile/editOtherFile/{id}")
    /**
     * @description 编辑三同时其他文件
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editOtherFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = otherFileService.getOtherFileById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("threeSimultaneity/otherFile/edit", data);
    }

    @RequestMapping("threeSimultaneity/otherFile/viewOtherFile/{relationId}/{relationModule}/{fileType}")
    /**
     * @description 返回三同时其他文件查看页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @param fileType 文件类型
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewOtherFile(@PathVariable Long relationId, @PathVariable String relationModule
            , @PathVariable String fileType){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        data.put("fileType", fileType);
        return new ModelAndView("threeSimultaneity/otherFile/view", data);
    }

    /**
     * @description 返回确认风险分类页面(危害程度严重)
     * @param projectId 项目id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/high/riskClass/{projectId}")
    public ModelAndView highRiskClass(@PathVariable Long projectId) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        return new ModelAndView("threeSimultaneity/completionAcceptance/high/riskClass", data);
    }

    /**
     * @description 返回确认风险分类页面(危害程度较重)
     * @param projectId 项目id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/middle/riskClass/{projectId}")
    public ModelAndView MiddleRiskClass(@PathVariable Long projectId) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        return new ModelAndView("threeSimultaneity/completionAcceptance/middle/riskClass", data);
    }

    /**
     * @description 返回确认风险分类页面(危害程度一般)
     * @param projectId 项目id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("threeSimultaneity/low/riskClass/{projectId}")
    public ModelAndView lowRiskClass(@PathVariable Long projectId) {
        JSONObject data = new JSONObject();
        data.put("projectId", projectId);
        return new ModelAndView("threeSimultaneity/completionAcceptance/low/riskClass", data);
    }

    @RequestMapping("threeSimultaneityFile/addOpinion/{projectId}/{projectState}/{fileType}")
    /**
     * @description 新增意见文件
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @param fileType 文件类型
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addOpinion(@PathVariable Long projectId, @PathVariable Integer projectState
            , @PathVariable String fileType) {
        JSONObject data = new JSONObject();
        EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile = new EnterpriseThreeSimultaneityFile();
        enterpriseThreeSimultaneityFile.setProjectId(projectId);
        enterpriseThreeSimultaneityFile.setProjectState(projectState);
        enterpriseThreeSimultaneityFile.setFileType(fileType);
        data.put("enterpriseThreeSimultaneityFile", enterpriseThreeSimultaneityFile);
        String projectStateModule = "";
        if (projectState == 0) {
            projectStateModule = "preEvaluation";
        } else if (projectState == 1) {
            projectStateModule = "protectiveDesign";
        } else {
            projectStateModule = "completionAcceptance";
        }
        return new ModelAndView("threeSimultaneity/" + projectStateModule +  "/addOpinion", data);
    }

    @RequestMapping("threeSimultaneityFile/viewOpinion/{id}")
    /**
     * @description 查看意见
     * @param id 三同时文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewOpinion(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile
                = threeSimultaneityFileService.getThreeSimultaneityFileById(id);
        data.put("enterpriseThreeSimultaneityFile", enterpriseThreeSimultaneityFile);
        return new ModelAndView("threeSimultaneity/viewOpinion", data);
    }
}
