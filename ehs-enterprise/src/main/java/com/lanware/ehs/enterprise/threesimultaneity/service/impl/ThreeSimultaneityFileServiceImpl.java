package com.lanware.ehs.enterprise.threesimultaneity.service.impl;

import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityFileService;
import com.lanware.ehs.mapper.custom.EnterpriseThreeSimultaneityFileCustomMapper;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @description 三同时文件interface实现类
 */
@Service
public class ThreeSimultaneityFileServiceImpl implements ThreeSimultaneityFileService {
    /** 注入enterpriseThreeSimultaneityFileCustomMapper的bean */
    @Autowired
    private EnterpriseThreeSimultaneityFileCustomMapper enterpriseThreeSimultaneityFileCustomMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    /**
     * @description
     * @param enterpriseThreeSimultaneityFile 三同时文件对象
     * @param opinionFile 上传的意见文件
     * @param enterpriseId 企业id
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertOpinionFile(EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile
            , MultipartFile opinionFile, Long enterpriseId) {
        // 新增数据先保存，以便获取id
        enterpriseThreeSimultaneityFileCustomMapper.insert(enterpriseThreeSimultaneityFile);
        if (opinionFile != null){
            String threeSimultaneity = "/" + enterpriseId + "/" + ModuleName.THREESIMULTANEITY.getValue()
                    + "/" + enterpriseThreeSimultaneityFile.getProjectId()
                    + "/" + enterpriseThreeSimultaneityFile.getId() + "/" + opinionFile.getOriginalFilename();
            try {
                ossTools.uploadStream(threeSimultaneity, opinionFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传三同时意见文件失败", e);
            }
            enterpriseThreeSimultaneityFile.setFileName(opinionFile.getOriginalFilename());
            enterpriseThreeSimultaneityFile.setFilePath(threeSimultaneity);
        }
        return enterpriseThreeSimultaneityFileCustomMapper.updateByPrimaryKeySelective(enterpriseThreeSimultaneityFile);
    }

    /**
     * @description 根据id查询三同时文件信息
     * @param id 三同时文件id
     * @return com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile
     */
    @Override
    public EnterpriseThreeSimultaneityFile getThreeSimultaneityFileById(Long id) {
        return enterpriseThreeSimultaneityFileCustomMapper.selectByPrimaryKey(id);
    }
}
