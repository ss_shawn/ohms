package com.lanware.ehs.enterprise.training.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseTraining;
import com.lanware.ehs.pojo.EnterpriseTrainingEmployee;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @description 培训管理interface
 */
public interface TrainingService {
    /**
     * @description 查询培训管理list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    JSONObject listTrainings(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 获取人员result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseEmployees的result
     */
    JSONObject listEmployeesByEnterprieId(Long enterpriseId);

    /**
     * @description 根据id查询培训扩展信息
     * @param id 培训id
     * @return com.lanware.management.pojo.EnterpriseTrainingCustom 返回EnterpriseTrainingCustom对象
     */
    EnterpriseTrainingCustom getTrainingCustomById(Long id);

    /**
     * @description 新增培训信息
     * @param enterpriseTraining 培训pojo
     * @param attendanceSheetFile 签到表
     * @return int 插入成功的数目
     */
    int insertTrainingAndEmployees(EnterpriseTraining enterpriseTraining, MultipartFile attendanceSheetFile);

    /**
     * @description 更新培训信息
     * @param enterpriseTraining 培训pojo
     * @param attendanceSheetFile 签到表
     * @return int 更新成功的数目
     */
    int updateTrainingAndEmployees(EnterpriseTraining enterpriseTraining, MultipartFile attendanceSheetFile);

    /**
     * @description 删除员培训信息
     * @param id 要删除的id
     * @param attendanceSheet 要删除的签到表路径
     * @return int 删除成功的数目
     */
    int deleteTraining(Long id, String attendanceSheet);

    /**
     * @description 批量删除培训信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteTrainings(Long[] ids);

    /**
     * 统计培训总时长
     * @param parameter
     * @return
     */
    String getTrainTime(Map<String,Object> parameter);

    /**
     * @description 查询管理员培训信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listTraingAdmins(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 查询普通员工培训扩展信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listTraingEmployees(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据培训id查询管理员培训信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listTrainingAdminsByTrainingId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据培训id查询普通员工培训信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listTraingEmployeesByTrainingId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据培训id过滤尚未培训的管理员
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listSelectTrainingAdminsByTrainingId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据培训id过滤尚未培训的普通员工
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listSelectTrainingEmployeesByTrainingId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 批量插入参加培训的员工集合
     * @param enterpriseTrainingEmployeeList 参加培训的员工集合
     * @return int 插入成功的数目
     */
    int insertBatch(List<EnterpriseTrainingEmployee> enterpriseTrainingEmployeeList);

    /**
     * @description 删除参加培训的员工
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteTrainingEmployee(Long id);

    /**
     * @description 批量删除参加培训的员工
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteTrainingEmployees(Long[] ids);

    /**
     * @description 获取管理员年度培训报表
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getYearAdminTrainingData(Map<String, Object> condition);

    /**
     * @description 获取普通员工年度培训报表
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getYearEmployeeTrainingData(Map<String, Object> condition);

    /**
     * 获取年度每月培训人数
     * @param condition
     * @return
     */
    Map<String, Integer> getTrainNumByCondition(Map<String, Object> condition);
}