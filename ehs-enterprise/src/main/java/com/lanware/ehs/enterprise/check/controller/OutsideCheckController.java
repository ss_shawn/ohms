package com.lanware.ehs.enterprise.check.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.util.ExportExcelUtil;
import com.lanware.ehs.common.utils.FileUploadUtil;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.check.service.CheckResultService;
import com.lanware.ehs.enterprise.check.service.OutsideCheckService;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;
import com.lanware.ehs.pojo.custom.EnterpriseWaitCheckCustom;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

/**
 * @description 企业外部检测controller
 */
@Controller
@RequestMapping("/outsideCheck")
@Slf4j
public class OutsideCheckController {
    @Autowired
    /** 注入outsideCheckService的bean */
    private OutsideCheckService outsideCheckService;
    @Autowired
    /** 注入checkResultService的bean */
    private CheckResultService checkResultService;
    @Autowired
    /** 注入fileUploadUtil的bean */
    private FileUploadUtil fileUploadUtil;
    
    /**
     * @description 返回检测管理页面
     * @return java.lang.String 返回页面路径
     */
    @RequestMapping("")
    public String list(){
        return "check/list";
    }

    @RequestMapping("/listOutsideChecks")
    @ResponseBody
    /**
     * @description 获取危害因素检测result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listOutsideChecks(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = outsideCheckService.listOutsideChecks(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /** @description 新增危害因素检测信息，并上传文件到oss服务器
     * @param enterpriseOutsideCheckJSON 危害因素检测json
     * @param checkReportFile 检测报告书
     * @param waitCheckId 待检测项目id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/addOutsideCheck/save")
    @ResponseBody
    public ResultFormat save(String enterpriseOutsideCheckJSON, MultipartFile checkReportFile, Long waitCheckId) {
        EnterpriseOutsideCheck enterpriseOutsideCheck
                = JSONObject.parseObject(enterpriseOutsideCheckJSON, EnterpriseOutsideCheck.class);
        enterpriseOutsideCheck.setGmtCreate(new Date());
        // 待检测项目
        EnterpriseWaitCheck enterpriseWaitCheck = new EnterpriseWaitCheck();
        enterpriseWaitCheck.setId(waitCheckId);
        enterpriseWaitCheck.setStatus(EnterpriseWaitCheckCustom.STATUS_CHECK_YES);
        enterpriseWaitCheck.setGmtModified(new Date());
        if (outsideCheckService.insertOutsideCheck(enterpriseOutsideCheck, checkReportFile, enterpriseWaitCheck) > 0) {
            return ResultFormat.success("添加成功");
        } else {
            return ResultFormat.error("添加失败");
        }

    }

    @RequestMapping("/editOutsideCheck/save")
    @ResponseBody
    /** @description 保存危害因素检测信息，并上传文件到oss服务器
     * @param enterpriseOutsideCheckJSON 危害因素检测json
     * @param checkReportFile 检测报告书
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseOutsideCheckJSON, MultipartFile checkReportFile) {
        EnterpriseOutsideCheck enterpriseOutsideCheck
                = JSONObject.parseObject(enterpriseOutsideCheckJSON, EnterpriseOutsideCheck.class);
        enterpriseOutsideCheck.setGmtModified(new Date());
        if (outsideCheckService.updateOutsideCheck(enterpriseOutsideCheck, checkReportFile) > 0) {
            return ResultFormat.success("更新成功");
        } else {
            return ResultFormat.error("更新失败");
        }
    }

    @RequestMapping("/viewOutsideCheck")
    /**
     * @description 查看危害因素检测信息
     * @param id 危害因素检测id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewOutsideCheck(Long id) {
        JSONObject data = new JSONObject();
        EnterpriseOutsideCheck enterpriseOutsideCheck = outsideCheckService.getOutsideCheckById(id);
        data.put("enterpriseOutsideCheck", enterpriseOutsideCheck);
        return new ModelAndView("check/viewOutsideCheck", data);
    }

    @RequestMapping("/deleteOutsideCheck")
    @ResponseBody
    /**
     * @description 删除危害因素检测信息
     * @param id 要删除的id
     * @param checkReport 要删除的检测报告书路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteOutsideCheck(Long id, String checkReport) {
        try {
            outsideCheckService.deleteOutsideCheck(id, checkReport);
            return ResultFormat.success("删除成功");
        } catch (Exception e) {
            String message = "删除失败";
            log.error(message, e);
            return ResultFormat.error(message);
        }
    }

    @RequestMapping("/deleteOutsideChecks")
    @ResponseBody
    /**
     * @description 批量删除危害因素检测信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteOutsideChecks(Long[] ids) {
        try {
            outsideCheckService.deleteOutsideChecks(ids);
            return ResultFormat.success("删除成功");
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return ResultFormat.error("删除失败");
        }
    }

    /**
     * @description 导出危害因素检测结果excel模板
     * @param request 请求
     * @param response 响应
     * @param id 危害因素检测id
     * @param checkDate 危害因素检测日期
     * @param enterpriseUser 企业用户
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/exportExcel")
    @ResponseBody
    public void exportExcel(HttpServletRequest request, HttpServletResponse response, Long id, String checkDate
            , @CurrentUser EnterpriseUser enterpriseUser) {
        String export = "检测结果ID#id,危害因素#harmFactorName,作业场所#workplaceName,监测点#monitorPointName,"
                + "涉及岗位#postName,MAC#mac,PC-TWA#twa,PC-STEL#stel,超限倍数#overLimitMultiple,"
                + "接触时间(h)#contactTime,检测结果#checkResult,单位#unit";
        String[] excelHeader = export.split(",");
        // 字段数据集合
        List<EnterpriseOutsideCheckResultCustom> checkResultList = new ArrayList<EnterpriseOutsideCheckResultCustom>();
        // 获取危害因素检测下的危害因素id集合
        List<EnterpriseOutsideCheckResultCustom> checkHarmFactorList = checkResultService.getHarmFactorIdsByOutsideCheckId(id);
        for (EnterpriseOutsideCheckResultCustom checkHarmFactor : checkHarmFactorList) {
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("outsideCheckId", id);
            condition.put("harmFactorId", checkHarmFactor.getHarmFactorId());
            condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
            // 获取危害因素关联的检测结果集合
            List<EnterpriseOutsideCheckResultCustom> checkResultCustomList = checkResultService.listCheckResultCustoms(condition);
            for (EnterpriseOutsideCheckResultCustom checkResultCustom : checkResultCustomList) {
                checkResultCustom.setHarmFactorName(checkHarmFactor.getHarmFactorName());
                checkResultList.add(checkResultCustom);
            }
        }
        try {
            ExportExcelUtil.export(response,checkDate + "危害因素检测结果.xls", excelHeader, checkResultList);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }

    /** @description 导入危害因素检测结果excel
     * @param importFile 导入的excel文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/importExcel")
    @ResponseBody
    public ResultFormat importExcel(MultipartFile importFile) {
        // 创建临时文件
        File tempFile = null;
        try {
            // 通过上传的文件得到服务器上的临时文件
            tempFile = fileUploadUtil.fileupload(importFile);
            String fileName = tempFile.getName();
            InputStream is = new FileInputStream(tempFile);
            Workbook workbook = null;
            // 判断文件类型：Excel 2007，Excel 2003
            if (fileName.endsWith("xlsx")) {
                workbook = new XSSFWorkbook(is);
            } else if (fileName.endsWith("xls")) {
                workbook = new HSSFWorkbook(is);
            }
            // 判断导入excel是否有不匹配数据的flag
            boolean flag = false;
            // 创建检测结果详细list
            List<EnterpriseCheckResultDetail> checkResultDetailList = new ArrayList<EnterpriseCheckResultDetail>();
            Sheet sheet = workbook.getSheetAt(0);
            // 从sheet第三行开始读取数据
            for (int rowNum = 2; rowNum <= sheet.getLastRowNum(); rowNum++) {
                Row row = sheet.getRow(rowNum);
                if (row != null) {
                    Long checkResultId = null;
                    if (row.getCell(0) != null) {
                        row.getCell(0).setCellType(CellType.STRING);
                        checkResultId = Long.valueOf(row.getCell(0).getStringCellValue());
                    } else {
                        flag = true;
                        break;
                    }
                    BigDecimal contactTime = null;
                    if (row.getCell(9) != null) {
                        row.getCell(9).setCellType(CellType.STRING);
                        contactTime = new BigDecimal(row.getCell(9).getStringCellValue());
                    } else {
                        flag = true;
                        break;
                    }
                    BigDecimal checkResult = null;
                    if (row.getCell(10) != null) {
                        row.getCell(10).setCellType(CellType.STRING);
                        checkResult = new BigDecimal(row.getCell(10).getStringCellValue());
                    } else {
                        flag = true;
                        break;
                    }
                    String unit = "";
                    if (row.getCell(11) != null) {
                        row.getCell(11).setCellType(CellType.STRING);
                        unit = row.getCell(11).getStringCellValue();
                    } else {
                        flag = true;
                        break;
                    }
                    // 创建检测结果详细对象，把excel每行读取数据set进对象
                    EnterpriseCheckResultDetail enterpriseCheckResultDetail = new EnterpriseCheckResultDetail();
                    enterpriseCheckResultDetail.setCheckResultId(checkResultId);
                    enterpriseCheckResultDetail.setContactTime(contactTime);
                    enterpriseCheckResultDetail.setCheckResult(checkResult);
                    enterpriseCheckResultDetail.setUnit(unit);
                    enterpriseCheckResultDetail.setGmtCreate(new Date());
                    // 检测结果详细对象添加进检测结果详细list
                    checkResultDetailList.add(enterpriseCheckResultDetail);
                }
            }
            // 如果flag为true，直接返回"导入失败"
            if (flag) {
                return ResultFormat.error("导入失败，请检查是否有不匹配的数据");
            }
            // 批量插入检测结果详细
            checkResultService.insertCheckResultDetails(checkResultDetailList);
            workbook.close();
            return ResultFormat.success("导入成功");
        } catch (Exception e) {
            return ResultFormat.error("导入失败，请检查是否有不匹配的数据");
        } finally {
            // 删除临时文件
            if (tempFile != null && tempFile.exists()) {
                tempFile.delete();
            }
        }
    }
}
