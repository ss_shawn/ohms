package com.lanware.ehs.enterprise.check.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @description 监测、检测点示意图interface
 */
public interface SchematicDiagramService {
    /**
     * @description 查询监测、检测点示意图list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseRelationFiles和totalNum的result
     */
    JSONObject listSchematicDiagrams(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询监测、检测点示意图
     * @param id
     * @return EnterpriseRelationFile
     */
    EnterpriseRelationFile getSchematicDiagramById(Long id);

    /**
     * @description 新增监测、检测点示意图，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param schematicDiagram 监测、检测点示意图
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertSchematicDiagram(EnterpriseRelationFile enterpriseRelationFile, MultipartFile schematicDiagram);

    /**
     * @description 更新监测、检测点示意图，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param schematicDiagram 监测、检测点示意图
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateSchematicDiagram(EnterpriseRelationFile enterpriseRelationFile, MultipartFile schematicDiagram);

    /**
     * @description 删除监测、检测点示意图
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteSchematicDiagram(Long id, String filePath);

    /**
     * @description 批量删除监测、检测点示意图
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteSchematicDiagrams(Long[] ids);
}
