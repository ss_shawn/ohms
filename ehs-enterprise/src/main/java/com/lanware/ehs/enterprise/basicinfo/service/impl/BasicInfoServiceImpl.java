package com.lanware.ehs.enterprise.basicinfo.service.impl;

import com.google.common.collect.Lists;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.basicinfo.service.BasicInfoService;
import com.lanware.ehs.mapper.EnterpriseBaseinfoMapper;
import com.lanware.ehs.mapper.custom.EnterpriseBaseinfoCustomMapper;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseBaseinfoExample;
import com.lanware.ehs.pojo.custom.EnterpriseBaseinfoCustom;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class BasicInfoServiceImpl implements BasicInfoService {


    private EnterpriseBaseinfoCustomMapper enterpriseBaseinfoCustomMapper;

    private OSSTools ossTools;

    @Autowired
    public BasicInfoServiceImpl(EnterpriseBaseinfoCustomMapper enterpriseBaseinfoCustomMapper, OSSTools ossTools){
        this.enterpriseBaseinfoCustomMapper = enterpriseBaseinfoCustomMapper;
        this.ossTools = ossTools;
    }

    @Override
    @Transactional
    public int saveEnterpriseBaseinfo(EnterpriseBaseinfo enterpriseBaseinfo) {
        //id为空
        if (enterpriseBaseinfo.getId() == null){
            throw new EhsException("企业信息未知");
        }
        //更新企业基础信息
        EnterpriseBaseinfo targetBaseinfoSource = enterpriseBaseinfoCustomMapper.selectByPrimaryKey(enterpriseBaseinfo.getId());
        BeanUtils.copyProperties(enterpriseBaseinfo,targetBaseinfoSource,"lastUseTime","lastUpdateTime","status","gmtCreate");
        targetBaseinfoSource.setGmtModified(new Date());
        return enterpriseBaseinfoCustomMapper.updateByPrimaryKeySelective(targetBaseinfoSource);
    }

    @Override
    public EnterpriseBaseinfo findById(Long id) {
        return enterpriseBaseinfoCustomMapper.selectByPrimaryKey(id);
    }


    @Override
    public List<EnterpriseBaseinfo> find(EnterpriseBaseinfo enterpriseBaseinfo) {
        List<EnterpriseBaseinfo> list = Lists.newArrayList();
        if(enterpriseBaseinfo == null){
             list = enterpriseBaseinfoCustomMapper.selectByExample(null);
        }else{
            //可以通过mybatis example动态增加查询条件。
            EnterpriseBaseinfoExample example = new EnterpriseBaseinfoExample();
            EnterpriseBaseinfoExample.Criteria criteria = example.createCriteria();
            if(enterpriseBaseinfo.getStatus() != null){
                criteria.andStatusEqualTo(enterpriseBaseinfo.getStatus());
            }
            list = enterpriseBaseinfoCustomMapper.selectByExample(example);
        }
        return list;
    }
}
