package com.lanware.ehs.enterprise.responsibility.service;

import com.lanware.ehs.pojo.EnterpriseManageFile;

import java.util.List;
import java.util.Map;

/**
 * @description 各级责任制interface
 */
public interface ResponsibilityService {
    /**
     * @description 插入企业管理文件信息
     * @param enterpriseManageFile 企业管理文件pojo
     * @return 插入成功的数目
     */
    int insertEnterpriseManageFile(EnterpriseManageFile enterpriseManageFile);

    /**
     * @description 根据type更新企业管理文件信息
     * @param enterpriseManageFile 企业管理文件pojo
     * @param type 企业管理文件类型
     * @param enterpriseId 企业id
     * @return 更新成功的数目
     */
    int updateEnterpriseManageFile(EnterpriseManageFile enterpriseManageFile, String type, Long enterpriseId);

    /**
     * @description 根据企业id查询各级责任制文件list
     * @description 根据条件查询各级责任制文件list
     * @param condition 查询条件
     * @return 企业管理文件list
     */
    List<EnterpriseManageFile> listResponsibilityFiles(Map<String, Object> condition);

    /**
     * @description 根据文件路径删除对应的文件对象
     * @param filePath 文件路径
     * @return 删除成功的数目
     */
    int deleteFileByFilePath(String filePath);
}
