package com.lanware.ehs.enterprise.file.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 往来文件上传interface
 */
public interface CorrespondFileService {

    JSONObject listCorrespondFiles(Long relationId, String relationModule);

    EnterpriseRelationFile getCorrespondFileById(Long id);

    int insertCorrespondFile(EnterpriseRelationFile enterpriseRelationFile
            , MultipartFile correspondFile, Long enterpriseId);

    int updateCorrespondFile(EnterpriseRelationFile enterpriseRelationFile
            , MultipartFile correspondFile, Long enterpriseId);

    int deleteCorrespondFile(Long id, String filePath);

    int deleteCorrespondFiles(Long[] ids);
}
