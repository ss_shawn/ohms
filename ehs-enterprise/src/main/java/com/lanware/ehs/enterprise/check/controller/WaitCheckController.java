package com.lanware.ehs.enterprise.check.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.check.service.WaitCheckService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseWaitCheckCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @description 待检测项目controller
 */
@Controller()
@RequestMapping("/waitCheck")
@Slf4j
public class WaitCheckController {
    /** 注入waitCheckService的bean */
    @Autowired
    private WaitCheckService waitCheckService;

    /** @description 查询待检测项目result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 当前登录企业
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/listWaitChecks")
    @ResponseBody
    public ResultFormat listWaitChecks(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("status", EnterpriseWaitCheckCustom.STATUS_CHECK_NO);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = waitCheckService.listWaitChecks(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /** @description 查询待检测项目台账result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @param enterpriseUser 当前登录企业
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/listWaitCheckAccounts")
    @ResponseBody
    public ResultFormat listWaitCheckAccounts(Integer pageNum, Integer pageSize, String startDate, String endDate
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        if (startDate != null && !startDate.equals("")) {
            condition.put("startDate", startDate);
        }
        if (endDate != null && !endDate.equals("")) {
            condition.put("endDate", endDate);
        }
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = waitCheckService.listWaitCheckAccounts(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public EhsResult delete(@PathVariable Long id){
        try {
            waitCheckService.delete(id);
            return EhsResult.ok();
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            throw new EhsException("删除待检测项目失败");
        }
    }
}
