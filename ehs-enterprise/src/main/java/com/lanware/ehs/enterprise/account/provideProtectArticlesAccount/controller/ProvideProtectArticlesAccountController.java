package com.lanware.ehs.enterprise.account.provideProtectArticlesAccount.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.employee.controller.EmployeeArchivesController;
import com.lanware.ehs.enterprise.protect.service.ProtectService;
import com.lanware.ehs.enterprise.protect.service.ProvideProtectArticlesService;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 从业人员防护用品的发放使用管理台帐controller
 */
@Controller
@RequestMapping("/provideProtectArticlesAccount")
public class ProvideProtectArticlesAccountController extends BaseController {
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private ProtectService protectService;

    @Autowired
    private ProvideProtectArticlesService provideProtectArticlesService;


    @Value(value="${upload.tempPath}")
    private String uploadTempPath;


    @RequestMapping("/provideReportYear")
    public String provideReportYear(Integer year,Model model, @CurrentUser EnterpriseUserCustom currentUser) {

        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("enterpriseId", currentUser.getEnterpriseId());
        if(year==null){
            Calendar date = Calendar.getInstance();
            year = date.get(Calendar.YEAR);
        }
        condition.put("year",year);
        List<Map<String, Object>> provideProtectArticlesList = provideProtectArticlesService.findProvideProtectArticles(condition);
        List<String> rowsNum = new ArrayList();
        List<Map> returnList = new ArrayList<>();
        Map<String, Object> curMap = new HashMap();//第一次有变量，每个累计行数
        Map<String, Object> curPostMap = null;//当前岗位Map
        String curPostName = "";
        String curEmployeeName = "";
        for (Map<String, Object> map : provideProtectArticlesList) {
            String postName = (String) map.get("post_name");
            String employeeName = (String) map.get("employee_name");
            String provideArticlesName = (String) map.get("provide_articles_name");
            String workplaceName = (String) map.get("workplace_name");
            Integer number = Integer.parseInt(map.get("number").toString());
            //和上一个人员是否重复，不重复使用原map
            if (postName.equals(curPostName) && employeeName.equals(curEmployeeName)) {
                Map<String, Integer> colMap = (Map<String, Integer>) curMap.get("colMap");
                if (!contains(rowsNum, provideArticlesName)) {
                    rowsNum.add(provideArticlesName);
                }
                colMap.put(provideArticlesName, number);
            } else {
                //换人，初始化
                curMap = new HashMap();//第一次有变量，每个累计行数
                curEmployeeName = employeeName;
                curMap.put("employeeName", employeeName);
                curMap.put("postnum", 1);
                curMap.put("postName", null);
                curMap.put("protectName", null);
                curMap.put("name", null);
                Map<String, Integer> colMap = new HashMap();
                //判断是否包含标题，没有包含则插入
                if (!contains(rowsNum, provideArticlesName)) {
                    rowsNum.add(provideArticlesName);
                }
                colMap.put(provideArticlesName, number);
                curMap.put("colMap", colMap);
                if (postName.equals(curPostName)) {//只是人变了，加行数
                    curPostMap.put("postnum", (int) curPostMap.get("postnum") + 1);
                } else {//都变了
                    curMap.put("postName", postName);
                    curMap.put("workplaceName", workplaceName);
                    curPostMap = curMap;
                    curPostName = postName;
                }
                returnList.add(curMap);
            }

        }
        //把数据生成列表
        for (Map<String, Object> curM : returnList) {
            Map<String, Integer> colMap = (Map<String, Integer>) curM.get("colMap");
            List<String> colList = new ArrayList();
            for (String title : rowsNum) {
                if (!colMap.containsKey(title)) {
                    colList.add("");
                } else {
                    colList.add(colMap.get(title).toString());
                }
            }
            curM.put("colList", colList);
        }


        model.addAttribute("returnList", returnList);
        model.addAttribute("rowsNum", rowsNum);
        model.addAttribute("returnListSize", returnList.size());
        return "account/provideProtectArticlesAccount/provideReportYear";
    }





    //下载防护用品清单
    @RequestMapping("/downloadProtect")
    @ResponseBody
    public void downloadProtect( HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");


            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            //设置不分页
            request.setPageSize(0);
            //返回word的变量
            Map<String, Object> dataMap =getProtectMap(currentUser.getEnterpriseId(),request);
            String uploadPath = upload +  File.separator+"防护用品清单.doc";
            exeWord(dataMap,uploadPath,"protectList.ftl");

            //放入压缩列表
//            files.add(uploadPath);
//            String uploadZipPath = upload +  File.separator+"防护用品清单.zip";
//            FileUtil.toZip(files, uploadZipPath, true);


            File uploadFile=new File(uploadPath);

            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("防护用品清单.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    private Map getProtectMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);
        Page<EnterpriseProtectArticlesCustom> enterpriseProtectArticlesCustomPage=protectService.findProtectList(condition,request);
        List  protectArticlesList=new ArrayList();
        for(EnterpriseProtectArticlesCustom enterpriseProtectArticlesCustom:enterpriseProtectArticlesCustomPage){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name",enterpriseProtectArticlesCustom.getName());
            temp.put("model",enterpriseProtectArticlesCustom.getModel());
            temp.put("harmFactorName",enterpriseProtectArticlesCustom.getHarmFactorName());
            temp.put("description",enterpriseProtectArticlesCustom.getDescription());
            temp.put("certificatePath",enterpriseProtectArticlesCustom.getCertificatePath()==null?"--":"已上传");
            protectArticlesList.add(temp);
        }
        dataMap.put("protectArticlesList",protectArticlesList);
        return dataMap;
    }



    //下载防护用品清单
    @RequestMapping("/provideReport")
    @ResponseBody
    public void provideReport(Integer year, HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");


            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            Map<String, Object> dataMap = getProvideReportMap(currentUser.getEnterpriseId(),year);
            String uploadPath = upload +  File.separator+"个人防护用品发放情况统计.doc";
            exeWord(dataMap,uploadPath,"provideReport.ftl");
            File uploadFile=new File(uploadPath);

            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("个人防护用品发放情况统计.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }




    private Map getProvideReportMap(long enterpriseId,Integer year){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        condition.put("enterpriseId",enterpriseId);
        if(year==null){
            Calendar date = Calendar.getInstance();
            year = date.get(Calendar.YEAR);
        }
        condition.put("year",year);
        List<Map<String,Object>> provideProtectArticlesList=provideProtectArticlesService.findProvideProtectArticlesByPost(condition);
        List<String> rowsNum=new ArrayList();
        List<Map> returnList=new ArrayList<>();
        Map<String ,Object> curMap=new HashMap();//第一次有变量，每个累计行数
        Map<String ,Object> curPostMap=null;//当前岗位Map
        String curPostName="";
        String curEmployeeName="";
        for(Map<String,Object> map: provideProtectArticlesList){
            String postName=(String) map.get("post_name");
            String deptName=(String) map.get("dept_name");
            String employeeName=(String) map.get("employee_name");
            String provideArticlesName=(String) map.get("provide_articles_name");
            String workplaceName=(String) map.get("workplace_name");
            Integer number= Integer.parseInt(map.get("number").toString());
            //和上一个人员是否重复，不重复使用原map
            if(postName.equals(curPostName)&&employeeName.equals(curEmployeeName)){
                Map<String ,Integer> colMap= (Map<String, Integer>) curMap.get("colMap");
                if(!contains(rowsNum,provideArticlesName)){
                    rowsNum.add(provideArticlesName);
                }
                colMap.put(provideArticlesName,number);
            }else{
                //换人，初始化
                curMap=new HashMap();//第一次有变量，每个累计行数
                curEmployeeName=employeeName;
                curMap.put("employeeName",employeeName);
                curMap.put("postnum",1);
                curMap.put("postName",null);
                curMap.put("protectName",null);
                curMap.put("name",null);
                Map<String ,Integer> colMap=new HashMap();
                //判断是否包含标题，没有包含则插入
                if(!contains(rowsNum,provideArticlesName)){
                    rowsNum.add(provideArticlesName);
                }
                colMap.put(provideArticlesName,number);
                curMap.put("colMap",colMap);
                if(postName.equals(curPostName)){//只是人变了，加行数
                    curPostMap.put("postnum",(int)curPostMap.get("postnum")+1);
                }else{//都变了
                    curMap.put("postName",postName);
                    curMap.put("deptName",deptName);
                    curMap.put("workplaceName",workplaceName);
                    curPostMap=curMap;
                    curPostName=postName;
                }
                returnList.add(curMap);
            }

        }
        //把数据生成列表
        for( Map<String ,Object> curM:returnList){
            Map<String ,Integer> colMap =( Map<String ,Integer> )curM.get("colMap");
            List<String> colList=new ArrayList();
            for(String title:rowsNum){
                if(!colMap.containsKey(title)){
                    colList.add("");
                }else{
                    colList.add(colMap.get(title).toString());
                }
            }
            curM.put("colList",colList);
        }
        dataMap.put("rowsNum",rowsNum);
        dataMap.put("returnList",returnList);
        return dataMap;
    }
    private Map getProvideReportMapWorkplan(long enterpriseId){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        condition.put("enterpriseId",enterpriseId);
        List<Map<String,Object>> provideProtectArticlesList=provideProtectArticlesService.findProvideProtectArticles(condition);
        List<String> rowsNum=new ArrayList();
        List<Map> returnList=new ArrayList<>();
        Map<String ,Object> curMap=new HashMap();//第一次有变量，每个累计行数
        Map<String ,Object> curEntMap=null;//当前场所MAP
        Map<String ,Object> curPostMap=null;//当前岗位Map
        String curName="";
        String curPostName="";
        String curEmployeeName="";
        for(Map<String,Object> map: provideProtectArticlesList){
            String name= (String) map.get("name");
            String postName=(String) map.get("post_name");
            String employeeName=(String) map.get("employee_name");
            String provideArticlesName=(String) map.get("provide_articles_name");
            String protectName=(String) map.get("protect_name");
            Integer number= Integer.parseInt(map.get("number").toString());
            //和上一个人员是否重复，不重复使用原map
            if(name.equals(curName)&&postName.equals(curPostName)&&employeeName.equals(curEmployeeName)){
                Map<String ,Integer> colMap= (Map<String, Integer>) curMap.get("colMap");
                if(!contains(rowsNum,provideArticlesName)){
                    rowsNum.add(provideArticlesName);
                }
                colMap.put(provideArticlesName,number);
            }else{
                //换人，初始化
                curMap=new HashMap();//第一次有变量，每个累计行数
                curEmployeeName=employeeName;
                curMap.put("employeeName",employeeName);
                curMap.put("num",1);
                curMap.put("postnum",1);
                curMap.put("postName",null);
                curMap.put("protectName",null);
                curMap.put("name",null);
                Map<String ,Integer> colMap=new HashMap();
                //判断是否包含标题，没有包含则插入
                if(!contains(rowsNum,provideArticlesName)){
                    rowsNum.add(provideArticlesName);
                }
                colMap.put(provideArticlesName,number);
                curMap.put("colMap",colMap);
                if(name.equals(curName)&&postName.equals(curPostName)){//只是人变了，加行数
                    curEntMap.put("num",(int)curEntMap.get("num")+1);
                    curPostMap.put("postnum",(int)curEntMap.get("postnum")+1);
                }else if(name.equals(curName)){//岗位也变了，工作场所没变
                    curEntMap.put("num",(int)curEntMap.get("num")+1);
                    curMap.put("protectName",protectName);
                    curMap.put("postName",postName);
                    curPostName=postName;
                    curPostMap=curMap;
                }else{//都变了
                    curMap.put("postName",postName);
                    curMap.put("protectName",protectName);
                    curMap.put("name",name);
                    curPostMap=curMap;
                    curEntMap=curMap;
                    curName=name;
                    curPostName=postName;
                }
                returnList.add(curMap);
            }

        }
        //把数据生成列表
        for( Map<String ,Object> curM:returnList){
            Map<String ,Integer> colMap =( Map<String ,Integer> )curM.get("colMap");
            List<String> colList=new ArrayList();
            for(String title:rowsNum){
                if(!colMap.containsKey(title)){
                    colList.add("");
                }else{
                    colList.add(colMap.get(title).toString());
                }
            }
            curM.put("colList",colList);
        }
        dataMap.put("rowsNum",rowsNum);
        dataMap.put("returnList",returnList);
        return dataMap;
    }
    //下载防护用品清单
    @RequestMapping("/provideList")
    @ResponseBody
    public void provideList(String provide_keyword, HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {


            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

            //设置不分页
            request.setPageSize(0);
            //返回word的变量
            Map<String, Object> dataMap = getProvideListMap(currentUser.getEnterpriseId(),request,provide_keyword);

            String uploadPath = upload +  File.separator+"个人防护用品领用登记表.doc";
            exeWord(dataMap,uploadPath,"provideList.ftl");

            //放入压缩列表
//            files.add(uploadPath);
//            String uploadZipPath = upload +  File.separator+"防护用品清单.zip";
//            FileUtil.toZip(files, uploadZipPath, true);


            File uploadFile=new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("个人防护用品领用登记表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    //下载从业人员防护用品的发放使用管理台帐
    @RequestMapping("/protectAccount")
    @ResponseBody
    public void protectAccount(Integer year,String provide_keyword, HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();//待压缩的文件
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();

//防护用品清单
            //返回word的变量
            Map<String, Object> dataMap =getProtectMap(currentUser.getEnterpriseId(),request);
            String uploadPath = upload +  File.separator+"防护用品清单.doc";
            exeWord(dataMap,uploadPath,"protectList.ftl");
            files.add(uploadPath);
            //个人防护用品领用登记表
            //返回word的变量
            dataMap = getProvideListMap(currentUser.getEnterpriseId(),request,provide_keyword);
             uploadPath = upload +  File.separator+"个人防护用品领用登记表.doc";
            exeWord(dataMap,uploadPath,"provideList.ftl");
            files.add(uploadPath);
//个人防护用品发放情况统计
            dataMap = getProvideReportMap(currentUser.getEnterpriseId(),year);
            uploadPath = upload +  File.separator+"个人防护用品发放情况统计.doc";
            exeWord(dataMap,uploadPath,"provideReport.ftl");
            files.add(uploadPath);

            //放入压缩列表

            String uploadZipPath = upload +  File.separator+"从业人员防护用品的发放使用管理台帐.zip";
            FileUtil.toZip(files, uploadZipPath, false);


            //下载文件
            File uploadFile=new File(uploadZipPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("从业人员防护用品的发放使用管理台帐.zip", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    private Map getProvideListMap(long enterpriseId, QueryRequest request,String provide_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);
        condition.put("keyword",provide_keyword);
        //排序
        condition.put("orderByClause","provide_date desc");
        //查询并返回deleteProvide
        Page<EnterpriseProvideProtectArticlesCustom>  enterpriseProvideProtectArticlesCustoms=this.provideProtectArticlesService.findProtectList(condition,request);
        List  provideArticlesList=new ArrayList();
        for(EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticlesCustom:enterpriseProvideProtectArticlesCustoms){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("provideArticlesName",enterpriseProvideProtectArticlesCustom.getProvideArticlesName());
            temp.put("number",enterpriseProvideProtectArticlesCustom.getNumber());
            temp.put("employeeName",enterpriseProvideProtectArticlesCustom.getEmployeeName());
            temp.put("provideDate",sdf1.format(enterpriseProvideProtectArticlesCustom.getProvideDate()));
            temp.put("signatureRecord",enterpriseProvideProtectArticlesCustom.getSignatureRecord()==null?"(未签)":"(已签)");
            provideArticlesList.add(temp);
        }
        dataMap.put("provideArticlesList",provideArticlesList);
        return dataMap;
    }

    //下载从业人员防护用品的发放使用管理台帐
    @RequestMapping("/downloadProtectArticlesAccount")
    @ResponseBody
    public void downloadProtectArticlesAccount(Integer year,String provide_keyword, HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();//待压缩的文件
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+"/"+uuid+"/");
            if (!upload.exists()) upload.mkdirs();
            Map<String, Object> dataMapAll=new  HashMap<String, Object>();
//防护用品清单
            //返回word的变量
            Map<String, Object> dataMap =getProtectMap(currentUser.getEnterpriseId(),request);
            dataMapAll.putAll(dataMap);

            //个人防护用品领用登记表
            //返回word的变量
            dataMap = getProvideListMap(currentUser.getEnterpriseId(),request,provide_keyword);
            dataMapAll.putAll(dataMap);
//个人防护用品发放情况统计
            dataMap = getProvideReportMap(currentUser.getEnterpriseId(),year);
            dataMapAll.putAll(dataMap);


            String uploadPath = upload +  File.separator+"从业人员防护用品的发放使用管理台帐.doc";
            exeWord(dataMapAll,uploadPath,"protectAccount.ftl");

            //放入压缩列表


            //下载文件
            File uploadFile=new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("从业人员防护用品的发放使用管理台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(EmployeeArchivesController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
    public  boolean contains(List<String> list,String str){
        for(int i=0;i<list.size();i++){
            if (list.get(i).equals(str)) {
                return true;
            }
        }
        return false;
    }

}
