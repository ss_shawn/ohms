package com.lanware.ehs.enterprise.threesimultaneity.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.threesimultaneity.service.OtherFileService;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.mapper.custom.EnterpriseThreeSimultaneityCustomMapper;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description 三同时其他文件interface实现类
 */
@Service
public class OtherFileServiceImpl implements OtherFileService {
    /** 注入enterpriseRelationFileMapper的bean */
    @Autowired
    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;
    /** 注入enterpriseThreeSimultaneityCustomMapper的bean */
    @Autowired
    private EnterpriseThreeSimultaneityCustomMapper enterpriseThreeSimultaneityCustomMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    /**
     * @description 查询三同时其他文件list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @param fileType 文件类型
     * @return 返回包含enterpriseRelationFiles的result
     */
    @Override
    public JSONObject listOtherFiles(Long relationId, String relationModule, String fileType) {
        JSONObject result = new JSONObject();
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria = enterpriseRelationFileExample.createCriteria();
        criteria.andRelationIdEqualTo(relationId);
        criteria.andRelationModuleEqualTo(relationModule);
        criteria.andFileTypeEqualTo(fileType);
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        result.put("enterpriseRelationFiles", enterpriseRelationFiles);
        return result;
    }

    /**
     * @description 根据id查询三同时其他文件
     * @param id
     * @return EnterpriseRelationFile
     */
    @Override
    public EnterpriseRelationFile getOtherFileById(Long id) {
        return enterpriseRelationFileMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional
    public int insertOtherFile(EnterpriseRelationFile enterpriseRelationFile
            , MultipartFile otherFile, Long enterpriseId) {
        // 新增数据先保存，以便获取id
        enterpriseRelationFileMapper.insert(enterpriseRelationFile);
        // 上传并保存三同时其他文件文件
        if (otherFile != null){
            String other = "/" + enterpriseId + "/" + ModuleName.THREESIMULTANEITY.getValue()
                    + "/" + enterpriseRelationFile.getRelationId() + "/" + enterpriseRelationFile.getId()
                    + "/" + otherFile.getOriginalFilename();
            try {
                ossTools.uploadStream(other, otherFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传三同时其他文件文件失败", e);
            }
            enterpriseRelationFile.setFileName(otherFile.getOriginalFilename());
            enterpriseRelationFile.setFilePath(other);
        }
        // 同时更新三同时的上次更新时间
        Map<String, Object> condition = Maps.newHashMap();
        condition.put("id", enterpriseRelationFile.getRelationId());
        condition.put("gmtModified", new Date());
        enterpriseThreeSimultaneityCustomMapper.updateGmtModifiedByCondition(condition);
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
    }

    @Override
    public int updateOtherFile(EnterpriseRelationFile enterpriseRelationFile
            , MultipartFile otherFile, Long enterpriseId) {
        // 上传并保存三同时其他文件文件
        if (otherFile != null) {
            // 上传了新的三同时其他文件文件
            if (!StringUtils.isEmpty(enterpriseRelationFile.getFilePath())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
            }
            String other = "/" + enterpriseId + "/" + ModuleName.THREESIMULTANEITY.getValue()
                    + "/" + enterpriseRelationFile.getRelationId() + "/" + enterpriseRelationFile.getId()
                    + "/" + otherFile.getOriginalFilename();
            try {
                ossTools.uploadStream(other, otherFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传三同时其他文件文件失败", e);
            }
            enterpriseRelationFile.setFileName(otherFile.getOriginalFilename());
            enterpriseRelationFile.setFilePath(other);
        }
        // 同时更新三同时的上次更新时间
        Map<String, Object> condition = Maps.newHashMap();
        condition.put("id", enterpriseRelationFile.getRelationId());
        condition.put("gmtModified", new Date());
        enterpriseThreeSimultaneityCustomMapper.updateGmtModifiedByCondition(condition);
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
    }

    /**
     * @description 删除三同时其他文件
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    @Override
    public int deleteOtherFile(Long id, String filePath) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(filePath);
        // 删除数据库中对应的数据
        return enterpriseRelationFileMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除三同时其他文件
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteOtherFiles(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseRelationFile enterpriseRelationFile = enterpriseRelationFileMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria = enterpriseRelationFileExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
    }
}
