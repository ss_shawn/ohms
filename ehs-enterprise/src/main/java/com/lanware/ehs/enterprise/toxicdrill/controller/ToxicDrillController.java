package com.lanware.ehs.enterprise.toxicdrill.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.toxicdrill.service.ToxicDrillService;
import com.lanware.ehs.pojo.EnterpriseToxicDrill;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 高毒演练记录controller
 */
@Controller
@RequestMapping("/toxicDrill")
public class ToxicDrillController {
    /** 注入toxicDrillService的bean */
    @Autowired
    private ToxicDrillService toxicDrillService;

    @RequestMapping("")
    /**
     * @description 返回高毒演练记录页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "toxicDrill/list";
    }

    @RequestMapping("/listToxicDrills")
    @ResponseBody
    /**
     * @description 获取高毒演练记录result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listToxicDrills(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = toxicDrillService.listToxicDrills(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editToxicDrill/save")
    @ResponseBody
    /**
     * @description 保存高毒演练记录
     * @param enterpriseToxicDrillJSON 高毒演练记录json
     * @param drillFile 演练记录
     * @param proveFile 证明文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseToxicDrillJSON, MultipartFile proveFile, MultipartFile drillFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseToxicDrill enterpriseToxicDrill
                = JSONObject.parseObject(enterpriseToxicDrillJSON, EnterpriseToxicDrill.class);
        if (enterpriseToxicDrill.getId() == null) {
            enterpriseToxicDrill.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseToxicDrill.setGmtCreate(new Date());
            if (toxicDrillService.insertToxicDrill(enterpriseToxicDrill, drillFile, proveFile) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseToxicDrill.setGmtModified(new Date());
            if (toxicDrillService.updateToxicDrill(enterpriseToxicDrill, drillFile, proveFile) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteToxicDrill")
    @ResponseBody
    /**
     * @description 删除高毒演练记录
     * @param id 要删除的id
     * @param drillFile 要删除的演练记录路径
     * @param proveFile 要删除的证明文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteToxicDrill(Long id, String drillFile, String proveFile) {
        if (toxicDrillService.deleteToxicDrill(id, drillFile, proveFile) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteToxicDrills")
    @ResponseBody
    /**
     * @description 批量删除高毒演练记录
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteToxicDrills(Long[] ids) {
        if (toxicDrillService.deleteToxicDrills(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
