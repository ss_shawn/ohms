package com.lanware.ehs.enterprise.threesimultaneity.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityFileService;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 三同时文件controller
 */
@Controller
@RequestMapping("/threeSimultaneityFile")
public class ThreeSimultaneityFileController {
    /** 注入threeSimultaneityFileService的bean */
    @Autowired
    private ThreeSimultaneityFileService threeSimultaneityFileService;
    
    /**
     * @description 新增意见文件，并上传文件到oss服务器
     * @param enterpriseThreeSimultaneityFileJSON 三同时文件JSON
     * @param opinionFile 意见相关文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/addOpinion")
    @ResponseBody
    public ResultFormat save(String enterpriseThreeSimultaneityFileJSON, MultipartFile opinionFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile
                = JSONObject.parseObject(enterpriseThreeSimultaneityFileJSON, EnterpriseThreeSimultaneityFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        enterpriseThreeSimultaneityFile.setGmtCreate(new Date());
        if (threeSimultaneityFileService.insertOpinionFile(enterpriseThreeSimultaneityFile, opinionFile, enterpriseId) > 0) {
            return ResultFormat.success("添加成功");
        } else {
            return ResultFormat.error("添加失败");
        }
    }
}
