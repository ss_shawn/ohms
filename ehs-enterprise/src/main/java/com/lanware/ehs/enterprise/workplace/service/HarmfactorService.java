package com.lanware.ehs.enterprise.workplace.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.WorkplaceHarmFactor;
import com.lanware.ehs.pojo.custom.WorkplaceHarmFactorCustom;

import java.util.Map;

/**
 * @description 职业病危害因素interface
 */
public interface HarmfactorService {
    /**
     * @description 根据作业场所id查询危害因素list
     * @param workplaceId 作业场所id
     * @return 返回包含workHarmFactors的result
     */
    JSONObject listHarmfactors(Long workplaceId);

    /**
     * @description 查询作业场所危害因素扩展list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces和totalNum的result
     */
    JSONObject listWorkplaceHarmFactors(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 获取未选择的危害因素result（管理端危害因素获取）
     * @param workplaceId 工作场所id
     * @param harmFactorId 危害因素id
     * @return com.alibaba.fastjson.JSONObject 返回包含baseHarmFactors的result
     */
    JSONObject getHarmfactors(Long workplaceId, Long harmFactorId);

    /**
     * @description 根据id查询危害因素信息
     * @param id 危害因素id
     * @return com.lanware.management.pojo.WorkplaceHarmFactor 返回WorkplaceHarmFactor对象
     */
    WorkplaceHarmFactor getHarmfactorById(Long id);

    /**
     * @description 根据id查询危害因素扩展信息
     * @param id 危害因素id
     * @return com.lanware.management.pojo.WorkplaceHarmFactor 返回WorkplaceHarmFactor对象
     */
    WorkplaceHarmFactorCustom getHarmfactorCustomById(Long id);

    /**
     * @description 插入作业场所危害因素信息
     * @param workplaceHarmFactor 作业场所危害因素pojo
     * @return 插入成功的数目
     */
    int insertHarmfactor(WorkplaceHarmFactor workplaceHarmFactor);

    /**
     * @description 更新作业场所危害因素信息
     * @param workplaceHarmFactor 作业场所危害因素pojo
     * @return 更新成功的数目
     */
    int updateHarmfactor(WorkplaceHarmFactor workplaceHarmFactor);

    /**
     * @description 删除危害因素信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteHarmfactor(Long id);

    /**
     * @description 批量删除危害因素信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteHarmfactors(Long[] ids);
}
