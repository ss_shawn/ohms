package com.lanware.ehs.enterprise.hygieneorg.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.EnterpriseHygieneOrg;
import com.lanware.ehs.pojo.custom.EnterpriseHygieneOrgCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface HygieneOrgService {

    /**
     * 分页查询职业卫生组织信息
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<EnterpriseHygieneOrgCustom> queryEnterpriseHygieneOrgByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 保存职业卫生组织信息
     * @param enterpriseHygieneOrg 职业卫生组织对象
     * @param employFile 聘用书文件
     * @return 更新数量
     */
    int saveEnterpriseHygieneOrg(EnterpriseHygieneOrg enterpriseHygieneOrg, MultipartFile employFile);

    /**
     * 删除职业卫生组织信息
     * @param ids 职业卫生组织id集合
     * @return 更新数量
     */
    int deleteEnterpriseHygieneOrgByIds(List<Long> ids);

    /**
     * 查询卫生组织管理数据集合
     * @param enterpriseId 查询条件 企业ID
     * @return
     */
    List<EnterpriseHygieneOrg> queryListByEnterpriseId(Long enterpriseId);

    /**
     * 查询职业卫生组织管理员信息
     * @param enterpriseId 企业ID
     * @return
     */
    JSONObject listEmployeesByEnterprieId(Long enterpriseId);
}
