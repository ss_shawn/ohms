package com.lanware.ehs.enterprise.medical.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.medical.service.DiseaseDiagnosiService;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 职业病诊断controller
 */
@Controller
@RequestMapping("/medical/diseaseDiagnosi")
public class DiseaseDiagnosiController {
    @Autowired
    /** 注入diseaseDiagnosiService的bean */
    private DiseaseDiagnosiService diseaseDiagnosiService;

    @RequestMapping("/listDiseaseDiagnosis")
    @ResponseBody
    /**
     * @description 获取职业病诊断result
     * @param medicalId 体检id
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listDiseaseDiagnosis(Long medicalId) {
        JSONObject result = diseaseDiagnosiService.listDiseaseDiagnosis(medicalId);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editDiseaseDiagnosi/save")
    @ResponseBody
    /** @description 保存职业病诊断信息，并上传文件到oss服务器
     * @param employeeDiseaseDiagnosiJSON 职业病诊断json
     * @param medicalCertificateFile 诊断书
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String employeeDiseaseDiagnosiJSON, MultipartFile medicalCertificateFile
                             ,@CurrentUser EnterpriseUser enterpriseUser) {
        EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi
                = JSONObject.parseObject(employeeDiseaseDiagnosiJSON, EmployeeDiseaseDiagnosi.class);
        if (employeeDiseaseDiagnosi.getId() == null) {
            employeeDiseaseDiagnosi.setGmtCreate(new Date());
            if (diseaseDiagnosiService.insertDiseaseDiagnosi(employeeDiseaseDiagnosi
                    , medicalCertificateFile, enterpriseUser.getEnterpriseId()) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            employeeDiseaseDiagnosi.setGmtModified(new Date());
            if (diseaseDiagnosiService.updateDiseaseDiagnosi(employeeDiseaseDiagnosi
                    , medicalCertificateFile, enterpriseUser.getEnterpriseId()) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteDiseaseDiagnosi")
    @ResponseBody
    /**
     * @description 删除职业病诊断
     * @param id 要删除的id
     * @param medicalCertificate 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteDiseaseDiagnosi(Long id, String medicalCertificate) {
        if (diseaseDiagnosiService.deleteDiseaseDiagnosi(id, medicalCertificate) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteDiseaseDiagnosis")
    @ResponseBody
    /**
     * @description 批量删除职业病诊断
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteDiseaseDiagnosis(Long[] ids) {
        if (diseaseDiagnosiService.deleteDiseaseDiagnosis(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
