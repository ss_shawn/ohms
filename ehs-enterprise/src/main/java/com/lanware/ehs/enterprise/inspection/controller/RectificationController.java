package com.lanware.ehs.enterprise.inspection.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.inspection.service.RectificationService;
import com.lanware.ehs.pojo.EnterpriseInspectionRectification;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 整改记录controller
 */
@Controller
@RequestMapping("/inspection/rectification")
public class RectificationController {
    /** 注入rectificationService的bean */
    @Autowired
    private RectificationService rectificationService;

    /**
     * @description 查询整改记录list
     * @param inspectionId 关联id
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listRectifications")
    @ResponseBody
    public ResultFormat listRectifications(Long inspectionId) {
        JSONObject result = rectificationService.listRectifications(inspectionId);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editRectification/save")
    @ResponseBody
    /** @description 保存整改记录，并上传文件到oss服务器
     * @param enterpriseInspectionRectificationJSON 整改记录JSON
     * @param rectificationFile 整改文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseInspectionRectificationJSON, MultipartFile rectificationFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseInspectionRectification enterpriseInspectionRectification
                = JSONObject.parseObject(enterpriseInspectionRectificationJSON, EnterpriseInspectionRectification.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseInspectionRectification.getId() == null) {
            enterpriseInspectionRectification.setGmtCreate(new Date());
            if (rectificationService.insertRectification(enterpriseInspectionRectification
                    , rectificationFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseInspectionRectification.setGmtModified(new Date());
            if (rectificationService.updateRectification(enterpriseInspectionRectification
                    , rectificationFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteRectification")
    @ResponseBody
    /**
     * @description 删除整改记录
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteRectification(Long id, String filePath) {
        if (rectificationService.deleteRectification(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteRectifications")
    @ResponseBody
    /**
     * @description 批量删除整改记录
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteRectifications(Long[] ids) {
        if (rectificationService.deleteRectifications(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
