package com.lanware.ehs.enterprise.inspectioninstitution.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 机构相关证书interface
 */
public interface InstitutionCertificateService {
    /**
     * @description 查询机构相关证书list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    JSONObject listInstitutionCertificates(Long relationId, String relationModule);

    /**
     * @description 根据id查询机构相关证书
     * @param id
     * @return 返回EnterpriseRelationFile
     */
    EnterpriseRelationFile getInstitutionCertificateById(Long id);

    /**
     * @description 新增机构相关证书，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param institutionCertificate 机构相关证书
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertInstitutionCertificate(EnterpriseRelationFile enterpriseRelationFile
            , MultipartFile institutionCertificate, Long enterpriseId);

    /**
     * @description 更新机构相关证书，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param institutionCertificate 机构相关证书
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateInstitutionCertificate(EnterpriseRelationFile enterpriseRelationFile
            , MultipartFile institutionCertificate, Long enterpriseId);

    /**
     * @description 删除机构相关证书
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteInstitutionCertificate(Long id, String filePath);

    /**
     * @description 批量删除机构相关证书
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteInstitutionCertificates(Long[] ids);
}