package com.lanware.ehs.enterprise.file.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.file.service.CorrespondFileService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 往来文件上传controller
 */
@Controller
@RequestMapping("/file/correspondFile")
public class CorrespondFileController {
    /** 注入correspondFileService的bean */
    @Autowired
    private CorrespondFileService correspondFileService;

    /**
     * @description 查询往来文件list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listCorrespondFiles")
    @ResponseBody
    public ResultFormat listCorrespondFiles(Long relationId, String relationModule) {
        JSONObject result = correspondFileService.listCorrespondFiles(relationId, relationModule);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editCorrespondFile/save")
    @ResponseBody
    /** @description 保存往来文件，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param correspondFile 往来文件
     * @param enterpriseUser 当前登录企业 enterpriseUser
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile correspondFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (correspondFileService.insertCorrespondFile(enterpriseRelationFile, correspondFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (correspondFileService.updateCorrespondFile(enterpriseRelationFile, correspondFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteCorrespondFile")
    @ResponseBody
    /**
     * @description 删除往来文件
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteCorrespondFile(Long id, String filePath) {
        if (correspondFileService.deleteCorrespondFile(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteCorrespondFiles")
    @ResponseBody
    /**
     * @description 批量删除往来文件
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteCorrespondFiles(Long[] ids) {
        if (correspondFileService.deleteCorrespondFiles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
