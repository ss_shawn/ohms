package com.lanware.ehs.enterprise.consult.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.consult.service.ConsultService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.SysConsult;
import com.lanware.ehs.pojo.custom.SysConsultCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/consult")
/**
 * @description 咨询提问controller
 */
public class ConsultController {
    @Autowired
    /** 注入consultService的bean */
    private ConsultService consultService;

    @RequestMapping("/listConsults")
    @ResponseBody
    /**
     * @description 获取咨询提问result
     * @param pageNum 请求的是页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @param enterpriseUser 企业用户
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat listConsults(Integer pageNum, Integer pageSize, String keyword
            , String sortField, String sortType, @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = consultService.listConsults(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editConsult/save")
    @ResponseBody
    /**
     * @description 保存咨询提问
     * @param sysConsultJSON 咨询提问json
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String sysConsultJSON, @CurrentUser EnterpriseUser enterpriseUser) {
        SysConsult sysConsult = JSONObject.parseObject(sysConsultJSON, SysConsult.class);
        if (sysConsult.getId() == null) {
            sysConsult.setEnterpriseId(enterpriseUser.getEnterpriseId());
            sysConsult.setAnswerStatus(SysConsultCustom.STATUS_ANSWER_NO);
            sysConsult.setReadStatus(SysConsultCustom.STATUS_READ_NO);
            sysConsult.setGmtCreate(new Date());
            if (consultService.insertConsult(sysConsult) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            sysConsult.setGmtModified(new Date());
            if (consultService.updateConsult(sysConsult) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteConsult")
    @ResponseBody
    /**
     * @description 删除咨询提问
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat deleteConsult(Long id) {
        if (consultService.deleteConsult(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteConsults")
    @ResponseBody
    /**
     * @description 批量删除咨询提问
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat deleteHarmFactors(Long[] ids) {
        if (consultService.deleteConsults(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
