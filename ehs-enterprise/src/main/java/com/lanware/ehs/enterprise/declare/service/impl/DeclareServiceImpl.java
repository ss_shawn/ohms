package com.lanware.ehs.enterprise.declare.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.declare.service.DeclareService;
import com.lanware.ehs.mapper.EnterpriseDeclareMapper;
import com.lanware.ehs.mapper.EnterpriseDeclareWorkplaceMapper;
import com.lanware.ehs.mapper.custom.EnterpriseDeclareCustomMapper;
import com.lanware.ehs.pojo.EnterpriseDeclare;
import com.lanware.ehs.pojo.EnterpriseDeclareExample;
import com.lanware.ehs.pojo.EnterpriseDeclareWorkplaceExample;
import com.lanware.ehs.pojo.custom.EnterpriseDeclareCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 作业场所危害申报interface实现类
 */
public class DeclareServiceImpl implements DeclareService {
    @Autowired
    /** 注入enterpriseDeclareMapper的bean */
    private EnterpriseDeclareMapper enterpriseDeclareMapper;
    @Autowired
    /** 注入enterpriseDeclareCustomMapper的bean */
    private EnterpriseDeclareCustomMapper enterpriseDeclareCustomMapper;
    @Autowired
    /** 注入enterpriseDeclareWorkplaceMapper的bean */
    private EnterpriseDeclareWorkplaceMapper enterpriseDeclareWorkplaceMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    @Override
    /**
     * @description 根据企业id查询作业场所危害申报list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces和totalNum的result
     */
    public JSONObject listDeclaresByEnterpriseId(Integer pageNum, Integer pageSize, Map<String, Object> condition, Long enterpriseId) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseDeclareCustom> enterpriseDeclareCustoms = enterpriseDeclareCustomMapper.listDeclares(condition);
        // 查询数据和分页数据装进result
        result.put("enterpriseDeclareCustoms", enterpriseDeclareCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseDeclareCustom> pageInfo = new PageInfo<EnterpriseDeclareCustom>(enterpriseDeclareCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        // 查询申报率(危害因素总数和申报数量)
        Map<String, Object> declareData = enterpriseDeclareCustomMapper.queryDeclareData(enterpriseId);
        result.put("declareData", declareData);
        return result;
    }

    @Override
    /**
     * @description 根据id查询作业场所危害申报信息
     * @param id 危害申报id
     * @return com.lanware.management.pojo.EnterpriseWorkplace 返回EnterpriseWorkplace对象
     */
    public EnterpriseDeclare getDeclareById(Long id) {
        return enterpriseDeclareMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional
    /**
     * @description 新增危害申报信息
     * @param enterpriseDeclare 危害申报pojo
     * @param declareFile 申报文件
     * @return int 插入成功的数目
     */
    public int insertDeclare(EnterpriseDeclare enterpriseDeclare, MultipartFile declareFile) {
        // 新增数据先保存，以便获取id
        enterpriseDeclareMapper.insert(enterpriseDeclare);
        // 上传并保存申报文件
        if (declareFile != null){
            String declare = "/" + enterpriseDeclare.getEnterpriseId() + "/" + ModuleName.DECLARE.getValue()
                    + "/" + enterpriseDeclare.getId() + "/" + declareFile.getOriginalFilename();
            try {
                ossTools.uploadStream(declare, declareFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传申报文件失败", e);
            }
            enterpriseDeclare.setDeclareAttachment(declare);
        }
        return enterpriseDeclareMapper.updateByPrimaryKeySelective(enterpriseDeclare);
    }

    @Override
    /**
     * @description 更新危害申报信息
     * @param enterpriseDeclare 危害申报pojo
     * @param declareFile 申报文件
     * @return int 更新成功的数目
     */
    public int updateDeclare(EnterpriseDeclare enterpriseDeclare, MultipartFile declareFile) {
        // 上传并保存申报文件
        if (declareFile != null){
            // 上传了新的申报文件
            if (!StringUtils.isEmpty(enterpriseDeclare.getDeclareAttachment())){
                // 删除旧文件
                ossTools.deleteOSS(enterpriseDeclare.getDeclareAttachment());
            }
            String declare = "/" + enterpriseDeclare.getEnterpriseId() + "/" + ModuleName.DECLARE.getValue()
                    + "/" + enterpriseDeclare.getId() + "/" + declareFile.getOriginalFilename();
            try {
                ossTools.uploadStream(declare, declareFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传申报文件失败", e);
            }
            enterpriseDeclare.setDeclareAttachment(declare);
        }
        return enterpriseDeclareMapper.updateByPrimaryKeySelective(enterpriseDeclare);
    }

    @Override
    /**
     * @description 更新申报回执信息
     * @param enterpriseDeclare 危害申报pojo
     * @param receiptFile 回执文件
     * @return int 更新成功的数目
     */
    public int updateDeclareReceipt(EnterpriseDeclare enterpriseDeclare, MultipartFile receiptFile) {
        // 上传并保存回执文件
        if (receiptFile != null){
            // 上传了新的回执文件
            if (!StringUtils.isEmpty(enterpriseDeclare.getReceiptAttachment())){
                // 删除旧文件
                ossTools.deleteOSS(enterpriseDeclare.getReceiptAttachment());
            }
            String receipt = "/" + enterpriseDeclare.getEnterpriseId() + "/" + ModuleName.DECLARE.getValue()
                    + "/" + enterpriseDeclare.getId() + "/" + receiptFile.getOriginalFilename();
            try {
                ossTools.uploadStream(receipt, receiptFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传回执文件失败", e);
            }
            enterpriseDeclare.setReceiptAttachment(receipt);
        }
        return enterpriseDeclareMapper.updateByPrimaryKeySelective(enterpriseDeclare);
    }

    @Override
    @Transactional
    /**
     * @description 删除危害申报信息
     * @param id 要删除的id
     * @param declareAttachment 要删除的申报附件路径
     * @param receiptAttachment 要删除的回执文件路径
     * @return int 删除成功的数目
     */
    public int deleteDeclare(Long id, String declareAttachment, String receiptAttachment) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(declareAttachment);
        ossTools.deleteOSS(receiptAttachment);
        // 删除申报涉及的场所和危害因素
        EnterpriseDeclareWorkplaceExample enterpriseDeclareWorkplaceExample = new EnterpriseDeclareWorkplaceExample();
        EnterpriseDeclareWorkplaceExample.Criteria criteria = enterpriseDeclareWorkplaceExample.createCriteria();
        criteria.andDeclareIdEqualTo(id);
        enterpriseDeclareWorkplaceMapper.deleteByExample(enterpriseDeclareWorkplaceExample);
        // 删除数据库中对应的数据
        return enterpriseDeclareMapper.deleteByPrimaryKey(id);
    }

    @Override
    @Transactional
    /**
     * @description 批量删除危害申报信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    public int deleteDeclares(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseDeclare enterpriseDeclare = enterpriseDeclareMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseDeclare.getDeclareAttachment());
            ossTools.deleteOSS(enterpriseDeclare.getReceiptAttachment());
        }
        // 删除申报涉及的场所和危害因素
        EnterpriseDeclareWorkplaceExample enterpriseDeclareWorkplaceExample = new EnterpriseDeclareWorkplaceExample();
        EnterpriseDeclareWorkplaceExample.Criteria criteria = enterpriseDeclareWorkplaceExample.createCriteria();
        criteria.andDeclareIdIn(Arrays.asList(ids));
        enterpriseDeclareWorkplaceMapper.deleteByExample(enterpriseDeclareWorkplaceExample);
        // 删除数据库中对应的数据
        EnterpriseDeclareExample enterpriseDeclareExample = new EnterpriseDeclareExample();
        EnterpriseDeclareExample.Criteria criteria1 = enterpriseDeclareExample.createCriteria();
        criteria1.andIdIn(Arrays.asList(ids));
        return enterpriseDeclareMapper.deleteByExample(enterpriseDeclareExample);
    }

    @Override
    public List<EnterpriseDeclare> queryDeclareNoReceipt(EnterpriseDeclareCustom entity) {
        EnterpriseDeclareExample example = new EnterpriseDeclareExample();
        EnterpriseDeclareExample.Criteria criteria = example.createCriteria();
        criteria.andReceiptDateIsNull();
        criteria.andReceiptAttachmentIsNull();
        criteria.andEnterpriseIdEqualTo(entity.getEnterpriseId());
        return enterpriseDeclareMapper.selectByExample(example);
    }

    /**
     * @description 查询申报百分比(危害因素总数和申报数量)
     * @param enterpriseId 当前企业id
     * @return java.util.Map<java.lang.String,java.lang.Integer>
     */
    @Override
    public Map<String, Object> getDeclaredData(Long enterpriseId) {
        return enterpriseDeclareCustomMapper.queryDeclareData(enterpriseId);
    }
}
