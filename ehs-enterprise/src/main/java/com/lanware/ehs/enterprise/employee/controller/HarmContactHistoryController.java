package com.lanware.ehs.enterprise.employee.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.employee.service.HarmContactHistoryService;
import com.lanware.ehs.enterprise.post.service.HarmFactorService;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EmployeeHarmContactHistoryCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectiveMeasuresCustom;
import com.lanware.ehs.pojo.custom.PostHarmFactorCustom;
import com.lanware.ehs.service.EmployeeHarmContactHistoryService;
import com.lanware.ehs.service.EnterprisePostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/employee/harmContactHistory")
@Controller
public class HarmContactHistoryController   extends BaseController {

    private String basePath = "employee/harmContactHistory/";

    private EmployeeHarmContactHistoryService employeeHarmContactHistoryService;

    private EnterprisePostService enterprisePostService;

    private HarmContactHistoryService harmContactHistoryService;

    private HarmFactorService harmFactorService;

    @Autowired
    public HarmContactHistoryController(EmployeeHarmContactHistoryService employeeHarmContactHistoryService, EnterprisePostService enterprisePostService, HarmContactHistoryService harmContactHistoryService, HarmFactorService harmFactorService) {
        this.employeeHarmContactHistoryService = employeeHarmContactHistoryService;
        this.enterprisePostService = enterprisePostService;
        this.harmContactHistoryService = harmContactHistoryService;
        this.harmFactorService = harmFactorService;
    }

    /**
     * 职业危害接触史列表页面
     * @return 职业危害接触史列表model
     */
    @RequestMapping("")
    public ModelAndView list(Long employeeId){
        JSONObject data = new JSONObject();
        data.put("employeeId", employeeId);
        return new ModelAndView(basePath + "list", data);
    }

    /**
     * 分页查询职业危害接触史
     * @param employeeId 人员id
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public EhsResult getList(Long employeeId, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("employeeId", employeeId);
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<EmployeeHarmContactHistoryCustom> page = harmContactHistoryService.queryEmployeeHarmContactHistoryByCondition(0, 0, condition);
        //查询并返回
        Map<String, Object> dataTable = getDataTable(page);
        return EhsResult.ok(dataTable);
    }

    /**
     * 编辑职业危害接触史页面
     * @param id 职业危害接触史id
     * @param harmContactHistoryJSON 职业危害接触史JSON
     * @param tableIndex 表格索引
     * @return 编辑职业危害接触史model
     */
    @RequestMapping("/edit")
    public ModelAndView edit(@CurrentUser EnterpriseUser enterpriseUser, Long id, String harmContactHistoryJSON, Integer tableIndex){
        JSONObject data = new JSONObject();
        EmployeeHarmContactHistory employeeHarmContactHistory = null;
        if (id != null){
            try {
                employeeHarmContactHistory = employeeHarmContactHistoryService.selectByPrimaryKey(id);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }else if (!StringUtils.isEmpty(harmContactHistoryJSON)){
            employeeHarmContactHistory = JSONObject.parseObject(harmContactHistoryJSON, EmployeeHarmContactHistory.class);
        }
        data.put("employeeHarmContactHistory", employeeHarmContactHistory);
        EnterprisePostExample enterprisePostExample = new EnterprisePostExample();
        EnterprisePostExample.Criteria criteria = enterprisePostExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseUser.getEnterpriseId());
        try {
            List<EnterprisePost> enterprisePosts = enterprisePostService.selectByExample(enterprisePostExample);
            data.put("enterprisePosts", enterprisePosts);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        if (employeeHarmContactHistory != null && employeeHarmContactHistory.getEmployeePostId()!= null){//原来是postId ，具体作用待定
            Map<String, Object> condition = new HashMap<>();
            condition.put("postId", employeeHarmContactHistory.getEmployeePostId());
            Page<PostHarmFactorCustom> page = harmFactorService.queryPostHarmFactorByCondition(0, 0, condition);
            data.put("postHarmFactors", page.getResult());
            List<EnterpriseProtectiveMeasuresCustom> protectWays = harmContactHistoryService.queryProtectEnterpriseProtectiveMeasuresByPostId(employeeHarmContactHistory.getEmployeePostId());
            data.put("protectWays", protectWays);
        }
        data.put("tableIndex", tableIndex);
        return new ModelAndView(basePath + "edit", data);
    }
    /**
     * 查询危害因素和保护措施
     * @param postId 岗位id
     * @return 查询结果
     */
    @RequestMapping("/queryHarmFactorAndProtectWay")
    @ResponseBody
    public EhsResult queryHarmFactorAndProtectWay(Long postId){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("postId", postId);
        Page<PostHarmFactorCustom> page = harmFactorService.queryPostHarmFactorByCondition(0, 0, condition);
        data.put("postHarmFactors", page.getResult());
        List<EnterpriseProtectiveMeasuresCustom> protectWays = harmContactHistoryService.queryProtectEnterpriseProtectiveMeasuresByPostId(postId);
        data.put("protectWays", protectWays);
        return EhsResult.ok(data);
    }

    /**
     * 保存职业危害接触史
     * @param employeeHarmContactHistoryJSON 职业危害接触史JSON
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(String employeeHarmContactHistoryJSON){
        EmployeeHarmContactHistory employeeHarmContactHistory = JSONObject.parseObject(employeeHarmContactHistoryJSON, EmployeeHarmContactHistory.class);
        if (employeeHarmContactHistory.getId() == null){//新建
            employeeHarmContactHistory.setStatus(0);
            employeeHarmContactHistory.setGmtCreate(new Date());
            try {
                employeeHarmContactHistoryService.insert(employeeHarmContactHistory);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存职业危害接触史失败");
            }
        }else {//修改
            employeeHarmContactHistory.setGmtModified(new Date());
            try {
                employeeHarmContactHistoryService.updateByPrimaryKeySelective(employeeHarmContactHistory);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存职业危害接触史失败");
            }
        }
        return EhsResult.ok();
    }

    /**
     * 删除职业危害接触史
     * @param idsJSON id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public EhsResult delete(String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        EmployeeHarmContactHistoryExample employeeHarmContactHistoryExample = new EmployeeHarmContactHistoryExample();
        EmployeeHarmContactHistoryExample.Criteria criteria = employeeHarmContactHistoryExample.createCriteria();
        criteria.andIdIn(ids);
        try {
            employeeHarmContactHistoryService.deleteByExample(employeeHarmContactHistoryExample);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), " 删除职业危害接触史失败");
        }
        return EhsResult.ok();
    }

    /**
     * 查看职业危害接触史页面
     * @param id 职业危害接触史id
     * @return 查看职业危害接触史model
     */
    @RequestMapping("/view")
    public ModelAndView view(@RequestParam Long id){
        JSONObject data = new JSONObject();
        EmployeeHarmContactHistoryCustom employeeHarmContactHistory = harmContactHistoryService.queryEmployeeHarmContactHistoryById(id);
        data.put("employeeHarmContactHistory", employeeHarmContactHistory);
        if(employeeHarmContactHistory.getStatus()==1){//手动添加
            return new ModelAndView(basePath + "view2", data);
        }else{//自动生成
            return new ModelAndView(basePath + "view", data);
        }

    }
}
