package com.lanware.ehs.enterprise.training.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.training.service.TrainingService;
import com.lanware.ehs.mapper.EnterpriseEmployeeMapper;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.mapper.custom.EnterpriseTrainingCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseTrainingEmployeeCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseYearPlanCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.Enum.YearPlanTypeEnum;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingCustom;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingEmployeeCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * @description 培训管理interface实现类
 */
@Service
public class TrainingServiceImpl implements TrainingService {
    /** 注入enterpriseTrainingCustomMapper的bean */
    @Autowired
    private EnterpriseTrainingCustomMapper enterpriseTrainingCustomMapper;
    /** 注入enterpriseEmployeeMapper的bean */
    @Autowired
    private EnterpriseEmployeeMapper enterpriseEmployeeMapper;
    @Autowired
    /** 注入enterpriseTrainingEmployeeCustomMapper的bean */
    private EnterpriseTrainingEmployeeCustomMapper enterpriseTrainingEmployeeCustomMapper;
    /** 注入enterpriseRelationFileMapper的bean */
    @Autowired
    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;
    /** 注入enterpriseYearPlanCustomMapper的bean */
    @Autowired
    private EnterpriseYearPlanCustomMapper enterpriseYearPlanCustomMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    /**
     * @description 查询培训管理list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    @Override
    public JSONObject listTrainings(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseTrainingCustom> enterpriseTrainings = enterpriseTrainingCustomMapper.listTrainings(condition);
        result.put("enterpriseTrainings", enterpriseTrainings);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseTrainingCustom> pageInfo = new PageInfo<EnterpriseTrainingCustom>(enterpriseTrainings);
            result.put("totalNum", pageInfo.getTotal());
        }
        // 查询该年度培训计划(普通员工培训时长和管理员培训时长)
        int year = Calendar.getInstance().get(Calendar.YEAR);
        // 年度培训计划查询条件
        Map<String ,Object> planCondition = new HashMap<String, Object>();
        planCondition.put("enterpriseId", condition.get("enterpriseId"));
        planCondition.put("year", year);
        planCondition.put("planType", YearPlanTypeEnum.XCJY);
        EnterpriseYearPlan enterpriseYearPlan = enterpriseYearPlanCustomMapper.getTrainingPlan(planCondition);
        // 如果本年度没有培训计划，查询员工信息时不考虑计划时长，否则考虑计划时长
        if (enterpriseYearPlan != null) {
            BigDecimal employeeTrainingPlanDuration = enterpriseYearPlan.getEmployeeTrainTime();
            BigDecimal adminTrainingPlanDuration = enterpriseYearPlan.getAdminTrainTime();
//            condition.put("employeeTrainingPlanDuration", employeeTrainingPlanDuration);
//            condition.put("adminTrainingPlanDuration", adminTrainingPlanDuration);
            condition.put("year", year);
        }
        // 查询普通员工信息(在岗的普通员工培训总时长和人数)
        Map<String, Object> employeeTrainingData
                = enterpriseTrainingEmployeeCustomMapper.getEmployeeTrainingData(condition);
        result.put("employeeTrainingData", employeeTrainingData);
        // 查询管理员信息(培训总时长和人数)
        Map<String, Object> adminTrainingData
                = enterpriseTrainingEmployeeCustomMapper.getAdminTrainingData(condition);
        result.put("adminTrainingData", adminTrainingData);
        return result;
    }

    /**
     * @description 获取人员result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseEmployees的result
     */
    @Override
    public JSONObject listEmployeesByEnterprieId(Long enterpriseId) {
        JSONObject result = new JSONObject();
        EnterpriseEmployeeExample enterpriseEmployeeExample = new EnterpriseEmployeeExample();
        EnterpriseEmployeeExample.Criteria criteria = enterpriseEmployeeExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        List<EnterpriseEmployee> enterpriseEmployees
                = enterpriseEmployeeMapper.selectByExample(enterpriseEmployeeExample);
        result.put("enterpriseEmployees", enterpriseEmployees);
        return result;
    }

    /**
     * @description 根据id查询培训扩展信息
     * @param id 培训id
     * @return com.lanware.management.pojo.EnterpriseTrainingCustom 返回EnterpriseTrainingCustom对象
     */
    @Override
    public EnterpriseTrainingCustom getTrainingCustomById(Long id) {
        return enterpriseTrainingCustomMapper.getTrainingCustomById(id);
    }

    /**
     * @description 新增培训信息
     * @param enterpriseTraining 培训pojo
     * @param attendanceSheetFile 签到表
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertTrainingAndEmployees(EnterpriseTraining enterpriseTraining, MultipartFile attendanceSheetFile) {
        // 新增数据先保存，以便获取id
        enterpriseTrainingCustomMapper.insert(enterpriseTraining);
        // 上传并保存签到表
        if (attendanceSheetFile != null){
            String attendanceSheet = "/" + enterpriseTraining.getEnterpriseId() + "/" + ModuleName.TRAINING.getValue()
                    + "/" + enterpriseTraining.getId() + "/" + attendanceSheetFile.getOriginalFilename();
            try {
                ossTools.uploadStream(attendanceSheet, attendanceSheetFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传签到表失败", e);
            }
            enterpriseTraining.setAttendanceSheet(attendanceSheet);
        }
        return enterpriseTrainingCustomMapper.updateByPrimaryKeySelective(enterpriseTraining);
    }

    /**
     * @description 更新培训信息
     * @param enterpriseTraining 培训pojo
     * @param attendanceSheetFile 签到表
     * @return int 更新成功的数目
     */
    @Override
    @Transactional
    public int updateTrainingAndEmployees(EnterpriseTraining enterpriseTraining, MultipartFile attendanceSheetFile) {
        // 上传并保存签到表
        if (attendanceSheetFile != null) {
            // 上传了新的签到表
            if (!StringUtils.isEmpty(enterpriseTraining.getAttendanceSheet())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseTraining.getAttendanceSheet());
            }
            String attendanceSheet = "/" + enterpriseTraining.getEnterpriseId() + "/" + ModuleName.TRAINING.getValue()
                    + "/" + enterpriseTraining.getId() + "/" + attendanceSheetFile.getOriginalFilename();
            try {
                ossTools.uploadStream(attendanceSheet, attendanceSheetFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传签到表失败", e);
            }
            enterpriseTraining.setAttendanceSheet(attendanceSheet);
        }
        return enterpriseTrainingCustomMapper.updateByPrimaryKeySelective(enterpriseTraining);
    }

    /**
     * @description 删除培训信息
     * @param id 要删除的id
     * @param attendanceSheet 要删除的签到表路径
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteTraining(Long id, String attendanceSheet) {
        // 删除数据库中当前培训信息的培训人员关系集合
        EnterpriseTrainingEmployeeExample enterpriseTrainingEmployeeExample = new EnterpriseTrainingEmployeeExample();
        EnterpriseTrainingEmployeeExample.Criteria criteria = enterpriseTrainingEmployeeExample.createCriteria();
        criteria.andTrainingIdEqualTo(id);
        enterpriseTrainingEmployeeCustomMapper.deleteByExample(enterpriseTrainingEmployeeExample);
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(attendanceSheet);
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria1= enterpriseRelationFileExample.createCriteria();
        criteria1.andRelationIdEqualTo(id);
        criteria1.andRelationModuleEqualTo("培训证明资料");
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        for (EnterpriseRelationFile enterpriseRelationFile : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        // 删除数据库中对应的数据
        enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        return enterpriseTrainingCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除培训信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteTrainings(Long[] ids) {
        // 删除当前当前培训信息的培训人员关系集合
        EnterpriseTrainingEmployeeExample enterpriseTrainingEmployeeExample = new EnterpriseTrainingEmployeeExample();
        EnterpriseTrainingEmployeeExample.Criteria criteria = enterpriseTrainingEmployeeExample.createCriteria();
        criteria.andTrainingIdIn(Arrays.asList(ids));
        enterpriseTrainingEmployeeCustomMapper.deleteByExample(enterpriseTrainingEmployeeExample);
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseTraining enterpriseTraining = enterpriseTrainingCustomMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseTraining.getAttendanceSheet());
        }
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria1= enterpriseRelationFileExample.createCriteria();
        criteria1.andRelationIdIn(Arrays.asList(ids));
        criteria1.andRelationModuleEqualTo("培训证明资料");
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        for (EnterpriseRelationFile enterpriseRelationFile : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        // 删除数据库中的培训关联文件信息
        enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        // 删除数据库中的培训信息
        EnterpriseTrainingExample enterpriseTrainingExample = new EnterpriseTrainingExample();
        EnterpriseTrainingExample.Criteria criteria2 = enterpriseTrainingExample.createCriteria();
        criteria2.andIdIn(Arrays.asList(ids));
        return enterpriseTrainingCustomMapper.deleteByExample(enterpriseTrainingExample);
    }

    @Override
    public String getTrainTime(Map<String, Object> parameter) {
        String trainTime = enterpriseTrainingCustomMapper.getTrainTime(parameter);
        return trainTime;
    }

    /**
     * @description 查询管理员培训信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listTraingAdmins(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseTrainingEmployeeCustom> trainingAdmins
                = enterpriseTrainingEmployeeCustomMapper.listTraingAdmins(condition);
        result.put("trainingAdmins", trainingAdmins);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseTrainingEmployeeCustom> pageInfo = new PageInfo<EnterpriseTrainingEmployeeCustom>(trainingAdmins);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 查询普通员工培训扩展信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listTraingEmployees(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseTrainingEmployeeCustom> trainingEmployees
                = enterpriseTrainingEmployeeCustomMapper.listTraingEmployees(condition);
        result.put("trainingEmployees", trainingEmployees);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseTrainingEmployeeCustom> pageInfo = new PageInfo<EnterpriseTrainingEmployeeCustom>(trainingEmployees);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据培训id查询管理员培训信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listTrainingAdminsByTrainingId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseTrainingEmployeeCustom> trainingAdmins
                = enterpriseTrainingEmployeeCustomMapper.listTrainingAdminsByTrainingId(condition);
        result.put("trainingAdmins", trainingAdmins);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseTrainingEmployeeCustom> pageInfo = new PageInfo<EnterpriseTrainingEmployeeCustom>(trainingAdmins);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据培训id查询员工培训信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listTraingEmployeesByTrainingId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseTrainingEmployeeCustom> trainingEmployees
                = enterpriseTrainingEmployeeCustomMapper.listTraingEmployeesByTrainingId(condition);
        result.put("trainingEmployees", trainingEmployees);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseTrainingEmployeeCustom> pageInfo = new PageInfo<EnterpriseTrainingEmployeeCustom>(trainingEmployees);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据培训id过滤尚未培训的管理员
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listSelectTrainingAdminsByTrainingId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseTrainingEmployeeCustom> selectTrainingAdmins
                = enterpriseTrainingEmployeeCustomMapper.listSelectTrainingAdminsByTrainingId(condition);
        result.put("selectTrainingAdmins", selectTrainingAdmins);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseTrainingEmployeeCustom> pageInfo = new PageInfo<EnterpriseTrainingEmployeeCustom>(selectTrainingAdmins);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据培训id过滤尚未培训的普通员工
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listSelectTrainingEmployeesByTrainingId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseTrainingEmployeeCustom> selectTrainingEmployees
                = enterpriseTrainingEmployeeCustomMapper.listSelectTrainingEmployeesByTrainingId(condition);
        result.put("selectTrainingEmployees", selectTrainingEmployees);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseTrainingEmployeeCustom> pageInfo = new PageInfo<EnterpriseTrainingEmployeeCustom>(selectTrainingEmployees);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 批量插入参加培训的员工集合
     * @param enterpriseTrainingEmployeeList 参加培训的员工集合
     * @return int 插入成功的数目
     */
    @Override
    public int insertBatch(List<EnterpriseTrainingEmployee> enterpriseTrainingEmployeeList) {
        return enterpriseTrainingEmployeeCustomMapper.insertBatch(enterpriseTrainingEmployeeList);
    }

    /**
     * @description 删除参加培训的员工
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    public int deleteTrainingEmployee(Long id) {
        return enterpriseTrainingEmployeeCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除参加培训的员工
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteTrainingEmployees(Long[] ids) {
        EnterpriseTrainingEmployeeExample enterpriseTrainingEmployeeExample = new EnterpriseTrainingEmployeeExample();
        EnterpriseTrainingEmployeeExample.Criteria criteria = enterpriseTrainingEmployeeExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseTrainingEmployeeCustomMapper.deleteByExample(enterpriseTrainingEmployeeExample);
    }

    /**
     * @description 获取管理员年度培训报表
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getYearAdminTrainingData(Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 查询该年度管理员培训时长
        EnterpriseYearPlan enterpriseYearPlan = enterpriseYearPlanCustomMapper.getTrainingPlan(condition);
        // 如果本年度没有培训计划，查询员工信息时不考虑计划时长，否则考虑计划时长
        if (enterpriseYearPlan != null) {
            BigDecimal adminTrainingPlanDuration = enterpriseYearPlan.getAdminTrainTime();
            condition.put("adminTrainingPlanDuration", adminTrainingPlanDuration);
        }
        // 查询该年度管理员培训信息(培训总时长和人数)
        List<EnterpriseTrainingEmployeeCustom> yearAdminTrainingData
                = enterpriseTrainingEmployeeCustomMapper.getYearAdminTrainingData(condition);
        result.put("yearAdminTrainingData", yearAdminTrainingData);
        return result;
    }

    /**
     * @description 获取普通员工年度培训报表
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getYearEmployeeTrainingData(Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 查询该年度管理员培训时长
        EnterpriseYearPlan enterpriseYearPlan = enterpriseYearPlanCustomMapper.getTrainingPlan(condition);
        // 如果本年度没有培训计划，查询员工信息时不考虑计划时长，否则考虑计划时长
        if (enterpriseYearPlan != null) {
            BigDecimal employeeTrainingPlanDuration = enterpriseYearPlan.getEmployeeTrainTime();
            condition.put("employeeTrainingPlanDuration", employeeTrainingPlanDuration);
        }
        // 查询该年度普通员工培训信息(培训总时长和人数)
        List<EnterpriseTrainingEmployeeCustom> yearEmployeeTraingData
                = enterpriseTrainingEmployeeCustomMapper.getYearEmployeeTrainingData(condition);
        result.put("yearEmployeeTraingData", yearEmployeeTraingData);
        return result;
    }

    /**
     * 获取年度每月培训人数
     * @param condition
     * @return
     */
    @Override
    public Map<String, Integer> getTrainNumByCondition(Map<String, Object> condition) {
        return enterpriseTrainingCustomMapper.getTrainNumByCondition(condition);
    }
}
