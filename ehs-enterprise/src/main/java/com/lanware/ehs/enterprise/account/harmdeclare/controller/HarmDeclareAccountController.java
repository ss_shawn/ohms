package com.lanware.ehs.enterprise.account.harmdeclare.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.declare.service.DeclareService;
import com.lanware.ehs.enterprise.post.service.HarmFactorService;
import com.lanware.ehs.enterprise.workplace.service.HarmfactorService;
import com.lanware.ehs.enterprise.workplace.service.WorkplaceService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseDeclareCustom;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceCustom;
import com.lanware.ehs.pojo.custom.PostHarmFactorCustom;
import com.lanware.ehs.pojo.custom.WorkplaceHarmFactorCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 申报台账controller
 */
@Controller
@RequestMapping("/harmDeclareAccount")
public class HarmDeclareAccountController {
    @Autowired
    /** 注入workplaceService的bean */
    private WorkplaceService workplaceService;
    @Autowired
    /** 注入harmfactorService的bean */
    private HarmfactorService harmfactorService;
    @Autowired
    /** 注入declareService的bean */
    private DeclareService declareService;
    @Autowired
    /** 注入harmFactorService的bean */
    private HarmFactorService harmFactorService;

    @Value(value="${upload.tempPath}")
    private String uploadTempPath;
    
    @RequestMapping("")
    /**
     * @description 返回申报台账页面
     * @param enterpriseUser 当前登录企业
     * @return java.lang.String 返回页面路径
     */
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/harmDeclare/list", data);
    }

    @RequestMapping("/listTouchHarmNum")
    @ResponseBody
    /**
     * @description 获取接触危害因素人数result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listTouchHarmNum(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = harmFactorService.listTouchHarmNum(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(HarmDeclareAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 存在职业危害因素的作业场所word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadHarmWorkplace")
    @ResponseBody
    public void downloadHarmWorkplace(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getHarmWorkplaceMap(enterpriseId,request);
            String uploadPath = upload + File.separator +"存在职业危害因素的作业场所.doc";
            exeWord(dataMap,uploadPath,"harmWorkplace.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("存在职业危害因素的作业场所.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到存在职业危害因素的作业场所的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getHarmWorkplaceMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        //查询并返回
        JSONObject result = workplaceService.listHarmWorkplaces(0, 0, condition);
        List<EnterpriseWorkplaceCustom> enterpriseWorkplaces
                = (List<EnterpriseWorkplaceCustom>)result.get("enterpriseWorkplaces");
        List harmWorkplaceList = new ArrayList();
        for (EnterpriseWorkplaceCustom enterpriseWorkplaceCustom : enterpriseWorkplaces) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name", enterpriseWorkplaceCustom.getName());
            harmWorkplaceList.add(temp);
        }
        dataMap.put("harmWorkplaceList", harmWorkplaceList);
        return dataMap;
    }

    /**
     * @description 作业场所职业病危害因素word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadWorkplaceHarmFactor")
    @ResponseBody
    public void downloadWorkplaceHarmFactor(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getWorkplaceHarmFactorMap(enterpriseId,request);
            String uploadPath = upload + File.separator +"作业场所职业病危害因素.doc";
            exeWord(dataMap,uploadPath,"workplaceHarmFactor.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("作业场所职业病危害因素.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到作业场所职业病危害因素的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getWorkplaceHarmFactorMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 查询存在危害因素的作业场所
        JSONObject workplaceResult = workplaceService.listHarmWorkplaces(0,0,condition);
        List<EnterpriseWorkplaceCustom> harmWorkplaces
                = (List<EnterpriseWorkplaceCustom>)workplaceResult.get("enterpriseWorkplaces");
        List harmWorkplaceList = new ArrayList();
        for (EnterpriseWorkplaceCustom enterpriseWorkplaceCustom : harmWorkplaces) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("workplaceName", enterpriseWorkplaceCustom.getName());
            temp.put("harmFactorNum", enterpriseWorkplaceCustom.getHarmfactorNum());
            // 根据作业场所id查询该作业场所存在的危害因素扩展信息
            condition.put("workplaceId", enterpriseWorkplaceCustom.getId());
            JSONObject result1 = harmfactorService.listWorkplaceHarmFactors(0, 0, condition);
            List<WorkplaceHarmFactorCustom> workplaceHarmFactorCustoms
                    = (List<WorkplaceHarmFactorCustom>)result1.get("workplaceHarmFactorCustoms");
            List workplaceHarmFactorList = new ArrayList();
            for (WorkplaceHarmFactorCustom workplaceHarmFactorCustom : workplaceHarmFactorCustoms) {
                Map<String, Object> temp1 = new HashMap<String, Object>();
                temp1.put("harmFactorName",workplaceHarmFactorCustom.getHarmFactorName() == null ? "无"
                        : workplaceHarmFactorCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                temp1.put("harmSource",workplaceHarmFactorCustom.getHarmSource());
                temp1.put("deviceStatus",workplaceHarmFactorCustom.getDeviceStatus());
                temp1.put("operationWay",workplaceHarmFactorCustom.getOperationWay());
                temp1.put("isPartition",workplaceHarmFactorCustom.getIsPartition());
                temp1.put("protectiveMeasureName",workplaceHarmFactorCustom.getProtectiveMeasureName());
                temp1.put("protectiveEquipmentName",workplaceHarmFactorCustom.getProtectiveEquipmentName());
                workplaceHarmFactorList.add(temp1);
            }
            temp.put("workplaceHarmFactorList", workplaceHarmFactorList);
            harmWorkplaceList.add(temp);
        }
        dataMap.put("harmWorkplaceList", harmWorkplaceList);
        return dataMap;
    }

    /**
     * @description 危害申报记录word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadHarmDeclareRecord")
    @ResponseBody
    public void downloadHarmDeclareRecord(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getHarmDeclareRecordMap(enterpriseId,request);
            String uploadPath = upload + File.separator +"危害申报记录.doc";
            exeWord(dataMap,uploadPath,"harmDeclareRecord.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("危害申报记录.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到危害申报记录的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getHarmDeclareRecordMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 查询并返回
        JSONObject result = declareService.listDeclaresByEnterpriseId(0, 0, condition, enterpriseId);
        List<EnterpriseDeclareCustom> enterpriseDeclareCustoms
                = (List<EnterpriseDeclareCustom>)result.get("enterpriseDeclareCustoms");
        List harmDeclareRecordList = new ArrayList();
        for (EnterpriseDeclareCustom enterpriseDeclareCustom : enterpriseDeclareCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("declareDate", sdf.format(enterpriseDeclareCustom.getDeclareDate()));
            temp.put("declareType", enterpriseDeclareCustom.getDeclareType());
            if (enterpriseDeclareCustom.getReceiptDate() == null) {
                temp.put("receiptDate", enterpriseDeclareCustom.getReceiptDate());
            } else {
                temp.put("receiptDate", sdf.format(enterpriseDeclareCustom.getReceiptDate()));
            }
            temp.put("declareHarmWorkplace", enterpriseDeclareCustom.getDeclareHarmWorkplace() == null ? "无"
                    :enterpriseDeclareCustom.getDeclareHarmWorkplace().replace("<","&lt;").replace(">","&gt;"));
            harmDeclareRecordList.add(temp);
        }
        dataMap.put("harmDeclareRecordList", harmDeclareRecordList);
        return dataMap;
    }

    /**
     * @description 在岗涉害人数统计word
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadTouchHarm")
    @ResponseBody
    public void downloadTouchHarm(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getTouchHarmMap(enterpriseId,request);
            String uploadPath = upload + File.separator +"在岗涉害人数统计.doc";
            exeWord(dataMap,uploadPath,"touchHarm.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("在岗涉害人数统计.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到在岗涉害人数统计的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getTouchHarmMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        //查询并返回
        JSONObject result = harmFactorService.listTouchHarmNum(0, 0, condition);
        List<PostHarmFactorCustom> postHarmFactorCustoms
                = (List<PostHarmFactorCustom>)result.get("postHarmFactorCustoms");
        List touchHarmList = new ArrayList();
        for (PostHarmFactorCustom postHarmFactorCustom : postHarmFactorCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name",postHarmFactorCustom.getHarmFactorName() == null ? "无"
                    : postHarmFactorCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
            temp.put("totalNum",postHarmFactorCustom.getTotalNum());
            temp.put("femaleNum",postHarmFactorCustom.getFemaleNum());
            temp.put("migrantNum",postHarmFactorCustom.getMigrantNum());
            touchHarmList.add(temp);
        }
        dataMap.put("touchHarmList", touchHarmList);
        return dataMap;
    }

    /**
     * @description 职业危害申报台账zip下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadHarmDeclare")
    @ResponseBody
    public void downloadHarmDeclare(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload=null;
        List<String> files=new ArrayList<String>();//待压缩的文件
        String uuid= UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 存在职业病危害的作业场所
            // 返回word的变量
            Map<String, Object> dataMap = getHarmWorkplaceMap(enterpriseId,request);
            String uploadPath = upload + File.separator +"存在职业病危害的作业场所.doc";
            exeWord(dataMap,uploadPath,"harmWorkplace.ftl");
            files.add(uploadPath);
            // 作业场所职业病危害因素
            dataMap = getWorkplaceHarmFactorMap(enterpriseId,request);
            uploadPath = upload + File.separator +"作业场所职业病危害因素.doc";
            exeWord(dataMap,uploadPath,"workplaceHarmFactor.ftl");
            files.add(uploadPath);
            // 危害申报记录
            dataMap = getHarmDeclareRecordMap(enterpriseId,request);
            uploadPath = upload + File.separator +"危害申报记录.doc";
            exeWord(dataMap,uploadPath,"harmDeclareRecord.ftl");
            files.add(uploadPath);
            // 在岗涉害人数统计
            dataMap = getTouchHarmMap(enterpriseId,request);
            uploadPath = upload + File.separator +"在岗涉害人数统计.doc";
            exeWord(dataMap,uploadPath,"touchHarm.ftl");
            files.add(uploadPath);
            //放入压缩列表
            String uploadZipPath = upload + File.separator +"申报台账.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            //下载文件
            File uploadFile = new File(uploadZipPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("申报台账.zip", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 申报台账文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadHarmDeclareAccount")
    @ResponseBody
    public void downloadHarmDeclareAccount(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            Map<String, Object> dataMap = getHarmDeclareAccountMap(enterpriseId,request);
            String uploadPath = upload + File.separator + "职业危害申报台帐.doc";
            exeWord(dataMap,uploadPath,"harmDeclareAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业危害申报台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到申报台账文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getHarmDeclareAccountMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 危害申报记录
        JSONObject result = declareService.listDeclaresByEnterpriseId(0, 0, condition, enterpriseId);
        List<EnterpriseDeclareCustom> enterpriseDeclareCustoms
                = (List<EnterpriseDeclareCustom>)result.get("enterpriseDeclareCustoms");
        List harmDeclareRecordList = new ArrayList();
        for (EnterpriseDeclareCustom enterpriseDeclareCustom : enterpriseDeclareCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("declareDate", sdf.format(enterpriseDeclareCustom.getDeclareDate()));
            temp.put("declareType", enterpriseDeclareCustom.getDeclareType());
            String declareAttachment = enterpriseDeclareCustom.getDeclareAttachment();
            temp.put("declareAttachment", (declareAttachment == null || declareAttachment.equals("")) ? "-" : "已上传");
            Date receiptDate = enterpriseDeclareCustom.getReceiptDate();
            temp.put("receiptDate", receiptDate == null ? "-" : sdf.format(receiptDate));
            String receiptAttachment = enterpriseDeclareCustom.getReceiptAttachment();
            temp.put("receiptAttachment", (receiptAttachment == null || receiptAttachment.equals("")) ? "-" : "已上传");
            harmDeclareRecordList.add(temp);
        }
        dataMap.put("harmDeclareRecordList", harmDeclareRecordList);
        return dataMap;
    }
}
