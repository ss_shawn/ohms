package com.lanware.ehs.enterprise.basicinfo.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.basicinfo.service.BasicInfoService;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseBaseinfoCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/basicInfo")
public class BasicInfoController {

    private EnterpriseBaseinfoService enterpriseBaseinfoService;

    private BasicInfoService basicInfoService;

    @Autowired
    private OSSTools ossTools;

    @Autowired
    public BasicInfoController(EnterpriseBaseinfoService enterpriseBaseinfoService, BasicInfoService basicInfoService){
        this.enterpriseBaseinfoService = enterpriseBaseinfoService;
        this.basicInfoService = basicInfoService;
    }


    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }


    /**
     * 企业基础信息页面
     * @return 企业基础信息model
     */
    @RequestMapping("")
    public ModelAndView basicInfo(@CurrentUser EnterpriseUser enterpriseUser){
        JSONObject data = new JSONObject();
        try {
            EnterpriseBaseinfo enterpriseBaseinfo = enterpriseBaseinfoService.selectByPrimaryKey(enterpriseUser.getEnterpriseId());
            data.put("enterpriseBaseinfo", enterpriseBaseinfo);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return new ModelAndView("basicInfo/view", data);
    }

    /**
     * 保存企业基础信息
     * @param enterpriseBaseinfo 企业基础信息Object
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(EnterpriseBaseinfo enterpriseBaseinfo){
        try {
            int num = basicInfoService.saveEnterpriseBaseinfo(enterpriseBaseinfo);
            if (num == 0){
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存企业信息失败");
            }
        } catch (EhsException e){
            throw e;
        }
        return EhsResult.ok();
    }



    @PostMapping("file/upload")
    @ResponseBody
    public EhsResult uploadFile(@RequestParam("file") MultipartFile file, @CurrentUser EnterpriseUserCustom userCustom) {
        if(file == null){
            return EhsResult.build(500,"上传文件为空");
        }
        String url;
        Map m = Maps.newHashMap();
        try {
            String path = "/"+userCustom.getEnterpriseId()+"/"+ ModuleName.BASICINFO;
            url = ossTools.upload(path,file);
            m.put("url", url);
            m.put("name", file.getOriginalFilename());
        } catch (Exception e) {
            throw new EhsException("上传文件失败");
        }
        return EhsResult.ok(m);
    }
}
