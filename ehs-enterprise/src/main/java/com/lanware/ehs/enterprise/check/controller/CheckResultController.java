package com.lanware.ehs.enterprise.check.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.check.service.CheckResultService;
import com.lanware.ehs.pojo.EnterpriseCheckResultDetail;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckResult;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/outsideCheck/checkResult")
/**
 * @description 检测结果controller
 */
public class CheckResultController {
    @Autowired
    /** 注入checkResultService的bean */
    private CheckResultService checkResultService;

    /**
     * @description 获取检测结果result
     * @param outsideCheckId 企业外部检测id
     * @param harmFactorId 检测因素id
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/listCheckResults")
    @ResponseBody
    public ResultFormat listCheckResults(Long outsideCheckId, Long harmFactorId, Integer pageNum, Integer pageSize
            , String keyword, @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("outsideCheckId", outsideCheckId);
        condition.put("harmFactorId", harmFactorId);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = checkResultService.listCheckResults(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /** @description 更新检测的判定结果
     * @param id 检测结果id
     * @param value 判定结果
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/updateQualified")
    @ResponseBody
    public ResultFormat updateQualified(Long id, String value) {
        EnterpriseOutsideCheckResult enterpriseOutsideCheckResult = new EnterpriseOutsideCheckResult();
        enterpriseOutsideCheckResult.setId(id);
        enterpriseOutsideCheckResult.setIsQualified(value);
        enterpriseOutsideCheckResult.setGmtModified(new Date());
        if (checkResultService.updateCheckResult(enterpriseOutsideCheckResult) > 0) {
            return ResultFormat.success("修改成功");
        } else {
            return ResultFormat.error("修改失败");
        }
    }

    /**
     * @description 获取检测结果详细result
     * @param checkResultId 检测结果id
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/detail/listCheckResultDetails")
    @ResponseBody
    public ResultFormat listCheckResultDetails(Long checkResultId, Integer pageNum, Integer pageSize
            , String keyword, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("checkResultId", checkResultId);
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = checkResultService.listCheckResultDetails(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /** @description 保存检测结果详细信息
     * @param enterpriseCheckResultDetail 检测结果详细pojo
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/detail/save")
    @ResponseBody
    public ResultFormat saveCheckResultDetail(EnterpriseCheckResultDetail enterpriseCheckResultDetail) {
        if (enterpriseCheckResultDetail.getId() == null) {
            enterpriseCheckResultDetail.setGmtCreate(new Date());
            if (checkResultService.insertDetail(enterpriseCheckResultDetail) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseCheckResultDetail.setGmtModified(new Date());
            if (checkResultService.updateDetail(enterpriseCheckResultDetail) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    /**
     * @description 取消检测
     * @param id 要取消检测的id
     * @param cancelReason 取消检测的原因
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/cancel")
    @ResponseBody
    public ResultFormat cancelCheckResult(Long id, String cancelReason) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("id", id);
        condition.put("cancelReason", cancelReason);
        condition.put("status", EnterpriseOutsideCheckResultCustom.STATUS_CANCEL_CHECK_YES);
        condition.put("gmtModified", new Date());
        if (checkResultService.cancelCheckResult(condition) > 0) {
            return ResultFormat.success("取消成功");
        }else{
            return ResultFormat.error("取消失败");
        }
    }


    /**
     * @description 判定结果
     * @param id 要更改判定结果的id
     * @param qualified 判定结果
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/qualified/save")
    @ResponseBody
    public ResultFormat saveQualified(Long id, String qualified) {
        try {
            EnterpriseOutsideCheckResult checkResult = new EnterpriseOutsideCheckResult();
            checkResult.setId(id);
            checkResult.setIsQualified(qualified);
            checkResult.setGmtModified(new Date());
            checkResultService.updateCheckResult(checkResult);
            return ResultFormat.success("更改判定结果成功");
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return ResultFormat.error("更改判定结果失败");
        }

    }




    /**
     * @description 删除危害因素检测结果详细
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/detail/delete")
    @ResponseBody
    public ResultFormat deleteCheckResultDetail(Long id) {
        if (checkResultService.deleteCheckResultDetail(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    /**
     * @description 批量删除危害因素检测结果详细
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/detail/deletes")
    @ResponseBody
    public ResultFormat deleteCheckResultDetail(Long[] ids) {
        if (checkResultService.deleteCheckResultDetails(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    /**
     * @description 获取危害因素检测结果详细result
     * @param outsideCheckId 企业外部检测id
     * @param harmFactorId 检测因素id
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/listCheckResultCustoms")
    @ResponseBody
    public List<EnterpriseOutsideCheckResultCustom> listCheckResultCustoms(Long outsideCheckId, Long harmFactorId) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("outsideCheckId", outsideCheckId);
        condition.put("harmFactorId", harmFactorId);
        return checkResultService.listCheckResultCustoms(condition);
    }
}
