package com.lanware.ehs.enterprise.resource.service;

import com.lanware.ehs.enterprise.resource.entity.MenuTree;
import com.lanware.ehs.pojo.EnterpriseResource;
import com.lanware.ehs.pojo.custom.EnterpriseResourceCustom;

import java.util.List;

public interface ResourceService {
    /**
     * 查找所有的菜单/按钮 （树形结构）
     *
     * @return MenuTree<Menu>
     */
    MenuTree<EnterpriseResourceCustom> findMenus(EnterpriseResourceCustom menu);


    /**
     * 查找用户菜单集合
     *
     * @param username 用户名
     * @return 用户菜单集合
     */
    MenuTree<EnterpriseResourceCustom> findUserMenus(String username);


    /**
     * 查找用户权限集
     *
     * @param username 用户名
     * @return 用户权限集合
     */
    List<EnterpriseResourceCustom> findUserPermissions(String username);

    /**
     * 查找菜单
     * @param menu
     * @return
     */
    List<EnterpriseResource> findAllMenus(EnterpriseResource menu);
}
