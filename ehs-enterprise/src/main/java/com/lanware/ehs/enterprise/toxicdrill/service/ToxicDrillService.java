package com.lanware.ehs.enterprise.toxicdrill.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseToxicDrill;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @description 高毒演练记录interface
 */
public interface ToxicDrillService {
    /**
     * @description 查询高毒演练记录list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseToxicDrills和totalNum的result
     */
    JSONObject listToxicDrills(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询高毒演练记录
     * @param id 高毒演练记录id
     * @return com.lanware.management.pojo.EnterpriseToxicDrill 返回EnterpriseToxicDrill对象
     */
    EnterpriseToxicDrill getToxicDrillById(Long id);

    /**
     * @description 新增高毒演练记录
     * @param enterpriseToxicDrill 高毒演练记录pojo
     * @param drillFile 演练记录
     * @param proveFile 证明文件
     * @return int 插入成功的数目
     */
    int insertToxicDrill(EnterpriseToxicDrill enterpriseToxicDrill, MultipartFile drillFile, MultipartFile proveFile);

    /**
     * @description 更新高毒演练记录
     * @param enterpriseToxicDrill 高毒演练记录pojo
     * @param drillFile 演练记录
     * @param proveFile 证明文件
     * @return int 更新成功的数目
     */
    int updateToxicDrill(EnterpriseToxicDrill enterpriseToxicDrill, MultipartFile drillFile, MultipartFile proveFile);

    /**
     * @description 删除高毒演练记录
     * @param id 要删除的id
     * @param drillFile 要删除的演练记录路径
     * @param proveFile 要删除的证明文件路径
     * @return int 删除成功的数目
     */
    int deleteToxicDrill(Long id, String drillFile, String proveFile);

    /**
     * @description 批量删除高毒演练记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteToxicDrills(Long[] ids);
}
