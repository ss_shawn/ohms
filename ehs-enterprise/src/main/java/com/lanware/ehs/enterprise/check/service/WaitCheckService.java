package com.lanware.ehs.enterprise.check.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseWaitCheck;
import com.lanware.ehs.pojo.WaitCheckHarmFactor;
import com.lanware.ehs.pojo.custom.EnterpriseWaitCheckCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 待检测项目interface
 */
public interface WaitCheckService {

    JSONObject listWaitChecks(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    JSONObject listWaitCheckAccounts(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    List<EnterpriseWaitCheckCustom> findEnterpriseWaitChecks(Map<String,Object> parameter);

    void insert(EnterpriseWaitCheck waitCheck);

    /**
     * 插入待检测的危害因素
     * @param waitCheckHarmFactor
     */
    void insertWaitCheckHarmFactor(WaitCheckHarmFactor waitCheckHarmFactor);

    /**
     * 删除待检测危害因素
     * @param id
     */
    void delete(Long id);
}
