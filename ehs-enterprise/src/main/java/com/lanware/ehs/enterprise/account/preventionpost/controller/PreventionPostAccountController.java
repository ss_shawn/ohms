package com.lanware.ehs.enterprise.account.preventionpost.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.post.service.PostService;
import com.lanware.ehs.enterprise.prevention.service.PreventionService;
import com.lanware.ehs.pojo.EnterpriseManageFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseManageFileCustom;
import com.lanware.ehs.pojo.custom.EnterprisePostCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 防治制度台帐controller
 */
@Controller
@RequestMapping("/preventionPostAccount")
public class PreventionPostAccountController {

    /** 下载word的临时路径 */
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;

    /** 注入preventionService的bean */
    @Autowired
    private PreventionService preventionService;
    /** 注入postService的bean */
    @Autowired
    private PostService postService;

    /**
     * @description 返回防治制度台帐页面
     * @param enterpriseUser 当前登录企业
     * @return java.lang.String 返回页面路径
     */
    @RequestMapping("")
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/preventionPost/list", data);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord(Map<String, Object> dataMap, String uploadPath, String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(PreventionPostAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 防治制度台账文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadPreventionPostAccount")
    @ResponseBody
    public void downloadPreventionPostAccount(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            Map<String, Object> dataMap = getPreventionPostAccountMap(enterpriseId,request);
            String uploadPath = upload + File.separator + "职业危害防治制度和岗位操作规程台帐.doc";
            exeWord(dataMap,uploadPath,"preventionPostAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业危害防治制度和岗位操作规程台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到防治制度台账文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getPreventionPostAccountMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 职业危害防治制度
        condition.put("module", EnterpriseManageFileCustom.MODULE_PREVENTION);
        List<EnterpriseManageFile> enterpriseManageFiles = preventionService.listPreventionFiles(condition);
        List preventionList = new ArrayList();
        for (EnterpriseManageFile enterpriseManageFile : enterpriseManageFiles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("fileType", enterpriseManageFile.getFileType());
            String filePath = enterpriseManageFile.getFilePath();
            temp.put("filePath", (filePath == null || filePath.equals("")) ? "-" : "已制定上传");
            preventionList.add(temp);
        }
        dataMap.put("preventionList", preventionList);
        // 岗位操作规程(只导出有岗位操作规程的)
        condition.put("existRule", 1);
        Page<EnterprisePostCustom> page = postService.queryEnterprisePostByCondition(0, 0, condition);
        List postList = new ArrayList();
        for (EnterprisePostCustom enterprisePostCustom : page) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("postName", enterprisePostCustom.getPostName());
            String operationRule = enterprisePostCustom.getOperationRule();
            temp.put("operationRule", (operationRule == null || operationRule.equals("")) ? "-" : "已制定上传");
            postList.add(temp);
        }
        dataMap.put("postList", postList);
        return dataMap;
    }
}
