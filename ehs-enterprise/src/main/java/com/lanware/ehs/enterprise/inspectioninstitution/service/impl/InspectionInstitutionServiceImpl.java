package com.lanware.ehs.enterprise.inspectioninstitution.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.inspectioninstitution.service.InspectionInstitutionService;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.mapper.custom.EnterpriseInspectionInstitutionCustomMapper;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitutionExample;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import com.lanware.ehs.pojo.custom.EnterpriseInspectionInstitutionCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 检查机构管理interface实现类
 */
@Service
public class InspectionInstitutionServiceImpl implements InspectionInstitutionService {
    /** 注入enterpriseInspectionInstitutionCustomMapper的bean */
    @Autowired
    private EnterpriseInspectionInstitutionCustomMapper enterpriseInspectionInstitutionCustomMapper;
    /** 注入enterpriseRelationFileMapper的bean */
    @Autowired
    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    /**
     * @description 查询检查机构list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseInspectionInstitutions和totalNum的result
     */
    @Override
    public JSONObject listInspectionInstitutions(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseInspectionInstitutionCustom> enterpriseInspectionInstitutions
                = enterpriseInspectionInstitutionCustomMapper.listInspectionInstitutions(condition);
        result.put("enterpriseInspectionInstitutions", enterpriseInspectionInstitutions);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseInspectionInstitutionCustom> pageInfo
                    = new PageInfo<EnterpriseInspectionInstitutionCustom>(enterpriseInspectionInstitutions);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据机构类型获取检查机构result
     * @param type 机构类型
     * @param enterpriseId 当前企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    public JSONObject listInspectionInstitutionsByType(String type, Long enterpriseId) {
        JSONObject result = new JSONObject();
        EnterpriseInspectionInstitutionExample enterpriseInspectionInstitutionExample
                = new EnterpriseInspectionInstitutionExample();
        EnterpriseInspectionInstitutionExample.Criteria criteria
                = enterpriseInspectionInstitutionExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        criteria.andTypeEqualTo(type);
        List<EnterpriseInspectionInstitution> enterpriseInspectionInstitutions
                = enterpriseInspectionInstitutionCustomMapper.selectByExample(enterpriseInspectionInstitutionExample);
        result.put("enterpriseInspectionInstitutions", enterpriseInspectionInstitutions);
        return result;
    }

    /**
     * @description 根据id查询检查机构信息
     * @param id 检查机构id
     * @return com.lanware.management.pojo.EnterpriseInspection 返回EnterpriseInspection对象
     */
    @Override
    public EnterpriseInspectionInstitution getInspectionInstitutionById(Long id) {
        return enterpriseInspectionInstitutionCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增检查机构信息
     * @param enterpriseInspectionInstitution 检查机构pojo
     * @param contractFile 机构合同
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertInspectionInstitution(EnterpriseInspectionInstitution enterpriseInspectionInstitution
            , MultipartFile contractFile) {
        // 新增数据先保存，以便获取id
        enterpriseInspectionInstitutionCustomMapper.insert(enterpriseInspectionInstitution);
        // 上传并保存机构合同
        if (contractFile != null){
            String contract = "/" + enterpriseInspectionInstitution.getEnterpriseId()
                    + "/" + ModuleName.INSPECTIONINSTITUTION.getValue()
                    + "/" + enterpriseInspectionInstitution.getId() + "/" + contractFile.getOriginalFilename();
            try {
                ossTools.uploadStream(contract, contractFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传机构合同失败", e);
            }
            enterpriseInspectionInstitution.setContract(contract);
        }
        return enterpriseInspectionInstitutionCustomMapper.updateByPrimaryKeySelective(enterpriseInspectionInstitution);
    }

    /**
     * @description 更新检查机构信息
     * @param enterpriseInspectionInstitution 检查机构pojo
     * @param contractFile 机构合同
     * @return int 更新成功的数目
     */
    @Override
    public int updateInspectionInstitution(EnterpriseInspectionInstitution enterpriseInspectionInstitution
            , MultipartFile contractFile) {
        // 上传并保存机构合同
        if (contractFile != null) {
            // 上传了新的机构合同
            if (!StringUtils.isEmpty(enterpriseInspectionInstitution.getContract())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseInspectionInstitution.getContract());
            }
            String contract = "/" + enterpriseInspectionInstitution.getEnterpriseId()
                    + "/" + ModuleName.INSPECTIONINSTITUTION.getValue()
                    + "/" + enterpriseInspectionInstitution.getId() + "/" + contractFile.getOriginalFilename();
            try {
                ossTools.uploadStream(contract, contractFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传机构合同失败", e);
            }
            enterpriseInspectionInstitution.setContract(contract);
        }
        return enterpriseInspectionInstitutionCustomMapper.updateByPrimaryKeySelective(enterpriseInspectionInstitution);
    }

    /**
     * @description 删除检查机构信息
     * @param id 要删除的id
     * @param contract 要删除的机构合同路径
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteInspectionInstitution(Long id, String contract) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(contract);
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria= enterpriseRelationFileExample.createCriteria();
        criteria.andRelationIdEqualTo(id);
        criteria.andRelationModuleEqualTo("机构相关证书");
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        for (EnterpriseRelationFile enterpriseRelationFile : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        // 删除数据库中对应的数据
        enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        return enterpriseInspectionInstitutionCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除检查机构信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteInspectionInstitutions(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseInspectionInstitution enterpriseInspectionInstitution
                    = enterpriseInspectionInstitutionCustomMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseInspectionInstitution.getContract());
        }
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria= enterpriseRelationFileExample.createCriteria();
        criteria.andRelationIdIn(Arrays.asList(ids));
        criteria.andRelationModuleEqualTo("机构相关证书");
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        for (EnterpriseRelationFile enterpriseRelationFile : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        // 删除数据库中对应的数据
        enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        EnterpriseInspectionInstitutionExample enterpriseInspectionInstitutionExample
                = new EnterpriseInspectionInstitutionExample();
        EnterpriseInspectionInstitutionExample.Criteria criteria1
                = enterpriseInspectionInstitutionExample.createCriteria();
        criteria1.andIdIn(Arrays.asList(ids));
        return enterpriseInspectionInstitutionCustomMapper.deleteByExample(enterpriseInspectionInstitutionExample);
    }
}
