package com.lanware.ehs.enterprise.servicerecord.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseServiceRecord;
import com.lanware.ehs.pojo.EquipmentServiceUse;
import com.lanware.ehs.pojo.custom.EnterpriseServiceRecordCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 维修记录interface
 */
public interface ServiceRecordService {
    /**
     * @description 查询维修记录list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    JSONObject listServiceRecords(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询维修记录
     * @param id 维修记录id
     * @return com.lanware.management.pojo.EnterpriseServiceRecord 返回EnterpriseServiceRecord对象
     */
    EnterpriseServiceRecord getServiceRecordById(Long id);

    /**
     * @description 新增维修记录
     * @param enterpriseServiceRecord 维修记录pojo
     * @return int 插入成功的数目
     */
    int insertServiceRecord(EnterpriseServiceRecord enterpriseServiceRecord);

    /**
     * @description 更新维修记录
     * @param enterpriseServiceRecord 维修记录pojo
     * @return int 更新成功的数目
     */
    int updateServiceRecord(EnterpriseServiceRecord enterpriseServiceRecord);

    /**
     * @description 删除维修记录
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteServiceRecord(Long id);

    /**
     * @description 批量删除维修记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteServiceRecords(Long[] ids);

    /**
     * @description 根据记录id获取维护设备
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listServiceEquipmentsByRecordId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据记录id过滤尚未维护的设备
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listSelectServiceEquipmentsByRecordId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 批量插入维护的设备集合
     * @param equipmentServiceUseList 入维护的设备集合
     * @return int 插入成功的数目
     */
    int insertBatch(List<EquipmentServiceUse> equipmentServiceUseList);

    /**
     * @description 删除维护的设备
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteServiceEquipment(Long id);

    /**
     * @description 批量删除维护的设备
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteServiceEquipments(Long[] ids);

    List<EnterpriseServiceRecordCustom> listServiceRecordAccounts(Map<String, Object> condition);
}
