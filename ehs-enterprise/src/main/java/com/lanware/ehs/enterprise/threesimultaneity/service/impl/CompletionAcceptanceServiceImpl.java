package com.lanware.ehs.enterprise.threesimultaneity.service.impl;

import com.lanware.ehs.enterprise.threesimultaneity.service.CompletionAcceptanceService;
import com.lanware.ehs.mapper.custom.EnterpriseThreeSimultaneityFileCustomMapper;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 三同时管理预评价阶段interface实现类
 */
@Service
public class CompletionAcceptanceServiceImpl implements CompletionAcceptanceService {
    /** 注入enterpriseThreeSimultaneityFileCustomMapper的bean */
    @Autowired
    private EnterpriseThreeSimultaneityFileCustomMapper enterpriseThreeSimultaneityFileCustomMapper;
    
    /**
     * @description 根据项目id和阶段查询三同时管理文件list
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile> 三同时管理文件list
     */
    @Override
    public List<EnterpriseThreeSimultaneityFile> listCompletionAcceptances(Long projectId, Integer projectState) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("projectId", projectId);
        condition.put("projectState", projectState);
        return enterpriseThreeSimultaneityFileCustomMapper.listThreeSimultaneityFiles(condition);
    }
}
