package com.lanware.ehs.enterprise.notify.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.notify.service.EnterpriseDiseaseDiagnosiNotifyService;
import com.lanware.ehs.pojo.custom.EnterpriseDiseaseDiagnosiNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("diseaseDiagnosiNotify")
public class EnterpriseDiseaseDiagnosiNotifyController extends BaseController {
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EnterpriseDiseaseDiagnosiNotifyService enterpriseDiseaseDiagnosiNotifyService;
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EnterpriseBaseinfoService enterpriseBaseinfoService;

    /**
     * 获取待职业病诊断结果告知列表
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("waitList")
    public EhsResult userList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //查询未告知数据
        condition.put("status",0);
        //排序条件
        condition.put("orderByClause","id");
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.enterpriseDiseaseDiagnosiNotifyService.findEnterpriseDiseaseDiagnosiWaitNotifyList(condition,request));
        return EhsResult.ok(dataTable);
    }

      /**
     * 保存职业病诊断结果告知
     * @return
     */
    @RequestMapping("diseaseDiagnosiAdd")
    @ResponseBody
    public EhsResult addEnterpriseProtectArticles(String enterpriseDiseaseDiagnosiCustoms, MultipartFile notifyFile,@CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseDiseaseDiagnosiNotifyCustom enterpriseDiseaseDiagnosiNotifyCustom = JSONObject.parseObject(enterpriseDiseaseDiagnosiCustoms, EnterpriseDiseaseDiagnosiNotifyCustom.class);
        enterpriseDiseaseDiagnosiNotifyCustom.setEnterpriseId(currentUser.getEnterpriseId());
        this.enterpriseDiseaseDiagnosiNotifyService.saveEnterpriseDiseaseDiagnosiNotify(enterpriseDiseaseDiagnosiNotifyCustom,notifyFile);
        return EhsResult.ok("");
    }
    /**
     * 删除职业病诊断结果告知
     * @return
     */
    @RequestMapping("deleteDiseaseDiagnosi")
    public EhsResult addEnterpriseProtectArticles(Long id, @CurrentUser EnterpriseUserCustom currentUser) {
        this.enterpriseDiseaseDiagnosiNotifyService.deleteEnterpriseDiseaseDiagnosiNotify(id);
        return EhsResult.ok("");
    }
    /**
     * 获取待职业病诊断结果告知列表
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("diseaseDiagnosiList")
    public EhsResult workList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //排序条件
        condition.put("orderByClause","mrw.gmt_create");
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.enterpriseDiseaseDiagnosiNotifyService.findEnterpriseDiseaseDiagnosiNotifyList(condition,request));
        return EhsResult.ok(dataTable);
    }
}
