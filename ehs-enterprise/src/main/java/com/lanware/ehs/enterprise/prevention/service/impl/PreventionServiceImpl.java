package com.lanware.ehs.enterprise.prevention.service.impl;

import com.lanware.ehs.enterprise.prevention.service.PreventionService;
import com.lanware.ehs.mapper.EnterpriseManageFileMapper;
import com.lanware.ehs.mapper.custom.EnterpriseManageFileCustomMapper;
import com.lanware.ehs.pojo.EnterpriseManageFile;
import com.lanware.ehs.pojo.EnterpriseManageFileExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
/**
 * @description 职业健康防治制度interface实现类
 */
public class PreventionServiceImpl implements PreventionService {
    @Autowired
    /** 注入enterpriseManageFileMapper的bean */
    private EnterpriseManageFileMapper enterpriseManageFileMapper;
    @Autowired
    /** 注入enterpriseManageFileCustomMapper的bean */
    private EnterpriseManageFileCustomMapper enterpriseManageFileCustomMapper;

    @Override
    /**
     * @description 插入企业管理文件信息
     * @param enterpriseManageFile 企业管理文件pojo
     * @return 插入成功的数目
     */
    public int insertEnterpriseManageFile(EnterpriseManageFile enterpriseManageFile) {
        return enterpriseManageFileMapper.insert(enterpriseManageFile);
    }

    @Override
    /**
     * @description 根据type更新企业管理文件信息
     * @param enterpriseManageFile 企业管理文件pojo
     * @param type 企业管理文件类型
     * @param enterpriseId 企业id
     * @return 更新成功的数目
     */
    public int updateEnterpriseManageFile(EnterpriseManageFile enterpriseManageFile, String type, Long enterpriseId) {
        EnterpriseManageFileExample enterpriseManageFileExample = new EnterpriseManageFileExample();
        EnterpriseManageFileExample.Criteria criteria = enterpriseManageFileExample.createCriteria();
        criteria.andFileTypeEqualTo(type);
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        return enterpriseManageFileMapper.updateByExampleSelective(enterpriseManageFile, enterpriseManageFileExample);
    }

    @Override
    /**
     * @description 根据条件查询职业健康防范制度文件list
     * @param condition 查询条件
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseManageFile> 企业管理文件list
     */
    public List<EnterpriseManageFile> listPreventionFiles(Map<String, Object> condition) {
        return enterpriseManageFileCustomMapper.listManageFiles(condition);
    }

    /**
     * @description 根据文件路径删除对应的文件对象
     * @param filePath 文件路径
     * @return 删除成功的数目
     */
    @Override
    public int deleteFileByFilePath(String filePath) {
        EnterpriseManageFileExample enterpriseManageFileExample = new EnterpriseManageFileExample();
        EnterpriseManageFileExample.Criteria criteria = enterpriseManageFileExample.createCriteria();
        criteria.andFilePathEqualTo(filePath);
        return enterpriseManageFileMapper.deleteByExample(enterpriseManageFileExample);
    }

    /**
     * @description 根据企业id查询企业管理文件
     * @param enterpriseId 企业id
     * @return 企业管理文件
     */
    @Override
    public List<EnterpriseManageFile> getRescue(Long enterpriseId) {
        EnterpriseManageFileExample enterpriseManageFileExample = new EnterpriseManageFileExample();
        EnterpriseManageFileExample.Criteria criteria = enterpriseManageFileExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        criteria.andFileTypeEqualTo("职业病危害应急救援与管理制度");
        return enterpriseManageFileMapper.selectByExample(enterpriseManageFileExample);
    }
}
