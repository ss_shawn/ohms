package com.lanware.ehs.enterprise.check.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.check.service.SchematicDiagramService;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.mapper.custom.EnterpriseRelationFileCustomMapper;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import com.lanware.ehs.pojo.custom.EnterpriseRelationFileCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 监测、检测点示意图interface实现类
 */
@Service
public class SchematicDiagramServiceImpl implements SchematicDiagramService {
    /** 注入enterpriseRelationFileMapper的bean */
    @Autowired
    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;
    /** 注入enterpriseRelationFileCustomMapper的bean */
    @Autowired
    private EnterpriseRelationFileCustomMapper enterpriseRelationFileCustomMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    /**
     * @description 查询监测、检测点示意图list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseRelationFiles和totalNum的result
     */
    @Override
    public JSONObject listSchematicDiagrams(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseRelationFileCustom> enterpriseRelationFileCustoms
                = enterpriseRelationFileCustomMapper.queryEnterpriseRelationFileByCondition(condition);
        result.put("enterpriseRelationFiles", enterpriseRelationFileCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseRelationFileCustom> pageInfo
                    = new PageInfo<EnterpriseRelationFileCustom>(enterpriseRelationFileCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询监测、检测点示意图
     * @param id
     * @return EnterpriseRelationFile
     */
    @Override
    public EnterpriseRelationFile getSchematicDiagramById(Long id) {
        return enterpriseRelationFileMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增监测、检测点示意图，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param schematicDiagram 监测、检测点示意图
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    @Transactional
    public int insertSchematicDiagram(EnterpriseRelationFile enterpriseRelationFile, MultipartFile schematicDiagram) {
        // 新增数据先保存，以便获取id
        enterpriseRelationFileMapper.insert(enterpriseRelationFile);
        // 上传并保存监测、检测点示意图
        if (schematicDiagram != null){
            String schematic = "/" + enterpriseRelationFile.getRelationId() + "/" + ModuleName.CHECK.getValue()
                    + "/" + enterpriseRelationFile.getId() + "/" + schematicDiagram.getOriginalFilename();
            try {
                ossTools.uploadStream(schematic, schematicDiagram.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传监测、检测点示意图", e);
            }
            enterpriseRelationFile.setFileName(schematicDiagram.getOriginalFilename());
            enterpriseRelationFile.setFilePath(schematic);
        }
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
    }

    /**
     * @description 更新监测、检测点示意图，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param schematicDiagram 监测、检测点示意图
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    public int updateSchematicDiagram(EnterpriseRelationFile enterpriseRelationFile, MultipartFile schematicDiagram) {
        // 上传并保存监测、检测点示意图
        if (schematicDiagram != null){
            // 上传了新的职业病危害警示文件
            if (!StringUtils.isEmpty(enterpriseRelationFile.getFilePath())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
            }
            String schematic = "/" + enterpriseRelationFile.getRelationId() + "/" + ModuleName.CHECK.getValue()
                    + "/" + enterpriseRelationFile.getId() + "/" + schematicDiagram.getOriginalFilename();
            try {
                ossTools.uploadStream(schematic, schematicDiagram.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传监测、检测点示意图", e);
            }
            enterpriseRelationFile.setFileName(schematicDiagram.getOriginalFilename());
            enterpriseRelationFile.setFilePath(schematic);
        }
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
    }

    /**
     * @description 删除监测、检测点示意图
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    @Override
    public int deleteSchematicDiagram(Long id, String filePath) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(filePath);
        // 删除数据库中对应的数据
        return enterpriseRelationFileMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除监测、检测点示意图
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteSchematicDiagrams(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseRelationFile enterpriseRelationFile = enterpriseRelationFileMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria = enterpriseRelationFileExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
    }
}
