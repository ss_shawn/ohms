package com.lanware.ehs.enterprise.file.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.file.service.FileService;
import com.lanware.ehs.mapper.custom.EnterpriseFileCustomMapper;
import com.lanware.ehs.pojo.EnterpriseFile;
import com.lanware.ehs.pojo.EnterpriseFileExample;
import com.lanware.ehs.pojo.custom.EnterpriseFileCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 文件管理interface实现类
 */
@Service
public class FileServiceImpl implements FileService {
    /** 注入enterpriseFilesCustomMapper的bean */
    @Autowired
    private EnterpriseFileCustomMapper enterpriseFileCustomMapper;

    /**
     * @description 查询文件管理list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseFiles和totalNum的result
     */
    @Override
    public JSONObject listFiles(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseFileCustom> enterpriseFiles = enterpriseFileCustomMapper.listFiles(condition);
        result.put("enterpriseFiles", enterpriseFiles);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseFileCustom> pageInfo = new PageInfo<EnterpriseFileCustom>(enterpriseFiles);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询文件信息
     * @param id 文件信息id
     * @return com.lanware.management.pojo.EnterpriseFiles 返回EnterpriseFiles对象
     */
    @Override
    public EnterpriseFile getFileById(Long id) {
        return enterpriseFileCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增文件信息
     * @param enterpriseFile 文件信息pojo
     * @return int 插入成功的数目
     */
    @Override
    public int insertFile(EnterpriseFile enterpriseFile) {
        return enterpriseFileCustomMapper.insert(enterpriseFile);
    }

    /**
     * @description 更新文件信息
     * @param enterpriseFile 文件信息pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateFile(EnterpriseFile enterpriseFile) {
        return enterpriseFileCustomMapper.updateByPrimaryKeySelective(enterpriseFile);
    }

    /**
     * @description 删除文件信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    public int deleteFile(Long id) {
        return enterpriseFileCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除文件信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteFiles(Long[] ids) {
        EnterpriseFileExample enterpriseFileExample = new EnterpriseFileExample();
        EnterpriseFileExample.Criteria criteria = enterpriseFileExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseFileCustomMapper.deleteByExample(enterpriseFileExample);
    }
}
