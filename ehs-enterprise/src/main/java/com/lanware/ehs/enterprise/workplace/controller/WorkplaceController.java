package com.lanware.ehs.enterprise.workplace.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.workplace.service.WorkplaceService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.EnterpriseWorkplace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/workplace")
/**
 * @description 作业场所管理controller
 */
public class WorkplaceController {
    @Autowired
    /** 注入workplaceService的bean */
    private WorkplaceService workplaceService;

    @RequestMapping("")
    /**
     * @description 返回作业场所管理页面
     * @param
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "workplace/list";
    }

    @RequestMapping("/listWorkplaces")
    @ResponseBody
    /**
     * @description 获取作业场所result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listWorkplaces(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = workplaceService.listWorkplacesByEnterpriseId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listHarmWorkplaces")
    @ResponseBody
    /**
     * @description 获取有职业病危害因素的作业场所result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listHarmWorkplaces(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = workplaceService.listHarmWorkplaces(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editWorkplace/save")
    @ResponseBody
    /**
     * @description 保存作业场所信息
     * @param enterpriseWorkplaceJSON 作业场所JSON
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseWorkplaceJSON, @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseWorkplace enterpriseWorkplace
                = JSONObject.parseObject(enterpriseWorkplaceJSON, EnterpriseWorkplace.class);
        if (enterpriseWorkplace.getId() == null) {
            enterpriseWorkplace.setGmtCreate(new Date());
            enterpriseWorkplace.setEnterpriseId(enterpriseUser.getEnterpriseId());
            if (workplaceService.insertWorkplace(enterpriseWorkplace) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseWorkplace.setGmtModified(new Date());
            if (workplaceService.updateWorkplace(enterpriseWorkplace) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/viewWorkplace")
    /**
     * @description 查看作业场所信息
     * @param id 作业场所id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewWorkplace(Long id) {
        JSONObject data = new JSONObject();
        EnterpriseWorkplace enterpriseWorkplace = workplaceService.getWorkplaceById(id);
        data.put("enterpriseWorkplace", enterpriseWorkplace);
        return new ModelAndView("workplace/view", data);
    }

    @RequestMapping("/deleteWorkplace")
    @ResponseBody
    /**
     * @description 删除作业场所信息
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteWorkplace(Long id) {
        if (workplaceService.deleteWorkplace(id) > 0) {
            return ResultFormat.success("删除成功");
        } else {
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteWorkplaces")
    @ResponseBody
    /**
     * @description 批量删除作业场所信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteWorkplaces(Long[] ids) {
        if (workplaceService.deleteWorkplaces(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        } else {
            return ResultFormat.error("删除成功");
        }
    }
}
