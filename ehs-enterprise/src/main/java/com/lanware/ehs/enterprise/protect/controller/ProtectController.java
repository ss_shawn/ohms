package com.lanware.ehs.enterprise.protect.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.protect.service.ProtectService;
import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("protect")
public class ProtectController  extends BaseController {
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private ProtectService protectService;

    /**
     * 获取防护用品列表
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("list")
    public EhsResult userList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //排序
        condition.put("orderByClause"," id desc");
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.protectService.findProtectList(condition,request));
        return EhsResult.ok(dataTable);
    }

    /**
     * 获取防护危害因素列表
     * @return
     */
    @RequestMapping("harmFactor")
    public EhsResult harmFactorList(String keyword, @CurrentUser EnterpriseUserCustom currentUser) {
        List<BaseHarmFactor> baseHarmFactors=this.protectService.getAllBaseHarmFactor(keyword);
        return EhsResult.ok(baseHarmFactors);
    }
    /**
     * 保存防护用品
     * @return
     */
    @RequestMapping("protectAdd")
    public EhsResult addEnterpriseProtectArticles(String protectCustoms, MultipartFile image,MultipartFile certificatePath, @CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseProtectArticlesCustom enterpriseProtectArticlesCustom= JSONObject.parseObject(protectCustoms, EnterpriseProtectArticlesCustom.class);
        enterpriseProtectArticlesCustom.setEnterpriseId(currentUser.getEnterpriseId());
        this.protectService.saveEnterpriseProtectArticles(enterpriseProtectArticlesCustom,image,certificatePath);
        return EhsResult.ok("");
    }
    /**
     * 删除防护用品
     * @return
     */
    @RequestMapping("deleteProtect")
    public EhsResult addEnterpriseProtectArticles(Long id, @CurrentUser EnterpriseUserCustom currentUser) {
        int n=this.protectService.deleteEnterpriseProtectArticles(id);
        return EhsResult.ok(n);
    }

    /**
     * 上传合格证书
     * @return
     */
    @RequestMapping("uploadCertificate")
    public EhsResult uploadCertificate(Long id, MultipartFile certificate) {
        this.protectService.uploadCertificate(id,certificate);
        return EhsResult.ok("");
    }
    /**
     * 上传图片
     * @return
     */
    @RequestMapping("uploadImage")
    public EhsResult uploadImage(Long id, MultipartFile image) {
        this.protectService.uploadImage(id,image);
        return EhsResult.ok("");
    }
}
