package com.lanware.ehs.enterprise.employee.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.enterprise.employee.service.PostHistoryService;
import com.lanware.ehs.mapper.EmployeePostMapper;
import com.lanware.ehs.mapper.EnterpriseWaitProvideProtectArticlesMapper;
import com.lanware.ehs.mapper.custom.EmployeePostCustomMapper;
import com.lanware.ehs.pojo.EnterpriseWaitProvideProtectArticlesExample;
import com.lanware.ehs.pojo.custom.EmployeePostCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PostHistoryServiceImpl implements PostHistoryService {

    private EmployeePostCustomMapper employeePostCustomMapper;
    @Autowired
    private EmployeePostMapper employeePostMapper;
    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private EnterpriseWaitProvideProtectArticlesMapper enterpriseWaitProvideProtectArticlesMapper;

    @Autowired
    public PostHistoryServiceImpl(EmployeePostCustomMapper employeePostCustomMapper) {
        this.employeePostCustomMapper = employeePostCustomMapper;
    }

    @Override
    public Page<EmployeePostCustom> queryEmployeePostByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<EmployeePostCustom> page = PageHelper.startPage(pageNum, pageSize);
        employeePostCustomMapper.queryEmployeePostByCondition(condition);
        return page;
    }

    @Override
    public EmployeePostCustom queryEmployeePostById(Long id) {
        return employeePostCustomMapper.queryEmployeePostById(id);
    }
    @Override
    @Transactional
    public int deleteEmployeePostById( List<Long> ids) {
        int num=0;
        for(Long id:ids){
            EnterpriseWaitProvideProtectArticlesExample enterpriseWaitProvideProtectArticlesExample=new EnterpriseWaitProvideProtectArticlesExample();
            enterpriseWaitProvideProtectArticlesExample.createCriteria().andEmployeePostIdEqualTo(id);
            long n=enterpriseWaitProvideProtectArticlesMapper.countByExample(enterpriseWaitProvideProtectArticlesExample);
            if (n>0){
                return -8;
            }
            num=num+employeePostMapper.deleteByPrimaryKey(id);
        }
        return num;
    }
}
