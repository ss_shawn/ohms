package com.lanware.ehs.enterprise.remind.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.EnterpriseRemind;

import java.util.Date;
import java.util.List;

/**
 * 提醒service
 */
public interface RemindService {
    /**
     * 查找提醒信息
     * @param remind
     * @param request
     * @return
     */
    Page<EnterpriseRemind> findRemeind(EnterpriseRemind remind, QueryRequest request);


    /**
     * 查找提醒信息
     * @param remind
     * @return
     */
    List<EnterpriseRemind> findRemeind(EnterpriseRemind remind);


    /**
     * 通过ID更新数据
     * @param remind
     */
    void updateById(EnterpriseRemind remind);

    /**
     * 通过提醒时间区间查找提醒内容
     * @param startDate
     * @param endDate
     * @return
     */
    List<EnterpriseRemind> findRemindByRemindDate(String type,String planDate,Long enterpriseId,Date startDate, Date endDate);

    /**
     * 新增提醒内容
     * @param remind
     */
    void insert(EnterpriseRemind remind);
}
