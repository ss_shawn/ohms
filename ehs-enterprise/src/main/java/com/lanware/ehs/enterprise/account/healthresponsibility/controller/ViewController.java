package com.lanware.ehs.enterprise.account.healthresponsibility.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 职业健康管理责任制台帐视图
 */
@Controller("healthResponsibilityAccountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @RequestMapping("healthResponsibility/certificateDetail/{relationId}")
    /**
     * @description 返回资质证书详细页面
     * @param relationId 关联id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView certificateDetail(@PathVariable Long relationId){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        return new ModelAndView("account/healthResponsibility/certificateDetail", data);
    }

    @RequestMapping("healthResponsibility/correspondFileDetail/{relationId}/{relationModule}")
    /**
     * @description 返回往来文件详细页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView correspondFileDetail(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/healthResponsibility/correspondFileDetail", data);
    }
}
