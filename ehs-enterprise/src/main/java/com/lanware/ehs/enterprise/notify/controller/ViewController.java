package com.lanware.ehs.enterprise.notify.controller;

import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.notify.service.EmployeeWorkNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseDiseaseDiagnosiNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseMedicalResultNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseWorkplaceNotifyService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 告知管理视图
 */
@Controller("notifyView")
public class ViewController {
    @Autowired
    EmployeeWorkNotifyService employeeWorkNotifyService;
    @Autowired
    EnterpriseWorkplaceNotifyService enterpriseWorkplaceNotifyService;
    @Autowired
    EnterpriseMedicalResultNotifyService enterpriseMedicalResultNotifyService;

    @Autowired
    EnterpriseDiseaseDiagnosiNotifyService enterpriseDiseaseDiagnosiNotifyService;


    /**
     * @description 待岗前告知管理
     * @param enterpriseUser 当前登录企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @GetMapping(Constants.VIEW_PREFIX + "workNotify")
    public String protect( @CurrentUser EnterpriseUser enterpriseUser) {

        return "notify/workNotify/workNotify";
    }
    /**
     * 进入岗前告知新建页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"workNotify/add/{id}")
    public String protectAdd( @PathVariable Long id ,@CurrentUser EnterpriseUserCustom currentUser, Model model) {
        EmployeeWorkNotifyCustom employeeWorkNotifyCustom=employeeWorkNotifyService.getEmployeeWorkNotifyCustomByWaitID(id);
        employeeWorkNotifyCustom.setWaitWorkNotifyId(id);
        employeeWorkNotifyCustom.setNotifyDate(new Date());
        model.addAttribute("employeeWorkNotify",employeeWorkNotifyCustom);
        return "notify/workNotify/workNotifyAdd";
    }
    /**
     * 进入岗前告知修改页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"workNotify/update/{id}")
    public String protectUpdate(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EmployeeWorkNotifyCustom employeeWorkNotifyCustom=employeeWorkNotifyService.getEmployeeWorkNotifyCustomByID(id);
        model.addAttribute("employeeWorkNotify",employeeWorkNotifyCustom);
        return "notify/workNotify/workNotifyAdd";
    }
    /**
     * 进入岗前告知查看页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"workNotify/view/{id}")
    public String viewProtect(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EmployeeWorkNotifyCustom employeeWorkNotifyCustom=employeeWorkNotifyService.getEmployeeWorkNotifyCustomByID(id);
        model.addAttribute("employeeWorkNotify",employeeWorkNotifyCustom);
        return "notify/workNotify/workNotifyView";
    }


    /**
     * @description 待作业场所告知管理
     * @param enterpriseUser 当前登录企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @GetMapping(Constants.VIEW_PREFIX + "workplaceNotify")
    public String workplacetNotify( @CurrentUser EnterpriseUser enterpriseUser) {

        return "notify/workplaceNotify/workplaceNotify";
    }
    /**
     * 进入作业场所告知新建页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"workplaceNotify/add/{id}")
    public String workplacetNotifyAdd( @PathVariable Long id ,@CurrentUser EnterpriseUserCustom currentUser, Model model) {
        EnterpriseWorkplaceNotifyCustom enterpriseWorkplaceNotifyCustom=enterpriseWorkplaceNotifyService.getEnterpriseWorkplaceNotifyCustomByWaitID(id);
        enterpriseWorkplaceNotifyCustom.setWorkplaceWaitNotifyId(id);
        enterpriseWorkplaceNotifyCustom.setNotifyDate(new Date());
        model.addAttribute("enterpriseWorkplaceNotify",enterpriseWorkplaceNotifyCustom);
        return "notify/workplaceNotify/workplaceNotifyAdd";
    }
    /**
     * 进入作业场所告知修改页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"workplaceNotify/update/{id}")
    public String workplacetNotifyUpdate(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseWorkplaceNotifyCustom enterpriseWorkplaceNotifyCustom=enterpriseWorkplaceNotifyService.getEnterpriseWorkplaceNotifyCustomByID(id);
        model.addAttribute("enterpriseWorkplaceNotify",enterpriseWorkplaceNotifyCustom);
        return "notify/workplaceNotify/workplaceNotifyAdd";
    }
    /**
     * 进入作业场所告知查看页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"workplaceNotify/view/{id}")
    public String workplacetNotifyView(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseWorkplaceNotifyCustom enterpriseWorkplaceNotifyCustom=enterpriseWorkplaceNotifyService.getEnterpriseWorkplaceNotifyCustomByID(id);
        model.addAttribute("enterpriseWorkplaceNotify",enterpriseWorkplaceNotifyCustom);
        return "notify/workplaceNotify/workplaceNotifyView";
    }



    /**
     * @description 待体检结果告知管理
     * @param enterpriseUser 当前登录企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @GetMapping(Constants.VIEW_PREFIX + "medicalResultNotify")
    public String medicalResultNotify( @CurrentUser EnterpriseUser enterpriseUser) {

        return "notify/medicalResultNotify/medicalResultNotify";
    }
    /**
     * 进入体检结果告知新建页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"medicalResultNotify/add/{id}")
    public String medicalResultAdd( @PathVariable Long id ,@CurrentUser EnterpriseUserCustom currentUser, Model model) {
        EnterpriseMedicalResultNotifyCustom enterpriseMedicalResultNotifyCustom=enterpriseMedicalResultNotifyService.getEnterpriseMedicalResultNotifyCustomByWaitID(id);
        enterpriseMedicalResultNotifyCustom.setWaitMedicalNotifyId(id);
        enterpriseMedicalResultNotifyCustom.setNotifyDate(new Date());
        model.addAttribute("enterpriseMedicalResult",enterpriseMedicalResultNotifyCustom);
        return "notify/medicalResultNotify/medicalResultNotifyAdd";
    }
    /**
     * 进入体检结果告知修改页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"medicalResultNotify/update/{id}")
    public String medicalResultUpdate(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseMedicalResultNotifyCustom enterpriseMedicalResultNotifyCustom=enterpriseMedicalResultNotifyService.getEnterpriseMedicalResultNotifyCustomByID(id);
        model.addAttribute("enterpriseMedicalResult",enterpriseMedicalResultNotifyCustom);
        return "notify/medicalResultNotify/medicalResultNotifyAdd";
    }
    /**
     * 进入体检结果知查看页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"medicalResultNotify/view/{id}")
    public String medicalResultNotifyView(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseMedicalResultNotifyCustom enterpriseMedicalResultNotifyCustom=enterpriseMedicalResultNotifyService.getEnterpriseMedicalResultNotifyCustomByID(id);
        model.addAttribute("enterpriseMedicalResult",enterpriseMedicalResultNotifyCustom);
        return "notify/medicalResultNotify/medicalResultNotifyView";
    }



    /**
     * @description 待职业病诊断告知管理
     * @param enterpriseUser 当前登录企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @GetMapping(Constants.VIEW_PREFIX + "diseaseDiagnosiNotify")
    public String diseaseDiagnosiNotify( @CurrentUser EnterpriseUser enterpriseUser) {

        return "notify/diseaseDiagnosiNotify/diseaseDiagnosiNotify";
    }
    /**
     * 进入职业病诊断告知新建页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"diseaseDiagnosiNotify/add/{id}")
    public String diseaseDiagnosiNotifyAdd( @PathVariable Long id ,@CurrentUser EnterpriseUserCustom currentUser, Model model) {
        EnterpriseDiseaseDiagnosiNotifyCustom enterpriseDiseaseDiagnosiNotifyCustom=enterpriseDiseaseDiagnosiNotifyService.getEnterpriseDiseaseDiagnosiNotifyCustomByWaitID(id);
        enterpriseDiseaseDiagnosiNotifyCustom.setDiseaseDiagnosiWaitId(id);
        enterpriseDiseaseDiagnosiNotifyCustom.setNotifyDate(new Date());
        model.addAttribute("enterpriseDiseaseDiagnosi",enterpriseDiseaseDiagnosiNotifyCustom);
        return "notify/diseaseDiagnosiNotify/diseaseDiagnosiNotifyAdd";
    }
    /**
     * 进入职业病诊断告知修改页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"diseaseDiagnosiNotify/update/{id}")
    public String diseaseDiagnosiNotifyUpdate(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseDiseaseDiagnosiNotifyCustom enterpriseDiseaseDiagnosiNotifyCustom=enterpriseDiseaseDiagnosiNotifyService.getEnterpriseDiseaseDiagnosiNotifyCustomByID(id);
        model.addAttribute("enterpriseDiseaseDiagnosi",enterpriseDiseaseDiagnosiNotifyCustom);
        return "notify/diseaseDiagnosiNotify/diseaseDiagnosiNotifyAdd";
    }
    /**
     * 进入职业病诊断知查看页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX +"diseaseDiagnosiNotify/view/{id}")
    public String diseaseDiagnosiNotifyView(@PathVariable Long id , @CurrentUser EnterpriseUserCustom currentUser, Model model, HttpServletRequest request) {
        EnterpriseDiseaseDiagnosiNotifyCustom enterpriseDiseaseDiagnosiNotifyCustom=enterpriseDiseaseDiagnosiNotifyService.getEnterpriseDiseaseDiagnosiNotifyCustomByID(id);
        model.addAttribute("enterpriseDiseaseDiagnosi",enterpriseDiseaseDiagnosiNotifyCustom);
        return "notify/diseaseDiagnosiNotify/diseaseDiagnosiNotifyView";
    }
}
