package com.lanware.ehs.enterprise.remind.controller;

import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.remind.service.RemindService;
import com.lanware.ehs.pojo.EnterpriseRemind;
import com.lanware.ehs.pojo.custom.EnterpriseRemindCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 提醒Controller
 */
@Controller
@RequestMapping("/remind")
@Slf4j
public class RemindController {
    @Autowired
    private RemindService remindService;
    /**
     *
     * 完成提醒内容
     * @param id
     * @return
     */
    @PostMapping("complete/{id}")
    @ResponseBody
    public EhsResult completeRemind(@PathVariable Long id){
        try {
            EnterpriseRemind remind = new EnterpriseRemind();
            remind.setId(id);
            remind.setStatus(EnterpriseRemindCustom.STATUS_YWC);
            remindService.updateById(remind);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "完成提醒失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }

    /**
     *
     * 忽略提醒内容
     * @param id
     * @return
     */
    @PostMapping("ignore/{id}")
    @ResponseBody
    public EhsResult ignoreRemind(@PathVariable Long id){
        try {
            EnterpriseRemind remind = new EnterpriseRemind();
            remind.setId(id);
            remind.setStatus(EnterpriseRemindCustom.STATUS_YHL);
            remindService.updateById(remind);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "忽略提醒失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }

}
