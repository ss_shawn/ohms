package com.lanware.ehs.enterprise.file.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseFile;

import java.util.Map;

/**
 * @description 文件管理interface
 */
public interface FileService {
    /**
     * @description 查询文件管理list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    JSONObject listFiles(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询文件信息
     * @param id 文件信息id
     * @return com.lanware.management.pojo.EnterpriseFile 返回EnterpriseFile对象
     */
    EnterpriseFile getFileById(Long id);

    /**
     * @description 新增文件信息
     * @param enterpriseFile 文文件信息pojo
     * @return int 插入成功的数目
     */
    int insertFile(EnterpriseFile enterpriseFile);

    /**
     * @description 更新文件信息
     * @param enterpriseFile 文件信息pojo
     * @return int 更新成功的数目
     */
    int updateFile(EnterpriseFile enterpriseFile);

    /**
     * @description 删除文件信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteFile(Long id);

    /**
     * @description 批量删除文件信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteFiles(Long[] ids);
}
