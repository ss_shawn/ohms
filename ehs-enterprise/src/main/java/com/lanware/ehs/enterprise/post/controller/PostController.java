package com.lanware.ehs.enterprise.post.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.post.service.PostService;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterprisePostCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.service.EnterprisePostService;
import com.lanware.ehs.service.EnterpriseWorkplaceService;
import com.lanware.ehs.service.PostHarmFactorService;
import com.lanware.ehs.service.PostProtectArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/post")
public class PostController {

    private EnterprisePostService enterprisePostService;

    private EnterpriseWorkplaceService enterpriseWorkplaceService;

    private PostProtectArticlesService postProtectArticlesService;

    private PostHarmFactorService postHarmFactorService;

    private PostService postService;

    @Autowired
    public PostController(EnterprisePostService enterprisePostService, EnterpriseWorkplaceService enterpriseWorkplaceService, PostProtectArticlesService postProtectArticlesService, PostHarmFactorService postHarmFactorService, PostService postService) {
        this.enterprisePostService = enterprisePostService;
        this.enterpriseWorkplaceService = enterpriseWorkplaceService;
        this.postProtectArticlesService = postProtectArticlesService;
        this.postHarmFactorService = postHarmFactorService;
        this.postService = postService;
    }

    /**
     * 岗位管理列表页面
     * @return 岗位管理列表model
     */
    @RequestMapping("")
    public ModelAndView post(){
        return new ModelAndView("post/list");
    }

    /**
     * 分页查询岗位
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @param existRule 是否存在岗位操作规程
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public ResultFormat getList(@CurrentUser EnterpriseUser enterpriseUser, Integer pageNum, Integer pageSize
            , String keyword, String sortField, String sortType, String existRule){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("keyword", keyword);
        condition.put("existRule", existRule);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<EnterprisePostCustom> page = postService.queryEnterprisePostByCondition(pageNum, pageSize, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }

    /**
     * 编辑岗位页面
     * @param id 岗位id
     * @return 编辑岗位model
     */
    @RequestMapping("/edit")
    public ModelAndView edit(@CurrentUser EnterpriseUser enterpriseUser,  Long id){
        JSONObject data = new JSONObject();
        if (id != null){

                EnterprisePostCustom enterprisePost = postService.queryEnterprisePostById(id);
                data.put("enterprisePost", enterprisePost);
        }

        return new ModelAndView("post/edit", data);
    }
    /**
     * 获取防护危害因素列表
     * @return
     */
    @RequestMapping("/getWorkplaces")
    @ResponseBody
    public EhsResult harmFactorList(String keyword, @CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseWorkplaceExample enterpriseWorkplaceExample = new EnterpriseWorkplaceExample();
        EnterpriseWorkplaceExample.Criteria criteria = enterpriseWorkplaceExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(currentUser.getEnterpriseId());
        if(keyword!=null&&!"".equals(keyword)){
            criteria.andNameLike("%"+keyword+"%");
        }
        List<EnterpriseWorkplace> enterpriseWorkplaces=new ArrayList<EnterpriseWorkplace>();
        try {
            enterpriseWorkplaces = enterpriseWorkplaceService.selectByExample(enterpriseWorkplaceExample);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return EhsResult.ok(enterpriseWorkplaces);
    }
    /**
     * 保存岗位
     * @param enterprisePostJSON 岗位JSON
     * @param operationRuleFile 岗位操作规程文件
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(@CurrentUser EnterpriseUser enterpriseUser, String enterprisePostJSON, MultipartFile operationRuleFile){
        EnterprisePostCustom enterprisePost = JSONObject.parseObject(enterprisePostJSON, EnterprisePostCustom.class);
        enterprisePost.setEnterpriseId(enterpriseUser.getEnterpriseId());
        try {
            int num = postService.saveEnterprisePost(enterprisePost, operationRuleFile);
            if (num == 0){
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存岗位失败");
            }
        } catch (EhsException e){
            e.printStackTrace();
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
        return EhsResult.ok();
    }

    /**
     * 删除岗位
     * @param idsJSON 职岗位id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultFormat delete(@RequestParam String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        String num = postService.deleteEnterprisePostByIds(ids);
        if ("0".equals(num)){
            return ResultFormat.error("删除岗位失败");
        }else if("1".equals(num)){
            return ResultFormat.success();
        }else{
            return ResultFormat.error(num+"岗位已有员工上岗，不能删除");
        }

    }

    /**
     * 查看岗位页面
     * @param id 岗位id
     * @return 查看岗位model
     */
    @RequestMapping("/view")
    public ModelAndView view(Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            EnterprisePostCustom enterprisePost = postService.queryEnterprisePostById(id);
            data.put("enterprisePost", enterprisePost);
        }
        return new ModelAndView("post/view", data);
    }
    /**
     * 保存岗位操作规程
     * @return
     */
    @RequestMapping("uploadOperationRule")
    @ResponseBody
    public EhsResult uploadOperationRule(Long id, MultipartFile operationRule, @CurrentUser EnterpriseUserCustom currentUser) {
        this.postService.uploadOperationRule(id,operationRule);
        return EhsResult.ok("");
    }
}
