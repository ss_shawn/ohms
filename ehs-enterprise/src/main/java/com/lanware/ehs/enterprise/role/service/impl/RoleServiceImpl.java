package com.lanware.ehs.enterprise.role.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.enterprise.role.service.RoleService;
import com.lanware.ehs.mapper.EnterpriseAuthMapper;
import com.lanware.ehs.mapper.EnterpriseRoleResourceRelationMapper;
import com.lanware.ehs.mapper.custom.EnterpriseUserRoleCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseUserRoleCustom;
import com.lanware.ehs.shiro.authentication.ShiroRealm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class RoleServiceImpl implements  RoleService{
    @Autowired
    private EnterpriseUserRoleCustomMapper customMapper;

    @Autowired
    private EnterpriseAuthMapper authMapper;

    @Autowired
    private ShiroRealm shiroRealm;

    @Autowired
    private EnterpriseRoleResourceRelationMapper roleResourceRelationMapper;
    @Override
    public List<EnterpriseUserRoleCustom> findUserRole(String username) {
        return this.customMapper.findUserRole(username);
    }

    @Override
    public List<EnterpriseUserRole> findRoles(EnterpriseUserRole role) {
        EnterpriseUserRoleExample example = new EnterpriseUserRoleExample();
        EnterpriseUserRoleExample.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(role.getName()))
            criteria.andNameLike(role.getName());
        criteria.andEnterpriseIdEqualTo(role.getEnterpriseId());
        criteria.andIsShowEqualTo(1);
        return customMapper.selectByExample(example);
    }

    @Override
    public Page<EnterpriseUserRoleCustom> findRoles(EnterpriseUserRoleCustom role, QueryRequest request) {
        if(request != null){
            PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        }
        return this.customMapper.findRolePage(role);
    }

    @Override
    public EnterpriseUserRoleCustom getRoleCustomById(Long id) {
        return customMapper.getRoleCustomById(id);
    }

    @Override
    @Transactional
    public void createRole(EnterpriseUserRoleCustom customRole) {
        EnterpriseUserRole role = new EnterpriseUserRole();
        BeanUtils.copyProperties(customRole,role);
        role.setGmtCreate(new Date());
        role.setIsShow(EnterpriseUserRoleCustom.SHOW);
        customMapper.insertSelective(role);
        BeanUtils.copyProperties(role,customRole);
        this.saveRoleMenus(customRole);
    }

    @Override
    @Transactional
    public void deleteRoles(String roleIds) {
        List<String> list = Arrays.asList(roleIds.split(StringPool.COMMA));
        for(String str : list) {
            customMapper.deleteByPrimaryKey(Long.valueOf(str));

            //根据角色id删除角色菜单关联表信息
            EnterpriseRoleResourceRelationExample roleResourceRelationExample
                    = new EnterpriseRoleResourceRelationExample();
            EnterpriseRoleResourceRelationExample.Criteria relationCriteria = roleResourceRelationExample.createCriteria();
            relationCriteria.andRoleIdEqualTo(Long.valueOf(str));
            roleResourceRelationMapper.deleteByExample(roleResourceRelationExample);

            //根据角色id删除用户角色关联信息
            EnterpriseAuthExample authExample = new EnterpriseAuthExample();
            EnterpriseAuthExample.Criteria authCriteria = authExample.createCriteria();
            authCriteria.andRoleIdEqualTo(Long.valueOf(str));
            authMapper.deleteByExample(authExample);
        }
    }

    @Override
    @Transactional
    public void updateRole(EnterpriseUserRoleCustom role) {
        role.setGmtModified(new Date());
        this.customMapper.updateByPrimaryKeySelective(role);

        //根据角色id删除角色菜单关联表信息
        EnterpriseRoleResourceRelationExample roleResourceRelationExample
                = new EnterpriseRoleResourceRelationExample();
        EnterpriseRoleResourceRelationExample.Criteria relationCriteria = roleResourceRelationExample.createCriteria();
        relationCriteria.andRoleIdEqualTo(role.getId());
        roleResourceRelationMapper.deleteByExample(roleResourceRelationExample);
        saveRoleMenus(role);
        shiroRealm.clearCache();

    }


    /**
     * 保存角色菜单信息
     * @param role
     */
    private void saveRoleMenus(EnterpriseUserRoleCustom role) {
        if (StringUtils.isNotBlank(role.getMenuIds())) {
            String[] menuIds = role.getMenuIds().split(StringPool.COMMA);
            List<EnterpriseRoleResourceRelation> roleMenus = new ArrayList<>();
            Arrays.stream(menuIds).forEach(menuId -> {
                EnterpriseRoleResourceRelation roleMenu = new EnterpriseRoleResourceRelation();
                roleMenu.setResourceId(Long.valueOf(menuId));
                roleMenu.setRoleId(role.getId());
                roleMenus.add(roleMenu);
            });

            for(EnterpriseRoleResourceRelation relation : roleMenus){
                roleResourceRelationMapper.insertSelective(relation);
            }
        }
    }

}
