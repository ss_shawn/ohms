package com.lanware.ehs.enterprise.workplace.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.workplace.service.InformFileService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 高毒物品告知卡controller
 */
@Controller
@RequestMapping("/workplace/informFile")
public class InformFileController {
    /** 注入informFileService的bean */
    @Autowired
    private InformFileService informFileService;

    /**
     * @description 查询高毒物品告知卡list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listInformFiles")
    @ResponseBody
    public ResultFormat listHarmfactors(Long relationId, String relationModule) {
        JSONObject result = informFileService.listInformFiles(relationId, relationModule);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editInformFile/save")
    @ResponseBody
    /**
     * @description 保存高毒物品告知卡，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param informFile 高毒物品告知卡文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile informFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (informFileService.insertInformFile(enterpriseRelationFile, informFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (informFileService.updateInformFile(enterpriseRelationFile, informFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteInformFile")
    @ResponseBody
    /**
     * @description 删除高毒物品告知卡
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInformFile(Long id, String filePath) {
        if (informFileService.deleteInformFile(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteInformFiles")
    @ResponseBody
    /**
     * @description 批量删除高毒物品告知卡
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInformFiles(Long[] ids) {
        if (informFileService.deleteInformFiles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
