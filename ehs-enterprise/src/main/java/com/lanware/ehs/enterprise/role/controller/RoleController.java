package com.lanware.ehs.enterprise.role.controller;

import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.role.service.RoleService;
import com.lanware.ehs.pojo.EnterpriseUserRole;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserRoleCustom;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * 角色管理
 */
@Slf4j
@RestController
@RequestMapping("role")
public class RoleController extends BaseController {
    @Autowired
    private RoleService roleService;

    /**
     * 获取所有用户角色
     * @param role
     * @return
     */
    @GetMapping
    public EhsResult getAllRoles(EnterpriseUserRole role, @CurrentUser EnterpriseUserCustom userCustom){
        role.setEnterpriseId(userCustom.getEnterpriseId());
        return EhsResult.ok(roleService.findRoles(role));
    }


    /**
     * 获取用户角色列表
     * @param role
     * @param request
     * @return
     */
    @GetMapping("list")
    public EhsResult roleList(EnterpriseUserRoleCustom role, QueryRequest request,@CurrentUser EnterpriseUserCustom userCustom) {
        role.setEnterpriseId(userCustom.getEnterpriseId());
        Map<String, Object> dataTable = getDataTable(this.roleService.findRoles(role, request));
        return EhsResult.ok(dataTable);
    }

    /**
     * 添加角色
     * @param role
     * @return
     * @throws EhsException
     */
    @PostMapping
    public EhsResult addRole(EnterpriseUserRoleCustom role,@CurrentUser EnterpriseUserCustom userCustom) throws EhsException {
        try {
            role.setEnterpriseId(userCustom.getEnterpriseId());
            this.roleService.createRole(role);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "新增角色失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }


    /**
     * 删除角色
     * @param roleIds
     * @return
     * @throws EhsException
     */
    @GetMapping("delete/{roleIds}")
    public EhsResult deleteRoles(@NotBlank(message = "{required}") @PathVariable String roleIds) throws EhsException {
        try {
            this.roleService.deleteRoles(roleIds);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "删除角色失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }


    /**
     * 修改角色
     * @param role
     * @return
     * @throws EhsException
     */
    @PostMapping("update")
    public EhsResult updateRole(EnterpriseUserRoleCustom role) throws EhsException {
        try {
            this.roleService.updateRole(role);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "修改角色失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }
}
