package com.lanware.ehs.enterprise.threesimultaneity.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityService;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.mapper.custom.EnterpriseThreeSimultaneityCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseThreeSimultaneityFileCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseThreeSimultaneityCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @description 三同时管理interface实现类
 */
@Service
public class ThreeSimultaneityServiceImpl implements ThreeSimultaneityService {
    /** 注入enterpriseThreeSimultaneityCustomMapper的bean */
    @Autowired
    private EnterpriseThreeSimultaneityCustomMapper enterpriseThreeSimultaneityCustomMapper;
    /** 注入enterpriseThreeSimultaneityFileCustomMapper的bean */
    @Autowired
    private EnterpriseThreeSimultaneityFileCustomMapper enterpriseThreeSimultaneityFileCustomMapper;
    /** 注入enterpriseRelationFileMapper的bean */
    @Autowired
    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    /**
     * @description 查询三同时list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseThreeSimultaneitys和totalNum的result
     */
    @Override
    public JSONObject listThreeSimultaneitys(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseThreeSimultaneityCustom> enterpriseThreeSimultaneitys
                = enterpriseThreeSimultaneityCustomMapper.listThreeSimultaneitys(condition);
        // 计算距离上次更新天数超出90天的项目数量，已完成的项目数量
        Integer overUpdateProjeactNum = 0,
                completionProjectNum = 0;
        // 计算距离上次更新天数(新增的项目如果没有更新时间，就把创建时间当做更新时间)
        for (EnterpriseThreeSimultaneityCustom enterpriseThreeSimultaneityCustom : enterpriseThreeSimultaneitys) {
            if (enterpriseThreeSimultaneityCustom.getGmtModified() != null) {
                Long timeDiff = new Date().getTime() - enterpriseThreeSimultaneityCustom.getGmtModified().getTime();
                Long dayDiff = timeDiff / (24 * 60 * 60 * 1000);
                enterpriseThreeSimultaneityCustom.setDayDiff(dayDiff);
                // 如果符合超出的要求，项目数量加1
                if (enterpriseThreeSimultaneityCustom.getStatus() == 0) {
                    if (enterpriseThreeSimultaneityCustom.getDayDiff() > 90) {
                        overUpdateProjeactNum += 1;
                    }
                } else if (enterpriseThreeSimultaneityCustom.getStatus() == 1) {
                    completionProjectNum += 1;
                }
            } else {
                Long timeDiff = new Date().getTime() - enterpriseThreeSimultaneityCustom.getGmtCreate().getTime();
                Long dayDiff = timeDiff / (24 * 60 * 60 * 1000);
                enterpriseThreeSimultaneityCustom.setDayDiff(dayDiff);
                // 如果符合超出的要求，项目数量加1
                if (enterpriseThreeSimultaneityCustom.getDayDiff() > 90) {
                    overUpdateProjeactNum += 1;
                }
            }
        }
        // 计算完成率(有百分号)
        DecimalFormat df = new DecimalFormat("0.00");
        String completionRate = "0%";
        if (enterpriseThreeSimultaneitys.size() > 0) {
            completionRate = df.format((float)100*completionProjectNum/enterpriseThreeSimultaneitys.size()) + "%";
        }
        // 将超出的项目数量和完成率放入map
        Map<String, Object> threeSimultaneityStatistics = new HashMap<String, Object>();
        threeSimultaneityStatistics.put("overUpdateProjeactNum", overUpdateProjeactNum);
        threeSimultaneityStatistics.put("completionRate", completionRate);
        result.put("threeSimultaneityStatistics", threeSimultaneityStatistics);

        result.put("enterpriseThreeSimultaneitys", enterpriseThreeSimultaneitys);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseThreeSimultaneityCustom> pageInfo
                    = new PageInfo<EnterpriseThreeSimultaneityCustom>(enterpriseThreeSimultaneitys);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询三同时信息
     * @param id
     * @return EnterpriseThreeSimultaneity三同时pojo
     */
    @Override
    public EnterpriseThreeSimultaneity getThreeSimultaneityById(Long id) {
        return enterpriseThreeSimultaneityCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增三同时信息
     * @param enterpriseThreeSimultaneity 三同时pojo
     * @return int 插入成功的数目
     */
    @Override
    public int insertThreeSimultaneity(EnterpriseThreeSimultaneity enterpriseThreeSimultaneity) {
        return enterpriseThreeSimultaneityCustomMapper.insert(enterpriseThreeSimultaneity);
    }

    /**
     * @description 更新三同时信息
     * @param enterpriseThreeSimultaneity 三同时pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateThreeSimultaneity(EnterpriseThreeSimultaneity enterpriseThreeSimultaneity) {
        return enterpriseThreeSimultaneityCustomMapper.updateByPrimaryKeySelective(enterpriseThreeSimultaneity);
    }

    /**
     * @description 删除三同时信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteThreeSimultaneity(Long id) {
        // 删除oss服务器上对应的文件(三同时各阶段的文件和三同时的其他关联文件)
        EnterpriseThreeSimultaneityFileExample enterpriseThreeSimultaneityFileExample
                = new EnterpriseThreeSimultaneityFileExample();
        EnterpriseThreeSimultaneityFileExample.Criteria criteria
                = enterpriseThreeSimultaneityFileExample.createCriteria();
        criteria.andProjectIdEqualTo(id);
        List<EnterpriseThreeSimultaneityFile> enterpriseThreeSimultaneityFiles
                = enterpriseThreeSimultaneityFileCustomMapper.selectByExample(enterpriseThreeSimultaneityFileExample);
        for (EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile : enterpriseThreeSimultaneityFiles) {
            ossTools.deleteOSS(enterpriseThreeSimultaneityFile.getFilePath());
        }
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria1 = enterpriseRelationFileExample.createCriteria();
        criteria1.andRelationIdEqualTo(id);
        criteria1.andRelationModuleEqualTo("三同时其他文件");
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        for (EnterpriseRelationFile enterpriseRelationFile : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        // 删除数据库中三同时各阶段的文件信息和三同时的其他关联文件信息
        enterpriseThreeSimultaneityFileCustomMapper.deleteByExample(enterpriseThreeSimultaneityFileExample);
        enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        // 删除数据库中三同时的信息
        return enterpriseThreeSimultaneityCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除三同时信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteThreeSimultaneitys(Long[] ids) {
        // 删除oss服务器上对应的文件(三同时各阶段的文件和三同时的其他关联文件)
        EnterpriseThreeSimultaneityFileExample enterpriseThreeSimultaneityFileExample
                = new EnterpriseThreeSimultaneityFileExample();
        EnterpriseThreeSimultaneityFileExample.Criteria criteria
                = enterpriseThreeSimultaneityFileExample.createCriteria();
        criteria.andProjectIdIn(Arrays.asList(ids));
        List<EnterpriseThreeSimultaneityFile> enterpriseThreeSimultaneityFiles
                = enterpriseThreeSimultaneityFileCustomMapper.selectByExample(enterpriseThreeSimultaneityFileExample);
        for (EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile : enterpriseThreeSimultaneityFiles) {
            ossTools.deleteOSS(enterpriseThreeSimultaneityFile.getFilePath());
        }
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria1 = enterpriseRelationFileExample.createCriteria();
        criteria1.andRelationIdIn(Arrays.asList(ids));
        criteria1.andRelationModuleEqualTo("三同时其他文件");
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        for (EnterpriseRelationFile enterpriseRelationFile : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        // 批量删除数据库中三同时各阶段的文件信息和三同时的其他关联文件信息
        enterpriseThreeSimultaneityFileCustomMapper.deleteByExample(enterpriseThreeSimultaneityFileExample);
        enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        // 批量删除数据库中三同时的信息
        EnterpriseThreeSimultaneityExample enterpriseThreeSimultaneityExample
                = new EnterpriseThreeSimultaneityExample();
        EnterpriseThreeSimultaneityExample.Criteria criteria2 = enterpriseThreeSimultaneityExample.createCriteria();
        criteria2.andIdIn(Arrays.asList(ids));
        return enterpriseThreeSimultaneityCustomMapper.deleteByExample(enterpriseThreeSimultaneityExample);
    }

    /**
     * @description 根据条件更新危害程度和三同时阶段
     * @param condition 条件
     * @return 更新成功的数目
     */
    @Override
    public int updateHarmDegreeAndStateByCondition(Map<String, Object> condition) {
        return enterpriseThreeSimultaneityCustomMapper.updateHarmDegreeAndStateByCondition(condition);
    }

    /**
     * @description 根据条件更新三同时阶段
     * @param condition 条件
     * @return 更新成功的数目
     */
    @Override
    public int updateProjectState(Map<String, Object> condition) {
        return enterpriseThreeSimultaneityCustomMapper.updateProjectStateByCondition(condition);
    }

    /**
     * @description 根据条件更新项目状态
     * @param condition 条件
     * @return 更新成功的数目
     */
    @Override
    public int updateStatus(Map<String, Object> condition) {
        return enterpriseThreeSimultaneityCustomMapper.updateStatusByCondition(condition);
    }

    /**
     * @description 三同时文件上传到oss，并保存数据到数据库
     * @param threeSimultaneityFile 上传文件
     * @param enterpriseThreeSimultaneityFile 三同时文件pojo
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    @Transactional
    public int insertThreeSimultaneityFile(EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile
            , MultipartFile threeSimultaneityFile, Long enterpriseId) {
        // 新增数据先保存，以便获取id
        enterpriseThreeSimultaneityFileCustomMapper.insert(enterpriseThreeSimultaneityFile);
        if (threeSimultaneityFile != null){
            String threeSimultaneity = "/" + enterpriseId + "/" + ModuleName.THREESIMULTANEITY.getValue()
                    + "/" + enterpriseThreeSimultaneityFile.getProjectId()
                    + "/" + enterpriseThreeSimultaneityFile.getId() + "/" + threeSimultaneityFile.getOriginalFilename();
            try {
                ossTools.uploadStream(threeSimultaneity, threeSimultaneityFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传三同时文件失败", e);
            }
            enterpriseThreeSimultaneityFile.setFileName(threeSimultaneityFile.getOriginalFilename());
            enterpriseThreeSimultaneityFile.setFilePath(threeSimultaneity);
        }
        // 同时更新三同时的上次更新时间
        Map<String, Object>  condition = Maps.newHashMap();
        condition.put("id", enterpriseThreeSimultaneityFile.getProjectId());
        condition.put("gmtModified", new Date());
        enterpriseThreeSimultaneityCustomMapper.updateGmtModifiedByCondition(condition);
        return enterpriseThreeSimultaneityFileCustomMapper.updateByPrimaryKeySelective(enterpriseThreeSimultaneityFile);
    }

    /**
     * @description 删除文件
     * @param filePath 文件路径
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    public int deleteFileByFilePath(String filePath) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(filePath);
        // 删除数据库中对应的数据
        EnterpriseThreeSimultaneityFileExample enterpriseThreeSimultaneityFileExample
                = new EnterpriseThreeSimultaneityFileExample();
        EnterpriseThreeSimultaneityFileExample.Criteria criteria
                = enterpriseThreeSimultaneityFileExample.createCriteria();
        criteria.andFilePathEqualTo(filePath);
        return enterpriseThreeSimultaneityFileCustomMapper.deleteByExample(enterpriseThreeSimultaneityFileExample);
    }
}
