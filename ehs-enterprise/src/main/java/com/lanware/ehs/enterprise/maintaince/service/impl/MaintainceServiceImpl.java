package com.lanware.ehs.enterprise.maintaince.service.impl;

import com.lanware.ehs.enterprise.maintaince.service.MaintainceService;
import com.lanware.ehs.mapper.SysMaintainMapper;
import com.lanware.ehs.pojo.SysMaintain;
import com.lanware.ehs.pojo.SysMaintainExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaintainceServiceImpl implements MaintainceService {
    @Autowired
    private SysMaintainMapper mapper;
    @Override
    public SysMaintain find() {
        List<SysMaintain> list = mapper.selectByExample(new SysMaintainExample());
        if(list.size() > 0){
            return list.get(0);
        }
        return null;
    }
}
