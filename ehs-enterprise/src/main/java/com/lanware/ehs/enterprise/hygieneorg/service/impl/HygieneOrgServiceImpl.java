package com.lanware.ehs.enterprise.hygieneorg.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.FileDeleteUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.hygieneorg.service.HygieneOrgService;
import com.lanware.ehs.mapper.EnterpriseHygieneOrgMapper;
import com.lanware.ehs.mapper.custom.EnterpriseHygieneOrgCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseRelationFileCustomMapper;
import com.lanware.ehs.pojo.EnterpriseHygieneOrg;
import com.lanware.ehs.pojo.EnterpriseHygieneOrgExample;
import com.lanware.ehs.pojo.custom.EnterpriseHygieneOrgCustom;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class HygieneOrgServiceImpl implements HygieneOrgService {

    private EnterpriseHygieneOrgMapper enterpriseHygieneOrgMapper;

    private EnterpriseHygieneOrgCustomMapper enterpriseHygieneOrgCustomMapper;

    private EnterpriseRelationFileCustomMapper enterpriseRelationFileCustomMapper;

    private OSSTools ossTools;

    private FileDeleteUtil fileDeleteUtil;

    public HygieneOrgServiceImpl(EnterpriseHygieneOrgMapper enterpriseHygieneOrgMapper, EnterpriseHygieneOrgCustomMapper enterpriseHygieneOrgCustomMapper, EnterpriseRelationFileCustomMapper enterpriseRelationFileCustomMapper, OSSTools ossTools, FileDeleteUtil fileDeleteUtil){
        this.enterpriseHygieneOrgMapper = enterpriseHygieneOrgMapper;
        this.enterpriseHygieneOrgCustomMapper = enterpriseHygieneOrgCustomMapper;
        this.enterpriseRelationFileCustomMapper = enterpriseRelationFileCustomMapper;
        this.ossTools = ossTools;
        this.fileDeleteUtil = fileDeleteUtil;
    }

    @Override
    public Page<EnterpriseHygieneOrgCustom> queryEnterpriseHygieneOrgByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<EnterpriseHygieneOrgCustom> page = PageHelper.startPage(pageNum, pageSize);
        enterpriseHygieneOrgCustomMapper.queryEnterpriseHygieneOrgByCondition(condition);
        return page;
    }

    @Override
    @Transactional
    public int saveEnterpriseHygieneOrg(EnterpriseHygieneOrg enterpriseHygieneOrg, MultipartFile employFile) {
        //新增数据先保存，以便获取id
        if (enterpriseHygieneOrg.getId() == null){
            enterpriseHygieneOrg.setGmtCreate(new Date());
            enterpriseHygieneOrgMapper.insert(enterpriseHygieneOrg);
        }
        if (employFile != null){
            //上传了新的聘用书
            if (!StringUtils.isEmpty(enterpriseHygieneOrg.getEmployFile())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseHygieneOrg.getEmployFile());
            }
            //上传并保存新文件
            String employFilePath = "/" + enterpriseHygieneOrg.getEnterpriseId() + "/" + ModuleName.HYGIENEORG.getValue() + "/" + enterpriseHygieneOrg.getId() + "/" + employFile.getOriginalFilename();
            try {
                ossTools.uploadStream(employFilePath, employFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传聘用书失败", e);
            }
            enterpriseHygieneOrg.setEmployFile(employFilePath);
        }
        //职业卫生组织信息
        enterpriseHygieneOrg.setGmtModified(new Date());
        return enterpriseHygieneOrgMapper.updateByPrimaryKeySelective(enterpriseHygieneOrg);
    }

    @Override
    public int deleteEnterpriseHygieneOrgByIds(List<Long> ids) {
        //获取所有文件路径
        List<Map<String, String>> mapList  = enterpriseHygieneOrgCustomMapper.getAllFilesByIds(ids, ModuleName.HYGIENEORG.getValue());
        int num = enterpriseHygieneOrgCustomMapper.deleteEnterpriseHygieneOrgByIds(ids, ModuleName.HYGIENEORG.getValue());
        if (num != 0){
            //异步删除文件
            Set<String> filePathSet = new HashSet<>();
            for (Map<String, String> map : mapList){
                filePathSet.add(map.get("employFile"));
                filePathSet.add(map.get("filePath"));
            }
            fileDeleteUtil.deleteFilesAsync(filePathSet);
        }
        return num;
    }

    @Override
    public List<EnterpriseHygieneOrg> queryListByEnterpriseId(Long enterpriseId) {
        EnterpriseHygieneOrgExample example = new  EnterpriseHygieneOrgExample();
        EnterpriseHygieneOrgExample.Criteria criteria = example.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        List<EnterpriseHygieneOrg> list = enterpriseHygieneOrgMapper.selectByExample(example);
        return list;
    }

    /**
     * 查询职业卫生组织管理员信息
     * @param enterpriseId 企业ID
     * @return
     */
    @Override
    public JSONObject listEmployeesByEnterprieId(Long enterpriseId) {
        JSONObject result = new JSONObject();
        EnterpriseHygieneOrgExample enterpriseHygieneOrgExample = new EnterpriseHygieneOrgExample();
        EnterpriseHygieneOrgExample.Criteria criteria = enterpriseHygieneOrgExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        List<EnterpriseHygieneOrg> enterpriseHygieneOrgs
                = enterpriseHygieneOrgMapper.selectByExample(enterpriseHygieneOrgExample);
        result.put("enterpriseEmployees", enterpriseHygieneOrgs);
        return result;
    }
}