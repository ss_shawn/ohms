package com.lanware.ehs.enterprise.workplace.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.lanware.ehs.enterprise.workplace.service.HarmfactorService;
import com.lanware.ehs.mapper.WorkplaceHarmFactorMapper;
import com.lanware.ehs.mapper.custom.BaseHarmFactorCustomMapper;
import com.lanware.ehs.mapper.custom.MonitorWorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.mapper.custom.WorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.WorkplaceHarmFactor;
import com.lanware.ehs.pojo.WorkplaceHarmFactorExample;
import com.lanware.ehs.pojo.custom.WorkplaceHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 职业病危害因素interface实现类
 */
public class HarmfactorServiceImpl implements HarmfactorService {
    @Autowired
    /** 注入workplaceHarmFactorMapper的bean */
    private WorkplaceHarmFactorMapper workplaceHarmFactorMapper;
    @Autowired
    /** 注入workplaceHarmFactorCustomMapper的bean */
    private WorkplaceHarmFactorCustomMapper workplaceHarmFactorCustomMapper;
    @Autowired
    /** 注入baseHarmFactorCustomMapper的bean */
    private BaseHarmFactorCustomMapper baseHarmFactorCustomMapper;
    /** 注入monitorWorkplaceHarmFactorCustomMapper的bean */
    @Autowired
    private MonitorWorkplaceHarmFactorCustomMapper monitorWorkplaceHarmFactorCustomMapper;

    @Override
    /**
     * @description 根据作业场所id查询危害因素list
     * @param workplaceId 作业场所id
     * @return 返回包含workHarmFactors的result
     */
    public JSONObject listHarmfactors(Long workplaceId) {
        JSONObject result = new JSONObject();
        List<WorkplaceHarmFactorCustom> workplaceHarmFactors
                = workplaceHarmFactorCustomMapper.listHarmFactorCustomsByWorkplaceId(workplaceId);
        result.put("workplaceHarmFactors", workplaceHarmFactors);
        return result;
    }

    /**
     * @description 查询作业场所危害因素扩展list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces和totalNum的result
     */
    @Override
    public JSONObject listWorkplaceHarmFactors(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<WorkplaceHarmFactorCustom> workplaceHarmFactorCustoms
                = workplaceHarmFactorCustomMapper.listWorkplaceHarmFactors(condition);
        // 查询数据和分页数据装进result
        result.put("workplaceHarmFactorCustoms", workplaceHarmFactorCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<WorkplaceHarmFactorCustom> pageInfo = new PageInfo<WorkplaceHarmFactorCustom>(workplaceHarmFactorCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 获取未选择的危害因素result（管理端危害因素获取）
     * @param workplaceId 工作场所id
     * @param harmFactorId 危害因素id
     * @return com.alibaba.fastjson.JSONObject 返回包含baseHarmFactors的result
     */
    public JSONObject getHarmfactors(Long workplaceId, Long harmFactorId) {
        JSONObject result = new JSONObject();
        List<BaseHarmFactor> baseHarmFactors = baseHarmFactorCustomMapper.listUnselectedHarmFactors(workplaceId, harmFactorId);
        result.put("baseHarmFactors", baseHarmFactors);
        return result;
    }

    @Override
    /**
     * @description 根据id查询危害因素信息
     * @param id 危害因素id
     * @return com.lanware.management.pojo.WorkplaceHarmFactor 返回WorkplaceHarmFactor对象
     */
    public WorkplaceHarmFactor getHarmfactorById(Long id) {
        return workplaceHarmFactorMapper.selectByPrimaryKey(id);
    }

    @Override
    /**
     * @description 根据id查询危害因素扩展信息
     * @param id 危害因素id
     * @return com.lanware.management.pojo.WorkplaceHarmFactor 返回WorkplaceHarmFactor对象
     */
    public WorkplaceHarmFactorCustom getHarmfactorCustomById(Long id) {
        return workplaceHarmFactorCustomMapper.getHarmfactorCustomById(id);
    }


    @Override
    /**
     * @description 插入作业场所危害因素信息
     * @param workplaceHarmFactor 作业场所危害因素pojo
     * @return 插入成功的数目
     */
    public int insertHarmfactor(WorkplaceHarmFactor workplaceHarmFactor) {
        return workplaceHarmFactorMapper.insert(workplaceHarmFactor);
    }

    @Override
    /**
     * @description 更新作业场所危害因素信息
     * @param workplaceHarmFactor 作业场所危害因素pojo
     * @return 更新成功的数目
     */
    public int updateHarmfactor(WorkplaceHarmFactor workplaceHarmFactor) {
        return workplaceHarmFactorMapper.updateByPrimaryKeySelective(workplaceHarmFactor);
    }

    /**
     * @description 删除危害因素信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteHarmfactor(Long id) {
        // 删除监测点下的该作业场所和危害因素集合
        WorkplaceHarmFactor workplaceHarmFactor = workplaceHarmFactorMapper.selectByPrimaryKey(id);
        Map<String, Object> condition = Maps.newHashMap();
        condition.put("workplaceId", workplaceHarmFactor.getWorkplaceId());
        condition.put("harmFactorId", workplaceHarmFactor.getHarmFactorId());
        monitorWorkplaceHarmFactorCustomMapper.deleteByCondition(condition);
        return workplaceHarmFactorMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除危害因素信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteHarmfactors(Long[] ids) {
        WorkplaceHarmFactorExample workplaceHarmFactorExample = new WorkplaceHarmFactorExample();
        WorkplaceHarmFactorExample.Criteria criteria = workplaceHarmFactorExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        // 删除监测点下的该作业场所和危害因素集合
        List<WorkplaceHarmFactor> workplaceHarmFactors
                = workplaceHarmFactorMapper.selectByExample(workplaceHarmFactorExample);
        for (WorkplaceHarmFactor workplaceHarmFactor : workplaceHarmFactors) {
            Map<String, Object> condition = Maps.newHashMap();
            condition.put("workplaceId", workplaceHarmFactor.getWorkplaceId());
            condition.put("harmFactorId", workplaceHarmFactor.getHarmFactorId());
            monitorWorkplaceHarmFactorCustomMapper.deleteByCondition(condition);
        }
        return workplaceHarmFactorMapper.deleteByExample(workplaceHarmFactorExample);
    }
}
