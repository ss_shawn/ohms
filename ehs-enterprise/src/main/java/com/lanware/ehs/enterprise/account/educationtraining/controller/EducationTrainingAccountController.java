package com.lanware.ehs.enterprise.account.educationtraining.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.inspectioninstitution.service.InspectionInstitutionService;
import com.lanware.ehs.enterprise.training.service.TrainingService;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.Enum.YearPlanTypeEnum;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingCustom;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingEmployeeCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 培训台账controller
 */
@Controller
@RequestMapping("/educationTrainingAccount")
public class EducationTrainingAccountController {
    /** 注入trainingService的bean */
    @Autowired
    private TrainingService trainingService;
    /** 注入inspectionInstitutionService的bean */
    @Autowired
    private InspectionInstitutionService inspectionInstitutionService;

    /** 下载word的临时路径 */
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;

    @RequestMapping("")
    /**
     * @description 返回培训台账页面
     * @param enterpriseUser 当前登录企业
     * @return java.lang.String 返回页面路径
     */
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/educationTraining/list", data);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(EducationTrainingAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 培训记录word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadTraining")
    @ResponseBody
    public void downloadTraining(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getTrainingMap(enterpriseId,request);
            String uploadPath = upload + File.separator +"培训记录.doc";
            exeWord(dataMap,uploadPath,"educationTrainingMould.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("培训记录.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到培训记录的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getTrainingMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        //查询并返回
        JSONObject result = trainingService.listTrainings(0, 0, condition);
        List<EnterpriseTrainingCustom> enterpriseTrainings
                = (List<EnterpriseTrainingCustom>)result.get("enterpriseTrainings");
        List trainingList = new ArrayList();
        for (EnterpriseTrainingCustom enterpriseTrainingCustom : enterpriseTrainings) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("type", enterpriseTrainingCustom.getType() == 0 ? "普通员工" : "管理员");
            temp.put("trainingDate", sdf.format(enterpriseTrainingCustom.getTrainingDate()));
            temp.put("duration", enterpriseTrainingCustom.getDuration());
            if (enterpriseTrainingCustom.getType() == 0) {
                temp.put("trainingEmployeeNum", enterpriseTrainingCustom.getTrainingEmployeeNum());
            } else if (enterpriseTrainingCustom.getType() == 1) {
                temp.put("trainingEmployeeNum", enterpriseTrainingCustom.getTrainingAdminNum());
            }
            temp.put("place", enterpriseTrainingCustom.getPlace());
            temp.put("theme", enterpriseTrainingCustom.getTheme());
            temp.put("lecturer", enterpriseTrainingCustom.getLecturer());
            trainingList.add(temp);
        }
        dataMap.put("trainingList", trainingList);
        return dataMap;
    }

    /**
     * @description 培训机构word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadTrainingInstitution")
    @ResponseBody
    public void downloadTrainingInstitution(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getTrainingInstitutionMap(enterpriseId,request);
            String uploadPath = upload + File.separator +"培训机构.doc";
            exeWord(dataMap,uploadPath,"trainingInstitution.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("培训机构.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到培训机构的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getTrainingInstitutionMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 查询并返回
        JSONObject result = inspectionInstitutionService.listInspectionInstitutionsByType("培训机构", enterpriseId);
        List<EnterpriseInspectionInstitution> enterpriseInspectionInstitutions
                = (List<EnterpriseInspectionInstitution>)result.get("enterpriseInspectionInstitutions");
        List trainingInstitutionList = new ArrayList();
        for (EnterpriseInspectionInstitution enterpriseInspectionInstitution : enterpriseInspectionInstitutions) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name",enterpriseInspectionInstitution.getName());
            temp.put("contacts",enterpriseInspectionInstitution.getContacts());
            temp.put("mobilePhone",enterpriseInspectionInstitution.getMobilePhone());
            trainingInstitutionList.add(temp);
        }
        dataMap.put("trainingInstitutionList", trainingInstitutionList);
        return dataMap;
    }

    /**
     * @description 管理员培训信息word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadTrainingAdmin")
    @ResponseBody
    public void downloadTrainingAdmin(Long enterpriseId,String training_admin_account_keyword, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getTrainingAdminMap(enterpriseId,request,training_admin_account_keyword);
            String uploadPath = upload + File.separator +"管理员培训信息.doc";
            exeWord(dataMap,uploadPath,"trainingAdmin.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("管理员培训信息.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到管理员培训信息的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getTrainingAdminMap(long enterpriseId, QueryRequest request,String training_admin_account_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        condition.put("type", EnterpriseTrainingCustom.ADMIN_TYPE);
        condition.put("keyword",training_admin_account_keyword);
        // 查询并返回
        JSONObject result = trainingService.listTraingAdmins(0, 0, condition);
        List<EnterpriseTrainingEmployeeCustom> trainingAdmins
                = (List<EnterpriseTrainingEmployeeCustom>)result.get("trainingAdmins");
        List trainingAdminList = new ArrayList();
        for (EnterpriseTrainingEmployeeCustom trainingAdmin : trainingAdmins) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name",trainingAdmin.getName());
            temp.put("year",trainingAdmin.getYear());
            temp.put("totalDuration",trainingAdmin.getTotalDuration());
            trainingAdminList.add(temp);
        }
        dataMap.put("trainingAdminList", trainingAdminList);
        return dataMap;
    }

    /**
     * @description 管理员年度培训报表word下载
     * @param enterpriseId 当前登录企业id
     * @param year 年度
     * @param response
     * @param request
     */
    @RequestMapping("/downloadTrainingAdminYear")
    @ResponseBody
    public void downloadTrainingAdminYear(Long enterpriseId, Integer year, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getYearTrainingAdminMap(enterpriseId,year,request);
            String uploadPath = upload + File.separator +"管理员年度培训报表.doc";
            exeWord(dataMap,uploadPath,"yearTrainingAdmin.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("管理员年度培训报表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到管理员年度培训报表信息的dataMap
     * @param enterpriseId 当前登录企业id
     * @param year 年度
     * @param request
     * @return Map
     */
    private Map getYearTrainingAdminMap(long enterpriseId, Integer year, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        condition.put("year", year);
        condition.put("planType", YearPlanTypeEnum.XCJY);
        // 查询并返回
        JSONObject result = trainingService.getYearAdminTrainingData(condition);
        List<EnterpriseTrainingEmployeeCustom> yearAdminTraingData
                = (List<EnterpriseTrainingEmployeeCustom>)result.get("yearAdminTrainingData");
        List yearTrainingAdminList = new ArrayList();
        for (EnterpriseTrainingEmployeeCustom enterpriseTrainingEmployeeCustom : yearAdminTraingData) {
            Map<String, Object> temp = new HashMap<String, Object>();
            Integer totalAdminNum = enterpriseTrainingEmployeeCustom.getAdminNum();
            BigDecimal totalAdminDuration = enterpriseTrainingEmployeeCustom.getTotalAdminTrainingDuration();
            temp.put("year",year.toString());
            temp.put("totalAdminNum",enterpriseTrainingEmployeeCustom.getAdminNum());
            temp.put("trainingAdminNum",enterpriseTrainingEmployeeCustom.getAdminTrainingNum());
            if (totalAdminNum == 0) {
                temp.put("avergaeAdminDuration", 0);
            } else {
                temp.put("avergaeAdminDuration", totalAdminDuration.doubleValue() / totalAdminNum);
            }
            yearTrainingAdminList.add(temp);
        }
        dataMap.put("yearTrainingAdminList", yearTrainingAdminList);
        return dataMap;
    }

    /**
     * @description 普通员工年度培训报表word下载
     * @param enterpriseId 当前登录企业id
     * @param year 年度
     * @param response
     * @param request
     */
    @RequestMapping("/downloadTrainingEmployeeYear")
    @ResponseBody
    public void downloadTrainingEmployeeYear(Long enterpriseId, Integer year, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getYearTrainingEmployeeMap(enterpriseId,year,request);
            String uploadPath = upload + File.separator +"普通员工年度培训报表.doc";
            exeWord(dataMap,uploadPath,"yearTrainingEmployee.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("普通员工年度培训报表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到普通员工年度培训报表信息的dataMap
     * @param enterpriseId 当前登录企业id
     * @param year 年度
     * @param request
     * @return Map
     */
    private Map getYearTrainingEmployeeMap(long enterpriseId, Integer year, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        condition.put("year", year);
        condition.put("planType", YearPlanTypeEnum.XCJY);
        // 查询并返回
        JSONObject result = trainingService.getYearEmployeeTrainingData(condition);
        List<EnterpriseTrainingEmployeeCustom> yearEmployeeTraingData
                = (List<EnterpriseTrainingEmployeeCustom>)result.get("yearEmployeeTraingData");
        List yearTrainingEmployeeList = new ArrayList();
        for (EnterpriseTrainingEmployeeCustom enterpriseTrainingEmployeeCustom : yearEmployeeTraingData) {
            Map<String, Object> temp = new HashMap<String, Object>();
            Integer employeeNum = enterpriseTrainingEmployeeCustom.getEmployeeNum();
            BigDecimal totalEmployeeDuration = enterpriseTrainingEmployeeCustom.getTotalEmployeeTrainingDuration();
            temp.put("year",year.toString());
            temp.put("involveEmployeeNum",enterpriseTrainingEmployeeCustom.getInvolveNum());
            temp.put("trainingEmployeeNum",enterpriseTrainingEmployeeCustom.getEmployeeTrainingNum());
            if (employeeNum == 0) {
                temp.put("avergaeEmployeeDuration", 0);
            } else {

                temp.put("avergaeEmployeeDuration", totalEmployeeDuration.doubleValue() / employeeNum);
            }
            yearTrainingEmployeeList.add(temp);
        }
        dataMap.put("yearTrainingEmployeeList", yearTrainingEmployeeList);
        return dataMap;
    }

    /**
     * @description 培训台账zip下载
     * @param enterpriseId 当前登录企业id
     * @param aYear 管理员培训报表年份
     * @param eYear 普通员工培训报表年份
     * @param response
     * @param request
     */
    @RequestMapping("/downloadEducationTraining")
    @ResponseBody
    public void downloadEducationTraining(Long enterpriseId, Integer aYear, Integer eYear
           ,String training_admin_account_keyword , HttpServletResponse response, QueryRequest request){
        File upload=null;
        List<String> files=new ArrayList<String>();//待压缩的文件
        String uuid= UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            // 培训记录
            Map<String, Object> dataMap = getTrainingMap(enterpriseId,request);
            String uploadPath = upload + File.separator +"培训记录.doc";
            exeWord(dataMap,uploadPath,"educationTrainingMould.ftl");
            files.add(uploadPath);
            // 培训机构
            dataMap = getTrainingInstitutionMap(enterpriseId,request);
            uploadPath = upload + File.separator +"培训机构.doc";
            exeWord(dataMap,uploadPath,"trainingInstitution.ftl");
            files.add(uploadPath);
            // 管理员培训信息
            dataMap = getTrainingAdminMap(enterpriseId,request,training_admin_account_keyword);
            uploadPath = upload + File.separator +"管理员培训信息.doc";
            exeWord(dataMap,uploadPath,"trainingAdmin.ftl");
            files.add(uploadPath);
            // 管理员年度培训报表
            dataMap = getYearTrainingAdminMap(enterpriseId,aYear,request);
            uploadPath = upload + File.separator +"管理员年度培训报表.doc";
            exeWord(dataMap,uploadPath,"yearTrainingAdmin.ftl");
            files.add(uploadPath);
            // 普通员工年度培训报表
            dataMap = getYearTrainingEmployeeMap(enterpriseId,eYear,request);
            uploadPath = upload + File.separator +"普通员工年度培训报表.doc";
            exeWord(dataMap,uploadPath,"yearTrainingEmployee.ftl");
            files.add(uploadPath);
            //放入压缩列表
            String uploadZipPath = upload + File.separator +"培训台账.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            //下载文件
            File uploadFile = new File(uploadZipPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("培训台账.zip", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 培训台账文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadEducationTrainingAccount")
    @ResponseBody
    public void downloadEducationTrainingAccount(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            Map<String, Object> dataMap = getEducationTrainingAccountMap(enterpriseId,request);
            String uploadPath = upload + File.separator + "职业健康宣传教育培训台帐.doc";
            exeWord(dataMap,uploadPath,"educationTrainingAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业健康宣传教育培训台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到培训台账文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getEducationTrainingAccountMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 管理员培训记录
        condition.put("type", EnterpriseTrainingCustom.ADMIN_TYPE);
        JSONObject result = trainingService.listTrainings(0, 0, condition);
        List<EnterpriseTrainingCustom> adminTrainings
                = (List<EnterpriseTrainingCustom>)result.get("enterpriseTrainings");
        List adminTrainingList = new ArrayList();
        for (EnterpriseTrainingCustom enterpriseTrainingCustom : adminTrainings) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("trainingDate", sdf.format(enterpriseTrainingCustom.getTrainingDate()));
            temp.put("duration", enterpriseTrainingCustom.getDuration());
            temp.put("trainingAdminNum", enterpriseTrainingCustom.getTrainingAdminNum());
            temp.put("place", enterpriseTrainingCustom.getPlace());
            temp.put("theme", enterpriseTrainingCustom.getTheme());
            temp.put("lecturer", enterpriseTrainingCustom.getLecturer());
            String attendanceSheet = enterpriseTrainingCustom.getAttendanceSheet();
            temp.put("attendanceSheet", (attendanceSheet == null || attendanceSheet.equals("")) ? "-" : "已上传");
            adminTrainingList.add(temp);
        }
        dataMap.put("adminTrainingList", adminTrainingList);
        // 普通员工培训记录
        condition.put("type", EnterpriseTrainingCustom.OPERATOR_TYPE);
        JSONObject result1 = trainingService.listTrainings(0, 0, condition);
        List<EnterpriseTrainingCustom> employeeTrainings
                = (List<EnterpriseTrainingCustom>)result1.get("enterpriseTrainings");
        List employeeTrainingList = new ArrayList();
        for (EnterpriseTrainingCustom enterpriseTrainingCustom : employeeTrainings) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("trainingDate", sdf.format(enterpriseTrainingCustom.getTrainingDate()));
            temp.put("duration", enterpriseTrainingCustom.getDuration());
            temp.put("trainingEmployeeNum", enterpriseTrainingCustom.getTrainingEmployeeNum());
            temp.put("place", enterpriseTrainingCustom.getPlace());
            temp.put("theme", enterpriseTrainingCustom.getTheme());
            temp.put("lecturer", enterpriseTrainingCustom.getLecturer());
            String attendanceSheet = enterpriseTrainingCustom.getAttendanceSheet();
            temp.put("attendanceSheet", (attendanceSheet == null || attendanceSheet.equals("")) ? "-" : "已上传");
            employeeTrainingList.add(temp);
        }
        dataMap.put("employeeTrainingList", employeeTrainingList);
        // 培训机构
        JSONObject result2 = inspectionInstitutionService.listInspectionInstitutionsByType("培训机构", enterpriseId);
        List<EnterpriseInspectionInstitution> enterpriseInspectionInstitutions
                = (List<EnterpriseInspectionInstitution>)result2.get("enterpriseInspectionInstitutions");
        List trainingInstitutionList = new ArrayList();
        for (EnterpriseInspectionInstitution enterpriseInspectionInstitution : enterpriseInspectionInstitutions) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name", enterpriseInspectionInstitution.getName());
            String contacts = enterpriseInspectionInstitution.getContacts();
            temp.put("contacts", (contacts == null || contacts.equals("")) ? "-" : contacts);
            String mobilePhone = enterpriseInspectionInstitution.getMobilePhone();
            temp.put("mobilePhone", (mobilePhone == null || mobilePhone.equals("")) ? "-" : mobilePhone);
            String contract  = enterpriseInspectionInstitution.getContract();
            temp.put("contract", (contract == null || contract.equals("")) ? "-" : "已上传");
            trainingInstitutionList.add(temp);
        }
        dataMap.put("trainingInstitutionList", trainingInstitutionList);
        return dataMap;
    }
}
