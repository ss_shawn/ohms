package com.lanware.ehs.enterprise.account.personHealthAccount.controller;

import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.protect.service.ProvideProtectArticlesService;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller("personHealthAccount")
public class ViewController {

    @RequestMapping(Constants.VIEW_PREFIX+"/personHealthAccount")
    /**
     * @description 返回从业人员职业健康监护档案台帐页面
     * @return java.lang.String 返回页面路径
     */
    public String personHealthAccount(Model model, @CurrentUser EnterpriseUserCustom currentUser) {

        return  "account/personHealthAccount/list";
    }


}
