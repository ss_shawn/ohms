package com.lanware.ehs.enterprise.declare.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.declare.service.DeclareService;
import com.lanware.ehs.pojo.EnterpriseDeclare;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/declare")
/**
 * @description 作业场所危害申报controller
 */
public class DeclareController {
    @Autowired
    /** 注入declareService的bean */
    private DeclareService declareService;

    @RequestMapping("")
    /**
     * @description 返回作业场所危害申报页面
     * @param
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "declare/list";
    }

    @RequestMapping("/listDeclares")
    @ResponseBody
    /**
     * @description 获取作业场所危害申报result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listDeclares(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = declareService.listDeclaresByEnterpriseId(pageNum, pageSize, condition, enterpriseUser.getEnterpriseId());
        return ResultFormat.success(result);
    }

    @RequestMapping("/viewDeclare")
    /**
     * @description 查看作业场所危害申报
     * @param id 危害申报id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewDeclare(Long id) {
        JSONObject data = new JSONObject();
        EnterpriseDeclare enterpriseDeclare = declareService.getDeclareById(id);;
        data.put("enterpriseDeclare", enterpriseDeclare);
        return new ModelAndView("declare/view", data);
    }

    @RequestMapping("/editDeclare/save")
    @ResponseBody
    /** @description 保存危害申报信息，并上传文件到oss服务器
     * @param enterpriseDeclareJSON 危害申报json
     * @param declareFile 申报文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseDeclareJSON, MultipartFile declareFile) {
        EnterpriseDeclare enterpriseDeclare = JSONObject.parseObject(enterpriseDeclareJSON, EnterpriseDeclare.class);
        if (enterpriseDeclare.getId() == null) {
            enterpriseDeclare.setGmtCreate(new Date());
            if (declareService.insertDeclare(enterpriseDeclare, declareFile) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseDeclare.setGmtModified(new Date());
            if (declareService.updateDeclare(enterpriseDeclare, declareFile) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/editDeclare/saveReceipt")
    @ResponseBody
    /** @description 保存回执信息，并上传回执文件到oss服务器
     * @param enterpriseDeclareJSON 危害申报json
     * @param declareFile 申报文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat saveReceipt(String enterpriseDeclareJSON, MultipartFile receiptFile) {
        EnterpriseDeclare enterpriseDeclare = JSONObject.parseObject(enterpriseDeclareJSON, EnterpriseDeclare.class);
        enterpriseDeclare.setGmtModified(new Date());
        if (declareService.updateDeclareReceipt(enterpriseDeclare, receiptFile) > 0) {
            return ResultFormat.success("更新成功");
        } else {
            return ResultFormat.error("更新失败");
        }
    }

    @RequestMapping("/deleteDeclare")
    @ResponseBody
    /**
     * @description 删除危害申报信息
     * @param id 要删除的id
     * @param declareAttachment 要删除的申报附件路径
     * @param receiptAttachment 要删除的回执文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteDeclare(Long id, String declareAttachment, String receiptAttachment) {
        if (declareService.deleteDeclare(id, declareAttachment, receiptAttachment) > 0) {
            return ResultFormat.success("删除成功");
        } else {
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteDeclares")
    @ResponseBody
    /**
     * @description 批量删除危害申报信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteDeclares(Long[] ids) {
        if (declareService.deleteDeclares(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
