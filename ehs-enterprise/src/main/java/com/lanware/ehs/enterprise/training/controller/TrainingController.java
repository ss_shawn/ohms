package com.lanware.ehs.enterprise.training.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.hygieneorg.service.HygieneOrgService;
import com.lanware.ehs.enterprise.training.service.TrainingService;
import com.lanware.ehs.pojo.EnterpriseTraining;
import com.lanware.ehs.pojo.EnterpriseTrainingEmployee;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.Enum.YearPlanTypeEnum;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 培训管理controller
 */
@Controller
@RequestMapping("/training")
public class TrainingController {
    /** 注入trainingService的bean */
    @Autowired
    private TrainingService trainingService;
    /** 注入hygieneOrgService的bean */
    @Autowired
    private HygieneOrgService hygieneOrgService;

    @RequestMapping("")
    /**
     * @description 返回培训管理页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "training/list";
    }

    @RequestMapping("/listTrainings")
    @ResponseBody
    /**
     * @description 获取培训管理result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listTrainings(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }else{
            condition.put("sortField", "training_date");
            condition.put("sortType", "desc");
        }
        JSONObject result = trainingService.listTrainings(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/getEmployeeByType")
    @ResponseBody
    /**
     * @description 获取人员result
     * @param type 人员类型
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat getEmployeeByType(Integer type, @CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject result = new JSONObject();
        // 根据人员类型(管理员或普通员工)来查询不同的员工list
        if (type.equals(0)) {
            // 普通员工
            result = trainingService.listEmployeesByEnterprieId(enterpriseUser.getEnterpriseId());
        } else if (type.equals(1)) {
            // 管理员
            result = hygieneOrgService.listEmployeesByEnterprieId(enterpriseUser.getEnterpriseId());
        }
        return ResultFormat.success(result);
    }

    @RequestMapping("/editTraining/save")
    @ResponseBody
    /** @description 保存培训信息，并上传签到表到oss服务器
     * @param employeeMedicalJSON 员工体检json
     * @param attendanceSheetFile 签到表
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseTrainingJSON, MultipartFile attendanceSheetFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseTraining enterpriseTraining = JSONObject.parseObject(enterpriseTrainingJSON, EnterpriseTraining.class);
        if (enterpriseTraining.getId() == null) {
            enterpriseTraining.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseTraining.setGmtCreate(new Date());
            if (trainingService.insertTrainingAndEmployees(enterpriseTraining, attendanceSheetFile) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseTraining.setGmtModified(new Date());
            if (trainingService.updateTrainingAndEmployees(enterpriseTraining, attendanceSheetFile) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteTraining")
    @ResponseBody
    /**
     * @description 删除培训信息
     * @param id 要删除的id
     * @param attendanceSheet 要删除的签到表路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteTraining(Long id, String attendanceSheet) {
        if (trainingService.deleteTraining(id, attendanceSheet) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteTrainings")
    @ResponseBody
    /**
     * @description 批量删除培训信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteTrainings(Long[] ids) {
        if (trainingService.deleteTrainings(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/listTraingAdmins")
    @ResponseBody
    /**
     * @description 获取管理员培训信息
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listTraingAdmins(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("type", EnterpriseTrainingCustom.ADMIN_TYPE);
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = trainingService.listTraingAdmins(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listTraingEmployees")
    @ResponseBody
    /**
     * @description 获取普通员工培训信息
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listTraingEmployees(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("type", EnterpriseTrainingCustom.OPERATOR_TYPE);
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = trainingService.listTraingEmployees(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listTrainingAdminsByTrainingId")
    @ResponseBody
    /**
     * @description 根据培训id获取管理员培训信息
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param trainingId 培训id
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listTrainingAdminsByTrainingId(Integer pageNum, Integer pageSize, String keyword
            , Long trainingId, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("trainingId", trainingId);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = trainingService.listTrainingAdminsByTrainingId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listTrainingEmployeesByTrainingId")
    @ResponseBody
    /**
     * @description 根据培训id获取普通员工培训信息
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param trainingId 培训id
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listTrainingEmployeesByTrainingId(Integer pageNum, Integer pageSize, String keyword
            , Long trainingId, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("trainingId", trainingId);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = trainingService.listTraingEmployeesByTrainingId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listSelectTrainingAdminsByTrainingId")
    @ResponseBody
    /**
     * @description 根据培训id过滤尚未培训的管理员
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param trainingId 培训id
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listSelectTrainingAdminsByTrainingId(Integer pageNum, Integer pageSize, String keyword
            , Long trainingId, @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("trainingId", trainingId);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = trainingService.listSelectTrainingAdminsByTrainingId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listSelectTrainingEmployeesByTrainingId")
    @ResponseBody
    /**
     * @description 根据培训id过滤尚未培训的普通员工
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param trainingId 培训id
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listSelectTrainingEmployeesByTrainingId(Integer pageNum, Integer pageSize, String keyword
            , Long trainingId, @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("trainingId", trainingId);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = trainingService.listSelectTrainingEmployeesByTrainingId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/selectTrainingEmployee/save")
    @ResponseBody
    /** @description 保存参加培训的人员信息
     * @param traingEmployeeListJSON 参加培训的人员json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String traingEmployeeListJSON) {
        List<EnterpriseTrainingEmployee> enterpriseTrainingEmployeeList
                = JSONArray.parseArray(traingEmployeeListJSON, EnterpriseTrainingEmployee.class);
        if (enterpriseTrainingEmployeeList == null || enterpriseTrainingEmployeeList.isEmpty()){
            return ResultFormat.error("选择培训员工失败");
        }
        for (EnterpriseTrainingEmployee enterpriseTrainingEmployee : enterpriseTrainingEmployeeList){
            enterpriseTrainingEmployee.setGmtCreate(new Date());
        }
        if (trainingService.insertBatch(enterpriseTrainingEmployeeList) > 0) {
            return ResultFormat.success("选择培训员工成功");
        } else {
            return ResultFormat.error("选择培训员工失败");
        }
    }

    @RequestMapping("/deleteTrainingEmployee")
    @ResponseBody
    /**
     * @description 删除参加培训的员工
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteTrainingEmployee(Long id) {
        if (trainingService.deleteTrainingEmployee(id) > 0) {
            return ResultFormat.success("移除成功");
        }else{
            return ResultFormat.error("移除失败");
        }
    }

    @RequestMapping("/deleteTrainingEmployees")
    @ResponseBody
    /**
     * @description 批量删除参加培训的员工
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteTrainingEmployees(Long[] ids) {
        if (trainingService.deleteTrainingEmployees(ids) == ids.length) {
            return ResultFormat.success("移除成功");
        }else{
            return ResultFormat.error("移除失败");
        }
    }

    @RequestMapping("/getYearAdminTrainingData")
    @ResponseBody
    /**
     * @description 获取管理员年度培训报表
     * @param year 年度
     * @param enterpriseUser 当前登录用户
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat getYearAdminTrainingData(Integer year, @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("year", year);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("planType", YearPlanTypeEnum.XCJY);
        JSONObject result = trainingService.getYearAdminTrainingData(condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/getYearEmployeeTrainingData")
    @ResponseBody
    /**
     * @description 获取普通员工年度培训报表
     * @param year 年度
     * @param enterpriseUser 当前登录用户
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat getYearEmployeeTrainingData(Integer year, @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("year", year);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("planType", YearPlanTypeEnum.XCJY);
        JSONObject result = trainingService.getYearEmployeeTrainingData(condition);
        return ResultFormat.success(result);
    }
}
