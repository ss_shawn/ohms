package com.lanware.ehs.enterprise.front.web.controller;

import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.utils.DateUtil;
import com.lanware.ehs.enterprise.user.service.UserService;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 视图显示Controller
 */
@Controller("systemView")
public class ViewController {
    @Autowired
    private UserService userService;

    @GetMapping(Constants.VIEW_PREFIX + "404")
    public String error404() {
        return "error/404";
    }

    @GetMapping(Constants.VIEW_PREFIX + "403")
    public String error403() {
        return "error/403";
    }

    @GetMapping(Constants.VIEW_PREFIX + "500")
    public String error500() {
        return "error/500";
    }

    @RequestMapping(Constants.VIEW_PREFIX + "index")
    public String pageIndex() {
        return "index/index";
    }

    /**
     * 系统布局页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "layout")
    public String layout() {
        return "index/layout";
    }

    /**
     * @description 各级责任制
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "responsibility")
    public String responsibility() {
        return "redirect:/responsibility";
    }

    /**
     * @description 职业健康防范制度
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "prevention")
    public String prevention() {
        return "redirect:/prevention";
    }

    /**
     * @description 年度计划管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "yearplan")
    public String yearplan() {
        return "redirect:/yearplan";
    }

    /**
     * @description 作业场所管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "workplace")
    public String workplace() {
        return "workplace/list";
    }

    /**
     * @description 作业场所危害申报管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "declare")
    public String declare() {
        return "declare/list";
    }

    /**
     * @description 检测管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "check")
    public String check() {
        return "redirect:/outsideCheck";
    }

    /**
     * @description 监测点管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "monitorPoint")
    public String monitorPoint() {
        return "check/monitorPoint/list";
    }

    /**
     * @description 年度危害因素检测计划
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "checkPlan")
    public String checkPlan() {
        return "check/checkPlan/list";
    }


    /**
     * @description 培训管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "training")
    public String training() {
        return "training/list";
    }

    /**
     * @description 年度培训计划
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "trainingPlan")
    public String trainingPlan() {
        return "training/trainingPlan/list";
    }


    @GetMapping(Constants.VIEW_PREFIX + "hygieneOrg")
    public String hygieneOrg() {
        return "redirect:/hygieneOrg";
    }

    @GetMapping(Constants.VIEW_PREFIX + "post")
    public String post() {
        return "redirect:/post";
    }

    @GetMapping(Constants.VIEW_PREFIX + "employee")
    public String employee() {
        return "redirect:/employee";
    }

    @GetMapping(Constants.VIEW_PREFIX + "inspection")
    public String inspection(String param,Model model) {
        model.addAttribute("paramz",param);
        return "inspection/list";
    }

    /**
     * @description 年度健康体检计划
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "medicalPlan")
    public String medicalPlan() {
        return "medical/medicalPlan/list";
    }

    /**
     * @description 防护设施
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "protectiveMeasure")
    public String protectiveMeasure() {
        return "protectiveMeasure/list";
    }

    /**
     * @description 年度防护设施检测维护计划
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "protectiveMeasurePlan")
    public String protectiveMeasurePlan() {
        return "protectiveMeasure/protectiveMeasurePlan/list";
    }

    /**
     * @description 维修记录
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "serviceRecord")
    public String serviceRecord() {
        return "serviceRecord/list";
    }

    /**
     * @description 运行使用
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "equipmentUse")
    public String equipmentUse() {
        return "equipmentUse/list";
    }

    /**
     * @description 检查机构管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "inspectionInstitution")
    public String inspectionInstitution() {
        return "inspectionInstitution/list";
    }

    /**
     * @description 高毒演练记录
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "toxicDrill")
    public String toxicDrill() {
        return "toxicDrill/list";
    }

    /**
     * @description 年度应急救援计划
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "rescuePlan")
    public String rescuePlan() {
        return "toxicDrill/rescuePlan/list";
    }

    /**
     * @description 三同时管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "threeSimultaneity")
    public String threeSimultaneity() {
        return "threeSimultaneity/list";
    }

    /**
     * @description 事故调查处理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "accidentHandle")
    public String accidentHandle() {
        return "accidentHandle/list";
    }

    /**
     * @description 咨询管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "consult")
    public String consult() {
        return "consult/list";
    }

    /**
     * @description 文件管理
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "file")
    public String file() {
        return "file/list";
    }

    /**
     * @description 12本台账页面
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "/account")
    public String account() {
        return "account/list";
    }

    /**
     * @description 12本台账之责任制台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "healthResponsibilityAccount")
    public String healthResponsibilityAccount() {
        return "redirect:/healthResponsibilityAccount";
    }

    /**
     * @description 12本台账之告知台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "harmInformAccount")
    public String harmInformAccount() {
        return "account/harmInform/list";
    }

    /**
     * @description 12本台账之申报台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "harmDeclareAccount")
    public String harmDeclareAccount() {
        return "redirect:/harmDeclareAccount";
    }

    /**
     * @description 12本台账之培训台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "educationTrainingAccount")
    public String educationTrainingAccount() {
        return "redirect:/educationTrainingAccount";
    }

    /**
     * @description 12本台账之防护设施台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "facilityMaintenanceAccount")
    public String facilityMaintenanceAccount() {
        return "redirect:/facilityMaintenanceAccount";
    }

    /**
     * @description 12本台账之检测台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "monitorCheckAccount")
    public String monitorCheckAccount() {
        return "redirect:/monitorCheckAccount";
    }

    /**
     * @description 12本台账之检查整改台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "inspectionAccount")
    public String inspectionAccount() {
        return "redirect:/inspectionAccount";
    }

    /**
     * @description 12本台账之防治制度台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "preventionPostAccount")
    public String preventionPostAccount() {
        return "redirect:/preventionPostAccount";
    }

    /**
     * @description 12本台账之应急救援台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "rescueAccidentAccount")
    public String rescueAccidentAccount() {
        return "redirect:/rescueAccidentAccount";
    }

    /**
     * @description 12本台账之三同时台帐
     * @return 页面路径
     */
    @GetMapping(Constants.VIEW_PREFIX + "measureThreeSimultaneityAccount")
    public String measureThreeSimultaneityAccount() {
        return "redirect:/measureThreeSimultaneityAccount";
    }

    /**
     * 用户管理
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "user")
    public String user() {
        return "user/user";
    }

    /**
     * 添加用户页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "user/add")
    public String systemUserAdd() {
        return "user/userAdd";
    }

    /**
     * 编辑用户页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "user/update/{username}")
    public String systemUserUpdate(@PathVariable String username, Model model) {
        resolveUserModel(username, model, false);
        return "user/userUpdate";
    }

    /**
     * 角色管理
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "role")
    public String role() {
        return "role/role";
    }

    /**
     * 修改密码
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "password/update")
    public String passwordUpdate() {
        return "user/passwordUpdate";
    }

    /**
     * 处理用户数据模型
     * @param username
     * @param model
     * @param transform
     */
    private void resolveUserModel(String username, Model model, Boolean transform) {
        EnterpriseUserCustom user = userService.findByName(username);
        model.addAttribute("user", user);
        if (user.getLastLoginTime() != null)
            model.addAttribute("lastLoginTime", DateUtil.getDateFormat(user.getLastLoginTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
    }

}
