package com.lanware.ehs.enterprise.medical.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.medical.service.WaitMedicalService;
import com.lanware.ehs.mapper.custom.EmployeeWaitMedicalCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseEmployeeCustomMapper;
import com.lanware.ehs.pojo.EmployeeWaitMedical;
import com.lanware.ehs.pojo.custom.EmployeeWaitMedicalCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
/**
 * @description 待体检管理interface实现类
 */
public class WaitMedicalServiceImpl implements WaitMedicalService {
    /** 注入employeeWaitMedicalCustomMapper的bean */
    @Autowired
    private EmployeeWaitMedicalCustomMapper employeeWaitMedicalCustomMapper;
    /** 注入enterpriseEmployeeCustomMapperMapper的bean */
    @Autowired
    private EnterpriseEmployeeCustomMapper enterpriseEmployeeCustomMapperMapper;
    
    @Override
    /**
     * @description 查询待体检人员list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeWaitMedicals和totalNum的result
     */
    public JSONObject listWaitMedicals(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EmployeeWaitMedicalCustom> employeeWaitMedicals = employeeWaitMedicalCustomMapper.listWaitMedicals(condition);
        result.put("employeeWaitMedicals", employeeWaitMedicals);
        if (pageNum != null && pageSize != null) {
            PageInfo<EmployeeWaitMedicalCustom> pageInfo = new PageInfo<EmployeeWaitMedicalCustom>(employeeWaitMedicals);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询员工姓名
     * @param id
     * @return 员工name
     */
    @Override
    public String getEmployeeNameById(Long id) {
        return enterpriseEmployeeCustomMapperMapper.getEmployeeNameById(id);
    }


    @Override
    public List<EmployeeWaitMedicalCustom> queryList(Map<String, Object> condition) {
        return employeeWaitMedicalCustomMapper.listWaitMedicals(condition);
    }

    /**
     * @description 更新待体检记录状态，并保存未体检原因
     * @param condition 查询条件
     * @return 更新成功的数目
     */
    @Override
    public int updateWaitMedicalById(Map<String, Object> condition) {
        return employeeWaitMedicalCustomMapper.updateWaitMedicalById(condition);
    }

    /**
     * @description 获取待体检年度报表
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listYearWaitMedicals(Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        List<EmployeeWaitMedicalCustom> yearWaitMedicals = employeeWaitMedicalCustomMapper.listYearWaitMedicals(condition);
        result.put("yearWaitMedicals", yearWaitMedicals);
        return result;
    }

    @Override
    public void addWaitMedical(EmployeeWaitMedical employeeWaitMedical) {
        employeeWaitMedicalCustomMapper.insert(employeeWaitMedical);
    }
}
