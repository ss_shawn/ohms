package com.lanware.ehs.enterprise.medical.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.medical.service.MedicalService;
import com.lanware.ehs.mapper.*;
import com.lanware.ehs.mapper.custom.EmployeeMedicalCustomMapper;
import com.lanware.ehs.mapper.custom.EmployeePostCustomMapper;
import com.lanware.ehs.mapper.custom.EmployeeWaitMedicalCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EmployeeMedicalCustom;
import com.lanware.ehs.pojo.custom.EmployeePostCustom;
import com.lanware.ehs.pojo.custom.EmployeeWaitMedicalCustom;
import com.lanware.ehs.pojo.custom.EnterpriseMedicalResultWaitNotifyCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

/**
 * @description 体检管理interface实现类
 */
@Service
public class MedicalServiceimpl implements MedicalService {
    /** 注入employeeMedicalMapper的bean */
    @Autowired
    private EmployeeMedicalMapper employeeMedicalMapper;
    /** 注入employeeMedicalCustomMapper的bean */
    @Autowired
    private EmployeeMedicalCustomMapper employeeMedicalCustomMapper;
    /** 注入enterpriseEmployeeMapper的bean */
    @Autowired
    private EnterpriseEmployeeMapper enterpriseEmployeeMapper;
   /** 注入employeeWaitMedicalCustomMapper的bean */
    @Autowired
    private EmployeeWaitMedicalCustomMapper employeeWaitMedicalCustomMapper;
    /** 注入employeeWaitMedicalMapper的bean */
    @Autowired
    private EmployeeWaitMedicalMapper employeeWaitMedicalMapper;

    /** 注入enterpriseMedicalResultWaitNotifyMapper的bean */
    @Autowired
    private EnterpriseMedicalResultWaitNotifyMapper enterpriseMedicalResultWaitNotifyMapper;
    /** 注入employeePostCustomMapper的bean */
    @Autowired
    private EmployeePostCustomMapper employeePostCustomMapper;
    @Autowired
    private EmployeePostMapper employeePostMapper;
    @Autowired
    private EmployeeMedicalHistoryMapper employeeMedicalHistoryMapper;
    @Autowired
    private EnterpriseInspectionInstitutionMapper enterpriseInspectionInstitutionMapper;
    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    @Override
    /**
     * @description 查询体检人员list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeWaitMedicals和totalNum的result
     */
    public JSONObject listMedicals(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        //如果是待诊断职业病链接，则过滤疑似职业病的数据
        String keyword = "";
        if(condition.containsKey("keyword")){
            keyword = condition.get("keyword").toString();
            if(keyword.equals("待诊断职业病")){
                condition.put("keyword","疑似职业病");
                condition.put("diseaseNum",0);
            }
        }

        List<EmployeeMedicalCustom> employeeMedicals = employeeMedicalCustomMapper.listMedicals(condition);

        result.put("employeeMedicals", employeeMedicals);
        if (pageNum != null && pageSize != null) {
            PageInfo<EmployeeMedicalCustom> pageInfo = new PageInfo<EmployeeMedicalCustom>(employeeMedicals);
            result.put("totalNum", pageInfo.getTotal());
        }



        return result;
    }

    @Override
    /**
     * @description 获取体检员工result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseEmployees的result
     */
    public JSONObject listEmployeesByEnterprieId(Long enterpriseId) {
        JSONObject result = new JSONObject();
        EnterpriseEmployeeExample enterpriseEmployeeExample = new EnterpriseEmployeeExample();
        EnterpriseEmployeeExample.Criteria criteria = enterpriseEmployeeExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        List<EnterpriseEmployee> enterpriseEmployees
                = enterpriseEmployeeMapper.selectByExample(enterpriseEmployeeExample);
        result.put("enterpriseEmployees", enterpriseEmployees);
        return result;
    }

    @Override
    /**
     * @description 根据id查询员工体检信息
     * @param id 员工体检id
     * @return com.lanware.management.pojo.EmployeeMedical 返回EmployeeMedical对象
     */
    public EmployeeMedical getMedicalById(Long id) {
        return employeeMedicalMapper.selectByPrimaryKey(id);
    }

    @Override
    /**
     * @description 根据id查询员工体检扩展信息
     * @param id 员工体检id
     * @return com.lanware.management.pojo.EmployeeMedical 返回EmployeeMedical对象
     */
    public EmployeeMedicalCustom getMedicalCustomById(Long id) {
        return employeeMedicalCustomMapper.getMedicalCustomById(id);
    }

    /** @description 新增待体检员工体检信息，并上传文件到oss服务器
     * @param employeeMedical 员工体检pojo
     * @param medicalReportFile 体检报告
     * @param enterpriseId 企业id
     * @param condition 条件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    @Transactional
    public int insertWaitMedical(EmployeeMedical employeeMedical, MultipartFile medicalReportFile, Long enterpriseId
            , Map<String ,Object> condition) {
        // 新增数据先保存，以便获取id
        employeeMedicalMapper.insert(employeeMedical);
        // 更新待体检(体检记录id，状态，更新时间)
        condition.put("medicalId", employeeMedical.getId());
        employeeWaitMedicalCustomMapper.updateWaitMedicalByCondition(condition);
        // 新增提交结果待告知记录
        EnterpriseMedicalResultWaitNotify enterpriseMedicalResultWaitNotify = new EnterpriseMedicalResultWaitNotify();
        enterpriseMedicalResultWaitNotify.setEmployeeId(employeeMedical.getEmployeeId());
        enterpriseMedicalResultWaitNotify.setMedicalId(employeeMedical.getId());
        enterpriseMedicalResultWaitNotify.setStatus(EnterpriseMedicalResultWaitNotifyCustom.STATUS_NOTIFY_NO);
        enterpriseMedicalResultWaitNotify.setGmtCreate(new Date());
        enterpriseMedicalResultWaitNotifyMapper.insert(enterpriseMedicalResultWaitNotify);

        // 上传并保存体检报告
        if (medicalReportFile != null){
            String medicalReport = "/" + enterpriseId + "/" + ModuleName.MEDICAL.getValue()
                    + "/" + employeeMedical.getId() + "/" + medicalReportFile.getOriginalFilename();
            try {
                ossTools.uploadStream(medicalReport, medicalReportFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传体检报告失败", e);
            }
            employeeMedical.setMedicalReport(medicalReport);
        }
        //体检结果是复查，插入待体检记录
        if(EmployeeWaitMedicalCustom.TYPE_REVIEW.equals(employeeMedical.getResult())){
            EmployeeWaitMedical employeeWaitMedical=new EmployeeWaitMedical();
            employeeWaitMedical.setStatus(0);
            employeeWaitMedical.setEmployeeId(employeeMedical.getEmployeeId());
            employeeWaitMedical.setPostId(employeeMedical.getPostId());
            employeeWaitMedical.setReviewId(employeeMedical.getId());
            //查出该岗位的上岗时间
//            EmployeePostExample employeePostExample=new EmployeePostExample();
//            employeePostExample.createCriteria().andPostIdEqualTo(employeeMedical.getPostId()).andEmployeeIdEqualTo(employeeMedical.getEmployeeId());
//            employeePostExample.setOrderByClause(" id desc");
//            List<EmployeePost> employeePosts=employeePostMapper.selectByExample(employeePostExample);
//            if(employeePosts.size()>0){
//                employeeWaitMedical.setWorkDate(employeePosts.get(0).getWorkDate());
//            }
            employeeWaitMedical.setWorkDate(employeeMedical.getMedicalDate());
            employeeWaitMedical.setType(EmployeeWaitMedicalCustom.TYPE_REVIEW);
            employeeWaitMedical.setGmtCreate(new Date());
            employeeWaitMedicalMapper.insert(employeeWaitMedical);
        }else if("职业禁忌症".equals(employeeMedical.getResult())){
            EmployeeMedicalHistory employeeMedicalHistory=new EmployeeMedicalHistory();
            employeeMedicalHistory.setDiseaseType(1);
            employeeMedicalHistory.setEmployeeId(employeeMedical.getEmployeeId());
            employeeMedicalHistory.setDiseaseName(employeeMedical.getProblemDescription());
            employeeMedicalHistory.setDiagnosiDate(employeeMedical.getMedicalDate());
            //获取检查机构名称
            EnterpriseInspectionInstitution enterpriseInspectionInstitution=enterpriseInspectionInstitutionMapper.selectByPrimaryKey(employeeMedical.getInstitutionId());
            employeeMedicalHistory.setDiagnosiHosipital(enterpriseInspectionInstitution.getName());
            employeeMedicalHistory.setDiagnosiReport(employeeMedical.getMedicalReport());
            employeeMedicalHistory.setMedicalId(employeeMedical.getId());
            employeeMedicalHistory.setGmtCreate(new Date());
            employeeMedicalHistoryMapper.insert(employeeMedicalHistory);
        }
        return employeeMedicalMapper.updateByPrimaryKeySelective(employeeMedical);
    }

    /**
     * @description 根据员工id获取岗位list
     * @param employeeId 员工id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseEmployees的result
     */
    @Override
    public JSONObject listPostsByEmployeeId(Long employeeId) {
        JSONObject result = new JSONObject();
        List<EmployeePostCustom> employeePostCustoms = employeePostCustomMapper.listPostsByEmployeeId(employeeId);
        result.put("employeePosts", employeePostCustoms);
        return result;
    }

    /**
     * @description 新增员工体检信息
     * @param employeeMedical 员工体检pojo
     * @param medicalReportFile 体检报告
     * @param enterpriseId 企业id
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertMedical(EmployeeMedical employeeMedical, MultipartFile medicalReportFile ,Long enterpriseId) {
        // 新增数据先保存，以便获取id
        employeeMedicalMapper.insert(employeeMedical);
        // 新增提交结果待告知记录
        EnterpriseMedicalResultWaitNotify enterpriseMedicalResultWaitNotify = new EnterpriseMedicalResultWaitNotify();
        enterpriseMedicalResultWaitNotify.setEmployeeId(employeeMedical.getEmployeeId());
        enterpriseMedicalResultWaitNotify.setMedicalId(employeeMedical.getId());
        enterpriseMedicalResultWaitNotify.setStatus(EnterpriseMedicalResultWaitNotifyCustom.STATUS_NOTIFY_NO);
        enterpriseMedicalResultWaitNotify.setGmtCreate(new Date());
        enterpriseMedicalResultWaitNotifyMapper.insert(enterpriseMedicalResultWaitNotify);

        // 上传并保存体检报告
        if (medicalReportFile != null){
            String medicalReport = "/" + enterpriseId + "/" + ModuleName.MEDICAL.getValue()
                    + "/" + employeeMedical.getId() + "/" + medicalReportFile.getOriginalFilename();
            try {
                ossTools.uploadStream(medicalReport, medicalReportFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传体检报告失败", e);
            }
            employeeMedical.setMedicalReport(medicalReport);
        }

        //体检结果是复查，插入待体检记录
        if(EmployeeWaitMedicalCustom.TYPE_REVIEW.equals(employeeMedical.getResult())){
            EmployeeWaitMedical employeeWaitMedical=new EmployeeWaitMedical();
            employeeWaitMedical.setStatus(0);
            employeeWaitMedical.setEmployeeId(employeeMedical.getEmployeeId());
            employeeWaitMedical.setPostId(employeeMedical.getPostId());
            employeeWaitMedical.setReviewId(employeeMedical.getId());
            //查出该岗位的上岗时间
//            EmployeePostExample employeePostExample=new EmployeePostExample();
//            employeePostExample.createCriteria().andPostIdEqualTo(employeeMedical.getPostId()).andEmployeeIdEqualTo(employeeMedical.getEmployeeId());
//            employeePostExample.setOrderByClause(" id desc");
//            List<EmployeePost> employeePosts=employeePostMapper.selectByExample(employeePostExample);
//            if(employeePosts.size()>0){
//                employeeWaitMedical.setWorkDate(employeePosts.get(0).getWorkDate());
//            }
            //员工复查的时间为当前体检时间
            employeeWaitMedical.setWorkDate(employeeMedical.getMedicalDate());

            employeeWaitMedical.setType(EmployeeWaitMedicalCustom.TYPE_REVIEW);
            employeeWaitMedical.setGmtCreate(new Date());
            employeeWaitMedicalMapper.insert(employeeWaitMedical);
        }else if("职业禁忌症".equals(employeeMedical.getResult())){
            EmployeeMedicalHistory employeeMedicalHistory=new EmployeeMedicalHistory();
            employeeMedicalHistory.setDiseaseType(1);
            employeeMedicalHistory.setEmployeeId(employeeMedical.getEmployeeId());
            employeeMedicalHistory.setDiseaseName(employeeMedical.getProblemDescription());
            employeeMedicalHistory.setDiagnosiDate(employeeMedical.getMedicalDate());
            //获取检查机构名称
            EnterpriseInspectionInstitution enterpriseInspectionInstitution=enterpriseInspectionInstitutionMapper.selectByPrimaryKey(employeeMedical.getInstitutionId());
            employeeMedicalHistory.setDiagnosiHosipital(enterpriseInspectionInstitution.getName());
            employeeMedicalHistory.setDiagnosiReport(employeeMedical.getMedicalReport());
            employeeMedicalHistory.setMedicalId(employeeMedical.getId());
            employeeMedicalHistory.setGmtCreate(new Date());
            employeeMedicalHistoryMapper.insert(employeeMedicalHistory);
        }
        return employeeMedicalMapper.updateByPrimaryKeySelective(employeeMedical);
    }

    /**
     * @description 更新员工体检信息
     * @param employeeMedical 员工体检pojo
     * @param medicalReportFile 体检报告
     * @param enterpriseId 企业id
     * @return int 更新成功的数目
     */
    @Override
    @Transactional
    public int updateMedical(EmployeeMedical employeeMedical, MultipartFile medicalReportFile, Long enterpriseId) {
        // 上传并保存体检报告
        if (medicalReportFile != null) {
            // 上传了新的体检报告
            if (!StringUtils.isEmpty(employeeMedical.getMedicalReport())) {
                // 删除旧文件
                ossTools.deleteOSS(employeeMedical.getMedicalReport());
            }
            String medicalReport = "/" + enterpriseId + "/" + ModuleName.MEDICAL.getValue()
                    + "/" + employeeMedical.getId() + "/" + medicalReportFile.getOriginalFilename();
            try {
                ossTools.uploadStream(medicalReport, medicalReportFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传体检报告失败", e);
            }
            employeeMedical.setMedicalReport(medicalReport);
        }

        //职业病历史有问题就删除
        //新增职业病，插入职业病史
        EmployeeMedicalHistoryExample employeeMedicalHistoryExample=new EmployeeMedicalHistoryExample();
        employeeMedicalHistoryExample.createCriteria().andMedicalIdEqualTo(employeeMedical.getId()).andDiseaseTypeEqualTo(1);
        List<EmployeeMedicalHistory> employeeMedicalHistorys=employeeMedicalHistoryMapper.selectByExample(employeeMedicalHistoryExample);
        if(employeeMedicalHistorys.size()>0&&!"职业禁忌症".equals(employeeMedical.getResult())){//修改为非职业病，删除
            employeeMedicalHistoryMapper.deleteByPrimaryKey(employeeMedicalHistorys.get(0).getId());
        }else if(employeeMedicalHistorys.size()==0&&"职业禁忌症".equals(employeeMedical.getResult())){//修改为职业病，删除
            EmployeeMedicalHistory employeeMedicalHistory=new EmployeeMedicalHistory();
            employeeMedicalHistory.setDiseaseType(1);
            employeeMedicalHistory.setEmployeeId(employeeMedical.getEmployeeId());
            employeeMedicalHistory.setDiseaseName(employeeMedical.getProblemDescription());
            employeeMedicalHistory.setDiagnosiDate(employeeMedical.getMedicalDate());
            //获取检查机构名称
            EnterpriseInspectionInstitution enterpriseInspectionInstitution=enterpriseInspectionInstitutionMapper.selectByPrimaryKey(employeeMedical.getInstitutionId());
            employeeMedicalHistory.setDiagnosiHosipital(enterpriseInspectionInstitution.getName());
            employeeMedicalHistory.setDiagnosiReport(employeeMedical.getMedicalReport());
            employeeMedicalHistory.setMedicalId(employeeMedical.getId());
            employeeMedicalHistory.setGmtCreate(new Date());
            employeeMedicalHistoryMapper.insert(employeeMedicalHistory);
        }else if(EmployeeWaitMedicalCustom.TYPE_REVIEW.equals(employeeMedical.getResult())){
            //复查，是否有待体检数据，没有就再插入
            EmployeeWaitMedicalExample employeeWaitMedicalExample = new EmployeeWaitMedicalExample();
            EmployeeWaitMedicalExample.Criteria criteria = employeeWaitMedicalExample.createCriteria();
            criteria.andEmployeeIdEqualTo(employeeMedical.getEmployeeId());
            criteria.andReviewIdEqualTo(employeeMedical.getId());
            criteria.andTypeEqualTo(EmployeeWaitMedicalCustom.TYPE_REVIEW);
            List<EmployeeWaitMedical> waitMedicalList = employeeWaitMedicalMapper.selectByExample(employeeWaitMedicalExample);
            if (waitMedicalList.size() == 0){//插入待体检
                EmployeeWaitMedical employeeWaitMedical=new EmployeeWaitMedical();
                employeeWaitMedical.setStatus(EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
                employeeWaitMedical.setEmployeeId(employeeMedical.getEmployeeId());
                employeeWaitMedical.setPostId(employeeMedical.getPostId());
                employeeWaitMedical.setReviewId(employeeMedical.getId());
                //查出该岗位的上岗时间
//                EmployeePostExample employeePostExample=new EmployeePostExample();
//                employeePostExample.createCriteria().andPostIdEqualTo(employeeMedical.getPostId()).andEmployeeIdEqualTo(employeeMedical.getEmployeeId());
//                employeePostExample.setOrderByClause(" id desc");
//                List<EmployeePost> employeePosts=employeePostMapper.selectByExample(employeePostExample);
//                if(employeePosts.size()>0){
//                    employeeWaitMedical.setWorkDate(employeePosts.get(0).getWorkDate());
//                }
                employeeWaitMedical.setWorkDate(employeeMedical.getMedicalDate());
                employeeWaitMedical.setType(EmployeeWaitMedicalCustom.TYPE_REVIEW);
                employeeWaitMedical.setGmtCreate(new Date());
                employeeWaitMedicalMapper.insert(employeeWaitMedical);
            }else{
                for(EmployeeWaitMedical waitMedical : waitMedicalList){
                    waitMedical.setWorkDate(employeeMedical.getMedicalDate());
                    waitMedical.setGmtModified(new Date());
                    employeeWaitMedicalMapper.updateByPrimaryKeySelective(waitMedical);
                }
            }
        }
        // 如果不是复查，删除复查的待体检
        if(!EmployeeWaitMedicalCustom.TYPE_REVIEW.equals(employeeMedical.getResult())){
            EmployeeWaitMedicalExample  employeeWaitMedicalExample=new EmployeeWaitMedicalExample();
            employeeWaitMedicalExample.createCriteria().andReviewIdEqualTo(employeeMedical.getId()).andTypeEqualTo(EmployeeWaitMedicalCustom.TYPE_REVIEW);
            employeeWaitMedicalMapper.deleteByExample(employeeWaitMedicalExample);
        }
        return employeeMedicalCustomMapper.updateMedical(employeeMedical);
    }

    /**
     * @description 删除员工体检信息，并在体检结果待告知中删除记录
     * @param id 要删除的id
     * @param type 体检类型
     * @param result 体检结论
     * @param medicalReport 要删除的体检报告路径
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteMedical(Long id, String type, String result, String medicalReport) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(medicalReport);
        // 删除数据库中对应的数据
        int num = employeeMedicalMapper.deleteByPrimaryKey(id);
        // 删除体检结果待告知记录
        EnterpriseMedicalResultWaitNotifyExample enterpriseMedicalResultWaitNotifyExample
                = new EnterpriseMedicalResultWaitNotifyExample();
        EnterpriseMedicalResultWaitNotifyExample.Criteria criteria
                = enterpriseMedicalResultWaitNotifyExample.createCriteria();
        criteria.andMedicalIdEqualTo(id);
        enterpriseMedicalResultWaitNotifyMapper.deleteByExample(enterpriseMedicalResultWaitNotifyExample);

        //删除职业病历史数据
        EmployeeMedicalHistoryExample employeeMedicalHistoryExample = new EmployeeMedicalHistoryExample();
        employeeMedicalHistoryExample.createCriteria().andMedicalIdEqualTo(id).andDiseaseTypeEqualTo(1);
        employeeMedicalHistoryMapper.deleteByExample(employeeMedicalHistoryExample);

        // 如果删除的记录体检结论是复查，同时删除该复查记录生成的体检类型为复查的待体检记录
        if (EmployeeMedicalCustom.RESULT_REVIEW.equals(result)) {
            EmployeeWaitMedicalExample employeeWaitMedicalExample = new EmployeeWaitMedicalExample();
            employeeWaitMedicalExample.createCriteria().andReviewIdEqualTo(id).andTypeEqualTo(EmployeeWaitMedicalCustom.TYPE_REVIEW);
            employeeWaitMedicalMapper.deleteByExample(employeeWaitMedicalExample);
        }

        // 如果删除的记录类型是 岗前，离岗，岗中，复查 中的一种，则还原待体检记录状态
        if (EmployeeWaitMedicalCustom.TYPE_PRE_JOB.equals(type) || EmployeeWaitMedicalCustom.TYPE_LEAVE_POST.equals(type)
                || EmployeeWaitMedicalCustom.TYPE_IN_POST.equals(type) || EmployeeWaitMedicalCustom.TYPE_REVIEW.equals(type)) {
            Map<String, Object> condition = new HashMap<>();
            condition.put("medicalId", id);
            condition.put("status", EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
            condition.put("gmtModified", new Date());
            employeeWaitMedicalCustomMapper.updateWaitMedicalByMedicalId(condition);
        }

        return num;
    }

    /**
     * @description 批量删除员工体检信息，并在体检结果待告知中删除记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteMedicals(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EmployeeMedical employeeMedical = employeeMedicalMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(employeeMedical.getMedicalReport());
        }
        // 删除数据库中对应的数据
        EmployeeMedicalExample employeeMedicalExample = new EmployeeMedicalExample();
        EmployeeMedicalExample.Criteria criteria = employeeMedicalExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        int num = employeeMedicalMapper.deleteByExample(employeeMedicalExample);
        // 删除体检结果待告知记录
        EnterpriseMedicalResultWaitNotifyExample enterpriseMedicalResultWaitNotifyExample
                = new EnterpriseMedicalResultWaitNotifyExample();
        EnterpriseMedicalResultWaitNotifyExample.Criteria criteria1
                = enterpriseMedicalResultWaitNotifyExample.createCriteria();
        criteria1.andMedicalIdIn(Arrays.asList(ids));
        enterpriseMedicalResultWaitNotifyMapper.deleteByExample(enterpriseMedicalResultWaitNotifyExample);

        //删除职业病历史数据
        EmployeeMedicalHistoryExample employeeMedicalHistoryExample=new EmployeeMedicalHistoryExample();
        employeeMedicalHistoryExample.createCriteria().andMedicalIdIn(Arrays.asList(ids)).andDiseaseTypeEqualTo(1);
        employeeMedicalHistoryMapper.deleteByExample(employeeMedicalHistoryExample);

        // 查询出 岗前，离岗，岗中，复查 的待体检list
        List<EmployeeWaitMedical> waitMedicalList = new ArrayList<EmployeeWaitMedical>();
        EmployeeWaitMedicalExample employeeWaitMedicalExample = new EmployeeWaitMedicalExample();
        employeeWaitMedicalExample.createCriteria().andMedicalIdIn(Arrays.asList(ids));
        List<EmployeeWaitMedical> employeeWaitMedicalCustomList
                = employeeWaitMedicalMapper.selectByExample(employeeWaitMedicalExample);
        for (EmployeeWaitMedical employeeWaitMedical : employeeWaitMedicalCustomList) {
            employeeWaitMedical.setStatus(EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
            employeeWaitMedical.setGmtModified(new Date());
            waitMedicalList.add(employeeWaitMedical);
        }
        // 恢复待体检记录
        if (waitMedicalList.size() > 0) {
            employeeWaitMedicalCustomMapper.updateBatch(waitMedicalList);
        }

        // 如果体检结论是复查，删除该记录对应生成的待体检记录
        EmployeeWaitMedicalExample employeeWaitMedicalExample1 = new EmployeeWaitMedicalExample();
        employeeWaitMedicalExample1.createCriteria().andReviewIdIn(Arrays.asList(ids));
        employeeWaitMedicalMapper.deleteByExample(employeeWaitMedicalExample1);

        return num;
    }

    /**
     * @description 查询上年度体检人员各项人数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listMedicalPersonNum(Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        List<EmployeeMedicalCustom> employeeMedicalCustoms = employeeMedicalCustomMapper.listMedicalPersonNum(condition);
        result.put("employeeMedicalCustoms", employeeMedicalCustoms);
        return result;
    }

    /**
     * 获取疾病诊断数据
     * @param condition
     * @return
     */
    @Override
    public List<EmployeeMedicalCustom> listMedicals(Map<String, Object> condition) {
        return employeeMedicalCustomMapper.listMedicals(condition);
    }

    /**
     * 获取年度每月体检人数
     * @param condition
     * @return
     */
    @Override
    public Map<String, Integer> getMedicalNumByCondition(Map<String, Object> condition) {
        return employeeMedicalCustomMapper.getMedicalNumByCondition(condition);
    }
}
