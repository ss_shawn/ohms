package com.lanware.ehs.enterprise.resource.service.impl;

import com.google.common.collect.Lists;
import com.lanware.ehs.common.util.TreeUtil;
import com.lanware.ehs.enterprise.resource.entity.MenuTree;
import com.lanware.ehs.enterprise.resource.service.ResourceService;
import com.lanware.ehs.mapper.custom.EnterpriseResourceMapperCustom;
import com.lanware.ehs.pojo.EnterpriseResource;
import com.lanware.ehs.pojo.EnterpriseResourceExample;
import com.lanware.ehs.pojo.custom.EnterpriseResourceCustom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ResourceImpl implements ResourceService {
    @Autowired
    private EnterpriseResourceMapperCustom customMapper;
    @Override
    public MenuTree<EnterpriseResourceCustom> findMenus(EnterpriseResourceCustom menu) {
        EnterpriseResourceExample example = new EnterpriseResourceExample();
        EnterpriseResourceExample.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotBlank(menu.getName())){
            criteria.andNameLike(menu.getName());
        }
        //criteria.andIsShowEqualTo(EnterpriseResourceCustom.SHOW_DISPLAY);
        List<EnterpriseResource> menus = customMapper.selectByExample(example);
        List<MenuTree<EnterpriseResourceCustom>> trees = this.convertMenus(menus);
        return TreeUtil.buildMenuTree(trees);
    }

    @Override
    public MenuTree<EnterpriseResourceCustom> findUserMenus(String username) {
        List<EnterpriseResource> menus = this.customMapper.findUserMenus(username);
        List<MenuTree<EnterpriseResourceCustom>> trees = this.convertMenus(menus);
        return TreeUtil.buildMenuTree(trees);
    }

    @Override
    public List<EnterpriseResourceCustom> findUserPermissions(String username) {
        return customMapper.findUserPermissions(username);
    }

    @Override
    public List<EnterpriseResource> findAllMenus(EnterpriseResource menu) {
        List<EnterpriseResource> list = Lists.newArrayList();
        if(menu == null){
            list = customMapper.selectByExample(null);
        }else{
            //定义查询条件
        }
        return list;
    }


    /**
     * 转换树菜单
     * @param menus
     * @return
     */
    private List<MenuTree<EnterpriseResourceCustom>> convertMenus(List<EnterpriseResource> menus) {
        List<MenuTree<EnterpriseResourceCustom>> trees = new ArrayList<>();
        menus.forEach(menu -> {
            MenuTree<EnterpriseResourceCustom> tree = new MenuTree<>();
            tree.setId(String.valueOf(menu.getId()));
            tree.setParentId(String.valueOf(menu.getParentId()));
            tree.setTitle(menu.getName());
            tree.setIcon(menu.getIcon());
            tree.setHref(menu.getUrl());
            tree.setData(menu);
            trees.add(tree);
        });
        return trees;
    }
}
