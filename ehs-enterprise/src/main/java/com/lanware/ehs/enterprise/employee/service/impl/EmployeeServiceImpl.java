package com.lanware.ehs.enterprise.employee.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.FileDeleteUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.employee.service.EmployeeService;
import com.lanware.ehs.mapper.*;
import com.lanware.ehs.mapper.custom.EmployeeHarmContactHistoryCustomMapper;
import com.lanware.ehs.mapper.custom.EmployeeMedicalHistoryCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseEmployeeCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EmployeeHarmContactHistoryCustom;
import com.lanware.ehs.pojo.custom.EnterpriseEmployeeCustom;
import com.lanware.ehs.service.EmployeePostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EnterpriseEmployeeMapper enterpriseEmployeeMapper;
    @Autowired
    private EmployeeHarmContactHistoryMapper employeeHarmContactHistoryMapper;

    @Autowired
    private EmployeeMedicalHistoryMapper employeeMedicalHistoryMapper;
    @Autowired
    private EmployeeWorkWaitNotifyMapper employeeWorkWaitNotifyMapper;
    @Autowired
    private PostProtectArticlesMapper postProtectArticlesMapper;
    @Autowired
    private EnterpriseWaitProvideProtectArticlesMapper enterpriseWaitProvideProtectArticlesMapper ;
    @Autowired
    private EmployeeWaitMedicalMapper employeeWaitMedicalMapper;
    @Autowired
    private EmployeePostMapper employeePostMapper;
    @Autowired
    private EnterpriseEmployeeCustomMapper enterpriseEmployeeCustomMapper;
    @Autowired
    private EmployeeHarmContactHistoryCustomMapper employeeHarmContactHistoryCustomMapper;
    @Autowired
    private EmployeeMedicalHistoryCustomMapper employeeMedicalHistoryCustomMapper;
    @Autowired
    private OSSTools ossTools;
    @Autowired
    private FileDeleteUtil fileDeleteUtil;


    @Override
    public Page<EnterpriseEmployeeCustom> queryEnterpriseEmployeeByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<EnterpriseEmployeeCustom> page = PageHelper.startPage(pageNum, pageSize);
        enterpriseEmployeeCustomMapper.queryEnterpriseEmployeeByCondition(condition);
        return page;
    }

    @Override
    @Transactional
    public int saveEnterpriseEmployee(EnterpriseEmployee enterpriseEmployee, List<EmployeeHarmContactHistory> harmContactHistoryList, List<Long> harmContactHistoryDeletedIds, List<EmployeeMedicalHistory> medicalHistoryList, List<Long> medicalHistoryDeletedIds, Map<String, MultipartFile> fileMap) {
        if (enterpriseEmployee.getId() == null){
            enterpriseEmployee.setGmtCreate(new Date());
            //生成电子档案号
            enterpriseEmployee.setElectronArchiveNumber(getElectronArchiveNumber(enterpriseEmployee.getEnterpriseId()));
            enterpriseEmployeeMapper.insert(enterpriseEmployee);
        }
        if (fileMap != null && fileMap.containsKey("photoFile")){
            MultipartFile photoFile = fileMap.get("photoFile");
            //上传了新的照片
            if (!StringUtils.isEmpty(enterpriseEmployee.getPhoto())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseEmployee.getPhoto());
            }
            //上传并保存新文件
            String photo = "/" + enterpriseEmployee.getEnterpriseId() + "/" + ModuleName.EMPLOYEE.getValue() + "/" + enterpriseEmployee.getId() + "/" + photoFile.getOriginalFilename();
            try {
                ossTools.uploadStream(photo, photoFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传照片失败", e);
            }
            enterpriseEmployee.setPhoto(photo);
        }
        if (fileMap != null && fileMap.containsKey("healthEntrustFile")){
            MultipartFile healthEntrustFile = fileMap.get("healthEntrustFile");
            //上传了新的职业健康委托书
            if (!StringUtils.isEmpty(enterpriseEmployee.getHealthEntrustFile())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseEmployee.getHealthEntrustFile());
            }
            //上传并保存新文件
            String healthEntrustPath = "/" + enterpriseEmployee.getEnterpriseId() + "/" + ModuleName.EMPLOYEE.getValue() + "/" + enterpriseEmployee.getId() + "/" + healthEntrustFile.getOriginalFilename();
            try {
                ossTools.uploadStream(healthEntrustPath, healthEntrustFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传职业健康委托书失败", e);
            }
            enterpriseEmployee.setHealthEntrustFile(healthEntrustPath);
        }
        enterpriseEmployee.setGmtModified(new Date());
        int num = enterpriseEmployeeMapper.updateByPrimaryKeySelective(enterpriseEmployee);
        //处理职业危害因素接触
        if (harmContactHistoryDeletedIds != null && !harmContactHistoryDeletedIds.isEmpty()){
            EmployeeHarmContactHistoryExample employeeHarmContactHistoryExample = new EmployeeHarmContactHistoryExample();
            EmployeeHarmContactHistoryExample.Criteria criteria = employeeHarmContactHistoryExample.createCriteria();
            criteria.andIdIn(harmContactHistoryDeletedIds);
            employeeHarmContactHistoryMapper.deleteByExample(employeeHarmContactHistoryExample);
        }
        //分离新增数据和更新数据
        if (harmContactHistoryList != null && !harmContactHistoryList.isEmpty()){
            List<EmployeeHarmContactHistory> EmployeeHarmContactHistoryAdd = new ArrayList<>();
            List<EmployeeHarmContactHistory> EmployeeHarmContactHistoryEdit = new ArrayList<>();
            for (int i = 0; i < harmContactHistoryList.size(); i ++){
                EmployeeHarmContactHistory employeeHarmContactHistory = harmContactHistoryList.get(i);
                if (employeeHarmContactHistory.getId() == null){
                    employeeHarmContactHistory.setEmployeeId(enterpriseEmployee.getId());
                    employeeHarmContactHistory.setStatus(0);
                    employeeHarmContactHistory.setGmtCreate(new Date());
                    EmployeeHarmContactHistoryAdd.add(employeeHarmContactHistory);
                }else {
                    employeeHarmContactHistory.setGmtModified(new Date());
                    EmployeeHarmContactHistoryEdit.add(employeeHarmContactHistory);
                }
            }
            if (!EmployeeHarmContactHistoryAdd.isEmpty()){
                employeeHarmContactHistoryCustomMapper.insertBatch(EmployeeHarmContactHistoryAdd);
            }
            if (!EmployeeHarmContactHistoryEdit.isEmpty()){
                employeeHarmContactHistoryCustomMapper.updateBatch(EmployeeHarmContactHistoryEdit);
            }
        }
        //处理既往病史
        if (medicalHistoryDeletedIds != null && !medicalHistoryDeletedIds.isEmpty()){
            EmployeeMedicalHistoryExample employeeMedicalHistoryExample = new EmployeeMedicalHistoryExample();
            EmployeeMedicalHistoryExample.Criteria criteria2 = employeeMedicalHistoryExample.createCriteria();
            criteria2.andIdIn(medicalHistoryDeletedIds);
            List<EmployeeMedicalHistory> employeeMedicalHistories = employeeMedicalHistoryMapper.selectByExample(employeeMedicalHistoryExample);
            Set<String> filePathSet = new HashSet<>();
            for (EmployeeMedicalHistory employeeMedicalHistory : employeeMedicalHistories){
                filePathSet.add(employeeMedicalHistory.getDiagnosiReport());
            }
            if (!filePathSet.isEmpty()){
                fileDeleteUtil.deleteFiles(filePathSet);
            }
            employeeMedicalHistoryMapper.deleteByExample(employeeMedicalHistoryExample);
        }
        //提取新增数据
        if (medicalHistoryList != null && !medicalHistoryList.isEmpty()){
            List<EmployeeMedicalHistory> employeeMedicalHistories = new ArrayList<>();
            for (int i = 0; i < medicalHistoryList.size(); i ++){
                EmployeeMedicalHistory employeeMedicalHistory = medicalHistoryList.get(i);
                if (employeeMedicalHistory.getId() == null){
                    employeeMedicalHistory.setEmployeeId(enterpriseEmployee.getId());
                    employeeMedicalHistory.setGmtCreate(new Date());
                    employeeMedicalHistories.add(employeeMedicalHistory);
                }
            }
            if (!employeeMedicalHistories.isEmpty()){
                employeeMedicalHistoryCustomMapper.insertBatch(employeeMedicalHistories);
            }
            //处理诊断报告
            for (int i = 0; i < medicalHistoryList.size(); i ++){
                EmployeeMedicalHistory employeeMedicalHistory = medicalHistoryList.get(i);
                if (fileMap != null && fileMap.containsKey("diagnosiReportFile" + i)){
                    MultipartFile diagnosiReportFile = fileMap.get("diagnosiReportFile" + i);
                    if (!StringUtils.isEmpty(employeeMedicalHistory.getDiagnosiReport())){
                        //删除旧文件
                        ossTools.deleteOSS(employeeMedicalHistory.getDiagnosiReport());
                    }
                    //上传并保存新文件
                    String diagnosiReport = "/" + enterpriseEmployee.getEnterpriseId() + "/" + ModuleName.EMPLOYEE.getValue() + "/" + employeeMedicalHistory.getId() + "/" + diagnosiReportFile.getOriginalFilename();
                    try {
                        ossTools.uploadStream(diagnosiReport, diagnosiReportFile.getInputStream());
                    } catch (IOException e) {
                        e.printStackTrace();
                        throw new EhsException("上传诊断报告失败", e);
                    }
                    employeeMedicalHistory.setDiagnosiReport(diagnosiReport);
                }
                employeeMedicalHistory.setGmtModified(new Date());
            }
            employeeMedicalHistoryCustomMapper.updateBatch(medicalHistoryList);
        }
        return num;
    }

    @Override
    public int deleteEnterpriseEmployeeByIds(List<Long> ids) {
        //判断该人员是否被使用
        for(Long id:ids){
            EmployeePostExample employeePostExample=new EmployeePostExample();
            employeePostExample.createCriteria().andEmployeeIdEqualTo(id);
            int n=employeePostMapper.selectByExample(employeePostExample).size();
            if(n>0){
                return -8;
            }
        }

        List<Map<String, String>> list = enterpriseEmployeeCustomMapper.getAllFilesByIds(ids);
        Set<String> filePathSet = new HashSet<>();
        for (Map<String, String> map : list){
            if (!StringUtils.isEmpty(map.get("photo"))){
                filePathSet.add(map.get("photo"));
            }
            if (!StringUtils.isEmpty(map.get("healthEntrustFile"))){
                filePathSet.add(map.get("healthEntrustFile"));
            }
            if (!StringUtils.isEmpty(map.get("diagnosiReport"))){
                filePathSet.add(map.get("diagnosiReport"));
            }
        }
        if (!filePathSet.isEmpty()){
            fileDeleteUtil.deleteFilesAsync(filePathSet);
        }
        int num = enterpriseEmployeeCustomMapper.deleteEnterpriseEmployeeByIds(ids);
        return num;
    }

    @Override
    @Transactional
    public int takeUpPost(JSONObject parameters) {
        Long id = parameters.getLong("id");
        Date now = new Date();
        EnterpriseEmployee enterpriseEmployee = new EnterpriseEmployee();
        enterpriseEmployee.setId(id);
        Long postId = parameters.getLong("postId");
        if (parameters.containsKey("inductionDate")){
            //是否已有上岗记录
            EmployeePostExample employeePostExample=new EmployeePostExample();
            employeePostExample.createCriteria().andEmployeeIdEqualTo(enterpriseEmployee.getId()).andLeaveDateIsNull();
            long n=employeePostMapper.countByExample(employeePostExample);
            if(n>0){//有上岗记录，不能上岗
                return -9;
            }
            Date inductionDate = parameters.getDate("inductionDate");

            //历史岗位
            EmployeePost employeePost = new EmployeePost();
            employeePost.setEmployeeId(id);
            employeePost.setPostId(postId);
            employeePost.setWorkDate(inductionDate);
            employeePost.setGmtCreate(now);
            employeePostMapper.insert(employeePost);

            //人员信息中的人员岗位ID设置
            enterpriseEmployee.setOndutyId(employeePost.getId());
            //危害因素接触史
            EmployeeHarmContactHistory employeeHarmContactHistory = new EmployeeHarmContactHistory();
            employeeHarmContactHistory.setEmployeeId(id);
            employeeHarmContactHistory.setStarttime(inductionDate);
            employeeHarmContactHistory.setEmployeePostId(employeePost.getId());
            employeeHarmContactHistory.setStatus(1);
            employeeHarmContactHistory.setGmtCreate(new Date());
            employeeHarmContactHistoryMapper.insert(employeeHarmContactHistory);
            //岗前待告知
            EmployeeWorkWaitNotify employeeWorkWaitNotify = new EmployeeWorkWaitNotify();
            employeeWorkWaitNotify.setEmployeeId(id);
            employeeWorkWaitNotify.setEmployeePostId(employeePost.getId());
            employeeWorkWaitNotify.setGmtCreate(now);
            employeeWorkWaitNotify.setStatus(0);
            employeeWorkWaitNotifyMapper.insert(employeeWorkWaitNotify);
            //防护用品待发
            PostProtectArticlesExample postProtectArticlesExample = new PostProtectArticlesExample();
            PostProtectArticlesExample.Criteria criteria = postProtectArticlesExample.createCriteria();
            criteria.andPostIdEqualTo(postId);
            List<PostProtectArticles> postProtectArticlesList = postProtectArticlesMapper.selectByExample(postProtectArticlesExample);
            //岗位有防护用品，才需要发放
            for(PostProtectArticles postProtectArticles:postProtectArticlesList){
                EnterpriseWaitProvideProtectArticles enterpriseWaitProvideProtectArticles = new EnterpriseWaitProvideProtectArticles();
                enterpriseWaitProvideProtectArticles.setEmployeePostId(employeePost.getId());
                enterpriseWaitProvideProtectArticles.setEnterpriseId(parameters.getLong("enterpriseId"));
                enterpriseWaitProvideProtectArticles.setStatus(0);
                enterpriseWaitProvideProtectArticles.setProtectArticlesId(postProtectArticles.getProtectArticlesId());
                enterpriseWaitProvideProtectArticles.setGmtCreate(now);
                enterpriseWaitProvideProtectArticlesMapper.insert(enterpriseWaitProvideProtectArticles);
            }

            //待体检
            EmployeeWaitMedical employeeWaitMedical = new EmployeeWaitMedical();
            employeeWaitMedical.setEmployeeId(id);
            employeeWaitMedical.setPostId(postId);
            employeeWaitMedical.setType("岗前");
            employeeWaitMedical.setWorkDate(inductionDate);
            employeeWaitMedical.setStatus(0);
            employeeWaitMedical.setGmtCreate(now);
            employeeWaitMedicalMapper.insert(employeeWaitMedical);
        }else {
            //人员设置岗位id为空
            enterpriseEmployee.setOndutyId((long)0);
            //查询在岗信息
            EmployeePostExample employeePostExample=new EmployeePostExample();
            employeePostExample.createCriteria().andEmployeeIdEqualTo(id).andLeaveDateIsNull();
            List<EmployeePost> employeePostList=employeePostMapper.selectByExample(employeePostExample);
            if(employeePostList.size()<1){//未找到在岗信息出现异常
                return -1;
            }
            EmployeePost employeePost=employeePostList.get(0);
            //离岗时间
            Date leaveDate = parameters.getDate("leaveDate");
            if(leaveDate.before(employeePost.getWorkDate())){
                return -8;
            }

            employeePost.setLeaveDate(leaveDate);
            employeePost.setGmtModified(new Date());
            employeePostMapper.updateByPrimaryKey(employeePost);
            //修改危害因素接触史,离岗时，固定所有变量
            EmployeeHarmContactHistoryExample employeeHarmContactHistoryExample=new EmployeeHarmContactHistoryExample();
            employeeHarmContactHistoryExample.createCriteria().andEmployeeIdEqualTo(id).andEmployeePostIdEqualTo(employeePost.getId());
            List<EmployeeHarmContactHistory> employeeHarmContactHistorys=employeeHarmContactHistoryMapper.selectByExample(employeeHarmContactHistoryExample);
            if(employeeHarmContactHistorys.size()>0){
                EmployeeHarmContactHistoryCustom mployeeHarmContactHistoryCustom=employeeHarmContactHistoryCustomMapper.queryEmployeeHarmContactHistoryById(employeeHarmContactHistorys.get(0).getId());
                mployeeHarmContactHistoryCustom.setEndtime(mployeeHarmContactHistoryCustom.getEndtime2());
                mployeeHarmContactHistoryCustom.setStarttime(mployeeHarmContactHistoryCustom.getStarttime2());
                mployeeHarmContactHistoryCustom.setEnterpriseName(mployeeHarmContactHistoryCustom.getEnterpriseName2());
                mployeeHarmContactHistoryCustom.setHarmFactorName(mployeeHarmContactHistoryCustom.getHarmFactorName2());
                mployeeHarmContactHistoryCustom.setPostName(mployeeHarmContactHistoryCustom.getPostName2());
                mployeeHarmContactHistoryCustom.setProtectArticlesName(mployeeHarmContactHistoryCustom.getProtectArticlesName2());
                mployeeHarmContactHistoryCustom.setProtectiveMeasuresName(mployeeHarmContactHistoryCustom.getProtectiveMeasuresName2());
                mployeeHarmContactHistoryCustom.setGmtModified(new Date());
                mployeeHarmContactHistoryCustom.setStatus(2);//2为离岗后的
                employeeHarmContactHistoryMapper.updateByPrimaryKey(mployeeHarmContactHistoryCustom);
            }
            //待体检
            EmployeeWaitMedical employeeWaitMedical = new EmployeeWaitMedical();
            employeeWaitMedical.setEmployeeId(id);
            employeeWaitMedical.setPostId(employeePost.getPostId());
            employeeWaitMedical.setType("离岗");
            employeeWaitMedical.setWorkDate(leaveDate);
            employeeWaitMedical.setStatus(0);
            employeeWaitMedical.setGmtCreate(now);
            employeeWaitMedicalMapper.insert(employeeWaitMedical);
        }
        enterpriseEmployee.setGmtModified(now);
        return enterpriseEmployeeMapper.updateByPrimaryKeySelective(enterpriseEmployee);
    }

    /**
     * 获取电子档案号
     * @param enterpriseId
     * @return
     */
    @Override
    public  String getElectronArchiveNumber(long enterpriseId){
        //生成电子档案号
        EnterpriseEmployeeExample enterpriseEmployeeExample=new EnterpriseEmployeeExample();
        enterpriseEmployeeExample.createCriteria().andEnterpriseIdEqualTo(enterpriseId);
        enterpriseEmployeeExample.setOrderByClause(" electron_archive_number desc");
        List<EnterpriseEmployee>  enterpriseEmployees=enterpriseEmployeeMapper.selectByExample(enterpriseEmployeeExample);
        String electronArchiveNumber="OH";
        if(enterpriseEmployees.size()>0){
            String tempElectronArchiveNumber=enterpriseEmployees.get(0).getElectronArchiveNumber();
            if(tempElectronArchiveNumber==null||"".equals(tempElectronArchiveNumber)){
                electronArchiveNumber+="000001";
            }else{
                if(tempElectronArchiveNumber.length()>2){
                    int num=Integer.parseInt(tempElectronArchiveNumber.substring(2,tempElectronArchiveNumber.length()));
                    electronArchiveNumber+=String.format("%06d",++num);
                }else{
                    electronArchiveNumber+="000001";
                }
            }
        }else{
            electronArchiveNumber+="000001";
        }
        return electronArchiveNumber;
    }

    @Override
    public List<EnterpriseEmployeeCustom> findWorkEmployeesByEnterpriseId(Long enterpriseId) {
        return enterpriseEmployeeCustomMapper.findWorkEmployeesByEnterpriseId(enterpriseId);
    }
}
