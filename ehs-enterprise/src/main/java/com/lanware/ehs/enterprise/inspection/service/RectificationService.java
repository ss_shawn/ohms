package com.lanware.ehs.enterprise.inspection.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseInspectionRectification;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 整改记录interface
 */
public interface RectificationService {
    /**
     * @description 查询整改记录list
     * @param inspectionId 日常检查id
     * @return 返回包含enterpriseInspectionRectifications的result
     */
    JSONObject listRectifications(Long inspectionId);

    /**
     * @description 根据id查询整改记录
     * @param id
     * @return 返回EnterpriseInspectionRectification
     */
    EnterpriseInspectionRectification getRectificationById(Long id);

    /**
     * @description 新增整改记录，并上传文件到oss服务器
     * @param enterpriseInspectionRectification 整改记录pojo
     * @param rectificationFile 整改文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertRectification(EnterpriseInspectionRectification enterpriseInspectionRectification
            , MultipartFile rectificationFile, Long enterpriseId);

    /**
     * @description 更新整改记录，并上传文件到oss服务器
     * @param enterpriseInspectionRectification 整改记录pojo
     * @param rectificationFile 整改文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateRectification(EnterpriseInspectionRectification enterpriseInspectionRectification
            , MultipartFile rectificationFile, Long enterpriseId);

    /**
     * @description 删除整改记录
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteRectification(Long id, String filePath);

    /**
     * @description 批量删除整改记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteRectifications(Long[] ids);
}