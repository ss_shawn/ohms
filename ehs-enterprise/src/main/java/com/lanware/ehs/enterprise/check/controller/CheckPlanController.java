package com.lanware.ehs.enterprise.check.controller;

import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 检测计划Controller
 */
@RestController
@RequestMapping("checkPlan")
@Slf4j
public class CheckPlanController {
    @Autowired
    private YearplanService yearplanService;

    /**
     * 删除危害因素年度检测计划
     * @param id
     * @return
     */
    @PostMapping("delete/{id}")
    public EhsResult delete(@PathVariable Long id){
        try {
            yearplanService.deleteYearPlanById(id);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "删除检测计划失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }
}
