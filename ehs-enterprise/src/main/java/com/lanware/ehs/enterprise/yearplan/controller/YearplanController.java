package com.lanware.ehs.enterprise.yearplan.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.training.service.TrainingService;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.Enum.YearPlanTypeEnum;
import com.lanware.ehs.pojo.custom.EnterpriseTrainingEmployeeCustom;
import com.lanware.ehs.pojo.custom.EnterpriseYearPlanCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/yearplan")
public class YearplanController {
    @Autowired
    /** 注入yearplanService的bean */
    private YearplanService yearplanService;
    /** 注入trainingService的bean */
    @Autowired
    private TrainingService trainingService;

    @RequestMapping("")
    /**
     * @description 返回年度计划管理页面
     * @return 返回页面路径
     */
    public String list() {
        return "yearplan/list";
    }

    @RequestMapping("/listYearplans")
    @ResponseBody
    /**
     * @description 获取年度计划result
     * @param year 年度
     * @param enterpriseUser 当前登录企业
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    public ResultFormat listYearplans(String year, @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String ,Object> result = new HashMap<String, Object>();
        List<EnterpriseYearPlanCustom> enterpriseYearPlanCustomList = yearplanService.listYearplansByEnterpriseId(year, enterpriseUser.getEnterpriseId());
        for (EnterpriseYearPlanCustom enterpriseYearPlanCustom : enterpriseYearPlanCustomList) {
            result.put(enterpriseYearPlanCustom.getPlanType(), enterpriseYearPlanCustom);
        }
        return ResultFormat.success(result);
    }

    /**
     * @description 获取年度培训计划result
     * @param year 年度
     * @param enterpriseUser 当前登录企业
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    @RequestMapping("/listYearTrainingPlans")
    @ResponseBody
    public ResultFormat listYearTrainingPlans(String year, @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String ,Object> result = new HashMap<String, Object>();
        Map<String ,Object> condition = new HashMap<String, Object>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("year", year);
        condition.put("planType", YearPlanTypeEnum.XCJY);
        EnterpriseYearPlanCustom enterpriseYearPlanCustom = yearplanService.getYearplanByCondition(condition);
        if (!StringUtils.isEmpty(enterpriseYearPlanCustom)) {
            result.put(enterpriseYearPlanCustom.getPlanType(), enterpriseYearPlanCustom);
        }
        // 获取管理员年度培训信息
        JSONObject adminResult = trainingService.getYearAdminTrainingData(condition);
        List<EnterpriseTrainingEmployeeCustom> yearTraingAdminDataList
                = (List<EnterpriseTrainingEmployeeCustom>)adminResult.get("yearAdminTrainingData");
        EnterpriseTrainingEmployeeCustom trainingAdmin = yearTraingAdminDataList.get(0);
        result.put("trainingAdmin", trainingAdmin);
        // 获取普通员工年度培训信息
        JSONObject employeeResult = trainingService.getYearEmployeeTrainingData(condition);
        List<EnterpriseTrainingEmployeeCustom> yearTraingEmployeeDataList
                = (List<EnterpriseTrainingEmployeeCustom>)employeeResult.get("yearEmployeeTraingData");
        EnterpriseTrainingEmployeeCustom trainingEmployee = yearTraingEmployeeDataList.get(0);
        result.put("trainingEmployee", trainingEmployee);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editYearplan/save")
    @ResponseBody
     /** @description 保存年度计划信息，并上传文件到oss服务器
     * @param enterpriseYearPlanJSON 年度计划json
     * @param planFile 计划文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseYearPlanJSON, MultipartFile planFile) {
        EnterpriseYearPlan enterpriseYearPlan
                = JSONObject.parseObject(enterpriseYearPlanJSON, EnterpriseYearPlan.class);
        // 要添加或更新的计划的condition
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("year", enterpriseYearPlan.getYear());
        condition.put("planType", enterpriseYearPlan.getPlanType());
        condition.put("enterpriseId", enterpriseYearPlan.getEnterpriseId());
        if (enterpriseYearPlan.getId() == null) {
            // 如果要添加计划的年份和类型已存在，则不允许添加
            if (yearplanService.findYearplanByCondition(condition) != null) {
                return ResultFormat.error("添加失败，该年度此类型的计划已经存在");
            } else {
                enterpriseYearPlan.setGmtCreate(new Date());
                if (yearplanService.insertYearplan(enterpriseYearPlan, planFile) > 0) {
                    return ResultFormat.success("添加成功");
                } else {
                    return ResultFormat.error("添加失败");
                }
            }
        } else {
            // 如果要更新计划的年份和类型已存在（除去本身），则不允许添加
           condition.put("id", enterpriseYearPlan.getId());
            if (yearplanService.findYearplanByCondition(condition) != null) {
                return ResultFormat.error("添加失败，该年度此类型的计划已经存在");
            } else {
                enterpriseYearPlan.setGmtModified(new Date());
                if (yearplanService.updateYearplan(enterpriseYearPlan, planFile) > 0) {
                    return ResultFormat.success("更新成功");
                } else {
                    return ResultFormat.error("更新失败");
                }
            }
        }
    }

    /**
     * @description 获取年度检测计划result
     * @param year 年度
     * @param enterpriseUser 当前登录企业
     * @return com.lanware.management.common.format.ResultFormat 返回格式化result
     */
    @RequestMapping("/listYearCheckPlans")
    @ResponseBody
    public ResultFormat listYearCheckPlans(String year, @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String ,Object> result = new HashMap<String, Object>();
        Map<String ,Object> condition = new HashMap<String, Object>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("year", year);
        condition.put("planType", YearPlanTypeEnum.WHYSJC);
        // 获取年度检测计划(检测月份和检测危害因素)
        List<EnterpriseYearPlanCustom> enterpriseYearPlanCustoms = yearplanService.listYearCheckPlans(condition);
        result.put("enterpriseYearPlans", enterpriseYearPlanCustoms);
        return ResultFormat.success(result);
    }

    /**
     * @description 获取该企业监测点涉及的危害因素
     * @param keyword 关键字
     * @param enterpriseUser 当前登录企业
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/getCheckPlanHarmFactors")
    @ResponseBody
    public ResultFormat getCheckPlanHarmFactors(String keyword, @CurrentUser EnterpriseUser enterpriseUser) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        JSONObject result = yearplanService.getCheckPlanHarmFactors(condition);
        return ResultFormat.success(result);
    }

    /** @description 保存年度检测计划和检测的危害因素集合
     * @param enterpriseYearPlanCustom 年度计划扩展pojo
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/editYearCheckPlan/save")
    @ResponseBody
    public ResultFormat save(EnterpriseYearPlanCustom enterpriseYearPlanCustom) {
        if (enterpriseYearPlanCustom.getId() == null) {
            if (yearplanService.insertYearCheckPlan(enterpriseYearPlanCustom) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            if (yearplanService.updateYearCheckPlan(enterpriseYearPlanCustom) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }
}
