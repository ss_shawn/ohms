package com.lanware.ehs.enterprise.workplace.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.workplace.service.IdentifyFileService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 职业病危害警示controller
 */
@Controller
@RequestMapping("/workplace/identifyFile")
public class IdentifyFileController {
    /** 注入identifyFileService的bean */
    @Autowired
    private IdentifyFileService identifyFileService;

    /**
     * @description 查询职业病危害警示list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listIdentifyFiles")
    @ResponseBody
    public ResultFormat listHarmfactors(Long relationId, String relationModule) {
        JSONObject result = identifyFileService.listIdentifyFiles(relationId, relationModule);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editIdentifyFile/save")
    @ResponseBody
    /**
     * @description 保存职业病危害警示，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param identifyFile 职业病危害警示文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile identifyFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (identifyFileService.insertIdentifyFile(enterpriseRelationFile, identifyFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (identifyFileService.updateIdentifyFile(enterpriseRelationFile, identifyFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteIdentifyFile")
    @ResponseBody
    /**
     * @description 删除职业病危害警示
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteIdentifyFile(Long id, String filePath) {
        if (identifyFileService.deleteIdentifyFile(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteIdentifyFiles")
    @ResponseBody
    /**
     * @description 批量删除职业病危害警示
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteIdentifyFiles(Long[] ids) {
        if (identifyFileService.deleteIdentifyFiles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
