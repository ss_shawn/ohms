package com.lanware.ehs.enterprise.accidenthandle.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseAccidentHandle;

import java.util.Map;

/**
 * @description 事故调查处理interface
 */
public interface AccidentHandleService {
    /**
     * @description 查询事故调查处理list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseAccidentHandles和totalNum的result
     */
    JSONObject listAccidentHandles(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询事故调查处理信息
     * @param id 事故调查处理id
     * @return com.lanware.management.pojo.EnterpriseInspection 返回EnterpriseInspection对象
     */
    EnterpriseAccidentHandle getAccidentHandleById(Long id);

    /**
     * @description 新增事故调查处理信息
     * @param enterpriseAccidentHandle 事故调查处理pojo
     * @return int 插入成功的数目
     */
    int insertAccidentHandle(EnterpriseAccidentHandle enterpriseAccidentHandle);

    /**
     * @description 更新事故调查处理信息
     * @param enterpriseAccidentHandle 事故调查处理pojo
     * @return int 更新成功的数目
     */
    int updateAccidentHandle(EnterpriseAccidentHandle enterpriseAccidentHandle);

    /**
     * @description 删除事故调查处理信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteAccidentHandle(Long id);

    /**
     * @description 批量删除事故调查处理信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteAccidentHandles(Long[] ids);
}
