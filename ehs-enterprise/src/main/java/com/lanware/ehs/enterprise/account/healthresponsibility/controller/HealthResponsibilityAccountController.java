package com.lanware.ehs.enterprise.account.healthresponsibility.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.file.service.FileService;
import com.lanware.ehs.enterprise.hygieneorg.service.HygieneOrgService;
import com.lanware.ehs.enterprise.responsibility.service.ResponsibilityService;
import com.lanware.ehs.pojo.EnterpriseFile;
import com.lanware.ehs.pojo.EnterpriseManageFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseHygieneOrgCustom;
import com.lanware.ehs.pojo.custom.EnterpriseManageFileCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 责任制台账controller
 */
@Controller
@RequestMapping("/healthResponsibilityAccount")
public class HealthResponsibilityAccountController {
    /** 注入hygieneOrgService的bean */
    @Autowired
    private HygieneOrgService hygieneOrgService;
    /** 注入fileService的bean */
    @Autowired
    private FileService fileService;
    /** 注入responsibilityService的bean */
    @Autowired
    private ResponsibilityService responsibilityService;

    /** 下载word的临时路径 */
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;

    @RequestMapping("")
    /**
     * @description 返回责任制台账页面
     * @param enterpriseUser 当前登录企业
     * @return java.lang.String 返回页面路径
     */
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/healthResponsibility/list", data);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(HealthResponsibilityAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 职业健康管理机构或组织word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadHygieneOrg")
    @ResponseBody
    public ResultFormat downloadHygieneOrg(Long enterpriseId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 职业健康管理组织
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        Page<EnterpriseHygieneOrgCustom> page
                = hygieneOrgService.queryEnterpriseHygieneOrgByCondition(0, 0 ,condition);
        List hygieneOrgList = new ArrayList();
        for (EnterpriseHygieneOrgCustom enterpriseHygieneOrgCustom : page) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name", enterpriseHygieneOrgCustom.getName());
            temp.put("department", enterpriseHygieneOrgCustom.getDepartment());
            temp.put("job", enterpriseHygieneOrgCustom.getJob());
            temp.put("duty", enterpriseHygieneOrgCustom.getDuty());
            temp.put("isFulltime", enterpriseHygieneOrgCustom.getIsFulltime() == 0 ? "否" : "是");
            hygieneOrgList.add(temp);
        }
        dataMap.put("hygieneOrgList", hygieneOrgList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(HealthResponsibilityAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("hygieneOrgMould.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "管理组织.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("管理组织.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 往来文件记录word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadFile")
    @ResponseBody
    public ResultFormat downloadFile(Long enterpriseId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 监管文件
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        JSONObject result = fileService.listFiles(0, 0, condition);
        List<EnterpriseFile> enterpriseFiles = (List<EnterpriseFile>)result.get("enterpriseFiles");
        List fileList = new ArrayList();
        for (EnterpriseFile enterpriseFile : enterpriseFiles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("fileNumber", enterpriseFile.getFileNumber());
            temp.put("transmDate", sdf.format(enterpriseFile.getTransmDate()));
            temp.put("fileName", enterpriseFile.getFileName());
            temp.put("transmUnit", enterpriseFile.getTransmUnit());
            temp.put("transmPerson", enterpriseFile.getTransmPerson());
            fileList.add(temp);
        }
        dataMap.put("fileList", fileList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(HealthResponsibilityAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("fileMould.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "往来文件记录.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("往来文件记录.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 责任制台账zip下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadHealthResponsibility")
    @ResponseBody
    public ResultFormat downloadHealthResponsibility(Long enterpriseId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> dataMap1 = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 职业健康管理组织
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        Page<EnterpriseHygieneOrgCustom> page
                = hygieneOrgService.queryEnterpriseHygieneOrgByCondition(0, 0 ,condition);
        List hygieneOrgList = new ArrayList();
        for (EnterpriseHygieneOrgCustom enterpriseHygieneOrgCustom : page) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name", enterpriseHygieneOrgCustom.getName());
            temp.put("department", enterpriseHygieneOrgCustom.getDepartment());
            temp.put("job", enterpriseHygieneOrgCustom.getJob());
            temp.put("duty", enterpriseHygieneOrgCustom.getDuty());
            temp.put("isFulltime", enterpriseHygieneOrgCustom.getIsFulltime() == 0 ? "否" : "是");
            hygieneOrgList.add(temp);
        }
        dataMap.put("hygieneOrgList", hygieneOrgList);
        // 监管文件
        JSONObject result = fileService.listFiles(0, 0, condition);
        List<EnterpriseFile> enterpriseFiles = (List<EnterpriseFile>)result.get("enterpriseFiles");
        List fileList = new ArrayList();
        for (EnterpriseFile enterpriseFile : enterpriseFiles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("fileNumber", enterpriseFile.getFileNumber());
            temp.put("transmDate", sdf.format(enterpriseFile.getTransmDate()));
            temp.put("fileName", enterpriseFile.getFileName());
            temp.put("transmUnit", enterpriseFile.getTransmUnit());
            temp.put("transmPerson", enterpriseFile.getTransmPerson());
            fileList.add(temp);
        }
        dataMap1.put("fileList", fileList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(HealthResponsibilityAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("hygieneOrgMould.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "管理组织.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            Template template1 = configuration.getTemplate("fileMould.ftl");
            // 输出文件
            String uploadPath1 = upload + File.separator + "往来文件记录.doc";
            File outFile1 = new File(uploadPath1);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile1.getParentFile().exists()){
                outFile1.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile1),"UTF-8"));
            // 生成文件
            template1.process(dataMap1, out1);
            // 关闭流
            out1.flush();
            out1.close();
            //放入压缩列表
            files.add(uploadPath);
            files.add(uploadPath1);
            String uploadZipPath = upload + File.separator + "责任制台账.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            File uploadZipFile = new File(uploadZipPath);
            FileInputStream fis = new FileInputStream(uploadZipFile);
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("责任制台账.zip", "utf-8"));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = fis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            fis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 责任制台账文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadHealthResponsibilityAccount")
    @ResponseBody
    public void downloadHealthResponsibilityAccount(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getHealthResponsibilityAccountMap(enterpriseId,request);
            String uploadPath = upload + File.separator + "职业健康管理责任制台帐.doc";
            exeWord(dataMap,uploadPath,"healthResponsibilityAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业健康管理责任制台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到责任制台账文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getHealthResponsibilityAccountMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 职业健康管理机构或组织
        Page<EnterpriseHygieneOrgCustom> page
                = hygieneOrgService.queryEnterpriseHygieneOrgByCondition(0, 0 ,condition);
        List hygieneOrgList = new ArrayList();
        for (EnterpriseHygieneOrgCustom enterpriseHygieneOrgCustom : page) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name", enterpriseHygieneOrgCustom.getName());
            temp.put("department", enterpriseHygieneOrgCustom.getDepartment());
            temp.put("job", enterpriseHygieneOrgCustom.getJob());
            String duty = enterpriseHygieneOrgCustom.getDuty();
            temp.put("duty", (duty == null || duty.equals("")) ? "-" : duty);
            temp.put("isFulltime", enterpriseHygieneOrgCustom.getIsFulltime() == 0 ? "否" : "是");
            String employFile = enterpriseHygieneOrgCustom.getEmployFile();
            temp.put("employFile", (employFile == null || employFile.equals("")) ? "-" : "已上传");
            hygieneOrgList.add(temp);
        }
        dataMap.put("hygieneOrgList", hygieneOrgList);
        // 职业健康管理各级责任制
        condition.put("module", EnterpriseManageFileCustom.MODULE_RESPONSIBILITY);
        List<EnterpriseManageFile> enterpriseManageFiles = responsibilityService.listResponsibilityFiles(condition);
        List responsibilityList = new ArrayList();
        for (EnterpriseManageFile enterpriseManageFile : enterpriseManageFiles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("fileType", enterpriseManageFile.getFileType());
            String filePath = enterpriseManageFile.getFilePath();
            temp.put("filePath", (filePath == null || filePath.equals("")) ? "-" : "已上传");
            responsibilityList.add(temp);
        }
        dataMap.put("responsibilityList", responsibilityList);
        // 监管文件
        JSONObject result = fileService.listFiles(0, 0, condition);
        List<EnterpriseFile> enterpriseFiles = (List<EnterpriseFile>)result.get("enterpriseFiles");
        List fileList = new ArrayList();
        for (EnterpriseFile enterpriseFile : enterpriseFiles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("fileNumber", enterpriseFile.getFileNumber());
            temp.put("transmDate", sdf.format(enterpriseFile.getTransmDate()));
            temp.put("fileName", enterpriseFile.getFileName());
            temp.put("transmUnit", enterpriseFile.getTransmUnit());
            temp.put("transmPerson", enterpriseFile.getTransmPerson());
            fileList.add(temp);
        }
        dataMap.put("fileList", fileList);
        return dataMap;
    }
}
