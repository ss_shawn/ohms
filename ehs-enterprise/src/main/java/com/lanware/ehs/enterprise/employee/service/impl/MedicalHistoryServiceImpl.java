package com.lanware.ehs.enterprise.employee.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.FileDeleteUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.employee.service.MedicalHistoryService;
import com.lanware.ehs.mapper.EmployeeMedicalHistoryMapper;
import com.lanware.ehs.mapper.custom.EmployeeMedicalHistoryCustomMapper;
import com.lanware.ehs.pojo.EmployeeMedicalHistory;
import com.lanware.ehs.pojo.EmployeeMedicalHistoryExample;
import com.lanware.ehs.pojo.custom.EmployeeMedicalHistoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class MedicalHistoryServiceImpl implements MedicalHistoryService {

    private EmployeeMedicalHistoryMapper employeeMedicalHistoryMapper;

    private EmployeeMedicalHistoryCustomMapper employeeMedicalHistoryCustomMapper;

    private OSSTools ossTools;

    private FileDeleteUtil fileDeleteUtil;

    @Autowired
    public MedicalHistoryServiceImpl(EmployeeMedicalHistoryMapper employeeMedicalHistoryMapper, EmployeeMedicalHistoryCustomMapper employeeMedicalHistoryCustomMapper, OSSTools ossTools, FileDeleteUtil fileDeleteUtil) {
        this.employeeMedicalHistoryMapper = employeeMedicalHistoryMapper;
        this.employeeMedicalHistoryCustomMapper = employeeMedicalHistoryCustomMapper;
        this.ossTools = ossTools;
        this.fileDeleteUtil = fileDeleteUtil;
    }

    @Override
    public Page<EmployeeMedicalHistoryCustom> queryEmployeeMedicalHistoryByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<EmployeeMedicalHistoryCustom> page = PageHelper.startPage(pageNum, pageSize);
        employeeMedicalHistoryCustomMapper.queryEmployeeMedicalHistoryByCondition(condition);
        return page;
    }

    @Override
    public int saveEmployeeMedicalHistory(Long enterpriseId, EmployeeMedicalHistory employeeMedicalHistory, MultipartFile diagnosiReportFile) {
        //新增数据先保存，以便获取id
        if (employeeMedicalHistory.getId() == null){
            employeeMedicalHistory.setGmtCreate(new Date());
            employeeMedicalHistoryMapper.insert(employeeMedicalHistory);
        }
        if (diagnosiReportFile != null){
            //上传了新的诊断报告
            if (!StringUtils.isEmpty(employeeMedicalHistory.getDiagnosiReport())){
                //删除旧文件
                ossTools.deleteOSS(employeeMedicalHistory.getDiagnosiReport());
            }
            //上传并保存新文件
            String diagnosiReport = "/" + enterpriseId + "/" + ModuleName.EMPLOYEE.getValue() + "/" + employeeMedicalHistory.getId() + "/" + diagnosiReportFile.getOriginalFilename();
            try {
                ossTools.uploadStream(diagnosiReport, diagnosiReportFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传诊断报告失败", e);
            }
            employeeMedicalHistory.setDiagnosiReport(diagnosiReport);
        }
        employeeMedicalHistory.setGmtModified(new Date());
        return employeeMedicalHistoryMapper.updateByPrimaryKeySelective(employeeMedicalHistory);
    }

    @Override
    public int deleteEmployeeMedicalHistory(List<Long> ids) {
        EmployeeMedicalHistoryExample employeeMedicalHistoryExample = new EmployeeMedicalHistoryExample();
        EmployeeMedicalHistoryExample.Criteria criteria2 = employeeMedicalHistoryExample.createCriteria();
        criteria2.andIdIn(ids);
        List<EmployeeMedicalHistory> employeeMedicalHistories = employeeMedicalHistoryMapper.selectByExample(employeeMedicalHistoryExample);
        Set<String> filePathSet = new HashSet<>();
        for (EmployeeMedicalHistory employeeMedicalHistory : employeeMedicalHistories){
            filePathSet.add(employeeMedicalHistory.getDiagnosiReport());
        }
        if (!filePathSet.isEmpty()){
            fileDeleteUtil.deleteFilesAsync(filePathSet);
        }
        int num = employeeMedicalHistoryMapper.deleteByExample(employeeMedicalHistoryExample);
        return num;
    }
}
