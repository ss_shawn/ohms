package com.lanware.ehs.enterprise.threesimultaneity.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 三同时其他文件interface
 */
public interface OtherFileService {
    /**
     * @description 查询三同时其他文件list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @param fileType 文件类型
     * @return 返回包含enterpriseRelationFiles的result
     */
    JSONObject listOtherFiles(Long relationId, String relationModule, String fileType);

    /**
     * @description 根据id查询三同时其他文件
     * @param id
     * @return EnterpriseRelationFile
     */
    EnterpriseRelationFile getOtherFileById(Long id);

    /**
     * @description 新增三同时其他文件，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param otherFile 三同时其他文件文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertOtherFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile otherFile, Long enterpriseId);

    /**
     * @description 更新三同时其他文件，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param otherFile 三同时其他文件文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateOtherFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile otherFile, Long enterpriseId);

    /**
     * @description 删除三同时其他文件
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteOtherFile(Long id, String filePath);

    /**
     * @description 批量删除三同时其他文件
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteOtherFiles(Long[] ids);
}
