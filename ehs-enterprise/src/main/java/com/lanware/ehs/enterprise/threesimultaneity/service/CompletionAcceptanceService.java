package com.lanware.ehs.enterprise.threesimultaneity.service;

import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;

import java.util.List;

/**
 * @description 三同时管理竣工验收阶段interface
 */
public interface CompletionAcceptanceService {
    /**
     * @description 根据项目id和阶段三同时文件list
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile> 三同时管理文件list
     */
    List<EnterpriseThreeSimultaneityFile> listCompletionAcceptances(Long projectId, Integer projectState);
}
