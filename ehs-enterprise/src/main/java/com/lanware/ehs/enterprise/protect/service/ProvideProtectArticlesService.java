package com.lanware.ehs.enterprise.protect.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @description 防护用品interface
 */
public interface ProvideProtectArticlesService {

    /**
     * @description 根据id查询防护用品
     * @param id 防护用品id
     * @return com.lanware.management.pojo.EnterpriseProtectArticlesCustom 返回EnterpriseProtectArticles对象
     */
    EnterpriseProvideProtectArticles getEnterpriseProvideProtectArticlesById(Long id);
    /**
     * @description 根据查询防护用品
     * @param
     * @return com.lanware.management.pojo.EnterpriseProtectArticlesCustom 返回EnterpriseProtectArticles对象
     */
    Page<EnterpriseProvideProtectArticlesCustom> findProtectList(Map<String, Object> condition, QueryRequest request);

    /**
     * 保存防护用品
     * @param enterpriseProtectArticles 防护用品发放 signatureRecord 签名记录文件
     * @return成功与否 1成功 0不成功
     */
    int saveEnterpriseProvideProtectArticles(EnterpriseProvideProtectArticlesCustom enterpriseProtectArticles, MultipartFile signatureRecord);
    /**
     * 修改防护用品,
     * @param enterpriseProtectArticles
     * @return成功与否 1成功 0不成功
     */
    int updateEnterpriseProvideProtectArticles(EnterpriseProvideProtectArticlesCustom enterpriseProtectArticles);
    /**
     * 通过id获取防护用品
     * @param id
     * @return防护用品
     */
    EnterpriseProvideProtectArticlesCustom getEnterpriseProvideProtectArticlesCustomByID(long id);
    /**
     * 删除防护用品,
     * @param id
     * @return成功与否 1成功 0不成功
     */
    int deleteEnterpriseProvideProtectArticles(long id);
    /**
     * 根据员工ID获取员工岗位信息
     * @param condition
     * @return 员工岗位信息
     */
    List<EmployeePostCustom> getEmployeePostByEmployeeId(Map<String, Object> condition);
    /**
     * 根据企业ID获取防护物品信息
     * @param enterpriseId 企业ID
     * @return 企业防护物品
     */
    List<EnterpriseProtectArticles> getProtectByEnterpriseId(Long enterpriseId);
    /**
     * 获取防护物品信息
     * @param
     * @return 岗位防护用品信息
     */
     List<EnterpriseProtectArticles> getProtect(Long enterpriseId);
    /**
     * 根据人员岗位ID获取防护物品信息
     * @param employeePostId 人员岗位ID
     * @return 岗位防护用品信息
     */
    List<EnterpriseProtectArticlesCustom> getProtectByByEmployeePostId(Long employeePostId);
    /**
     * 根据人员岗位ID获取防护物品信息
     * @param employeePostId 人员岗位ID
     * @return 岗位防护用品信息
     */
     List<EnterpriseProtectArticlesCustom> getWaitProtectByByEmployeePostId(Long employeePostId);

    /**
     * @description 根据条件查询待发放防护用品发放
     * @param condition 模糊查询条件等
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProvideProtectArticles对象
     */
     Page<EnterpriseWaitProvideProtectArticlesCustom>  findEnterpriseWaitProvideProtectArticles(Map<String, Object> condition, QueryRequest request);
    /**
     * 通过待发放ID获取发放物品信息
     * @param id
     * @return防护用品发放
     */
    EnterpriseProvideProtectArticlesCustom getEnterpriseProvideProtectArticlesCustomByWaitID(long id);

    /**
     * 返回个人防护用品发放情况
     * @param condition 条件 企业ID
     * @return 个人防护用品发放情况
     */
    List<Map<String,Object>> findProvideProtectArticles(Map<String,Object> condition);

    /**
     * 返回个人防护用品发放情况
     * @param condition 条件 企业ID
     * @return 个人防护用品发放情况
     */
    List<Map<String, Object>> findProvideProtectArticlesByPost(Map<String,Object> condition);
    /**
     * 查询待发放防护用品信息
     * @param condition
     * @return 岗位防护用品信息
     */
    List<EnterpriseWaitProvideProtectArticlesCustom> queryWaitProvideProtectArticles(Map<String,Object> condition);
    /**
     * 保存防护用品发放签名记录文件,
     * @param id 防护用品发放 signatureRecord 签名记录文件
     * @return成功与否 1成功 0不成功
     */
    int uploadSignatureRecord(Long id, MultipartFile signatureRecord);
    /**
     * 删除待发放防护用品记录
     * @param enterpriseWaitProvideProtectArticles
     * @return 修改条数
     */
     int delProvideProtectSubmit(EnterpriseWaitProvideProtectArticles enterpriseWaitProvideProtectArticles);
    /**
     * 根据岗位信息
     * @param condition
     * @return 员工岗位信息
     */
    List<EnterprisePostCustom> getPost(Map<String, Object> condition);
    /**
     * 根据岗位ID获取员工岗位信息
     * @param condition
     * @return 员工岗位信息
     */
    List<EmployeePostCustom> listEmployeePostByPostId(Map<String, Object> condition);
    /**
     * 保存防护用品批量发放,
     * @param enterpriseProvideProtectArticles 防护用品发放 signatureRecord 签名记录文件
     * @return成功与否 1成功 0不成功
     */
    int saveEnterpriseProvideProtectArticlesAll(EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticles, MultipartFile signatureRecord);
    }
