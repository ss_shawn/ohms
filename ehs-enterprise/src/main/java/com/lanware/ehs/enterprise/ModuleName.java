package com.lanware.ehs.enterprise;

public enum ModuleName {

    BASICINFO("basicInfo","基础信息"),
    HYGIENEORG("hygieneOrg",""),
    POST("post",""),
    EMPLOYEE("employee",""),
    RESPONSIBILITY("responsibility","各级责任制"),
    PREVENTION("prevention","职业健康防范制度"),
    YEARPLAN("yearplan","年度计划管理"),
    FILE("file", "文件管理"),
    WORKPLACE("workplace","作业场所管理"),
    DECLARE("declare","作业场所危害申报"),
    CHECK("check","检测管理"),
    MEDICAL("medical","体检管理"),
    TRAINING("training","培训管理"),
    INSPECTION("inspection","日常检查整改登记"),
    PROTECTIVEMEASURE("protectiveMeasure","防护措施"),
    INSPECTIONINSTITUTION("inspectionInstitution","检查机构管理"),
    TOXICDRILL("toxicDrill","高毒演练记录"),
    ACCIDENTHANDLE("accidentHandle","事故调查处理"),
    PROTECTARTICLES("protectArticles","防护用品"),
    THREESIMULTANEITY("threeSimultaneity", "三同时管理"),
    WORKNOTIFY("workNotify","岗前告知"),
    WORKPLACENOTIFY("workplaceNotify","作业场所告知"),
    MEDICALNOTIFY("medicalNotify","体检结果告知");

    ModuleName(String value,String description){
        this.value = value;
        this.description = description;
    }

    private String value;
    private String description;

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
