package com.lanware.ehs.enterprise.declare.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseDeclareWorkplace;

import java.util.List;
import java.util.Map;

/**
 * @description 涉及的场所和危害因素interface
 */
public interface DeclareWorkplaceService {
    /**
     * @description 根据申报id查询申报涉及的危害场所和危害因素集合
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listDeclareWorkplaces(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据企业id查询所有的作业场所和危害因素集合
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listSelectDeclareWorkplaces(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据企业id查询未申报的场所危害集合
     * @param enterpriseId 企业id
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseDeclareWorkplace> 危害场所和危害因素集合
     */
    List<EnterpriseDeclareWorkplace> listUnselectedDeclareWorkplaces(Long enterpriseId);

    /**
     * @description 批量插入场所危害集合
     * @param enterpriseDeclareWorkplaces 场所危害集合
     * @return int 插入成功的数目
     */
    int insertBatch(List<EnterpriseDeclareWorkplace> enterpriseDeclareWorkplaces);

    /**
     * @description 删除危害场所和危害因素
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteDeclareWorkplace(Long id);

    /**
     * @description 批量删除危害场所和危害因素
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteDeclareWorkplaces(Long[] ids);
}
