package com.lanware.ehs.enterprise.threesimultaneity.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityService;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneity;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseThreeSimultaneityCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 三同时管理controller
 */
@Controller
@RequestMapping("/threeSimultaneity")
public class ThreeSimultaneityController {
    /** 注入threeSimultaneityService的bean */
    @Autowired
    private ThreeSimultaneityService threeSimultaneityService;

    /**
     * @description 返回三同时管理页面
     * @return java.lang.String 返回页面路径
     */
    @RequestMapping("")
    public String list() {
        return "threeSimultaneity/list";
    }

    @RequestMapping("/listThreeSimultaneitys")
    @ResponseBody
    /**
     * @description 获取三同时result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listThreeSimultaneitys(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = threeSimultaneityService.listThreeSimultaneitys(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editThreeSimultaneity/save")
    @ResponseBody
    /**
     * @description 保存三同时信息
     * @param enterpriseThreeSimultaneityJSON 三同时json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseThreeSimultaneityJSON, @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseThreeSimultaneity enterpriseThreeSimultaneity
                = JSONObject.parseObject(enterpriseThreeSimultaneityJSON, EnterpriseThreeSimultaneity.class);
        if (enterpriseThreeSimultaneity.getId() == null) {
            enterpriseThreeSimultaneity.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseThreeSimultaneity.setProjectState(EnterpriseThreeSimultaneityCustom.STAGE_PRE);
            enterpriseThreeSimultaneity.setStatus(EnterpriseThreeSimultaneityCustom.STATUS_UNCOMPLETE);
            enterpriseThreeSimultaneity.setGmtCreate(new Date());
            if (threeSimultaneityService.insertThreeSimultaneity(enterpriseThreeSimultaneity) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseThreeSimultaneity.setGmtModified(new Date());
            if (threeSimultaneityService.updateThreeSimultaneity(enterpriseThreeSimultaneity) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteThreeSimultaneity")
    @ResponseBody
    /**
     * @description 删除三同时信息
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteThreeSimultaneity(Long id) {
        if (threeSimultaneityService.deleteThreeSimultaneity(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteThreeSimultaneitys")
    @ResponseBody
    /**
     * @description 批量删除三同时信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteThreeSimultaneitys(Long[] ids) {
        if (threeSimultaneityService.deleteThreeSimultaneitys(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    /**
     * @description 更新危害程度和三同时阶段
     * @param id 项目id
     * @param harmDegree 危害程度
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/updateHarmDegree")
    @ResponseBody
    public ResultFormat updateHarmDegree(Long id, Integer harmDegree) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("id", id);
        condition.put("projectState", EnterpriseThreeSimultaneityCustom.STAGE_DESIGN);
        condition.put("harmDegree", harmDegree);
        condition.put("gmtModified", new Date());
        if (threeSimultaneityService.updateHarmDegreeAndStateByCondition(condition) > 0) {
            return ResultFormat.success("确认危害程度成功，进入防护设施设计阶段");
        } else {
            return ResultFormat.error("确认危害程度失败");
        }
    }

    /**
     * @description 更新三同时阶段
     * @param id 项目id
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/updateProjectState")
    @ResponseBody
    public ResultFormat updateProjectState(Long id) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("id", id);
        condition.put("projectState", EnterpriseThreeSimultaneityCustom.STAGE_ACCEPTANCE);
        condition.put("gmtModified", new Date());
        if (threeSimultaneityService.updateProjectState(condition) > 0) {
            return ResultFormat.success("进入竣工验收阶段成功");
        } else {
            return ResultFormat.error("进入竣工验收阶段失败");
        }
    }

    @RequestMapping("/updateRiskClass")
    @ResponseBody
    /**
     * @description 更新风险分类并提交项目
     * @param id 要提交的项目id
     * @param riskClass 风险分类
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat updateRiskClass(Long id, Integer riskClass) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("id", id);
        condition.put("status", EnterpriseThreeSimultaneityCustom.STATUS_COMPLETE);
        condition.put("riskClass", riskClass);
        condition.put("gmtModified", new Date());
        if (threeSimultaneityService.updateStatus(condition) > 0) {
            return ResultFormat.success("项目提交成功");
        }else{
            return ResultFormat.error("项目提交失败");
        }
    }

    /**
     * @description 三同时文件上传到oss，并保存数据到数据库
     * @param threeSimultaneityFile 上传文件
     * @param enterpriseThreeSimultaneityFileJSON 三同时文件json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/fileUpload")
    @ResponseBody
    public ResultFormat fileUpload(String enterpriseThreeSimultaneityFileJSON, MultipartFile threeSimultaneityFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile
                = JSONObject.parseObject(enterpriseThreeSimultaneityFileJSON, EnterpriseThreeSimultaneityFile.class);
        enterpriseThreeSimultaneityFile.setGmtCreate(new Date());
        if (threeSimultaneityService.insertThreeSimultaneityFile(enterpriseThreeSimultaneityFile
                , threeSimultaneityFile, enterpriseUser.getEnterpriseId()) > 0) {
            return ResultFormat.success("上传文件成功");
        } else {
            return ResultFormat.error("上传文件失败");
        }
    }

    /**
     * @description 删除文件
     * @param filePath 文件路径
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/fileDelete")
    @ResponseBody
    public ResultFormat fileDelete(String filePath) {
        // 删除数据库上对应的文件数据
        if (threeSimultaneityService.deleteFileByFilePath(filePath) > 0) {
            return ResultFormat.success("删除文件成功");
        }else{
            return ResultFormat.error("删除文件失败");
        }
    }
}
