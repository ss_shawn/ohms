package com.lanware.ehs.enterprise.threesimultaneity.controller;

import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.threesimultaneity.service.PreEvaluationService;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityService;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneity;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 三同时管理预评价阶段controller
 */
@Controller
@RequestMapping("/threeSimultaneity/preEvaluation")
public class PreEvaluationController {
    /** 注入preEvaluationService的bean */
    @Autowired
    private PreEvaluationService preEvaluationService;
    /** 注入threeSimultaneityService的bean */
    @Autowired
    private ThreeSimultaneityService threeSimultaneityService;
    
    /**
     * @description 查询预评价阶段list
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listPreEvaluations")
    @ResponseBody
    public ResultFormat listPreEvaluations(Long projectId, Integer projectState) {
        Map<String ,Object> result = new HashMap<String, Object>();
        List<EnterpriseThreeSimultaneityFile> enterpriseThreeSimultaneityFileList
                = preEvaluationService.listPreEvaluations(projectId, projectState);
        for (EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile : enterpriseThreeSimultaneityFileList) {
            result.put(enterpriseThreeSimultaneityFile.getFileType(), enterpriseThreeSimultaneityFile);
        }
        // 根据id查询三同时项目，并保存危害程度
        EnterpriseThreeSimultaneity enterpriseThreeSimultaneity
                = threeSimultaneityService.getThreeSimultaneityById(projectId);
        result.put("harmDegree", enterpriseThreeSimultaneity.getHarmDegree());
        return ResultFormat.success(result);
    }
}
