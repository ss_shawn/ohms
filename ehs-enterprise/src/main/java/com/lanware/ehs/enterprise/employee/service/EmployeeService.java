package com.lanware.ehs.enterprise.employee.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.EmployeeHarmContactHistory;
import com.lanware.ehs.pojo.EmployeeMedicalHistory;
import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.pojo.custom.EnterpriseEmployeeCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface EmployeeService {

    /**
     * 分页查询企业员工
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<EnterpriseEmployeeCustom> queryEnterpriseEmployeeByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 保存人员
     * @param enterpriseEmployee 企业人员
     * @param harmContactHistoryList 职业危害因素接触史集合
     * @param harmContactHistoryDeletedIds 职业危害因素接触史删除id集合
     * @param medicalHistoryList 既往病史集合
     * @param medicalHistoryDeletedIds 既往病史删除id集合
     * @param fileMap 文件集合
     * @return 更新数量
     */
    int saveEnterpriseEmployee(EnterpriseEmployee enterpriseEmployee, List<EmployeeHarmContactHistory> harmContactHistoryList, List<Long> harmContactHistoryDeletedIds, List<EmployeeMedicalHistory> medicalHistoryList, List<Long> medicalHistoryDeletedIds, Map<String, MultipartFile> fileMap);

    /**
     * 根据id删除人员
     * @param ids id集合
     * @return 更新数量
     */
    int deleteEnterpriseEmployeeByIds(List<Long> ids);

    /**
     * 人员上岗
     * @param parameters 参数
     * @return 操作结果
     */
    int takeUpPost(JSONObject parameters);
    /**
     * 获取电子档案号
     * @param enterpriseId
     * @return
     */
     String getElectronArchiveNumber(long enterpriseId);

    /**
     * 查询在岗员工
     * @return
     */
     List<EnterpriseEmployeeCustom> findWorkEmployeesByEnterpriseId(Long enterpriseId);
}
