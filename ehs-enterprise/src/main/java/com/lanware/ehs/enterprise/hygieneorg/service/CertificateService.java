package com.lanware.ehs.enterprise.hygieneorg.service;

import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CertificateService {

    /**
     * 保存资质证书
     * @param enterpriseId 企业id
     * @param enterpriseRelationFile 资质证书对象
     * @param file 附件
     * @return 更新数量
     */
    int saveCertificate(Long enterpriseId, EnterpriseRelationFile enterpriseRelationFile, MultipartFile file);

    /**
     * 删除资质证书
     * @param ids 资质证书id集合
     * @return 更新数量
     */
    int deleteCertificateByIds(List<Long> ids);
}
