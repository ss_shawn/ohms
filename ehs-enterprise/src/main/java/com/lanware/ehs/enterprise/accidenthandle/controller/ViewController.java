package com.lanware.ehs.enterprise.accidenthandle.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.accidenthandle.service.AccidentFileService;
import com.lanware.ehs.enterprise.accidenthandle.service.AccidentHandleService;
import com.lanware.ehs.pojo.EnterpriseAccidentHandle;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 事故调查处理视图
 */
@Controller("accidentHandleView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入accidentHandleService的bean */
    @Autowired
    private AccidentHandleService accidentHandleService;
    /** 注入accidentFileService的bean */
    @Autowired
    private AccidentFileService accidentFileService;

    @RequestMapping("accidentHandle/editAccidentHandle")
    /**
     * @description 新增事故调查处理信息
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addAccidentHandle() {
        JSONObject data = new JSONObject();
        EnterpriseAccidentHandle enterpriseAccidentHandle = new EnterpriseAccidentHandle();
        data.put("enterpriseAccidentHandle", enterpriseAccidentHandle);
        return new ModelAndView("accidentHandle/edit", data);
    }

    /**
     * @description 编辑事故调查处理信息
     * @param id 事故调查处理id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("accidentHandle/editAccidentHandle/{id}")
    public ModelAndView editMedical(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseAccidentHandle enterpriseAccidentHandle = accidentHandleService.getAccidentHandleById(id);
        data.put("enterpriseAccidentHandle", enterpriseAccidentHandle);
        return new ModelAndView("accidentHandle/edit", data);
    }

    @RequestMapping("accidentHandle/accidentFile/{relationId}/{relationModule}")
    /**
     * @description 返回机事故调查处理相关文件页面
     * @param relationId 检查机构id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView accidentFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("accidentHandle/accidentFile/list", data);
    }

    @RequestMapping("accidentHandle/accidentFile/addAccidentFile/{relationId}/{relationModule}")
    /**
     * @description 新增机事故调查处理相关文件
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addAccidentFile(@PathVariable Long relationId, @PathVariable String relationModule) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(relationId);
        enterpriseRelationFile.setRelationModule(relationModule);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("accidentHandle/accidentFile/edit", data);
    }

    @RequestMapping("accidentHandle/accidentFile/editAccidentFile/{id}")
    /**
     * @description 编辑机事故调查处理相关文件
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editAccidentFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = accidentFileService.getAccidentFileById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("accidentHandle/accidentFile/edit", data);
    }
}
