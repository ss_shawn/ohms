package com.lanware.ehs.enterprise.employee.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.EmployeeHarmContactHistory;
import com.lanware.ehs.pojo.EmployeeMedicalHistory;
import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.pojo.custom.EmployeeMedicalCustom;
import com.lanware.ehs.pojo.custom.EnterpriseEmployeeCustom;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultAllCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProvideProtectArticlesCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface EmployeeArchivesService {
    /**
     * 根据员工ID获取职业病诊断
     * @param employeeId 员工ID
     * @return 职业病诊断列表
     */
     List<EmployeeDiseaseDiagnosi> getEmployeeDiseaseDiagnosiByEmployeeId(Long employeeId);
    /**
     * 根据员工ID获取历次职业健康检查结果及处理情况
     * @param employeeId 员工ID
     * @return 体检列表
     */
     List<EmployeeMedicalCustom> getEmployeeMedicalByEmployeeId(Long employeeId);

    /**
     * 根据员工ID获取工作场所职业病危害因素检测结果
     * @param employeeId 员工ID
     * @return 工作场所职业病危害因素检测结果列表
     */
     List<EnterpriseOutsideCheckResultAllCustom> getWorkplaceOutsideCheckByEmployeeId(Long employeeId);

    /**
     * 根据员工ID获取防护用品发放记录
     * @param condition 员工ID
     * @return 工作场所职业病危害因素检测结果列表
     */
    Page<EnterpriseProvideProtectArticlesCustom> getEmployeeProvideProtectList(Map<String, Object> condition);
}
