package com.lanware.ehs.enterprise.check.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.check.service.SchematicDiagramService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 监测、检测点示意图controller
 */
@Controller
@RequestMapping("/schematicDiagram")
public class SchematicDiagramController {
    /**  注入schematicDiagramService的bean */
    @Autowired
    private SchematicDiagramService schematicDiagramService;
    
    /**
     * @description 获取监测、检测点示意图result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param relationModule 关联模块
     * @param keyword 关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/listSchematicDiagrams")
    @ResponseBody
    public ResultFormat listSchematicDiagrams(Integer pageNum, Integer pageSize, String relationModule
            , String keyword, @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("relationModule", relationModule);
        condition.put("relationId", enterpriseUser.getEnterpriseId());
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = schematicDiagramService.listSchematicDiagrams(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /**
     * @description 保存监测、检测点示意图，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param schematicDiagram 监测、检测点示意图
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/editSchematicDiagram/save")
    @ResponseBody
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile schematicDiagram) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (schematicDiagramService.insertSchematicDiagram(enterpriseRelationFile, schematicDiagram) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (schematicDiagramService.updateSchematicDiagram(enterpriseRelationFile, schematicDiagram) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteSchematicDiagram")
    @ResponseBody
    /**
     * @description 删除监测、检测点示意图
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteSchematicDiagram(Long id, String filePath) {
        if (schematicDiagramService.deleteSchematicDiagram(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteSchematicDiagrams")
    @ResponseBody
    /**
     * @description 批量删除监测、检测点示意图
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteSchematicDiagrams(Long[] ids) {
        if (schematicDiagramService.deleteSchematicDiagrams(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
