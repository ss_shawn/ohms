package com.lanware.ehs.enterprise.notify.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.notify.service.EnterpriseDiseaseDiagnosiNotifyService;
import com.lanware.ehs.mapper.EnterpriseDiseaseDiagnosiNotifyMapper;
import com.lanware.ehs.mapper.EnterpriseDiseaseDiagnosiWaitNotifyMapper;
import com.lanware.ehs.mapper.custom.EnterpriseDiseaseDiagnosiNotifyCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseDiseaseDiagnosiWaitNotifyCustomMapper;
import com.lanware.ehs.pojo.EnterpriseDiseaseDiagnosiNotify;
import com.lanware.ehs.pojo.EnterpriseDiseaseDiagnosiWaitNotify;
import com.lanware.ehs.pojo.EnterpriseDiseaseDiagnosiWaitNotifyExample;
import com.lanware.ehs.pojo.custom.EnterpriseDiseaseDiagnosiNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseDiseaseDiagnosiWaitNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

@Service
public class EnterpriseDiseaseDiagnosiNotifyServiceImpl implements EnterpriseDiseaseDiagnosiNotifyService {

    @Autowired
    /** 注入EnterpriseDiseaseDiagnosiWaitNotifyMapper的bean */
    private EnterpriseDiseaseDiagnosiWaitNotifyMapper enterpriseDiseaseDiagnosiWaitNotifyMapper;
    @Autowired
    /** 注入EnterpriseDiseaseDiagnosiNotifyMapper的bean */
    private EnterpriseDiseaseDiagnosiNotifyMapper enterpriseDiseaseDiagnosiNotifyMapper;
    @Autowired
    /** 注入EnterpriseDiseaseDiagnosiNotifyCustomMapper的bean */
    private EnterpriseDiseaseDiagnosiNotifyCustomMapper enterpriseDiseaseDiagnosiNotifyCustomMapper;
    @Autowired
    /** 注入EnterpriseDiseaseDiagnosiWaitNotifyCustomMapper的bean */
    private EnterpriseDiseaseDiagnosiWaitNotifyCustomMapper enterpriseDiseaseDiagnosiWaitNotifyCustomMapper;
    @Autowired
    private OSSTools ossTools;
    @Override
    /**
     * @description 根据id查询待体检结果告知
     * @param id 待体检结果告知id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    public EnterpriseDiseaseDiagnosiWaitNotify getEnterpriseDiseaseDiagnosiWaitNotifyById(Long id) {
        return enterpriseDiseaseDiagnosiWaitNotifyMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 根据id查询待体检结果告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public Page<EnterpriseDiseaseDiagnosiWaitNotifyCustom> findEnterpriseDiseaseDiagnosiWaitNotifyList(Map<String, Object> condition, QueryRequest request){
       if(request != null){
           //分页设置
           Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
           String sortField = request.getField();
           if(StringUtils.isNotBlank(sortField)) {
               sortField = EhsUtil.camelToUnderscore(sortField);
               page.setOrderBy(sortField + " "+request.getOrder());
           }
       }
        return enterpriseDiseaseDiagnosiWaitNotifyCustomMapper.findEnterpriseDiseaseDiagnosiWaitNotifyList(condition);
    }



    /**
     * 保存体检结果告知,
     * @param enterpriseDiseaseDiagnosiNotifyCustom
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int saveEnterpriseDiseaseDiagnosiNotify(EnterpriseDiseaseDiagnosiNotifyCustom enterpriseDiseaseDiagnosiNotifyCustom,MultipartFile checkResultProve){
        int n=0;
        //新增数据先保存，以便获取id
        if(enterpriseDiseaseDiagnosiNotifyCustom.getId()==null) {//
            enterpriseDiseaseDiagnosiNotifyCustom.setGmtCreate(new Date());
            n=enterpriseDiseaseDiagnosiNotifyMapper.insert(enterpriseDiseaseDiagnosiNotifyCustom);
            //修改待发放状态
            EnterpriseDiseaseDiagnosiWaitNotify enterpriseDiseaseDiagnosiWaitNotify=new EnterpriseDiseaseDiagnosiWaitNotify();
            enterpriseDiseaseDiagnosiWaitNotify.setStatus(1);
            enterpriseDiseaseDiagnosiWaitNotify.setId(enterpriseDiseaseDiagnosiNotifyCustom.getDiseaseDiagnosiWaitId());
            enterpriseDiseaseDiagnosiWaitNotifyMapper.updateByPrimaryKeySelective(enterpriseDiseaseDiagnosiWaitNotify);
        }
        if (checkResultProve != null){
            //上传了新的聘用书
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseDiseaseDiagnosiNotifyCustom.getNotifyFile())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseDiseaseDiagnosiNotifyCustom.getNotifyFile());
            }
            //上传并保存新文件
            String employFilePath = "/" + enterpriseDiseaseDiagnosiNotifyCustom.getEnterpriseId() + "/" + ModuleName.MEDICALNOTIFY.getValue() + "/" + enterpriseDiseaseDiagnosiNotifyCustom.getId() + "/" + checkResultProve.getOriginalFilename();
            try {
                ossTools.uploadStream(employFilePath, checkResultProve.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传告知书失败", e);
            }
            enterpriseDiseaseDiagnosiNotifyCustom.setNotifyFile(employFilePath);
        }
        enterpriseDiseaseDiagnosiNotifyCustom.setGmtModified(new Date());
        n=enterpriseDiseaseDiagnosiNotifyMapper.updateByPrimaryKey(enterpriseDiseaseDiagnosiNotifyCustom);
        return n;
    }
      /**
     * 通过id获取体检结果告知
     * @param id 体检结果告知id
     * @return体检结果告知
     */
    @Override
    public EnterpriseDiseaseDiagnosiNotifyCustom getEnterpriseDiseaseDiagnosiNotifyCustomByID(long id){
        return  enterpriseDiseaseDiagnosiNotifyCustomMapper.selectByPrimaryKey(id);
    }
    /**
     * 删除待体检结果告知,
     * @param id
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int deleteEnterpriseDiseaseDiagnosiNotify(long id){
        //获取体检结果告知来删除文件
        EnterpriseDiseaseDiagnosiNotify enterpriseDiseaseDiagnosiNotify=enterpriseDiseaseDiagnosiNotifyMapper.selectByPrimaryKey(id);
        String notifyFile=enterpriseDiseaseDiagnosiNotify.getNotifyFile();
        if (!org.springframework.util.StringUtils.isEmpty(notifyFile)){
            ossTools.deleteOSS(notifyFile);
        }
        //修改待告知状态
        EnterpriseDiseaseDiagnosiWaitNotifyExample enterpriseDiseaseDiagnosiWaitNotifyExample=new EnterpriseDiseaseDiagnosiWaitNotifyExample();
        enterpriseDiseaseDiagnosiWaitNotifyExample.createCriteria().andDiagnosiIdEqualTo(enterpriseDiseaseDiagnosiNotify.getDiagnosiId());
        EnterpriseDiseaseDiagnosiWaitNotify enterpriseDiseaseDiagnosiWaitNotify=new EnterpriseDiseaseDiagnosiWaitNotify();
        enterpriseDiseaseDiagnosiWaitNotify.setStatus(0);
        enterpriseDiseaseDiagnosiWaitNotifyMapper.updateByExampleSelective(enterpriseDiseaseDiagnosiWaitNotify,enterpriseDiseaseDiagnosiWaitNotifyExample);
        //删除待体检结果告知
        int n=enterpriseDiseaseDiagnosiNotifyMapper.deleteByPrimaryKey(id);
        return n;
    }
    /**
     * 通过待体检结果告知ID获取待体检结果告知的人员ID，体检ID，体检日期信息,放入体检结果告知返回
     * @param id
     * @return 体检结果告知
     */
    @Override
    public EnterpriseDiseaseDiagnosiNotifyCustom getEnterpriseDiseaseDiagnosiNotifyCustomByWaitID(long id){
        //查询出待体检结果告知信息
        EnterpriseDiseaseDiagnosiWaitNotifyCustom enterpriseDiseaseDiagnosiWaitNotify=enterpriseDiseaseDiagnosiWaitNotifyCustomMapper.selectByPrimaryKey(id);
        //创建体检结果告知，并把人员ID，体检ID，体检日期设置
        EnterpriseDiseaseDiagnosiNotifyCustom enterpriseDiseaseDiagnosiNotifyCustom=new EnterpriseDiseaseDiagnosiNotifyCustom();
        enterpriseDiseaseDiagnosiNotifyCustom.setEmployeeName(enterpriseDiseaseDiagnosiWaitNotify.getEmployeeName());
        enterpriseDiseaseDiagnosiNotifyCustom.setEnterpriseId(enterpriseDiseaseDiagnosiWaitNotify.getEnterpriseId());
        enterpriseDiseaseDiagnosiNotifyCustom.setDiagnosiDate(enterpriseDiseaseDiagnosiWaitNotify.getDiagnosiDate());
        enterpriseDiseaseDiagnosiNotifyCustom.setDiagnosiId(enterpriseDiseaseDiagnosiWaitNotify.getDiagnosiId());
        return  enterpriseDiseaseDiagnosiNotifyCustom;
    }
    /**
     * @description 根据条件查询体检结果告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public Page<EnterpriseDiseaseDiagnosiNotifyCustom> findEnterpriseDiseaseDiagnosiNotifyList(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return enterpriseDiseaseDiagnosiNotifyCustomMapper.findEnterpriseDiseaseDiagnosiNotifyList(condition);
    }
}
