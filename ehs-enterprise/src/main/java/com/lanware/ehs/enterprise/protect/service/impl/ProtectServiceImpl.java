package com.lanware.ehs.enterprise.protect.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.protect.service.ProtectService;
import com.lanware.ehs.mapper.BaseHarmFactorMapper;
import com.lanware.ehs.mapper.EnterpriseProtectArticlesMapper;
import com.lanware.ehs.mapper.EnterpriseProtectHarmFactorRelationMapper;
import com.lanware.ehs.mapper.PostProtectArticlesMapper;
import com.lanware.ehs.mapper.custom.EnterpriseProtectArticlesCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 年度计划interface实现类
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ProtectServiceImpl implements ProtectService {
    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private EnterpriseProtectArticlesMapper enterpriseProtectArticlesMapper;
    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private EnterpriseProtectArticlesCustomMapper enterpriseProtectArticlesCustomMapper;

    @Autowired
    /** 注入EnterpriseProtectArticlesMapper的bean */
    private BaseHarmFactorMapper baseHarmFactorMapper;
    @Autowired
    /** 注入EnterpriseProtectHarmFactorRelationMapper的bean */
    private EnterpriseProtectHarmFactorRelationMapper enterpriseProtectHarmFactorRelationMapper;
    @Autowired
    private PostProtectArticlesMapper postProtectArticlesMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    

    @Override
    /**
     * @description 根据id查询防护用品
     * @param id 防护用品id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    public EnterpriseProtectArticles getEnterpriseProtectArticlesById(Long id) {
        return enterpriseProtectArticlesMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 根据id查询防护用品
     * @param condition 条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public  Page<EnterpriseProtectArticlesCustom>  findProtectList(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return enterpriseProtectArticlesCustomMapper.findProtectList(condition);
    }

    /***
     * 获取全部危害因素
     * @return危害因素列表
     */
    @Override
    public List<BaseHarmFactor> getAllBaseHarmFactor(String keyword){
        BaseHarmFactorExample baseHarmFactorExample=new BaseHarmFactorExample();
        if(keyword!=null&&!"".equals(keyword)){
            baseHarmFactorExample.createCriteria().andNameLike("%"+keyword+"%");
        }
        baseHarmFactorExample.setOrderByClause("name");//按名称排序
        return baseHarmFactorMapper.selectByExample(baseHarmFactorExample);
    }

    /**
     * 保存防护用品,
     * @param enterpriseProtectArticles 防护用品 signatureRecord 图片
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int saveEnterpriseProtectArticles(EnterpriseProtectArticlesCustom enterpriseProtectArticles,MultipartFile image,MultipartFile certificatePath){
        int n=0;
        if(enterpriseProtectArticles.getId()!=null){//修改，先删除危害因素
            EnterpriseProtectHarmFactorRelationExample enterpriseProtectHarmFactorRelationExample=new EnterpriseProtectHarmFactorRelationExample();
            enterpriseProtectHarmFactorRelationExample.createCriteria().andProtectIdEqualTo(enterpriseProtectArticles.getId());
            enterpriseProtectHarmFactorRelationMapper.deleteByExample(enterpriseProtectHarmFactorRelationExample);
//            enterpriseProtectArticles.setGmtModified(new Date());
//            n=enterpriseProtectArticlesMapper.updateByPrimaryKey(enterpriseProtectArticles);
        }else{
            enterpriseProtectArticles.setGmtCreate(new Date());
            n=enterpriseProtectArticlesMapper.insert(enterpriseProtectArticles);
        }
        //上传图片
        if (certificatePath != null){
            //上传了新的聘用书
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseProtectArticles.getCertificatePath())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseProtectArticles.getCertificatePath());
            }
            //上传并保存新文件
            String imageFilePath = "/" + enterpriseProtectArticles.getEnterpriseId() + "/" + ModuleName.PROTECTARTICLES.getValue() + "/" + enterpriseProtectArticles.getId() + "/" + certificatePath.getOriginalFilename();
            try {
                ossTools.uploadStream(imageFilePath, certificatePath.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传合格证书失败", e);
            }
            enterpriseProtectArticles.setCertificatePath(imageFilePath);
        }
        //上传图片
        if (image != null){
            //上传了新的聘用书
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseProtectArticles.getImage())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseProtectArticles.getImage());
            }
            //上传并保存新文件
            String imageFilePath = "/" + enterpriseProtectArticles.getEnterpriseId() + "/" + ModuleName.PROTECTARTICLES.getValue() + "/" + enterpriseProtectArticles.getId() + "/" + image.getOriginalFilename();
            try {
                ossTools.uploadStream(imageFilePath, image.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传图片失败", e);
            }
            enterpriseProtectArticles.setImage(imageFilePath);
        }


        enterpriseProtectArticles.setGmtModified(new Date());
        n=enterpriseProtectArticlesMapper.updateByPrimaryKey(enterpriseProtectArticles);

        //获取防护用品ID
        String harmFactorId=enterpriseProtectArticles.getHarmFactorId();

        //插入危害因素关联表
        if(harmFactorId!=null&&!"".equals(harmFactorId)){
            String[] harmFactorIds=harmFactorId.split(",");
            for(int i=0;i<harmFactorIds.length;i++){
                String curHarmFactorId=harmFactorIds[i];
                EnterpriseProtectHarmFactorRelation enterpriseProtectHarmFactorRelation=new EnterpriseProtectHarmFactorRelation();
                enterpriseProtectHarmFactorRelation.setHarmFactorId(Long.parseLong(curHarmFactorId));
                enterpriseProtectHarmFactorRelation.setProtectId(enterpriseProtectArticles.getId());
                enterpriseProtectHarmFactorRelationMapper.insert(enterpriseProtectHarmFactorRelation);
            }
        }
        return n;
    }
    /**
     * 修改防护用品,
     * @param enterpriseProtectArticles
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int updateEnterpriseProtectArticles(EnterpriseProtectArticlesCustom enterpriseProtectArticles){
        int n=enterpriseProtectArticlesMapper.insert(enterpriseProtectArticles);
        //获取防护用品ID
        String harmFactorId=enterpriseProtectArticles.getHarmFactorId();
        //先删除防护用品下的危害因素//修改使用
        EnterpriseProtectHarmFactorRelationExample enterpriseProtectHarmFactorRelationExample=new EnterpriseProtectHarmFactorRelationExample();
        enterpriseProtectHarmFactorRelationExample.createCriteria().andProtectIdEqualTo(enterpriseProtectArticles.getId());
        enterpriseProtectHarmFactorRelationMapper.deleteByExample(enterpriseProtectHarmFactorRelationExample);
        //插入危害因素关联表
        if(harmFactorId!=null&&!"".equals(harmFactorId)){
            String[] harmFactorIds=harmFactorId.split(",");
            for(int i=0;i<harmFactorIds.length;i++){
                String curHarmFactorId=harmFactorIds[i];
                EnterpriseProtectHarmFactorRelation enterpriseProtectHarmFactorRelation=new EnterpriseProtectHarmFactorRelation();
                enterpriseProtectHarmFactorRelation.setHarmFactorId(Long.parseLong(curHarmFactorId));
                enterpriseProtectHarmFactorRelation.setProtectId(enterpriseProtectArticles.getId());
                enterpriseProtectHarmFactorRelationMapper.insert(enterpriseProtectHarmFactorRelation);
            }
        }
        return n;
    }

    /**
     * 通过id获取防护用品
     * @param id
     * @return防护用品
     */
    @Override
    public EnterpriseProtectArticlesCustom getEnterpriseProtectArticlesCustomByID(long id){
        return  enterpriseProtectArticlesCustomMapper.selectByPrimaryKey(id);
    }
    /**
     * 删除防护用品,
     * @param id
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int deleteEnterpriseProtectArticles(long id){
        //判断是否使用
        PostProtectArticlesExample postProtectArticlesExample=new PostProtectArticlesExample();
        postProtectArticlesExample.createCriteria().andProtectArticlesIdEqualTo(id);
        long num=postProtectArticlesMapper.countByExample(postProtectArticlesExample);
        if(num>0){//已使用，不能删除
            return -8;
        }
        //删除防护用品

        EnterpriseProtectArticles enterpriseProtectArticles=  enterpriseProtectArticlesMapper.selectByPrimaryKey(id);
        if (!org.springframework.util.StringUtils.isEmpty(enterpriseProtectArticles.getCertificatePath())){
            //删除文件
            ossTools.deleteOSS(enterpriseProtectArticles.getCertificatePath());
        }
        if (!org.springframework.util.StringUtils.isEmpty(enterpriseProtectArticles.getImage())){
            //删除文件
            ossTools.deleteOSS(enterpriseProtectArticles.getImage());
        }

        int n=enterpriseProtectArticlesMapper.deleteByPrimaryKey(id);
        //删除防护用品下的危害因素
        EnterpriseProtectHarmFactorRelationExample enterpriseProtectHarmFactorRelationExample=new EnterpriseProtectHarmFactorRelationExample();
        enterpriseProtectHarmFactorRelationExample.createCriteria().andProtectIdEqualTo(id);
        enterpriseProtectHarmFactorRelationMapper.deleteByExample(enterpriseProtectHarmFactorRelationExample);

        return n;
    }

    /**
     * 上传合格证书
     * @param id 防护用品id
     * @param certificate 合格证书
     * @return int 更新成功的数目
     */
    @Override
    public int uploadCertificate(Long id, MultipartFile certificate) {
        EnterpriseProtectArticles enterpriseProtectArticles = enterpriseProtectArticlesMapper.selectByPrimaryKey(id);
        // 上传合格证书
        if (certificate != null){
            // 上传了新的合格证书
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseProtectArticles.getCertificatePath())){
                // 删除旧文件
                ossTools.deleteOSS(enterpriseProtectArticles.getCertificatePath());
            }
            // 上传并保存新文件
            String certificatePath = "/" + enterpriseProtectArticles.getEnterpriseId() + "/" + ModuleName.PROTECTARTICLES.getValue() + "/" + enterpriseProtectArticles.getId() + "/" + certificate.getOriginalFilename();
            try {
                ossTools.uploadStream(certificatePath, certificate.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传合格证书失败", e);
            }
            enterpriseProtectArticles.setCertificatePath(certificatePath);
        }
        enterpriseProtectArticles.setGmtModified(new Date());
        return enterpriseProtectArticlesMapper.updateByPrimaryKey(enterpriseProtectArticles);
    }

    /**
     * 上传图片
     * @param id 防护用品id
     * @param image 图片
     * @return int 更新成功的数目
     */
    @Override
    public int uploadImage(Long id, MultipartFile image) {
        EnterpriseProtectArticles enterpriseProtectArticles = enterpriseProtectArticlesMapper.selectByPrimaryKey(id);
        // 上传图片
        if (image != null){
            // 上传了新的图片
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseProtectArticles.getImage())){
                // 删除旧文件
                ossTools.deleteOSS(enterpriseProtectArticles.getImage());
            }
            // 上传并保存新文件
            String imagePath = "/" + enterpriseProtectArticles.getEnterpriseId() + "/" + ModuleName.PROTECTARTICLES.getValue() + "/" + enterpriseProtectArticles.getId() + "/" + image.getOriginalFilename();
            try {
                ossTools.uploadStream(imagePath, image.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传图片失败", e);
            }
            enterpriseProtectArticles.setImage(imagePath);
        }
        enterpriseProtectArticles.setGmtModified(new Date());
        return enterpriseProtectArticlesMapper.updateByPrimaryKey(enterpriseProtectArticles);
    }
}
