package com.lanware.ehs.enterprise.inspection.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.inspection.service.RectificationService;
import com.lanware.ehs.mapper.EnterpriseInspectionRectificationMapper;
import com.lanware.ehs.pojo.EnterpriseInspectionRectification;
import com.lanware.ehs.pojo.EnterpriseInspectionRectificationExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @description 整改记录interface实现类
 */
@Service
public class RectificationServiceImpl implements RectificationService {
    /** 注入enterpriseInspectionRectificationMapper的bean */
    @Autowired
    private EnterpriseInspectionRectificationMapper enterpriseInspectionRectificationMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    /**
     * @description 查询整改记录list
     * @param inspectionId 日常检查id
     * @return 返回包含enterpriseInspectionRectifications的result
     */
    @Override
    public JSONObject listRectifications(Long inspectionId) {
        JSONObject result = new JSONObject();
        EnterpriseInspectionRectificationExample enterpriseInspectionRectificationExample
                = new EnterpriseInspectionRectificationExample();
        EnterpriseInspectionRectificationExample.Criteria criteria
                = enterpriseInspectionRectificationExample.createCriteria();
        criteria.andInspectionIdEqualTo(inspectionId);
        List<EnterpriseInspectionRectification> enterpriseInspectionRectifications
                = enterpriseInspectionRectificationMapper.selectByExample(enterpriseInspectionRectificationExample);
        result.put("enterpriseInspectionRectifications", enterpriseInspectionRectifications);
        return result;
    }

    /**
     * @description 根据id查询整改记录
     * @param id
     * @return 返回EnterpriseInspectionRectification
     */
    @Override
    public EnterpriseInspectionRectification getRectificationById(Long id) {
        return enterpriseInspectionRectificationMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增整改记录，并上传文件到oss服务器
     * @param enterpriseInspectionRectification 整改记录pojo
     * @param rectificationFile 整改文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    @Transactional
    public int insertRectification(EnterpriseInspectionRectification enterpriseInspectionRectification
            , MultipartFile rectificationFile, Long enterpriseId) {
        // 新增数据先保存，以便获取id
        enterpriseInspectionRectificationMapper.insert(enterpriseInspectionRectification);
        // 上传并保存整改记录
        if (rectificationFile != null){
            String rectification = "/" + enterpriseId + "/" + ModuleName.INSPECTION.getValue()
                    + "/" + enterpriseInspectionRectification.getInspectionId()
                    + "/" + enterpriseInspectionRectification.getId() + "/" + rectificationFile.getOriginalFilename();
            try {
                ossTools.uploadStream(rectification, rectificationFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传整改记录失败", e);
            }
            enterpriseInspectionRectification.setFileName(rectificationFile.getOriginalFilename());
            enterpriseInspectionRectification.setFilePath(rectification);
        }
        return enterpriseInspectionRectificationMapper.updateByPrimaryKeySelective(enterpriseInspectionRectification);
}

    /**
     * @description 更新整改记录，并上传文件到oss服务器
     * @param enterpriseInspectionRectification 整改记录pojo
     * @param rectificationFile 整改文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    public int updateRectification(EnterpriseInspectionRectification enterpriseInspectionRectification
            , MultipartFile rectificationFile, Long enterpriseId) {
        // 上传并保存整改记录
        if (rectificationFile != null) {
            // 上传了新的整改记录
            if (!StringUtils.isEmpty(enterpriseInspectionRectification.getFilePath())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseInspectionRectification.getFilePath());
            }
            String rectification = "/" + enterpriseId + "/" + ModuleName.INSPECTION.getValue()
                    + "/" + enterpriseInspectionRectification.getInspectionId()
                    + "/" + enterpriseInspectionRectification.getId() + "/" + rectificationFile.getOriginalFilename();
            try {
                ossTools.uploadStream(rectification, rectificationFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传整改记录失败", e);
            }
            enterpriseInspectionRectification.setFileName(rectificationFile.getOriginalFilename());
            enterpriseInspectionRectification.setFilePath(rectification);
        }
        return enterpriseInspectionRectificationMapper.updateByPrimaryKeySelective(enterpriseInspectionRectification);
    }

    /**
     * @description 删除整改记录
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    @Override
    public int deleteRectification(Long id, String filePath) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(filePath);
        // 删除数据库中对应的数据
        return enterpriseInspectionRectificationMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除整改记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteRectifications(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseInspectionRectification enterpriseInspectionRectification
                    = enterpriseInspectionRectificationMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseInspectionRectification.getFilePath());
        }
        EnterpriseInspectionRectificationExample enterpriseInspectionRectificationExample
                = new EnterpriseInspectionRectificationExample();
        EnterpriseInspectionRectificationExample.Criteria criteria
                = enterpriseInspectionRectificationExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseInspectionRectificationMapper.deleteByExample(enterpriseInspectionRectificationExample);
    }
}
