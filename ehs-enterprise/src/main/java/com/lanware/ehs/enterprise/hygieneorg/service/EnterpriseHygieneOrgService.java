package com.lanware.ehs.enterprise.hygieneorg.service;

import com.lanware.ehs.pojo.EnterpriseHygieneOrg;

import java.lang.reflect.InvocationTargetException;

public interface EnterpriseHygieneOrgService  {
    EnterpriseHygieneOrg selectByPrimaryKey(Long id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

}