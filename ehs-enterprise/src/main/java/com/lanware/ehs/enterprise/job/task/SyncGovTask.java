package com.lanware.ehs.enterprise.job.task;

import com.lanware.ehs.common.utils.DateUtil;
import com.lanware.ehs.enterprise.basicinfo.service.BasicInfoService;
import com.lanware.ehs.enterprise.government.service.GovernmentService;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * 同步局端任务
 */
@Service
@Slf4j
public class SyncGovTask {
    @Autowired
    private BasicInfoService basicInfoService;

    @Autowired
    private GovernmentService governmentService;
    /**
     * 定时执行同步局端任务
     */
    public void executeSyncGovTask(){
        try {
            //查找企业数据
            EnterpriseBaseinfo enterpriseBaseinfo = new EnterpriseBaseinfo();
            enterpriseBaseinfo.setStatus(0);
            List<EnterpriseBaseinfo> list = basicInfoService.find(enterpriseBaseinfo);
            for(EnterpriseBaseinfo baseinfo : list){
                if(("自动").equals(baseinfo.getUpdateWay())){
                    //如果自动取出最后一次更新时间和更新周期
                    Integer updateFrequency = baseinfo.getUpdateFrequency();
                    Date lastUpdateTime = baseinfo.getLastUpdateTime();
                    //如果最后一次更新时间为null，则立即执行更新
                    if(lastUpdateTime == null){
                        governmentService.uploadGovernment(baseinfo.getId());
                    }else{
                        LocalDateTime updateTime = DateUtil.date2LocalDateTime(lastUpdateTime);
                        LocalDate update = updateTime.toLocalDate();
                        update = update.plusDays(updateFrequency);
                        //获取当前日期
                        LocalDate currentDate = LocalDate.now();
                        if(currentDate.isEqual(update) || currentDate.isAfter(update)){
                            governmentService.uploadGovernment(baseinfo.getId());
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("定时同步局端数据失败",e);
        }
    }
}
