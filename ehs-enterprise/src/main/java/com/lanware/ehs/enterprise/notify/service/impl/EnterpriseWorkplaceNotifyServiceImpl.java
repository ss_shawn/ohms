package com.lanware.ehs.enterprise.notify.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.notify.service.EnterpriseWorkplaceNotifyService;
import com.lanware.ehs.mapper.EnterpriseWorkplaceNotifyMapper;
import com.lanware.ehs.mapper.EnterpriseWorkplaceWaitNotifyMapper;
import com.lanware.ehs.mapper.custom.EnterpriseWorkplaceNotifyCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseWorkplaceWaitNotifyCustomMapper;
import com.lanware.ehs.pojo.EnterpriseWorkplaceNotify;
import com.lanware.ehs.pojo.EnterpriseWorkplaceWaitNotify;
import com.lanware.ehs.pojo.EnterpriseWorkplaceWaitNotifyExample;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceWaitNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

@Service
public class EnterpriseWorkplaceNotifyServiceImpl implements EnterpriseWorkplaceNotifyService {

    @Autowired
    /** 注入EnterpriseWorkplaceWaitNotifyMapper的bean */
    private EnterpriseWorkplaceWaitNotifyMapper enterpriseWorkplaceWaitNotifyMapper;
    @Autowired
    /** 注入EnterpriseWorkplaceNotifyMapper的bean */
    private EnterpriseWorkplaceNotifyMapper enterpriseWorkplaceNotifyMapper;
    @Autowired
    /** 注入EnterpriseWorkplaceNotifyCustomMapper的bean */
    private EnterpriseWorkplaceNotifyCustomMapper enterpriseWorkplaceNotifyCustomMapper;
    @Autowired
    /** 注入EnterpriseWorkplaceWaitNotifyCustomMapper的bean */
    private EnterpriseWorkplaceWaitNotifyCustomMapper enterpriseWorkplaceWaitNotifyCustomMapper;
    @Autowired
    private OSSTools ossTools;
    @Override
    /**
     * @description 根据id查询待作业场所告知
     * @param id 待作业场所告知id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    public EnterpriseWorkplaceWaitNotify getEnterpriseWorkplaceWaitNotifyById(Long id) {
        return enterpriseWorkplaceWaitNotifyMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 根据id查询待作业场所告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public Page<EnterpriseWorkplaceWaitNotifyCustom> findEnterpriseWorkplaceWaitNotifyList(Map<String, Object> condition, QueryRequest request){
        if(request != null){
            Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
            String sortField = request.getField();
            if(StringUtils.isNotBlank(sortField)) {
                sortField = EhsUtil.camelToUnderscore(sortField);
                page.setOrderBy(sortField + " "+request.getOrder());
            }
        }
        return enterpriseWorkplaceWaitNotifyCustomMapper.findEnterpriseWorkplaceWaitNotifyList(condition);
    }



    /**
     * 保存作业场所告知,
     * @param enterpriseWorkplaceNotifyCustom 作业场所告知,notifyFile 检测结果公示证明  harmWarnImage 危害警示标示图片
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int saveEnterpriseWorkplaceNotify(EnterpriseWorkplaceNotifyCustom enterpriseWorkplaceNotifyCustom,MultipartFile checkResultProve,MultipartFile harmWarnImage){
        int n=0;
        //新增数据先保存，以便获取id
        if(enterpriseWorkplaceNotifyCustom.getId()==null) {//
            enterpriseWorkplaceNotifyCustom.setGmtCreate(new Date());
            n=enterpriseWorkplaceNotifyMapper.insert(enterpriseWorkplaceNotifyCustom);
            //修改待发放状态
            EnterpriseWorkplaceWaitNotify enterpriseWorkplaceWaitNotify=new EnterpriseWorkplaceWaitNotify();
            enterpriseWorkplaceWaitNotify.setStatus(1);
            enterpriseWorkplaceWaitNotify.setId(enterpriseWorkplaceNotifyCustom.getWorkplaceWaitNotifyId());
            enterpriseWorkplaceWaitNotifyMapper.updateByPrimaryKeySelective(enterpriseWorkplaceWaitNotify);
        }
        //上传检测结果公示证明
        if (checkResultProve != null){
            //上传了新的检测结果公示证明
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseWorkplaceNotifyCustom.getCheckResultProve())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseWorkplaceNotifyCustom.getCheckResultProve());
            }
            //上传并保存新文件
            String employFilePath = "/" + enterpriseWorkplaceNotifyCustom.getEnterpriseId() + "/" + ModuleName.WORKPLACENOTIFY.getValue() + "/" + enterpriseWorkplaceNotifyCustom.getId() + "/" + checkResultProve.getOriginalFilename();
            try {
                ossTools.uploadStream(employFilePath, checkResultProve.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传检测结果公示证明失败", e);
            }
            enterpriseWorkplaceNotifyCustom.setCheckResultProve(employFilePath);
        }
        //上传危害警示标示图片
        if (harmWarnImage != null){
            //上传了新的危害警示标示图片
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseWorkplaceNotifyCustom.getHarmWarnImage())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseWorkplaceNotifyCustom.getHarmWarnImage());
            }
            //上传并保存新文件
            String harmWarnImagePath = "/" + enterpriseWorkplaceNotifyCustom.getEnterpriseId() + "/" + ModuleName.WORKPLACENOTIFY.getValue() + "/" + enterpriseWorkplaceNotifyCustom.getId() + "/" + harmWarnImage.getOriginalFilename();
            try {
                ossTools.uploadStream(harmWarnImagePath, harmWarnImage.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传危害警示标示图片失败", e);
            }
            enterpriseWorkplaceNotifyCustom.setHarmWarnImage(harmWarnImagePath);
        }
        enterpriseWorkplaceNotifyCustom.setGmtModified(new Date());
        n=enterpriseWorkplaceNotifyMapper.updateByPrimaryKey(enterpriseWorkplaceNotifyCustom);
        return n;
    }
      /**
     * 通过id获取作业场所告知
     * @param id 作业场所告知id
     * @return作业场所告知
     */
    @Override
    public EnterpriseWorkplaceNotifyCustom getEnterpriseWorkplaceNotifyCustomByID(long id){
        return  enterpriseWorkplaceNotifyCustomMapper.selectByPrimaryKey(id);
    }
    /**
     * 删除待作业场所告知,
     * @param id
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int deleteEnterpriseWorkplaceNotify(long id){
        //获取作业场所告知来删除文件
        EnterpriseWorkplaceNotify enterpriseWorkplaceNotify=enterpriseWorkplaceNotifyMapper.selectByPrimaryKey(id);
        String notifyFile=enterpriseWorkplaceNotify.getCheckResultProve();
        if (!org.springframework.util.StringUtils.isEmpty(notifyFile)){
            ossTools.deleteOSS(notifyFile);
        }
        //修改待告知状态
        EnterpriseWorkplaceWaitNotifyExample enterpriseWorkplaceWaitNotifyExample=new EnterpriseWorkplaceWaitNotifyExample();
        enterpriseWorkplaceWaitNotifyExample.createCriteria().andWorkplaceIdEqualTo(enterpriseWorkplaceNotify.getWorkplaceId());
        EnterpriseWorkplaceWaitNotify enterpriseWorkplaceWaitNotify=new EnterpriseWorkplaceWaitNotify();
        enterpriseWorkplaceWaitNotify.setStatus(0);
        enterpriseWorkplaceWaitNotifyMapper.updateByExampleSelective(enterpriseWorkplaceWaitNotify,enterpriseWorkplaceWaitNotifyExample);
        //删除待作业场所告知
        int n=enterpriseWorkplaceNotifyMapper.deleteByPrimaryKey(id);
        return n;
    }
    /**
     * 通过待作业场所告知ID获取待作业场所告知的人员和岗位信息,放入作业场所告知返回
     * @param id
     * @return 作业场所告知
     */
    @Override
    public EnterpriseWorkplaceNotifyCustom getEnterpriseWorkplaceNotifyCustomByWaitID(long id){
        //查询出待发放物品信息
        EnterpriseWorkplaceWaitNotifyCustom enterpriseWorkplaceWaitNotify=enterpriseWorkplaceWaitNotifyCustomMapper.selectByPrimaryKey(id);
        EnterpriseWorkplaceNotifyCustom enterpriseWorkplaceNotifyCustom=new EnterpriseWorkplaceNotifyCustom();
        enterpriseWorkplaceNotifyCustom.setWorkplaceName(enterpriseWorkplaceWaitNotify.getWorkplaceName());
        enterpriseWorkplaceNotifyCustom.setEnterpriseId(enterpriseWorkplaceWaitNotify.getEnterpriseId());
        enterpriseWorkplaceNotifyCustom.setWorkplaceId(enterpriseWorkplaceWaitNotify.getWorkplaceId());
        enterpriseWorkplaceNotifyCustom.setCheckId(enterpriseWorkplaceWaitNotify.getCheckId());
        enterpriseWorkplaceNotifyCustom.setCheckDate(enterpriseWorkplaceWaitNotify.getCheckDate());
        return  enterpriseWorkplaceNotifyCustom;
    }
    /**
     * @description 根据条件查询作业场所告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public Page<EnterpriseWorkplaceNotifyCustom> findEnterpriseWorkplaceNotifyList(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return enterpriseWorkplaceNotifyCustomMapper.findEnterpriseWorkplaceNotifyList(condition);
    }
}
