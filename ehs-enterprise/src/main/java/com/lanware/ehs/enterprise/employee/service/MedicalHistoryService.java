package com.lanware.ehs.enterprise.employee.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.EmployeeMedicalHistory;
import com.lanware.ehs.pojo.custom.EmployeeMedicalHistoryCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface MedicalHistoryService {

    /**
     * 分页查询既往病史
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<EmployeeMedicalHistoryCustom> queryEmployeeMedicalHistoryByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 保存询既往病史
     * @param enterpriseId 企业id
     * @param employeeMedicalHistory 询既往病史
     * @param diagnosiReportFile 诊断报告
     * @return 更新数量
     */
    int saveEmployeeMedicalHistory(Long enterpriseId, EmployeeMedicalHistory employeeMedicalHistory, MultipartFile diagnosiReportFile);

    /**
     * 删除询既往病史
     * @param ids id集合
     * @return 更新数量
     */
    int deleteEmployeeMedicalHistory(List<Long> ids);
}
