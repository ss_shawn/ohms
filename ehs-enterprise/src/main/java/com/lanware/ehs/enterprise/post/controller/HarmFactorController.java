package com.lanware.ehs.enterprise.post.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.post.service.HarmFactorService;
import com.lanware.ehs.pojo.PostHarmFactor;
import com.lanware.ehs.pojo.PostHarmFactorExample;
import com.lanware.ehs.pojo.custom.PostHarmFactorCustom;
import com.lanware.ehs.pojo.custom.PostHarmFactorCustom;
import com.lanware.ehs.service.PostHarmFactorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/post/harmFactor")
public class HarmFactorController {

    private PostHarmFactorService postHarmFactorService;

    private HarmFactorService harmFactorService;

    @Autowired
    public HarmFactorController(PostHarmFactorService postHarmFactorService, HarmFactorService harmFactorService){
        this.postHarmFactorService = postHarmFactorService;
        this.harmFactorService = harmFactorService;
    }

    /**
     * 危害因素页面
     * @param postId 岗位id
     * @return 危害因素model
     */
    @RequestMapping("")
    public ModelAndView harmFactor(Long postId){
        JSONObject data = new JSONObject();
        data.put("postId", postId);
        return new ModelAndView("post/harmFactor/list", data);
    }

    /**
     * 查询危害因素集合
     * @param postId 岗位id
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public ResultFormat getList(@RequestParam Long postId, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("postId", postId);
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<PostHarmFactorCustom> page = harmFactorService.queryPostHarmFactorByCondition(0, 0, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }


    /**
     * 删除危害因素
     * @param idsJSON 危害因素id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultFormat delete(@RequestParam String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        PostHarmFactorExample postHarmFactorExample = new PostHarmFactorExample();
        PostHarmFactorExample.Criteria criteria = postHarmFactorExample.createCriteria();
        criteria.andIdIn(ids);
        try {
            int num = postHarmFactorService.deleteByExample(postHarmFactorExample);
            if (num == 0){
                return ResultFormat.error("删除危害因素失败");
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            return ResultFormat.error("删除危害因素失败");
        }
        return ResultFormat.success();
    }

    /**
     * 保存危害因素
     * @param postHarmFactorListJSON 危害因素关联对象集合
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultFormat save(String postHarmFactorListJSON){
        List<PostHarmFactor> postHarmFactorList = JSONArray.parseArray(postHarmFactorListJSON, PostHarmFactor.class);
        if (postHarmFactorList == null || postHarmFactorList.isEmpty()){
            return ResultFormat.error("选择危害因素失败");
        }
        Date createTime = new Date();
        for (PostHarmFactor postHarmFactor : postHarmFactorList){
            postHarmFactor.setGmtCreate(createTime);
        }
        int num = postHarmFactorService.insertBatch(postHarmFactorList);
        if (num == 0){
            return ResultFormat.error("选择危害因素失败");
        }
        return ResultFormat.success();
    }

    /**
     * 危害因素选择页面
     * @return 危害因素选择model
     */
    @RequestMapping("/select")
    public ModelAndView select(){
        return new ModelAndView("post/harmFactor/select");
    }

    /**
     * 查询未选择的危害因素集合
     * @param postId 岗位id
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getUnselectedHarmFactorList")
    @ResponseBody
    public ResultFormat getUnselectedHarmFactorList(@RequestParam Long postId, Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("postId", postId);
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<PostHarmFactorCustom> page = harmFactorService.queryUnSelectedPostHarmFactorByCondition(pageNum, pageSize, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return ResultFormat.success(data);
    }

}
