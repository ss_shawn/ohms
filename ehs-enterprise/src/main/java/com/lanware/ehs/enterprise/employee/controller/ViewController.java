package com.lanware.ehs.enterprise.employee.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.service.EnterpriseEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping(Constants.VIEW_PREFIX+"/employee")
@Controller("employee")
public class ViewController {
    @Autowired
    private EnterpriseEmployeeService enterpriseEmployeeService;



    /**
     * 查看个人健康监护档案页面
     * @param id 人员id
     * @return 查看个人健康监护档案model
     */
    @RequestMapping("/employeeArchives/{id}")
    public ModelAndView employeeArchives(@PathVariable Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            try {
                EnterpriseEmployee enterpriseEmployee = enterpriseEmployeeService.selectByPrimaryKey(id);

                data.put("enterpriseEmployee", enterpriseEmployee);
                data.put("titleName",enterpriseEmployee.getName()+"个人健康监护档案");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ModelAndView("employee/employeeArchives", data);
    }
    
}
