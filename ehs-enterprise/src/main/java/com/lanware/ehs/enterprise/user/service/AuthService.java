package com.lanware.ehs.enterprise.user.service;

import com.lanware.ehs.pojo.EnterpriseAuth;

import java.util.Collection;

public interface AuthService {
    /**
     * 批量保存用户角色关联信息
     * @param entityList
     * @return
     */
    void saveBatch(Collection<EnterpriseAuth> entityList);
}
