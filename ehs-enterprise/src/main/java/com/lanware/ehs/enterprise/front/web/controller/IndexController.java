package com.lanware.ehs.enterprise.front.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.google.common.collect.Maps;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.check.service.WaitCheckService;
import com.lanware.ehs.enterprise.consult.service.ConsultService;
import com.lanware.ehs.enterprise.declare.service.DeclareService;
import com.lanware.ehs.enterprise.hygieneorg.service.HygieneOrgService;
import com.lanware.ehs.enterprise.inspection.service.InspectionService;
import com.lanware.ehs.enterprise.medical.service.MedicalService;
import com.lanware.ehs.enterprise.medical.service.WaitMedicalService;
import com.lanware.ehs.enterprise.notify.service.EmployeeWorkNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseDiseaseDiagnosiNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseMedicalResultNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseWorkplaceNotifyService;
import com.lanware.ehs.enterprise.protect.service.ProvideProtectArticlesService;
import com.lanware.ehs.enterprise.remind.service.RemindService;
import com.lanware.ehs.enterprise.responsibility.service.ResponsibilityService;
import com.lanware.ehs.enterprise.threesimultaneity.service.ThreeSimultaneityService;
import com.lanware.ehs.enterprise.training.service.TrainingService;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.Enum.MedicalResultEnum;
import com.lanware.ehs.pojo.Enum.YearPlanTypeEnum;
import com.lanware.ehs.pojo.custom.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * 项目首页
 */
@RestController
@RequestMapping("index")
public class IndexController extends BaseController {

    @Autowired
    private ResponsibilityService responsibilityService;

    @Autowired
    private YearplanService yearplanService;

    @Autowired
    private TrainingService trainingService;

    @Autowired
    private DeclareService declareService;

    @Autowired
    private HygieneOrgService hygieneOrgService;

    @Autowired
    private WaitMedicalService waitMedicalService;

    @Autowired
    private ProvideProtectArticlesService provideProtectArticlesService;

    @Autowired
    private MedicalService medicalService;

    @Autowired
    private EmployeeWorkNotifyService workNotifyService;

    @Autowired
    private EnterpriseWorkplaceNotifyService workplaceNotifyService;

    @Autowired
    private EnterpriseDiseaseDiagnosiNotifyService diseaseDiagnosiNotifyService;

    @Autowired
    private EnterpriseMedicalResultNotifyService medicalResultNotifyService;

    @Autowired
    private ThreeSimultaneityService threeSimultaneityService;

    @Autowired
    private InspectionService inspectionService;

    @Autowired
    private WaitCheckService waitCheckService;

    @Autowired
    private ConsultService consultService;

    @Autowired
    private RemindService remindService;


    /**
     * 获取待办事项数据
     * @param userCustom
     * @return
     */
    @GetMapping("getWaitTaskData")
    public EhsResult getWaitTaskData(@CurrentUser EnterpriseUserCustom userCustom){
        Map<String,Integer> resultMap = Maps.newHashMap();
        Map<String,Object> parameter = Maps.newHashMap();
        //岗中待体检数量
        int gzdtj = getInPostWaitMedicalCountData(userCustom);
        resultMap.put("gzdtj",gzdtj);
        //岗前待体检数量
        int gqdtj = getPreJobWaitMedicalCountData(userCustom);
        resultMap.put("gqdtj",gqdtj);
        //离岗待体检数量
        int lgdtj = getLeavePostWaitMedicalCountData(userCustom);
        resultMap.put("lgdtj",lgdtj);

        //待诊断职业病数量
        int dzdzyb = getDiseasesDiagnosisCountData(userCustom);
        resultMap.put("dzdzyb",dzdzyb);

        //待检测项目数量
        int djcxm = getWaitCheckCountData(userCustom);
        resultMap.put("djcxm",djcxm);

        //待发放防护用品数量
        int dfffhyp = getWaitProvideProtectCountData(userCustom);
        resultMap.put("dfffhyp",dfffhyp);
        //整改过期数量
        int zggq = inspectionService.getOverdueInspectionNum(userCustom.getEnterpriseId());
        resultMap.put("zggq",zggq);

        //待更新三同时数量
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        JSONObject jsonObj = threeSimultaneityService.listThreeSimultaneitys(0,0,parameter);
        Map <String,Object> stsMap = (Map<String,Object>)jsonObj.get("threeSimultaneityStatistics");
        int dgxsts = Integer.parseInt(stsMap.get("overUpdateProjeactNum").toString());
        resultMap.put("dgxsts",dgxsts);

        //体检告知数量
        int tjgz = getMedicalResultWaitNotifyCountData(userCustom);
        resultMap.put("tjgz",tjgz);

        //岗前告知数量
        int gqgz = getWorkWaitNotifyCountData(userCustom);
        resultMap.put("gqgz",gqgz);

        //检测告知数量
        int jcgz = getWorkplaceWaitNotifyCountData(userCustom);
        resultMap.put("jcgz",jcgz);

        //职业病诊断告知数量
        int zybzdgz = getDiagnoseResultWaitNofifyCountData(userCustom);
        resultMap.put("zybzdgz",zybzdgz);
        return EhsResult.ok(resultMap);
    }


    /**
     * 统计责任制上传文件数量
     * @param userCustom
     * @return
     */
    @GetMapping("getDutyFileData")
    public EhsResult getDutyFileData(@CurrentUser EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("module",0);
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        List<EnterpriseManageFile> manageFiles =  responsibilityService.listResponsibilityFiles(parameter);
        Map<String,Object> resultMap = Maps.newHashMap();
        //已上传
        resultMap.put("uploaded",manageFiles.size());
        //应上传
        resultMap.put("total", Constants.DUTY_FILE_COUNT);
        //已上传的数据集合
        resultMap.put("data",manageFiles);
        return EhsResult.ok(resultMap);
    }

    /**
     * 获取职业健康防范制度统计数据
     * @param userCustom
     * @return
     */
    @GetMapping("getPreventiveFileData")
    public EhsResult getPreventiveFileData(@CurrentUser EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("module", 1);
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        List<EnterpriseManageFile> manageFiles =  responsibilityService.listResponsibilityFiles(parameter);
        Map<String,Object> resultMap = Maps.newHashMap();
        //已上传
        resultMap.put("uploaded",manageFiles.size());
        //应上传
        resultMap.put("total", Constants.DUTY_FILE_COUNT);
        //已上传的数据集合
        resultMap.put("data",manageFiles);
        return EhsResult.ok(resultMap);
    }

    /**
     * 获取年度计划文件上传情况
     * @param userCustom
     * @return
     */
    @GetMapping("getYearPlanCountData")
    public EhsResult getYearPlanCountData(@CurrentUser EnterpriseUserCustom userCustom){
        Map<String,Object> resultMap = Maps.newHashMap();
        LocalDate date = LocalDate.now();
        String year = date.getYear()+"";
        List<EnterpriseYearPlanCustom> yearplanList = yearplanService.listYearplansByEnterpriseId(year,userCustom.getEnterpriseId());
        //已上传
        resultMap.put("uploaded",yearplanList.size());
        //应上传
        resultMap.put("total", Constants.YEAR_PLAN_FILE_COUNT);
        //已上传的数据集合
        resultMap.put("data",yearplanList);
        return EhsResult.ok(resultMap);
    }


    /**
     * 获取普通人员培训统计数据
     * @param userCustom
     * @return
     */
    @GetMapping("getTrainData")
    public EhsResult getTrainData(@CurrentUser EnterpriseUserCustom userCustom){
        /*//获取当前年度
        LocalDate date = LocalDate.now();
        String year = date.getYear()+"";

        Map<String,Object> parameter = Maps.newHashMap();
        //获取当前年度普通人员计划总培训时长
        parameter.put("year",year);
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("planType", YearPlanTypeEnum.XCJY);
        EnterpriseYearPlan yearPlan = yearplanService.findYearplanByCondition(parameter);
        int trainTotalTime = yearPlan.getEmployeeTrainTime();

        //查找当前年度已培训时长(传参包含：企业id，当前年度，人员类型)
        parameter.clear();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("type", EnterpriseTrainingCustom.OPERATOR_TYPE);
        parameter.put("year",year);
        String trainTime = trainingService.getTrainTime(parameter);*/

        Map<String,Object> resultMap = Maps.newHashMap();

        resultMap.put("total",0);
        resultMap.put("trainTime",0);
        return EhsResult.ok(resultMap);
    }


    /**
     * 获取管理员培训统计数据
     * @param userCustom
     * @return
     */
    @GetMapping("getAdaminTrainData")
    public EhsResult getAdaminTrainData(@CurrentUser EnterpriseUserCustom userCustom){
        /*//获取当前年度
        LocalDate date = LocalDate.now();
        String year = date.getYear()+"";

        Map<String,Object> parameter = Maps.newHashMap();
        //获取当前年度普通人员计划总培训时长
        parameter.put("year",year);
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("planType", YearPlanTypeEnum.XCJY);
        EnterpriseYearPlan yearPlan = yearplanService.findYearplanByCondition(parameter);
        int trainTotalTime = yearPlan.getAdminTrainTime();

        //查找当前年度已培训时长(传参包含：企业id，当前年度，人员类型)
        parameter.clear();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("type", EnterpriseTrainingCustom.ADMIN_TYPE);
        parameter.put("year",year);
        String trainTime = trainingService.getTrainTime(parameter);*/

        Map<String,Object> resultMap = Maps.newHashMap();

        resultMap.put("total",0);
        resultMap.put("trainTime",0);
        return EhsResult.ok(resultMap);
    }

    /**
     * 统计危害申报数据
     * @param userCustom
     * @return
     */
    @GetMapping("getDeclareCountData")
    public EhsResult getDeclareCountData(@CurrentUser EnterpriseUserCustom userCustom){
        Map<String,Object> declareData = declareService.getDeclaredData(userCustom.getEnterpriseId());
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("total",declareData.get("total_num"));
        resultMap.put("declared",declareData.get("declare_num"));
        return EhsResult.ok(resultMap);
    }

    /**
     * 获取职业卫生组织管理数据
     * @param userCustom
     * @return
     */
    @GetMapping("getHygieneOrgCountData")
    public EhsResult getHygieneOrgCountData(@CurrentUser EnterpriseUserCustom userCustom){
        List<EnterpriseHygieneOrg> list = hygieneOrgService.queryListByEnterpriseId(userCustom.getEnterpriseId());
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("total",list.size());
        return EhsResult.ok(resultMap);
    }

    /**
     * 获取岗前待体检数据
     * @param userCustom
     * @return
     */
    public Integer getPreJobWaitMedicalCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("type",EmployeeWaitMedicalCustom.TYPE_PRE_JOB);
        parameter.put("status",EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
        List<EmployeeWaitMedicalCustom> list =  waitMedicalService.queryList(parameter);
        return list.size();
    }

    /**
     * 统计离岗待体检数据
     * @param userCustom
     * @return
     */
    public Integer getLeavePostWaitMedicalCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("type",EmployeeWaitMedicalCustom.TYPE_LEAVE_POST);
        parameter.put("status",EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
        List<EmployeeWaitMedicalCustom> list =  waitMedicalService.queryList(parameter);
        return list.size();
    }

    /**
     * 统计岗中待体检数据
     * @param userCustom
     * @return
     */
    public Integer getInPostWaitMedicalCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("type",EmployeeWaitMedicalCustom.TYPE_IN_POST);
        parameter.put("status",EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
        List<EmployeeWaitMedicalCustom> list =  waitMedicalService.queryList(parameter);
        return list.size();
    }

    /**
     *  获取待发放防护用品统计数据
     * @param userCustom
     * @return
     */
    public Integer getWaitProvideProtectCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("status",EnterpriseWaitProvideProtectArticlesCustom.STATUS_UNISSUED);
        List<EnterpriseWaitProvideProtectArticlesCustom> list = provideProtectArticlesService.queryWaitProvideProtectArticles(parameter);
        return list.size();
    }

    /**
     * 统计危害申报未回执数据
     * @param userCustom
     * @return
     */
    @GetMapping("getDeclareNoReceiptCountData")
    public EhsResult getDeclareNoReceiptCountData(@CurrentUser EnterpriseUserCustom userCustom){
        EnterpriseDeclareCustom custom = new EnterpriseDeclareCustom();
        custom.setEnterpriseId(userCustom.getEnterpriseId());
        List<EnterpriseDeclare> list = declareService.queryDeclareNoReceipt(custom);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("total",list.size());
        return EhsResult.ok(resultMap);
    }

    /**
     * 统计职业病待诊断数据
     * @param userCustom
     * @return
     */
    public Integer getDiseasesDiagnosisCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("result", MedicalResultEnum.YSZYB.getDescription());
        //职业病诊断数目
        parameter.put("diseaseNum",0);
        List<EmployeeMedicalCustom> list = medicalService.listMedicals(parameter);
        return list.size();
    }

    /**
     * 统计待检测项目数据
     * @param userCustom
     * @return
     */
    public Integer getWaitCheckCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("status", EnterpriseWaitCheckCustom.STATUS_CHECK_NO);
        JSONObject result = waitCheckService.listWaitChecks(0,0,parameter);
        List<EnterpriseWaitCheckCustom> list = (List<EnterpriseWaitCheckCustom>)result.get("enterpriseWaitChecks");
        return list.size();
    }

    /**
     * 查询岗前告知待告知数据
     * @param userCustom
     * @return
     */
    public Integer getWorkWaitNotifyCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("status",0);
        List<EmployeeWorkWaitNotifyCustom> list = workNotifyService.findWorkWaitNofity(parameter);
        return list.size();
    }


    /**
     * 查询作业场所检测结果待告知项
     * @param userCustom
     * @return
     */
    public Integer getWorkplaceWaitNotifyCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("status",EnterpriseWorkplaceWaitNotifyCustom.STATUS_NOTIFY_NO);
        Page<EnterpriseWorkplaceWaitNotifyCustom> page = workplaceNotifyService.findEnterpriseWorkplaceWaitNotifyList(parameter,null);
        return page.getResult().size();
    }

    /**
     * 查询体检结果待告知数量
     * @param userCustom
     * @return
     */
    public Integer getMedicalResultWaitNotifyCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        parameter.put("status",EnterpriseMedicalResultWaitNotifyCustom.STATUS_NOTIFY_NO);
        Page<EnterpriseMedicalResultWaitNotifyCustom> page = medicalResultNotifyService.findEnterpriseMedicalResultWaitNotifyList(parameter,null);
        return page.getResult().size();
    }

    /**
     * 查询职业病诊断结果待告知数量
     * @param userCustom
     * @return
     */
    public Integer getDiagnoseResultWaitNofifyCountData(EnterpriseUserCustom userCustom){
        Map<String,Object> parameter = Maps.newHashMap();
        parameter.put("status",EnterpriseDiseaseDiagnosiWaitNotifyCustom.STATUS_NOTIFY_NO);
        parameter.put("enterpriseId",userCustom.getEnterpriseId());
        Page<EnterpriseDiseaseDiagnosiWaitNotifyCustom> page = diseaseDiagnosiNotifyService.findEnterpriseDiseaseDiagnosiWaitNotifyList(parameter,null);
        return page.getResult().size();
    }

    /**
     * 获取企业今年各种完成率
     * @param year 当前年份
     * @param userCustom
     * @return
     */
    @GetMapping("getAllCompletionRate")
    public EhsResult getAllCompletionRate(Integer year, @CurrentUser EnterpriseUserCustom userCustom){
        Map<String, Object> resultMap = Maps.newHashMap();
        Map<String, Object> condition = Maps.newHashMap();
        condition.put("year", year);
        condition.put("enterpriseId", userCustom.getEnterpriseId());
        // 年度计划完成率(总数和完成数)
        int yearPlanCompletionNum = yearplanService.getYearPlanCompletionNum(condition);
        resultMap.put("yearPlanCompletionNum", yearPlanCompletionNum);
        resultMap.put("yearPlanAllNum", Constants.YEAR_PLAN_FILE_COUNT);
        // 责任制完成率(总数和已上传数)
        condition.put("module", 0);
        List<EnterpriseManageFile> responsibilityFiles =  responsibilityService.listResponsibilityFiles(condition);
        resultMap.put("responsibilityUploadedNum", responsibilityFiles.size());
        resultMap.put("responsibilityAllNum", Constants.DUTY_FILE_COUNT);
        // 防治制度完成率(总数和已上传数)
        condition.put("module", 1);
        List<EnterpriseManageFile> preventionFiles =  responsibilityService.listResponsibilityFiles(condition);
        resultMap.put("preventionUploadedNum", preventionFiles.size());
        resultMap.put("preventionAllNum", Constants.PREVENTIVE_SYSTEM_FILE_COUNT);
        // 申报百分比(危害因素总数和已申报数)
        Map<String, Object> declaredData = declareService.getDeclaredData(userCustom.getEnterpriseId());
        resultMap.put("declaredNum", declaredData.get("declare_num"));
        resultMap.put("harmFactorAllNum", declaredData.get("total_num"));
        // 查询是否有年度培训计划
        condition.put("planType", YearPlanTypeEnum.XCJY);
        EnterpriseYearPlanCustom enterpriseYearPlanCustom = yearplanService.getYearplanByCondition(condition);
        // 如果有，计算培训完成率
        if (enterpriseYearPlanCustom != null) {
            resultMap.put("yearTrainingPlan", 1);
            // 计划培训人均时长(管理员和普通员工)
            resultMap.put("adminPlanTrainingDuration", enterpriseYearPlanCustom.getAdminTrainTime());
            resultMap.put("employeePlanTrainingDuration", enterpriseYearPlanCustom.getEmployeeTrainTime());
            // 管理员培训完成率(管理员培训总时长和人数)
            JSONObject adminResult = trainingService.getYearAdminTrainingData(condition);
            List<EnterpriseTrainingEmployeeCustom> yearTraingAdminDataList
                    = (List<EnterpriseTrainingEmployeeCustom>)adminResult.get("yearAdminTrainingData");
            EnterpriseTrainingEmployeeCustom trainingAdmin = yearTraingAdminDataList.get(0);
            resultMap.put("trainingAdminAllDuration", trainingAdmin.getTotalAdminTrainingDuration());
            resultMap.put("adminNum", trainingAdmin.getAdminNum());
            // 普通员工培训完成率(在岗普通员工培训总时长和人数)
            JSONObject employeeResult = trainingService.getYearEmployeeTrainingData(condition);
            List<EnterpriseTrainingEmployeeCustom> yearTraingEmployeeDataList
                    = (List<EnterpriseTrainingEmployeeCustom>)employeeResult.get("yearEmployeeTraingData");
            EnterpriseTrainingEmployeeCustom trainingEmployee = yearTraingEmployeeDataList.get(0);
            resultMap.put("trainingEmployeeAllDuration", trainingEmployee.getTotalEmployeeTrainingDuration());
            resultMap.put("employeeNum", trainingEmployee.getEmployeeNum());
        } else {
            // 如果没有，则页面不显示培训完成率
            resultMap.put("yearTrainingPlan", 0);
        }
        return EhsResult.ok(resultMap);
    }

    /**
     * 获取企业今年每月的体检人数和培训人数
     * @param year 当前年份
     * @param userCustom
     * @return
     */
    @GetMapping("getMedicalTrainData")
    public EhsResult getMedicalTrainData(Integer year, @CurrentUser EnterpriseUserCustom userCustom){
        Map<String, Object> resultMap = Maps.newHashMap();
        Map<String, Object> condition = Maps.newHashMap();
        condition.put("year", year);
        condition.put("enterpriseId", userCustom.getEnterpriseId());
        // 查询体检人数
        Map<String, Integer> medicalNum = medicalService.getMedicalNumByCondition(condition);
        resultMap.put("medicalNum", medicalNum);
        // 查询培训人数
        Map<String, Integer> trainNum = trainingService.getTrainNumByCondition(condition);
        resultMap.put("trainNum", trainNum);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业咨询是否有新的回复
     * @param userCustom
     * @return
     */
    @GetMapping("queryNewReply")
    public EhsResult queryNewReply(@CurrentUser EnterpriseUserCustom userCustom){
        Map<String, Object> resultMap = Maps.newHashMap();
        List<SysConsult> sysConsults = consultService.queryNewReply(userCustom.getEnterpriseId(), SysConsultCustom.STATUS_ANSWER_NEW);
        // 获取最新的回复条数
        int newReplyNum =  sysConsults.size();
        resultMap.put("newReplyNum", newReplyNum);
        return EhsResult.ok(resultMap);
    }

    /**
     * 获取其他提醒信息
     * @param userCustom
     * @return
     */
    @GetMapping("getRemind")
    public EhsResult getRemind(EnterpriseRemind remind, QueryRequest request, @CurrentUser EnterpriseUserCustom userCustom){
        remind.setStatus(EnterpriseRemindCustom.STATUS_DTX);
        remind.setEnterpriseId(userCustom.getEnterpriseId());
        Map<String, Object> dataTable =  getDataTable(remindService.findRemeind(remind,request));
        return EhsResult.ok(dataTable);
    }

}
