package com.lanware.ehs.enterprise.medical.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.custom.EmployeeDiseaseDiagnosiCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @description 职业病诊断interface
 */
public interface DiseaseDiagnosiService {
    /**
     * @description 获取职业病诊断result
     * @param medicalId 体检id
     * @return com.lanware.management.common.format.ResultFormat
     */
    JSONObject listDiseaseDiagnosis(Long medicalId);

    /**
     * @description 根据id查询职业病诊断
     * @param id
     * @return com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi 职业病诊断pojo
     */
    EmployeeDiseaseDiagnosi getDiseaseDiagnosiById(Long id);

    /**
     * @description 新增职业病诊断信息
     * @param employeeDiseaseDiagnosi 职业病诊断pojo
     * @param enterpriseId 企业id
     * @param medicalCertificateFile 诊断书
     * @return int 插入成功的数目
     */
    int insertDiseaseDiagnosi(EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi
            , MultipartFile medicalCertificateFile, Long enterpriseId);

    /**
     * @description 更新职业病诊断信息
     * @param employeeDiseaseDiagnosi 职业病诊断pojo
     * @param medicalCertificateFile 诊断书
     * @param enterpriseId 企业id
     * @return int 更新成功的数目
     */
    int updateDiseaseDiagnosi(EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi
            , MultipartFile medicalCertificateFile, Long enterpriseId);

    /**
     * @description 删除职业病诊断
     * @param id 要删除的id
     * @param medicalCertificate 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteDiseaseDiagnosi(Long id, String medicalCertificate);

    /**
     * @description 批量删除职业病诊断
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteDiseaseDiagnosis(Long[] ids);

    /**
     * 职业病检出名单和职业禁忌症名单及后续安置情况
     * @param condition
     * @return 职业病检出名单和职业禁忌症名单及后续安置情况
     */
    Page<EmployeeDiseaseDiagnosiCustom> queryEnterpriseDiseaseDiagnosiByCondition(Map<String, Object> condition, QueryRequest request);
}

