package com.lanware.ehs.enterprise.account.facilitymaintenance.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.equipmentuse.service.EquipmentUseService;
import com.lanware.ehs.enterprise.protectivemeasure.service.ProtectiveMeasureService;
import com.lanware.ehs.enterprise.servicerecord.service.ServiceRecordService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseEquipmentUseCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectiveMeasuresCustom;
import com.lanware.ehs.pojo.custom.EnterpriseServiceRecordCustom;
import com.lanware.ehs.pojo.custom.EquipmentServiceUseCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 防护设施台账controller
 */
@Controller
@RequestMapping("/facilityMaintenanceAccount")
public class FacilityMaintenanceAccountController {
    /** 注入equipmentUseService的bean */
    @Autowired
    private EquipmentUseService equipmentUseService;
    /** 注入serviceRecordService的bean */
    @Autowired
    private ServiceRecordService serviceRecordService;
    /** 注入protectiveMeasureService的bean */
    @Autowired
    private ProtectiveMeasureService protectiveMeasureService;

    /** 下载word的临时路径 */
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;

    @RequestMapping("")
    /**
     * @description 返回防护设施台账页面
     * @param enterpriseUser 当前登录企业
     * @return java.lang.String 返回页面路径
     */
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/facilityMaintenance/list", data);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(FacilityMaintenanceAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 维护记录word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadServiceRecord")
    @ResponseBody
    public ResultFormat downloadServiceRecord(Long enterpriseId,String service_record_keyword, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 维护记录

        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", service_record_keyword);
        condition.put("enterpriseId", enterpriseId);
        List<EnterpriseServiceRecordCustom> enterpriseServiceRecordCustoms
                = serviceRecordService.listServiceRecordAccounts(condition);
        List serviceRecordList = new ArrayList();
        for (EnterpriseServiceRecordCustom enterpriseServiceRecordCustom : enterpriseServiceRecordCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("equipmentName", enterpriseServiceRecordCustom.getEquipmentName());
            temp.put("serviceDate", sdf.format(enterpriseServiceRecordCustom.getServiceDate()));
            temp.put("reason", enterpriseServiceRecordCustom.getReason());
            temp.put("result", enterpriseServiceRecordCustom.getResult());
            temp.put("servicePersonnel", enterpriseServiceRecordCustom.getServicePersonnel());
            temp.put("serviceLeader", enterpriseServiceRecordCustom.getServiceLeader());
            serviceRecordList.add(temp);
        }
        dataMap.put("serviceRecordList", serviceRecordList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(FacilityMaintenanceAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("serviceRecord.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "防护设备维修登记表.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("防护设备维修登记表.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 运行记录word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadEquipmentUse")
    @ResponseBody
    public ResultFormat downloadEquipmentUse(Long enterpriseId,String equipment_use_keyword, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 运行记录
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", equipment_use_keyword);
        condition.put("enterpriseId", enterpriseId);
        List<EnterpriseEquipmentUseCustom> enterpriseEquipmentUseCustoms
                = equipmentUseService.listEquipmentUseAccounts(condition);
        List equipmentUseList = new ArrayList();
        for (EnterpriseEquipmentUseCustom enterpriseEquipmentUseCustom : enterpriseEquipmentUseCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("equipmentName", enterpriseEquipmentUseCustom.getEquipmentName());
            temp.put("useDate", sdf.format(enterpriseEquipmentUseCustom.getUseDate()));
            temp.put("description", enterpriseEquipmentUseCustom.getDescription());
            equipmentUseList.add(temp);
        }
        dataMap.put("equipmentUseList", equipmentUseList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(FacilityMaintenanceAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("runEquipment.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "防护设备运行记录表.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("防护设备运行记录表.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 防护设施台账zip下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadFacilityMaintenance")
    @ResponseBody
    public ResultFormat downloadFacilityMaintenance(Long enterpriseId,String service_record_keyword,String equipment_use_keyword, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // 创建配置实例
            Configuration configuration = new Configuration();
            // 设置编码
            configuration.setDefaultEncoding("UTF-8");
            // ftl模板文件
            configuration.setClassForTemplateLoading(FacilityMaintenanceAccountController.class,"/templates/ftl");
            // 返回word的变量
            Map<String, Object> dataMap = new HashMap<String, Object>();
            Map<String, Object> dataMap1 = new HashMap<String, Object>();
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator);
            if (!upload.exists()) upload.mkdirs();

            // 维护记录
            Map<String, Object> condition = new HashMap<>();
            condition.put("keyword", service_record_keyword);
            condition.put("enterpriseId", enterpriseId);
            List<EnterpriseServiceRecordCustom> enterpriseServiceRecordCustoms
                    = serviceRecordService.listServiceRecordAccounts(condition);
            List serviceRecordList = new ArrayList();
            for (EnterpriseServiceRecordCustom enterpriseServiceRecordCustom : enterpriseServiceRecordCustoms) {
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("equipmentName", enterpriseServiceRecordCustom.getEquipmentName());
                temp.put("serviceDate", sdf.format(enterpriseServiceRecordCustom.getServiceDate()));
                temp.put("reason", enterpriseServiceRecordCustom.getReason());
                temp.put("result", enterpriseServiceRecordCustom.getResult());
                temp.put("servicePersonnel", enterpriseServiceRecordCustom.getServicePersonnel());
                temp.put("serviceLeader", enterpriseServiceRecordCustom.getServiceLeader());
                serviceRecordList.add(temp);
            }
            dataMap.put("serviceRecordList", serviceRecordList);
            Template template = configuration.getTemplate("serviceRecord.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "防护设备维修登记表.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            //放入压缩列表
            files.add(uploadPath);

            // 运行记录
            condition = new HashMap<>();
            condition.put("keyword", equipment_use_keyword);
            condition.put("enterpriseId", enterpriseId);
            List<EnterpriseEquipmentUseCustom> enterpriseEquipmentUseCustoms
                    = equipmentUseService.listEquipmentUseAccounts(condition);
            List equipmentUseList = new ArrayList();
            for (EnterpriseEquipmentUseCustom enterpriseEquipmentUseCustom : enterpriseEquipmentUseCustoms) {
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("equipmentName", enterpriseEquipmentUseCustom.getEquipmentName());
                temp.put("useDate", sdf.format(enterpriseEquipmentUseCustom.getUseDate()));
                temp.put("description", enterpriseEquipmentUseCustom.getDescription());
                equipmentUseList.add(temp);
            }
            dataMap1.put("equipmentUseList", equipmentUseList);
            Template template1 = configuration.getTemplate("runEquipment.ftl");
            // 输出文件
            String uploadPath1 = upload + File.separator + "防护设备运行记录表.doc";
            File outFile1 = new File(uploadPath1);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile1.getParentFile().exists()){
                outFile1.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile1),"UTF-8"));
            // 生成文件
            template1.process(dataMap1, out1);
            // 关闭流
            out1.flush();
            out1.close();
            //放入压缩列表
            files.add(uploadPath1);

            String uploadZipPath = upload + File.separator + "防护设施台账.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            File uploadZipFile = new File(uploadZipPath);
            FileInputStream fis = new FileInputStream(uploadZipFile);
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("防护设施台账.zip", "utf-8"));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = fis.read(buffer)) != -1) {
                os.write(buffer, 0, length);
            }
            fis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 防护设施信息台账文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadFacilityMaintenanceAccount")
    @ResponseBody
    public void downloadFacilityMaintenanceAccount(Long enterpriseId,String service_record_keyword,String equipment_use_keyword, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            Map<String, Object> dataMap = getFacilityMaintenanceAccountMap(enterpriseId,service_record_keyword,equipment_use_keyword,request);
            String uploadPath = upload + File.separator + "职业危害防护设施维护检修台帐.doc";
            exeWord(dataMap,uploadPath,"facilityMaintenanceAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业危害防护设施维护检修台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到防护设施信息台账文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getFacilityMaintenanceAccountMap(long enterpriseId,String service_record_keyword,String equipment_use_keyword, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 设备的操作规程，安装、调试验收记录
        JSONObject result = protectiveMeasureService.listProtectiveMeasures(0, 0, condition);
        List<EnterpriseProtectiveMeasuresCustom> enterpriseProtectiveMeasures
                = (List<EnterpriseProtectiveMeasuresCustom>)result.get("enterpriseProtectiveMeasures");
        List protectiveMeasureList = new ArrayList();
        for (EnterpriseProtectiveMeasuresCustom enterpriseProtectiveMeasuresCustom : enterpriseProtectiveMeasures) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("equipmentName", enterpriseProtectiveMeasuresCustom.getEquipmentName());
            temp.put("workplaceName", enterpriseProtectiveMeasuresCustom.getWorkplaceName());
            String operationRule = enterpriseProtectiveMeasuresCustom.getOperationRule();
            temp.put("operationRule", (operationRule == null || operationRule.equals("")) ? "-" : "已上传");
            String qualificationRecord = enterpriseProtectiveMeasuresCustom.getQualificationRecord();
            temp.put("qualificationRecord", (qualificationRecord == null || qualificationRecord.equals("")) ? "-" : "已上传");
            String acceptanceCertificate = enterpriseProtectiveMeasuresCustom.getAcceptanceCertificate();
            temp.put("acceptanceCertificate", (acceptanceCertificate == null || acceptanceCertificate.equals("")) ? "-" : "已上传");
            protectiveMeasureList.add(temp);
        }
        dataMap.put("protectiveMeasureList", protectiveMeasureList);
        // 运行使用记录
        condition = new HashMap<>();
        condition.put("keyword", equipment_use_keyword);
        condition.put("enterpriseId", enterpriseId);
        List<EnterpriseEquipmentUseCustom> enterpriseEquipmentUseCustoms
                = equipmentUseService.listEquipmentUseAccounts(condition);
        List equipmentUseList = new ArrayList();
        for (EnterpriseEquipmentUseCustom enterpriseEquipmentUseCustom : enterpriseEquipmentUseCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            String equipmentName = enterpriseEquipmentUseCustom.getEquipmentName();
            temp.put("equipmentName", (equipmentName == null || equipmentName.equals("")) ? "-" : equipmentName);
            temp.put("useDate", sdf.format(enterpriseEquipmentUseCustom.getUseDate()));
            temp.put("description", enterpriseEquipmentUseCustom.getDescription());
            equipmentUseList.add(temp);
        }
        dataMap.put("equipmentUseList", equipmentUseList);
        // 维护记录
        condition = new HashMap<>();
        condition.put("keyword", service_record_keyword);
        condition.put("enterpriseId", enterpriseId);
        List<EnterpriseServiceRecordCustom> enterpriseServiceRecordCustoms
                = serviceRecordService.listServiceRecordAccounts(condition);
        List serviceRecordList = new ArrayList();
        for (EnterpriseServiceRecordCustom enterpriseServiceRecordCustom : enterpriseServiceRecordCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            String equipmentName = enterpriseServiceRecordCustom.getEquipmentName();
            temp.put("equipmentName", (equipmentName == null || equipmentName.equals("")) ? "-" : equipmentName);
            temp.put("reason", enterpriseServiceRecordCustom.getReason());
            temp.put("timeLimit", enterpriseServiceRecordCustom.getTimeLimit());
            temp.put("result", enterpriseServiceRecordCustom.getResult());
            temp.put("serviceDate", sdf.format(enterpriseServiceRecordCustom.getServiceDate()));
            temp.put("serviceLeader", enterpriseServiceRecordCustom.getServiceLeader());
            temp.put("servicePersonnel", enterpriseServiceRecordCustom.getServicePersonnel());
            serviceRecordList.add(temp);
        }
        dataMap.put("serviceRecordList", serviceRecordList);
        return dataMap;
    }
}
