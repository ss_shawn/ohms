package com.lanware.ehs.enterprise.hygieneorg.service.impl;

import com.lanware.ehs.enterprise.hygieneorg.service.EnterpriseHygieneOrgService;
import com.lanware.ehs.mapper.EnterpriseHygieneOrgMapper;
import com.lanware.ehs.pojo.EnterpriseHygieneOrg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;

@Service
public class EnterpriseHygieneOrgServiceImpl  implements EnterpriseHygieneOrgService {

@Autowired
private EnterpriseHygieneOrgMapper enterpriseHygieneOrgMapper;


	@Override
	public EnterpriseHygieneOrg selectByPrimaryKey(Long id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		return enterpriseHygieneOrgMapper.selectByPrimaryKey(id);
	}

}
