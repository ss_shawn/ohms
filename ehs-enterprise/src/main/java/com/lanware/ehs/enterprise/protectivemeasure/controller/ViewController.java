package com.lanware.ehs.enterprise.protectivemeasure.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.protectivemeasure.service.ProtectiveMeasureService;
import com.lanware.ehs.pojo.EnterpriseProtectiveMeasures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 防护措施视图
 */
@Controller("protectiveMeasureView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入protectiveMeasureService的bean */
    @Autowired
    private ProtectiveMeasureService protectiveMeasureService;

    @RequestMapping("protectiveMeasure/editProtectiveMeasure")
    /**
     * @description 新增防护措施
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addProtectiveMeasure() {
        JSONObject data = new JSONObject();
        EnterpriseProtectiveMeasures enterpriseProtectiveMeasure = new EnterpriseProtectiveMeasures();
        data.put("enterpriseProtectiveMeasure", enterpriseProtectiveMeasure);
        return new ModelAndView("protectiveMeasure/edit", data);
    }

    /**
     * @description 编辑防护措施
     * @param id 防护措施id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("protectiveMeasure/editProtectiveMeasure/{id}")
    public ModelAndView editProtectiveMeasure(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseProtectiveMeasures enterpriseProtectiveMeasure = protectiveMeasureService.getProtectiveMeasureById(id);
        data.put("enterpriseProtectiveMeasure", enterpriseProtectiveMeasure);
        return new ModelAndView("protectiveMeasure/edit", data);
    }

    /**
     * @description 返回上传操作规程页面
     * @param id 防护设施id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("protectiveMeasure/addOperationRule/{id}")
    public ModelAndView addOperationRule(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseProtectiveMeasures enterpriseProtectiveMeasure = protectiveMeasureService.getProtectiveMeasureById(id);
        data.put("enterpriseProtectiveMeasure", enterpriseProtectiveMeasure);
        return new ModelAndView("protectiveMeasure/addOperationRule", data);
    }

    /**
     * @description 返回上传验收记录页面
     * @param id 防护设施id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("protectiveMeasure/addQualificationRecord/{id}")
    public ModelAndView addQualificationRecord(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseProtectiveMeasures enterpriseProtectiveMeasure = protectiveMeasureService.getProtectiveMeasureById(id);
        data.put("enterpriseProtectiveMeasure", enterpriseProtectiveMeasure);
        return new ModelAndView("protectiveMeasure/addQualificationRecord", data);
    }

    /**
     * @description 返回上传合格证书页面
     * @param id 防护设施id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("protectiveMeasure/addAcceptanceCertificate/{id}")
    public ModelAndView addAcceptanceCertificate(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseProtectiveMeasures enterpriseProtectiveMeasure = protectiveMeasureService.getProtectiveMeasureById(id);
        data.put("enterpriseProtectiveMeasure", enterpriseProtectiveMeasure);
        return new ModelAndView("protectiveMeasure/addAcceptanceCertificate", data);
    }
}
