package com.lanware.ehs.enterprise.employee.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.employee.service.EmployeeService;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseEmployeeCustom;
import com.lanware.ehs.service.EnterpriseEmployeeService;
import com.lanware.ehs.service.EnterprisePostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/employee")
@Controller
public class EmployeeController {

    private EnterpriseEmployeeService enterpriseEmployeeService;

    private EnterprisePostService enterprisePostService;

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EnterpriseEmployeeService enterpriseEmployeeService, EnterprisePostService enterprisePostService, EmployeeService employeeService) {
        this.enterpriseEmployeeService = enterpriseEmployeeService;
        this.enterprisePostService = enterprisePostService;
        this.employeeService = employeeService;
    }

    /**
     * 人员管理列表页面
     * @return 人员管理列表model
     */
    @RequestMapping("")
    public ModelAndView list(){
        return new ModelAndView("employee/list");
    }

    /**
     * 分页查询人员
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public EhsResult getList(@CurrentUser EnterpriseUser enterpriseUser, Integer pageNum, Integer pageSize, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<EnterpriseEmployeeCustom> page = employeeService.queryEnterpriseEmployeeByCondition(pageNum, pageSize, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return EhsResult.ok(data);
    }

    /**
     * 编辑人员页面
     * @param id 人员id
     * @return 编辑人员model
     */
    @RequestMapping("/edit")
    public ModelAndView edit(Long id,@CurrentUser EnterpriseUser enterpriseUser){
        JSONObject data = new JSONObject();
        if (id != null){
            try {
                EnterpriseEmployee enterpriseEmployee = enterpriseEmployeeService.selectByPrimaryKey(id);
                data.put("enterpriseEmployee", enterpriseEmployee);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }else{
            EnterpriseEmployee enterpriseEmployee = new EnterpriseEmployee();
            enterpriseEmployee.setBuildTime(new Date());
            enterpriseEmployee.setElectronArchiveNumber(employeeService.getElectronArchiveNumber(enterpriseUser.getEnterpriseId()));
            data.put("enterpriseEmployee", enterpriseEmployee);
        }
        return new ModelAndView("employee/edit", data);
    }

    /**
     * 保存人员
     * @param enterpriseUser 当前登录用户
     * @param enterpriseEmployeeJSON 企业人员JSON
     * @param harmContactHistoryListJSON 职业危害因素接触史集合JSON
     * @param harmContactHistoryDeletedIdsJSON 职业危害因素接触史删除id集合JSON
     * @param medicalHistoryListJSON 既往病史集合JSON
     * @param medicalHistoryDeletedIdsJSON 既往病史删除id集合JSON
     * @param request 请求
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(@CurrentUser EnterpriseUser enterpriseUser, String enterpriseEmployeeJSON, String harmContactHistoryListJSON, String harmContactHistoryDeletedIdsJSON, String medicalHistoryListJSON, String medicalHistoryDeletedIdsJSON, MultipartHttpServletRequest request){
        EnterpriseEmployee enterpriseEmployee = JSONObject.parseObject(enterpriseEmployeeJSON, EnterpriseEmployee.class);
        if (enterpriseEmployee.getEnterpriseId() == null){
            enterpriseEmployee.setEnterpriseId(enterpriseUser.getEnterpriseId());
        }
        List<EmployeeHarmContactHistory> harmContactHistoryList = JSONArray.parseArray(harmContactHistoryListJSON, EmployeeHarmContactHistory.class);
        List<Long> harmContactHistoryDeletedIds = JSONArray.parseArray(harmContactHistoryDeletedIdsJSON, Long.class);
        List<EmployeeMedicalHistory> medicalHistoryList = JSONArray.parseArray(medicalHistoryListJSON, EmployeeMedicalHistory.class);
        List<Long> medicalHistoryDeletedIds = JSONArray.parseArray(medicalHistoryDeletedIdsJSON, Long.class);
        Map<String, MultipartFile> fileMap = request.getFileMap();
        try {
            int num = employeeService.saveEnterpriseEmployee(enterpriseEmployee, harmContactHistoryList, harmContactHistoryDeletedIds, medicalHistoryList, medicalHistoryDeletedIds, fileMap);
            if (num == 0){
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存人员失败");
            }
        } catch (EhsException e){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
        return EhsResult.ok();
    }

    /**
     * 查看人员页面
     * @param id 人员id
     * @return 查看人员model
     */
    @RequestMapping("/view")
    public ModelAndView view(Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            try {
                EnterpriseEmployee enterpriseEmployee = enterpriseEmployeeService.selectByPrimaryKey(id);

                data.put("enterpriseEmployee", enterpriseEmployee);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return new ModelAndView("employee/view", data);
    }

    /**
     * 删除人员
     * @param idsJSON id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public EhsResult delete(@RequestParam String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        if (ids == null || ids.isEmpty()){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "删除人员失败");
        }
        int num = employeeService.deleteEnterpriseEmployeeByIds(ids);
        if (num == 0){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "删除人员失败");
        }else if (num == -8){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "删除人员失败,有人员上过岗，不能删除！");
        }
        return EhsResult.ok();
    }

    /**
     * 人员上岗页面
     * @param id 人员id
     * @param flag 标识 true 上岗 false 离岗
     * @return 员上岗页面model
     */
    @RequestMapping("/takeUpPost")
    public ModelAndView takeUpPost(@CurrentUser EnterpriseUser enterpriseUser, @RequestParam Long id, @RequestParam Boolean flag){
        JSONObject data = new JSONObject();
        data.put("id", id);
        data.put("flag", flag);
        EnterprisePostExample enterprisePostExample = new EnterprisePostExample();
        EnterprisePostExample.Criteria criteria = enterprisePostExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseUser.getEnterpriseId());
        try {
            List<EnterprisePost> enterprisePosts = enterprisePostService.selectByExample(enterprisePostExample);
            data.put("enterprisePosts", enterprisePosts);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return new ModelAndView("employee/takeUp", data);
    }

    /**
     * 人员上岗
     * @param parametersJSON 参数JSON
     * @return 操作结果
     */
    @RequestMapping("/submitTakeUpPost")
    @ResponseBody
    public EhsResult submitTakeUpPost(@CurrentUser EnterpriseUser enterpriseUser, String parametersJSON){
        JSONObject parameters = JSONObject.parseObject(parametersJSON, JSONObject.class);
        parameters.put("enterpriseId", enterpriseUser.getEnterpriseId());
        int num = employeeService.takeUpPost(parameters);
        if (num == 0){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), (parameters.containsKey("inductionDate") ? "上" : "离") + "岗失败");
        }else if(num==-8){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(),  "离岗失败,离岗时间在上岗时间之前，请重新选择离岗时间！");
        }else if(num==-9){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(),  "上岗失败,已经上岗，请刷新页面！");
        }
        return EhsResult.ok();
    }

    /**
     * 查看人员页面
     * @param id 人员id
     * @return 查看人员model
     */
    @RequestMapping("/employeeArchives/{id}")
    public ModelAndView employeeArchives(@PathVariable Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            try {
                EnterpriseEmployee enterpriseEmployee = enterpriseEmployeeService.selectByPrimaryKey(id);
                data.put("enterpriseEmployee", enterpriseEmployee);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ModelAndView("employee/employeeArchives", data);
    }
}
