package com.lanware.ehs.enterprise.threesimultaneity.controller;

import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.threesimultaneity.service.ProtectiveDesignService;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 三同时管理防护设施设计阶段controller
 */
@Controller
@RequestMapping("/threeSimultaneity/protectiveDesign")
public class ProtectiveDesignController {
    /** 注入protectiveDesignService的bean */
    @Autowired
    private ProtectiveDesignService protectiveDesignService;
    
    /**
     * @description 查询防护设施设计阶段list
     * @param projectId 项目id
     * @param projectState 项目阶段
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listProtectiveDesigns")
    @ResponseBody
    public ResultFormat listProtectiveDesigns(Long projectId, Integer projectState) {
        Map<String ,Object> result = new HashMap<String, Object>();
        List<EnterpriseThreeSimultaneityFile> enterpriseThreeSimultaneityFileList
                = protectiveDesignService.listProtectiveDesigns(projectId, projectState);
        for (EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile : enterpriseThreeSimultaneityFileList) {
            result.put(enterpriseThreeSimultaneityFile.getFileType(), enterpriseThreeSimultaneityFile);
        }
        return ResultFormat.success(result);
    }
}
