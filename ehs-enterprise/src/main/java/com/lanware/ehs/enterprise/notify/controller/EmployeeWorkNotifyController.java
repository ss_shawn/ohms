package com.lanware.ehs.enterprise.notify.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.notify.service.EmployeeWorkNotifyService;
import com.lanware.ehs.pojo.EnterpriseHygieneOrg;
import com.lanware.ehs.pojo.custom.EmployeeWorkNotifyCustom;
import com.lanware.ehs.pojo.custom.EmployeeWorkWaitNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("workNotify")
public class EmployeeWorkNotifyController extends BaseController {
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EmployeeWorkNotifyService employeeWorkNotifyService;
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EnterpriseBaseinfoService enterpriseBaseinfoService;

    /**
     * 获取待岗前告知列表
     * @param keywordworkList
     * @param request
     * @return
     */
    @RequestMapping("waitList")
    public EhsResult userList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //查询未告知数据
        condition.put("status",0);
        //排序条件
        condition.put("orderByClause","gmt_create");
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.employeeWorkNotifyService.findEmployeeWorkWaitNotifyList(condition,request));
        return EhsResult.ok(dataTable);
    }

      /**
     * 保存岗前告知
     * @return
     */
    @RequestMapping("workAdd")
    @ResponseBody
    public EhsResult addEnterpriseProtectArticles(String employeeWorkNotifyCustoms, MultipartFile notifyFile,@CurrentUser EnterpriseUserCustom currentUser) {
        EmployeeWorkNotifyCustom employeeWorkNotifyCustom = JSONObject.parseObject(employeeWorkNotifyCustoms, EmployeeWorkNotifyCustom.class);
        employeeWorkNotifyCustom.setEnterpriseId(currentUser.getEnterpriseId());
        this.employeeWorkNotifyService.saveEmployeeWorkNotify(employeeWorkNotifyCustom,notifyFile);
        return EhsResult.ok("");
    }
    /**
     * 删除岗前告知
     * @return
     */
    @RequestMapping("deleteWork")
    public EhsResult addEnterpriseProtectArticles(Long id, @CurrentUser EnterpriseUserCustom currentUser) {
        this.employeeWorkNotifyService.deleteEmployeeWorkNotify(id);
        return EhsResult.ok("");
    }
    /**
     * 获取待岗前告知列表
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("workList")
    public EhsResult workList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //排序条件
        condition.put("orderByClause","gmt_create");
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.employeeWorkNotifyService.findEmployeeWorkNotifyList(condition,request));
        return EhsResult.ok(dataTable);
    }
}
