package com.lanware.ehs.enterprise.check.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.check.service.*;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;
import com.lanware.ehs.pojo.custom.MonitorWorkplaceHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 检测管理视图
 */
@Controller("checkView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @Autowired
    /** 注入outsideCheckService的bean */
    private OutsideCheckService outsideCheckService;
    @Autowired
    /** 注入insideMonitorService的bean */
    private InsideMonitorService insideMonitorService;
    @Autowired
    /** 注入checkResultService的bean */
    private CheckResultService checkResultService;
    @Autowired
    /** 注入schematicDiagramService的bean */
    private SchematicDiagramService schematicDiagramService;
    /** 注入outsideCheckService的bean */
    @Autowired
    private MonitorPointService monitorPointService;
    /** 注入monitorWorkplaceHarmFactorService的bean */
    @Autowired
    private MonitorWorkplaceHarmFactorService monitorWorkplaceHarmFactorService;

    /**
     * @description 编辑危害因素检测
     * @param id 危害因素检测id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("outsideCheck/editOutsideCheck/{id}")
    public ModelAndView editOutsideCheck(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseOutsideCheck enterpriseOutsideCheck = outsideCheckService.getOutsideCheckById(id);
        data.put("enterpriseOutsideCheck", enterpriseOutsideCheck);
        return new ModelAndView("check/editOutsideCheck", data);
    }

    @RequestMapping("insideMonitor/editInsideMonitor")
    /**
     * @description 新增日常监测
     * @param enterpriseUser 企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addInsideMonitor(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        EnterpriseInsideMonitor enterpriseInsideMonitor = new EnterpriseInsideMonitor();
        enterpriseInsideMonitor.setEnterpriseId(enterpriseUser.getEnterpriseId());
        data.put("enterpriseInsideMonitor", enterpriseInsideMonitor);
        return new ModelAndView("check/editInsideMonitor", data);
    }

    /**
     * @description 编辑日常监测
     * @param id 日常监测id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("insideMonitor/editInsideMonitor/{id}")
    public ModelAndView editInsideMonitor(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseInsideMonitor enterpriseInsideMonitor = insideMonitorService.getInsideMonitorById(id);
        data.put("enterpriseInsideMonitor", enterpriseInsideMonitor);
        return new ModelAndView("check/editInsideMonitor", data);
    }

    @RequestMapping("schematicDiagram/addSchematicDiagram")
    /**
     * @description 新增监测、检测点示意图
     * @param enterpriseUser 企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addIdentifyFile(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(enterpriseUser.getEnterpriseId());
        enterpriseRelationFile.setRelationModule("监测、检测点示意图");
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("check/editSchematicDiagram", data);
    }

    @RequestMapping("schematicDiagram/editSchematicDiagram/{id}")
    /**
     * @description 编辑监测、检测点示意图
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editSchematicDiagram(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = schematicDiagramService.getSchematicDiagramById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("check/editSchematicDiagram", data);
    }


    /**
     * @description 新增监测点
     * @param enterpriseUser 企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorPoint/addMonitorPoint")
    public ModelAndView addMonitorPoint(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        EnterpriseMonitorPoint enterpriseMonitorPoint = new EnterpriseMonitorPoint();
        enterpriseMonitorPoint.setEnterpriseId(enterpriseUser.getEnterpriseId());
        data.put("enterpriseMonitorPoint", enterpriseMonitorPoint);
        return new ModelAndView("check/monitorPoint/edit", data);
    }

    /**
     * @description 编辑监测点
     * @param id 监测点id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorPoint/editMonitorPoint/{id}")
    public ModelAndView editMonitorPoint(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseMonitorPoint enterpriseMonitorPoint = monitorPointService.getMonitorPointById(id);
        data.put("enterpriseMonitorPoint", enterpriseMonitorPoint);
        return new ModelAndView("check/monitorPoint/edit", data);
    }

    /**
     * @description 返回监测点关联的作业场所和监测危害因素页面
     * @param monitorPointId 监测点id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorPoint/monitorWorkplaceHarmFactor/{monitorPointId}")
    public ModelAndView monitorWorkplaceHarmFactor(@PathVariable Long monitorPointId){
        JSONObject data = new JSONObject();
        data.put("monitorPointId", monitorPointId);
        return new ModelAndView("check/monitorPoint/workplaceHarmFactor/list", data);
    }

    /**
     * @description 新增作业场所和监测危害因素
     * @param monitorPointId 监测点id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorPoint/workplaceHarmFactor/add/{monitorPointId}")
    public ModelAndView addMonitorPoint(@PathVariable Long monitorPointId) {
        JSONObject data = new JSONObject();
        MonitorWorkplaceHarmFactorCustom monitorWorkplaceHarmFactorCustom = new MonitorWorkplaceHarmFactorCustom();
        monitorWorkplaceHarmFactorCustom.setMonitorPointId(monitorPointId);
        data.put("monitorWorkplaceHarmFactorCustom", monitorWorkplaceHarmFactorCustom);
        return new ModelAndView("check/monitorPoint/workplaceHarmFactor/edit", data);
    }

    /**
     * @description 编辑作业场所和监测危害因素
     * @param monitorPointId 监测点id
     * @param workplaceId 工作场所id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorPoint/workplaceHarmFactor/edit/{monitorPointId}/{workplaceId}")
    public ModelAndView addMonitorPoint(@PathVariable Long monitorPointId, @PathVariable Long workplaceId) {
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("workplaceId", workplaceId);
        condition.put("monitorPointId", monitorPointId);
        MonitorWorkplaceHarmFactorCustom monitorWorkplaceHarmFactorCustom
                = monitorWorkplaceHarmFactorService.getMonitorWorkplaceHarmFactorCustom(condition);
        data.put("monitorWorkplaceHarmFactorCustom", monitorWorkplaceHarmFactorCustom);
        return new ModelAndView("check/monitorPoint/workplaceHarmFactor/edit", data);
    }

    @RequestMapping("outsideCheck/addOutsideCheck/{waitCheckId}")
    /**
     * @description 新增检测记录
     * @param checkPlanId 检测计划id
     * @param enterpriseUser 企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addOutsideCheck(@PathVariable Long waitCheckId, @CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        EnterpriseOutsideCheck enterpriseOutsideCheck = new EnterpriseOutsideCheck();
        enterpriseOutsideCheck.setEnterpriseId(enterpriseUser.getEnterpriseId());
        data.put("enterpriseOutsideCheck", enterpriseOutsideCheck);
        data.put("waitCheckId", waitCheckId);
        return new ModelAndView("check/addOutsideCheck", data);
    }

    /**
     * @description 返回检测结果页面
     * @param outsideCheckId 企业外部检测id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("outsideCheck/checkResult/{outsideCheckId}")
    public ModelAndView checkResult(@PathVariable Long outsideCheckId){
        JSONObject data = new JSONObject();
        data.put("outsideCheckId", outsideCheckId);
        List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResultCustomList =
                checkResultService.getHarmFactorIdsByOutsideCheckId(outsideCheckId);
        data.put("checkResultList", enterpriseOutsideCheckResultCustomList);
        return new ModelAndView("check/result/list", data);
    }

    /**
     * @description 返回检测结果详细页面
     * @param checkResultId 检测结果id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("outsideCheck/checkResult/detail/{checkResultId}")
    public ModelAndView checkResultDetail(@PathVariable Long checkResultId){
        JSONObject data = new JSONObject();
        data.put("checkResultId", checkResultId);
        return new ModelAndView("check/result/detail/list", data);
    }

    /**
     * @description 返回新增检测结果详细页面
     * @param checkResultId 检测结果id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("outsideCheck/checkResult/detail/add/{checkResultId}")
    public ModelAndView addCheckResultDetail(@PathVariable Long checkResultId){
        JSONObject data = new JSONObject();
        EnterpriseCheckResultDetail enterpriseCheckResultDetail = new EnterpriseCheckResultDetail();
        enterpriseCheckResultDetail.setCheckResultId(checkResultId);
        data.put("enterpriseCheckResultDetail", enterpriseCheckResultDetail);
        return new ModelAndView("check/result/detail/edit", data);
    }

    /**
     * @description 返回编辑检测结果详细页面
     * @param id 检测结果详细id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("outsideCheck/checkResult/detail/edit/{id}")
    public ModelAndView editCheckResultDetail(@PathVariable Long id){
        JSONObject data = new JSONObject();
        EnterpriseCheckResultDetail enterpriseCheckResultDetail = checkResultService.getCheckResultDetailById(id);
        data.put("enterpriseCheckResultDetail", enterpriseCheckResultDetail);
        return new ModelAndView("check/result/detail/edit", data);
    }

    /**
     * @description 返回填写取消检测原因页面
     * @param id 检测结果id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("outsideCheck/checkResult/cancel/{id}")
    public ModelAndView cancelCheckResultDetail(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        data.put("id", id);
        return new ModelAndView("check/result/cancel", data);
    }


    /**
     * @description 编辑判定结果页面
     * @param id 检测结果id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("outsideCheck/checkResult/qualified/{id}")
    public String editQualified(@PathVariable Long id, Model model) {

        EnterpriseOutsideCheckResult checkResult = checkResultService.getCheckResultById(id);

        model.addAttribute("id",id);
        model.addAttribute("qualified",checkResult.getIsQualified());
        return "check/result/qualified";
    }
}
