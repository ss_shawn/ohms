package com.lanware.ehs.enterprise.medical.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EmployeeMedical;
import com.lanware.ehs.pojo.custom.EmployeeMedicalCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @description 体检管理interface
 */
public interface MedicalService {
    /**
     * @description 查询体检人员list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    JSONObject listMedicals(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 获取体检员工result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseEmployees的result
     */
    JSONObject listEmployeesByEnterprieId(Long enterpriseId);

    /**
     * @description 根据id查询员工体检信息
     * @param id 员工体检id
     * @return com.lanware.management.pojo.EmployeeMedical 返回EmployeeMedical对象
     */
    EmployeeMedical getMedicalById(Long id);

    /**
     * @description 根据id查询员工体检扩展信息
     * @param id 员工体检id
     * @return com.lanware.management.pojo.EmployeeMedicalCustom 返回EmployeeMedicalCustom对象
     */
    EmployeeMedicalCustom getMedicalCustomById(Long id);

    /**
     * @description 根据员工id获取岗位list
     * @param employeeId 员工id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseEmployees的result
     */
    JSONObject listPostsByEmployeeId(Long employeeId);

    /** @description 新增待体检员工体检信息，并上传文件到oss服务器
     * @param employeeMedical 员工体检pojo
     * @param medicalReportFile 体检报告
     * @param enterpriseId 企业id
     * @param condition 条件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertWaitMedical(EmployeeMedical employeeMedical, MultipartFile medicalReportFile, Long enterpriseId
            , Map<String ,Object> condition);

    /**
     * @description 新增员工体检信息
     * @param employeeMedical 员工体检pojo
     * @param medicalReportFile 体检报告
     * @param enterpriseId 企业id
     * @return int 插入成功的数目
     */
    int insertMedical(EmployeeMedical employeeMedical, MultipartFile medicalReportFile, Long enterpriseId);

    /**
     * @description 更新员工体检信息
     * @param employeeMedical 员工体检pojo
     * @param medicalReportFile 体检报告
     * @param enterpriseId 企业id
     * @return int 更新成功的数目
     */
    int updateMedical(EmployeeMedical employeeMedical, MultipartFile medicalReportFile, Long enterpriseId);

    /**
     * @description 删除员工体检信息
     * @param id 要删除的id
     * @param type 体检类型
     * @param result 体检结论
     * @param medicalReport 要删除的体检报告路径
     * @return int 删除成功的数目
     */
    int deleteMedical(Long id, String type, String result, String medicalReport);

    /**
     * @description 批量删除员工体检信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteMedicals(Long[] ids);

    /**
     * @description 查询上年度体检人员各项人数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listMedicalPersonNum(Map<String, Object> condition);

    /**
     * 获取疾病诊断数据
     * @param condition
     * @return
     */
    List<EmployeeMedicalCustom> listMedicals(Map<String, Object> condition);

    /**
     * 获取年度每月体检人数
     * @param condition
     * @return
     */
    Map<String, Integer> getMedicalNumByCondition(Map<String, Object> condition);
}
