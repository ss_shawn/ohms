package com.lanware.ehs.enterprise.medical.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.medical.service.DiseaseDiagnosiService;
import com.lanware.ehs.mapper.EmployeeDiseaseDiagnosiMapper;
import com.lanware.ehs.mapper.EmployeeMedicalHistoryMapper;
import com.lanware.ehs.mapper.EnterpriseDiseaseDiagnosiWaitNotifyMapper;
import com.lanware.ehs.mapper.custom.EmployeeDiseaseDiagnosiCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EmployeeDiseaseDiagnosiCustom;
import com.lanware.ehs.pojo.custom.EnterpriseDiseaseDiagnosiWaitNotifyCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description 职业病诊断interface实现类
 */
@Service
public class DiseaseDiagnosiServiceImpl implements DiseaseDiagnosiService {
    /** 注入employeeDiseaseDiagnosiMapper的bean */
    @Autowired
    private EmployeeDiseaseDiagnosiMapper employeeDiseaseDiagnosiMapper;
    @Autowired
    private EmployeeDiseaseDiagnosiCustomMapper employeeDiseaseDiagnosiCustomMapper;
    @Autowired
    private EmployeeMedicalHistoryMapper employeeMedicalHistoryMapper;
    @Autowired
    private EnterpriseDiseaseDiagnosiWaitNotifyMapper enterpriseDiseaseDiagnosiWaitNotifyMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    /**
     * @description 获取职业病诊断result
     * @param medicalId 体检id
     * @return com.lanware.management.common.format.ResultFormat
     */
    @Override
    public JSONObject listDiseaseDiagnosis(Long medicalId) {
        JSONObject result = new JSONObject();
        EmployeeDiseaseDiagnosiExample employeeDiseaseDiagnosiExample = new EmployeeDiseaseDiagnosiExample();
        EmployeeDiseaseDiagnosiExample.Criteria criteria = employeeDiseaseDiagnosiExample.createCriteria();
        criteria.andMedicalIdEqualTo(medicalId);
        List<EmployeeDiseaseDiagnosi> employeeDiseaseDiagnosis
                = employeeDiseaseDiagnosiMapper.selectByExample(employeeDiseaseDiagnosiExample);
        result.put("employeeDiseaseDiagnosis", employeeDiseaseDiagnosis);
        return result;
    }

    /**
     * @description 根据id查询职业病诊断
     * @param id
     * @return com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi 职业病诊断pojo
     */
    @Override
    public EmployeeDiseaseDiagnosi getDiseaseDiagnosiById(Long id) {
        return employeeDiseaseDiagnosiMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增职业病诊断信息
     * @param employeeDiseaseDiagnosi 职业病诊断pojo
     * @param medicalCertificateFile 诊断书
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertDiseaseDiagnosi(EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi
            , MultipartFile medicalCertificateFile, Long enterpriseId) {
        // 新增数据先保存，以便获取id
        employeeDiseaseDiagnosiMapper.insert(employeeDiseaseDiagnosi);
        // 新增职业病诊断待告知记录
        EnterpriseDiseaseDiagnosiWaitNotify enterpriseDiseaseDiagnosiWaitNotify
                = new EnterpriseDiseaseDiagnosiWaitNotify();
        enterpriseDiseaseDiagnosiWaitNotify.setEnterpriseId(enterpriseId);
        enterpriseDiseaseDiagnosiWaitNotify.setDiagnosiId(employeeDiseaseDiagnosi.getId());
        enterpriseDiseaseDiagnosiWaitNotify.setStatus(EnterpriseDiseaseDiagnosiWaitNotifyCustom.STATUS_NOTIFY_NO);
        enterpriseDiseaseDiagnosiWaitNotify.setGmtCreate(new Date());
        enterpriseDiseaseDiagnosiWaitNotifyMapper.insert(enterpriseDiseaseDiagnosiWaitNotify);
        // 上传并保存诊断书
        if (medicalCertificateFile != null){
            String medicalCertificate = "/" + enterpriseId + "/" + ModuleName.MEDICAL.getValue()
                    + "/" + employeeDiseaseDiagnosi.getMedicalId() + "/" + employeeDiseaseDiagnosi.getId()
                    + "/" + medicalCertificateFile.getOriginalFilename();
            try {
                ossTools.uploadStream(medicalCertificate, medicalCertificateFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传诊断书失败", e);
            }
            employeeDiseaseDiagnosi.setMedicalCertificate(medicalCertificate);
        }
        //新增职业病，插入职业病史
        if(employeeDiseaseDiagnosi.getIsCertain()==1){
            EmployeeMedicalHistory employeeMedicalHistory=new EmployeeMedicalHistory();
            employeeMedicalHistory.setDiseaseType(0);
            employeeMedicalHistory.setEmployeeId(employeeDiseaseDiagnosi.getEmployeeId());
            employeeMedicalHistory.setDiseaseName(employeeDiseaseDiagnosi.getName());
            employeeMedicalHistory.setDiagnosiDate(employeeDiseaseDiagnosi.getDiagnosiDate());
            employeeMedicalHistory.setDiagnosiHosipital(employeeDiseaseDiagnosi.getHospital());
            employeeMedicalHistory.setDiagnosiReport(employeeDiseaseDiagnosi.getMedicalCertificate());
            employeeMedicalHistory.setMedicalId(employeeDiseaseDiagnosi.getId());
            employeeMedicalHistory.setGmtCreate(new Date());
            employeeMedicalHistoryMapper.insert(employeeMedicalHistory);
        }

        return employeeDiseaseDiagnosiMapper.updateByPrimaryKeySelective(employeeDiseaseDiagnosi);
    }

    /**
     * @description 更新职业病诊断信息
     * @param employeeDiseaseDiagnosi 职业病诊断pojo
     * @param medicalCertificateFile 诊断书
     * @return int 更新成功的数目
     */
    @Override
    @Transactional
    public int updateDiseaseDiagnosi(EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi
            , MultipartFile medicalCertificateFile, Long enterpriseId) {
        // 上传并保存检测报告书
        if (medicalCertificateFile != null) {
            // 上传了新的申报文件
            if (!StringUtils.isEmpty(employeeDiseaseDiagnosi.getMedicalCertificate())) {
                // 删除旧文件
                ossTools.deleteOSS(employeeDiseaseDiagnosi.getMedicalCertificate());
            }
            String medicalCertificate = "/" + enterpriseId + "/" + ModuleName.MEDICAL.getValue()
                    + "/" + employeeDiseaseDiagnosi.getMedicalId() + "/" + employeeDiseaseDiagnosi.getId()
                    + "/" + medicalCertificateFile.getOriginalFilename();
            try {
                ossTools.uploadStream(medicalCertificate, medicalCertificateFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传诊断书失败", e);
            }
            employeeDiseaseDiagnosi.setMedicalCertificate(medicalCertificate);
        }
        //职业病历史有问题就删除
        //新增职业病，插入职业病史
        EmployeeMedicalHistoryExample employeeMedicalHistoryExample=new EmployeeMedicalHistoryExample();
        employeeMedicalHistoryExample.createCriteria().andMedicalIdEqualTo(employeeDiseaseDiagnosi.getId()).andDiseaseTypeEqualTo(0);
        List<EmployeeMedicalHistory> employeeMedicalHistorys=employeeMedicalHistoryMapper.selectByExample(employeeMedicalHistoryExample);
        if(employeeMedicalHistorys.size()>0&&employeeDiseaseDiagnosi.getIsCertain()==0){//修改为非职业病，删除
            employeeMedicalHistoryMapper.deleteByPrimaryKey(employeeMedicalHistorys.get(0).getId());
        }else if(employeeMedicalHistorys.size()==0&&employeeDiseaseDiagnosi.getIsCertain()==1){//修改为职业病，删除
            EmployeeMedicalHistory employeeMedicalHistory=new EmployeeMedicalHistory();
            employeeMedicalHistory.setDiseaseType(0);
            employeeMedicalHistory.setEmployeeId(employeeDiseaseDiagnosi.getEmployeeId());
            employeeMedicalHistory.setDiseaseName(employeeDiseaseDiagnosi.getName());
            employeeMedicalHistory.setDiagnosiDate(employeeDiseaseDiagnosi.getDiagnosiDate());
            employeeMedicalHistory.setDiagnosiHosipital(employeeDiseaseDiagnosi.getHospital());
            employeeMedicalHistory.setDiagnosiReport(employeeDiseaseDiagnosi.getMedicalCertificate());
            employeeMedicalHistory.setMedicalId(employeeDiseaseDiagnosi.getId());
            employeeMedicalHistory.setGmtCreate(new Date());
            employeeMedicalHistoryMapper.insert(employeeMedicalHistory);
        }
        return employeeDiseaseDiagnosiMapper.updateByPrimaryKeySelective(employeeDiseaseDiagnosi);
    }

    /**
     * @description 删除职业病诊断
     * @param id 要删除的id
     * @param medicalCertificate 要删除的文件路径
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteDiseaseDiagnosi(Long id, String medicalCertificate) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(medicalCertificate);
        //删除职业病历史数据
        EmployeeMedicalHistoryExample employeeMedicalHistoryExample=new EmployeeMedicalHistoryExample();
        employeeMedicalHistoryExample.createCriteria().andMedicalIdEqualTo(id).andDiseaseTypeEqualTo(0);
        employeeMedicalHistoryMapper.deleteByExample(employeeMedicalHistoryExample);
        // 删除数据库中对应的数据


        return employeeDiseaseDiagnosiMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除职业病诊断
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteDiseaseDiagnosis(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi = employeeDiseaseDiagnosiMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(employeeDiseaseDiagnosi.getMedicalCertificate());
        }
        EmployeeDiseaseDiagnosiExample employeeDiseaseDiagnosiExample = new EmployeeDiseaseDiagnosiExample();
        EmployeeDiseaseDiagnosiExample.Criteria criteria = employeeDiseaseDiagnosiExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return employeeDiseaseDiagnosiMapper.deleteByExample(employeeDiseaseDiagnosiExample);
    }

    /**
     * 职业病检出名单和职业禁忌症名单及后续安置情况
     * @param condition
     * @return 职业病检出名单和职业禁忌症名单及后续安置情况
     */
    @Override
    public Page<EmployeeDiseaseDiagnosiCustom> queryEnterpriseDiseaseDiagnosiByCondition(Map<String, Object> condition, QueryRequest request){
        Page<EmployeeDiseaseDiagnosiCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"t.id desc");
        String sortField = request.getField();
        if(org.apache.commons.lang3.StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return employeeDiseaseDiagnosiCustomMapper.queryEnterpriseDiseaseDiagnosiByCondition(condition);
    }

}
