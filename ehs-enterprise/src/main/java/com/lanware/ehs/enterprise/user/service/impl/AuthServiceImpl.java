package com.lanware.ehs.enterprise.user.service.impl;

import com.lanware.ehs.enterprise.user.service.AuthService;
import com.lanware.ehs.mapper.EnterpriseAuthMapper;
import com.lanware.ehs.pojo.EnterpriseAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class AuthServiceImpl implements AuthService {
    @Autowired
    private EnterpriseAuthMapper mapper;
    @Override
    public void saveBatch(Collection<EnterpriseAuth> entityList) {
        for(EnterpriseAuth auth : entityList){
            mapper.insert(auth);
        }
    }
}
