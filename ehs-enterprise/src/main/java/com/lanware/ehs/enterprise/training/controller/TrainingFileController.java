package com.lanware.ehs.enterprise.training.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.training.service.TrainingFileService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 其他培训证明资料controller
 */
@Controller
@RequestMapping("/training/trainingFile")
public class TrainingFileController {
    /** 注入trainingFileService的bean */
    @Autowired
    private TrainingFileService trainingFileService;

    /**
     * @description 查询培训证明资料list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listTrainingFiles")
    @ResponseBody
    public ResultFormat listTrainingFiles(Long relationId, String relationModule) {
        JSONObject result = trainingFileService.listTrainingFiles(relationId, relationModule);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editTrainingFile/save")
    @ResponseBody
    /** @description 保存培训证明资料，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param trainingFile 培训证明资料
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile trainingFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (trainingFileService.insertTrainingFile(enterpriseRelationFile, trainingFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (trainingFileService.updateTrainingFile(enterpriseRelationFile, trainingFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteTrainingFile")
    @ResponseBody
    /**
     * @description 删除培训证明资料
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteTrainingFile(Long id, String filePath) {
        if (trainingFileService.deleteTrainingFile(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteTrainingFiles")
    @ResponseBody
    /**
     * @description 批量删除培训证明资料
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteTrainingFiles(Long[] ids) {
        if (trainingFileService.deleteTrainingFiles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
