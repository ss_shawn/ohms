package com.lanware.ehs.enterprise.post.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.enterprise.post.service.ProtectArticlesService;
import com.lanware.ehs.mapper.custom.PostProtectArticlesCustomMapper;
import com.lanware.ehs.pojo.custom.PostProtectArticlesCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ProtectArticlesServiceImpl implements ProtectArticlesService {

    private PostProtectArticlesCustomMapper postProtectArticlesCustomMapper;

    @Autowired
    public ProtectArticlesServiceImpl(PostProtectArticlesCustomMapper postProtectArticlesCustomMapper) {
        this.postProtectArticlesCustomMapper = postProtectArticlesCustomMapper;
    }

    @Override
    public Page<PostProtectArticlesCustom> queryPostProtectArticlesByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<PostProtectArticlesCustom> page = PageHelper.startPage(pageNum, pageSize);
        postProtectArticlesCustomMapper.queryPostProtectArticlesByCondition(condition);
        return page;
    }

    @Override
    public Page<PostProtectArticlesCustom> queryUnSelectedPostProtectArticlesByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<PostProtectArticlesCustom> page = PageHelper.startPage(pageNum, pageSize);
        postProtectArticlesCustomMapper.queryUnSelectedPostProtectArticlesByCondition(condition);
        return page;
    }
}
