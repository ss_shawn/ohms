package com.lanware.ehs.enterprise.inspection.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.inspection.service.InspectionService;
import com.lanware.ehs.mapper.EnterpriseInspectionRectificationMapper;
import com.lanware.ehs.mapper.custom.EnterpriseInspectionCustomMapper;
import com.lanware.ehs.pojo.EnterpriseInspection;
import com.lanware.ehs.pojo.EnterpriseInspectionExample;
import com.lanware.ehs.pojo.EnterpriseInspectionRectification;
import com.lanware.ehs.pojo.EnterpriseInspectionRectificationExample;
import com.lanware.ehs.pojo.custom.EnterpriseInspectionCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

/**
 * @description 日常检查整改登记interface实现类
 */
@Service
public class InspectoinServiceImpl implements InspectionService {
    /** 注入enterpriseInspectionCustomMapper的bean */
    @Autowired
    private EnterpriseInspectionCustomMapper enterpriseInspectionCustomMapper;
    /** 注入enterpriseInspectionRectificationMapper的bean */
    @Autowired
    private EnterpriseInspectionRectificationMapper enterpriseInspectionRectificationMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    /**
     * @description 查询日常检查整改登记list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    @Override
    public JSONObject listInspections(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseInspectionCustom> enterpriseInspections = enterpriseInspectionCustomMapper.listInspections(condition);
        result.put("enterpriseInspections", enterpriseInspections);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseInspectionCustom> pageInfo = new PageInfo<EnterpriseInspectionCustom>(enterpriseInspections);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询日常检查整改信息
     * @param id 日常检查整改id
     * @return com.lanware.management.pojo.EnterpriseInspection 返回EnterpriseInspection对象
     */
    @Override
    public EnterpriseInspection getInspectionById(Long id) {
        return enterpriseInspectionCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增日常检查整改信息
     * @param enterpriseInspection 日常检查整改pojo
     * @param registrationSheetFile 日常检查整改登记表
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertInspection(EnterpriseInspection enterpriseInspection, MultipartFile registrationSheetFile) {
        // 新增数据先保存，以便获取id
        enterpriseInspectionCustomMapper.insert(enterpriseInspection);
        // 上传并保存日常检查整改登记表
        if (registrationSheetFile != null){
            String registrationSheet = "/" + enterpriseInspection.getEnterpriseId()
                    + "/" + ModuleName.INSPECTION.getValue() + "/" + enterpriseInspection.getId()
                    + "/" + registrationSheetFile.getOriginalFilename();
            try {
                ossTools.uploadStream(registrationSheet, registrationSheetFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传日常检查整改登记表失败", e);
            }
            enterpriseInspection.setRegistrationSheet(registrationSheet);
        }
        return enterpriseInspectionCustomMapper.updateByPrimaryKeySelective(enterpriseInspection);
    }

    /**
     * @description 更新日常检查整改信息
     * @param enterpriseInspection 日常检查整改pojo
     * @param registrationSheetFile 日常检查整改登记表
     * @return int 更新成功的数目
     */
    @Override
    public int updateInspection(EnterpriseInspection enterpriseInspection, MultipartFile registrationSheetFile) {
        // 上传并保存日常检查整改登记表
        if (registrationSheetFile != null) {
            // 上传了新的日常检查整改登记表
            if (!StringUtils.isEmpty(enterpriseInspection.getRegistrationSheet())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseInspection.getRegistrationSheet());
            }
            String registrationSheet = "/" + enterpriseInspection.getEnterpriseId()
                    + "/" + ModuleName.INSPECTION.getValue() + "/" + enterpriseInspection.getId()
                    + "/" + registrationSheetFile.getOriginalFilename();
            try {
                ossTools.uploadStream(registrationSheet, registrationSheetFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传日常检查整改登记表失败", e);
            }
            enterpriseInspection.setRegistrationSheet(registrationSheet);
        }
        return enterpriseInspectionCustomMapper.updateInspection(enterpriseInspection);
    }

    /**
     * @description 删除员日常检查整改信息
     * @param id 要删除的id
     * @param registrationSheet 要删除的检查整改登记表路径
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteInspection(Long id, String registrationSheet) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(registrationSheet);
        EnterpriseInspectionRectificationExample enterpriseInspectionRectificationExample
                = new EnterpriseInspectionRectificationExample();
        EnterpriseInspectionRectificationExample.Criteria criteria
                = enterpriseInspectionRectificationExample.createCriteria();
        criteria.andInspectionIdEqualTo(id);
        List<EnterpriseInspectionRectification> enterpriseInspectionRectifications
                = enterpriseInspectionRectificationMapper.selectByExample(enterpriseInspectionRectificationExample);
        for (EnterpriseInspectionRectification enterpriseInspectionRectification : enterpriseInspectionRectifications) {
            ossTools.deleteOSS(enterpriseInspectionRectification.getFilePath());
        }
        // 删除数据库中对应的数据
        enterpriseInspectionRectificationMapper.deleteByExample(enterpriseInspectionRectificationExample);
        return enterpriseInspectionCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除日常检查整改信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteInspections(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseInspection enterpriseInspection = enterpriseInspectionCustomMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseInspection.getRegistrationSheet());
        }
        EnterpriseInspectionRectificationExample enterpriseInspectionRectificationExample
                = new EnterpriseInspectionRectificationExample();
        EnterpriseInspectionRectificationExample.Criteria criteria
                = enterpriseInspectionRectificationExample.createCriteria();
        criteria.andInspectionIdIn(Arrays.asList(ids));
        List<EnterpriseInspectionRectification> enterpriseInspectionRectifications
                = enterpriseInspectionRectificationMapper.selectByExample(enterpriseInspectionRectificationExample);
        for (EnterpriseInspectionRectification enterpriseInspectionRectification : enterpriseInspectionRectifications) {
            ossTools.deleteOSS(enterpriseInspectionRectification.getFilePath());
        }
        // 删除数据库中对应的数据
        enterpriseInspectionRectificationMapper.deleteByExample(enterpriseInspectionRectificationExample);
        EnterpriseInspectionExample enterpriseInspectionExample = new EnterpriseInspectionExample();
        EnterpriseInspectionExample.Criteria criteria1 = enterpriseInspectionExample.createCriteria();
        criteria1.andIdIn(Arrays.asList(ids));
        return enterpriseInspectionCustomMapper.deleteByExample(enterpriseInspectionExample);
    }

    /**
     * @description 保存检查整改登记表
     * @param enterpriseInspection 日常检查整改pojo
     * @param registrationSheetFile 日常检查整改登记表
     * @return int 更新成功的数目
     */
    @Override
    public int updateRegistrationSheet(EnterpriseInspection enterpriseInspection, MultipartFile registrationSheetFile) {
        // 上传并保存日常检查整改登记表
        if (registrationSheetFile != null) {
            String registrationSheet = "/" + enterpriseInspection.getEnterpriseId()
                    + "/" + ModuleName.INSPECTION.getValue() + "/" + enterpriseInspection.getId()
                    + "/" + registrationSheetFile.getOriginalFilename();
            try {
                ossTools.uploadStream(registrationSheet, registrationSheetFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传日常检查整改登记表失败", e);
            }
            enterpriseInspection.setRegistrationSheet(registrationSheet);
        }
        return enterpriseInspectionCustomMapper.updateByPrimaryKeySelective(enterpriseInspection);
    }

    /**
     * @description 查询整改过期记录的数量
     * @param enterpriseId 企业id
     * @return int 数量
     */
    @Override
    public int getOverdueInspectionNum(Long enterpriseId) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("enterpriseId", enterpriseId);
        List<EnterpriseInspectionCustom> enterpriseInspections = enterpriseInspectionCustomMapper.listInspections(condition);
        // 初始化整改过期记录的数量
        int overdueInspectionNum = 0;
        for (EnterpriseInspectionCustom enterpriseInspectionCustom : enterpriseInspections) {
            if (enterpriseInspectionCustom.getIsRectified() == 1 && enterpriseInspectionCustom.getRectificationNum() == 0) {
                if (new Date().getTime() > enterpriseInspectionCustom.getRectificationDeadline().getTime()) {
                    overdueInspectionNum += 1;
                }
            }
        }
        return overdueInspectionNum;
    }
}
