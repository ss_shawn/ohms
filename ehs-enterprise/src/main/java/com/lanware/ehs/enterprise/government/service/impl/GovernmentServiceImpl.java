package com.lanware.ehs.enterprise.government.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.government.service.GovernmentService;
import com.lanware.ehs.mapper.*;
import com.lanware.ehs.mapper.custom.*;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.Enum.YearPlanTypeEnum;
import com.lanware.ehs.pojo.custom.EnterpriseManageFileCustom;
import com.lanware.ehs.pojo.custom.EnterpriseServiceRecordCustom;
import com.lanware.ehs.pojo.custom.EquipmentServiceUseCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class GovernmentServiceImpl implements GovernmentService {
    @Autowired
    private GovernmentEnterpriseBaseinfoMapper governmentEnterpriseBaseinfoMapper;
    @Autowired
    private EnterpriseBaseinfoCustomMapper enterpriseBaseinfoCustomMapper;
    @Autowired
    private GovernmentEnterpriseYearMapper governmentEnterpriseYearMapper;
    @Autowired
    private EnterpriseHygieneOrgMapper enterpriseHygieneOrgMapper;
    @Autowired
    /** 注入enterpriseManageFileCustomMapper的bean */
    private EnterpriseManageFileCustomMapper enterpriseManageFileCustomMapper;
    @Autowired
    /** 注入EmployeeWorkWaitNotifyMapper的bean */
    private EmployeeWorkWaitNotifyCustomMapper employeeWorkWaitNotifyCustomMapper;
    @Autowired
    /** 注入EnterpriseWorkplaceWaitNotifyMapper的bean */
    private EnterpriseWorkplaceWaitNotifyMapper enterpriseWorkplaceWaitNotifyMapper;
    @Autowired
    /** 注入EnterpriseMedicalResultWaitNotifyMapper的bean */
    private EnterpriseMedicalResultWaitNotifyCustomMapper enterpriseMedicalResultWaitNotifyCustomMapper;
    @Autowired
    /** 注入enterpriseDeclareCustomMapper的bean */
    private EnterpriseDeclareCustomMapper enterpriseDeclareCustomMapper;
    @Autowired
    private GovernmentCustomMapper governmentCustomMapper;
    @Autowired
    /** 注入enterpriseYearPlanMapper的bean */
    private EnterpriseYearPlanMapper enterpriseYearPlanMapper;
    @Autowired
    private EnterpriseEmployeeMapper enterpriseEmployeeMapper;
    @Autowired
    private GovernmentHarmReportMapper governmentHarmReportMapper;
    @Autowired
    private GovernmentLastProvideMapper governmentLastProvideMapper;
    @Autowired
    private EnterpriseServiceRecordCustomMapper enterpriseServiceRecordCustomMapper;
    @Autowired
    private GovernmentServiceRecordMapper governmentServiceRecordMapper;
    @Autowired
    private EnterpriseToxicDrillCustomMapper enterpriseToxicDrillCustomMapper;
    @Autowired
    private EnterpriseThreeSimultaneityMapper enterpriseThreeSimultaneityMapper;
    @Autowired
    private GovernmentThreeSimultaneityMapper governmentThreeSimultaneityMapper;
    @Autowired
    private GovernmentLastMedicalMapper governmentLastMedicalMapper;
    @Autowired
    private GovernmentLastTrainingMapper governmentLastTrainingMapper;
    @Autowired
    private GovernmentProvideProtectArtMapper governmentProvideProtectArtMapper;
    @Autowired
    private GovernmentProtectiveMeasuresMapper governmentProtectiveMeasuresMapper;
    @Autowired
    private GovernmentServiceInspectionMapper governmentServiceInspectionMapper;
    @Autowired
    private GovernmentServiceRectifiedMapper governmentServiceRectifiedMapper;
    @Autowired
    private GovernmentOutsideCheckMapper governmentOutsideCheckMapper;
    @Autowired
    private GovernmentLastHarmCheckMapper governmentLastHarmCheckMapper;
    @Autowired
    private GovernmentEnterpriseBaseinfoCustomMapper governmentEnterpriseBaseinfoCustomMapper;
    @Autowired
    private EquipmentServiceUseCustomMapper equipmentServiceUseCustomMapper;
    /**
     * 根据企业ID获取提交的企业信息
     * @param enterpriseId 企业ID
     * @return
     */
    @Override
    public GovernmentEnterpriseBaseinfo getGovernmentEnterpriseBaseinfoByEnterpriseId(Long enterpriseId){
        return governmentEnterpriseBaseinfoMapper.selectByPrimaryKey(enterpriseId);
    }

    /**
     * 保存提交的企业信息
     * @param governmentEnterpriseBaseinfo
     * @return 成功数量
     */
    @Override
    public int getGovernmentEnterpriseBaseinfoByEnterpriseId(GovernmentEnterpriseBaseinfo governmentEnterpriseBaseinfo){
        return governmentEnterpriseBaseinfoMapper.updateByPrimaryKey(governmentEnterpriseBaseinfo);
    }

    /**
     * 提交企业信息
     * @param enterpriseId
     * @return 是否成功
     */
    @Override
    @Transactional
    public int uploadGovernment(Long enterpriseId ){
        //更新企业基本信息
        EnterpriseBaseinfo enterpriseBaseinfo =enterpriseBaseinfoCustomMapper.selectByPrimaryKey(enterpriseId);
        enterpriseBaseinfo.setLastUpdateTime(new Date());
        //复制企业基本信息，最后步骤哦存
        GovernmentEnterpriseBaseinfo governmentEnterpriseBaseinfo;
        governmentEnterpriseBaseinfo = JSONObject.parseObject( JSONObject.toJSONString(enterpriseBaseinfo), GovernmentEnterpriseBaseinfo.class);
        governmentEnterpriseBaseinfo.setId(enterpriseId);
        if(governmentEnterpriseBaseinfoMapper.selectByPrimaryKey(enterpriseId)==null){
            governmentEnterpriseBaseinfoCustomMapper.insert(governmentEnterpriseBaseinfo);
        }else{
            governmentEnterpriseBaseinfoMapper.updateByPrimaryKey(governmentEnterpriseBaseinfo);
        }
        //更新年度统计信息
        Calendar date = Calendar.getInstance();
        int year = date.get(Calendar.YEAR);
        GovernmentEnterpriseYearExample governmentEnterpriseYearExample=new GovernmentEnterpriseYearExample();
        governmentEnterpriseYearExample.createCriteria().andEnterpriseIdEqualTo(enterpriseId);
        governmentEnterpriseYearExample.setOrderByClause(" year desc");
        List<GovernmentEnterpriseYear> governmentEnterpriseYearList= governmentEnterpriseYearMapper.selectByExample(governmentEnterpriseYearExample);
        GovernmentEnterpriseYear governmentEnterpriseYear;
        int harmTotalNumber=0;
        if(governmentEnterpriseBaseinfo.getHarmTotalNumber()!=null){
            harmTotalNumber=governmentEnterpriseBaseinfo.getHarmTotalNumber();
        }

        if(governmentEnterpriseYearList.size()==0){
            governmentEnterpriseYear=new GovernmentEnterpriseYear();
            governmentEnterpriseYear.setEnterpriseId(enterpriseId);
            governmentEnterpriseYear.setYear(year);
            insertGovernmentEnterpriseYear(governmentEnterpriseYear,1,harmTotalNumber,governmentEnterpriseBaseinfo.getIsHighlyToxic());
        }else{
            governmentEnterpriseYear=governmentEnterpriseYearList.get(0);
            insertGovernmentEnterpriseYear(governmentEnterpriseYear,0,harmTotalNumber,governmentEnterpriseBaseinfo.getIsHighlyToxic());
            if(governmentEnterpriseYear.getYear()!=year){
                governmentEnterpriseYear=new GovernmentEnterpriseYear();
                governmentEnterpriseYear.setEnterpriseId(enterpriseId);
                governmentEnterpriseYear.setYear(year);
                insertGovernmentEnterpriseYear(governmentEnterpriseYear,1,harmTotalNumber,governmentEnterpriseBaseinfo.getIsHighlyToxic());
            }
        }
        return enterpriseBaseinfoCustomMapper.updateByPrimaryKey(enterpriseBaseinfo);
    }

    /**
     * 更新年度统计
     * @param governmentEnterpriseYear
     */
    public void insertGovernmentEnterpriseYear(GovernmentEnterpriseYear governmentEnterpriseYear,int isInsert,int harmTotalNumber,Integer isHighlyToxic){
        float score=0;
        //1.   管理组织
        EnterpriseHygieneOrgExample enterpriseHygieneOrgExample=new EnterpriseHygieneOrgExample();
        enterpriseHygieneOrgExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId());
        long managerNumber=enterpriseHygieneOrgMapper.countByExample(enterpriseHygieneOrgExample);
        if(managerNumber>0){
            governmentEnterpriseYear.setAdministration(100);
            score+=100;
        }else{
            governmentEnterpriseYear.setAdministration(0);
        }
        //2.	责任制度文件
        Map<String ,Object> condition = new HashMap<String, Object>();
        condition.put("enterpriseId", governmentEnterpriseYear.getEnterpriseId());
        condition.put("module", EnterpriseManageFileCustom.MODULE_RESPONSIBILITY);
        List<EnterpriseManageFile> enterpriseManageFiles=enterpriseManageFileCustomMapper.listManageFiles(condition);
        double responsibilityNumber=enterpriseManageFiles.size()*1.0/ Constants.DUTY_FILE_COUNT *100;
        governmentEnterpriseYear.setResponsibilityNumber(enterpriseManageFiles.size());
        score+=responsibilityNumber;
        //3.	职业健康防治文件
        condition = new HashMap<String, Object>();
        condition.put("enterpriseId", governmentEnterpriseYear.getEnterpriseId());
        condition.put("module", EnterpriseManageFileCustom.MODULE_PREVENTION);
        enterpriseManageFiles=enterpriseManageFileCustomMapper.listManageFiles(condition);
        double preventionNumber=enterpriseManageFiles.size()*1.0/Constants.PREVENTIVE_SYSTEM_FILE_COUNT*100;
        governmentEnterpriseYear.setPreventionNumber(enterpriseManageFiles.size());
        score+=preventionNumber;
        //4.	岗前告知
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        //查询未告知数据
        condition.put("status",1);
        int postHeadNotify=employeeWorkWaitNotifyCustomMapper.findEmployeeWorkWaitNotifyList(condition).size();
        governmentEnterpriseYear.setPostHeadNotify(postHeadNotify);
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        int postHeadNotifyAll=employeeWorkWaitNotifyCustomMapper.findEmployeeWorkWaitNotifyList(condition).size();
        governmentEnterpriseYear.setPostHeadNotifyAll(postHeadNotifyAll);
        if(postHeadNotifyAll>0){
            score+=(postHeadNotify*1.0/postHeadNotifyAll*100);
        }else{
            score+=100;
        }
        //5.	年度作业场所危害因素检测告知
        EnterpriseWorkplaceWaitNotifyExample enterpriseWorkplaceWaitNotifyExample=new EnterpriseWorkplaceWaitNotifyExample();
        enterpriseWorkplaceWaitNotifyExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId()).andStatusEqualTo(1);
        int workplaceNotify=(int) enterpriseWorkplaceWaitNotifyMapper.countByExample(enterpriseWorkplaceWaitNotifyExample);
        governmentEnterpriseYear.setWorkplaceNotify(workplaceNotify);
        enterpriseWorkplaceWaitNotifyExample=new EnterpriseWorkplaceWaitNotifyExample();
        enterpriseWorkplaceWaitNotifyExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId());
        int workplaceNotifyAll=(int)  enterpriseWorkplaceWaitNotifyMapper.countByExample(enterpriseWorkplaceWaitNotifyExample);
        governmentEnterpriseYear.setWorkplaceNotifyAll(workplaceNotifyAll);
        if(workplaceNotifyAll>0){
            score+=(workplaceNotify*1.0/workplaceNotifyAll*100);
        }else{
            score+=100;
        }
//         6.	年度体检结果告知率
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        //查询已告知数据
        condition.put("status",1);
        condition.put("year",governmentEnterpriseYear.getYear());
        int medicalResultNotify=enterpriseMedicalResultWaitNotifyCustomMapper.findEnterpriseMedicalResultWaitNotifyList(condition).size();
        governmentEnterpriseYear.setMedicalResultNotify(medicalResultNotify);
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        int medicalResultNotifyAll=enterpriseMedicalResultWaitNotifyCustomMapper.findEnterpriseMedicalResultWaitNotifyList(condition).size();
        governmentEnterpriseYear.setMedicalResultNotifyAll(medicalResultNotifyAll);
        if(medicalResultNotifyAll>0){
            score+=(medicalResultNotify*1.0/medicalResultNotifyAll*100);
        }
        //7.	危害因素申报率
        Map<String, Object> declareData = enterpriseDeclareCustomMapper.queryDeclareData(governmentEnterpriseYear.getEnterpriseId());
        long harmDeclareNum=(long)declareData.get("declare_num");
        long harmDeclareNumAll=(long)declareData.get("total_num");
        governmentEnterpriseYear.setHarmDeclareNum((int)harmDeclareNum);
        governmentEnterpriseYear.setHarmDeclareNumAll((int)harmDeclareNumAll);
        if(harmDeclareNumAll>0){
            score+=(harmDeclareNum*1.0/harmDeclareNumAll*100);
        }
        //government_last_medical 员工体检表


        //查询在岗职工数
        EnterpriseEmployeeExample enterpriseEmployeeExample=new EnterpriseEmployeeExample();
        EnterpriseEmployeeExample.Criteria c = enterpriseEmployeeExample.createCriteria();
        c.andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId()).andOndutyIdIsNotNull().andOndutyIdNotEqualTo((long)0);
        int employeeNumber =(int)enterpriseEmployeeMapper.countByExample(enterpriseEmployeeExample);

        //8.	年度管理人员培训率
        //培训计划
        EnterpriseYearPlanExample enterpriseYearPlanExample=new EnterpriseYearPlanExample();
        enterpriseYearPlanExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId()).andYearEqualTo(governmentEnterpriseYear.getYear()+"").andPlanTypeEqualTo(YearPlanTypeEnum.XCJY.getValue());
        List<EnterpriseYearPlan> enterpriseYearPlanList= enterpriseYearPlanMapper.selectByExample(enterpriseYearPlanExample);
        if(enterpriseYearPlanList.size()>0){
            EnterpriseYearPlan enterpriseYearPlan=enterpriseYearPlanList.get(0);
            condition = new JSONObject();
            //过滤当前企业
            condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
            condition.put("year",governmentEnterpriseYear.getYear());
            condition.put("duration",enterpriseYearPlan.getAdminTrainTime());
            condition.put("type",1);
            Map<String,Object> trainTime=governmentCustomMapper.getAllTrainTime(condition);

            //管理人员培训数
            long managerTrainingNumber=trainTime.get("employeeNum")==null?0:((long)trainTime.get("employeeNum"));
            //管理人员培训总时长
            int managerAllLength=trainTime.get("durationAll")==null?0:((BigDecimal)trainTime.get("durationAll")).intValue();//duration是提出超过培训计划的，durationAll是不剔除的
            governmentEnterpriseYear.setManagerNumber((int)managerNumber);
            governmentEnterpriseYear.setManagerTrainingNumber((int)managerTrainingNumber);
            governmentEnterpriseYear.setManagerAverageLength(managerAllLength);
            //管理人员培训率
            if(managerNumber>0){
                score+=(managerTrainingNumber*1.0/managerNumber*100);
            }

            //9.	年度上岗员工培训率
            condition = new JSONObject();
            //过滤当前企业
            condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
            condition.put("year",governmentEnterpriseYear.getYear());
            condition.put("duration",enterpriseYearPlan.getEmployeeTrainTime());
            condition.put("type",0);
            trainTime=governmentCustomMapper.getAllTrainTime(condition);

            //员工培训数
            long employeeTrainingNumber=trainTime.get("employeeNum")==null?0:((long)trainTime.get("employeeNum"));
            //员工培训总时长
            long employeeAllLength=trainTime.get("durationAll")==null?0:((BigDecimal)trainTime.get("durationAll")).longValue();//duration是提出超过培训计划的，durationAll是不剔除的
            governmentEnterpriseYear.setEmployeeNumber(employeeNumber);
            governmentEnterpriseYear.setEmployeeTrainingNumber((int)employeeTrainingNumber);
            governmentEnterpriseYear.setEmployeeAverageLength((int)employeeAllLength);

            //年度上岗员工培训率
            if(employeeNumber>0){
                score+=(employeeTrainingNumber*1.0/employeeNumber*100);
            }
        }else{
            governmentEnterpriseYear.setManagerNumber((int)managerNumber);
            governmentEnterpriseYear.setManagerTrainingNumber(0);
            governmentEnterpriseYear.setManagerAverageLength(0);
            governmentEnterpriseYear.setEmployeeNumber(employeeNumber);
            governmentEnterpriseYear.setEmployeeTrainingNumber(0);
            governmentEnterpriseYear.setEmployeeAverageLength(0);
        }
        //10.	年度危害因素检测率  场所-危害因素
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        List<Map<String,Object>> harmReport=governmentCustomMapper.getHarmReport(condition);
        //删除历史数
        GovernmentHarmReportExample governmentHarmReportExample=new GovernmentHarmReportExample();
        governmentHarmReportExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId()).andYearEqualTo(governmentEnterpriseYear.getYear());
        governmentHarmReportMapper.deleteByExample(governmentHarmReportExample);
        int harmCheckNumber=0;
        for(Map<String,Object> map:harmReport){
            GovernmentHarmReport governmentHarmReport=new GovernmentHarmReport();
            governmentHarmReport.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentHarmReport.setYear(governmentEnterpriseYear.getYear());
            governmentHarmReport.setHarmFactorId((long)map.get("harm_factor_id"));
            governmentHarmReport.setInvolvedNumber(((BigDecimal)map.get("personNum")).intValue());
            governmentHarmReport.setWomanNumber(((BigDecimal)map.get("womanNum")).intValue());
            governmentHarmReport.setFarmerNumber(((BigDecimal)map.get("migrantNum")).intValue());
            if(map.get("workplace_id")!=null){
                governmentHarmReport.setWorkplaceId((long)map.get("workplace_id"));
                governmentHarmReport.setWorkplaceName((String)map.get("workplace_name"));
                governmentHarmReport.setTestNumber((int)(long)map.get("checkNum"));
                governmentHarmReport.setLastCheckDate((Date)map.get("check_date"));
                governmentHarmReport.setCheckOrganizatio((String)map.get("name"));
                harmCheckNumber++;
            }
            governmentHarmReportMapper.insert(governmentHarmReport);
        }

        governmentEnterpriseYear.setHarmCheckNumber(harmCheckNumber);
        governmentEnterpriseYear.setHarmNumber((int)harmDeclareNumAll);
        //年度危害因素检测率
        if(harmDeclareNumAll>0){
            score+=(harmCheckNumber*1.0/harmDeclareNumAll*100);
        }

        //11.	建档比例
       //查询建档人数
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        Map<String,Object> archiveMap =governmentCustomMapper.getArchiveNumber(condition);
        governmentEnterpriseYear.setHealthArchiveNumber((int)(long)archiveMap.get("personNum"));
        if(archiveMap==null||archiveMap.get("womanNum")==null){
            governmentEnterpriseYear.setHealthArchiveWomanNumber(0);
        }else{
            governmentEnterpriseYear.setHealthArchiveWomanNumber(((BigDecimal)archiveMap.get("womanNum")).intValue());
        }
        if(archiveMap==null||archiveMap.get("migrantNum")==null){
            governmentEnterpriseYear.setHealthArchiveFatmerNumber(0);
        }else{
            governmentEnterpriseYear.setHealthArchiveFatmerNumber(((BigDecimal)archiveMap.get("migrantNum")).intValue());
        }
        //建档比例
        if(harmTotalNumber>0){
            score+=(governmentEnterpriseYear.getHealthArchiveNumber()*1.0/harmTotalNumber*100);
        }
        //12.	岗前体检率
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        archiveMap =governmentCustomMapper.getHeadMedical(condition);
        int post_head_medical_all_num=0;
        if(archiveMap!=null&&archiveMap.get("post_head_medical_all_num")!=null){
            post_head_medical_all_num=((BigDecimal)archiveMap.get("post_head_medical_all_num")).intValue();
        }
        int post_head_medical_num=0;
        if(archiveMap!=null&&archiveMap.get("post_head_medical_num")!=null){
            post_head_medical_num=((BigDecimal)archiveMap.get("post_head_medical_num")).intValue();
        }
        governmentEnterpriseYear.setPostHeadMedicalAllNum(post_head_medical_all_num);
        governmentEnterpriseYear.setPostHeadMedicalNum(post_head_medical_num);
        //岗前体检率
        if(post_head_medical_all_num>0){
            score+=(post_head_medical_num*1.0/post_head_medical_all_num*100);
        }else{
            score+=100;
        }
        //13.	离岗体检率
        int post_later_medical_all_num=0;
        if(archiveMap!=null&&archiveMap.get("post_later_medical_all_num")!=null){
            post_later_medical_all_num=((BigDecimal)archiveMap.get("post_later_medical_all_num")).intValue();
        }
        int post_later_medical_num=0;
        if(archiveMap!=null&&archiveMap.get("post_later_medical_num")!=null){
            post_later_medical_num=((BigDecimal)archiveMap.get("post_later_medical_num")).intValue();
        }

        governmentEnterpriseYear.setPostLaterMedicalAllNum(post_later_medical_all_num);
        governmentEnterpriseYear.setPostLaterMedicalNum(post_later_medical_num);
        //岗前体检率
        if(post_later_medical_all_num>0){
            score+=(post_later_medical_num*1.0/post_later_medical_all_num*100);
        }else{
            score+=100;
        }

        //14.	年度岗中体检率
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        Map medicalMap =governmentCustomMapper.getMedical(condition);
        if(medicalMap!=null&&medicalMap.get("medicalNum")!=null){
            int medicalNum=((BigDecimal)medicalMap.get("medicalNum")).intValue();
            governmentEnterpriseYear.setPostMedicalNum(medicalNum);
            //年度岗中体检率
            if(employeeNumber>0){
                score+=(medicalNum*1.0/employeeNumber*100);
            }
        }else{
            governmentEnterpriseYear.setPostMedicalNum(0);
        }
        governmentEnterpriseYear.setPostMedicalAllNum(employeeNumber);
        //



        //15.	防护用品发放
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        Map lastProvideMap =governmentCustomMapper.getLastProvide(condition);

        if(lastProvideMap!=null&&lastProvideMap.get("id")!=null){
            score+=100;
            //插入最新记录
            GovernmentLastProvide governmentLastProvide=new GovernmentLastProvide();
            governmentLastProvide.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentLastProvide.setYear(governmentEnterpriseYear.getYear());
            governmentLastProvide.setEmployeeName((String)lastProvideMap.get("employee_name"));
            governmentLastProvide.setProvideArticlesName((String)lastProvideMap.get("provide_articles_name"));
            governmentLastProvide.setProvideDate((Date)lastProvideMap.get("provide_date"));
            governmentLastProvide.setNumber((int)lastProvideMap.get("number"));
            governmentLastProvideMapper.insert(governmentLastProvide);
            governmentEnterpriseYear.setProvideProtect(100);
        }else{
            governmentEnterpriseYear.setProvideProtect(0);
        }
        //16.	防护设施维护（本年度有无防护设施维护记录（有100%，无0%））
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        condition.put("type","0");
        condition.put("sortField","service_date ");
        condition.put("sortType"," desc");
        List<EnterpriseServiceRecordCustom> enterpriseServiceRecordCustomList=enterpriseServiceRecordCustomMapper.listServiceRecords(condition);
        if(enterpriseServiceRecordCustomList.size()>0){
            score+=100;
            EnterpriseServiceRecordCustom enterpriseServiceRecordCustom=enterpriseServiceRecordCustomList.get(0);
            GovernmentServiceRecord governmentServiceRecord =new GovernmentServiceRecord();
            governmentServiceRecord.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentServiceRecord.setYear(governmentEnterpriseYear.getYear());
            //装备子表
            condition = new JSONObject();
            condition.put("recordId",enterpriseServiceRecordCustom.getId());
            condition.put("type",0);
            condition.put("sortField","id");
            condition.put("sortType","desc");
            List<EquipmentServiceUseCustom> equipmentServiceUseCustomsList= equipmentServiceUseCustomMapper.listEquipmentsByRecordId(condition);
            if(equipmentServiceUseCustomsList.size()>0){
                governmentServiceRecord.setEquipmentName(equipmentServiceUseCustomsList.get(0).getEquipmentName());
            }else{
                governmentServiceRecord.setEquipmentName(enterpriseServiceRecordCustom.getEquipmentName());
            }

            governmentServiceRecord.setReason(enterpriseServiceRecordCustom.getReason());
            governmentServiceRecord.setResult(enterpriseServiceRecordCustom.getResult());
            governmentServiceRecord.setServiceDate(enterpriseServiceRecordCustom.getServiceDate());
            governmentServiceRecord.setServicePersonnel(enterpriseServiceRecordCustom.getServicePersonnel());
            governmentServiceRecordMapper.insert(governmentServiceRecord);
            governmentEnterpriseYear.setServiceRecord(100);
        }else{
            governmentEnterpriseYear.setServiceRecord(0);
        }
        //17.	如果是高毒企业，本年度有无高毒演练记录（有100%，无0%）
        if(isHighlyToxic==1){
            condition = new JSONObject();
            //过滤当前企业
            condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
            condition.put("year",governmentEnterpriseYear.getYear());
            int n=enterpriseToxicDrillCustomMapper.listToxicDrills(condition).size();
            governmentEnterpriseYear.setToxicDrill(100);
            if(n>0){
                score+=100;
            }
        }else{
            governmentEnterpriseYear.setToxicDrill(0);
        }
        //18.	日常检查（本年度有无日常检查记录（有100%，无0%））
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        Map inspectionMap =governmentCustomMapper.getInspection(condition);
        if(inspectionMap!=null&&inspectionMap.get("sum")!=null){
            int num=(int)(long)inspectionMap.get("sum");
            if(num>0){
                score+=100;
                governmentEnterpriseYear.setCheckDate((int)(long)inspectionMap.get("sum"));
            }else{
                governmentEnterpriseYear.setCheckDate(0);
            }
        }else{
            governmentEnterpriseYear.setCheckDate(0);
        }
        //19.	整改完成率
        int check_nature_all=0;
        if(inspectionMap!=null&&inspectionMap.get("check_nature_all")!=null){
            check_nature_all=((BigDecimal)inspectionMap.get("check_nature_all")).intValue();
        }
        int check_nature=0;
        if(inspectionMap!=null&&inspectionMap.get("check_nature")!=null){
            check_nature=((BigDecimal)inspectionMap.get("check_nature")).intValue();
        }
        governmentEnterpriseYear.setCheckNature(check_nature_all);
        double rectified=100;
        if(check_nature_all>0){
            rectified=check_nature*1.0/check_nature_all*100;
        }
        governmentEnterpriseYear.setRectified(rectified);
        score+=rectified;
        //20.	三同时（有超过90天未跟新的项目0%，60天70%）
        EnterpriseThreeSimultaneityExample enterpriseThreeSimultaneityExample=new EnterpriseThreeSimultaneityExample();
        enterpriseThreeSimultaneityExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId());
        List<EnterpriseThreeSimultaneity> enterpriseThreeSimultaneityList= enterpriseThreeSimultaneityMapper.selectByExample(enterpriseThreeSimultaneityExample);
        //删除历史三同时数据
        GovernmentThreeSimultaneityExample governmentThreeSimultaneityExample=new GovernmentThreeSimultaneityExample();
        governmentThreeSimultaneityExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId());
        governmentThreeSimultaneityMapper.deleteByExample(governmentThreeSimultaneityExample);
        double three_simultaneity=100;
        for(EnterpriseThreeSimultaneity enterpriseThreeSimultaneity:enterpriseThreeSimultaneityList){
            GovernmentThreeSimultaneity governmentThreeSimultaneity=new GovernmentThreeSimultaneity();
            governmentThreeSimultaneity.setEnterpriseId(enterpriseThreeSimultaneity.getEnterpriseId());
            governmentThreeSimultaneity.setGmtCreate(enterpriseThreeSimultaneity.getGmtCreate());
            governmentThreeSimultaneity.setGmtModified(enterpriseThreeSimultaneity.getGmtModified());
            governmentThreeSimultaneity.setHarmDegree(enterpriseThreeSimultaneity.getHarmDegree());
            governmentThreeSimultaneity.setProjectName(enterpriseThreeSimultaneity.getProjectName());
            governmentThreeSimultaneity.setProjectSource(enterpriseThreeSimultaneity.getProjectSource());
            governmentThreeSimultaneity.setProjectState(enterpriseThreeSimultaneity.getProjectState());
            governmentThreeSimultaneity.setStatus(enterpriseThreeSimultaneity.getStatus());
            governmentThreeSimultaneityMapper.insert(governmentThreeSimultaneity);
            if(enterpriseThreeSimultaneity.getProjectState()!=2||enterpriseThreeSimultaneity.getStatus()!=1){
                if (enterpriseThreeSimultaneity.getGmtModified() != null) {
                    int days = ((int)((new Date()).getTime()/1000)-(int)(enterpriseThreeSimultaneity.getGmtModified().getTime()/1000))/3600/24;
                    if(days>90){
                        three_simultaneity=0;
                    }else if(days>60){
                        if(three_simultaneity>70){
                            three_simultaneity=70;
                        }
                    }
                } else {
                    int days = ((int)((new Date()).getTime()/1000)-(int)(enterpriseThreeSimultaneity.getGmtCreate().getTime()/1000))/3600/24;
                    if(days>90){
                        three_simultaneity=0;
                    }else if(days>60){
                        if(three_simultaneity>70){
                            three_simultaneity=70;
                        }
                    }
                }
            }

        }
        governmentEnterpriseYear.setThreeSimultaneity(three_simultaneity);
        score+=three_simultaneity;


        if(isHighlyToxic==1){
            score=score/20;
        }else{
            score=score/19;
        }
        governmentEnterpriseYear.setScore(score);
        //查询职业病人数和职业禁忌症人数
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        Map tabooMap =governmentCustomMapper.getTabooNumber(condition);
        if(tabooMap!=null&&tabooMap.get("taboo_number")!=null){
            governmentEnterpriseYear.setTabooNumber(((BigDecimal)tabooMap.get("taboo_number")).intValue());
        }else{
            governmentEnterpriseYear.setTabooNumber(0);
        }
        if(tabooMap!=null&&tabooMap.get("suspected_disease_number")!=null){
            governmentEnterpriseYear.setSuspectedDiseaseNumber(((BigDecimal)tabooMap.get("suspected_disease_number")).intValue());
        }else{
            governmentEnterpriseYear.setSuspectedDiseaseNumber(0);
        }
        if(tabooMap!=null&&tabooMap.get("occupational_disease_number")!=null){
            governmentEnterpriseYear.setOccupationalDiseaseNumber(((BigDecimal)tabooMap.get("occupational_disease_number")).intValue());
        }else{
            governmentEnterpriseYear.setOccupationalDiseaseNumber(0);
        }
        if(isInsert==1){
            governmentEnterpriseYearMapper.insert(governmentEnterpriseYear);
        }else{
            governmentEnterpriseYearMapper.updateByPrimaryKey(governmentEnterpriseYear);
        }
        //员工体检表  government_last_medical
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        condition.put("type","岗中");
        Map lastMedicalMap =governmentCustomMapper.getLastMedical(condition);
        if(lastMedicalMap!=null&&lastMedicalMap.get("id")!=null){
            GovernmentLastMedical governmentLastMedical=new GovernmentLastMedical();
            governmentLastMedical.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentLastMedical.setYear(governmentEnterpriseYear.getYear());
            if(lastMedicalMap.get("employee_name")!=null){
                governmentLastMedical.setEmployeeName((String)lastMedicalMap.get("employee_name"));
            }
            if(lastMedicalMap.get("inspection_name")!=null){
                governmentLastMedical.setInstitutionName((String)lastMedicalMap.get("inspection_name"));
            }
            if(lastMedicalMap.get("type")!=null){
                governmentLastMedical.setType((String)lastMedicalMap.get("type"));
            }
            if(lastMedicalMap.get("result")!=null){
                governmentLastMedical.setResult((String)lastMedicalMap.get("result"));
            }if(lastMedicalMap.get("medical_date")!=null){
                governmentLastMedical.setMedicalDate((Date)lastMedicalMap.get("medical_date"));
            }

            governmentLastMedicalMapper.insert(governmentLastMedical);
        }
        //企业员工最后培训表  government_last_training
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        Map lastTrainingMap =governmentCustomMapper.getLastTraining(condition);
        if(lastTrainingMap!=null&&lastTrainingMap.get("id")!=null){
            GovernmentLastTraining governmentLastTraining=new GovernmentLastTraining();
            governmentLastTraining.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentLastTraining.setYear(governmentEnterpriseYear.getYear());
            if(lastTrainingMap.get("theme")!=null){
                governmentLastTraining.setTheme((String)lastTrainingMap.get("theme"));
            }
            if(lastTrainingMap.get("lecturer")!=null){
                governmentLastTraining.setLecturer((String)lastTrainingMap.get("lecturer"));
            }
            if(lastTrainingMap.get("training_date")!=null){
                governmentLastTraining.setTrainingDate((Date)lastTrainingMap.get("training_date"));
            }

            governmentLastTrainingMapper.insert(governmentLastTraining);
        }
        //防护用品发放情况 government_provide_protect_art
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        List<Map> provideProtectArtList =governmentCustomMapper.getProvideProtectArt(condition);
        //删除历史数据
        GovernmentProvideProtectArtExample governmentProvideProtectArtExample=new GovernmentProvideProtectArtExample();
        governmentProvideProtectArtExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId()).andYearEqualTo(governmentEnterpriseYear.getYear());
        governmentProvideProtectArtMapper.deleteByExample(governmentProvideProtectArtExample);
        for(Map provideProtectArtMap:provideProtectArtList){
            GovernmentProvideProtectArt governmentProvideProtectArt=new GovernmentProvideProtectArt();
            governmentProvideProtectArt.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentProvideProtectArt.setYear(governmentEnterpriseYear.getYear());
            if(provideProtectArtMap.get("name")!=null){
                governmentProvideProtectArt.setName((String)provideProtectArtMap.get("name"));
            }
            if(provideProtectArtMap.get("model")!=null){
                governmentProvideProtectArt.setModel((String)provideProtectArtMap.get("model"));
            }
            if(provideProtectArtMap.get("allNumber")!=null){
                governmentProvideProtectArt.setNumber(((BigDecimal)provideProtectArtMap.get("allNumber")).intValue());
            }
            governmentProvideProtectArtMapper.insert(governmentProvideProtectArt);
        }
        //防护措施 government_protective_measures
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        List<Map> protectiveMeasuresList =governmentCustomMapper.getProtectiveMeasures(condition);
        //删除历史数据
        GovernmentProtectiveMeasuresExample governmentProtectiveMeasuresExample=new GovernmentProtectiveMeasuresExample();
        governmentProtectiveMeasuresExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId()).andYearEqualTo(governmentEnterpriseYear.getYear());
        governmentProtectiveMeasuresMapper.deleteByExample(governmentProtectiveMeasuresExample);
        for(Map protectiveMeasuresMap:protectiveMeasuresList){
            GovernmentProtectiveMeasures governmentProtectiveMeasures=new GovernmentProtectiveMeasures();
            governmentProtectiveMeasures.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentProtectiveMeasures.setYear(governmentEnterpriseYear.getYear());
            if(protectiveMeasuresMap.get("equipment_name")!=null){
                governmentProtectiveMeasures.setEquipmentName((String)protectiveMeasuresMap.get("equipment_name"));
            }
            if(protectiveMeasuresMap.get("name")!=null){
                governmentProtectiveMeasures.setWorkplaceName((String)protectiveMeasuresMap.get("name"));
            }
            if(protectiveMeasuresMap.get("num")!=null){
                governmentProtectiveMeasures.setServiceNumber((int)(long)protectiveMeasuresMap.get("num"));
            }else{
                governmentProtectiveMeasures.setServiceNumber(0);
            }
            if(protectiveMeasuresMap.get("last_service_date")!=null){
                governmentProtectiveMeasures.setLastServiceDate((Date)protectiveMeasuresMap.get("last_service_date"));
            }
            governmentProtectiveMeasuresMapper.insert(governmentProtectiveMeasures);

        }
        //企业最新检查  government_service_inspection
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        Map lastServiceInspectionMap =governmentCustomMapper.getLastServiceInspection(condition);
        if(lastServiceInspectionMap!=null&&lastServiceInspectionMap.get("id")!=null){
            GovernmentServiceInspection governmentServiceInspection=new GovernmentServiceInspection();
            governmentServiceInspection.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentServiceInspection.setYear(governmentEnterpriseYear.getYear());
            if(lastServiceInspectionMap.get("check_dept")!=null){
                governmentServiceInspection.setCheckDept((String)lastServiceInspectionMap.get("check_dept"));
            }
            if(lastServiceInspectionMap.get("check_date")!=null){
                governmentServiceInspection.setCheckDate((Date)lastServiceInspectionMap.get("check_date"));
            }
            if(lastServiceInspectionMap.get("check_nature")!=null){
                governmentServiceInspection.setCheckNature((String)lastServiceInspectionMap.get("check_nature"));
            }
            if(lastServiceInspectionMap.get("content")!=null){
                governmentServiceInspection.setContent((String)lastServiceInspectionMap.get("content"));
            }
            governmentServiceInspectionMapper.insert(governmentServiceInspection);
        }
        //企业最新整改  government_service_rectified

        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        Map lastServiceRectifiedMap =governmentCustomMapper.getLastServiceRectified(condition);
        if(lastServiceRectifiedMap!=null&&lastServiceRectifiedMap.get("id")!=null){
            GovernmentServiceRectified governmentServiceRectified=new GovernmentServiceRectified();
            governmentServiceRectified.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentServiceRectified.setYear(governmentEnterpriseYear.getYear());
            if(lastServiceRectifiedMap.get("department")!=null){
                governmentServiceRectified.setDepartment((String)lastServiceRectifiedMap.get("department"));
            }
            if(lastServiceRectifiedMap.get("rectification_date")!=null){
                governmentServiceRectified.setRectificationDate((Date)lastServiceRectifiedMap.get("rectification_date"));
            }
            if(lastServiceRectifiedMap.get("content")!=null){
                governmentServiceRectified.setContent2((String)lastServiceRectifiedMap.get("content"));
            }
            governmentServiceRectifiedMapper.insert(governmentServiceRectified);
        }
        //检测  government_outside_check
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        List<Map> outsideCheckList =governmentCustomMapper.getOutsideCheck(condition);
        //删除历史数据
        GovernmentOutsideCheckExample governmentOutsideCheckExample=new GovernmentOutsideCheckExample();
        governmentOutsideCheckExample.createCriteria().andEnterpriseIdEqualTo(governmentEnterpriseYear.getEnterpriseId()).andYearEqualTo(governmentEnterpriseYear.getYear());
        governmentOutsideCheckMapper.deleteByExample(governmentOutsideCheckExample);
        for(Map protectiveMeasuresMap:outsideCheckList){
            GovernmentOutsideCheck governmentOutsideCheck=new GovernmentOutsideCheck();
            governmentOutsideCheck.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentOutsideCheck.setYear(governmentEnterpriseYear.getYear());
            if(protectiveMeasuresMap.get("month")!=null){
                governmentOutsideCheck.setMonth((int)protectiveMeasuresMap.get("month"));
            }
            if(protectiveMeasuresMap.get("sum")!=null){
                governmentOutsideCheck.setPostMedicalNum((int)(long)protectiveMeasuresMap.get("sum"));
            }else{
                governmentOutsideCheck.setPostMedicalNum(0);
            }
            governmentOutsideCheckMapper.insert(governmentOutsideCheck);
        }
        //最后危害因素监测记录  government_last_harm_check
        condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",governmentEnterpriseYear.getEnterpriseId());
        condition.put("year",governmentEnterpriseYear.getYear());
        Map lastHarmCheckMap =governmentCustomMapper.getLastHarmCheck(condition);
        if(lastHarmCheckMap!=null&&lastHarmCheckMap.get("id")!=null){
            GovernmentLastHarmCheck governmentLastHarmCheck=new GovernmentLastHarmCheck();
            governmentLastHarmCheck.setEnterpriseId(governmentEnterpriseYear.getEnterpriseId());
            governmentLastHarmCheck.setYear(governmentEnterpriseYear.getYear());
            if(lastHarmCheckMap.get("harm_factor_id")!=null){
                governmentLastHarmCheck.setHarmFactorId((long)lastHarmCheckMap.get("harm_factor_id"));
            }
            if(lastHarmCheckMap.get("harm_factor_name")!=null){
                governmentLastHarmCheck.setHarmFactorName((String)lastHarmCheckMap.get("harm_factor_name"));
            }
            if(lastHarmCheckMap.get("check_organization")!=null){
                governmentLastHarmCheck.setCheckOrganization((String)lastHarmCheckMap.get("check_organization"));
            }
            if(lastHarmCheckMap.get("is_qualified")!=null){
                governmentLastHarmCheck.setIsQualified((String)lastHarmCheckMap.get("is_qualified"));
            }

            if(lastHarmCheckMap.get("check_date")!=null){
                governmentLastHarmCheck.setCheckDate((Date)lastHarmCheckMap.get("check_date"));
            }
            governmentLastHarmCheckMapper.insert(governmentLastHarmCheck);
        }

    }


}
