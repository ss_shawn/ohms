package com.lanware.ehs.enterprise.job.task;

import com.google.common.collect.Maps;
import com.lanware.ehs.common.utils.DateUtil;
import com.lanware.ehs.enterprise.employee.service.EmployeeService;
import com.lanware.ehs.enterprise.medical.service.WaitMedicalService;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.pojo.EmployeeWaitMedical;
import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.custom.EmployeeWaitMedicalCustom;
import com.lanware.ehs.pojo.custom.EnterpriseEmployeeCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 体检任务
 */
@Service
public class MedicalTask{
    @Autowired
    private YearplanService yearplanService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private WaitMedicalService waitMedicalService;


    public void executeTask(){
        LocalDate localDate = LocalDate.now();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        String waitMedicalDate = year + "-" +month;
        //查询年度计划健康体检执行月份。
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("year",year);
        condition.put("planType","jktj");
        List<EnterpriseYearPlan> yearPlans = yearplanService.findYearplansByCondition(condition);
        for(EnterpriseYearPlan yearPlan : yearPlans){
            String planPeriod = yearPlan.getPlanPeriod();
            if(org.apache.commons.lang3.StringUtils.isNotEmpty(planPeriod)){
                String[] medicalMonths = planPeriod.split(",");
                for(String medicalMonth : medicalMonths){
                    if(Integer.parseInt(medicalMonth) == month){
                        insertWaitMetical(waitMedicalDate,yearPlan);
                    }
                }
            }
        }

    }

    /**
     * 生成待体检记录
     * @param waitMedicalDate
     */
    private void insertWaitMetical(String waitMedicalDate,EnterpriseYearPlan yearPlan) {
        //查找所有在岗的员工
        List<EnterpriseEmployeeCustom> employeeCustoms =
                employeeService.findWorkEmployeesByEnterpriseId(yearPlan.getEnterpriseId());
        for(EnterpriseEmployeeCustom employeeCustom : employeeCustoms){
            //查询该员工在待体检表中是否已经生成待体检记录
            Map<String,Object> condition = Maps.newHashMap();
            condition.put("type", EmployeeWaitMedicalCustom.TYPE_IN_POST);
            condition.put("waitMedicalDate",waitMedicalDate);
            condition.put("employeeId",employeeCustom.getId());
            List<EmployeeWaitMedicalCustom> waitMedicalCustoms =  waitMedicalService.queryList(condition);
            if(waitMedicalCustoms.size() ==  0){
                //该员工还未生成待体检记录，这里插入待体检记录
                EmployeeWaitMedical employeeWaitMedical = new EmployeeWaitMedical();
                employeeWaitMedical.setEmployeeId(employeeCustom.getId());
                employeeWaitMedical.setPostId(employeeCustom.getPostId());
                employeeWaitMedical.setWorkDate(DateUtil.localDate2Date(LocalDate.now()));
                employeeWaitMedical.setStatus(EmployeeWaitMedicalCustom.STATUS_MEDICAL_NO);
                employeeWaitMedical.setType(EmployeeWaitMedicalCustom.TYPE_IN_POST);
                employeeWaitMedical.setWaitMeticalDate(waitMedicalDate);
                employeeWaitMedical.setGmtCreate(new Date());
                waitMedicalService.addWaitMedical(employeeWaitMedical);
            }
        }

    }
}
