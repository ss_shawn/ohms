package com.lanware.ehs.enterprise.check.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.check.service.OutsideCheckService;
import com.lanware.ehs.mapper.EnterpriseOutsideCheckMapper;
import com.lanware.ehs.mapper.EnterpriseOutsideCheckResultMapper;
import com.lanware.ehs.mapper.EnterpriseWorkplaceWaitNotifyMapper;
import com.lanware.ehs.mapper.custom.EnterpriseOutsideCheckCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseWaitCheckCustomMapper;
import com.lanware.ehs.mapper.custom.MonitorWorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckCustom;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceWaitNotifyCustom;
import com.lanware.ehs.pojo.custom.MonitorWorkplaceHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
/**
 * @description 企业外部检测interface实现类
 */
public class OutsideCheckServiceImpl implements OutsideCheckService {
    /** 注入enterpriseOutsideCheckMapper的bean */
    @Autowired
    private EnterpriseOutsideCheckMapper enterpriseOutsideCheckMapper;
    /** 注入enterpriseOutsideCheckCustomMapper的bean */
    @Autowired
    private EnterpriseOutsideCheckCustomMapper enterpriseOutsideCheckCustomMapper;
    /** 注入enterpriseWaitCheckCustomMapper的bean */
    @Autowired
    private EnterpriseWaitCheckCustomMapper enterpriseWaitCheckCustomMapper;
    /** 注入monitorWorkplaceHarmFactorCustomMapper的bean */
    @Autowired
    private MonitorWorkplaceHarmFactorCustomMapper monitorWorkplaceHarmFactorCustomMapper;
    /** 注入enterpriseOutsideCheckResultMapper的bean */
    @Autowired
    private EnterpriseOutsideCheckResultMapper enterpriseOutsideCheckResultMapper;
    /** 注入enterpriseWorkplaceWaitNotifyMapper的bean */
    @Autowired
    private EnterpriseWorkplaceWaitNotifyMapper enterpriseWorkplaceWaitNotifyMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    @Override
    /**
     * @description 查询危害因素检测list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseOutsideChecks和totalNum的result
     */
    public JSONObject listOutsideChecks(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseOutsideCheckCustom> enterpriseOutsideChecks
                = enterpriseOutsideCheckCustomMapper.listOutsideChecks(condition);
        result.put("enterpriseOutsideChecks", enterpriseOutsideChecks);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseOutsideCheckCustom> pageInfo = new PageInfo<EnterpriseOutsideCheckCustom>(enterpriseOutsideChecks);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 根据id查询危害因素检测信息
     * @param id 危害因素检测id
     * @return com.lanware.management.pojo.EnterpriseOutsideCheck 返回EnterpriseOutsideCheck对象
     */
    public EnterpriseOutsideCheck getOutsideCheckById(Long id) {
        return enterpriseOutsideCheckMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 根据id查询危害因素检测扩展信息
     * @param id 危害因素检测id
     * @return com.lanware.management.pojo.EnterpriseOutsideCheck 返回EnterpriseOutsideCheck对象
     */
    @Override
    public EnterpriseOutsideCheckCustom getOutsideCheckCustomById(Long id) {
        return enterpriseOutsideCheckCustomMapper.getOutsideCheckCustomById(id);
    }

    /**
     * @description 新增危害因素检测信息
     * @param enterpriseOutsideCheck 危害因素检测pojo
     * @param checkReportFile 检测报告书
     * @param enterpriseWaitCheck 待检测项目pojo
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertOutsideCheck(EnterpriseOutsideCheck enterpriseOutsideCheck, MultipartFile checkReportFile
            , EnterpriseWaitCheck enterpriseWaitCheck) {
        // 新增数据先保存，以便获取id
        enterpriseOutsideCheckMapper.insert(enterpriseOutsideCheck);
        // 更新待检测
        enterpriseWaitCheck.setOutsideCheckId(enterpriseOutsideCheck.getId());
        enterpriseWaitCheckCustomMapper.updateByPrimaryKeySelective(enterpriseWaitCheck);
        // 新增检测结果(按照检测因素-工作场所-监测点批量插入)
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("waitCheckId", enterpriseWaitCheck.getId());
        condition.put("enterpriseId", enterpriseOutsideCheck.getEnterpriseId());
        List<MonitorWorkplaceHarmFactorCustom> monitorWorkplaceHarmFactorCustomList
                = monitorWorkplaceHarmFactorCustomMapper.listCheckResults(condition);
        // 新增检测结果，同时获取检测结果相关的作业场所list
        List<Long> workplaceIdList = new ArrayList<Long>();
        for (MonitorWorkplaceHarmFactorCustom monitorWorkplaceHarmFactorCustom : monitorWorkplaceHarmFactorCustomList) {
            EnterpriseOutsideCheckResult enterpriseOutsideCheckResult = new EnterpriseOutsideCheckResult();
            enterpriseOutsideCheckResult.setOutsideCheckId(monitorWorkplaceHarmFactorCustom.getOutsideCheckId());
            enterpriseOutsideCheckResult.setWorkplaceId(monitorWorkplaceHarmFactorCustom.getWorkplaceId());
            enterpriseOutsideCheckResult.setMonitorPointId(monitorWorkplaceHarmFactorCustom.getMonitorPointId());
            enterpriseOutsideCheckResult.setHarmFactorId(monitorWorkplaceHarmFactorCustom.getHarmFactorId());
            // 默认状态为未取消检测
            enterpriseOutsideCheckResult.setStatus(EnterpriseOutsideCheckResultCustom.STATUS_CANCEL_CHECK_NO);
            // 默认检测结果都是合格
            enterpriseOutsideCheckResult.setIsQualified("");
            enterpriseOutsideCheckResult.setGmtCreate(new Date());
            enterpriseOutsideCheckResultMapper.insert(enterpriseOutsideCheckResult);
            // 包含检测结果的作业场所id的list
            workplaceIdList.add(monitorWorkplaceHarmFactorCustom.getWorkplaceId());
        }
        // 删除作业场所id的list中重复的id
        workplaceIdList = workplaceIdList.stream().distinct().collect(Collectors.toList());
        // 如果检测的作业场所没有被告知，新增检测结果告知记录
        EnterpriseWorkplaceWaitNotifyExample enterpriseWorkplaceWaitNotifyExample
                = new EnterpriseWorkplaceWaitNotifyExample();
        EnterpriseWorkplaceWaitNotifyExample.Criteria criteria = enterpriseWorkplaceWaitNotifyExample.createCriteria();
        criteria.andWorkplaceIdIn(workplaceIdList).andCheckIdEqualTo(enterpriseOutsideCheck.getId());
        criteria.andStatusEqualTo(EnterpriseWorkplaceWaitNotifyCustom.STATUS_NOTIFY_NO);
        List<EnterpriseWorkplaceWaitNotify> enterpriseWorkplaceWaitNotifyList
                = enterpriseWorkplaceWaitNotifyMapper.selectByExample(enterpriseWorkplaceWaitNotifyExample);
        // 检测结果的作业场所id中已经有待告知记录的作业场所id的list
        List<Long> waitNotifyWorkplaceIdList = new ArrayList<Long>();
        for (EnterpriseWorkplaceWaitNotify enterpriseWorkplaceWaitNotify : enterpriseWorkplaceWaitNotifyList) {
            waitNotifyWorkplaceIdList.add(enterpriseWorkplaceWaitNotify.getWorkplaceId());
        }
        // 找出两个作业场所id的list中不重复的部分，进行新增作业场所待告知的记录
        workplaceIdList.removeAll(waitNotifyWorkplaceIdList);
        for (Long workplaeId : workplaceIdList) {
            EnterpriseWorkplaceWaitNotify enterpriseWorkplaceWaitNotify = new EnterpriseWorkplaceWaitNotify();
            enterpriseWorkplaceWaitNotify.setEnterpriseId(enterpriseOutsideCheck.getEnterpriseId());
            enterpriseWorkplaceWaitNotify.setWorkplaceId(workplaeId);
            enterpriseWorkplaceWaitNotify.setCheckId(enterpriseOutsideCheck.getId());
            enterpriseWorkplaceWaitNotify.setStatus(EnterpriseWorkplaceWaitNotifyCustom.STATUS_NOTIFY_NO);
            enterpriseWorkplaceWaitNotify.setGmtCreate(new Date());
            enterpriseWorkplaceWaitNotifyMapper.insert(enterpriseWorkplaceWaitNotify);
        }
        // 上传并保存检测报告书
        if (checkReportFile != null){
            String checkReport = "/" + enterpriseOutsideCheck.getEnterpriseId() + "/" + ModuleName.CHECK.getValue()
                    + "/" + enterpriseOutsideCheck.getId() + "/" + checkReportFile.getOriginalFilename();
            try {
                ossTools.uploadStream(checkReport, checkReportFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传检测报告书失败", e);
            }
            enterpriseOutsideCheck.setCheckReport(checkReport);
        }
        return enterpriseOutsideCheckMapper.updateByPrimaryKeySelective(enterpriseOutsideCheck);
    }

    @Override
    /**
     * @description 更新危害因素检测信息
     * @param enterpriseOutsideCheck 危害因素检测pojo
     * @param checkReportFile 检测报告书
     * @return int 更新成功的数目
     */
    public int updateOutsideCheck(EnterpriseOutsideCheck enterpriseOutsideCheck, MultipartFile checkReportFile) {
        // 上传并保存检测报告书
        if (checkReportFile != null){
            // 上传了新的申报文件
            if (!StringUtils.isEmpty(enterpriseOutsideCheck.getCheckReport())){
                // 删除旧文件
                ossTools.deleteOSS(enterpriseOutsideCheck.getCheckReport());
            }
            String checkReport = "/" + enterpriseOutsideCheck.getEnterpriseId() + "/" + ModuleName.CHECK.getValue()
                    + "/" + enterpriseOutsideCheck.getId() + "/" + checkReportFile.getOriginalFilename();
            try {
                ossTools.uploadStream(checkReport, checkReportFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传检测报告书失败", e);
            }
            enterpriseOutsideCheck.setCheckReport(checkReport);
        }
        return enterpriseOutsideCheckMapper.updateByPrimaryKeySelective(enterpriseOutsideCheck);
    }

    /**
     * @description 删除危害因素检测信息
     * @param id 要删除的id
     * @param checkReport 要删除的检测报告书路径
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public void deleteOutsideCheck(Long id, String checkReport) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(checkReport);
        // 删除数据库中对应的主表危害因素检测数据
        enterpriseOutsideCheckMapper.deleteByPrimaryKey(id);

        //删除危害因素检测对应的子表检测结果数据
        EnterpriseOutsideCheckResultExample example = new EnterpriseOutsideCheckResultExample();
        EnterpriseOutsideCheckResultExample.Criteria criteria = example.createCriteria();
        criteria.andOutsideCheckIdEqualTo(id);
        enterpriseOutsideCheckResultMapper.deleteByExample(example);
        //删除检测告知
        EnterpriseWorkplaceWaitNotifyExample enterpriseWorkplaceWaitNotifyExample=new EnterpriseWorkplaceWaitNotifyExample();
        EnterpriseWorkplaceWaitNotifyExample.Criteria criteria2 = enterpriseWorkplaceWaitNotifyExample.createCriteria();
        criteria2.andCheckIdEqualTo(id);
        criteria2.andStatusEqualTo(EnterpriseWorkplaceWaitNotifyCustom.STATUS_NOTIFY_NO);
        enterpriseWorkplaceWaitNotifyMapper.deleteByExample(enterpriseWorkplaceWaitNotifyExample);
        //修改待检测状态
        EnterpriseWaitCheckExample enterpriseWaitCheckExample=new EnterpriseWaitCheckExample();
        enterpriseWaitCheckExample.createCriteria().andOutsideCheckIdEqualTo(id);
        List<EnterpriseWaitCheck> waitCheckList = enterpriseWaitCheckCustomMapper.selectByExample(enterpriseWaitCheckExample);
        for(EnterpriseWaitCheck waitCheck : waitCheckList){
            waitCheck.setOutsideCheckId(null);
            waitCheck.setStatus(0);
            enterpriseWaitCheckCustomMapper.updateByPrimaryKey(waitCheck);
        }
    }

    /**
     * @description 批量删除危害因素检测信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public void deleteOutsideChecks(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseOutsideCheck enterpriseOutsideCheck = enterpriseOutsideCheckMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseOutsideCheck.getCheckReport());
        }

        // 删除数据库中对应的数据
        EnterpriseOutsideCheckExample enterpriseOutsideCheckExample = new EnterpriseOutsideCheckExample();
        EnterpriseOutsideCheckExample.Criteria criteria = enterpriseOutsideCheckExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        enterpriseOutsideCheckMapper.deleteByExample(enterpriseOutsideCheckExample);

        //删除危害因素检测对应的子表记录
        EnterpriseOutsideCheckResultExample resultExample = new EnterpriseOutsideCheckResultExample();
        EnterpriseOutsideCheckResultExample.Criteria resultCriteria = resultExample.createCriteria();
        resultCriteria.andOutsideCheckIdIn(Arrays.asList(ids));
        enterpriseOutsideCheckResultMapper.deleteByExample(resultExample);
    }

    /**
     * @description 查询危害因素检测扩展list
     * @param condition 查询条件
     * @return List<EnterpriseOutsideCheckCustom>
     */
    @Override
    public List<EnterpriseOutsideCheckCustom> listOutsideCheckCustoms(Map<String, Object> condition) {
        return enterpriseOutsideCheckCustomMapper.listOutsideCheckCustoms(condition);
    }
}
