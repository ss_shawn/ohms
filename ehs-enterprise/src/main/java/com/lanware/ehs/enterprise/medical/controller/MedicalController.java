package com.lanware.ehs.enterprise.medical.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.medical.service.MedicalService;
import com.lanware.ehs.pojo.EmployeeMedical;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EmployeeMedicalCustom;
import com.lanware.ehs.pojo.custom.EmployeeWaitMedicalCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 体检管理controller
 */
@Controller
@RequestMapping("/medical")
public class MedicalController {
    @Autowired
    /** 注入medicalService的bean */
    private MedicalService medicalService;
    
    @RequestMapping("")
    /**
     * @description 返回检测管理页面
     * @param
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "medical/list";
    }

    @RequestMapping("/listMedicals")
    @ResponseBody
    /**
     * @description 获取体检人员result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 当前登录用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listMedicals(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = medicalService.listMedicals(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/getEmployee")
    @ResponseBody
    /**
     * @description 获取体检员工result
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat getEmployee(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject result = medicalService.listEmployeesByEnterprieId(enterpriseUser.getEnterpriseId());
        return ResultFormat.success(result);
    }

    @RequestMapping("/listPostsByEmployeeId")
    @ResponseBody
    /**
     * @description 根据员工id获取岗位list
     * @param employeeId 员工id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat getEmployee(Long employeeId) {
        JSONObject result = medicalService.listPostsByEmployeeId(employeeId);
        return ResultFormat.success(result);
    }

    @RequestMapping("/addMedical/save")
    @ResponseBody
    /** @description 新增待体检员工体检信息，并上传文件到oss服务器
     * @param employeeMedicalJSON 员工体检json
     * @param medicalReportFile 体检报告
     * @param waitMedicalId 待体检id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String employeeMedicalJSON, MultipartFile medicalReportFile, Long waitMedicalId
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EmployeeMedical employeeMedical = JSONObject.parseObject(employeeMedicalJSON, EmployeeMedical.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        employeeMedical.setGmtCreate(new Date());
        // 待体检查询条件
        Map<String ,Object> condition = new HashMap<String ,Object>();
        condition.put("id", waitMedicalId);
        condition.put("status", EmployeeWaitMedicalCustom.STATUS_MEDICAL_YES);
        condition.put("gmtModified", new Date());
        if (medicalService.insertWaitMedical(employeeMedical, medicalReportFile, enterpriseId, condition) > 0) {
            return ResultFormat.success("添加成功");
        } else {
            return ResultFormat.error("添加失败");
        }
    }

    @RequestMapping("/editMedical/save")
    @ResponseBody
    /** @description 保存员工体检信息，并上传文件到oss服务器
     * @param employeeMedicalJSON 员工体检json
     * @param medicalReportFile 体检报告
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String employeeMedicalJSON, MultipartFile medicalReportFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EmployeeMedical employeeMedical = JSONObject.parseObject(employeeMedicalJSON, EmployeeMedical.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (employeeMedical.getId() == null) {
            employeeMedical.setGmtCreate(new Date());
            if (medicalService.insertMedical(employeeMedical, medicalReportFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            employeeMedical.setGmtModified(new Date());
            if (medicalService.updateMedical(employeeMedical, medicalReportFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/viewMedical")
    /**
     * @description 查看员工体检扩展信息
     * @param id 员工体检id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewMedical(Long id) {
        JSONObject data = new JSONObject();
        EmployeeMedicalCustom employeeMedicalCustom = medicalService.getMedicalCustomById(id);
        data.put("employeeMedical", employeeMedicalCustom);
        return new ModelAndView("medical/view", data);
    }

    @RequestMapping("/deleteMedical")
    @ResponseBody
    /**
     * @description 删除员工体检信息
     * @param id 要删除的id
     * @param type 体检类型
     * @param result 体检结论
     * @param medicalReport 要删除的体检报告路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteMedical(Long id, String type, String result, String medicalReport) {
        if (medicalService.deleteMedical(id, type, result, medicalReport) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteMedicals")
    @ResponseBody
    /**
     * @description 批量删除员工体检信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteMedicals(Long[] ids) {
        if (medicalService.deleteMedicals(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/listMedicalPersonNum")
    @ResponseBody
    /**
     * @description 获取上年度体检人员各项人数
     * @param lastYear 上年度
     * @param enterpriseUser 当前登录用户
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listMedicalPersonNum(Integer lastYear, @CurrentUser EnterpriseUser enterpriseUser) {
        // 查询条件
        Map<String, Object> condition = new HashMap<>();
        condition.put("lastYear", lastYear);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        JSONObject result = medicalService.listMedicalPersonNum(condition);
        return ResultFormat.success(result);
    }
}
