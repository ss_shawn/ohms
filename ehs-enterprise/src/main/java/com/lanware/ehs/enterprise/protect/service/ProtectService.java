package com.lanware.ehs.enterprise.protect.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.EnterpriseProtectArticles;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @description 防护用品interface
 */
public interface ProtectService {

    /**
     * @description 根据id查询防护用品
     * @param id 防护用品id
     * @return com.lanware.management.pojo.EnterpriseProtectArticlesCustom 返回EnterpriseProtectArticles对象
     */
    EnterpriseProtectArticles getEnterpriseProtectArticlesById(Long id);
    /**
     * @description 根据查询防护用品
     * @param 查询条件
     * @return com.lanware.management.pojo.EnterpriseProtectArticlesCustom 返回EnterpriseProtectArticles对象
     */
    Page<EnterpriseProtectArticlesCustom> findProtectList(Map<String, Object> condition, QueryRequest request);

    /***
     * 获取全部危害因素
     * @return危害因素列表
     */
    List<BaseHarmFactor> getAllBaseHarmFactor(String keyword);

    /**
     * 保存防护用品,
     * @param enterpriseProtectArticles 防护用品 signatureRecord 图片
     * @return成功与否 1成功 0不成功
     */
     int saveEnterpriseProtectArticles(EnterpriseProtectArticlesCustom enterpriseProtectArticles,MultipartFile image,MultipartFile certificatePath);
    /**
     * 修改防护用品,
     * @param enterpriseProtectArticles
     * @return成功与否 1成功 0不成功
     */
    int updateEnterpriseProtectArticles(EnterpriseProtectArticlesCustom enterpriseProtectArticles);
    /**
     * 通过id获取防护用品
     * @param id
     * @return防护用品
     */
    EnterpriseProtectArticlesCustom getEnterpriseProtectArticlesCustomByID(long id);
    /**
     * 删除防护用品,
     * @param id
     * @return成功与否 1成功 0不成功
     */
    int deleteEnterpriseProtectArticles(long id);

    /**
     * 上传合格证书
     * @param id 防护用品id
     * @param certificate 合格证书
     * @return int 更新成功的数目
     */
    int uploadCertificate(Long id, MultipartFile certificate);
    /**
     * 上传图片
     * @param id 防护用品id
     * @param image 图片
     * @return int 更新成功的数目
     */
    int uploadImage(Long id, MultipartFile image);
}
