package com.lanware.ehs.enterprise.check.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.check.service.WaitCheckService;
import com.lanware.ehs.mapper.WaitCheckHarmFactorMapper;
import com.lanware.ehs.mapper.custom.EnterpriseWaitCheckCustomMapper;
import com.lanware.ehs.pojo.EnterpriseWaitCheck;
import com.lanware.ehs.pojo.WaitCheckHarmFactor;
import com.lanware.ehs.pojo.WaitCheckHarmFactorExample;
import com.lanware.ehs.pojo.custom.EnterpriseWaitCheckCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description 待检测项目interface实现类
 */
@Service
public class WaitCheckServiceImpl implements WaitCheckService {
    /** 注入enterpriseWaitCheckCustomMapper的bean */
    @Autowired
    private EnterpriseWaitCheckCustomMapper enterpriseWaitCheckCustomMapper;

    @Autowired
    private WaitCheckHarmFactorMapper waitCheckHarmFactorMapper;

    /**
     * @description 查询待检测项目list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWaitChecks和totalNum的result
     */
    @Override
    public JSONObject listWaitChecks(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseWaitCheckCustom> enterpriseWaitCheckCustoms
                = enterpriseWaitCheckCustomMapper.listWaitChecks(condition);
        result.put("enterpriseWaitChecks", enterpriseWaitCheckCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseWaitCheckCustom> pageInfo = new PageInfo<EnterpriseWaitCheckCustom>(enterpriseWaitCheckCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 查询待检测项目台账list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWaitChecks和totalNum的result
     */
    @Override
    public JSONObject listWaitCheckAccounts(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseWaitCheckCustom> enterpriseWaitCheckCustoms
                = enterpriseWaitCheckCustomMapper.listWaitCheckAccounts(condition);
        result.put("enterpriseWaitChecks", enterpriseWaitCheckCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseWaitCheckCustom> pageInfo = new PageInfo<EnterpriseWaitCheckCustom>(enterpriseWaitCheckCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    public List<EnterpriseWaitCheckCustom> findEnterpriseWaitChecks(Map<String,Object> parameter) {
        return enterpriseWaitCheckCustomMapper.findEnterpriseWaitChecks(parameter);
    }

    @Override
    public void insert(EnterpriseWaitCheck waitCheck) {
        enterpriseWaitCheckCustomMapper.insert(waitCheck);
    }

    @Override
    public void insertWaitCheckHarmFactor(WaitCheckHarmFactor waitCheckHarmFactor) {
        waitCheckHarmFactorMapper.insert(waitCheckHarmFactor);
    }


    @Override
    public void delete(Long id){
       enterpriseWaitCheckCustomMapper.deleteByPrimaryKey(id);
        //删除待检测项目关联的检测危害因素
        WaitCheckHarmFactorExample example = new WaitCheckHarmFactorExample();
        WaitCheckHarmFactorExample.Criteria criteria = example.createCriteria();
        criteria.andWaitCheckIdEqualTo(id);
        waitCheckHarmFactorMapper.deleteByExample(example);
    }
}
