package com.lanware.ehs.enterprise.declare.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseDeclare;
import com.lanware.ehs.pojo.custom.EnterpriseDeclareCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @description 作业场所危害申报interface
 */
public interface DeclareService {
    /**
     * @description 根据企业id查询作业场所危害申报list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces和totalNum的result
     */
    JSONObject listDeclaresByEnterpriseId(Integer pageNum, Integer pageSize, Map<String, Object> condition, Long enterpriseId);

    /**
     * @description 根据id查询作业场所危害申报信息
     * @param id 危害申报id
     * @return com.lanware.management.pojo.EnterpriseDeclare 返回EnterpriseDeclare对象
     */
    EnterpriseDeclare getDeclareById(Long id);

    /**
     * @description 新增危害申报信息
     * @param enterpriseDeclare 危害申报pojo
     * @param declareFile 申报文件
     * @return int 插入成功的数目
     */
    int insertDeclare(EnterpriseDeclare enterpriseDeclare, MultipartFile declareFile);

    /**
     * @description 更新危害申报信息
     * @param enterpriseDeclare 危害申报pojo
     * @param declareFile 申报文件
     * @return int 更新成功的数目
     */
    int updateDeclare(EnterpriseDeclare enterpriseDeclare, MultipartFile declareFile);

    /**
     * @description 更新申报回执信息
     * @param enterpriseDeclare 危害申报pojo
     * @param receiptFile 回执文件
     * @return int 更新成功的数目
     */
    int updateDeclareReceipt(EnterpriseDeclare enterpriseDeclare, MultipartFile receiptFile);

    /**
     * @description 删除危害申报信息
     * @param id 要删除的id
     * @param declareAttachment 要删除的申报附件路径
     * @param receiptAttachment 要删除的回执文件路径
     * @return int 删除成功的数目
     */
    int deleteDeclare(Long id, String declareAttachment, String receiptAttachment);

    /**
     * @description 批量删除危害申报信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteDeclares(Long[] ids);

    /**
     * 查询作业场所危害申报未回执数据集
     * @param entity
     * @return
     */
    List<EnterpriseDeclare> queryDeclareNoReceipt(EnterpriseDeclareCustom entity);

    Map<String, Object> getDeclaredData(Long enterpriseId);
}
