package com.lanware.ehs.enterprise.check.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseCheckResultDetail;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckResult;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 检测结果interface
 */
public interface CheckResultService {
    /**
     * @description 获取检测结果result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param condition 查询条件(外部检测id和危害因素id)
     * @return com.lanware.management.common.format.ResultFormat
     */
    JSONObject listCheckResults(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    List<EnterpriseOutsideCheckResultCustom> getHarmFactorIdsByOutsideCheckId(Long outsideCheckId);

    EnterpriseOutsideCheckResult getCheckResultById(Long id);

    int updateCheckResult(EnterpriseOutsideCheckResult enterpriseOutsideCheckResult);

    int cancelCheckResult(Map<String, Object> condition);

    JSONObject listCheckResultDetails(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    int insertDetail(EnterpriseCheckResultDetail enterpriseCheckResultDetail);

    int updateDetail(EnterpriseCheckResultDetail enterpriseCheckResultDetail);

    EnterpriseCheckResultDetail getCheckResultDetailById(Long id);

    int deleteCheckResultDetail(Long id);

    int deleteCheckResultDetails(Long[] ids);

    List<EnterpriseOutsideCheckResultCustom> listHarmFactorsByOutsideCheckId(Long outsideCheckId);

    List<EnterpriseOutsideCheckResultCustom> listCheckResultCustoms(Map<String, Object> condition);

    int insertCheckResultDetails(List<EnterpriseCheckResultDetail> checkResultDetailList);
}
