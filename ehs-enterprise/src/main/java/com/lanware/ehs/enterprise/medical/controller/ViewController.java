package com.lanware.ehs.enterprise.medical.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.medical.service.DiseaseDiagnosiService;
import com.lanware.ehs.enterprise.medical.service.MedicalService;
import com.lanware.ehs.enterprise.medical.service.WaitMedicalService;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.custom.EmployeeMedicalCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 体检管理视图
 */
@Controller("medicalView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @Autowired
    /** 注入waitMedicalService的bean */
    private WaitMedicalService waitMedicalService;
    @Autowired
    /** 注入medicalService的bean */
    private MedicalService medicalService;
    @Autowired
    /** 注入diseaseDiagnosiService的bean */
    private DiseaseDiagnosiService diseaseDiagnosiService;



//    /**
//     * @description 体检管理条件进入
//     * @return 页面路径
//     */
//    @GetMapping("medical/{type}")
//    public ModelAndView medicalType(@PathVariable String type ) {
//        JSONObject data = new JSONObject();
//        data.put("type",type==null?"":type);
//        return new ModelAndView("medical/list", data);
//    }

    /**
     * @description 首页岗中待体检进入
     * @return 页面路径
     */
    @GetMapping("medical/{tabIndex}/{keyword}")
    public ModelAndView medicalPosting(@PathVariable Integer tabIndex,@PathVariable String keyword) {
        JSONObject data = new JSONObject();
        data.put("tabIndex", tabIndex);
        String convertKeyword = "";
        if(tabIndex == 0){

            if(keyword.equals("leaveJob")){
                convertKeyword = "离岗";
            }else if(keyword.equals("preJob")){
                convertKeyword = "岗前";
            }
            data.put("wait_medical_keyword",convertKeyword);
        }else if(tabIndex == 2){
            if(keyword.equals("doubtDisease")){
                convertKeyword = "待诊断职业病";
            }
            data.put("medical_keyword", convertKeyword);
        }

        return new ModelAndView("medical/list", data);
    }

    /**
     * @description 体检管理
     * @return 页面路径
     */
    @GetMapping("medical")
    public ModelAndView medical() {
        JSONObject data = new JSONObject();
        return new ModelAndView("medical/list", data);
    }



    @RequestMapping("medical/addMedical/{waitMedicalId}/{employeeId}/{postId}/{type}")
    /**
     * @description 发起体检
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView initiateMedical(@PathVariable Long waitMedicalId, @PathVariable Long employeeId
            ,@PathVariable Long postId, @PathVariable String type) {
        JSONObject data = new JSONObject();
        EmployeeMedicalCustom employeeMedicalCustom = new EmployeeMedicalCustom();
        employeeMedicalCustom.setEmployeeId(employeeId);
        employeeMedicalCustom.setPostId(postId);
        employeeMedicalCustom.setEmployeeName(waitMedicalService.getEmployeeNameById(employeeId));
        employeeMedicalCustom.setType(type);
        data.put("waitMedicalId", waitMedicalId);
        data.put("employeeMedical", employeeMedicalCustom);
        return new ModelAndView("medical/add", data);
    }

    @RequestMapping("medical/editMedical")
    /**
     * @description 新增体检信息
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addMedical() {
        JSONObject data = new JSONObject();
        EmployeeMedicalCustom employeeMedicalCustom = new EmployeeMedicalCustom();
        data.put("employeeMedical", employeeMedicalCustom);
        return new ModelAndView("medical/edit", data);
    }

    /**
     * @description 编辑体检信息
     * @param id 体检信息id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("medical/editMedical/{id}")
    public ModelAndView editMedical(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EmployeeMedicalCustom employeeMedicalCustom = medicalService.getMedicalCustomById(id);
        data.put("employeeMedical", employeeMedicalCustom);
        return new ModelAndView("medical/edit", data);
    }

    @RequestMapping("medical/diseaseDiagnosi/{medicalId}/{employeeId}")
    /**
     * @description 返回职业病诊断页面
     * @param medicalId 体检id
     * @param employeeId 体检员工id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView list(@PathVariable Long medicalId, @PathVariable Long employeeId){
        JSONObject data = new JSONObject();
        data.put("medicalId", medicalId);
        data.put("employeeId", employeeId);
        return new ModelAndView("medical/diseaseDiagnosi/list", data);
    }

    @RequestMapping("medical/diseaseDiagnosi/addDiseaseDiagnosi/{medicalId}/{employeeId}")
    /**
     * @description 新增职业病诊断
     * @param medicalId 体检id
     * @param employeeId 体检员工id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addCheckResult(@PathVariable Long medicalId, @PathVariable Long employeeId) {
        JSONObject data = new JSONObject();
        EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi = new EmployeeDiseaseDiagnosi();
        employeeDiseaseDiagnosi.setMedicalId(medicalId);
        employeeDiseaseDiagnosi.setEmployeeId(employeeId);
        data.put("employeeDiseaseDiagnosi", employeeDiseaseDiagnosi);
        return new ModelAndView("medical/diseaseDiagnosi/edit", data);
    }

    @RequestMapping("medical/diseaseDiagnosi/editDiseaseDiagnosi/{id}")
    /**
     * @description 编辑职业病诊断
     * @param id 职业病诊断id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editCheckResult(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi = diseaseDiagnosiService.getDiseaseDiagnosiById(id);
        data.put("employeeDiseaseDiagnosi", employeeDiseaseDiagnosi);
        return new ModelAndView("medical/diseaseDiagnosi/edit", data);
    }

    /**
     * @description 返回选择未体检原因页面
     * @param waitMedicalId 待体检id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("waitMedical/unMedicalReason/{waitMedicalId}")
    public ModelAndView unMedicalReason(@PathVariable Long waitMedicalId) {
        JSONObject data = new JSONObject();
        data.put("waitMedicalId", waitMedicalId);
        return new ModelAndView("medical/reason", data);
    }
}
