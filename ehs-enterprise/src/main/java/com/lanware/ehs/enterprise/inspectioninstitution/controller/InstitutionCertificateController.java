package com.lanware.ehs.enterprise.inspectioninstitution.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.inspectioninstitution.service.InstitutionCertificateService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 其他机构相关证书controller
 */
@Controller
@RequestMapping("/inspectionInstitution/institutionCertificate")
public class InstitutionCertificateController {
    /** 注入institutionCertificateService的bean */
    @Autowired
    private InstitutionCertificateService institutionCertificateService;

    /**
     * @description 查询机构相关证书list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    @RequestMapping("/listInstitutionCertificates")
    @ResponseBody
    public ResultFormat listInstitutionCertificates(Long relationId, String relationModule) {
        JSONObject result = institutionCertificateService.listInstitutionCertificates(relationId, relationModule);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editInstitutionCertificate/save")
    @ResponseBody
    /** @description 保存机构相关证书，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param institutionCertificate 机构相关证书
     * @param enterpriseUser 当前登录企业 enterpriseUser
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile institutionCertificate
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (institutionCertificateService.insertInstitutionCertificate(enterpriseRelationFile
                    , institutionCertificate, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (institutionCertificateService.updateInstitutionCertificate(enterpriseRelationFile
                    , institutionCertificate, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteInstitutionCertificate")
    @ResponseBody
    /**
     * @description 删除机构相关证书
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInstitutionCertificate(Long id, String filePath) {
        if (institutionCertificateService.deleteInstitutionCertificate(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteInstitutionCertificates")
    @ResponseBody
    /**
     * @description 批量删除机构相关证书
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInstitutionCertificates(Long[] ids) {
        if (institutionCertificateService.deleteInstitutionCertificates(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
