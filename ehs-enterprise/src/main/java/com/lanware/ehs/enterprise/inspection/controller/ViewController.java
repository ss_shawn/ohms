package com.lanware.ehs.enterprise.inspection.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.inspection.service.InspectionService;
import com.lanware.ehs.enterprise.inspection.service.RectificationService;
import com.lanware.ehs.pojo.EnterpriseInspection;
import com.lanware.ehs.pojo.EnterpriseInspectionRectification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 日常检查整改登记视图
 */
@Controller("inspectionView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入inspectionService的bean */
    @Autowired
    private InspectionService inspectionService;
    /** 注入rectificationService的bean */
    @Autowired
    private RectificationService rectificationService;

    @RequestMapping("inspection/editInspection")
    /**
     * @description 新增日常检查整改信息
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addInspection() {
        JSONObject data = new JSONObject();
        EnterpriseInspection enterpriseInspection = new EnterpriseInspection();
        data.put("enterpriseInspection", enterpriseInspection);
        return new ModelAndView("inspection/edit", data);
    }

    /**
     * @description 编辑日常检查整改信息
     * @param id 日常检查整改信息id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("inspection/editInspection/{id}")
    public ModelAndView editMedical(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseInspection enterpriseInspection = inspectionService.getInspectionById(id);
        data.put("enterpriseInspection", enterpriseInspection);
        return new ModelAndView("inspection/edit", data);
    }

    @RequestMapping("inspection/addRegistrationSheet/{id}")
    /**
     * @description 返回新增检查整改登记表页面
     * @param id 检查整改id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addRegistrationSheet(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseInspection enterpriseInspection = inspectionService.getInspectionById(id);
        data.put("enterpriseInspection", enterpriseInspection);
        return new ModelAndView("inspection/addRegistrationSheet", data);
    }

    @RequestMapping("inspection/rectification/{inspectionId}")
    /**
     * @description 返回整改记录页面
     * @param inspectionId 日常检查id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView trainingFile(@PathVariable Long inspectionId){
        JSONObject data = new JSONObject();
        data.put("inspectionId", inspectionId);
        return new ModelAndView("inspection/rectification/list", data);
    }

    @RequestMapping("inspection/rectification/addRectification/{inspectionId}")
    /**
     * @description 新增整改记录
     * @param inspectionId 日常检查id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addInformationFile(@PathVariable Long inspectionId) {
        JSONObject data = new JSONObject();
        EnterpriseInspectionRectification enterpriseInspectionRectification = new EnterpriseInspectionRectification();
        enterpriseInspectionRectification.setInspectionId(inspectionId);
        data.put("enterpriseInspectionRectification", enterpriseInspectionRectification);
        return new ModelAndView("inspection/rectification/edit", data);
    }

    @RequestMapping("inspection/rectification/editRectification/{id}")
    /**
     * @description 编辑整改记录
     * @param id 整改记录id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editInformationFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseInspectionRectification enterpriseInspectionRectification
                = rectificationService.getRectificationById(id);
        data.put("enterpriseInspectionRectification", enterpriseInspectionRectification);
        return new ModelAndView("inspection/rectification/edit", data);
    }
}
