package com.lanware.ehs.enterprise.declare.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.declare.service.DeclareWorkplaceService;
import com.lanware.ehs.mapper.EnterpriseDeclareWorkplaceMapper;
import com.lanware.ehs.mapper.custom.EnterpriseDeclareWorkplaceCustomMapper;
import com.lanware.ehs.pojo.EnterpriseDeclareWorkplace;
import com.lanware.ehs.pojo.EnterpriseDeclareWorkplaceExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 涉及的场所和危害因素interface实现类
 */
public class DeclareWorkplaceServiceImpl implements DeclareWorkplaceService {
    @Autowired
    /** 注入enterpriseDeclareWorkplaceMapper的bean */
    private EnterpriseDeclareWorkplaceMapper enterpriseDeclareWorkplaceMapper;
    @Autowired
    /** 注入enterpriseDeclareWorkplaceCustomMapper的bean */
    private EnterpriseDeclareWorkplaceCustomMapper enterpriseDeclareWorkplaceCustomMapper;

    /**
     * @description 根据培训id查询管理员培训信息list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listDeclareWorkplaces(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseDeclareWorkplace> enterpriseDeclareWorkplaces
                = enterpriseDeclareWorkplaceCustomMapper.listDeclareWorkplaces(condition);
        result.put("declareWorkplaces", enterpriseDeclareWorkplaces);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseDeclareWorkplace> pageInfo = new PageInfo<EnterpriseDeclareWorkplace>(enterpriseDeclareWorkplaces);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据企业id查询所有的作业场所和危害因素集合
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listSelectDeclareWorkplaces(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseDeclareWorkplace> enterpriseDeclareWorkplaces
                = enterpriseDeclareWorkplaceCustomMapper.listSelectDeclareWorkplaces(condition);
        result.put("selectDeclareWorkplaces", enterpriseDeclareWorkplaces);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseDeclareWorkplace> pageInfo = new PageInfo<EnterpriseDeclareWorkplace>(enterpriseDeclareWorkplaces);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据企业id查询未申报的场所危害集合
     * @param enterpriseId 企业id
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseDeclareWorkplace> 危害场所和危害因素集合
     */
    @Override
    public List<EnterpriseDeclareWorkplace> listUnselectedDeclareWorkplaces(Long enterpriseId) {
        return enterpriseDeclareWorkplaceCustomMapper.listUnselectedDeclareWorkplaces(enterpriseId);
    }

    @Override
    /**
     * @description 批量插入场所危害集合
     * @param enterpriseDeclareWorkplaces 场所危害集合
     * @return int 插入成功的数目
     */
    public int insertBatch(List<EnterpriseDeclareWorkplace> enterpriseDeclareWorkplaces) {
        return enterpriseDeclareWorkplaceCustomMapper.insertBatch(enterpriseDeclareWorkplaces);
    }

    @Override
    /**
     * @description 删除危害场所和危害因素
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    public int deleteDeclareWorkplace(Long id) {
        return enterpriseDeclareWorkplaceMapper.deleteByPrimaryKey(id);
    }

    @Override
    /**
     * @description 批量删除危害场所和危害因素
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    public int deleteDeclareWorkplaces(Long[] ids) {
        EnterpriseDeclareWorkplaceExample enterpriseDeclareWorkplaceExample = new EnterpriseDeclareWorkplaceExample();
        EnterpriseDeclareWorkplaceExample.Criteria criteria = enterpriseDeclareWorkplaceExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseDeclareWorkplaceMapper.deleteByExample(enterpriseDeclareWorkplaceExample);
    }
}
