package com.lanware.ehs.enterprise.workplace.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseWorkplace;

import java.util.Map;

/**
 * @description 作业场所管理interface
 */
public interface WorkplaceService {
    /**
     * @description 根据企业id查询作业场所list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces和totalNum的result
     */
    JSONObject listWorkplacesByEnterpriseId(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 查询有职业病危害因素的作业场所list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces和totalNum的result
     */
    JSONObject listHarmWorkplaces(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询作业场所信息
     * @param id 作业场所id
     * @return com.lanware.management.pojo.EnterpriseWorkplace 返回EnterpriseWorkplace对象
     */
    EnterpriseWorkplace getWorkplaceById(Long id);

    /**
     * @description 新增工作场所
     * @param enterpriseWorkplace 工作场所pojo
     * @return int 插入成功的数目
     */
    int insertWorkplace(EnterpriseWorkplace enterpriseWorkplace);

    /**
     * @description 更新工作场所
     * @param enterpriseWorkplace 工作场所pojo
     * @return int 更新成功的数目
     */
    int updateWorkplace(EnterpriseWorkplace enterpriseWorkplace);

    /**
     * @description 删除作业场所信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteWorkplace(Long id);

    /**
     * @description 批量删除作业场所信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteWorkplaces(Long[] ids);
}
