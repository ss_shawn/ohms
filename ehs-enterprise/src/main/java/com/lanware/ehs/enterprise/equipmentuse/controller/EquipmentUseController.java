package com.lanware.ehs.enterprise.equipmentuse.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.equipmentuse.service.EquipmentUseService;
import com.lanware.ehs.pojo.EnterpriseEquipmentUse;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.EquipmentServiceUse;
import com.lanware.ehs.pojo.custom.EquipmentServiceUseCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 运行使用controller
 */
@Controller
@RequestMapping("/equipmentUse")
public class EquipmentUseController {
    /** 注入equipmentUseService的bean */
    @Autowired
    private EquipmentUseService equipmentUseService;

    @RequestMapping("")
    /**
     * @description 返回运行使用页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "equipmentUse/list";
    }

    @RequestMapping("/listEquipmentUses")
    @ResponseBody
    /**
     * @description 获取运行使用result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listEquipmentUses(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("type", EquipmentServiceUseCustom.RECORD_RUN);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = equipmentUseService.listEquipmentUses(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editEquipmentUse/save")
    @ResponseBody
    /**
     * @description 保存运行使用
     * @param enterpriseEquipmentUseJSON 运行使用json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseEquipmentUseJSON, @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseEquipmentUse enterpriseEquipmentUse
                = JSONObject.parseObject(enterpriseEquipmentUseJSON, EnterpriseEquipmentUse.class);
        if (enterpriseEquipmentUse.getId() == null) {
            enterpriseEquipmentUse.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseEquipmentUse.setGmtCreate(new Date());
            if (equipmentUseService.insertEquipmentUse(enterpriseEquipmentUse) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseEquipmentUse.setGmtModified(new Date());
            if (equipmentUseService.updateEquipmentUse(enterpriseEquipmentUse) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteEquipmentUse")
    @ResponseBody
    /**
     * @description 删除运行使用
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteEquipmentUse(Long id) {
        if (equipmentUseService.deleteEquipmentUse(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteEquipmentUses")
    @ResponseBody
    /**
     * @description 批量删除运行使用
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteEquipmentUses(Long[] ids) {
        if (equipmentUseService.deleteEquipmentUses(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/listRunEquipmentsByRecordId")
    @ResponseBody
    /**
     * @description 根据记录id获取运行设备
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param recordId 记录id
     * @param type 记录类型(运行记录)
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listRunEquipmentsByRecordId(Integer pageNum, Integer pageSize, String keyword
            , Long recordId, Integer type, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("recordId", recordId);
        condition.put("type", type);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = equipmentUseService.listRunEquipmentsByRecordId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listSelectRunEquipmentsByRecordId")
    @ResponseBody
    /**
     * @description 根据记录id过滤尚未运行的设备
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param recordId 记录id
     * @param type 记录类型(运行记录)
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listSelectRunEquipmentsByRecordId(Integer pageNum, Integer pageSize, String keyword
            , Long recordId, Integer type, @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("recordId", recordId);
        condition.put("type", type);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = equipmentUseService.listSelectRunEquipmentsByRecordId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/selectRunEquipment/save")
    @ResponseBody
    /** @description 保存运行的设备信息
     * @param runEquipmentListJSON 运行的设备json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String runEquipmentListJSON) {
        List<EquipmentServiceUse> equipmentServiceUseList
                = JSONArray.parseArray(runEquipmentListJSON, EquipmentServiceUse.class);
        if (equipmentServiceUseList == null || equipmentServiceUseList.isEmpty()){
            return ResultFormat.error("选择运行设备失败");
        }
        for (EquipmentServiceUse equipmentServiceUse : equipmentServiceUseList){
            equipmentServiceUse.setGmtCreate(new Date());
        }
        if (equipmentUseService.insertBatch(equipmentServiceUseList) > 0) {
            return ResultFormat.success("选择运行设备成功");
        } else {
            return ResultFormat.error("选择运行设备失败");
        }
    }

    @RequestMapping("/deleteRunEquipment")
    @ResponseBody
    /**
     * @description 删除运行的设备
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteRunEquipment(Long id) {
        if (equipmentUseService.deleteRunEquipment(id) > 0) {
            return ResultFormat.success("移除成功");
        }else{
            return ResultFormat.error("移除失败");
        }
    }

    @RequestMapping("/deleteRunEquipments")
    @ResponseBody
    /**
     * @description 批量删除运行的设备
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteRunEquipments(Long[] ids) {
        if (equipmentUseService.deleteRunEquipments(ids) == ids.length) {
            return ResultFormat.success("移除成功");
        }else{
            return ResultFormat.error("移除失败");
        }
    }
}
