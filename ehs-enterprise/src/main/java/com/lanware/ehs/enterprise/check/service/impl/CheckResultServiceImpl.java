package com.lanware.ehs.enterprise.check.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.check.service.CheckResultService;
import com.lanware.ehs.mapper.custom.EnterpriseCheckResultDetailCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseOutsideCheckResultCustomMapper;
import com.lanware.ehs.pojo.EnterpriseCheckResultDetail;
import com.lanware.ehs.pojo.EnterpriseCheckResultDetailExample;
import com.lanware.ehs.pojo.EnterpriseOutsideCheckResult;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 检测结果interface实现类
 */
public class CheckResultServiceImpl implements CheckResultService {
    /** 注入enterpriseOutsideCheckResultCustomMapper的bean */
    @Autowired
    private EnterpriseOutsideCheckResultCustomMapper enterpriseOutsideCheckResultCustomMapper;
    /** 注入enterpriseCheckResultDetailCustomMapper的bean */
    @Autowired
    private EnterpriseCheckResultDetailCustomMapper enterpriseCheckResultDetailCustomMapper;

    /**
     * @description 获取检测结果result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param condition 查询条件(外部检测id和危害因素id)
     * @return com.lanware.management.common.format.ResultFormat
     */
    @Override
    public JSONObject listCheckResults(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResults
                = enterpriseOutsideCheckResultCustomMapper.listOutsideCheckResultCustomsByOutsideCheckId(condition);
        result.put("enterpriseOutsideCheckResults", enterpriseOutsideCheckResults);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseOutsideCheckResultCustom> pageInfo = new PageInfo<EnterpriseOutsideCheckResultCustom>(enterpriseOutsideCheckResults);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 获取检测结果包含的危害因素id
     * @param outsideCheckId 外部检测id
     * @return 检测结果list(只含有不重复的危害因素id)
     */
    @Override
    public List<EnterpriseOutsideCheckResultCustom> getHarmFactorIdsByOutsideCheckId(Long outsideCheckId) {
        return enterpriseOutsideCheckResultCustomMapper.getHarmFactorIdsByOutsideCheckId(outsideCheckId);
    }

    /**
     * @description 获取检测结果详细result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param condition 查询条件(外部检测id和危害因素id)
     * @return com.lanware.management.common.format.ResultFormat
     */
    @Override
    public JSONObject listCheckResultDetails(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseCheckResultDetail> enterpriseCheckResultDetails
                = enterpriseCheckResultDetailCustomMapper.listCheckResultDetails(condition);
        result.put("enterpriseCheckResultDetails", enterpriseCheckResultDetails);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseCheckResultDetail> pageInfo = new PageInfo<EnterpriseCheckResultDetail>(enterpriseCheckResultDetails);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询检测结果
     * @param id 检测结果id
     * @return com.lanware.ehs.pojo.EnterpriseOutsideCheckResult
     */
    @Override
    public EnterpriseOutsideCheckResult getCheckResultById(Long id) {
        return enterpriseOutsideCheckResultCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 更新检测结果
     * @param enterpriseOutsideCheckResult 检测结果pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateCheckResult(EnterpriseOutsideCheckResult enterpriseOutsideCheckResult) {
        return enterpriseOutsideCheckResultCustomMapper.updateByPrimaryKeySelective(enterpriseOutsideCheckResult);
    }

    /**
     * @description 取消检测结果
     * @param condition 条件
     * @return int 更新成功的数目
     */
    @Override
    public int cancelCheckResult(Map<String, Object> condition) {
        return enterpriseOutsideCheckResultCustomMapper.updateCheckResultByCondition(condition);
    }

    /**
     * @description 新增检测结果详细
     * @param enterpriseCheckResultDetail 检测结果详细pojo
     * @return int 插入成功的数目
     */
    @Override
    public int insertDetail(EnterpriseCheckResultDetail enterpriseCheckResultDetail) {
        return enterpriseCheckResultDetailCustomMapper.insert(enterpriseCheckResultDetail);
    }

    /**
     * @description 更新检测结果详细
     * @param enterpriseCheckResultDetail 检测结果详细pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateDetail(EnterpriseCheckResultDetail enterpriseCheckResultDetail) {
        return enterpriseCheckResultDetailCustomMapper.updateByPrimaryKeySelective(enterpriseCheckResultDetail);
    }

    /**
     * @description 根据id查询检测结果详细
     * @param id 检测结果详细id
     * @return com.lanware.ehs.pojo.EnterpriseCheckResultDetail
     */
    @Override
    public EnterpriseCheckResultDetail getCheckResultDetailById(Long id) {
        return enterpriseCheckResultDetailCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 删除危害因素检测结果详细
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    public int deleteCheckResultDetail(Long id) {
        return enterpriseCheckResultDetailCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除危害因素检测结果详细
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteCheckResultDetails(Long[] ids) {
        EnterpriseCheckResultDetailExample enterpriseCheckResultDetailExample = new EnterpriseCheckResultDetailExample();
        enterpriseCheckResultDetailExample.createCriteria().andIdIn(Arrays.asList(ids));
        return enterpriseCheckResultDetailCustomMapper.deleteByExample(enterpriseCheckResultDetailExample);
    }

    /**
     * @description 根据外部检测id查询检测结果扩展
     * @param outsideCheckId 外部检测id
     * @return java.util.List<com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom>
     */
    @Override
    public List<EnterpriseOutsideCheckResultCustom> listHarmFactorsByOutsideCheckId(Long outsideCheckId) {
        return enterpriseOutsideCheckResultCustomMapper.listHarmFactorsByOutsideCheckId(outsideCheckId);
    }

    /**
     * @description 根据条件查询检测结果扩展
     * @param condition 查询条件
     * @return java.util.List<com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom>
     */
    @Override
    public List<EnterpriseOutsideCheckResultCustom> listCheckResultCustoms(Map<String, Object> condition) {
        return enterpriseOutsideCheckResultCustomMapper.listCheckResultCustoms(condition);
    }

    /**
     * @description 批量插入检测结果详细
     * @param checkResultDetailList 检测结果详细list
     * @return int 插入成功的数目
     */
    @Override
    public int insertCheckResultDetails(List<EnterpriseCheckResultDetail> checkResultDetailList) {
        return enterpriseCheckResultDetailCustomMapper.insertBatch(checkResultDetailList);
    }
}
