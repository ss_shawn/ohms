package com.lanware.ehs.enterprise.resource.entity;

import com.lanware.ehs.pojo.EnterpriseResource;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 菜单树
 * @param <T>
 */
@Data
public class MenuTree<T> implements Serializable {
    private static final long serialVersionUID = 3686182459184345807L;
    private String id;
    private String icon;
    private String href;
    private String title;
    private Map<String, Object> state;
    private boolean checked = false;
    private Map<String, Object> attributes;
    private List<MenuTree<T>> childs = new ArrayList<>();
    private String parentId;
    private boolean hasParent = false;
    private boolean hasChild = false;

    private EnterpriseResource data;

}