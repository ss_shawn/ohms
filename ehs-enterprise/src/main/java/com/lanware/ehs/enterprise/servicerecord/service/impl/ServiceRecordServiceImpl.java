package com.lanware.ehs.enterprise.servicerecord.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.servicerecord.service.ServiceRecordService;
import com.lanware.ehs.mapper.custom.EnterpriseServiceRecordCustomMapper;
import com.lanware.ehs.mapper.custom.EquipmentServiceUseCustomMapper;
import com.lanware.ehs.pojo.EnterpriseServiceRecord;
import com.lanware.ehs.pojo.EnterpriseServiceRecordExample;
import com.lanware.ehs.pojo.EquipmentServiceUse;
import com.lanware.ehs.pojo.EquipmentServiceUseExample;
import com.lanware.ehs.pojo.custom.EnterpriseServiceRecordCustom;
import com.lanware.ehs.pojo.custom.EquipmentServiceUseCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 维护记录interface实现类
 */
@Service
public class ServiceRecordServiceImpl implements ServiceRecordService {
    /** 注入enterpriseServiceRecordsCustomMapper的bean */
    @Autowired
    private EnterpriseServiceRecordCustomMapper enterpriseServiceRecordCustomMapper;
    /** 注入equipmentServiceUseCustomMapper的bean */
    @Autowired
    private EquipmentServiceUseCustomMapper equipmentServiceUseCustomMapper;

    /**
     * @description 查询维护记录list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseServiceRecords和totalNum的result
     */
    @Override
    public JSONObject listServiceRecords(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseServiceRecordCustom> enterpriseServiceRecords
                = enterpriseServiceRecordCustomMapper.listServiceRecords(condition);
        result.put("enterpriseServiceRecords", enterpriseServiceRecords);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseServiceRecordCustom> pageInfo
                    = new PageInfo<EnterpriseServiceRecordCustom>(enterpriseServiceRecords);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询维护记录
     * @param id 维护记录id
     * @return com.lanware.management.pojo.EnterpriseServiceRecords 返回EnterpriseServiceRecords对象
     */
    @Override
    public EnterpriseServiceRecord getServiceRecordById(Long id) {
        return enterpriseServiceRecordCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增维护记录
     * @param enterpriseServiceRecord 维护记录pojo
     * @return int 插入成功的数目
     */
    @Override
    public int insertServiceRecord(EnterpriseServiceRecord enterpriseServiceRecord) {
        return enterpriseServiceRecordCustomMapper.insert(enterpriseServiceRecord);
    }

    /**
     * @description 更新维护记录
     * @param enterpriseServiceRecord 维护记录pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateServiceRecord(EnterpriseServiceRecord enterpriseServiceRecord) {
        return enterpriseServiceRecordCustomMapper.updateByPrimaryKeySelective(enterpriseServiceRecord);
    }

    /**
     * @description 删除维护记录
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteServiceRecord(Long id) {
        // 删除维护记录的维护设备
        EquipmentServiceUseExample equipmentServiceUseExample = new EquipmentServiceUseExample();
        EquipmentServiceUseExample.Criteria criteria = equipmentServiceUseExample.createCriteria();
        criteria.andRecordIdEqualTo(id);
        equipmentServiceUseCustomMapper.deleteByExample(equipmentServiceUseExample);
        // 删除维护记录
        return enterpriseServiceRecordCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除维护记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteServiceRecords(Long[] ids) {
        // 删除维护记录的维护设备
        EquipmentServiceUseExample equipmentServiceUseExample = new EquipmentServiceUseExample();
        EquipmentServiceUseExample.Criteria criteria = equipmentServiceUseExample.createCriteria();
        criteria.andRecordIdIn(Arrays.asList(ids));
        equipmentServiceUseCustomMapper.deleteByExample(equipmentServiceUseExample);
        // 删除维护记录
        EnterpriseServiceRecordExample enterpriseServiceRecordExample = new EnterpriseServiceRecordExample();
        EnterpriseServiceRecordExample.Criteria criteria1 = enterpriseServiceRecordExample.createCriteria();
        criteria1.andIdIn(Arrays.asList(ids));
        return enterpriseServiceRecordCustomMapper.deleteByExample(enterpriseServiceRecordExample);
    }

    /**
     * @description 根据记录id获取维护设备
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listServiceEquipmentsByRecordId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EquipmentServiceUseCustom> serviceEquipments
                = equipmentServiceUseCustomMapper.listEquipmentsByRecordId(condition);
        result.put("serviceEquipments", serviceEquipments);
        if (pageNum != null && pageSize != null) {
            PageInfo<EquipmentServiceUseCustom> pageInfo = new PageInfo<EquipmentServiceUseCustom>(serviceEquipments);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据记录id过滤尚未维护的设备
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listSelectServiceEquipmentsByRecordId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EquipmentServiceUseCustom> selectServiceEquipments
                = equipmentServiceUseCustomMapper.listSelectEquipmentsByRecordId(condition);
        result.put("selectServiceEquipments", selectServiceEquipments);
        if (pageNum != null && pageSize != null) {
            PageInfo<EquipmentServiceUseCustom> pageInfo = new PageInfo<EquipmentServiceUseCustom>(selectServiceEquipments);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 批量插入维护的设备集合
     * @param equipmentServiceUseList 入维护的设备集合
     * @return int 插入成功的数目
     */
    @Override
    public int insertBatch(List<EquipmentServiceUse> equipmentServiceUseList) {
        return equipmentServiceUseCustomMapper.insertBatch(equipmentServiceUseList);
    }

    /**
     * @description 删除维护的设备
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    public int deleteServiceEquipment(Long id) {
        return equipmentServiceUseCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除维护的设备
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteServiceEquipments(Long[] ids) {
        EquipmentServiceUseExample equipmentServiceUseExample = new EquipmentServiceUseExample();
        EquipmentServiceUseExample.Criteria criteria = equipmentServiceUseExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return equipmentServiceUseCustomMapper.deleteByExample(equipmentServiceUseExample);
    }

    /**
     * @description 查询维护记录台账信息
     * @param enterpriseId 企业id
     * @return java.util.List<com.lanware.ehs.pojo.custom.EnterpriseServiceRecordCustom>
     */
    @Override
    public List<EnterpriseServiceRecordCustom> listServiceRecordAccounts(Map<String, Object> condition) {
        return enterpriseServiceRecordCustomMapper.listServiceRecordAccounts(condition);
    }
}
