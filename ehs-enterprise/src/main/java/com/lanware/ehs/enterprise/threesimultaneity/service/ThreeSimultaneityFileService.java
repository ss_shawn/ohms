package com.lanware.ehs.enterprise.threesimultaneity.service;

import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 三同时文件interface
 */
public interface ThreeSimultaneityFileService {

    int insertOpinionFile(EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile
            , MultipartFile opinionFile, Long enterpriseId);

    EnterpriseThreeSimultaneityFile getThreeSimultaneityFileById(Long id);
}
