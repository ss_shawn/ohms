package com.lanware.ehs.enterprise.accidenthandle.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.accidenthandle.service.AccidentFileService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 事故调查处理相关文件controller
 */
@Controller
@RequestMapping("/accidentHandle/accidentFile")
public class AccidentFileController {
    /** 注入accidentFileService的bean */
    @Autowired
    private AccidentFileService accidentFileService;

    /**
     * @description 查询事故调查处理相关文件list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    @RequestMapping("/listAccidentFiles")
    @ResponseBody
    public ResultFormat listAccidentFiles(Long relationId, String relationModule) {
        JSONObject result = accidentFileService.listAccidentFiles(relationId, relationModule);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editAccidentFile/save")
    @ResponseBody
    /** @description 保存事故调查处理相关文件，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param accidentFile 事故调查处理相关文件
     * @param enterpriseUser 当前登录企业 enterpriseUser
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile accidentFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (accidentFileService.insertAccidentFile(enterpriseRelationFile, accidentFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (accidentFileService.updateAccidentFile(enterpriseRelationFile, accidentFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteAccidentFile")
    @ResponseBody
    /**
     * @description 删除事故调查处理相关文件
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteAccidentFile(Long id, String filePath) {
        if (accidentFileService.deleteAccidentFile(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteAccidentFiles")
    @ResponseBody
    /**
     * @description 批量删除事故调查处理相关文件
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteAccidentFiles(Long[] ids) {
        if (accidentFileService.deleteAccidentFiles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
