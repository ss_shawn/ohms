package com.lanware.ehs.enterprise.employee.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.enterprise.employee.service.HarmContactHistoryService;
import com.lanware.ehs.mapper.custom.EmployeeHarmContactHistoryCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseProtectiveMeasuresCustomMapper;
import com.lanware.ehs.pojo.custom.EmployeeHarmContactHistoryCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectiveMeasuresCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class HarmContactHistoryServiceImpl implements HarmContactHistoryService {

    private EmployeeHarmContactHistoryCustomMapper employeeHarmContactHistoryCustomMapper;

    private EnterpriseProtectiveMeasuresCustomMapper enterpriseProtectiveMeasuresCustomMapper;

    @Autowired
    public HarmContactHistoryServiceImpl(EmployeeHarmContactHistoryCustomMapper employeeHarmContactHistoryCustomMapper, EnterpriseProtectiveMeasuresCustomMapper enterpriseProtectiveMeasuresCustomMapper) {
        this.employeeHarmContactHistoryCustomMapper = employeeHarmContactHistoryCustomMapper;
        this.enterpriseProtectiveMeasuresCustomMapper = enterpriseProtectiveMeasuresCustomMapper;
    }

    @Override
    public Page<EmployeeHarmContactHistoryCustom> queryEmployeeHarmContactHistoryByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<EmployeeHarmContactHistoryCustom> page = PageHelper.startPage(pageNum, pageSize);
        employeeHarmContactHistoryCustomMapper.queryEmployeeHarmContactHistoryByCondition(condition);
        return page;
    }

    @Override
    public List<EnterpriseProtectiveMeasuresCustom> queryProtectEnterpriseProtectiveMeasuresByPostId(Long postId) {
        return enterpriseProtectiveMeasuresCustomMapper.queryProtectEnterpriseProtectiveMeasuresByPostId(postId);
    }

    @Override
    public EmployeeHarmContactHistoryCustom queryEmployeeHarmContactHistoryById(Long id) {
        return employeeHarmContactHistoryCustomMapper.queryEmployeeHarmContactHistoryById(id);
    }
}
