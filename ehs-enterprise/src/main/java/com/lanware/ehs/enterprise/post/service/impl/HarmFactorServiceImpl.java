package com.lanware.ehs.enterprise.post.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.post.service.HarmFactorService;
import com.lanware.ehs.mapper.custom.PostHarmFactorCustomMapper;
import com.lanware.ehs.pojo.custom.PostHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class HarmFactorServiceImpl implements HarmFactorService {

    private PostHarmFactorCustomMapper postHarmFactorCustomMapper;

    @Autowired
    public HarmFactorServiceImpl(PostHarmFactorCustomMapper postHarmFactorCustomMapper) {
        this.postHarmFactorCustomMapper = postHarmFactorCustomMapper;
    }

    @Override
    public Page<PostHarmFactorCustom> queryPostHarmFactorByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<PostHarmFactorCustom> page = PageHelper.startPage(pageNum, pageSize);
        postHarmFactorCustomMapper.queryPostHarmFactorByCondition(condition);
        return page;
    }

    @Override
    public Page<PostHarmFactorCustom> queryUnSelectedPostHarmFactorByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<PostHarmFactorCustom> page = PageHelper.startPage(pageNum, pageSize);
        postHarmFactorCustomMapper.queryUnSelectedPostHarmFactorByCondition(condition);
        return page;
    }

    @Override
    public JSONObject listTouchHarmNum(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<PostHarmFactorCustom> postHarmFactorCustoms = postHarmFactorCustomMapper.listTouchHarmNum(condition);
        // 查询数据和分页数据装进result
        result.put("postHarmFactorCustoms", postHarmFactorCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<PostHarmFactorCustom> pageInfo = new PageInfo<PostHarmFactorCustom>(postHarmFactorCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }
}
