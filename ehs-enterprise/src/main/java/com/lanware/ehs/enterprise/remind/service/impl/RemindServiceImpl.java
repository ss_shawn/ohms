package com.lanware.ehs.enterprise.remind.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.enterprise.remind.service.RemindService;
import com.lanware.ehs.mapper.EnterpriseRemindMapper;
import com.lanware.ehs.pojo.EnterpriseRemind;
import com.lanware.ehs.pojo.EnterpriseRemindExample;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class RemindServiceImpl implements RemindService {
    @Autowired
    private EnterpriseRemindMapper remindMapper;
    @Override
    public Page<EnterpriseRemind> findRemeind(EnterpriseRemind remind, QueryRequest request) {
        Page<EnterpriseRemind> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        EnterpriseRemindExample example = new EnterpriseRemindExample();
        EnterpriseRemindExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo(remind.getStatus());
        criteria.andEnterpriseIdEqualTo(remind.getEnterpriseId());
        remindMapper.selectByExample(example);
        return page;
    }

    @Override
    public List<EnterpriseRemind> findRemeind(EnterpriseRemind remind) {
        EnterpriseRemindExample example = new EnterpriseRemindExample();
        EnterpriseRemindExample.Criteria criteria = example.createCriteria();
        if(remind.getEnterpriseId() != null){
            criteria.andEnterpriseIdEqualTo(remind.getEnterpriseId());
        }
        if(StringUtils.isNotEmpty(remind.getPlanDate())){
            criteria.andPlanDateEqualTo(remind.getPlanDate());
        }

        if(StringUtils.isNotEmpty(remind.getType())){
            criteria.andTypeEqualTo(remind.getType());
        }

        return remindMapper.selectByExample(example);
    }

    @Override
    public void updateById(EnterpriseRemind remind) {
        EnterpriseRemindExample example = new EnterpriseRemindExample();
        EnterpriseRemindExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(remind.getId());
        remindMapper.updateByExampleSelective(remind,example);
    }

    @Override
    public List<EnterpriseRemind> findRemindByRemindDate(String type,String planDate,Long enterpriseId,Date startDate, Date endDate){
        EnterpriseRemindExample example = new EnterpriseRemindExample();
        EnterpriseRemindExample.Criteria criteria = example.createCriteria();
        criteria.andPlanDateEqualTo(planDate);
        criteria.andTypeEqualTo(type);
        criteria.andGmtCreateBetween(startDate,endDate);
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        return remindMapper.selectByExample(example);
    }

    @Override
    public void insert(EnterpriseRemind remind) {
        remindMapper.insertSelective(remind);
    }
}
