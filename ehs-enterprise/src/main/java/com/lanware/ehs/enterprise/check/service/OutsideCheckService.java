package com.lanware.ehs.enterprise.check.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseOutsideCheck;
import com.lanware.ehs.pojo.EnterpriseWaitCheck;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @description 企业外部检测interface
 */
public interface OutsideCheckService {
    /**
     * @description 查询危害因素检测list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseOutsideChecks和totalNum的result
     */
    JSONObject listOutsideChecks(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询危害因素检测信息
     * @param id 危害因素检测id
     * @return com.lanware.management.pojo.EnterpriseOutsideCheck 返回EnterpriseOutsideCheck对象
     */
    EnterpriseOutsideCheck getOutsideCheckById(Long id);

    /**
     * @description 根据id查询危害因素检测扩展信息
     * @param id 危害因素检测id
     * @return com.lanware.management.pojo.EnterpriseOutsideCheck 返回EnterpriseOutsideCheck对象
     */
    EnterpriseOutsideCheckCustom getOutsideCheckCustomById(Long id);

    /**
     * @description 新增危害因素检测信息
     * @param enterpriseOutsideCheck 危害因素检测pojo
     * @param checkReportFile 检测报告书
     * @param enterpriseWaitCheck 待检测项目pojo
     * @return int 插入成功的数目
     */
    int insertOutsideCheck(EnterpriseOutsideCheck enterpriseOutsideCheck, MultipartFile checkReportFile
            , EnterpriseWaitCheck enterpriseWaitCheck);

    /**
     * @description 更新危害因素检测信息
     * @param enterpriseOutsideCheck 危害因素检测pojo
     * @param checkReportFile 检测报告书
     * @return int 更新成功的数目
     */
    int updateOutsideCheck(EnterpriseOutsideCheck enterpriseOutsideCheck, MultipartFile checkReportFile);

    /**
     * @description 删除危害因素检测信息
     * @param id 要删除的id
     * @param checkReport 要删除的检测报告书路径
     * @return int 删除成功的数目
     */
    void deleteOutsideCheck(Long id, String checkReport);

    /**
     * @description 批量删除危害因素检测信息
     * @param ids 要删除的数组ids
     */
    void deleteOutsideChecks(Long[] ids);

    /**
     * @description 查询危害因素检测扩展list
     * @param condition 查询条件
     * @return List<EnterpriseOutsideCheckCustom>
     */
    List<EnterpriseOutsideCheckCustom> listOutsideCheckCustoms(Map<String, Object> condition);
}
