package com.lanware.ehs.enterprise.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.common.utils.SortUtil;
import com.lanware.ehs.enterprise.user.service.AuthService;
import com.lanware.ehs.enterprise.user.service.MailService;
import com.lanware.ehs.enterprise.user.service.UserService;
import com.lanware.ehs.mapper.EnterpriseAuthMapper;
import com.lanware.ehs.mapper.custom.EnterpriseBaseinfoCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseUserCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.shiro.authentication.ShiroRealm;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 *
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {
    @Autowired
    private EnterpriseUserCustomMapper mapper;

    @Autowired
    private EnterpriseAuthMapper authMapper;

    @Autowired
    private AuthService authService;

    @Autowired
    private EnterpriseBaseinfoCustomMapper baseinfoCustomMapper;

    @Autowired
    private ShiroRealm shiroRealm;

    /** 发送邮件的主题 */
    @Value("${spring.mail.subject}")
    private String subject;

    @Autowired
    private MailService mailService;

    @Override
    public EnterpriseUserCustom findByName(String username) {
        return this.mapper.findByName(username);
    }


    @Override
    public Page<EnterpriseUserCustom> findUserDetail(EnterpriseUserCustom user, QueryRequest request) {
        Page<EnterpriseUserCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());

        }
        return this.mapper.findUserDetailPage(user);
    }

    @Override
    public EnterpriseUserCustom findUserDetail(String username) {
        EnterpriseUserCustom param = new EnterpriseUserCustom();
        param.setUsername(username);
        List<EnterpriseUserCustom> users = this.mapper.findUserDetail(param);
        return CollectionUtils.isNotEmpty(users) ? users.get(0) : null;
    }

    @Override
    @Transactional
    public void createUser(EnterpriseUserCustom userCustom) throws MessagingException {
        EnterpriseUser user = new EnterpriseUser();
        BeanUtils.copyProperties(userCustom,user);
        user.setGmtCreate(new Date());
        user.setIsAdmin(EnterpriseUserCustom.ADMIN_NO);
        user.setIsDeleted(EnterpriseUserCustom.DELETED_NO);
        user.setTheme(EnterpriseUserCustom.THEME_BLACK);
        user.setIsTab(EnterpriseUserCustom.TAB_OPEN);
        user.setPassword(MD5Util.encrypt(userCustom.getUsername(), EnterpriseUserCustom.DEFAULT_PASSWORD));
        mapper.insertSelective(user);
        // 保存用户角色
        String[] roles = userCustom.getRoleId().split(StringPool.COMMA);
        setUserRoles(user, roles);

        //发送邮件
        sendMail(user);
    }

    /**
     * 发送邮件
     */
    private void sendMail(EnterpriseUser user) throws MessagingException {
        String username = user.getUsername();
        // 账号注册成功的邮件发送html模板
        String content = "<div style='background-color:#ECECEC; padding: 35px;'>" +
                "<table cellpadding='0' align='center' " +
                "style='width: 600px; margin: 0px auto; text-align: left; position: relative; " +
                "border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; " +
                "border-bottom-left-radius: 5px; font-size: 14px; font-family:微软雅黑, 黑体; line-height: 1.5; " +
                "box-shadow: rgb(153, 153, 153) 0px 0px 5px; border-collapse: collapse; background-position: " +
                "initial initial; background-repeat: initial initial;background:#fff;'>" +
                "<tbody><tr>" +
                "<th valign='middle' style='height: 25px; line-height: 25px; padding: 15px 35px; " +
                "border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #42a3d3; " +
                "background-color: #49bcff; border-top-left-radius: 5px; border-top-right-radius: 5px; " +
                "border-bottom-right-radius: 0px; border-bottom-left-radius: 0px;'>" +
                "<font face='微软雅黑' size='5' style='color: rgb(255, 255, 255); '>注册成功！（职业健康智慧管理系统）</font>" +
                "</th></tr><tr><td>" +
                "<div style='padding:25px 35px 40px; background-color:#fff;'>" +
                "<h2 style='margin: 5px 0px; '><font color='#333333' style='line-height: 20px; '>" +
                "<font style='line-height: 22px; ' size='4'>亲爱的" + username + "</font></font></h2>" +
                "<p>感谢您加入职业健康智慧管理系统！下面是您的账号信息</p>" +
                "您的账号：<b>" + username + "</b><br>" +
                "您的密码：<b>"+EnterpriseUserCustom.DEFAULT_PASSWORD+"</b><br>" +
                "您注册时的时间：<b>" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "</b><br>" +
                "当您在使用本系统时，遵守当地法律法规。<br>" +
                "<p align='right'>职业健康智慧管理系统</p>" +
                "<p align='right'>" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "</p>" +
                "<div style='width:700px;margin:0 auto;'>" +
                "<div style='padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;" +
                "line-height:1.3em;font-size:12px;'>" +
                "<p>此为系统邮件，请勿回复<br>" +
                "请保管好您的邮箱，避免账号被他人盗用" +
                "</p></div></div></div></td></tr></tbody></table></div>";
        mailService.sendHTMLMail(subject,content,user.getEmail());
    }

    @Override
    @Transactional
    public void resetPassword(String[] usernames) {
        Arrays.stream(usernames).forEach(username -> {
            EnterpriseUser user = new EnterpriseUser();
            user.setPassword(MD5Util.encrypt(username, EnterpriseUserCustom.DEFAULT_PASSWORD));
            user.setGmtModified(new Date());
            EnterpriseUserExample example = new EnterpriseUserExample();
            EnterpriseUserExample.Criteria criteria = example.createCriteria();
            criteria.andUsernameEqualTo(username);
            mapper.updateByExampleSelective(user,example);
        });
    }

    @Override
    public void updateSatus(Integer status, String[] usernames) {
        Arrays.stream(usernames).forEach(username -> {
            EnterpriseUser user = new EnterpriseUser();
            user.setGmtModified(new Date());
            user.setStatus(status);
            EnterpriseUserExample example = new EnterpriseUserExample();
            EnterpriseUserExample.Criteria criteria = example.createCriteria();
            criteria.andUsernameEqualTo(username);
            mapper.updateByExampleSelective(user,example);
        });
    }

    @Override
    @Transactional
    public void updateUser(EnterpriseUserCustom user) {
        // 更新用户
        user.setPassword(null);
        user.setUsername(null);
        user.setGmtModified(new Date());
        mapper.updateByPrimaryKeySelective(user);
        // 更新关联角色  管理端新建的用户为超级管理员不需要角色操作
        EnterpriseAuthExample authExample = new EnterpriseAuthExample();
        EnterpriseAuthExample.Criteria authCriteria = authExample.createCriteria();
        authCriteria.andUserIdEqualTo(user.getId());
        authMapper.deleteByExample(authExample);
        String[] roles = user.getRoleId().split(StringPool.COMMA);
        setUserRoles(user, roles);



        String currentUsername = ((EnterpriseUserCustom)SecurityUtils.getSubject().getPrincipal()).getUsername();
        if (StringUtils.equalsIgnoreCase(currentUsername, user.getUsername())) {
            shiroRealm.clearCache();
        }
    }

    @Override
    @Transactional
    public void updatePassword(String username, String password) {
        EnterpriseUser user = new EnterpriseUser();
        user.setPassword(MD5Util.encrypt(username, password));
        user.setGmtModified(new Date());
        EnterpriseUserExample example = new EnterpriseUserExample();
        EnterpriseUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        mapper.updateByExampleSelective(user,example);
    }

    @Override
    @Transactional
    public void updateLoginTime(String username) {
        //更新企业用户登录时间
        EnterpriseUser user = new EnterpriseUser();
        user.setLastLoginTime(new Date());
        EnterpriseUserExample example = new EnterpriseUserExample();
        EnterpriseUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        mapper.updateByExampleSelective(user,example);

        //查询当前用户
        EnterpriseUserCustom userCustom = findByName(username);

        //更新企业最后一次使用时间
        EnterpriseBaseinfo baseinfo = new EnterpriseBaseinfo();
        baseinfo.setLastUseTime(new Date());
        EnterpriseBaseinfoExample baseinfoExample = new EnterpriseBaseinfoExample();
        EnterpriseBaseinfoExample.Criteria baseInfoCriteria = baseinfoExample.createCriteria();
        baseInfoCriteria.andIdEqualTo(userCustom.getEnterpriseId());
        baseinfoCustomMapper.updateByExampleSelective(baseinfo,baseinfoExample);

    }

    private void setUserRoles(EnterpriseUser user, String[] roles) {
        List<EnterpriseAuth> userRoles = new ArrayList<>();
        Arrays.stream(roles).forEach(roleId -> {
            EnterpriseAuth ur = new EnterpriseAuth();
            ur.setUserId(user.getId());
            ur.setRoleId(Long.valueOf(roleId));
            userRoles.add(ur);
        });
        authService.saveBatch(userRoles);
    }


}
