package com.lanware.ehs.enterprise.post.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.custom.PostProtectArticlesCustom;

import java.util.Map;

public interface ProtectArticlesService {

    /**
     * 查询防护用具
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<PostProtectArticlesCustom> queryPostProtectArticlesByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 查询未选择的防护用具
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<PostProtectArticlesCustom> queryUnSelectedPostProtectArticlesByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);
}
