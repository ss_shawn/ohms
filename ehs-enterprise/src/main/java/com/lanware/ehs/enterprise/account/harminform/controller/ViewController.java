package com.lanware.ehs.enterprise.account.harminform.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 职业危害告知台帐视图
 */
@Controller("harmInformAccountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @RequestMapping("harmInform/identifyFile/{relationId}/{relationModule}")
    /**
     * @description 返回职业病危害警示页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView identifyFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/harmInform/identifyFile", data);
    }

    @RequestMapping("harmInform/informFile/{relationId}/{relationModule}")
    /**
     * @description 返回职业高毒物品告知卡页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView informFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/harmInform/informFile", data);
    }

    @RequestMapping("harmInform/informationFile/{relationId}/{relationModule}")
    /**
     * @description 返回职业危害因素信息卡页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView informationFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/harmInform/informationFile", data);
    }

    @RequestMapping("harmInform/harmFactor/{workplaceId}")
    /**
     * @description 返回职业病危害因素页面
     * @param workplaceId 作业场所id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView harmFactor(@PathVariable Long workplaceId){
        JSONObject data = new JSONObject();
        data.put("workplaceId", workplaceId);
        return new ModelAndView("account/harmInform/harmFactor", data);
    }

    @RequestMapping("harmInform/diseaseDiagnosi/{medicalId}/{employeeId}")
    /**
     * @description 返回职业病诊断页面
     * @param medicalId 体检id
     * @param employeeId 体检员工id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView diseaseDiagnosi(@PathVariable Long medicalId, @PathVariable Long employeeId){
        JSONObject data = new JSONObject();
        data.put("medicalId", medicalId);
        data.put("employeeId", employeeId);
        return new ModelAndView("account/harmInform/diseaseDiagnosi", data);
    }
}
