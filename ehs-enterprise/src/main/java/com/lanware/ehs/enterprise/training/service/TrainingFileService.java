package com.lanware.ehs.enterprise.training.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 培训证明资料interface
 */
public interface TrainingFileService {
    /**
     * @description 查询培训证明资料list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    JSONObject listTrainingFiles(Long relationId, String relationModule);

    /**
     * @description 根据id查询培训证明资料
     * @param id
     * @return 返回EnterpriseRelationFile
     */
    EnterpriseRelationFile getTrainingFileById(Long id);

    /**
     * @description 新增培训证明资料，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param trainingFile 培训证明资料
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertTrainingFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile trainingFile, Long enterpriseId);

    /**
     * @description 更新培训证明资料，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param trainingFile 培训证明资料
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateTrainingFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile trainingFile, Long enterpriseId);

    /**
     * @description 删除培训证明资料
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteTrainingFile(Long id, String filePath);

    /**
     * @description 批量删除培训证明资料
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteTrainingFiles(Long[] ids);
}