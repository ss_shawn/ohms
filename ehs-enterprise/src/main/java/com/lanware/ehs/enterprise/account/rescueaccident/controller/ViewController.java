package com.lanware.ehs.enterprise.account.rescueaccident.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 职业危害应急救援和事故台帐视图
 */
@Controller("rescueAccidentAccountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @RequestMapping("rescueAccident/accidentFileDetail/{relationId}/{relationModule}")
    /**
     * @description 返回机事故调查处理相关文件页面
     * @param relationId 检查机构id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView accidentFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("account/rescueAccident/accidentFileDetail", data);
    }
}
