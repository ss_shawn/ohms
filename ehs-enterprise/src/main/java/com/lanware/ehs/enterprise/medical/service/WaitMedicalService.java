package com.lanware.ehs.enterprise.medical.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EmployeeWaitMedical;
import com.lanware.ehs.pojo.custom.EmployeeWaitMedicalCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 待体检管理interface
 */
public interface WaitMedicalService {
    /**
     * @description 查询待体检人员list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeWaitMedicals和totalNum的result
     */
    JSONObject listWaitMedicals(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询员工姓名
     * @param id
     * @return 员工name
     */
    String getEmployeeNameById(Long id);

    /**
     * 查询待体检数据集合
     * @param condition
     * @return
     */
    List<EmployeeWaitMedicalCustom> queryList(Map<String, Object> condition);

    /**
     * @description 更新待体检记录状态，并保存未体检原因
     * @param condition 查询条件
     * @return 更新成功的数目
     */
    int updateWaitMedicalById(Map<String, Object> condition);

    /**
     * @description 获取待体检年度报表
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listYearWaitMedicals(Map<String, Object> condition);

    void addWaitMedical(EmployeeWaitMedical employeeWaitMedical);
}
