package com.lanware.ehs.enterprise.notify.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.notify.service.EmployeeWorkNotifyService;
import com.lanware.ehs.enterprise.notify.service.EnterpriseWorkplaceNotifyService;
import com.lanware.ehs.pojo.custom.EmployeeWorkNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceNotifyCustom;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("workplaceNotify")
public class EnterpriseWorkplaceNotifyController extends BaseController {
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EnterpriseWorkplaceNotifyService enterpriseWorkplaceNotifyService;
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private EnterpriseBaseinfoService enterpriseBaseinfoService;

    /**
     * 获取待作业场所告知列表
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("waitList")
    public EhsResult userList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //查询未告知数据
        condition.put("status",0);
        //排序条件
        condition.put("orderByClause","gmt_create");
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.enterpriseWorkplaceNotifyService.findEnterpriseWorkplaceWaitNotifyList(condition,request));
        return EhsResult.ok(dataTable);
    }

      /**
     * 保存作业场所告知
     * @return
     */
    @RequestMapping("workplaceAdd")
    @ResponseBody
    public EhsResult addEnterpriseProtectArticles(String enterpriseWorkplaceNotifyCustoms, MultipartFile checkResultProve, MultipartFile harmWarnImage,@CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseWorkplaceNotifyCustom enterpriseWorkplaceNotifyCustom = JSONObject.parseObject(enterpriseWorkplaceNotifyCustoms, EnterpriseWorkplaceNotifyCustom.class);
        enterpriseWorkplaceNotifyCustom.setEnterpriseId(currentUser.getEnterpriseId());
        this.enterpriseWorkplaceNotifyService.saveEnterpriseWorkplaceNotify(enterpriseWorkplaceNotifyCustom,checkResultProve,harmWarnImage);
        return EhsResult.ok("");
    }
    /**
     * 删除作业场所告知
     * @return
     */
    @RequestMapping("deleteWorkplace")
    public EhsResult addEnterpriseProtectArticles(Long id, @CurrentUser EnterpriseUserCustom currentUser) {
        this.enterpriseWorkplaceNotifyService.deleteEnterpriseWorkplaceNotify(id);
        return EhsResult.ok("");
    }
    /**
     * 获取待作业场所告知列表
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("workplaceList")
    public EhsResult workList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //排序条件
        condition.put("orderByClause","gmt_create");
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.enterpriseWorkplaceNotifyService.findEnterpriseWorkplaceNotifyList(condition,request));
        return EhsResult.ok(dataTable);
    }
}
