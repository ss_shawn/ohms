package com.lanware.ehs.enterprise.inspection.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseInspection;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @description 日常检查整改登记interface
 */
public interface InspectionService {
    /**
     * @description 查询日常检查整改登记list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    JSONObject listInspections(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询日常检查整改信息
     * @param id 日常检查整改id
     * @return com.lanware.management.pojo.EnterpriseInspection 返回EnterpriseInspection对象
     */
    EnterpriseInspection getInspectionById(Long id);

    /**
     * @description 新增日常检查整改信息
     * @param enterpriseInspection 日常检查整改pojo
     * @param registrationSheetFile 日常检查整改登记表
     * @return int 插入成功的数目
     */
    int insertInspection(EnterpriseInspection enterpriseInspection, MultipartFile registrationSheetFile);

    /**
     * @description 更新日常检查整改信息
     * @param enterpriseInspection 日常检查整改pojo
     * @param registrationSheetFile 日常检查整改登记表
     * @return int 更新成功的数目
     */
    int updateInspection(EnterpriseInspection enterpriseInspection, MultipartFile registrationSheetFile);

    /**
     * @description 删除员日常检查整改信息
     * @param id 要删除的id
     * @param registrationSheet 要删除的检查整改登记表路径
     * @return int 删除成功的数目
     */
    int deleteInspection(Long id, String registrationSheet);

    /**
     * @description 批量删除日常检查整改信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteInspections(Long[] ids);

    /**
     * @description 保存检查整改登记表
     * @param enterpriseInspection 日常检查整改pojo
     * @param registrationSheetFile 日常检查整改登记表
     * @return int 更新成功的数目
     */
    int updateRegistrationSheet(EnterpriseInspection enterpriseInspection, MultipartFile registrationSheetFile);

    /**
     * @description 查询整改过期记录的数量
     * @param enterpriseId 企业id
     * @return int 数量
     */
    int getOverdueInspectionNum(Long enterpriseId);
}