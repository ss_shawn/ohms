package com.lanware.ehs.enterprise.equipmentuse.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.equipmentuse.service.EquipmentUseService;
import com.lanware.ehs.pojo.EnterpriseEquipmentUse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 运行使用视图
 */
@Controller("equipmentUseView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入equipmentUseService的bean */
    @Autowired
    private EquipmentUseService equipmentUseService;

    @RequestMapping("equipmentUse/editEquipmentUse")
    /**
     * @description 新增运行使用
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addEquipmentUse() {
        JSONObject data = new JSONObject();
        EnterpriseEquipmentUse enterpriseEquipmentUse = new EnterpriseEquipmentUse();
        data.put("enterpriseEquipmentUse", enterpriseEquipmentUse);
        return new ModelAndView("equipmentUse/edit", data);
    }

    /**
     * @description 编辑运行使用
     * @param id 运行使用id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("equipmentUse/editEquipmentUse/{id}")
    public ModelAndView editEquipmentUse(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseEquipmentUse enterpriseEquipmentUse = equipmentUseService.getEquipmentUseById(id);
        data.put("enterpriseEquipmentUse", enterpriseEquipmentUse);
        return new ModelAndView("equipmentUse/edit", data);
    }

    @RequestMapping("equipmentUse/runEquipment/{recordId}/{type}")
    /**
     * 运行的设备页面
     * @param recordId 记录id
     * @param type 记录类型(运行记录)
     * @return 运行的设备model
     */
    public ModelAndView runEquipment(@PathVariable Long recordId, @PathVariable Integer type){
        JSONObject data = new JSONObject();
        data.put("recordId", recordId);
        data.put("type", type);
        return new ModelAndView("equipmentUse/runEquipment/list", data);
    }

    @RequestMapping("equipmentUse/selectRunEquipment/{recordId}/{type}")
    /**
     * @description 选择运行的设备
     * @param recordId 记录id
     * @param type 记录类型(运行记录)
     * @return 尚未运行的设备model
     */
    public ModelAndView selectRunEquipment(@PathVariable Long recordId, @PathVariable Integer type){
        JSONObject data = new JSONObject();
        data.put("recordId", recordId);
        data.put("type", type);
        return new ModelAndView("equipmentUse/runEquipment/select", data);
    }
}
