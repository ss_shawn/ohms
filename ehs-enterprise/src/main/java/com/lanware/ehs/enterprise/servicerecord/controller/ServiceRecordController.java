package com.lanware.ehs.enterprise.servicerecord.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.servicerecord.service.ServiceRecordService;
import com.lanware.ehs.pojo.EnterpriseServiceRecord;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.EquipmentServiceUse;
import com.lanware.ehs.pojo.custom.EquipmentServiceUseCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 维修记录controller
 */
@Controller
@RequestMapping("/serviceRecord")
public class ServiceRecordController {
    /** 注入serviceRecordService的bean */
    @Autowired
    private ServiceRecordService serviceRecordService;

    @RequestMapping("")
    /**
     * @description 返回维修记录页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "serviceRecord/list";
    }

    @RequestMapping("/listServiceRecords")
    @ResponseBody
    /**
     * @description 获取维修记录result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listServiceRecords(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("type", EquipmentServiceUseCustom.RECORD_SERVICE);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = serviceRecordService.listServiceRecords(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editServiceRecord/save")
    @ResponseBody
    /**
     * @description 保存维修记录
     * @param enterpriseServiceRecordJSON 维修记录json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseServiceRecordJSON, @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseServiceRecord enterpriseServiceRecord
                = JSONObject.parseObject(enterpriseServiceRecordJSON, EnterpriseServiceRecord.class);
        if (enterpriseServiceRecord.getId() == null) {
            enterpriseServiceRecord.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseServiceRecord.setGmtCreate(new Date());
            if (serviceRecordService.insertServiceRecord(enterpriseServiceRecord) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseServiceRecord.setGmtModified(new Date());
            if (serviceRecordService.updateServiceRecord(enterpriseServiceRecord) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteServiceRecord")
    @ResponseBody
    /**
     * @description 删除维修记录
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteServiceRecord(Long id) {
        if (serviceRecordService.deleteServiceRecord(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteServiceRecords")
    @ResponseBody
    /**
     * @description 批量删除维修记录
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteServiceRecords(Long[] ids) {
        if (serviceRecordService.deleteServiceRecords(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/listServiceEquipmentsByRecordId")
    @ResponseBody
    /**
     * @description 根据记录id获取维护设备
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param recordId 记录id
     * @param type 记录类型(维护记录)
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listServiceEquipmentsByRecordId(Integer pageNum, Integer pageSize, String keyword
            , Long recordId, Integer type, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("recordId", recordId);
        condition.put("type", type);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = serviceRecordService.listServiceEquipmentsByRecordId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listSelectServiceEquipmentsByRecordId")
    @ResponseBody
    /**
     * @description 根据记录id过滤尚未维护的设备
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param recordId 记录id
     * @param type 记录类型(维护记录)
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listSelectServiceEquipmentsByRecordId(Integer pageNum, Integer pageSize, String keyword
            , Long recordId, Integer type, @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("recordId", recordId);
        condition.put("type", type);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = serviceRecordService.listSelectServiceEquipmentsByRecordId(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/selectServiceEquipment/save")
    @ResponseBody
    /** @description 保存维护的设备信息
     * @param serviceEquipmentListJSON 维护的设备json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String serviceEquipmentListJSON) {
        List<EquipmentServiceUse> equipmentServiceUseList
                = JSONArray.parseArray(serviceEquipmentListJSON, EquipmentServiceUse.class);
        if (equipmentServiceUseList == null || equipmentServiceUseList.isEmpty()){
            return ResultFormat.error("选择维护设备失败");
        }
        for (EquipmentServiceUse equipmentServiceUse : equipmentServiceUseList){
            equipmentServiceUse.setGmtCreate(new Date());
        }
        if (serviceRecordService.insertBatch(equipmentServiceUseList) > 0) {
            return ResultFormat.success("选择维护设备成功");
        } else {
            return ResultFormat.error("选择维护设备失败");
        }
    }

    @RequestMapping("/deleteServiceEquipment")
    @ResponseBody
    /**
     * @description 删除维护的设备
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteServiceEquipment(Long id) {
        if (serviceRecordService.deleteServiceEquipment(id) > 0) {
            return ResultFormat.success("移除成功");
        }else{
            return ResultFormat.error("移除失败");
        }
    }

    @RequestMapping("/deleteServiceEquipments")
    @ResponseBody
    /**
     * @description 批量删除维护的设备
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteServiceEquipments(Long[] ids) {
        if (serviceRecordService.deleteServiceEquipments(ids) == ids.length) {
            return ResultFormat.success("移除成功");
        }else{
            return ResultFormat.error("移除失败");
        }
    }
}
