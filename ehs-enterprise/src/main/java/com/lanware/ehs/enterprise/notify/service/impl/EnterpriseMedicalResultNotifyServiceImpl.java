package com.lanware.ehs.enterprise.notify.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.notify.service.EnterpriseMedicalResultNotifyService;
import com.lanware.ehs.mapper.EnterpriseMedicalResultNotifyMapper;
import com.lanware.ehs.mapper.EnterpriseMedicalResultWaitNotifyMapper;
import com.lanware.ehs.mapper.custom.EnterpriseMedicalResultNotifyCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseMedicalResultWaitNotifyCustomMapper;
import com.lanware.ehs.mapper.custom.YearNotifyReportCustomMapper;
import com.lanware.ehs.pojo.EnterpriseMedicalResultNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotify;
import com.lanware.ehs.pojo.EnterpriseMedicalResultWaitNotifyExample;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.EnterpriseMedicalResultNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseMedicalResultWaitNotifyCustom;
import com.lanware.ehs.pojo.custom.YearNotifyReportCustom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EnterpriseMedicalResultNotifyServiceImpl implements EnterpriseMedicalResultNotifyService {

    @Autowired
    /** 注入EnterpriseMedicalResultWaitNotifyMapper的bean */
    private EnterpriseMedicalResultWaitNotifyMapper enterpriseMedicalResultWaitNotifyMapper;
    @Autowired
    /** 注入EnterpriseMedicalResultNotifyMapper的bean */
    private EnterpriseMedicalResultNotifyMapper enterpriseMedicalResultNotifyMapper;
    @Autowired
    /** 注入EnterpriseMedicalResultNotifyCustomMapper的bean */
    private EnterpriseMedicalResultNotifyCustomMapper enterpriseMedicalResultNotifyCustomMapper;
    @Autowired
    /** 注入EnterpriseMedicalResultWaitNotifyCustomMapper的bean */
    private EnterpriseMedicalResultWaitNotifyCustomMapper enterpriseMedicalResultWaitNotifyCustomMapper;
    @Autowired
    private YearNotifyReportCustomMapper yearNotifyReportCustomMapper;
    @Autowired
    private OSSTools ossTools;
    @Override
    /**
     * @description 根据id查询待体检结果告知
     * @param id 待体检结果告知id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    public EnterpriseMedicalResultWaitNotify getEnterpriseMedicalResultWaitNotifyById(Long id) {
        return enterpriseMedicalResultWaitNotifyMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 根据id查询待体检结果告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public Page<EnterpriseMedicalResultWaitNotifyCustom> findEnterpriseMedicalResultWaitNotifyList(Map<String, Object> condition, QueryRequest request){
        if(request != null){
            //分页设置
            Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
            String sortField = request.getField();
            if(StringUtils.isNotBlank(sortField)) {
                sortField = EhsUtil.camelToUnderscore(sortField);
                page.setOrderBy(sortField + " "+request.getOrder());
            }
        }
        return enterpriseMedicalResultWaitNotifyCustomMapper.findEnterpriseMedicalResultWaitNotifyList(condition);
    }



    /**
     * 保存体检结果告知,
     * @param enterpriseMedicalResultNotifyCustom
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int saveEnterpriseMedicalResultNotify(EnterpriseMedicalResultNotifyCustom enterpriseMedicalResultNotifyCustom,MultipartFile checkResultProve){
        int n=0;
        //新增数据先保存，以便获取id
        if(enterpriseMedicalResultNotifyCustom.getId()==null) {//
            enterpriseMedicalResultNotifyCustom.setGmtCreate(new Date());
            n=enterpriseMedicalResultNotifyMapper.insert(enterpriseMedicalResultNotifyCustom);
            //修改待发放状态
            EnterpriseMedicalResultWaitNotify enterpriseMedicalResultWaitNotify=new EnterpriseMedicalResultWaitNotify();
            enterpriseMedicalResultWaitNotify.setStatus(1);
            enterpriseMedicalResultWaitNotify.setId(enterpriseMedicalResultNotifyCustom.getWaitMedicalNotifyId());
            enterpriseMedicalResultWaitNotifyMapper.updateByPrimaryKeySelective(enterpriseMedicalResultWaitNotify);
        }
        if (checkResultProve != null){
            //上传了新的聘用书
            if (!org.springframework.util.StringUtils.isEmpty(enterpriseMedicalResultNotifyCustom.getNotifyFile())){
                //删除旧文件
                ossTools.deleteOSS(enterpriseMedicalResultNotifyCustom.getNotifyFile());
            }
            //上传并保存新文件
            String employFilePath = "/" + enterpriseMedicalResultNotifyCustom.getEnterpriseId() + "/" + ModuleName.MEDICALNOTIFY.getValue() + "/" + enterpriseMedicalResultNotifyCustom.getId() + "/" + checkResultProve.getOriginalFilename();
            try {
                ossTools.uploadStream(employFilePath, checkResultProve.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传告知书失败", e);
            }
            enterpriseMedicalResultNotifyCustom.setNotifyFile(employFilePath);
        }
        enterpriseMedicalResultNotifyCustom.setGmtModified(new Date());
        n=enterpriseMedicalResultNotifyMapper.updateByPrimaryKey(enterpriseMedicalResultNotifyCustom);
        return n;
    }
      /**
     * 通过id获取体检结果告知
     * @param id 体检结果告知id
     * @return体检结果告知
     */
    @Override
    public EnterpriseMedicalResultNotifyCustom getEnterpriseMedicalResultNotifyCustomByID(long id){
        return  enterpriseMedicalResultNotifyCustomMapper.selectByPrimaryKey(id);
    }
    /**
     * 删除待体检结果告知,
     * @param id
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int deleteEnterpriseMedicalResultNotify(long id){
        //获取体检结果告知来删除文件
        EnterpriseMedicalResultNotify enterpriseMedicalResultNotify=enterpriseMedicalResultNotifyMapper.selectByPrimaryKey(id);
        String notifyFile=enterpriseMedicalResultNotify.getNotifyFile();
        if (!org.springframework.util.StringUtils.isEmpty(notifyFile)){
            ossTools.deleteOSS(notifyFile);
        }
        //修改待告知状态
        EnterpriseMedicalResultWaitNotifyExample enterpriseMedicalResultWaitNotifyExample=new EnterpriseMedicalResultWaitNotifyExample();
        enterpriseMedicalResultWaitNotifyExample.createCriteria().andMedicalIdEqualTo(enterpriseMedicalResultNotify.getMedicalId());
        EnterpriseMedicalResultWaitNotify enterpriseMedicalResultWaitNotify=new EnterpriseMedicalResultWaitNotify();
        enterpriseMedicalResultWaitNotify.setStatus(0);
        enterpriseMedicalResultWaitNotifyMapper.updateByExampleSelective(enterpriseMedicalResultWaitNotify,enterpriseMedicalResultWaitNotifyExample);
        //删除待体检结果告知
        int n=enterpriseMedicalResultNotifyMapper.deleteByPrimaryKey(id);
        return n;
    }
    /**
     * 通过待体检结果告知ID获取待体检结果告知的人员ID，体检ID，体检日期信息,放入体检结果告知返回
     * @param id
     * @return 体检结果告知
     */
    @Override
    public EnterpriseMedicalResultNotifyCustom getEnterpriseMedicalResultNotifyCustomByWaitID(long id){
        //查询出待体检结果告知信息
        EnterpriseMedicalResultWaitNotifyCustom enterpriseMedicalResultWaitNotify=enterpriseMedicalResultWaitNotifyCustomMapper.selectByPrimaryKey(id);
        //创建体检结果告知，并把人员ID，体检ID，体检日期设置
        EnterpriseMedicalResultNotifyCustom enterpriseMedicalResultNotifyCustom=new EnterpriseMedicalResultNotifyCustom();
        enterpriseMedicalResultNotifyCustom.setEmployeeName(enterpriseMedicalResultWaitNotify.getEmployeeName());
        enterpriseMedicalResultNotifyCustom.setEmployeeId(enterpriseMedicalResultWaitNotify.getEmployeeId());
        enterpriseMedicalResultNotifyCustom.setEnterpriseId(enterpriseMedicalResultWaitNotify.getEnterpriseId());
        enterpriseMedicalResultNotifyCustom.setMedicalDate(enterpriseMedicalResultWaitNotify.getMedicalDate());
        enterpriseMedicalResultNotifyCustom.setMedicalId(enterpriseMedicalResultWaitNotify.getMedicalId());
        return  enterpriseMedicalResultNotifyCustom;
    }
    /**
     * @description 根据条件查询体检结果告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public Page<EnterpriseMedicalResultNotifyCustom> findEnterpriseMedicalResultNotifyList(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return enterpriseMedicalResultNotifyCustomMapper.findEnterpriseMedicalResultNotifyList(condition);
    }

    /**
     * 获取告知年度统计报表
     * @param condition
     * @return告知年度统计报表
     */
    @Override
    public List<YearNotifyReportCustom> getYearReportNotifyList(Map<String, Object> condition){
        List<YearNotifyReportCustom> yearNotifyReportCustomList=yearNotifyReportCustomMapper.getWorkWaitNotify(condition);
        yearNotifyReportCustomList.addAll(yearNotifyReportCustomMapper.getWorkplaceWaitNotify(condition));
        yearNotifyReportCustomList.addAll(yearNotifyReportCustomMapper.getMedicalResultNotify(condition));
        yearNotifyReportCustomList.addAll(yearNotifyReportCustomMapper.getDiseaseDiagnosiWaitNotify(condition));
        return yearNotifyReportCustomList;
    }
}
