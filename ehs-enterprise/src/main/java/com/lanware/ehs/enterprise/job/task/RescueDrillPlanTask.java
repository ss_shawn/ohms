package com.lanware.ehs.enterprise.job.task;

import com.google.common.collect.Maps;
import com.lanware.ehs.common.utils.DateUtil;
import com.lanware.ehs.enterprise.remind.service.RemindService;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.pojo.EnterpriseRemind;
import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.custom.EnterpriseRemindCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 应急救援演练任务
 */
@Service
@Slf4j
public class RescueDrillPlanTask {
    @Autowired
    private YearplanService yearplanService;

    @Autowired
    private RemindService remindService;

    /**
     * 执行应急救援演练计划任务
     */
    public void executeRescueDrillPlanTask(){
        //获取当前时间
        LocalDate localDate = LocalDate.now();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        try {
            LocalDate currentDate = LocalDate.of(year,month,1);
            //查询应急救援演练计划执行月份。
            Map<String,Object> condition = Maps.newHashMap();
            condition.put("year",year);
            condition.put("planType","yjjyyl");
            List<EnterpriseYearPlan> yearPlans = yearplanService.findYearplansByCondition(condition);
            for(EnterpriseYearPlan yearPlan : yearPlans){
                String planPeriod = yearPlan.getPlanPeriod();
                if(org.apache.commons.lang3.StringUtils.isNotEmpty(planPeriod)){
                    String[] planMonths = planPeriod.split(",");
                    for(String planMonth : planMonths){
                        LocalDate planTaskDate = LocalDate.of(year,Integer.parseInt(planMonth),1);
                        //计划日期等于当前日期前一个月或者等于当前时间执行提醒
                        LocalDate nextMonthDate = currentDate.plusMonths(1);
                        if(nextMonthDate.isEqual(planTaskDate) || currentDate.isEqual(planTaskDate)){
                            String planDate = year+"-"+planMonth;
                            //如果提醒记录表已经存在记录，则不在提醒
                            List<EnterpriseRemind> remindList = remindService.findRemindByRemindDate(
                                    EnterpriseRemindCustom.TYPE_YJJY,planDate,yearPlan.getEnterpriseId(),
                                    DateUtil.localDate2Date(currentDate)
                                    ,DateUtil.localDate2Date(currentDate.with(TemporalAdjusters.lastDayOfMonth())));

                            //如果当月已经提醒则不在提醒
                            if(remindList.size() == 0){
                                EnterpriseRemind remind = new EnterpriseRemind();
                                remind.setType(EnterpriseRemindCustom.TYPE_YJJY);
                                remind.setStatus(EnterpriseRemindCustom.STATUS_DTX);
                                remind.setEnterpriseId(yearPlan.getEnterpriseId());
                                remind.setGmtCreate(new Date());
                                remind.setPlanDate(planDate);
                                remind.setContent("您计划于"+yearPlan.getYear()+"-"+planMonth+"月进行紧急救援演练");
                                remindService.insert(remind);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("{}-{}生成应急救援演练计划提醒失败",year,month,e);
        }
    }
}
