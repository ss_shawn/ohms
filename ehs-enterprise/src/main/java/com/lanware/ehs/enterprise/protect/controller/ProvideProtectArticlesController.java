package com.lanware.ehs.enterprise.protect.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.protect.service.ProvideProtectArticlesService;
import com.lanware.ehs.pojo.BaseHarmFactor;
import com.lanware.ehs.pojo.EnterpriseProtectArticles;
import com.lanware.ehs.pojo.EnterpriseWaitProvideProtectArticles;
import com.lanware.ehs.pojo.custom.*;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("protectArticles")
public class ProvideProtectArticlesController extends BaseController {
    @Autowired
    /** 注入enterpriseBaseinfoService的bean */
    private ProvideProtectArticlesService provideProtectArticlesService;

    /**
     * 获取防护用品发放列表
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("list")
    public EhsResult userList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //排序
        condition.put("orderByClause","provide_date desc");
        //查询并返回deleteProvide
        Map<String, Object> dataTable = getDataTable(this.provideProtectArticlesService.findProtectList(condition,request));
        return EhsResult.ok(dataTable);
    }
    /**
     * 保存防护用品发放
     * @return
     */
    @RequestMapping("provideAdd")
    public EhsResult addEnterpriseProtectArticles(String provideCustoms, MultipartFile signatureRecord, @CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticlesCustom=JSONObject.parseObject(provideCustoms, EnterpriseProvideProtectArticlesCustom.class);
        enterpriseProvideProtectArticlesCustom.setEnterpriseId(currentUser.getEnterpriseId());//插入企业ID
        this.provideProtectArticlesService.saveEnterpriseProvideProtectArticles(enterpriseProvideProtectArticlesCustom,signatureRecord);
        return EhsResult.ok("");
    }
    /**
     * 保存防护用品批量发放
     * @return
     */
    @RequestMapping("provideAddAll")
    public EhsResult provideAddAll(String provideCustoms, MultipartFile signatureRecord, @CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticlesCustom=JSONObject.parseObject(provideCustoms, EnterpriseProvideProtectArticlesCustom.class);
        enterpriseProvideProtectArticlesCustom.setEnterpriseId(currentUser.getEnterpriseId());//插入企业ID
        this.provideProtectArticlesService.saveEnterpriseProvideProtectArticlesAll(enterpriseProvideProtectArticlesCustom,signatureRecord);
        return EhsResult.ok("");
    }

    /**
     * 获取防护危害因素列表
     * @return
     */
    @RequestMapping("getEmployeeByPostId/{postId}")
    public EhsResult getEmployeeByPostId(String keyword, @PathVariable Long postId, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤岗位
        condition.put("postId",postId);
        //过滤查询条件
        condition.put("keyword",keyword);
        List<EmployeePostCustom>  employeePostCustomList=provideProtectArticlesService.listEmployeePostByPostId(condition);
        return EhsResult.ok(employeePostCustomList);
    }
    /**
     * 删除防护用品待发放记录
     * @return
     */
    @RequestMapping("delProvideProtectSubmit")
    public EhsResult delProvideProtectSubmit(String provideCustoms,  @CurrentUser EnterpriseUserCustom currentUser) {
        EnterpriseWaitProvideProtectArticles enterpriseWaitProvideProtectArticles=JSONObject.parseObject(provideCustoms, EnterpriseWaitProvideProtectArticles.class);
        enterpriseWaitProvideProtectArticles.setStatus(2);
        enterpriseWaitProvideProtectArticles.setDeleteTime(new Date());
        enterpriseWaitProvideProtectArticles.setGmtModified(new Date());
        this.provideProtectArticlesService.delProvideProtectSubmit(enterpriseWaitProvideProtectArticles);
        return EhsResult.ok("");
    }
    /**
     * 删除防护用品发放
     * @return
     */
    @RequestMapping("deleteProvide")
    public EhsResult addEnterpriseProtectArticles(Long id, @CurrentUser EnterpriseUserCustom currentUser) {
        this.provideProtectArticlesService.deleteEnterpriseProvideProtectArticles(id);
        return EhsResult.ok("");
    }
    /**
     * 获取防护危害因素列表根据员工ID获取员工岗位信息
     * @return
     */
    @RequestMapping("getPostByEmployeeId")
    public EhsResult getPostByEmployeeId( Long employeeId,@CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤员工
        condition.put("employeeId",employeeId);
        condition.put("sortField","work_date");
        condition.put("sortType","desc");
        List<EmployeePostCustom> employeePosts=this.provideProtectArticlesService.getEmployeePostByEmployeeId(condition);
        return EhsResult.ok(employeePosts);
    }
    /**
     * 获取岗位信息
     * @return
     */
    @RequestMapping("getPost")
    public EhsResult getPost( @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤单位
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        List<EnterprisePostCustom> employeePosts=this.provideProtectArticlesService.getPost(condition);
        return EhsResult.ok(employeePosts);
    }

    /**
     * 根据人员岗位ID获取防护物品信息
     * @return
     */
    @RequestMapping("getProtectByPostId")
    public EhsResult getProtectByPostId( Long employeePostId,@CurrentUser EnterpriseUserCustom currentUser) {

        List<EnterpriseProtectArticlesCustom> protects=this.provideProtectArticlesService.getProtectByByEmployeePostId(employeePostId);
        return EhsResult.ok(protects);
    }
    /**
     * 防护物品信息
     * @return
     */
    @RequestMapping("getProtect")
    public EhsResult getProtect( @CurrentUser EnterpriseUserCustom currentUser) {

        List<EnterpriseProtectArticles> protects=this.provideProtectArticlesService.getProtect(currentUser.getEnterpriseId());
        return EhsResult.ok(protects);
    }

    /**
     * 根据人员岗位ID获取防护物品信息
     * @return
     */
    @RequestMapping("getProtectByEmployeePostId")
    public EhsResult getWaitProtectByEmployeePostId( Long employeePostId,@CurrentUser EnterpriseUserCustom currentUser) {
        List<EnterpriseProtectArticlesCustom> protects=this.provideProtectArticlesService.getWaitProtectByByEmployeePostId(employeePostId);
        return EhsResult.ok(protects);
    }


    /**
     * 获取防护用品发放列表
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("waitList")
    public EhsResult waitList( String keyword,QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        condition.put("status",0);
        //查询并返回deleteProvide
        Map<String, Object> dataTable = getDataTable(this.provideProtectArticlesService.findEnterpriseWaitProvideProtectArticles(condition,request));
        return EhsResult.ok(dataTable);
    }
    /**
     * 保存防护用品发放签名记录
     * @return
     */
    @RequestMapping("uploadSignatureRecord")
    public EhsResult uploadSignatureRecord(Long id, MultipartFile signatureRecord, @CurrentUser EnterpriseUserCustom currentUser) {
        this.provideProtectArticlesService.uploadSignatureRecord(id,signatureRecord);
        return EhsResult.ok("");
    }
}
