package com.lanware.ehs.enterprise.workplace.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.workplace.service.HarmfactorService;
import com.lanware.ehs.enterprise.workplace.service.IdentifyFileService;
import com.lanware.ehs.enterprise.workplace.service.WorkplaceService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseWorkplace;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.WorkplaceHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 工作场所视图
 */
@Controller("workplaceView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入workplaceService的bean */
    @Autowired
    private WorkplaceService workplaceService;
    @Autowired
    /** 注入harmfactorService的bean */
    private HarmfactorService harmfactorService;
    /** 注入identifyFileService的bean */
    @Autowired
    private IdentifyFileService identifyFileService;

    /**
     * @description 新增作业场所信息
     * @param enterpriseUser 企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("workplace/editWorkplace")
    public ModelAndView addWorkplace(@CurrentUser EnterpriseUserCustom enterpriseUser) {
        JSONObject data = new JSONObject();
        EnterpriseWorkplace enterpriseWorkplace = new EnterpriseWorkplace();
        enterpriseWorkplace.setEnterpriseId(enterpriseUser.getEnterpriseId());
        data.put("enterpriseWorkplace", enterpriseWorkplace);
        return new ModelAndView("workplace/edit", data);
    }

    /**
     * @description 编辑作业场所信息
     * @param id 作业场所id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("workplace/editWorkplace/{id}")
    public ModelAndView editWorkplace(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseWorkplace enterpriseWorkplace = workplaceService.getWorkplaceById(id);
        data.put("enterpriseWorkplace", enterpriseWorkplace);
        return new ModelAndView("workplace/edit", data);
    }

    @RequestMapping("workplace/harmfactor/{workplaceId}")
    /**
     * @description 返回职业病危害因素页面
     * @param workplaceId 作业场所id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView list(@PathVariable Long workplaceId){
        JSONObject data = new JSONObject();
        data.put("workplaceId", workplaceId);
        return new ModelAndView("workplace/harmfactor/list", data);
    }

    @RequestMapping("workplace/harmfactor/addHarmfactor/{workplaceId}")
    /**
     * @description 新增职业病危害因素
     * @param workplaceId 工作场所id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addHarmfactor(@PathVariable Long workplaceId) {
        JSONObject data = new JSONObject();
        WorkplaceHarmFactorCustom workplaceHarmFactor = new WorkplaceHarmFactorCustom();
        workplaceHarmFactor.setWorkplaceId(workplaceId);
        data.put("workplaceHarmFactor", workplaceHarmFactor);
        return new ModelAndView("workplace/harmfactor/edit", data);
    }

    @RequestMapping("workplace/harmfactor/editHarmfactor/{id}")
    /**
     * @description 编辑职业病危害因素
     * @param id 职业病危害因素id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editHarmfactor(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        WorkplaceHarmFactorCustom workplaceHarmFactor = harmfactorService.getHarmfactorCustomById(id);
        data.put("workplaceHarmFactor", workplaceHarmFactor);
        return new ModelAndView("workplace/harmfactor/edit", data);
    }

    @RequestMapping("workplace/identifyFile/{relationId}/{relationModule}")
    /**
     * @description 返回职业病危害警示页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView identifyFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("workplace/identifyFile/list", data);
    }

    @RequestMapping("workplace/identifyFile/addIdentifyFile/{relationId}/{relationModule}")
    /**
     * @description 新增职业病危害警示
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addIdentifyFile(@PathVariable Long relationId, @PathVariable String relationModule) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(relationId);
        enterpriseRelationFile.setRelationModule(relationModule);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("workplace/identifyFile/edit", data);
    }

    @RequestMapping("workplace/identifyFile/editIdentifyFile/{id}")
    /**
     * @description 编辑职业病危害警示
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editIdentifyFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = identifyFileService.getIdentifyFileById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("workplace/identifyFile/edit", data);
    }

    @RequestMapping("workplace/informFile/{relationId}/{relationModule}")
    /**
     * @description 返回职业高毒物品告知卡页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView informFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("workplace/informFile/list", data);
    }

    @RequestMapping("workplace/informFile/addInformFile/{relationId}/{relationModule}")
    /**
     * @description 新增高毒物品告知卡
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addInformFile(@PathVariable Long relationId, @PathVariable String relationModule) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(relationId);
        enterpriseRelationFile.setRelationModule(relationModule);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("workplace/informFile/edit", data);
    }

    @RequestMapping("workplace/informFile/editInformFile/{id}")
    /**
     * @description 编辑高毒物品告知卡
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editInformFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = identifyFileService.getIdentifyFileById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("workplace/informFile/edit", data);
    }

    @RequestMapping("workplace/informationFile/{relationId}/{relationModule}")
    /**
     * @description 返回职业危害因素信息卡页面
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView informationFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("workplace/informationFile/list", data);
    }

    @RequestMapping("workplace/informationFile/addInformationFile/{relationId}/{relationModule}")
    /**
     * @description 新增职业危害因素信息卡
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addInformationFile(@PathVariable Long relationId, @PathVariable String relationModule) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(relationId);
        enterpriseRelationFile.setRelationModule(relationModule);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("workplace/informationFile/edit", data);
    }

    @RequestMapping("workplace/informationFile/editInformationFile/{id}")
    /**
     * @description 编辑职业危害因素信息卡
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView editInformationFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = identifyFileService.getIdentifyFileById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("workplace/informationFile/edit", data);
    }
}
