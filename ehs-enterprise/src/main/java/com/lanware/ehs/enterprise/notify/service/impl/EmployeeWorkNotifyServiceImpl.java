package com.lanware.ehs.enterprise.notify.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.notify.service.EmployeeWorkNotifyService;
import com.lanware.ehs.mapper.EmployeeWorkNotifyMapper;
import com.lanware.ehs.mapper.EmployeeWorkWaitNotifyMapper;
import com.lanware.ehs.mapper.EnterpriseProtectArticlesMapper;
import com.lanware.ehs.mapper.custom.EmployeeWorkNotifyCustomMapper;
import com.lanware.ehs.mapper.custom.EmployeeWorkWaitNotifyCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseProtectArticlesCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EmployeeWorkNotifyServiceImpl implements EmployeeWorkNotifyService {

    @Autowired
    /** 注入EmployeeWorkWaitNotifyMapper的bean */
    private EmployeeWorkWaitNotifyMapper employeeWorkWaitNotifyMapper;
    @Autowired
    /** 注入EmployeeWorkNotifyMapper的bean */
    private EmployeeWorkNotifyMapper employeeWorkNotifyMapper;
    @Autowired
    /** 注入EmployeeWorkNotifyCustomMapper的bean */
    private EmployeeWorkNotifyCustomMapper employeeWorkNotifyCustomMapper;
    @Autowired
    /** 注入EmployeeWorkWaitNotifyCustomMapper的bean */
    private EmployeeWorkWaitNotifyCustomMapper employeeWorkWaitNotifyCustomMapper;
    @Autowired
    private OSSTools ossTools;
    @Override
    /**
     * @description 根据id查询待岗前告知
     * @param id 待岗前告知id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    public EmployeeWorkWaitNotify getEmployeeWorkWaitNotifyById(Long id) {
        return employeeWorkWaitNotifyMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 根据id查询待岗前告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public Page<EmployeeWorkWaitNotifyCustom> findEmployeeWorkWaitNotifyList(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return employeeWorkWaitNotifyCustomMapper.findEmployeeWorkWaitNotifyList(condition);
    }



    /**
     * 保存岗前告知,
     * @param employeeWorkNotifyCustom
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int saveEmployeeWorkNotify(EmployeeWorkNotifyCustom employeeWorkNotifyCustom,MultipartFile notifyFile){
        int n=0;
        //新增数据先保存，以便获取id
        if(employeeWorkNotifyCustom.getId()==null) {//
            employeeWorkNotifyCustom.setGmtCreate(new Date());
            n=employeeWorkNotifyMapper.insert(employeeWorkNotifyCustom);
            //修改待发放状态
            EmployeeWorkWaitNotify employeeWorkWaitNotify=new EmployeeWorkWaitNotify();
            employeeWorkWaitNotify.setStatus(1);
            employeeWorkWaitNotify.setId(employeeWorkNotifyCustom.getWaitWorkNotifyId());
            employeeWorkWaitNotifyMapper.updateByPrimaryKeySelective(employeeWorkWaitNotify);
        }
        if (notifyFile != null){
            //上传了新的聘用书
            if (!org.springframework.util.StringUtils.isEmpty(employeeWorkNotifyCustom.getNotifyFile())){
                //删除旧文件
                ossTools.deleteOSS(employeeWorkNotifyCustom.getNotifyFile());
            }
            //上传并保存新文件
            String employFilePath = "/" + employeeWorkNotifyCustom.getEnterpriseId() + "/" + ModuleName.WORKNOTIFY.getValue() + "/" + employeeWorkNotifyCustom.getId() + "/" + notifyFile.getOriginalFilename();
            try {
                ossTools.uploadStream(employFilePath, notifyFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传告知书失败", e);
            }
            employeeWorkNotifyCustom.setNotifyFile(employFilePath);
        }
        employeeWorkNotifyCustom.setGmtModified(new Date());
        n=employeeWorkNotifyMapper.updateByPrimaryKey(employeeWorkNotifyCustom);
        return n;
    }
      /**
     * 通过id获取岗前告知
     * @param id 岗前告知id
     * @return岗前告知
     */
    @Override
    public EmployeeWorkNotifyCustom getEmployeeWorkNotifyCustomByID(long id){
        return  employeeWorkNotifyCustomMapper.selectByPrimaryKey(id);
    }
    /**
     * 删除待岗前告知,
     * @param id
     * @return成功与否 1成功 0不成功
     */
    @Override
    @Transactional
    public int deleteEmployeeWorkNotify(long id){
        //获取岗前告知来删除文件
        EmployeeWorkNotify employeeWorkNotify=employeeWorkNotifyMapper.selectByPrimaryKey(id);
        String notifyFile=employeeWorkNotify.getNotifyFile();
        if (!org.springframework.util.StringUtils.isEmpty(notifyFile)){
            ossTools.deleteOSS(notifyFile);
        }
        //修改待告知状态
        EmployeeWorkWaitNotifyExample employeeWorkWaitNotifyExample=new EmployeeWorkWaitNotifyExample();
        employeeWorkWaitNotifyExample.createCriteria().andEmployeePostIdEqualTo(employeeWorkNotify.getEmployeePostId());
        EmployeeWorkWaitNotify employeeWorkWaitNotify=new EmployeeWorkWaitNotify();
        employeeWorkWaitNotify.setStatus(0);
        employeeWorkWaitNotifyMapper.updateByExampleSelective(employeeWorkWaitNotify,employeeWorkWaitNotifyExample);
        //删除待岗前告知
        int n=employeeWorkNotifyMapper.deleteByPrimaryKey(id);
        return n;
    }
    /**
     * 通过待岗前告知ID获取待岗前告知的人员和岗位信息,放入岗前告知返回
     * @param id
     * @return 岗前告知
     */
    @Override
    public EmployeeWorkNotifyCustom getEmployeeWorkNotifyCustomByWaitID(long id){
        //查询出待发放物品信息
        EmployeeWorkWaitNotifyCustom employeeWorkWaitNotify=employeeWorkWaitNotifyCustomMapper.selectByPrimaryKey(id);
        EmployeeWorkNotifyCustom employeeWorkNotifyCustom=new EmployeeWorkNotifyCustom();
        employeeWorkNotifyCustom.setEmployeeId(employeeWorkWaitNotify.getEmployeeId());//人员ID
        employeeWorkNotifyCustom.setEmployeeName(employeeWorkWaitNotify.getEmployeeName());
        employeeWorkNotifyCustom.setEmployeePostId(employeeWorkWaitNotify.getEmployeePostId());//人员岗位ID
        employeeWorkNotifyCustom.setPostName(employeeWorkWaitNotify.getPostName());
        return  employeeWorkNotifyCustom;
    }
    /**
     * @description 根据条件查询岗前告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    @Override
    public Page<EmployeeWorkNotifyCustom> findEmployeeWorkNotifyList(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"id desc");
        String sortField = request.getField();
        if(StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return employeeWorkNotifyCustomMapper.findEmployeeWorkNotifyList(condition);
    }

    @Override
    public List<EmployeeWorkWaitNotifyCustom> findWorkWaitNofity(Map<String, Object> condition) {
        return employeeWorkWaitNotifyCustomMapper.findEmployeeWorkWaitNotifyList(condition).getResult();
    }
}
