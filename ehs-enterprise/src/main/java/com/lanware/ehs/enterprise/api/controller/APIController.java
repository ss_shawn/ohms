package com.lanware.ehs.enterprise.api.controller;

import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.enterprise.government.service.GovernmentService;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 对外开放接口
 */
@RestController
@RequestMapping("api")
public class APIController {
    @Autowired
    private GovernmentService governmentService;

    /**
     * 同步局端数据
     * @param enterpriseId
     * @return
     */
    @RequestMapping("/uploadGovernment/{enterpriseId}")
    public EhsResult uploadGovernment(@PathVariable Long enterpriseId){
        try {
            governmentService.uploadGovernment(enterpriseId);
            return EhsResult.ok();
        } catch (Exception e) {
            return EhsResult.build(500,e.getMessage());
        }

    }
}
