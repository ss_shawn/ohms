package com.lanware.ehs.enterprise.file.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.file.service.CorrespondFileService;
import com.lanware.ehs.enterprise.file.service.FileService;
import com.lanware.ehs.pojo.EnterpriseFile;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 文件管理视图
 */
@Controller("fileView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入fileService的bean */
    @Autowired
    private FileService fileService;
    /** 注入correspondFileService的bean */
    @Autowired
    private CorrespondFileService correspondFileService;

    /**
     * @description 新增文件信息
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("file/editFile")
    public ModelAndView addFile() {
        JSONObject data = new JSONObject();
        EnterpriseFile enterpriseFile = new EnterpriseFile();
        data.put("enterpriseFile", enterpriseFile);
        return new ModelAndView("file/edit", data);
    }

    /**
     * @description 编辑文件信息
     * @param id 文件信息id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("file/editFile/{id}")
    public ModelAndView editFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseFile enterpriseFile = fileService.getFileById(id);
        data.put("enterpriseFile", enterpriseFile);
        return new ModelAndView("file/edit", data);
    }

    /**
     * @description 返回往来文件页面
     * @param relationId 往来文件信息id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("file/correspondFile/{relationId}/{relationModule}")
    public ModelAndView correspondFile(@PathVariable Long relationId, @PathVariable String relationModule){
        JSONObject data = new JSONObject();
        data.put("relationId", relationId);
        data.put("relationModule", relationModule);
        return new ModelAndView("file/correspondFile/list", data);
    }

    /**
     * @description 新增往来文件
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("file/correspondFile/addCorrespondFile/{relationId}/{relationModule}")
    public ModelAndView addCorrespondFile(@PathVariable Long relationId, @PathVariable String relationModule) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = new EnterpriseRelationFile();
        enterpriseRelationFile.setRelationId(relationId);
        enterpriseRelationFile.setRelationModule(relationModule);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("file/correspondFile/edit", data);
    }

    /**
     * @description 编辑往来文件
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("file/correspondFile/editCorrespondFile/{id}")
    public ModelAndView editCorrespondFile(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = correspondFileService.getCorrespondFileById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("file/correspondFile/edit", data);
    }
}
