package com.lanware.ehs.enterprise.consult.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.SysConsult;

import java.util.List;
import java.util.Map;

/**
 * @description 咨询提问interface
 */
public interface ConsultService {
    /**
     * @description 查询咨询提问list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含sysRecommendInstitutions和totalNum的result
     */
    JSONObject listConsults(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询咨询提问信息
     * @param id
     * @return SysConsult 咨询提问pojo
     */
    SysConsult getConsultById(Long id);

    /**
     * @description 新增咨询提问
     * @param sysConsult 咨询提问pojo
     * @return 插入成功的数目
     */
    int insertConsult(SysConsult sysConsult);

    /**
     * @description 更新咨询提问
     * @param sysConsult 咨询提问pojo
     * @return 更新成功的数目
     */
    int updateConsult(SysConsult sysConsult);

    /**
     * @description 删除咨询提问
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteConsult(Long id);

    /**
     * @description 批量删除咨询提问
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteConsults(Long[] ids);

    /**
     * 查询企业咨询最新的回复信息
     * @param enterpriseId 企业id
     * @param answerStatus 回复状态:最新回复
     * @return 最新回复list
     */
    List<SysConsult> queryNewReply(Long enterpriseId, Integer answerStatus);

    /**
     * @description 将状态为新回复的咨询更新为已回复
     * @param enterpriseId 企业id
     * @return 更新成功的数目
     */
    int updateAnswerStatusByEnterpriseId(Long enterpriseId);
}
