package com.lanware.ehs.enterprise.role.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.EnterpriseUserRole;
import com.lanware.ehs.pojo.custom.EnterpriseUserRoleCustom;

import java.util.List;

/**
 *
 */
public interface RoleService{
    /**
     * 通过用户名查找用户角色
     *
     * @param username 用户名
     * @return 用户角色集合
     */
    List<EnterpriseUserRoleCustom> findUserRole(String username);

    /**
     * 查找所有角色
     *
     * @param role 角色对象（用于传递查询条件）
     * @return 角色集合
     */
    List<EnterpriseUserRole> findRoles(EnterpriseUserRole role);


    /**
     * 查找所有角色（分页）
     *
     * @param role    角色对象（用于传递查询条件）
     * @param request request
     * @return IPage
     */
    Page<EnterpriseUserRoleCustom> findRoles(EnterpriseUserRoleCustom role, QueryRequest request);

    /**
     * 根据id获取用户角色
     * @param id
     * @return
     */
    EnterpriseUserRoleCustom getRoleCustomById(Long id);


    /**
     * 新增角色
     *
     * @param role 待新增的角色
     */
    void createRole(EnterpriseUserRoleCustom role);


    /**
     * 删除角色
     *
     * @param roleIds 待删除角色的 id
     */
    void deleteRoles(String roleIds);


    /**
     * 修改角色
     *
     * @param role 待修改的角色
     */
    void updateRole(EnterpriseUserRoleCustom role);

}
