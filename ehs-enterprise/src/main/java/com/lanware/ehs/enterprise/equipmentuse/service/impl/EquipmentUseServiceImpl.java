package com.lanware.ehs.enterprise.equipmentuse.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.equipmentuse.service.EquipmentUseService;
import com.lanware.ehs.mapper.custom.EnterpriseEquipmentUseCustomMapper;
import com.lanware.ehs.mapper.custom.EquipmentServiceUseCustomMapper;
import com.lanware.ehs.pojo.EnterpriseEquipmentUse;
import com.lanware.ehs.pojo.EnterpriseEquipmentUseExample;
import com.lanware.ehs.pojo.EquipmentServiceUse;
import com.lanware.ehs.pojo.EquipmentServiceUseExample;
import com.lanware.ehs.pojo.custom.EnterpriseEquipmentUseCustom;
import com.lanware.ehs.pojo.custom.EquipmentServiceUseCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 运行使用interface实现类
 */
@Service
public class EquipmentUseServiceImpl implements EquipmentUseService {
    /** 注入enterpriseEquipmentUsesCustomMapper的bean */
    @Autowired
    private EnterpriseEquipmentUseCustomMapper enterpriseEquipmentUseCustomMapper;
    /** 注入equipmentServiceUseCustomMapper的bean */
    @Autowired
    private EquipmentServiceUseCustomMapper equipmentServiceUseCustomMapper;

    /**
     * @description 查询运行使用list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseEquipmentUses和totalNum的result
     */
    @Override
    public JSONObject listEquipmentUses(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseEquipmentUseCustom> enterpriseEquipmentUses
                = enterpriseEquipmentUseCustomMapper.listEquipmentUses(condition);
        result.put("enterpriseEquipmentUses", enterpriseEquipmentUses);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseEquipmentUseCustom> pageInfo
                    = new PageInfo<EnterpriseEquipmentUseCustom>(enterpriseEquipmentUses);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询运行使用
     * @param id 运行使用id
     * @return com.lanware.management.pojo.EnterpriseEquipmentUses 返回EnterpriseEquipmentUses对象
     */
    @Override
    public EnterpriseEquipmentUse getEquipmentUseById(Long id) {
        return enterpriseEquipmentUseCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增运行使用
     * @param enterpriseEquipmentUse 运行使用pojo
     * @return int 插入成功的数目
     */
    @Override
    public int insertEquipmentUse(EnterpriseEquipmentUse enterpriseEquipmentUse) {
        return enterpriseEquipmentUseCustomMapper.insert(enterpriseEquipmentUse);
    }

    /**
     * @description 更新运行使用
     * @param enterpriseEquipmentUse 运行使用pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateEquipmentUse(EnterpriseEquipmentUse enterpriseEquipmentUse) {
        return enterpriseEquipmentUseCustomMapper.updateByPrimaryKeySelective(enterpriseEquipmentUse);
    }

    /**
     * @description 删除运行使用
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteEquipmentUse(Long id) {
        // 删除运行记录的运行设备
        EquipmentServiceUseExample equipmentServiceUseExample = new EquipmentServiceUseExample();
        EquipmentServiceUseExample.Criteria criteria = equipmentServiceUseExample.createCriteria();
        criteria.andRecordIdEqualTo(id);
        equipmentServiceUseCustomMapper.deleteByExample(equipmentServiceUseExample);
        // 删除运行记录
        return enterpriseEquipmentUseCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除运行使用
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteEquipmentUses(Long[] ids) {
        // 删除运行记录的运行设备
        EquipmentServiceUseExample equipmentServiceUseExample = new EquipmentServiceUseExample();
        EquipmentServiceUseExample.Criteria criteria = equipmentServiceUseExample.createCriteria();
        criteria.andRecordIdIn(Arrays.asList(ids));
        equipmentServiceUseCustomMapper.deleteByExample(equipmentServiceUseExample);
        // 删除运行记录
        EnterpriseEquipmentUseExample enterpriseEquipmentUseExample = new EnterpriseEquipmentUseExample();
        EnterpriseEquipmentUseExample.Criteria criteria1 = enterpriseEquipmentUseExample.createCriteria();
        criteria1.andIdIn(Arrays.asList(ids));
        return enterpriseEquipmentUseCustomMapper.deleteByExample(enterpriseEquipmentUseExample);
    }

    /**
     * @description 根据记录id获取运行设备
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listRunEquipmentsByRecordId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EquipmentServiceUseCustom> runEquipments
                = equipmentServiceUseCustomMapper.listEquipmentsByRecordId(condition);
        result.put("runEquipments", runEquipments);
        if (pageNum != null && pageSize != null) {
            PageInfo<EquipmentServiceUseCustom> pageInfo = new PageInfo<EquipmentServiceUseCustom>(runEquipments);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据记录id过滤尚未运行的设备
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listSelectRunEquipmentsByRecordId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EquipmentServiceUseCustom> selectRunEquipments
                = equipmentServiceUseCustomMapper.listSelectEquipmentsByRecordId(condition);
        result.put("selectRunEquipments", selectRunEquipments);
        if (pageNum != null && pageSize != null) {
            PageInfo<EquipmentServiceUseCustom> pageInfo = new PageInfo<EquipmentServiceUseCustom>(selectRunEquipments);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 批量插入运行的设备集合
     * @param equipmentServiceUseList 入运行的设备集合
     * @return int 插入成功的数目
     */
    @Override
    public int insertBatch(List<EquipmentServiceUse> equipmentServiceUseList) {
        return equipmentServiceUseCustomMapper.insertBatch(equipmentServiceUseList);
    }

    /**
     * @description 删除运行的设备
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    public int deleteRunEquipment(Long id) {
        return equipmentServiceUseCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除运行的设备
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteRunEquipments(Long[] ids) {
        EquipmentServiceUseExample equipmentServiceUseExample = new EquipmentServiceUseExample();
        EquipmentServiceUseExample.Criteria criteria = equipmentServiceUseExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return equipmentServiceUseCustomMapper.deleteByExample(equipmentServiceUseExample);
    }

    /**
     * @description 查询防护设备运行记录台账信息
     * @param condition 企业id
     * @return java.util.List<com.lanware.ehs.pojo.custom.EnterpriseEquipmentUseCustom>
     */
    @Override
    public List<EnterpriseEquipmentUseCustom> listEquipmentUseAccounts(Map<String, Object> condition) {
        return enterpriseEquipmentUseCustomMapper.listEquipmentUseAccounts(condition);
    }
}
