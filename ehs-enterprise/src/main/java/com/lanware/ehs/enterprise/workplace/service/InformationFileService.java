package com.lanware.ehs.enterprise.workplace.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 职业危害因素信息卡interface
 */
public interface InformationFileService {
    /**
     * @description 查询职业危害因素信息卡list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    JSONObject listInformationFiles(Long relationId, String relationModule);

    /**
     * @description 根据id查询职业危害因素信息卡
     * @param id
     * @return EnterpriseRelationFile
     */
    EnterpriseRelationFile getInformationFileById(Long id);

    /**
     * @description 新增职业危害因素信息卡，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param informationFile 职业危害因素信息卡文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertInformationFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile informationFile, Long enterpriseId);

    /**
     * @description 更新职业危害因素信息卡，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param informationFile 职业危害因素信息卡文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateInformationFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile informationFile, Long enterpriseId);

    /**
     * @description 删除职业危害因素信息卡
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteInformationFile(Long id, String filePath);

    /**
     * @description 批量删除职业危害因素信息卡
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteInformationFiles(Long[] ids);
}
