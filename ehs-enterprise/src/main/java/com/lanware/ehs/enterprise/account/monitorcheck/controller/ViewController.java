package com.lanware.ehs.enterprise.account.monitorcheck.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.check.service.CheckResultService;
import com.lanware.ehs.enterprise.check.service.SchematicDiagramService;
import com.lanware.ehs.pojo.EnterpriseCheckResultDetail;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.custom.EnterpriseOutsideCheckResultCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 职业危害日常监测、检测台帐视图
 */
@Controller("monitorCheckAccountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    @Autowired
    /** 注入schematicDiagramService的bean */
    private SchematicDiagramService schematicDiagramService;
    @Autowired
    /** 注入checkResultService的bean */
    private CheckResultService checkResultService;

    @RequestMapping("monitorCheck/viewSchematicDiagram/{id}")
    /**
     * @description 查看监测、检测点示意图
     * @param id 企业关联文件id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewSchematicDiagram(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseRelationFile enterpriseRelationFile = schematicDiagramService.getSchematicDiagramById(id);
        data.put("enterpriseRelationFile", enterpriseRelationFile);
        return new ModelAndView("account/monitorCheck/schematicDiagram", data);
    }

    /**
     * @description 返回检测结果页面
     * @param outsideCheckId 外部检测id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorCheck/checkResultAccount/{outsideCheckId}")
    public ModelAndView checkResultAccount(@PathVariable Long outsideCheckId){
        JSONObject data = new JSONObject();
        data.put("outsideCheckId", outsideCheckId);
        List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResultCustomList =
                checkResultService.getHarmFactorIdsByOutsideCheckId(outsideCheckId);
        data.put("checkResultList", enterpriseOutsideCheckResultCustomList);
        return new ModelAndView("account/monitorCheck/checkResult", data);
    }

    /**
     * @description 返回检测结果详细页面
     * @param checkResultId 检测结果id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorCheck/checkResultAccount/detail/{checkResultId}")
    public ModelAndView detail(@PathVariable Long checkResultId){
        JSONObject data = new JSONObject();
        data.put("checkResultId", checkResultId);
        return new ModelAndView("account/monitorCheck/checkResultDetail", data);
    }

    /**
     * @description 返回查看检测结果详细页面
     * @param id 检测结果详细id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("monitorCheck/checkResultAccount/detail/view/{id}")
    public ModelAndView viewDetail(@PathVariable Long id){
        JSONObject data = new JSONObject();
        EnterpriseCheckResultDetail enterpriseCheckResultDetail = checkResultService.getCheckResultDetailById(id);
        data.put("enterpriseCheckResultDetail", enterpriseCheckResultDetail);
        return new ModelAndView("account/monitorCheck/checkResultDetailView", data);
    }
}
