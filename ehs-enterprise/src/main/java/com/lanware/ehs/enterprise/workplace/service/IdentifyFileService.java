package com.lanware.ehs.enterprise.workplace.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 职业病危害警示interface
 */
public interface IdentifyFileService {
    /**
     * @description 查询职业病危害警示list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    JSONObject listIdentifyFiles(Long relationId, String relationModule);

    /**
     * @description 根据id查询职业病危害警示
     * @param id
     * @return EnterpriseRelationFile
     */
    EnterpriseRelationFile getIdentifyFileById(Long id);

    /**
     * @description 新增职业病危害警示，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param identifyFile 职业病危害警示文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertIdentifyFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile identifyFile, Long enterpriseId);

    /**
     * @description 更新职业病危害警示，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param identifyFile 职业病危害警示文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateIdentifyFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile identifyFile, Long enterpriseId);

    /**
     * @description 删除职业病危害警示
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteIdentifyFile(Long id, String filePath);

    /**
     * @description 批量删除职业病危害警示
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteIdentifyFiles(Long[] ids);
}
