package com.lanware.ehs.enterprise.workplace.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.workplace.service.WorkplaceService;
import com.lanware.ehs.mapper.EnterpriseWorkplaceMapper;
import com.lanware.ehs.mapper.EnterpriseWorkplaceWaitNotifyMapper;
import com.lanware.ehs.mapper.custom.EnterpriseRelationFileCustomMapper;
import com.lanware.ehs.mapper.custom.EnterpriseWorkplaceCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseRelationFileCustom;
import com.lanware.ehs.pojo.custom.EnterpriseWorkplaceCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
/**
 * @description 作业场所管理interface实现类
 */
public class WorkplaceServiceImpl implements WorkplaceService {
    @Autowired
    /** 注入enterpriseWorkplaceMapper的bean */
    private EnterpriseWorkplaceMapper enterpriseWorkplaceMapper;
    @Autowired
    /** 注入enterpriseWorkplaceCustomMapper的bean */
    private EnterpriseWorkplaceCustomMapper enterpriseWorkplaceCustomMapper;
    @Autowired
    /** 注入enterpriseWorkplaceWaitNotifyMapper的bean */
    private EnterpriseWorkplaceWaitNotifyMapper enterpriseWorkplaceWaitNotifyMapper;
    /** 注入enterpriseRelationFileCustomMapper的bean */
    @Autowired
    private EnterpriseRelationFileCustomMapper enterpriseRelationFileCustomMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    @Override
    /**
     * @description 查询作业场所list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces和totalNum的result
     */
    public JSONObject listWorkplacesByEnterpriseId(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseWorkplaceCustom> enterpriseWorkplaces = enterpriseWorkplaceCustomMapper.listWorkplaces(condition);
        // 查询数据和分页数据装进result
        result.put("enterpriseWorkplaces", enterpriseWorkplaces);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseWorkplaceCustom> pageInfo = new PageInfo<EnterpriseWorkplaceCustom>(enterpriseWorkplaces);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 查询有职业病危害因素的作业场所list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseWorkplaces和totalNum的result
     */
    @Override
    public JSONObject listHarmWorkplaces(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseWorkplaceCustom> enterpriseWorkplaces
                = enterpriseWorkplaceCustomMapper.listHarmWorkplaces(condition);
        // 查询数据和分页数据装进result
        result.put("enterpriseWorkplaces", enterpriseWorkplaces);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseWorkplaceCustom> pageInfo = new PageInfo<EnterpriseWorkplaceCustom>(enterpriseWorkplaces);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    @Override
    /**
     * @description 根据id查询作业场所信息
     * @param id 作业场所id
     * @return com.lanware.management.pojo.EnterpriseWorkplace 返回EnterpriseWorkplace对象
     */
    public EnterpriseWorkplace getWorkplaceById(Long id) {
        return enterpriseWorkplaceMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增工作场所，并在工作场所待告知中新增记录
     * @param enterpriseWorkplace 工作场所pojo
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertWorkplace(EnterpriseWorkplace enterpriseWorkplace) {
        return enterpriseWorkplaceMapper.insert(enterpriseWorkplace);
    }

    /**
     * @description 更新工作场所
     * @param enterpriseWorkplace 工作场所pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateWorkplace(EnterpriseWorkplace enterpriseWorkplace) {
        return enterpriseWorkplaceMapper.updateByPrimaryKeySelective(enterpriseWorkplace);
    }

    /**
     * @description 删除作业场所信息，并在工作场所待告知中删除记录
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteWorkplace(Long id) {
        // 删除oss服务器上对应的文件(工作场所上传的3种文件)
        List<EnterpriseRelationFileCustom> enterpriseRelationFiles
                = enterpriseRelationFileCustomMapper.listWorkplaceRelationFilesByRealtionId(id);
        for (EnterpriseRelationFileCustom enterpriseRelationFileCustom : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFileCustom.getFilePath());
        }
        // 删除数据库中工作场所关联文件的信息
        enterpriseRelationFileCustomMapper.deleteWorkplaceRelationFilesByRealtionId(id);
        // 删除工作场所信息
        int num = enterpriseWorkplaceMapper.deleteByPrimaryKey(id);
        // 删除作业场所待告知记录
        EnterpriseWorkplaceWaitNotifyExample enterpriseWorkplaceWaitNotifyExample
                = new EnterpriseWorkplaceWaitNotifyExample();
        EnterpriseWorkplaceWaitNotifyExample.Criteria criteria = enterpriseWorkplaceWaitNotifyExample.createCriteria();
        criteria.andWorkplaceIdEqualTo(id);
        enterpriseWorkplaceWaitNotifyMapper.deleteByExample(enterpriseWorkplaceWaitNotifyExample);
        return num;
    }

    /**
     * @description 批量删除作业场所信息，并在工作场所待告知中删除记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteWorkplaces(Long[] ids) {
        // 批量删除oss服务器上对应的文件(工作场所上传的3种文件)
        List<EnterpriseRelationFileCustom> enterpriseRelationFiles
                = enterpriseRelationFileCustomMapper.listWorkplaceRelationFilesByRealtionIds(ids);
        for (EnterpriseRelationFileCustom enterpriseRelationFileCustom : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFileCustom.getFilePath());
        }
        // 批量删除数据库中工作场所关联文件的信息
        enterpriseRelationFileCustomMapper.deleteWorkplaceRelationFilesByRealtionIds(ids);
        // 批量删除作业场所信息
        EnterpriseWorkplaceExample enterpriseWorkplaceExample = new EnterpriseWorkplaceExample();
        EnterpriseWorkplaceExample.Criteria criteria = enterpriseWorkplaceExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        int num = enterpriseWorkplaceMapper.deleteByExample(enterpriseWorkplaceExample);
        //  删除作业场所待告知记录
        EnterpriseWorkplaceWaitNotifyExample enterpriseWorkplaceWaitNotifyExample
                = new EnterpriseWorkplaceWaitNotifyExample();
        EnterpriseWorkplaceWaitNotifyExample.Criteria criteria1 = enterpriseWorkplaceWaitNotifyExample.createCriteria();
        criteria1.andWorkplaceIdIn(Arrays.asList(ids));
        enterpriseWorkplaceWaitNotifyMapper.deleteByExample(enterpriseWorkplaceWaitNotifyExample);
        return num;
    }
}
