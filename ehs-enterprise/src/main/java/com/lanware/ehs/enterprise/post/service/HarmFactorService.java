package com.lanware.ehs.enterprise.post.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.pojo.custom.PostHarmFactorCustom;

import java.util.Map;

public interface HarmFactorService {

    /**
     * 查询危害因素
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<PostHarmFactorCustom> queryPostHarmFactorByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 查询未选择的危害因素
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<PostHarmFactorCustom> queryUnSelectedPostHarmFactorByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 查询在岗涉害人数list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listTouchHarmNum(Integer pageNum, Integer pageSize, Map<String, Object> condition);
}
