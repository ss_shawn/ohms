package com.lanware.ehs.enterprise.account.monitorcheck.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.check.service.*;
import com.lanware.ehs.enterprise.inspectioninstitution.service.InspectionInstitutionService;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 检测台账controller
 */
@Controller
@RequestMapping("/monitorCheckAccount")
public class MonitorCheckAccountController {
    @Autowired
    /** 注入insideMonitorService的bean */
    private InsideMonitorService insideMonitorService;
    @Autowired
    /** 注入outsideCheckService的bean */
    private OutsideCheckService outsideCheckService;
    @Autowired
    /** 注入checkResultService的bean */
    private CheckResultService checkResultService;
    /** 注入inspectionInstitutionService的bean */
    @Autowired
    private InspectionInstitutionService inspectionInstitutionService;
    /** 注入waitCheckService的bean */
    @Autowired
    private WaitCheckService waitCheckService;
    /**  注入schematicDiagramService的bean */
    @Autowired
    private SchematicDiagramService schematicDiagramService;

    /** 下载word的临时路径 */
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;

    /**
     * 检测台账
     * @param enterpriseUser 当前登录企业
     * @return ModelAndView
     */
    @RequestMapping("")
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/monitorCheck/list", data);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(MonitorCheckAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 日常监测记录word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadInsideMonitor")
    @ResponseBody
    public ResultFormat downloadInsideMonitor(Long enterpriseId,String inside_monitor_keyword, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 日常监测记录
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        condition.put("keyword",inside_monitor_keyword);
        JSONObject result = insideMonitorService.listInsideMonitors(0, 0, condition);
        List<EnterpriseInsideMonitorCustom> enterpriseInsideMonitors
                = (List<EnterpriseInsideMonitorCustom>)result.get("enterpriseInsideMonitors");
        List insideMonitorList = new ArrayList();
        for (EnterpriseInsideMonitorCustom enterpriseInsideMonitorCustom : enterpriseInsideMonitors) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("monitorDate", sdf.format(enterpriseInsideMonitorCustom.getMonitorDate()));
            temp.put("monitorPlaceName", enterpriseInsideMonitorCustom.getMonitorPointName());
            temp.put("workplaceName", enterpriseInsideMonitorCustom.getWorkplaceName());
            temp.put("harmFactorName",enterpriseInsideMonitorCustom.getHarmFactorName() == null ? "无"
                    : enterpriseInsideMonitorCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
            temp.put("limitValue", enterpriseInsideMonitorCustom.getLimitValue());
            temp.put("monitorPeriod", enterpriseInsideMonitorCustom.getMonitorPeriod());
            temp.put("monitorResult", enterpriseInsideMonitorCustom.getMonitorResult());
            insideMonitorList.add(temp);
        }
        dataMap.put("insideMonitorList", insideMonitorList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(MonitorCheckAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("insideMonitorMould.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "日常监测记录.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("日常监测记录.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 危害因素检测记录zip下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadOutsideCheck")
    @ResponseBody
    public ResultFormat downloadOutsideCheck(Long enterpriseId,String outside_check_keyword, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // 创建配置实例
            Configuration configuration = new Configuration();
            // 设置编码
            configuration.setDefaultEncoding("UTF-8");
            // ftl模板文件
            configuration.setClassForTemplateLoading(MonitorCheckAccountController.class,"/templates/ftl");
            // 返回word的变量
            Map<String, Object> dataMap = new HashMap<String, Object>();
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 查询危害因素检测记录(检测日期，机构，检测危害因素)
            Map<String, Object> condition = new HashMap<>();
            condition.put("enterpriseId", enterpriseId);
            condition.put("keyword",outside_check_keyword);
            JSONObject result = outsideCheckService.listOutsideChecks(0, 0, condition);
            List<EnterpriseOutsideCheckCustom> enterpriseOutsideChecks
                    = (List<EnterpriseOutsideCheckCustom>)result.get("enterpriseOutsideChecks");
            List outsideCheckList = new ArrayList();
            for (int i = 0; i < enterpriseOutsideChecks.size(); i++) {
                EnterpriseOutsideCheckCustom enterpriseOutsideCheckCustom = enterpriseOutsideChecks.get(i);
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("checkDate", sdf.format(enterpriseOutsideCheckCustom.getCheckDate()));
                temp.put("checkOrganizationName", enterpriseOutsideCheckCustom.getCheckOrganizationName());
                //检测危害因素
                List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResults
                        = checkResultService.listHarmFactorsByOutsideCheckId(enterpriseOutsideCheckCustom.getId());
                List outsideCheckResultList = new ArrayList();
                for (EnterpriseOutsideCheckResultCustom enterpriseOutsideCheckResultCustom : enterpriseOutsideCheckResults) {
                    Map<String, Object> temp1 = new HashMap<String, Object>();
                    temp1.put("harmFactorName", enterpriseOutsideCheckResultCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                    temp1.put("mac", enterpriseOutsideCheckResultCustom.getMac());
                    temp1.put("twa", enterpriseOutsideCheckResultCustom.getTwa());
                    temp1.put("stel", enterpriseOutsideCheckResultCustom.getStel());
                    temp1.put("overLimitMultiple", enterpriseOutsideCheckResultCustom.getOverLimitMultiple());
                    outsideCheckResultList.add(temp1);
                }
                temp.put("outsideCheckResultList", outsideCheckResultList);

                outsideCheckList.add(temp);
            }
            dataMap.put("outsideCheckList", outsideCheckList);
            // 获取模板
            Template template = configuration.getTemplate("outsideCheckRecord.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "危害因素检测记录.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            //放入压缩列表
            files.add(uploadPath);


            // 查询危害因素检测结果详细
            for (EnterpriseOutsideCheckCustom enterpriseOutsideCheckCustom : enterpriseOutsideChecks) {
                if (enterpriseOutsideCheckCustom.getCheckResultNum() > 0) {
                    List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResults
                            = checkResultService.getHarmFactorIdsByOutsideCheckId(enterpriseOutsideCheckCustom.getId());
                    for (EnterpriseOutsideCheckResultCustom enterpriseOutsideCheckResultCustom : enterpriseOutsideCheckResults) {
                        Map<String, Object> dataMap2 = new HashMap<String, Object>();
                        Map<String, Object> condition2 = new HashMap<>();
                        condition2.put("outsideCheckId", enterpriseOutsideCheckCustom.getId());
                        condition2.put("harmFactorId", enterpriseOutsideCheckResultCustom.getHarmFactorId());
                        condition2.put("enterpriseId", enterpriseId);
                        List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResultCustoms =
                                checkResultService.listCheckResultCustoms(condition2);
                        List outsideCheckResultDetailList = new ArrayList();
                        for (EnterpriseOutsideCheckResultCustom enterpriseOutsideCheckResultCustom1 : enterpriseOutsideCheckResultCustoms) {
                            Map<String, Object> temp2 = new HashMap<String, Object>();
                            temp2.put("workplaceName", enterpriseOutsideCheckResultCustom1.getWorkplaceName());
                            temp2.put("monitorPointName", enterpriseOutsideCheckResultCustom1.getMonitorPointName());
                            temp2.put("postName", enterpriseOutsideCheckResultCustom1.getPostName());
                            temp2.put("isQualified", enterpriseOutsideCheckResultCustom1.getIsQualified());
                            temp2.put("checkResultDetail", enterpriseOutsideCheckResultCustom1.getCheckResultDetail());
                            outsideCheckResultDetailList.add(temp2);
                        }

                        // 获取危害因素检测记录的日期,机构,检测危害因素
                        String checkDate = sdf.format(enterpriseOutsideCheckCustom.getCheckDate());
                        String checkOrganizationName = enterpriseOutsideCheckCustom.getCheckOrganizationName();
                        String harmFactorName = enterpriseOutsideCheckResultCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;");
                        dataMap2.put("harmFactorName", harmFactorName);
                        dataMap2.put("checkDate", checkDate);
                        dataMap2.put("checkOrganizationName", checkOrganizationName);
                        dataMap2.put("outsideCheckResultDetailList", outsideCheckResultDetailList);
                        // 获取模板
                        Template template2 = configuration.getTemplate("outsideCheckResultDetail.ftl");
                        // 输出文件
                        String uploadPath2 = upload + File.separator + checkDate + checkOrganizationName + harmFactorName + "危害因素检测结果详细.doc";
                        File outFile2 = new File(uploadPath2);
                        // 如果输出目标文件夹不存在，则创建
                        if (!outFile2.getParentFile().exists()){
                            outFile2.getParentFile().mkdirs();
                        }
                        // 将模板和数据模型合并生成文件
                        Writer out2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile2),"UTF-8"));
                        // 生成文件
                        template2.process(dataMap2, out2);
                        // 关闭流
                        out2.flush();
                        out2.close();
                        //放入压缩列表
                        files.add(uploadPath2);
                    }
                }
            }
            String uploadZipPath = upload + File.separator + "危害因素检测记录.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            File uploadZipFile = new File(uploadZipPath);
            FileInputStream fis = new FileInputStream(uploadZipFile);
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("危害因素检测记录.zip", "utf-8"));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = fis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            fis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 检测机构word下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadCheckInstitution")
    @ResponseBody
    public ResultFormat downloadCheckInstitution(Long enterpriseId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 检测机构
        JSONObject result = inspectionInstitutionService.listInspectionInstitutionsByType("检测机构", enterpriseId);
        List<EnterpriseInspectionInstitution> enterpriseInspectionInstitutions
                = (List<EnterpriseInspectionInstitution>)result.get("enterpriseInspectionInstitutions");
        List checkInstitutionList = new ArrayList();
        for (EnterpriseInspectionInstitution enterpriseInspectionInstitution : enterpriseInspectionInstitutions) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name",enterpriseInspectionInstitution.getName());
            temp.put("contacts",enterpriseInspectionInstitution.getContacts());
            temp.put("mobilePhone",enterpriseInspectionInstitution.getMobilePhone());
            checkInstitutionList.add(temp);
        }
        dataMap.put("checkInstitutionList", checkInstitutionList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(MonitorCheckAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("checkInstitution.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "检测机构.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("检测机构.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }


    /**
     * @description 工作场所监测结果汇总word下载
     * @param enterpriseId 当前登录企业id
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadWaitCheckAccount")
    @ResponseBody
    public ResultFormat downloadWaitCheckAccount(Long enterpriseId, String startDate, String endDate, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, enterpriseId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 待检测项目
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        if (startDate != null && !startDate.equals("")) {
            condition.put("startDate", startDate);
        }
        if (endDate != null && !endDate.equals("")) {
            condition.put("endDate", endDate);
        }
        JSONObject result = waitCheckService.listWaitCheckAccounts(0,0,condition);
        List<EnterpriseWaitCheckCustom> enterpriseWaitCheckCustoms
                = (List<EnterpriseWaitCheckCustom>)result.get("enterpriseWaitChecks");
        List waitCheckList = new ArrayList();
        for (EnterpriseWaitCheckCustom enterpriseWaitCheckCustom : enterpriseWaitCheckCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("harmFactorName", enterpriseWaitCheckCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
            temp.put("mac", enterpriseWaitCheckCustom.getMac());
            temp.put("twa", enterpriseWaitCheckCustom.getTwa());
            temp.put("stel", enterpriseWaitCheckCustom.getStel());
            temp.put("overLimitMultiple", enterpriseWaitCheckCustom.getOverLimitMultiple());
            temp.put("remark", enterpriseWaitCheckCustom.getRemark());

            temp.put("postShouldCheckNum", enterpriseWaitCheckCustom.getPostShouldCheckNum());
            temp.put("postActualCheckNum", enterpriseWaitCheckCustom.getPostActualCheckNum());
            String postCheckRate = "100";
            if (enterpriseWaitCheckCustom.getPostShouldCheckNum() > 0) {
                postCheckRate = 100 * enterpriseWaitCheckCustom.getPostActualCheckNum() / enterpriseWaitCheckCustom.getPostShouldCheckNum() + "";
            }
            temp.put("postCheckRate", postCheckRate);
            temp.put("postQualifiedNum", enterpriseWaitCheckCustom.getPostQualifiedNum());
            String postQualifiedRate = "100";
            if (enterpriseWaitCheckCustom.getPostActualCheckNum() > 0) {
                postQualifiedRate = 100 * enterpriseWaitCheckCustom.getPostQualifiedNum() / enterpriseWaitCheckCustom.getPostActualCheckNum() + "";
            }
            temp.put("postQualifiedRate", postQualifiedRate);

            // 监测点应测数目 = 实测 + 未测
            Integer pointShouldCheckNum = enterpriseWaitCheckCustom.getPointActualCheckNum() + enterpriseWaitCheckCustom.getPointNotCheckNum();
            temp.put("pointShouldCheckNum", pointShouldCheckNum);
            temp.put("pointActualCheckNum", enterpriseWaitCheckCustom.getPointActualCheckNum());
            String pointCheckRate = "100";
            if (pointShouldCheckNum > 0) {
                pointCheckRate = 100 * enterpriseWaitCheckCustom.getPointActualCheckNum() / pointShouldCheckNum + "";
            }
            temp.put("pointCheckRate", pointCheckRate);
            temp.put("pointQualifiedNum", enterpriseWaitCheckCustom.getPointQualifiedNum());
            String pointQualifiedRate = "100";
            if (enterpriseWaitCheckCustom.getPointActualCheckNum() > 0) {
                pointQualifiedRate = 100 * enterpriseWaitCheckCustom.getPointQualifiedNum() / enterpriseWaitCheckCustom.getPointActualCheckNum() + "";
            }
            temp.put("pointQualifiedRate", pointQualifiedRate);

            waitCheckList.add(temp);
        }
        dataMap.put("waitCheckList", waitCheckList);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(MonitorCheckAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("waitCheck.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "工作场所监测结果汇总表.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("工作场所监测结果汇总表.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }


    /**
     * @description 检测台账zip下载
     * @param enterpriseId 当前登录企业id
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadMonitorCheck")
    @ResponseBody
    public ResultFormat downloadMonitorCheck(Long enterpriseId, String startDate, String endDate,String inside_monitor_keyword,String outside_check_keyword,  HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // 创建配置实例
            Configuration configuration = new Configuration();
            // 设置编码
            configuration.setDefaultEncoding("UTF-8");
            // ftl模板文件
            configuration.setClassForTemplateLoading(MonitorCheckAccountController.class,"/templates/ftl");
            // 返回word的变量
            Map<String, Object> dataMap = new HashMap<String, Object>();
            Map<String, Object> dataMap2 = new HashMap<String, Object>();
            Map<String, Object> dataMap3 = new HashMap<String, Object>();
            Map<String, Object> dataMap4 = new HashMap<String, Object>();
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator);
            if (!upload.exists()) upload.mkdirs();

            // 查询危害因素检测记录(检测日期，机构，检测危害因素)
            Map<String, Object> condition = new HashMap<>();
            condition.put("enterpriseId", enterpriseId);
            condition.put("keyword",outside_check_keyword);
            JSONObject result = outsideCheckService.listOutsideChecks(0, 0, condition);
            List<EnterpriseOutsideCheckCustom> enterpriseOutsideChecks
                    = (List<EnterpriseOutsideCheckCustom>)result.get("enterpriseOutsideChecks");
            List outsideCheckList = new ArrayList();
            for (int i = 0; i < enterpriseOutsideChecks.size(); i++) {
                EnterpriseOutsideCheckCustom enterpriseOutsideCheckCustom = enterpriseOutsideChecks.get(i);
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("checkDate", sdf.format(enterpriseOutsideCheckCustom.getCheckDate()));
                temp.put("checkOrganizationName", enterpriseOutsideCheckCustom.getCheckOrganizationName());

                Map<String, Object> condition1 = new HashMap<>();
                List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResults
                        = checkResultService.listHarmFactorsByOutsideCheckId(enterpriseOutsideCheckCustom.getId());
                List outsideCheckResultList = new ArrayList();
                for (EnterpriseOutsideCheckResultCustom enterpriseOutsideCheckResultCustom : enterpriseOutsideCheckResults) {
                    Map<String, Object> temp1 = new HashMap<String, Object>();
                    temp1.put("harmFactorName", enterpriseOutsideCheckResultCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                    temp1.put("mac", enterpriseOutsideCheckResultCustom.getMac());
                    temp1.put("twa", enterpriseOutsideCheckResultCustom.getTwa());
                    temp1.put("stel", enterpriseOutsideCheckResultCustom.getStel());
                    temp1.put("overLimitMultiple", enterpriseOutsideCheckResultCustom.getOverLimitMultiple());
                    outsideCheckResultList.add(temp1);
                }
                temp.put("outsideCheckResultList", outsideCheckResultList);
                outsideCheckList.add(temp);
            }
            dataMap.put("outsideCheckList", outsideCheckList);
            // 获取模板
            Template template = configuration.getTemplate("outsideCheckRecord.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "危害因素检测记录.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            //放入压缩列表
            files.add(uploadPath);

            // 查询危害因素检测结果详细
            for (EnterpriseOutsideCheckCustom enterpriseOutsideCheckCustom : enterpriseOutsideChecks) {
                if (enterpriseOutsideCheckCustom.getCheckResultNum() > 0) {
                    List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResults
                            = checkResultService.getHarmFactorIdsByOutsideCheckId(enterpriseOutsideCheckCustom.getId());
                    for (EnterpriseOutsideCheckResultCustom enterpriseOutsideCheckResultCustom : enterpriseOutsideCheckResults) {
                        Map<String, Object> dataMap1 = new HashMap<String, Object>();
                        Map<String, Object> condition1 = new HashMap<>();
                        condition1.put("outsideCheckId", enterpriseOutsideCheckCustom.getId());
                        condition1.put("harmFactorId", enterpriseOutsideCheckResultCustom.getHarmFactorId());
                        condition1.put("enterpriseId", enterpriseId);
                        List<EnterpriseOutsideCheckResultCustom> enterpriseOutsideCheckResultCustoms =
                                checkResultService.listCheckResultCustoms(condition1);
                        List outsideCheckResultDetailList = new ArrayList();
                        for (EnterpriseOutsideCheckResultCustom enterpriseOutsideCheckResultCustom1 : enterpriseOutsideCheckResultCustoms) {
                            Map<String, Object> temp1 = new HashMap<String, Object>();
                            temp1.put("workplaceName", enterpriseOutsideCheckResultCustom1.getWorkplaceName());
                            temp1.put("monitorPointName", enterpriseOutsideCheckResultCustom1.getMonitorPointName());
                            temp1.put("postName", enterpriseOutsideCheckResultCustom1.getPostName());
                            temp1.put("isQualified", enterpriseOutsideCheckResultCustom1.getIsQualified());
                            temp1.put("checkResultDetail", enterpriseOutsideCheckResultCustom1.getCheckResultDetail());
                            outsideCheckResultDetailList.add(temp1);
                        }

                        // 获取危害因素检测记录的日期,机构,检测危害因素
                        String checkDate = sdf.format(enterpriseOutsideCheckCustom.getCheckDate());
                        String checkOrganizationName = enterpriseOutsideCheckCustom.getCheckOrganizationName();
                        String harmFactorName = enterpriseOutsideCheckResultCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;");
                        dataMap1.put("harmFactorName", harmFactorName);
                        dataMap1.put("checkDate", checkDate);
                        dataMap1.put("checkOrganizationName", checkOrganizationName);
                        dataMap1.put("outsideCheckResultDetailList", outsideCheckResultDetailList);
                        // 获取模板
                        Template template1 = configuration.getTemplate("outsideCheckResultDetail.ftl");
                        // 输出文件
                        String uploadPath1 = upload + File.separator + checkDate + checkOrganizationName + harmFactorName + "危害因素检测结果详细.doc";
                        File outFile1 = new File(uploadPath1);
                        // 如果输出目标文件夹不存在，则创建
                        if (!outFile1.getParentFile().exists()){
                            outFile1.getParentFile().mkdirs();
                        }
                        // 将模板和数据模型合并生成文件
                        Writer out1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile1),"UTF-8"));
                        // 生成文件
                        template1.process(dataMap1, out1);
                        // 关闭流
                        out1.flush();
                        out1.close();
                        //放入压缩列表
                        files.add(uploadPath1);
                    }
                }
            }

            // 日常监测记录
            condition.put("keyword",inside_monitor_keyword);
            JSONObject result2 = insideMonitorService.listInsideMonitors(0, 0, condition);
            List<EnterpriseInsideMonitorCustom> enterpriseInsideMonitors
                    = (List<EnterpriseInsideMonitorCustom>)result2.get("enterpriseInsideMonitors");
            List insideMonitorList = new ArrayList();
            for (EnterpriseInsideMonitorCustom enterpriseInsideMonitorCustom : enterpriseInsideMonitors) {
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("monitorDate", sdf.format(enterpriseInsideMonitorCustom.getMonitorDate()));
                temp.put("monitorPlaceName", enterpriseInsideMonitorCustom.getMonitorPointName());
                temp.put("workplaceName", enterpriseInsideMonitorCustom.getWorkplaceName());
                temp.put("harmFactorName",enterpriseInsideMonitorCustom.getHarmFactorName() == null ? "无"
                        : enterpriseInsideMonitorCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                temp.put("limitValue", enterpriseInsideMonitorCustom.getLimitValue());
                temp.put("monitorPeriod", enterpriseInsideMonitorCustom.getMonitorPeriod());
                temp.put("monitorResult", enterpriseInsideMonitorCustom.getMonitorResult());
                insideMonitorList.add(temp);
            }
            dataMap2.put("insideMonitorList", insideMonitorList);
            // 获取模板
            Template template2 = configuration.getTemplate("insideMonitorMould.ftl");
            // 输出文件
            String uploadPath2 = upload + File.separator + "日常监测记录.doc";
            File outFile2 = new File(uploadPath2);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile2.getParentFile().exists()){
                outFile2.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile2),"UTF-8"));
            // 生成文件
            template2.process(dataMap2, out2);
            // 关闭流
            out2.flush();
            out2.close();
            //放入压缩列表
            files.add(uploadPath2);

            // 检测机构
            JSONObject result3 = inspectionInstitutionService.listInspectionInstitutionsByType("检测机构", enterpriseId);
            List<EnterpriseInspectionInstitution> enterpriseInspectionInstitutions
                    = (List<EnterpriseInspectionInstitution>)result3.get("enterpriseInspectionInstitutions");
            List checkInstitutionList = new ArrayList();
            for (EnterpriseInspectionInstitution enterpriseInspectionInstitution : enterpriseInspectionInstitutions) {
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("name",enterpriseInspectionInstitution.getName());
                temp.put("contacts",enterpriseInspectionInstitution.getContacts());
                temp.put("mobilePhone",enterpriseInspectionInstitution.getMobilePhone());
                checkInstitutionList.add(temp);
            }
            dataMap3.put("checkInstitutionList", checkInstitutionList);
            // 获取模板
            Template template3 = configuration.getTemplate("checkInstitution.ftl");
            // 输出文件
            String uploadPath3 = upload + File.separator + "检测机构.doc";
            File outFile3 = new File(uploadPath3);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile3.getParentFile().exists()){
                outFile3.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out3 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile3),"UTF-8"));
            // 生成文件
            template3.process(dataMap3, out3);
            // 关闭流
            out3.flush();
            out3.close();
            //放入压缩列表
            files.add(uploadPath3);

            // 待检测项目
            if (startDate != null && !startDate.equals("")) {
                condition.put("startDate", startDate);
            }
            if (endDate != null && !endDate.equals("")) {
                condition.put("endDate", endDate);
            }
            JSONObject result4 = waitCheckService.listWaitCheckAccounts(0,0,condition);
            List<EnterpriseWaitCheckCustom> enterpriseWaitCheckCustoms
                    = (List<EnterpriseWaitCheckCustom>)result4.get("enterpriseWaitChecks");
            List waitCheckList = new ArrayList();
            for (EnterpriseWaitCheckCustom enterpriseWaitCheckCustom : enterpriseWaitCheckCustoms) {
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("harmFactorName", enterpriseWaitCheckCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                temp.put("mac", enterpriseWaitCheckCustom.getMac());
                temp.put("twa", enterpriseWaitCheckCustom.getTwa());
                temp.put("stel", enterpriseWaitCheckCustom.getStel());
                temp.put("overLimitMultiple", enterpriseWaitCheckCustom.getOverLimitMultiple());
                temp.put("remark", enterpriseWaitCheckCustom.getRemark());

                temp.put("postShouldCheckNum", enterpriseWaitCheckCustom.getPostShouldCheckNum());
                temp.put("postActualCheckNum", enterpriseWaitCheckCustom.getPostActualCheckNum());
                String postCheckRate = "100";
                if (enterpriseWaitCheckCustom.getPostShouldCheckNum() > 0) {
                    postCheckRate = 100 * enterpriseWaitCheckCustom.getPostActualCheckNum() / enterpriseWaitCheckCustom.getPostShouldCheckNum() + "";
                }
                temp.put("postCheckRate", postCheckRate);
                temp.put("postQualifiedNum", enterpriseWaitCheckCustom.getPostQualifiedNum());
                String postQualifiedRate = "100";
                if (enterpriseWaitCheckCustom.getPostActualCheckNum() > 0) {
                    postQualifiedRate = 100 * enterpriseWaitCheckCustom.getPostQualifiedNum() / enterpriseWaitCheckCustom.getPostActualCheckNum() + "";
                }
                temp.put("postQualifiedRate", postQualifiedRate);

                // 监测点应测数目 = 实测 + 未测
                Integer pointShouldCheckNum = enterpriseWaitCheckCustom.getPointActualCheckNum() + enterpriseWaitCheckCustom.getPointNotCheckNum();
                temp.put("pointShouldCheckNum", pointShouldCheckNum);
                temp.put("pointActualCheckNum", enterpriseWaitCheckCustom.getPointActualCheckNum());
                String pointCheckRate = "100";
                if (pointShouldCheckNum > 0) {
                    pointCheckRate = 100 * enterpriseWaitCheckCustom.getPointActualCheckNum() / pointShouldCheckNum + "";
                }
                temp.put("pointCheckRate", pointCheckRate);
                temp.put("pointQualifiedNum", enterpriseWaitCheckCustom.getPointQualifiedNum());
                String pointQualifiedRate = "100";
                if (enterpriseWaitCheckCustom.getPointActualCheckNum() > 0) {
                    pointQualifiedRate = 100 * enterpriseWaitCheckCustom.getPointQualifiedNum() / enterpriseWaitCheckCustom.getPointActualCheckNum() + "";
                }
                temp.put("pointQualifiedRate", pointQualifiedRate);

                waitCheckList.add(temp);
            }
            dataMap4.put("waitCheckList", waitCheckList);
            Template template4 = configuration.getTemplate("waitCheck.ftl");
            // 输出文件
            String uploadPath4 = upload + File.separator + "工作场所监测结果汇总表.doc";
            File outFile4 = new File(uploadPath4);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile4.getParentFile().exists()){
                outFile4.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out4 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile4),"UTF-8"));
            // 生成文件
            template4.process(dataMap4, out4);
            // 关闭流
            out4.flush();
            out4.close();
            //放入压缩列表
            files.add(uploadPath4);

            String uploadZipPath = upload + File.separator + "检测台账.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            File uploadZipFile = new File(uploadZipPath);
            FileInputStream fis = new FileInputStream(uploadZipFile);
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("检测台账.zip", "utf-8"));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = fis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            fis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 检测台账文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadMonitorCheckAccount")
    @ResponseBody
    public void downloadMonitorCheckAccount(Long enterpriseId,String inside_monitor_keyword,String outside_check_keyword, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            Map<String, Object> dataMap = getMonitorCheckAccountMap(enterpriseId,request,inside_monitor_keyword,outside_check_keyword);
            String uploadPath = upload + File.separator + "职业危害日常监测、检测台帐.doc";
            exeWord(dataMap,uploadPath,"monitorCheckAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业危害日常监测、检测台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到检测台账文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getMonitorCheckAccountMap(long enterpriseId, QueryRequest request,String inside_monitor_keyword,String outside_check_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("relationId", enterpriseId);
        // 职业病危害因素监测、检测点分布示意图
        condition.put("relationModule", "监测、检测点示意图");
        JSONObject result = schematicDiagramService.listSchematicDiagrams(0, 0, condition);
        List<EnterpriseRelationFileCustom> enterpriseRelationFiles
                = (List<EnterpriseRelationFileCustom>)result.get("enterpriseRelationFiles");
        List schematicDiagramList = new ArrayList();
        for (EnterpriseRelationFileCustom enterpriseRelationFileCustom : enterpriseRelationFiles) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("description", enterpriseRelationFileCustom.getDescription());
            String filePath = enterpriseRelationFileCustom.getFilePath();
            temp.put("filePath", (filePath == null || filePath.equals("")) ? "-" : "已上传");
            schematicDiagramList.add(temp);
        }
        dataMap.put("schematicDiagramList", schematicDiagramList);
        // 日常监测记录
        condition.put("enterpriseId", enterpriseId);
        condition.put("keyword",inside_monitor_keyword);
        JSONObject result1 = insideMonitorService.listInsideMonitors(0, 0, condition);
        List<EnterpriseInsideMonitorCustom> enterpriseInsideMonitors
                = (List<EnterpriseInsideMonitorCustom>)result1.get("enterpriseInsideMonitors");
        List insideMonitorList = new ArrayList();
        for (EnterpriseInsideMonitorCustom enterpriseInsideMonitorCustom : enterpriseInsideMonitors) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("workplaceName", enterpriseInsideMonitorCustom.getWorkplaceName());
            temp.put("monitorPointName", enterpriseInsideMonitorCustom.getMonitorPointName());
            String harmFactorName = enterpriseInsideMonitorCustom.getHarmFactorName();
            temp.put("harmFactorName", (harmFactorName == null || harmFactorName.equals("")) ? "-"
                    : harmFactorName.replace("<","&lt;").replace(">","&gt;"));
            temp.put("limitValue", enterpriseInsideMonitorCustom.getLimitValue());
            temp.put("monitorPeriod", enterpriseInsideMonitorCustom.getMonitorPeriod());
            temp.put("monitorResult", enterpriseInsideMonitorCustom.getMonitorResult().replace("<","&lt;").replace(">","&gt;"));
            temp.put("monitorDate", sdf.format(enterpriseInsideMonitorCustom.getMonitorDate()));
            insideMonitorList.add(temp);
        }
        dataMap.put("insideMonitorList", insideMonitorList);
        // 检测服务机构
        JSONObject result2 = inspectionInstitutionService.listInspectionInstitutionsByType("检测机构", enterpriseId);
        List<EnterpriseInspectionInstitution> enterpriseInspectionInstitutions
                = (List<EnterpriseInspectionInstitution>)result2.get("enterpriseInspectionInstitutions");
        List checkInstitutionList = new ArrayList();
        for (EnterpriseInspectionInstitution enterpriseInspectionInstitution : enterpriseInspectionInstitutions) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name", enterpriseInspectionInstitution.getName());
            String contacts = enterpriseInspectionInstitution.getContacts();
            temp.put("contacts", (contacts == null || contacts.equals("")) ? "-" : contacts);
            String mobilePhone = enterpriseInspectionInstitution.getMobilePhone();
            temp.put("mobilePhone", (mobilePhone == null || mobilePhone.equals("")) ? "-" : mobilePhone);
            String contract  = enterpriseInspectionInstitution.getContract();
            temp.put("contract", (contract == null || contract.equals("")) ? "-" : "已上传");
            checkInstitutionList.add(temp);
        }
        dataMap.put("checkInstitutionList", checkInstitutionList);
        // 检测记录
        condition.put("status", EnterpriseOutsideCheckResultCustom.STATUS_CANCEL_CHECK_NO);
        condition.put("keyword",outside_check_keyword);
        List<EnterpriseOutsideCheckCustom> enterpriseOutsideChecks = outsideCheckService.listOutsideCheckCustoms(condition);
        List checkRecordList = new ArrayList();
        for (EnterpriseOutsideCheckCustom enterpriseOutsideCheckCustom : enterpriseOutsideChecks) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("checkDate", sdf.format(enterpriseOutsideCheckCustom.getCheckDate()));
            temp.put("checkOrganizationName", enterpriseOutsideCheckCustom.getCheckOrganizationName());
            String checkHarmFactorName = enterpriseOutsideCheckCustom.getCheckHarmFactorName();
            temp.put("checkHarmFactorName", (checkHarmFactorName == null || checkHarmFactorName.equals("")) ? "-"
                    : checkHarmFactorName.replace("<","&lt;").replace(">","&gt;"));
            String checkReport = enterpriseOutsideCheckCustom.getCheckReport();
            temp.put("checkReport", (checkReport == null || checkReport.equals("")) ? "-" : "已上传");
            checkRecordList.add(temp);
        }
        dataMap.put("checkRecordList", checkRecordList);
        return dataMap;
    }
}
