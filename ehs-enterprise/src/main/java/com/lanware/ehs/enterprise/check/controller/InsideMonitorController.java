package com.lanware.ehs.enterprise.check.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.check.service.InsideMonitorService;
import com.lanware.ehs.pojo.EnterpriseInsideMonitor;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 企业内部监测controller
 */
@Controller
@RequestMapping("/insideMonitor")
public class InsideMonitorController {
    @Autowired
    /** 注入insideMonitorService的bean */
    private InsideMonitorService insideMonitorService;

    @RequestMapping("/listInsideMonitors")
    @ResponseBody
    /**
     * @description 获取日常监测result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listInsideMonitors(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = insideMonitorService.listInsideMonitors(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /**
     * @description 根据企业id获取监测点result
     * @param enterpriseUser 当前登录用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/getMointorPoint")
    @ResponseBody
    public ResultFormat getMointorPoint(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject result = insideMonitorService.listMonitorPointsByEnterpriseId(enterpriseUser.getEnterpriseId());
        return ResultFormat.success(result);
    }

    /**
     * @description 根据监测点id获取作业场所result
     * @param monitorPointId 监测点id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/getWorkplaceByMonitorId")
    @ResponseBody
    public ResultFormat getWorkplaceByMonitorId(Long monitorPointId) {
        JSONObject result = insideMonitorService.listWorkplacesByMonitorId(monitorPointId);
        return ResultFormat.success(result);
    }

    /**
     * @description 根据监测点id和工作场所id获取危害因素result
     * @param monitorPointId 监测点id
     * @param workplaceId 工作场所id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/getHarmFactorByMonitorWorkplaceId")
    @ResponseBody
    public ResultFormat getHarmFactorByMonitorWorkplaceId(Long monitorPointId, Long workplaceId) {
        JSONObject result = insideMonitorService.listHarmFactorsByMonitorWorkplaceId(monitorPointId, workplaceId);
        return ResultFormat.success(result);
    }

    /**
     * @description 根据企业id获取作业场所result
     * @param enterpriseUser 当前登录用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/getWorkplace")
    @ResponseBody
    public ResultFormat getWorkplace(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject result = insideMonitorService.listWorkplacesByEnterpriseId(enterpriseUser.getEnterpriseId());
        return ResultFormat.success(result);
    }

    /**
     * @description 根据作业场所id获取危害因素result
     * @param workplaceId 工作场所id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/getHarmfactorByWorkplaceId")
    @ResponseBody
    public ResultFormat getHarmfactorByWorkplaceId(Long workplaceId) {
        JSONObject result = insideMonitorService.listHarmfactorsByWorkplaceId(workplaceId);
        return ResultFormat.success(result);
    }

    /**
     * @description 保存日常监测信息
     * @param enterpriseInsideMonitorJSON 日常监测JSON
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/editInsideMonitor/save")
    @ResponseBody
    public ResultFormat save(String enterpriseInsideMonitorJSON) {
        EnterpriseInsideMonitor enterpriseInsideMonitor
                = JSONObject.parseObject(enterpriseInsideMonitorJSON, EnterpriseInsideMonitor.class);
        if (enterpriseInsideMonitor.getId() == null) {
            enterpriseInsideMonitor.setGmtCreate(new Date());
            if (insideMonitorService.insertInsideMonitor(enterpriseInsideMonitor) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseInsideMonitor.setGmtModified(new Date());
            if (insideMonitorService.updateInsideMonitor(enterpriseInsideMonitor) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    /**
     * @description 查看日常监测信息
     * @param id 日常监测id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("/viewInsideMonitor")
    public ModelAndView viewInsideMonitor(Long id) {
        JSONObject data = new JSONObject();
        EnterpriseInsideMonitor enterpriseInsideMonitor = insideMonitorService.getInsideMonitorById(id);
        data.put("enterpriseInsideMonitor", enterpriseInsideMonitor);
        return new ModelAndView("check/viewInsideMonitor", data);
    }

    /**
     * @description 删除日常监测信息
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/deleteInsideMonitor")
    @ResponseBody
    public ResultFormat deleteInsideMonitor(Long id) {
        if (insideMonitorService.deleteInsideMonitor(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    /**
     * @description 批量删除日常监测信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/deleteInsideMonitors")
    @ResponseBody
    public ResultFormat deleteInsideMonitors(Long[] ids) {
        if (insideMonitorService.deleteInsideMonitors(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
