package com.lanware.ehs.enterprise.check.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.check.service.MonitorPointService;
import com.lanware.ehs.pojo.EnterpriseMonitorPoint;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 监测点管理controller
 */
@Controller
@RequestMapping("/monitorPoint")
public class MonitorPointController {
    /** 注入outsideCheckService的bean */
    @Autowired
    private MonitorPointService monitorPointService;

    /** @description 查询监测点result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 当前登录企业
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    @RequestMapping("/listMonitorPoints")
    @ResponseBody
    public ResultFormat listMonitorPoints(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = monitorPointService.listMonitorPoints(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    /** @description 保存监测点
     * @param enterpriseMonitorPointJSON 监测点json
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/editMonitorPoint/save")
    @ResponseBody
    public ResultFormat save(String enterpriseMonitorPointJSON) {
        EnterpriseMonitorPoint enterpriseMonitorPoint
                = JSONObject.parseObject(enterpriseMonitorPointJSON, EnterpriseMonitorPoint.class);
        if (enterpriseMonitorPoint.getId() == null) {
            enterpriseMonitorPoint.setGmtCreate(new Date());
            if (monitorPointService.insertMonitorPoint(enterpriseMonitorPoint) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseMonitorPoint.setGmtModified(new Date());
            if (monitorPointService.updateMonitorPoint(enterpriseMonitorPoint) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    /**
     * @description 删除监测点(同时删除监测点关联的工作场所和监测危害因素)
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/deleteMonitorPoint")
    @ResponseBody
    public ResultFormat deleteMonitorPoint(Long id) {
        if (monitorPointService.deleteMonitorPoint(id) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    /**
     * @description 批量删除监测点(同时删除监测点关联的工作场所和监测危害因素)
     * @param ids 要删除的id数组
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    @RequestMapping("/deleteMonitorPoints")
    @ResponseBody
    public ResultFormat deleteMonitorPoints(Long[] ids) {
        if (monitorPointService.deleteMonitorPoints(ids) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
