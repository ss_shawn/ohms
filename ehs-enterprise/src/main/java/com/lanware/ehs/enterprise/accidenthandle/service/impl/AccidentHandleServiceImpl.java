package com.lanware.ehs.enterprise.accidenthandle.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.accidenthandle.service.AccidentHandleService;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.mapper.custom.EnterpriseAccidentHandleCustomMapper;
import com.lanware.ehs.pojo.EnterpriseAccidentHandle;
import com.lanware.ehs.pojo.EnterpriseAccidentHandleExample;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import com.lanware.ehs.pojo.custom.EnterpriseAccidentHandleCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 事故调查处理interface实现类
 */
@Service
public class AccidentHandleServiceImpl implements AccidentHandleService {
    /** 注入enterpriseAccidentHandleCustomMapper的bean */
    @Autowired
    private EnterpriseAccidentHandleCustomMapper enterpriseAccidentHandleCustomMapper;
    /** 注入enterpriseRelationFileMapper的bean */
    @Autowired
    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    /**
     * @description 查询事故调查处理list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseAccidentHandles和totalNum的result
     */
    @Override
    public JSONObject listAccidentHandles(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseAccidentHandleCustom> enterpriseAccidentHandles
                = enterpriseAccidentHandleCustomMapper.listAccidentHandles(condition);
        result.put("enterpriseAccidentHandles", enterpriseAccidentHandles);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseAccidentHandleCustom> pageInfo
                    = new PageInfo<EnterpriseAccidentHandleCustom>(enterpriseAccidentHandles);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询事故调查处理信息
     * @param id 事故调查处理id
     * @return com.lanware.management.pojo.EnterpriseInspection 返回EnterpriseInspection对象
     */
    @Override
    public EnterpriseAccidentHandle getAccidentHandleById(Long id) {
        return enterpriseAccidentHandleCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增事故调查处理信息
     * @param enterpriseAccidentHandle 事故调查处理pojo
     * @return int 插入成功的数目
     */
    @Override
    public int insertAccidentHandle(EnterpriseAccidentHandle enterpriseAccidentHandle) {
        return enterpriseAccidentHandleCustomMapper.insert(enterpriseAccidentHandle);
    }

    /**
     * @description 更新事故调查处理信息
     * @param enterpriseAccidentHandle 事故调查处理pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateAccidentHandle(EnterpriseAccidentHandle enterpriseAccidentHandle) {
        return enterpriseAccidentHandleCustomMapper.updateByPrimaryKeySelective(enterpriseAccidentHandle);
    }

    /**
     * @description 删除员事故调查处理信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteAccidentHandle(Long id) {
        // 删除oss服务器上对应的文件
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria= enterpriseRelationFileExample.createCriteria();
        criteria.andRelationIdEqualTo(id);
        criteria.andRelationModuleEqualTo("事故调查处理");
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        for (EnterpriseRelationFile enterpriseRelationFile : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        // 删除数据库中对应的数据
        enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        return enterpriseAccidentHandleCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除事故调查处理信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteAccidentHandles(Long[] ids) {
        // 删除oss服务器上对应的文件
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria= enterpriseRelationFileExample.createCriteria();
        criteria.andRelationIdIn(Arrays.asList(ids));
        criteria.andRelationModuleEqualTo("事故调查处理");
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        for (EnterpriseRelationFile enterpriseRelationFile : enterpriseRelationFiles) {
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        // 删除数据库中对应的数据
        enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
        EnterpriseAccidentHandleExample enterpriseAccidentHandleExample = new EnterpriseAccidentHandleExample();
        EnterpriseAccidentHandleExample.Criteria criteria1 = enterpriseAccidentHandleExample.createCriteria();
        criteria1.andIdIn(Arrays.asList(ids));
        return enterpriseAccidentHandleCustomMapper.deleteByExample(enterpriseAccidentHandleExample);
    }
}
