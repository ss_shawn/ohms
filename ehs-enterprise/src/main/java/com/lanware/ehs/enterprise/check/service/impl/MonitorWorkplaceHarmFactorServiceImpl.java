package com.lanware.ehs.enterprise.check.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.check.service.MonitorWorkplaceHarmFactorService;
import com.lanware.ehs.mapper.custom.EnterpriseWorkplaceCustomMapper;
import com.lanware.ehs.mapper.custom.MonitorWorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.mapper.custom.WorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.pojo.EnterpriseWorkplace;
import com.lanware.ehs.pojo.MonitorWorkplaceHarmFactor;
import com.lanware.ehs.pojo.MonitorWorkplaceHarmFactorExample;
import com.lanware.ehs.pojo.custom.MonitorWorkplaceHarmFactorCustom;
import com.lanware.ehs.pojo.custom.WorkplaceHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description 监测点关联的作业场所和监测危害因素interface实现类
 */
@Service
public class MonitorWorkplaceHarmFactorServiceImpl implements MonitorWorkplaceHarmFactorService {
    /** 注入monitorWorkplaceHarmFactorCustomMapper的bean */
    @Autowired
    private MonitorWorkplaceHarmFactorCustomMapper monitorWorkplaceHarmFactorCustomMapper;
    /** 注入enterpriseWorkplaceCustomMapper的bean */
    @Autowired
    private EnterpriseWorkplaceCustomMapper enterpriseWorkplaceCustomMapper;
    /** 注入workplaceHarmFactorCustomMapper的bean */
    @Autowired
    private WorkplaceHarmFactorCustomMapper workplaceHarmFactorCustomMapper;


    /**
     * @description 查询监测点关联的作业场所和监测危害因素list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseMonitorPoints和totalNum的result
     */
    @Override
    public JSONObject listWorkplaceHarmFactors(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<MonitorWorkplaceHarmFactorCustom> monitorWorkplaceHarmFactorCustoms
                = monitorWorkplaceHarmFactorCustomMapper.listWorkplaceHarmFactors(condition);
        result.put("monitorWorkplaceHarmFactors", monitorWorkplaceHarmFactorCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<MonitorWorkplaceHarmFactorCustom> pageInfo = new PageInfo<MonitorWorkplaceHarmFactorCustom>(monitorWorkplaceHarmFactorCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 获取该监测点未添加的作业场所(编辑的时候原作业场所也存在)
     * @param condition 查询条件(企业id,监测点id,作业场所id)
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    public JSONObject getSelectWorkplaces(Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        List<EnterpriseWorkplace> enterpriseWorkplaces
                = enterpriseWorkplaceCustomMapper.getSelectMonitorWorkplaces(condition);
        result.put("enterpriseWorkplaces", enterpriseWorkplaces);
        return result;
    }

    /**
     * @description 获取该监测点中作业场所未选择的危害因素
     * @param condition 查询条件(作业场所id,关键字)
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @Override
    public JSONObject getSelectHarmFactors(Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        List<WorkplaceHarmFactorCustom> workplaceHarmFactorCustoms
                = workplaceHarmFactorCustomMapper.getSelectMonitorHarmFactors(condition);
        result.put("workplaceHarmFactorCustoms", workplaceHarmFactorCustoms);
        return result;
    }

    /**
     * @description 新增监测点关联的作业场所和监测危害因素
     * @param monitorWorkplaceHarmFactorCustom 监测点关联的作业场所和监测危害因素扩展pojo
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insert(MonitorWorkplaceHarmFactorCustom monitorWorkplaceHarmFactorCustom) {
        // 获取新增的监测危害因素id数组
        String[] harmFactorIds = monitorWorkplaceHarmFactorCustom.getTempHarmFactorId().split(",");
        for (int i = 0; i < harmFactorIds.length; i++) {
            Long harmFactorId = Long.parseLong(harmFactorIds[i]);
            MonitorWorkplaceHarmFactor monitorWorkplaceHarmFactor = new MonitorWorkplaceHarmFactor();
            monitorWorkplaceHarmFactor.setMonitorPointId(monitorWorkplaceHarmFactorCustom.getMonitorPointId());
            monitorWorkplaceHarmFactor.setWorkplaceId(monitorWorkplaceHarmFactorCustom.getWorkplaceId());
            monitorWorkplaceHarmFactor.setHarmFactorId(harmFactorId);
            monitorWorkplaceHarmFactor.setGmtCreate(new Date());
            monitorWorkplaceHarmFactorCustomMapper.insert(monitorWorkplaceHarmFactor);
        }
        return harmFactorIds.length;
    }

    /**
     * @description 更新监测点关联的作业场所和监测危害因素(先删除,再新增)
     * @param monitorWorkplaceHarmFactorCustom 监测点关联的作业场所和监测危害因素扩展pojo
     * @return int 更新成功的数目
     */
    @Override
    @Transactional
    public int update(MonitorWorkplaceHarmFactorCustom monitorWorkplaceHarmFactorCustom) {
        // 删除该监测点关联的作业场所和监测危害因素
        MonitorWorkplaceHarmFactorExample monitorWorkplaceHarmFactorExample = new MonitorWorkplaceHarmFactorExample();
        MonitorWorkplaceHarmFactorExample.Criteria criteria = monitorWorkplaceHarmFactorExample.createCriteria();
        criteria.andMonitorPointIdEqualTo(monitorWorkplaceHarmFactorCustom.getMonitorPointId());
        criteria.andWorkplaceIdEqualTo(monitorWorkplaceHarmFactorCustom.getWorkplaceId());
        monitorWorkplaceHarmFactorCustomMapper.deleteByExample(monitorWorkplaceHarmFactorExample);
        // 获取新增的监测危害因素id数组
        String[] harmFactorIds = monitorWorkplaceHarmFactorCustom.getTempHarmFactorId().split(",");
        for (int i = 0; i < harmFactorIds.length; i++) {
            Long harmFactorId = Long.parseLong(harmFactorIds[i]);
            MonitorWorkplaceHarmFactor monitorWorkplaceHarmFactor = new MonitorWorkplaceHarmFactor();
            monitorWorkplaceHarmFactor.setMonitorPointId(monitorWorkplaceHarmFactorCustom.getMonitorPointId());
            monitorWorkplaceHarmFactor.setWorkplaceId(monitorWorkplaceHarmFactorCustom.getWorkplaceId());
            monitorWorkplaceHarmFactor.setHarmFactorId(harmFactorId);
            monitorWorkplaceHarmFactor.setGmtCreate(new Date());
            monitorWorkplaceHarmFactorCustomMapper.insert(monitorWorkplaceHarmFactor);
        }
        return harmFactorIds.length;
    }

    /**
     * @description 获取监测点关联的作业场所和监测危害因素扩展
     * @param condition 查询条件(工作场所id和监测点id)
     * @return MonitorWorkplaceHarmFactorCustom 监测点关联的作业场所和监测危害因素扩展类
     */
    @Override
    public MonitorWorkplaceHarmFactorCustom getMonitorWorkplaceHarmFactorCustom(Map<String, Object> condition) {
        return monitorWorkplaceHarmFactorCustomMapper.getMonitorWorkplaceHarmFactorCustom(condition);
    }

    /**
     * @description 删除作业场所和监测危害因素
     * @param monitorPointId 监测点id
     * @param workplaceId 作业场所id
     * @return 删除成功的数目
     */
    @Override
    public int delete(Long monitorPointId, Long workplaceId) {
        MonitorWorkplaceHarmFactorExample monitorWorkplaceHarmFactorExample = new MonitorWorkplaceHarmFactorExample();
        MonitorWorkplaceHarmFactorExample.Criteria criteria = monitorWorkplaceHarmFactorExample.createCriteria();
        criteria.andMonitorPointIdEqualTo(monitorPointId);
        criteria.andWorkplaceIdEqualTo(workplaceId);
        return monitorWorkplaceHarmFactorCustomMapper.deleteByExample(monitorWorkplaceHarmFactorExample);
    }

    /**
     * @description 批量删除作业场所和监测危害因素
     * @param monitorPointId 监测点id
     * @param workplaceIds 作业场所id数组
     * @return 删除成功的数目
     */
    @Override
    public int deletes(Long monitorPointId, Long[] workplaceIds) {
        MonitorWorkplaceHarmFactorExample monitorWorkplaceHarmFactorExample = new MonitorWorkplaceHarmFactorExample();
        MonitorWorkplaceHarmFactorExample.Criteria criteria = monitorWorkplaceHarmFactorExample.createCriteria();
        criteria.andMonitorPointIdEqualTo(monitorPointId);
        criteria.andWorkplaceIdIn(Arrays.asList(workplaceIds));
        return monitorWorkplaceHarmFactorCustomMapper.deleteByExample(monitorWorkplaceHarmFactorExample);
    }
}
