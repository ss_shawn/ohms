package com.lanware.ehs.enterprise.inspection.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.inspection.service.InspectionService;
import com.lanware.ehs.pojo.EnterpriseInspection;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 日常检查整改登记controller
 */
@Controller
@RequestMapping("/inspection")
public class InspectionController {
    /** 注入inspectionService的bean */
    @Autowired
    private InspectionService inspectionService;

    @RequestMapping("")
    /**
     * @description 返回日常检查整改登记页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "inspection/list";
    }

    @RequestMapping("/listInspections")
    @ResponseBody
    /**
     * @description 获取日常检查整改登记result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param dzkparam 待整改表示字段  参数值：dzk,其他值一律不认
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listInspections(Integer pageNum, Integer pageSize, String keyword,String zkgqparam
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());

        //判断是否是待整改链接过来，必须参数和值对应为param = dzk
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(zkgqparam) && zkgqparam.equals("zkgq")){
            condition.put("zkgqparam", zkgqparam);
        }

        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = inspectionService.listInspections(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editInspection/save")
    @ResponseBody
    /** @description 保存日常检查整改信息，并上传签到表到oss服务器
     * @param enterpriseInspectionJSON 检查pojoo
     * @param registrationSheetFile 日常检查整改登记表
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseInspectionJSON, MultipartFile registrationSheetFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseInspection enterpriseInspection = JSONObject.parseObject(enterpriseInspectionJSON, EnterpriseInspection.class);
        if (enterpriseInspection.getId() == null) {
            enterpriseInspection.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseInspection.setGmtCreate(new Date());
            if (inspectionService.insertInspection(enterpriseInspection, registrationSheetFile) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseInspection.setGmtModified(new Date());
            if (inspectionService.updateInspection(enterpriseInspection, registrationSheetFile) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteInspection")
    @ResponseBody
    /**
     * @description 删除日常检查整改信息
     * @param id 要删除的id
     * @param registrationSheet 要删除的检查整改登记表路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInspection(Long id, String registrationSheet) {
        if (inspectionService.deleteInspection(id, registrationSheet) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteInspections")
    @ResponseBody
    /**
     * @description 批量删除日常检查整改信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInspections(Long[] ids) {
        if (inspectionService.deleteInspections(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/saveRegistrationSheet")
    @ResponseBody
    /** @description 保存检查整改登记表，并上传签到表到oss服务器
     * @param enterpriseInspectionJSON 检查pojoo
     * @param registrationSheetFile 日常检查整改登记表
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat saveRegistrationSheet(String enterpriseInspectionJSON, MultipartFile registrationSheetFile) {
        EnterpriseInspection enterpriseInspection = JSONObject.parseObject(enterpriseInspectionJSON, EnterpriseInspection.class);
            enterpriseInspection.setGmtModified(new Date());
        if (inspectionService.updateRegistrationSheet(enterpriseInspection, registrationSheetFile) > 0) {
            return ResultFormat.success("更新成功");
        } else {
            return ResultFormat.error("更新失败");
        }
    }
}
