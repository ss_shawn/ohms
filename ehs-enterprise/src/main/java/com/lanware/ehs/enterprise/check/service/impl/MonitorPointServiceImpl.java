package com.lanware.ehs.enterprise.check.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.check.service.MonitorPointService;
import com.lanware.ehs.mapper.custom.EnterpriseMonitorPointCustomMapper;
import com.lanware.ehs.mapper.custom.MonitorWorkplaceHarmFactorCustomMapper;
import com.lanware.ehs.pojo.EnterpriseMonitorPoint;
import com.lanware.ehs.pojo.EnterpriseMonitorPointExample;
import com.lanware.ehs.pojo.MonitorWorkplaceHarmFactorExample;
import com.lanware.ehs.pojo.custom.EnterpriseMonitorPointCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 监测点关联interface实现类
 */
@Service
public class MonitorPointServiceImpl implements MonitorPointService {
    @Autowired
    private EnterpriseMonitorPointCustomMapper enterpriseMonitorPointCustomMapper;
    /** 注入monitorWorkplaceHarmFactorCustomMapper的bean */
    @Autowired
    private MonitorWorkplaceHarmFactorCustomMapper monitorWorkplaceHarmFactorCustomMapper;

    /**
     * @description 查询监测点list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseMonitorPoints和totalNum的result
     */
    @Override
    public JSONObject listMonitorPoints(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseMonitorPointCustom> enterpriseMonitorPointCustoms
                = enterpriseMonitorPointCustomMapper.listMonitorPoints(condition);
        result.put("enterpriseMonitorPoints", enterpriseMonitorPointCustoms);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseMonitorPointCustom> pageInfo = new PageInfo<EnterpriseMonitorPointCustom>(enterpriseMonitorPointCustoms);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 新增监测点
     * @param enterpriseMonitorPoint 监测点pojo
     * @return int 插入成功的数目
     */
    @Override
    public int insertMonitorPoint(EnterpriseMonitorPoint enterpriseMonitorPoint) {
        return enterpriseMonitorPointCustomMapper.insert(enterpriseMonitorPoint);
    }

    /**
     * @description 更新监测点
     * @param enterpriseMonitorPoint 监测点pojo
     * @return int 更新成功的数目
     */
    @Override
    public int updateMonitorPoint(EnterpriseMonitorPoint enterpriseMonitorPoint) {
        return enterpriseMonitorPointCustomMapper.updateByPrimaryKeySelective(enterpriseMonitorPoint);
    }

    /**
     * @description 根据id查询监测点
     * @param id 危害因素检测id
     * @return com.lanware.management.pojo.EnterpriseMonitorPoint 返回EnterpriseMonitorPoint对象
     */
    @Override
    public EnterpriseMonitorPoint getMonitorPointById(Long id) {
        return enterpriseMonitorPointCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 删除监测点(同时删除监测点关联的工作场所和监测危害因素)
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteMonitorPoint(Long id) {
        // 删除监测点关联的工作场所和监测危害因素
        MonitorWorkplaceHarmFactorExample monitorWorkplaceHarmFactorExample = new MonitorWorkplaceHarmFactorExample();
        MonitorWorkplaceHarmFactorExample.Criteria criteria = monitorWorkplaceHarmFactorExample.createCriteria();
        criteria.andMonitorPointIdEqualTo(id);
        monitorWorkplaceHarmFactorCustomMapper.deleteByExample(monitorWorkplaceHarmFactorExample);
        // 删除监测点
        return enterpriseMonitorPointCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除监测点(同时删除监测点关联的工作场所和监测危害因素)
     * @param ids 要删除的id数组
     * @return int 删除成功的数目
     */
    @Override
    @Transactional
    public int deleteMonitorPoints(Long[] ids) {
        // 删除监测点关联的工作场所和监测危害因素
        MonitorWorkplaceHarmFactorExample monitorWorkplaceHarmFactorExample = new MonitorWorkplaceHarmFactorExample();
        MonitorWorkplaceHarmFactorExample.Criteria criteria = monitorWorkplaceHarmFactorExample.createCriteria();
        criteria.andMonitorPointIdIn(Arrays.asList(ids));
        monitorWorkplaceHarmFactorCustomMapper.deleteByExample(monitorWorkplaceHarmFactorExample);
        // 删除监测点
        EnterpriseMonitorPointExample enterpriseMonitorPointExample = new EnterpriseMonitorPointExample();
        EnterpriseMonitorPointExample.Criteria criteria1 = enterpriseMonitorPointExample.createCriteria();
        criteria1.andIdIn(Arrays.asList(ids));
        return enterpriseMonitorPointCustomMapper.deleteByExample(enterpriseMonitorPointExample);
    }
}
