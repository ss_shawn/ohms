package com.lanware.ehs.enterprise.account.inspection.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.inspection.service.InspectionService;
import com.lanware.ehs.enterprise.inspection.service.RectificationService;
import com.lanware.ehs.pojo.EnterpriseInspection;
import com.lanware.ehs.pojo.EnterpriseInspectionRectification;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseInspectionCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 检查整改台帐controller
 */
@Controller
@RequestMapping("/inspectionAccount")
public class InspectionAccountController {
    /** 注入inspectionService的bean */
    @Autowired
    private InspectionService inspectionService;
    /** 注入rectificationService的bean */
    @Autowired
    private RectificationService rectificationService;

    /** 下载word的临时路径 */
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;

    @RequestMapping("")
    /**
     * @description 返回检查整改台帐页面
     * @param enterpriseUser 当前登录企业
     * @return java.lang.String 返回页面路径
     */
    public ModelAndView list(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseUser.getEnterpriseId());
        return new ModelAndView("account/inspection/list", data);
    }

    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(InspectionAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 整改记录word下载
     * @param inspectionId 日常检查id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadRectification")
    @ResponseBody
    public ResultFormat downloadRectification(Long inspectionId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 返回word的变量
        Map<String, Object> dataMap = new HashMap<String, Object>();
        // 主文件路径
        upload = new File(uploadTempPath, inspectionId + File.separator);
        if (!upload.exists()) upload.mkdirs();
        // 运行使用
        Map<String, Object> condition = new HashMap<>();
        condition.put("inspectionId", inspectionId);
        JSONObject result = rectificationService.listRectifications(inspectionId);
        List<EnterpriseInspectionRectification> enterpriseInspectionRectifications
                = (List<EnterpriseInspectionRectification>)result.get("enterpriseInspectionRectifications");
        List rectificationList = new ArrayList();
        for (EnterpriseInspectionRectification enterpriseInspectionRectification : enterpriseInspectionRectifications) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("rectificationDate", sdf.format(enterpriseInspectionRectification.getRectificationDate()));
            temp.put("department", enterpriseInspectionRectification.getDepartment());
            temp.put("content", enterpriseInspectionRectification.getContent());
            rectificationList.add(temp);
        }
        // 获取日常检查整改登记的日期和部门，用于生成整改记录的名称，便于区分
        EnterpriseInspection enterpriseInspection = inspectionService.getInspectionById(inspectionId);
        String checkDate = sdf.format(enterpriseInspection.getCheckDate());
        String checkNature = enterpriseInspection.getCheckNature();
        String checkDept = enterpriseInspection.getCheckDept();
        String content = enterpriseInspection.getContent();
        String isRectified = enterpriseInspection.getIsRectified() == 0 ? "否" : "是";
        dataMap.put("rectificationList", rectificationList);
        dataMap.put("checkDate", checkDate);
        dataMap.put("checkNature", checkNature);
        dataMap.put("checkDept", checkDept);
        dataMap.put("content", content);
        dataMap.put("isRectified", isRectified);
        // 创建配置实例
        Configuration configuration = new Configuration();
        // 设置编码
        configuration.setDefaultEncoding("UTF-8");
        // ftl模板文件
        configuration.setClassForTemplateLoading(InspectionAccountController.class,"/templates/ftl");
        // 获取模板
        try {
            Template template = configuration.getTemplate("rectification.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "检查整改记录表.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("检查整改记录表.doc", "utf-8"));
            bis = new BufferedInputStream(new FileInputStream(outFile));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = bis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            bis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 检查整改台帐zip下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @return 格式化的result
     */
    @RequestMapping("/downloadInspection")
    @ResponseBody
    public ResultFormat downloadInspection(Long enterpriseId, HttpServletResponse response) {
        File upload = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        List<String> files = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // 创建配置实例
            Configuration configuration = new Configuration();
            // 设置编码
            configuration.setDefaultEncoding("UTF-8");
            // ftl模板文件
            configuration.setClassForTemplateLoading(InspectionAccountController.class,"/templates/ftl");
            // 返回word的变量
            Map<String, Object> dataMap = new HashMap<String, Object>();
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 日常检查整改登记
            Map<String, Object> condition = new HashMap<>();
            condition.put("enterpriseId", enterpriseId);
            JSONObject result = inspectionService.listInspections(0, 0, condition);
            List<EnterpriseInspectionCustom> enterpriseInspections
                    = (List<EnterpriseInspectionCustom>)result.get("enterpriseInspections");
            List inspectionList = new ArrayList();
            for (EnterpriseInspectionCustom enterpriseInspectionCustom : enterpriseInspections) {
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("checkDate", sdf.format(enterpriseInspectionCustom.getCheckDate()));
                temp.put("checkNature", enterpriseInspectionCustom.getCheckNature());
                temp.put("checkDept", enterpriseInspectionCustom.getCheckDept());
                temp.put("content", enterpriseInspectionCustom.getContent());
                temp.put("isRectified", enterpriseInspectionCustom.getIsRectified() == 0 ? "否" : "是");
                // 整改记录
                JSONObject result1 = rectificationService.listRectifications(enterpriseInspectionCustom.getId());
                List<EnterpriseInspectionRectification> enterpriseInspectionRectifications
                        = (List<EnterpriseInspectionRectification>) result1.get("enterpriseInspectionRectifications");
                List rectificationList = new ArrayList();
                for (EnterpriseInspectionRectification enterpriseInspectionRectification : enterpriseInspectionRectifications) {
                    Map<String, Object> temp1 = new HashMap<String, Object>();
                    temp1.put("rectificationDate", sdf.format(enterpriseInspectionRectification.getRectificationDate()));
                    temp1.put("department", enterpriseInspectionRectification.getDepartment());
                    temp1.put("content", enterpriseInspectionRectification.getContent());
                    rectificationList.add(temp1);
                }
                temp.put("rectificationList", rectificationList);
                inspectionList.add(temp);
            }
            dataMap.put("inspectionList", inspectionList);
            // 获取模板
            Template template = configuration.getTemplate("inspectionRectification.ftl");
            // 输出文件
            String uploadPath = upload + File.separator + "检查整改记录表.doc";
            File outFile = new File(uploadPath);
            // 如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            // 将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            // 生成文件
            template.process(dataMap, out);
            // 关闭流
            out.flush();
            out.close();
            //放入压缩列表
            files.add(uploadPath);

            String uploadZipPath = upload + File.separator + "检查整改台帐.zip";
            FileUtil.toZip(files, uploadZipPath, false);
            File uploadZipFile = new File(uploadZipPath);
            FileInputStream fis = new FileInputStream(uploadZipFile);
            // 浏览器下载
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("检查整改台帐.zip", "utf-8"));
            os = response.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            while ((length = fis.read(buffer)) != -1){
                os.write(buffer, 0, length);
            }
            fis.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除临时文件
        FileUtil.delFolder(upload.getPath());
        return null;
    }

    /**
     * @description 检查整改台帐文件下载
     * @param enterpriseId 当前登录企业id
     * @param response
     * @param request
     */
    @RequestMapping("/downloadInspectionAccount")
    @ResponseBody
    public void downloadInspectionAccount(Long enterpriseId, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();
            // 返回word的变量
            Map<String, Object> dataMap = getEducationTrainingAccountMap(enterpriseId,request);
            String uploadPath = upload + File.separator + "职业危害检查整改台帐.doc";
            exeWord(dataMap,uploadPath,"inspectionAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业危害检查整改台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到检查整改台帐文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getEducationTrainingAccountMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 日常检查整改登记
        JSONObject result = inspectionService.listInspections(0, 0, condition);
        List<EnterpriseInspectionCustom> enterpriseInspections
                = (List<EnterpriseInspectionCustom>)result.get("enterpriseInspections");
        List inspectionList = new ArrayList();
        for (EnterpriseInspectionCustom enterpriseInspectionCustom : enterpriseInspections) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("checkDate", sdf.format(enterpriseInspectionCustom.getCheckDate()));
            temp.put("checkNature", enterpriseInspectionCustom.getCheckNature());
            temp.put("checkDept", enterpriseInspectionCustom.getCheckDept());
            temp.put("content", enterpriseInspectionCustom.getContent());
            temp.put("isRectified", enterpriseInspectionCustom.getIsRectified() == 0 ? "否" : "是");
            // 整改记录
            JSONObject result1 = rectificationService.listRectifications(enterpriseInspectionCustom.getId());
            List<EnterpriseInspectionRectification> enterpriseInspectionRectifications
                    = (List<EnterpriseInspectionRectification>) result1.get("enterpriseInspectionRectifications");
            List rectificationList = new ArrayList();
            for (EnterpriseInspectionRectification enterpriseInspectionRectification : enterpriseInspectionRectifications) {
                Map<String, Object> temp1 = new HashMap<String, Object>();
                temp1.put("rectificationDate", sdf.format(enterpriseInspectionRectification.getRectificationDate()));
                temp1.put("department", enterpriseInspectionRectification.getDepartment());
                temp1.put("content", enterpriseInspectionRectification.getContent());
                rectificationList.add(temp1);
            }
            temp.put("rectificationList", rectificationList);
            inspectionList.add(temp);
        }
        dataMap.put("inspectionList", inspectionList);
        return dataMap;
    }
}
