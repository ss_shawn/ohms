package com.lanware.ehs.enterprise.yearplan.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.yearplan.service.YearplanService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.Enum.YearPlanTypeEnum;
import com.lanware.ehs.pojo.custom.EnterpriseYearPlanCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 年度计划视图
 */
@Controller("yearPlanView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /** 注入yearplanService的bean */
    @Autowired
    private YearplanService yearplanService;

    /**
     * @description 新增年度计划
     * @param type 计划类型
     * @param enterpriseUser 当前登录企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("yearplan/addYearplan/{type}/{year}")
    public ModelAndView addYearplan(@PathVariable String type, @PathVariable String year
            , @CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        String viewName = "";
        EnterpriseYearPlan enterpriseYearPlan = new EnterpriseYearPlan();
        enterpriseYearPlan.setEnterpriseId(enterpriseUser.getEnterpriseId());
        enterpriseYearPlan.setPlanType(type);
        enterpriseYearPlan.setYear(year);
        data.put("enterpriseYearPlan", enterpriseYearPlan);
        if (YearPlanTypeEnum.ZYBFZ.getValue().equals(type)) {
            viewName = "yearplan/editPrevention";
        }
        if (YearPlanTypeEnum.JFTR.getValue().equals(type)) {
            viewName = "yearplan/editCost";
        }
        if (YearPlanTypeEnum.XCJY.getValue().equals(type)) {
            viewName = "training/trainingPlan/edit";
        }
        if (YearPlanTypeEnum.JKTJ.getValue().equals(type)) {
            viewName = "medical/medicalPlan/edit";
        }
        if (YearPlanTypeEnum.FHSSJC.getValue().equals(type)) {
            viewName = "protectiveMeasure/protectiveMeasurePlan/edit";
        }
        if (YearPlanTypeEnum.YJJYYL.getValue().equals(type)) {
            viewName = "toxicDrill/rescuePlan/edit";
        }
        return new ModelAndView(viewName, data);
    }

    /**
     * @description 编辑年度计划
     * @param id 计划id
     * @param type 计划类型
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("yearplan/editYearplan/{id}/{type}")
    public ModelAndView editYearplan(@PathVariable Long id, @PathVariable String type) {
        JSONObject data = new JSONObject();
        String viewName = "";
        EnterpriseYearPlan enterpriseYearPlan = yearplanService.getYearPlanById(id);
        data.put("enterpriseYearPlan", enterpriseYearPlan);
        if (YearPlanTypeEnum.ZYBFZ.getValue().equals(type)) {
            viewName = "yearplan/editPrevention";
        }
        if (YearPlanTypeEnum.JFTR.getValue().equals(type)) {
            viewName = "yearplan/editCost";
        }
        if (YearPlanTypeEnum.XCJY.getValue().equals(type)) {
            viewName = "training/trainingPlan/edit";
        }
        if (YearPlanTypeEnum.JKTJ.getValue().equals(type)) {
            viewName = "medical/medicalPlan/edit";
        }
        if (YearPlanTypeEnum.FHSSJC.getValue().equals(type)) {
            viewName = "protectiveMeasure/protectiveMeasurePlan/edit";
        }
        if (YearPlanTypeEnum.YJJYYL.getValue().equals(type)) {
            viewName = "toxicDrill/rescuePlan/edit";
        }
        return new ModelAndView(viewName, data);
    }

    /**
     * @description 新增年度检测计划
     * @param enterpriseUser 当前登录企业用户
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("yearplan/addYearCheckPlan/{year}")
    public ModelAndView addYearCheckPlan(@PathVariable String year, @CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        EnterpriseYearPlanCustom enterpriseYearPlanCustom = new EnterpriseYearPlanCustom();
        enterpriseYearPlanCustom.setEnterpriseId(enterpriseUser.getEnterpriseId());
        enterpriseYearPlanCustom.setPlanType(YearPlanTypeEnum.WHYSJC.getValue());
        enterpriseYearPlanCustom.setYear(year);
        data.put("enterpriseYearPlanCustom", enterpriseYearPlanCustom);
        return new ModelAndView("check/checkPlan/edit", data);
    }

    /**
     * @description 编辑年度检测计划
     * @param id 年度检测计划id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("yearplan/editYearCheckPlan/{id}")
    public ModelAndView editYearCheckPlan(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseYearPlanCustom enterpriseYearPlanCustom = yearplanService.getYearCheckPlanById(id);
        data.put("enterpriseYearPlanCustom", enterpriseYearPlanCustom);
        return new ModelAndView("check/checkPlan/edit", data);
    }

    /**
     * @description 查看应急救援演练计划
     * @param id 计划id
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("yearplan/viewRescuePlan/{id}")
    public ModelAndView viewRescuePlan(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseYearPlan enterpriseYearPlan = yearplanService.getYearPlanById(id);
        data.put("enterpriseYearPlan", enterpriseYearPlan);
        return new ModelAndView("toxicDrill/rescuePlan/view", data);
    }
}
