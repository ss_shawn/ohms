package com.lanware.ehs.enterprise.protectivemeasure.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseProtectiveMeasures;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @description 维修记录interface
 */
public interface ProtectiveMeasureService {
    /**
     * @description 查询维修记录list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含employeeMedicals和totalNum的result
     */
    JSONObject listProtectiveMeasures(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据企业id获取工作场所result
     * @param enterpriseId 企业id
     * @return com.alibaba.fastjson.JSONObject 返回包含baseHarmFactors的result
     */
    JSONObject listWorkplacesByEnterpriseId(Long enterpriseId);

    /**
     * @description 根据id查询维修记录
     * @param id 维修记录id
     * @return com.lanware.management.pojo.EnterpriseProtectiveMeasures 返回EnterpriseProtectiveMeasures对象
     */
    EnterpriseProtectiveMeasures getProtectiveMeasureById(Long id);

    /**
     * @description 新增维修记录
     * @param enterpriseProtectiveMeasure 维修记录pojo
      * @param operationRuleFile 操作规程
     * @param qualificationRecordFile 验收记录
     * @param acceptanceCertificateFile 合格证书
     * @return int 插入成功的数目
     */
    int insertProtectiveMeasure(EnterpriseProtectiveMeasures enterpriseProtectiveMeasure
            , MultipartFile operationRuleFile ,MultipartFile qualificationRecordFile
            , MultipartFile acceptanceCertificateFile);

    /**
     * @description 更新维修记录
     * @param enterpriseProtectiveMeasure 维修记录pojo
      * @param operationRuleFile 操作规程
     * @param qualificationRecordFile 验收记录
     * @param acceptanceCertificateFile 合格证书
     * @return int 更新成功的数目
     */
    int updateProtectiveMeasure(EnterpriseProtectiveMeasures enterpriseProtectiveMeasure
            , MultipartFile operationRuleFile ,MultipartFile qualificationRecordFile
            , MultipartFile acceptanceCertificateFile);

    /**
     * @description 删除维修记录
     * @param id 要删除的id
     * @param operationRule 要删除的操作规程
     * @param qualificationRecord 要删除的验收记录
     * @param acceptanceCertificate 要删除的合格证书
     * @return int 删除成功的数目
     */
    int deleteProtectiveMeasure(Long id, String operationRule
            , String qualificationRecord, String acceptanceCertificate);

    /**
     * @description 批量删除维修记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteProtectiveMeasures(Long[] ids);
}
