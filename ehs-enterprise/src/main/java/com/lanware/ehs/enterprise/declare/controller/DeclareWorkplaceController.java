package com.lanware.ehs.enterprise.declare.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.declare.service.DeclareWorkplaceService;
import com.lanware.ehs.pojo.EnterpriseDeclareWorkplace;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/declare/declareWorkplace")
/**
 * @description 涉及的场所和危害因素Controller
 */
public class DeclareWorkplaceController {
    @Autowired
    /** 注入declareWorkplaceService的bean */
    private DeclareWorkplaceService declareWorkplaceService;
    
    /**
     * @description 查询申报涉及危害场所和危害因素集合
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param declareId 申报id
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    @RequestMapping("/listDeclareWorkplaces")
    @ResponseBody
    public ResultFormat listDeclareWorkplaces(Integer pageNum, Integer pageSize, String keyword
            , Long declareId, String sortField, String sortType){
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("declareId", declareId);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = declareWorkplaceService.listDeclareWorkplaces(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listSelectDeclareWorkplaces")
    @ResponseBody
    /**
     * @description 查询该企业所有的作业场所和危害因素集合
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat listSelectDeclareWorkplaces(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = declareWorkplaceService.listSelectDeclareWorkplaces(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/getUnselectedDeclareWorkplaceList")
    @ResponseBody
    /**
     * @description 查询未申报的场所危害集合
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat getUnselectedDeclareWorkplaceList(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject data = new JSONObject();
        List<EnterpriseDeclareWorkplace> list
                = declareWorkplaceService.listUnselectedDeclareWorkplaces(enterpriseUser.getEnterpriseId());
        data.put("list", list);
        return ResultFormat.success(data);
    }

    /**
     * @description 保存场所危害集合
     * @param declareWorkplaceListJSON 场所危害集合
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultFormat save(String declareWorkplaceListJSON){
        List<EnterpriseDeclareWorkplace> enterpriseDeclareWorkplaces
                = JSONArray.parseArray(declareWorkplaceListJSON, EnterpriseDeclareWorkplace.class);
        if (enterpriseDeclareWorkplaces == null || enterpriseDeclareWorkplaces.isEmpty()){
            return ResultFormat.error("选择场所和危害因素失败");
        }
        for (EnterpriseDeclareWorkplace enterpriseDeclareWorkplace : enterpriseDeclareWorkplaces){
            enterpriseDeclareWorkplace.setGmtCreate(new Date());
        }
        if (declareWorkplaceService.insertBatch(enterpriseDeclareWorkplaces) > 0) {
            return ResultFormat.success("选择场所和危害因素成功");
        } else {
            return ResultFormat.error("选择场所和危害因素失败");
        }
    }

    @RequestMapping("/deleteDeclareWorkplace")
    @ResponseBody
    /**
     * @description 删除危害场所和危害因素
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteDeclareWorkplace(Long id) {
        if (declareWorkplaceService.deleteDeclareWorkplace(id) > 0) {
            return ResultFormat.success("移除成功");
        }else{
            return ResultFormat.error("移除失败");
        }
    }

    @RequestMapping("/deleteDeclareWorkplaces")
    @ResponseBody
    /**
     * @description 批量删除危害场所和危害因素
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteDeclareWorkplaces(Long[] ids) {
        if (declareWorkplaceService.deleteDeclareWorkplaces(ids) == ids.length) {
            return ResultFormat.success("移除成功");
        }else{
            return ResultFormat.error("移除失败");
        }
    }
}
