package com.lanware.ehs.enterprise.post.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.FileDeleteUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.post.service.PostService;
import com.lanware.ehs.mapper.*;
import com.lanware.ehs.mapper.custom.EnterprisePostCustomMapper;
import com.lanware.ehs.mapper.custom.PostHarmFactorCustomMapper;
import com.lanware.ehs.mapper.custom.PostProtectArticlesCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterprisePostCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.PostEmployeeResultCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class PostServiceImpl implements PostService {

    private EnterprisePostMapper enterprisePostMapper;

    private EnterprisePostCustomMapper enterprisePostCustomMapper;
    @Autowired
    private EmployeePostMapper employeePostMapper;
    @Autowired
    private PostProtectArticlesMapper postProtectArticlesMapper;
    @Autowired
    private PostHarmFactorMapper postHarmFactorMapper;
    @Autowired
    private EnterprisePostWorkplaceRelationMapper enterprisePostWorkplaceRelationMapper;
    private OSSTools ossTools;

    private FileDeleteUtil fileDeleteUtil;

    @Autowired
    public PostServiceImpl(EnterprisePostMapper enterprisePostMapper, EnterprisePostCustomMapper enterprisePostCustomMapper, OSSTools ossTools, FileDeleteUtil fileDeleteUtil){
        this.enterprisePostMapper = enterprisePostMapper;
        this.enterprisePostCustomMapper = enterprisePostCustomMapper;
        this.ossTools = ossTools;
        this.fileDeleteUtil = fileDeleteUtil;
    }

    @Override
    public Page<EnterprisePostCustom> queryEnterprisePostByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        Page<EnterprisePostCustom> page = PageHelper.startPage(pageNum, pageSize);
        enterprisePostCustomMapper.queryEnterprisePostByCondition(condition);
        return page;
    }

    @Override
    @Transactional
    public int saveEnterprisePost(EnterprisePostCustom enterprisePost, MultipartFile operationRuleFile) {
        //新增数据先保存，以便获取id
        if (enterprisePost.getId() == null){
            enterprisePost.setGmtCreate(new Date());
            enterprisePostMapper.insert(enterprisePost);
        }else{//修改时先删除工作场所列表
            EnterprisePostWorkplaceRelationExample enterprisePostWorkplaceRelationExample=new EnterprisePostWorkplaceRelationExample();
            enterprisePostWorkplaceRelationExample.createCriteria().andPostIdEqualTo(enterprisePost.getId());
            enterprisePostWorkplaceRelationMapper.deleteByExample(enterprisePostWorkplaceRelationExample);
        }
        //插入工作场所列表
        String workplaceId=enterprisePost.getWorkplaceId();
        if(workplaceId!=null&&!"".equals(workplaceId)){
            String[] workplaceIds=workplaceId.split(",");
            for(String tempWorkplaceId:workplaceIds){
                EnterprisePostWorkplaceRelation enterprisePostWorkplaceRelation=new EnterprisePostWorkplaceRelation();
                enterprisePostWorkplaceRelation.setPostId(enterprisePost.getId());
                enterprisePostWorkplaceRelation.setWorkplaceId(Long.parseLong(tempWorkplaceId));
                enterprisePostWorkplaceRelationMapper.insert(enterprisePostWorkplaceRelation);
            }
        }
        if (operationRuleFile != null){
            //上传了新的岗位操作规程
            if (!StringUtils.isEmpty(enterprisePost.getOperationRule())){
                //删除旧文件
                ossTools.deleteOSS(enterprisePost.getOperationRule());
            }
            //上传并保存新文件
            String operationRule = "/" + enterprisePost.getEnterpriseId() + "/" + ModuleName.POST.getValue() + "/" + enterprisePost.getId() + "/" + operationRuleFile.getOriginalFilename();
            try {
                ossTools.uploadStream(operationRule, operationRuleFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传岗位操作规程失败", e);
            }
            enterprisePost.setOperationRule(operationRule);
        }



        //职业卫生组织信息
        enterprisePost.setGmtModified(new Date());
        return enterprisePostMapper.updateByPrimaryKeySelective(enterprisePost);
    }

    @Override
    public String deleteEnterprisePostByIds(List<Long> ids) {
        EnterprisePostExample enterprisePostExample = new EnterprisePostExample();
        EnterprisePostExample.Criteria criteria = enterprisePostExample.createCriteria();
        criteria.andIdIn(ids);
        List<EnterprisePost> enterprisePosts = enterprisePostMapper.selectByExample(enterprisePostExample);
        //判断岗使用位是否被使用，的岗位不能删除
        for(EnterprisePost enterprisePost:enterprisePosts){
            EmployeePostExample employeePostExample=new EmployeePostExample();
            employeePostExample.createCriteria().andPostIdEqualTo(enterprisePost.getId());
            int n=employeePostMapper.selectByExample(employeePostExample).size();
            if(n>0){
                return enterprisePost.getPostName();
            }
        }

        //删除关联的文件、记录
        int num = enterprisePostCustomMapper.deleteEnterprisePostByIds(ids);
        if (num != 0){
            Set<String> filePathSet = new HashSet<>();
            for (EnterprisePost enterprisePost : enterprisePosts){
                //删除文件
                filePathSet.add(enterprisePost.getOperationRule());
                //删除防护用品
                PostProtectArticlesExample postProtectArticlesExample=new PostProtectArticlesExample();
                postProtectArticlesExample.createCriteria().andPostIdEqualTo(enterprisePost.getId());
                postProtectArticlesMapper.deleteByExample(postProtectArticlesExample);
                //删除岗位危害因素
                PostHarmFactorExample postHarmFactorExample=new PostHarmFactorExample();
                postHarmFactorExample.createCriteria().andPostIdEqualTo(enterprisePost.getId());
                postHarmFactorMapper.deleteByExample(postHarmFactorExample);
                //删除工作场所
                EnterprisePostWorkplaceRelationExample enterprisePostWorkplaceRelationExample=new EnterprisePostWorkplaceRelationExample();
                enterprisePostWorkplaceRelationExample.createCriteria().andPostIdEqualTo(enterprisePost.getId());
                enterprisePostWorkplaceRelationMapper.deleteByExample(enterprisePostWorkplaceRelationExample);
            }
            fileDeleteUtil.deleteFilesAsync(filePathSet);
        }
        if(num>0){
            return "1";
        }else{
            return "0";
        }

    }

    @Override
    public EnterprisePostCustom queryEnterprisePostById(Long id) {
        return enterprisePostCustomMapper.queryEnterprisePostById(id);
    }

    /**
     * 职业健康检查工种、接触的职业危害因素及人员名单
     * @param condition
     * @return 职业健康检查工种、接触的职业危害因素及人员名单
     */
    @Override
    public Page<PostEmployeeResultCustom> queryEnterprisePostEmployeeByCondition(Map<String, Object> condition, QueryRequest request){
        Page<EnterpriseProtectArticlesCustom> page = PageHelper.startPage(request.getPageNum(), request.getPageSize(),"p.post_name,e.name");
        String sortField = request.getField();
        if(org.apache.commons.lang3.StringUtils.isNotBlank(sortField)) {
            sortField = EhsUtil.camelToUnderscore(sortField);
            page.setOrderBy(sortField + " "+request.getOrder());
        }
        return enterprisePostCustomMapper.queryEnterprisePostEmployeeByCondition(condition);
    }
    /**
     * 保存防护用品发放签名记录文件,
     * @param id 防护用品发放 signatureRecord 签名记录文件
     * @return成功与否 1成功 0不成功
     */
    @Override
    public int uploadOperationRule(Long id, MultipartFile operationRule){

        EnterprisePost enterprisePost=enterprisePostMapper.selectByPrimaryKey(id);
        //上传图片
        if (operationRule != null){

            //上传并保存新文件
            String imageFilePath = "/" + enterprisePost.getEnterpriseId() + "/" + ModuleName.PROTECTARTICLES.getValue() + "/" + enterprisePost.getId() + "/" + operationRule.getOriginalFilename();
            try {
                ossTools.uploadStream(imageFilePath, operationRule.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传签名记录失败", e);
            }
            enterprisePost.setOperationRule(imageFilePath);
        }
        enterprisePost.setGmtModified(new Date());
        int n=enterprisePostMapper.updateByPrimaryKey(enterprisePost);
        return n;
    }
}
