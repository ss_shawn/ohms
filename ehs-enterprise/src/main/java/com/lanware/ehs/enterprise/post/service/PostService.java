package com.lanware.ehs.enterprise.post.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.EnterprisePost;
import com.lanware.ehs.pojo.custom.EnterprisePostCustom;
import com.lanware.ehs.pojo.custom.PostEmployeeResultCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface PostService {

    /**
     * 分页查询岗位
     * @param pageNum 页码
     * @param pageSize 单页数量
     * @param condition 查询条件
     * @return 分页对象
     */
    Page<EnterprisePostCustom> queryEnterprisePostByCondition(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * 保存岗位
     * @param enterprisePost 岗位对象
     * @param operationRuleFile 岗位操作规程文件
     * @return 更新数量
     */
    int saveEnterprisePost(EnterprisePostCustom enterprisePost, MultipartFile operationRuleFile);

    /**
     * 删除岗位
     * @param ids 岗位id集合
     * @return 更新数量
     */
    String deleteEnterprisePostByIds(List<Long> ids);

    /**
     * 查询岗位
     * @param id 岗位id
     * @return 更新数量
     */
    EnterprisePostCustom queryEnterprisePostById(Long id);
    /**
     * 职业健康检查工种、接触的职业危害因素及人员名单
     * @param condition
     * @return 职业健康检查工种、接触的职业危害因素及人员名单
     */
     Page<PostEmployeeResultCustom> queryEnterprisePostEmployeeByCondition(Map<String, Object> condition, QueryRequest request);
    /**
     * 保存岗位操作规程
     * @param id 防护用品发放 signatureRecord 签名记录文件
     * @return成功与否 1成功 0不成功
     */
    int uploadOperationRule(Long id, MultipartFile operationRule);

}
