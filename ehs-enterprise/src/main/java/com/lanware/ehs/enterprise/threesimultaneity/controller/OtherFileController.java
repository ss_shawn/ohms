package com.lanware.ehs.enterprise.threesimultaneity.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.threesimultaneity.service.OtherFileService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 三同时其他文件controller
 */
@Controller
@RequestMapping("/threeSimultaneity/otherFile")
public class OtherFileController {
    /** 注入otherFileService的bean */
    @Autowired
    private OtherFileService otherFileService;

    /**
     * @description 查询三同时其他文件list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @param fileType 文件类型
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listOtherFiles")
    @ResponseBody
    public ResultFormat listHarmfactors(Long relationId, String relationModule, String fileType) {
        JSONObject result = otherFileService.listOtherFiles(relationId, relationModule, fileType);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editOtherFile/save")
    @ResponseBody
    /**
     * @description 保存三同时其他文件，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param otherFile 三同时其他文件文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile otherFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (otherFileService.insertOtherFile(enterpriseRelationFile, otherFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (otherFileService.updateOtherFile(enterpriseRelationFile, otherFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteOtherFile")
    @ResponseBody
    /**
     * @description 删除三同时其他文件
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteOtherFile(Long id, String filePath) {
        if (otherFileService.deleteOtherFile(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteOtherFiles")
    @ResponseBody
    /**
     * @description 批量删除三同时其他文件
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteOtherFiles(Long[] ids) {
        if (otherFileService.deleteOtherFiles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
