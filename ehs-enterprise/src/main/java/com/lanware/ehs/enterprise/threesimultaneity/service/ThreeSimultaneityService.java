package com.lanware.ehs.enterprise.threesimultaneity.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneity;
import com.lanware.ehs.pojo.EnterpriseThreeSimultaneityFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @description 三同时管理interface
 */
public interface ThreeSimultaneityService {
    /**
     * @description 查询三同时list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseThreeSimultaneitys和totalNum的result
     */
    JSONObject listThreeSimultaneitys(Integer pageNum, Integer pageSize, Map<String, Object> condition);

    /**
     * @description 根据id查询三同时信息
     * @param id
     * @return EnterpriseThreeSimultaneity三同时pojo
     */
    EnterpriseThreeSimultaneity getThreeSimultaneityById(Long id);

    /**
     * @description 新增三同时信息
     * @param enterpriseThreeSimultaneity 三同时pojo
     * @return int 插入成功的数目
     */
    int insertThreeSimultaneity(EnterpriseThreeSimultaneity enterpriseThreeSimultaneity);

    /**
     * @description 更新三同时信息
     * @param enterpriseThreeSimultaneity 三同时pojo
     * @return int 更新成功的数目
     */
    int updateThreeSimultaneity(EnterpriseThreeSimultaneity enterpriseThreeSimultaneity);

    /**
     * @description 删除三同时信息
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    int deleteThreeSimultaneity(Long id);

    /**
     * @description 批量删除三同时信息
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteThreeSimultaneitys(Long[] ids);


    /**
     * @description 根据条件更新危害程度和三同时阶段
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateHarmDegreeAndStateByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件更新三同时阶段
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateProjectState(Map<String, Object> condition);

    /**
     * @description 根据条件更新项目状态
     * @param condition 条件
     * @return 更新成功的数目
     */
    int updateStatus(Map<String, Object> condition);

    /**
     * @description 三同时文件上传到oss，并保存数据到数据库
     * @param threeSimultaneityFile 上传文件
     * @param enterpriseThreeSimultaneityFile 三同时文件pojo
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertThreeSimultaneityFile(EnterpriseThreeSimultaneityFile enterpriseThreeSimultaneityFile
            , MultipartFile threeSimultaneityFile, Long enterpriseId);

    /**
     * @description 删除文件
     * @param filePath 文件路径
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int deleteFileByFilePath(String filePath);
}
