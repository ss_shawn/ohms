package com.lanware.ehs.enterprise.consult.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.enterprise.consult.service.ConsultService;
import com.lanware.ehs.mapper.SysConsultMapper;
import com.lanware.ehs.mapper.custom.SysConsultCustomMapper;
import com.lanware.ehs.pojo.SysConsult;
import com.lanware.ehs.pojo.SysConsultExample;
import com.lanware.ehs.pojo.custom.SysConsultCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 咨询提问interface实现类
 */
@Service
public class ConsultServiceImpl implements ConsultService {
    @Autowired
    /** 注入sysConsultMapper的bean */
    private SysConsultMapper sysConsultMapper;
    @Autowired
    /** 注入sysConsultCustomMapper的bean */
    private SysConsultCustomMapper sysConsultCustomMapper;

    @Override
    /**
     * @description 查询咨询提问list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含sysRecommendInstitutions和totalNum的result
     */
    public JSONObject listConsults(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        // 开启pageHelper分页
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<SysConsultCustom> sysConsults = sysConsultCustomMapper.listConsults(condition);
        result.put("sysConsults", sysConsults);
        // 分页数据装进result
        if (pageNum != null && pageSize != null) {
            PageInfo<SysConsultCustom> pageInfo = new PageInfo<SysConsultCustom>(sysConsults);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询咨询提问信息
     * @param id
     * @return SysConsult 咨询提问pojo
     */
    @Override
    public SysConsult getConsultById(Long id) {
        return sysConsultMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增咨询提问
     * @param sysConsult 咨询提问pojo
     * @return 插入成功的数目
     */
    @Override
    public int insertConsult(SysConsult sysConsult) {
        return sysConsultMapper.insert(sysConsult);
    }

    /**
     * @description 更新咨询提问
     * @param sysConsult 咨询提问pojo
     * @return 更新成功的数目
     */
    @Override
    public int updateConsult(SysConsult sysConsult) {
        return sysConsultMapper.updateByPrimaryKeySelective(sysConsult);
    }

    @Override
    /**
     * @description 删除咨询提问
     * @param id 要删除的id
     * @return int 删除成功的数目
     */
    public int deleteConsult(Long id) {
        return sysConsultMapper.deleteByPrimaryKey(id);
    }

    @Override
    /**
     * @description 批量删除咨询提问
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    public int deleteConsults(Long[] ids) {
        SysConsultExample sysConsultExample = new SysConsultExample();
        SysConsultExample.Criteria criteria = sysConsultExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return sysConsultMapper.deleteByExample(sysConsultExample);
    }

    /**
     * 查询企业咨询最新的回复信息
     * @param enterpriseId 企业id
     * @param answerStatus 回复状态:最新回复
     * @return 最新回复list
     */
    @Override
    public List<SysConsult> queryNewReply(Long enterpriseId, Integer answerStatus) {
        SysConsultExample sysConsultExample = new SysConsultExample();
        SysConsultExample.Criteria criteria = sysConsultExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        criteria.andAnswerStatusEqualTo(answerStatus);
        return sysConsultMapper.selectByExample(sysConsultExample);
    }

    /**
     * @description 将状态为新回复的咨询更新为已回复
     * @param enterpriseId 企业id
     * @return 更新成功的数目
     */
    @Override
    public int updateAnswerStatusByEnterpriseId(Long enterpriseId) {
        return sysConsultCustomMapper.updateAnswerStatusByEnterpriseId(enterpriseId);
    }
}
