package com.lanware.ehs.enterprise.accidenthandle.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 事故调查处理相关文件interface
 */
public interface AccidentFileService {
    /**
     * @description 查询事故调查处理相关文件list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    JSONObject listAccidentFiles(Long relationId, String relationModule);

    /**
     * @description 根据id查询事故调查处理相关文件
     * @param id
     * @return 返回EnterpriseRelationFile
     */
    EnterpriseRelationFile getAccidentFileById(Long id);

    /**
     * @description 新增事故调查处理相关文件，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param accidentFile 事故调查处理相关文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int insertAccidentFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile accidentFile
            , Long enterpriseId);

    /**
     * @description 更新事故调查处理相关文件，并上传文件到oss服务器
     * @param enterpriseRelationFile 企业关联文件pojo
     * @param accidentFile 事故调查处理相关文件
     * @param enterpriseId 企业id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    int updateAccidentFile(EnterpriseRelationFile enterpriseRelationFile, MultipartFile accidentFile
            , Long enterpriseId);

    /**
     * @description 删除事故调查处理相关文件
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    int deleteAccidentFile(Long id, String filePath);

    /**
     * @description 批量删除事故调查处理相关文件
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    int deleteAccidentFiles(Long[] ids);
}