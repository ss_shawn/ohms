package com.lanware.ehs.enterprise.workplace.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.enterprise.workplace.service.InformationFileService;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @description 职业危害因素信息卡controller
 */
@Controller
@RequestMapping("/workplace/informationFile")
public class InformationFileController {
    /** 注入informationFileService的bean */
    @Autowired
    private InformationFileService informationFileService;

    /**
     * @description 查询职业危害因素信息卡list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含workHarmFactors的result
     */
    @RequestMapping("/listInformationFiles")
    @ResponseBody
    public ResultFormat listHarmfactors(Long relationId, String relationModule) {
        JSONObject result = informationFileService.listInformationFiles(relationId, relationModule);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editInformationFile/save")
    @ResponseBody
    /**
     * @description 保存职业危害因素信息卡，并上传文件到oss服务器
     * @param enterpriseRelationFileJSON 企业关联文件JSON
     * @param informationFile 职业危害因素信息卡文件
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseRelationFileJSON, MultipartFile informationFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseRelationFile enterpriseRelationFile
                = JSONObject.parseObject(enterpriseRelationFileJSON, EnterpriseRelationFile.class);
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        if (enterpriseRelationFile.getId() == null) {
            enterpriseRelationFile.setGmtCreate(new Date());
            if (informationFileService.insertInformationFile(enterpriseRelationFile, informationFile, enterpriseId) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseRelationFile.setGmtModified(new Date());
            if (informationFileService.updateInformationFile(enterpriseRelationFile, informationFile, enterpriseId) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteInformationFile")
    @ResponseBody
    /**
     * @description 删除职业危害因素信息卡
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInformationFile(Long id, String filePath) {
        if (informationFileService.deleteInformationFile(id, filePath) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteInformationFiles")
    @ResponseBody
    /**
     * @description 批量删除职业危害因素信息卡
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInformationFiles(Long[] ids) {
        if (informationFileService.deleteInformationFiles(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
