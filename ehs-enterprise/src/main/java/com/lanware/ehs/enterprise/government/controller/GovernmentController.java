package com.lanware.ehs.enterprise.government.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.enterprise.basicinfo.service.BasicInfoService;
import com.lanware.ehs.enterprise.employee.controller.EmployeeArchivesController;
import com.lanware.ehs.enterprise.government.service.GovernmentService;
import com.lanware.ehs.enterprise.protect.service.ProtectService;
import com.lanware.ehs.enterprise.protect.service.ProvideProtectArticlesService;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.GovernmentEnterpriseBaseinfo;
import com.lanware.ehs.pojo.custom.EnterpriseProtectArticlesCustom;
import com.lanware.ehs.pojo.custom.EnterpriseProvideProtectArticlesCustom;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 提交数据到监管端controller
 */
@Controller
@RequestMapping("/government")
public class GovernmentController {

    @Autowired
    private GovernmentService governmentService;
    @Autowired
    private BasicInfoService basicInfoService;
    @RequestMapping("/uploadGovernment")
    @ResponseBody
    public EhsResult uploadGovernment(@CurrentUser EnterpriseUser enterpriseUser){
        governmentService.uploadGovernment(enterpriseUser.getEnterpriseId());
        return EhsResult.ok();
    }
}
