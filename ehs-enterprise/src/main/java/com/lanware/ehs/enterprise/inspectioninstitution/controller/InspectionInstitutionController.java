package com.lanware.ehs.enterprise.inspectioninstitution.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.inspectioninstitution.service.InspectionInstitutionService;
import com.lanware.ehs.pojo.EnterpriseInspectionInstitution;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 检查机构controller
 */
@Controller
@RequestMapping("/inspectionInstitution")
public class InspectionInstitutionController {
    /** 注入inspectionInstitutionService的bean */
    @Autowired
    private InspectionInstitutionService inspectionInstitutionService;

    @RequestMapping("")
    /**
     * @description 返回检查机构页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "inspectionInstitution/list";
    }

    @RequestMapping("/listInspectionInstitutions")
    @ResponseBody
    /**
     * @description 获取检查机构result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listInspectionInstitutions(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = inspectionInstitutionService.listInspectionInstitutions(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listInstitutionsByType")
    @ResponseBody
    /**
     * @description 获取检查机构result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listInstitutionsByType(Integer pageNum, Integer pageSize, String keyword, String type
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        condition.put("type", type);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = inspectionInstitutionService.listInspectionInstitutions(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/getInstitutionByType")
    @ResponseBody
    /**
     * @description 根据机构类型获取检测机构result（企业端检测机构获取）
     * @param type 机构类型
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat getInstitutionByType(String type, @CurrentUser EnterpriseUser enterpriseUser) {
        Long enterpriseId = enterpriseUser.getEnterpriseId();
        JSONObject result = inspectionInstitutionService.listInspectionInstitutionsByType(type, enterpriseId);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editInspectionInstitution/save")
    @ResponseBody
    /** @description 保存检查机构信息，并上传机构合同到oss服务器
     * @param enterpriseInspectionInstitutionJSON 检查机构json
     * @param contractFile 机构合同
     * @param enterpriseUser 企业用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseInspectionInstitutionJSON, MultipartFile contractFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseInspectionInstitution enterpriseInspectionInstitution
                = JSONObject.parseObject(enterpriseInspectionInstitutionJSON, EnterpriseInspectionInstitution.class);
        if (enterpriseInspectionInstitution.getId() == null) {
            enterpriseInspectionInstitution.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseInspectionInstitution.setGmtCreate(new Date());
            if (inspectionInstitutionService.insertInspectionInstitution(enterpriseInspectionInstitution
                    , contractFile) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseInspectionInstitution.setGmtModified(new Date());
            if (inspectionInstitutionService.updateInspectionInstitution(enterpriseInspectionInstitution
                    , contractFile) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteInspectionInstitution")
    @ResponseBody
    /**
     * @description 删除检查机构信息
     * @param id 要删除的id
     * @param contract 要删除的机构合同路径
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInspectionInstitution(Long id, String contract) {
        if (inspectionInstitutionService.deleteInspectionInstitution(id, contract) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteInspectionInstitutions")
    @ResponseBody
    /**
     * @description 批量删除检查机构信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteInspectionInstitutions(Long[] ids) {
        if (inspectionInstitutionService.deleteInspectionInstitutions(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
