package com.lanware.ehs.enterprise.account.inspection.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 职业危害检查整改台帐视图
 */
@Controller("inspectionAccountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /**
     * @description 返回整改记录页面
     * @param inspectionId 日常检查id
     * @param rectificationNum 整改记录数目
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping("inspection/rectificationDetail/{inspectionId}/{rectificationNum}")
    public ModelAndView rectificationDetail(@PathVariable Long inspectionId, @PathVariable Integer rectificationNum){
        JSONObject data = new JSONObject();
        data.put("inspectionId", inspectionId);
        data.put("rectificationNum", rectificationNum);
        return new ModelAndView("account/inspection/rectificationDetail", data);
    }
}
