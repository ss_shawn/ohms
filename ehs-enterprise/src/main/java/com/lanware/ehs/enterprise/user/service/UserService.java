package com.lanware.ehs.enterprise.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.SysUser;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;

import javax.mail.MessagingException;

public interface UserService {
    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return 用户
     */
    EnterpriseUserCustom findByName(String username);

    /**
     * 查找用户信息
     *
     * @param request request
     * @param user    用户对象，用于传递查询条件
     * @return IPage
     */
    Page<EnterpriseUserCustom> findUserDetail(EnterpriseUserCustom user, QueryRequest request);


    /**
     * 通过用户名查找用户详细信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    EnterpriseUser findUserDetail(String username);

    /**
     * 新增用户
     *
     * @param user user
     */
    void createUser(EnterpriseUserCustom user) throws MessagingException;

    /**
     * 重置密码
     *
     * @param usernames 用户名数组
     */
    void resetPassword(String[] usernames);

    /**
     * 更新用户状态
     * @param status
     * @param usernames
     */
    void updateSatus(Integer status,String[] usernames);


    /**
     * 修改用户
     *
     * @param user user
     */
    void updateUser(EnterpriseUserCustom user);

    /**
     * 修改密码
     *
     * @param username 用户名
     * @param password 新密码
     */
    void updatePassword(String username, String password);

    /**
     * 更新用户登录时间
     *
     * @param username 用户名
     */
    void updateLoginTime(String username);

}
