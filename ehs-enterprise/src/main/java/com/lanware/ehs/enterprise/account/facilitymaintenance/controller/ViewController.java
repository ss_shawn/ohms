package com.lanware.ehs.enterprise.account.facilitymaintenance.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 防护设施台帐视图
 */
@Controller("facilityMaintenanceAccountView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /**
     * 返回维护设备页面
     * @param recordId 记录id
     * @param type 记录类型(维护记录)
     * @return 维护设备model
     */
    @RequestMapping("facilityMaintenance/serviceEquipmentDetail/{recordId}/{type}")
    public ModelAndView serviceEquipmentDetail(@PathVariable Long recordId, @PathVariable Integer type){
        JSONObject data = new JSONObject();
        data.put("recordId", recordId);
        data.put("type", type);
        return new ModelAndView("account/facilityMaintenance/serviceEquipmentDetail", data);
    }

    /**
     * 返回运行设备页面
     * @param recordId 记录id
     * @param type 记录类型(运行记录)
     * @return 运行设备model
     */
    @RequestMapping("facilityMaintenance/runEquipmentDetail/{recordId}/{type}")
    public ModelAndView runEquipmentDetail(@PathVariable Long recordId, @PathVariable Integer type){
        JSONObject data = new JSONObject();
        data.put("recordId", recordId);
        data.put("type", type);
        return new ModelAndView("account/facilityMaintenance/runEquipmentDetail", data);
    }
}
