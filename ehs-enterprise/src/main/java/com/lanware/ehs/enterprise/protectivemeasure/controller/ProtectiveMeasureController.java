package com.lanware.ehs.enterprise.protectivemeasure.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.protectivemeasure.service.ProtectiveMeasureService;
import com.lanware.ehs.pojo.EnterpriseProtectiveMeasures;
import com.lanware.ehs.pojo.EnterpriseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 防护措施controller
 */
@Controller
@RequestMapping("/protectiveMeasure")
public class ProtectiveMeasureController {
    /** 注入protectiveMeasureService的bean */
    @Autowired
    private ProtectiveMeasureService protectiveMeasureService;

    @RequestMapping("")
    /**
     * @description 返回防护措施页面
     * @return java.lang.String 返回页面路径
     */
    public String list() {
        return "protectiveMeasure/list";
    }

    @RequestMapping("/listProtectiveMeasures")
    @ResponseBody
    /**
     * @description 获取防护措施result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listProtectiveMeasures(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = protectiveMeasureService.listProtectiveMeasures(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/getWorkplace")
    @ResponseBody
    /**
     * @description 根据企业id获取作业场所result
     * @param enterpriseUser 当前登录用户
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat getWorkplace(@CurrentUser EnterpriseUser enterpriseUser) {
        JSONObject result = protectiveMeasureService.listWorkplacesByEnterpriseId(enterpriseUser.getEnterpriseId());
        return ResultFormat.success(result);
    }

    @RequestMapping("/editProtectiveMeasure/save")
    @ResponseBody
    /**
     * @description 保存防护措施，并上传文件到oss服务器
     * @param enterpriseProtectiveMeasureJSON 防护措施json
     * @param operationRuleFile 操作规程
     * @param qualificationRecordFile 验收记录
     * @param acceptanceCertificateFile 合格证书
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat save(String enterpriseProtectiveMeasureJSON, MultipartFile operationRuleFile
            ,MultipartFile qualificationRecordFile, MultipartFile acceptanceCertificateFile
            , @CurrentUser EnterpriseUser enterpriseUser) {
        EnterpriseProtectiveMeasures enterpriseProtectiveMeasure
                = JSONObject.parseObject(enterpriseProtectiveMeasureJSON, EnterpriseProtectiveMeasures.class);
        if (enterpriseProtectiveMeasure.getId() == null) {
            enterpriseProtectiveMeasure.setEnterpriseId(enterpriseUser.getEnterpriseId());
            enterpriseProtectiveMeasure.setGmtCreate(new Date());
            if (protectiveMeasureService.insertProtectiveMeasure(enterpriseProtectiveMeasure
                    , operationRuleFile, qualificationRecordFile, acceptanceCertificateFile) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            enterpriseProtectiveMeasure.setGmtModified(new Date());
            if (protectiveMeasureService.updateProtectiveMeasure(enterpriseProtectiveMeasure
                    , operationRuleFile, qualificationRecordFile, acceptanceCertificateFile) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/deleteProtectiveMeasure")
    @ResponseBody
    /**
     * @description 删除防护措施
     * @param id 要删除的id
     * @param operationRule 要删除的操作规程
     * @param qualificationRecord 要删除的验收记录
     * @param acceptanceCertificate 要删除的合格证书
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteProtectiveMeasure(Long id, String operationRule
            , String qualificationRecord, String acceptanceCertificate) {
        if (protectiveMeasureService.deleteProtectiveMeasure(id, operationRule
                , qualificationRecord, acceptanceCertificate) > 0) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteProtectiveMeasures")
    @ResponseBody
    /**
     * @description 批量删除防护措施
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteProtectiveMeasures(Long[] ids) {
        if (protectiveMeasureService.deleteProtectiveMeasures(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }
}
