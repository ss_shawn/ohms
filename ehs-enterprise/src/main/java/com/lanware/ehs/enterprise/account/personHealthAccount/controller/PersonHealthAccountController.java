package com.lanware.ehs.enterprise.account.personHealthAccount.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.employee.controller.EmployeeArchivesController;
import com.lanware.ehs.enterprise.employee.service.EmployeeArchivesService;
import com.lanware.ehs.enterprise.employee.service.EmployeeService;
import com.lanware.ehs.enterprise.employee.service.HarmContactHistoryService;
import com.lanware.ehs.enterprise.employee.service.MedicalHistoryService;
import com.lanware.ehs.enterprise.inspectioninstitution.service.InspectionInstitutionService;
import com.lanware.ehs.enterprise.medical.service.DiseaseDiagnosiService;
import com.lanware.ehs.enterprise.medical.service.MedicalService;
import com.lanware.ehs.enterprise.medical.service.WaitMedicalService;
import com.lanware.ehs.enterprise.post.service.PostService;
import com.lanware.ehs.enterprise.training.service.TrainingService;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.*;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import com.lanware.ehs.service.EnterpriseEmployeeService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description 从业人员防护用品的发放使用管理台帐controller
 */
@Controller
@RequestMapping("/personHealthAccount")
public class PersonHealthAccountController extends BaseController {
    @Autowired
    private PostService postService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private DiseaseDiagnosiService diseaseDiagnosiService;
    @Autowired
    private InspectionInstitutionService inspectionInstitutionService;
    @Autowired
    private EmployeeArchivesService employeeArchivesService;
    @Autowired
    private EnterpriseEmployeeService enterpriseEmployeeService;
    @Autowired
    private OSSTools ossTools;
    @Autowired
    private EnterpriseBaseinfoService enterpriseBaseinfoService;
    @Autowired
    private HarmContactHistoryService harmContactHistoryService;
    @Autowired
    private MedicalHistoryService medicalHistoryService;
    @Autowired
    private TrainingService trainingService;
    @Autowired
    /** 注入medicalService的bean */
    private MedicalService medicalService;
    @Autowired
    /** 注入waitMedicalService的bean */
    private WaitMedicalService waitMedicalService;


    @Value(value="${upload.tempPath}")
    private String uploadTempPath;
    /**
     * 获取职业健康检查工种、接触的职业危害因素及人员名单
     * @param keyword
     * @param request
     * @return
     *
     *
     * <w:p wsp:rsidP="00786D87" wsp:rsidR="00285B36" wsp:rsidRDefault="00285B36" wsp:rsidRPr="00786D87">
     *     <w:r wsp:rsidRPr="00786D87">
     *         <w:rPr>
     *             <w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体" w:hint="fareast"/>
     *             <wx:font wx:val="宋体"/>
     *         </w:rPr>
     *         <w:t>${index}．<![CDATA[ ${quetion.questionTitle} ]]></w:t>
     *     </w:r>
     * </w:p>

     */
    @RequestMapping("/enterprisePostEmployeeList")
    @ResponseBody
    public EhsResult enterprisePostEmployeeList(String keyword, QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.postService.queryEnterprisePostEmployeeByCondition(condition,request));
        return EhsResult.ok(dataTable);
    }
    /**
     * 获取职业病检出名单和职业禁忌症名单后续安置情况
     * @param keyword
     * @param request
     * @return
     */
    @RequestMapping("/enterpriseDiseaseDiagnosiList")
    @ResponseBody
    public EhsResult enterpriseDiseaseDiagnosiList(String keyword, QueryRequest request, @CurrentUser EnterpriseUserCustom currentUser) {
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",currentUser.getEnterpriseId());
        //过滤模糊查询
        condition.put("keyword",keyword);
        //查出所有职业病检出名单和职业禁忌症名单
//        condition.put("all",1);
        //查询并返回
        Map<String, Object> dataTable = getDataTable(this.diseaseDiagnosiService.queryEnterpriseDiseaseDiagnosiByCondition(condition,request));
        return EhsResult.ok(dataTable);
    }

    //下载体检机构清单
    @RequestMapping("/inspectionInstitutionDownload")
    @ResponseBody
    public void inspectionInstitutionDownload(HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            Map<String, Object> dataMap =getInspectionInstitutionMap(currentUser.getEnterpriseId());
            String uploadPath = upload + File.separator+"体检机构清单.doc";
            exeWord(dataMap,uploadPath,"inspectionInstitution.ftl");

            File uploadFile=new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("体检机构清单.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    //体检机构清单MAP
    private Map getInspectionInstitutionMap(long enterpriseId){
        Map<String, Object> dataMap = new HashMap<String, Object>();

        JSONObject result = inspectionInstitutionService.listInspectionInstitutionsByType("体检机构",enterpriseId);
        List<EnterpriseInspectionInstitution> enterpriseInspectionInstitutionList= (List<EnterpriseInspectionInstitution>) result.get("enterpriseInspectionInstitutions");
        List  inspectionInstitutionList=new ArrayList();
        for(EnterpriseInspectionInstitution enterpriseInspectionInstitution:enterpriseInspectionInstitutionList){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name",enterpriseInspectionInstitution.getName());
            temp.put("contacts",enterpriseInspectionInstitution.getContacts());
            temp.put("mobilePhone",enterpriseInspectionInstitution.getMobilePhone());
            inspectionInstitutionList.add(temp);
        }
        dataMap.put("inspectionInstitutionList",inspectionInstitutionList);
        return dataMap;
    }

    //下载职业健康检查工种、接触的职业危害因素及人员名单
    @RequestMapping("/enterprisePostEmployeeDownload")
    @ResponseBody
    public void enterprisePostEmployeeDownload(HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            Map<String, Object> dataMap =getEnterprisePostEmployeeMap(currentUser.getEnterpriseId(),request);
            String uploadPath = upload +  File.separator+"涉害人员名单.doc";
            exeWord(dataMap,uploadPath,"enterprisePostEmployee.ftl");

            File uploadFile=new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("涉害人员名单.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    //涉害人员名单MAP
    private Map getEnterprisePostEmployeeMap(long enterpriseId,QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);

        //查询并返回
        Page<PostEmployeeResultCustom> postEmployeeResultCustomPage=this.postService.queryEnterprisePostEmployeeByCondition(condition,request);
        List enterprisePostEmployeeList = new ArrayList();
        for(PostEmployeeResultCustom postEmployeeResultCustom:postEmployeeResultCustomPage){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("postName",postEmployeeResultCustom.getPostName());
            temp.put("harmFactorName",postEmployeeResultCustom.getHarmFactorName()==null?"":postEmployeeResultCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
            temp.put("employeeName",postEmployeeResultCustom.getEmployeeName());
            enterprisePostEmployeeList.add(temp);
        }
        dataMap.put("enterprisePostEmployeeList",enterprisePostEmployeeList);
        return dataMap;
    }

    //下载职业健康检查结果与分析报告
    @RequestMapping("/medicalDownload")
    @ResponseBody
    public void medicalDownload(String medical_list_keyword,HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {

            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            Map<String, Object> dataMap =getMedicalMap(currentUser.getEnterpriseId(),request,medical_list_keyword);
            String uploadPath = upload + File.separator+"体检记录表.doc";
            exeWord(dataMap,uploadPath,"medical.ftl");

            File uploadFile=new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("体检记录表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    //体检记录MAP
    private Map getMedicalMap(long enterpriseId,QueryRequest request,String medical_list_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new HashMap<>();
        condition.put("enterpriseId", enterpriseId);
        condition.put("keyword",medical_list_keyword);
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject result = medicalService.listMedicals(null, null, condition);
        List<EmployeeMedicalCustom> employeeMedicals= (List<EmployeeMedicalCustom>) result.get("employeeMedicals");
        List  employeeList=new ArrayList();
        for(EmployeeMedicalCustom employeeMedicalCustom:employeeMedicals){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("employeeName",employeeMedicalCustom.getEmployeeName());
            temp.put("postName",employeeMedicalCustom.getPostName());
            temp.put("medicalDate",sdf1.format(employeeMedicalCustom.getMedicalDate()));
            temp.put("institutionName",employeeMedicalCustom.getInstitutionName());
            temp.put("type",employeeMedicalCustom.getType());
            temp.put("result",employeeMedicalCustom.getResult());
            employeeList.add(temp);
        }
        dataMap.put("employeeList",employeeList);
        return dataMap;
    }

    //下载职业健康监护汇总表
    @RequestMapping("/diseaseDiagnosiDownload")
    @ResponseBody
    public void diseaseDiagnosiDownload(String diseaseDiagnosi_list_keyword,HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;

        String uuid= UUID.randomUUID().toString();
        try {

            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            Map<String, Object> dataMap =getDiseaseDiagnosiMap(currentUser.getEnterpriseId(),request,diseaseDiagnosi_list_keyword);
            String uploadPath = upload +  File.separator+"职业病诊断记录.doc";
            exeWord(dataMap,uploadPath,"diseaseDiagnosi.ftl");

            File uploadFile=new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业病诊断记录.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    //职业健康监护汇总表MAP
    private Map getDiseaseDiagnosiMap(long enterpriseId,QueryRequest request,String diseaseDiagnosi_list_keyword){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        //过滤当前企业
        condition.put("enterpriseId",enterpriseId);
        condition.put("keyword",diseaseDiagnosi_list_keyword);
        //查出所有职业病检出名单和职业禁忌症名单
        condition.put("all",1);
        //查询并返回
        Page<EmployeeDiseaseDiagnosiCustom> employeeDiseaseDiagnosiCustomPage= this.diseaseDiagnosiService.queryEnterpriseDiseaseDiagnosiByCondition(condition,request);
        List  diseaseDiagnosiList=new ArrayList();
        for(EmployeeDiseaseDiagnosiCustom employeeDiseaseDiagnosiCustom:employeeDiseaseDiagnosiCustomPage){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("postName",employeeDiseaseDiagnosiCustom.getPostName());
            temp.put("employeeName",employeeDiseaseDiagnosiCustom.getEmployeeName());
            temp.put("name",employeeDiseaseDiagnosiCustom.getName());
            temp.put("isCertain",employeeDiseaseDiagnosiCustom.getIsCertain()==0?"否":"是");
            temp.put("isTaboo",employeeDiseaseDiagnosiCustom.getIsTaboo()==null?"":(employeeDiseaseDiagnosiCustom.getIsTaboo()==0?"否":"是"));
            temp.put("isTransferred",employeeDiseaseDiagnosiCustom.getIsTransferred()==null?"":(employeeDiseaseDiagnosiCustom.getIsTransferred()==0?"否":"是"));
            temp.put("hospital",employeeDiseaseDiagnosiCustom.getHospital());
            temp.put("diagnosiDate",sdf1.format(employeeDiseaseDiagnosiCustom.getDiagnosiDate()));
            diseaseDiagnosiList.add(temp);
        }
        dataMap.put("diseaseDiagnosiList",diseaseDiagnosiList);
        return dataMap;
    }

    //下载职业健康检查统计年度报表
    @RequestMapping("/yearWaitMedicalDownload")
    @ResponseBody
    public void yearWaitMedicalDownload(String startDate, String endDate, HttpServletResponse response
            , QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload=null;
        List<String> files=new ArrayList<String>();
        String uuid= UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();

            //返回word的变量
            Map<String, Object> dataMap = getYearWaitMedicalMap(currentUser.getEnterpriseId(),startDate,endDate);
            String uploadPath = upload + File.separator+"职业健康检查统计年度报表.doc";
            exeWord(dataMap,uploadPath,"yearWaitMedical.ftl");

            File uploadFile=new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业健康检查统计年度报表.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    //职业健康检查统计年度报表MAP
    private Map getYearWaitMedicalMap(long enterpriseId, String startDate, String endDate){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        condition.put("enterpriseId", enterpriseId);
        if (startDate != null && !startDate.equals("")) {
            condition.put("startDate", startDate);
        }
        if (endDate != null && !endDate.equals("")) {
            condition.put("endDate", endDate);
        }
        JSONObject result = waitMedicalService.listYearWaitMedicals(condition);
        List<EmployeeWaitMedicalCustom> yearWaitMedicals = (List<EmployeeWaitMedicalCustom>) result.get("yearWaitMedicals");
        List yearWaitMedicalList=new ArrayList();
        for(EmployeeWaitMedicalCustom employeeWaitMedicalCustom : yearWaitMedicals){
            Map<String, Object> temp = new HashMap<String, Object>();
            String duration = startDate + " -- " + endDate;
            temp.put("year",duration);
            temp.put("type",employeeWaitMedicalCustom.getType());
            temp.put("employeeNum",employeeWaitMedicalCustom.getEmployeeNum());
            temp.put("shouldMedicalNum",employeeWaitMedicalCustom.getShouldMedicalNum());
            temp.put("actualMedicalNum",employeeWaitMedicalCustom.getActualMedicalNum());
            String medicalRate = "100";
            if (employeeWaitMedicalCustom.getShouldMedicalNum() > 0 ) {
                medicalRate = 100 * employeeWaitMedicalCustom.getActualMedicalNum() / employeeWaitMedicalCustom.getShouldMedicalNum() + "";
            }
            temp.put("medicalRate",medicalRate);
            temp.put("dustShouldMedicalNum",employeeWaitMedicalCustom.getDustShouldMedicalNum());
            temp.put("dustActualMedicalNum",employeeWaitMedicalCustom.getDustActualMedicalNum());
            String dustMedicalRate = "100";
            if (employeeWaitMedicalCustom.getDustShouldMedicalNum() > 0) {
                dustMedicalRate = 100 * employeeWaitMedicalCustom.getDustActualMedicalNum() / employeeWaitMedicalCustom.getDustShouldMedicalNum() + "";
            }
            temp.put("dustMedicalRate",dustMedicalRate);
            temp.put("chemicalShouldMedicalNum",employeeWaitMedicalCustom.getChemicalShouldMedicalNum());
            temp.put("chemicalActualMedicalNum",employeeWaitMedicalCustom.getChemicalActualMedicalNum());
            String chemicalMedicalRate = "100";
            if (employeeWaitMedicalCustom.getChemicalActualMedicalNum() > 0) {
                chemicalMedicalRate = 100 * employeeWaitMedicalCustom.getChemicalActualMedicalNum() / employeeWaitMedicalCustom.getChemicalShouldMedicalNum() + "";
            }
            temp.put("chemicalMedicalRate",chemicalMedicalRate);
            temp.put("physicalShouldMedicalNum",employeeWaitMedicalCustom.getPhysicalShouldMedicalNum());
            temp.put("physicalActualMedicalNum",employeeWaitMedicalCustom.getPhysicalActualMedicalNum());
            String physicalMedicalRate = "100";
            if (employeeWaitMedicalCustom.getPhysicalActualMedicalNum() > 0) {
                physicalMedicalRate =100 * employeeWaitMedicalCustom.getPhysicalActualMedicalNum() / employeeWaitMedicalCustom.getPhysicalShouldMedicalNum() + "";
            }
            temp.put("physicalMedicalRate",physicalMedicalRate);
            temp.put("radiationShouldMedicalNum",employeeWaitMedicalCustom.getRadiationShouldMedicalNum());
            temp.put("radiationActualMedicalNum",employeeWaitMedicalCustom.getRadiationActualMedicalNum());
            String radiationMedicalRate = "100";
            if (employeeWaitMedicalCustom.getRadiationActualMedicalNum() > 0) {
                radiationMedicalRate = 100 * employeeWaitMedicalCustom.getRadiationActualMedicalNum() / employeeWaitMedicalCustom.getRadiationShouldMedicalNum() + "";
            }
            temp.put("radiationMedicalRate",radiationMedicalRate);
            temp.put("biologicalShouldMedicalNum",employeeWaitMedicalCustom.getBiologicalShouldMedicalNum());
            temp.put("biologicalActualMedicalNum",employeeWaitMedicalCustom.getBiologicalActualMedicalNum());
            String biologicalMedicalRate = "100";
            if (employeeWaitMedicalCustom.getBiologicalActualMedicalNum() > 0) {
                biologicalMedicalRate = 100 * employeeWaitMedicalCustom.getBiologicalActualMedicalNum() / employeeWaitMedicalCustom.getBiologicalShouldMedicalNum() + "";
            }
            temp.put("biologicalMedicalRate",biologicalMedicalRate);
            temp.put("otherShouldMedicalNum",employeeWaitMedicalCustom.getOtherShouldMedicalNum());
            temp.put("otherActualMedicalNum",employeeWaitMedicalCustom.getOtherActualMedicalNum());
            String otherMedicalRate = "100";
            if (employeeWaitMedicalCustom.getPhysicalActualMedicalNum() > 0) {
                otherMedicalRate = 100 * employeeWaitMedicalCustom.getPhysicalActualMedicalNum() / employeeWaitMedicalCustom.getPhysicalShouldMedicalNum() + "";
            }
            temp.put("otherMedicalRate",otherMedicalRate);
            yearWaitMedicalList.add(temp);
        }
        dataMap.put("yearWaitMedicalList",yearWaitMedicalList);
        return dataMap;
    }

    /**
     * @description 体检人数word
     * @param currentUser 当前登录企业
     * @param response
     * @param request
     */
    @RequestMapping("/lastYearMedicalPersonDownload")
    @ResponseBody
    public void lastYearMedicalPersonDownload(@CurrentUser EnterpriseUser currentUser, HttpServletResponse response, QueryRequest request){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        try {
            //主文件路径
            upload = new File(uploadTempPath, currentUser.getEnterpriseId()+File.separator+uuid+File.separator);
            if (!upload.exists()) upload.mkdirs();
            //返回word的变量
            Map<String, Object> dataMap = getMedicalPersonMap(currentUser.getEnterpriseId(),request);
            String uploadPath = upload + File.separator +"体检人数.doc";
            exeWord(dataMap,uploadPath,"medicalPerson.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("体检人数.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到体检人数的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getMedicalPersonMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 获取上年度
        Calendar calendar = Calendar.getInstance();
        Integer lastYear = calendar.get(calendar.YEAR)  - 1;
        condition.put("lastYear", lastYear);
        // 查询并返回
        JSONObject result = medicalService.listMedicalPersonNum(condition);
        List<EmployeeMedicalCustom> employeeMedicalCustoms
                = (List<EmployeeMedicalCustom>)result.get("employeeMedicalCustoms");
        List medicalPersonList = new ArrayList();
        for (EmployeeMedicalCustom employeeMedicalCustom : employeeMedicalCustoms) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("beforePostNum",employeeMedicalCustom.getBeforePostNum());
            temp.put("postingNum",employeeMedicalCustom.getPostingNum());
            temp.put("afterPostNum",employeeMedicalCustom.getAfterPostNum());
            temp.put("occupationalDiseaseNum",employeeMedicalCustom.getOccupationalDiseaseNum());
            temp.put("transferPostNum",employeeMedicalCustom.getTransferPostNum());
            medicalPersonList.add(temp);
        }
        dataMap.put("medicalPersonList", medicalPersonList);
        return dataMap;
    }


    /**
     * 生成word
     * @param dataMap 模板内参数
     * @param uploadPath 生成word全路径
     * @param ftlName 模板名称 在/templates/ftl下面
     */
    private void exeWord( Map<String, Object> dataMap,String uploadPath,String ftlName ){
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(PersonHealthAccountController.class,"/templates/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName);
            //输出文件
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }


    //下载职业健康监护档案
    @RequestMapping("/employeeDownload")
    @ResponseBody
    public void employeeDownload(HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        Map<String, Object> condition = new HashMap<>();
        List<String> files=new ArrayList<String>();
        condition.put("enterpriseId", currentUser.getEnterpriseId());
        String uuid= UUID.randomUUID().toString();
        File upload=upload = new File(uploadTempPath, currentUser.getEnterpriseId()+File.separator);
        if (!upload.exists()) upload.mkdirs();

        Page<EnterpriseEmployeeCustom> page = employeeService.queryEnterpriseEmployeeByCondition(0, 0, condition);
        for(EnterpriseEmployeeCustom enterpriseEmployeeCustom:page){
            String employyeePath=downloadThreeinquiryDoc(enterpriseEmployeeCustom.getId(),upload);
            files.add(employyeePath);
        }
        try {
            String uploadZipPath = upload + File.separator+"职业健康监护档案.zip";
            FileUtil.toZip(files, uploadZipPath, true);


            //下载文件
            File uploadFile=new File(uploadZipPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("职业健康监护档案.zip", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }
    //下载从业人员职业健康监护档案台帐
    @RequestMapping("/personHealthAccountDownload")
    @ResponseBody
    public void personHealthAccountDownload(String medical_list_keyword,String diseaseDiagnosi_list_keyword,HttpServletResponse response, QueryRequest request
            , @CurrentUser EnterpriseUser currentUser, String startDate, String endDate){
        Map<String, Object> condition = new HashMap<>();
        List<String> files=new ArrayList<String>();
        condition.put("enterpriseId", currentUser.getEnterpriseId());
        String uuid= UUID.randomUUID().toString();
        File upload=upload = new File(uploadTempPath, currentUser.getEnterpriseId()+File.separator+uuid+File.separator);
        if (!upload.exists()) upload.mkdirs();
        //从业人员职业健康监护档案每个生成word
        Page<EnterpriseEmployeeCustom> page = employeeService.queryEnterpriseEmployeeByCondition(0, 0, condition);
        for(EnterpriseEmployeeCustom enterpriseEmployeeCustom:page){
            String employyeePath=downloadThreeinquiryDoc(enterpriseEmployeeCustom.getId(),upload);
            files.add(employyeePath);
        }
        try {
            //从业人员职业健康监护档案word压缩
            String uploadZipPath = upload + File.separator+"职业健康监护档案.zip";
            FileUtil.toZip(files, uploadZipPath, true);
            //从业人员职业健康监护档案
            files=new ArrayList<String>();
            files.add(uploadZipPath);
            //体检机构清单
            //返回word的变量
            Map<String, Object> dataMap =getInspectionInstitutionMap(currentUser.getEnterpriseId());
            String uploadPath = upload +  File.separator+"体检机构清单.doc";
            exeWord(dataMap,uploadPath,"inspectionInstitution.ftl");
            files.add(uploadPath);
            //涉害人员名单
            //返回word的变量
            dataMap =getEnterprisePostEmployeeMap(currentUser.getEnterpriseId(),request);
             uploadPath = upload +  File.separator+"涉害人员名单.doc";
            exeWord(dataMap,uploadPath,"enterprisePostEmployee.ftl");
            files.add(uploadPath);
            //体检记录表
            //返回word的变量
            dataMap =getMedicalMap(currentUser.getEnterpriseId(),request,medical_list_keyword);
            uploadPath = upload + File.separator+"体检记录表.doc";
            exeWord(dataMap,uploadPath,"medical.ftl");
            files.add(uploadPath);

            //职业健康监护汇总表
            //返回word的变量
            dataMap =getDiseaseDiagnosiMap(currentUser.getEnterpriseId(),request,diseaseDiagnosi_list_keyword);
            uploadPath = upload + File.separator+"职业病诊断记录.doc";
            exeWord(dataMap,uploadPath,"diseaseDiagnosi.ftl");
            files.add(uploadPath);

            //职业健康检查统计年度报表
            //返回word的变量
            dataMap = getYearWaitMedicalMap(currentUser.getEnterpriseId(),startDate,endDate);
            uploadPath = upload + File.separator+"职业健康检查统计年度报表.doc";
            exeWord(dataMap,uploadPath,"yearWaitMedical.ftl");
            files.add(uploadPath);

            //体检人数表
            //返回word的变量
            dataMap = getMedicalPersonMap(currentUser.getEnterpriseId(),request);
            uploadPath = upload + File.separator+"体检人数.doc";
            exeWord(dataMap,uploadPath,"medicalPerson.ftl");
            files.add(uploadPath);

            //从业人员职业健康监护档案word压缩
            uploadZipPath = upload +  File.separator+"从业人员职业健康监护档案台帐.zip";
            FileUtil.toZip(files, uploadZipPath, false);

            //下载文件
            File uploadFile=new File(uploadZipPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);

            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("从业人员职业健康监护档案台帐.zip", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    //一人一挡
    public String downloadThreeinquiryDoc(Long id,File upload){
        String uploadPath=null;
        List<String> files=new ArrayList<String>();
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            EnterpriseEmployee enterpriseEmployee = enterpriseEmployeeService.selectByPrimaryKey(id);

            EnterpriseBaseinfo enterpriseBaseinfo = enterpriseBaseinfoService.selectByPrimaryKey(enterpriseEmployee.getEnterpriseId());
            //返回word的变量
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("enterpriseName",enterpriseBaseinfo.getName());
            dataMap.put("id", String.format("%06d", id));
            dataMap.put("archiveNumber",enterpriseEmployee.getElectronArchiveNumber());
            dataMap.put("buildTime",enterpriseEmployee.getBuildTime()==null?"":sdf1.format(enterpriseEmployee.getBuildTime()));
            dataMap.put("name",enterpriseEmployee.getName());
            dataMap.put("sex",enterpriseEmployee.getSex()==0?"男":"女");
            dataMap.put("nativePlace",enterpriseEmployee.getNativePlace());
            dataMap.put("isMarried",enterpriseEmployee.getIsMarried()==null?"":(enterpriseEmployee.getIsMarried()==0?"未婚":"已婚"));
            dataMap.put("educationDegree",enterpriseEmployee.getEducationDegree());
            dataMap.put("hobby",enterpriseEmployee.getHobby());
            dataMap.put("attendWorkTime",enterpriseEmployee.getAttendWorkTime()==null?"":sdf1.format(enterpriseEmployee.getAttendWorkTime()));
            dataMap.put("idCard",enterpriseEmployee.getIdCard());
            //主文件路径



            if(enterpriseEmployee.getPhoto()!=null&&!"".equals(enterpriseEmployee.getPhoto())){
                //下载照片
                ossTools.downloadToLocal(enterpriseEmployee.getPhoto(),upload+"photo.jpg");
                //设置照片
                dataMap.put("photo",getImageBase(upload+"photo.jpg"));
                //删除照片
                File photoFile = new File(upload+"photo.jpg");
                photoFile.delete();
            }else{//使用默认图片
                ClassPathResource classPathResource = new ClassPathResource("static/xadmin/images/dsc.png");
                dataMap.put("photo",getImageBase(classPathResource.getInputStream()));
            }



            //职业史及职业病危害接触史
            Map<String, Object> condition = new HashMap<>();
            condition.put("employeeId", id);
            Page<EmployeeHarmContactHistoryCustom> page = harmContactHistoryService.queryEmployeeHarmContactHistoryByCondition(0, 0, condition);
            List harmContactHistoryList=new ArrayList();
            for(EmployeeHarmContactHistoryCustom employeeHarmContactHistoryCustom:page){
                Map<String, Object> temp = new HashMap<String, Object>();
                if(employeeHarmContactHistoryCustom.getStatus()==1){

                    temp.put("startDate",sdf1.format(employeeHarmContactHistoryCustom.getStarttime2())+"—"+(employeeHarmContactHistoryCustom.getEndtime2()==null?"至今":sdf1.format(employeeHarmContactHistoryCustom.getEndtime2())) );

                    temp.put("enterpriseName",employeeHarmContactHistoryCustom.getEnterpriseName2());
                    temp.put("postName",employeeHarmContactHistoryCustom.getPostName2());
                    temp.put("harmFactorName",employeeHarmContactHistoryCustom.getHarmFactorName2()==null?"":employeeHarmContactHistoryCustom.getHarmFactorName2().replace("<","&lt;").replace(">","&gt;"));
                    String protectArticlesName="";
                    if(employeeHarmContactHistoryCustom.getProtectArticlesName2()!=null){
                        protectArticlesName="防护用品："+employeeHarmContactHistoryCustom.getProtectArticlesName2()+"。";
                    }
                    if(employeeHarmContactHistoryCustom.getProtectiveMeasuresName2()!=null){
                        protectArticlesName+="防护设施："+employeeHarmContactHistoryCustom.getProtectiveMeasuresName2()+"。";
                    }
                    temp.put("protectArticlesName",protectArticlesName);
                }else{
                    temp.put("startDate",sdf1.format(employeeHarmContactHistoryCustom.getStarttime())+"—"+(employeeHarmContactHistoryCustom.getEndtime()==null?"至今":sdf1.format(employeeHarmContactHistoryCustom.getEndtime())) );
                    temp.put("enterpriseName",employeeHarmContactHistoryCustom.getEnterpriseName());
                    temp.put("postName",employeeHarmContactHistoryCustom.getPostName());
                    temp.put("harmFactorName",employeeHarmContactHistoryCustom.getHarmFactorName()==null?"":employeeHarmContactHistoryCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                    String protectArticlesName="";
                    if(employeeHarmContactHistoryCustom.getProtectArticlesName()!=null){
                        protectArticlesName="防护用品："+employeeHarmContactHistoryCustom.getProtectArticlesName()+"。";
                    }
                    if(employeeHarmContactHistoryCustom.getProtectiveMeasuresName()!=null){
                        protectArticlesName+="防护设施："+employeeHarmContactHistoryCustom.getProtectiveMeasuresName()+"。";
                    }
                    temp.put("protectArticlesName",protectArticlesName);
                }
                harmContactHistoryList.add(temp);
            }
            dataMap.put("harmContactHistoryList",harmContactHistoryList);

            //职业病史
            condition = new HashMap<>();
            condition.put("employeeId", id);
            condition.put("diseaseType", 0);
            Page<EmployeeMedicalHistoryCustom> employeeMedicalHistoryPage = medicalHistoryService.queryEmployeeMedicalHistoryByCondition(0, 0, condition);
            List  employeeMedicalHistoryList=new ArrayList();
            for(EmployeeMedicalHistoryCustom employeeMedicalHistoryCustom:employeeMedicalHistoryPage){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("diseaseName",employeeMedicalHistoryCustom.getDiseaseName());
                temp.put("diagnosiDate",employeeMedicalHistoryCustom.getDiagnosiDate()==null?"":sdf1.format(employeeMedicalHistoryCustom.getDiagnosiDate()));
                temp.put("diagnosiHosipital",employeeMedicalHistoryCustom.getDiagnosiHosipital());
                temp.put("diagnosiResult",employeeMedicalHistoryCustom.getDiagnosiResult());
                temp.put("remark",employeeMedicalHistoryCustom.getRemark());
                employeeMedicalHistoryList.add(temp);
            }
            dataMap.put("employeeMedicalHistoryList",employeeMedicalHistoryList);
            //职业禁忌症史
            condition = new HashMap<>();
            condition.put("employeeId", id);
            condition.put("diseaseType", 1);
            employeeMedicalHistoryPage = medicalHistoryService.queryEmployeeMedicalHistoryByCondition(0, 0, condition);
            List  employeeMedicalHistoryList2=new ArrayList();
            for(EmployeeMedicalHistoryCustom employeeMedicalHistoryCustom:employeeMedicalHistoryPage){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("diseaseName",employeeMedicalHistoryCustom.getDiseaseName());
                temp.put("diagnosiDate",employeeMedicalHistoryCustom.getDiagnosiDate()==null?"":sdf1.format(employeeMedicalHistoryCustom.getDiagnosiDate()));
                temp.put("diagnosiHosipital",employeeMedicalHistoryCustom.getDiagnosiHosipital());
                temp.put("diagnosiResult",employeeMedicalHistoryCustom.getDiagnosiResult());
                temp.put("remark",employeeMedicalHistoryCustom.getRemark());
                employeeMedicalHistoryList2.add(temp);
            }
            dataMap.put("employeeMedicalHistoryList2",employeeMedicalHistoryList2);
            //职业病诊断
            List<EmployeeDiseaseDiagnosi> employeeDiseaseDiagnosiList = employeeArchivesService.getEmployeeDiseaseDiagnosiByEmployeeId(id);
            List  employeeDiseaseDiagnosiList2=new ArrayList();
            for(EmployeeDiseaseDiagnosi employeeMedicalHistoryCustom:employeeDiseaseDiagnosiList){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("name",employeeMedicalHistoryCustom.getName());
                temp.put("diagnosiDate",employeeMedicalHistoryCustom.getDiagnosiDate()==null?"":sdf1.format(employeeMedicalHistoryCustom.getDiagnosiDate()));
                temp.put("hospital",employeeMedicalHistoryCustom.getHospital());
                temp.put("diagnosiLevel",employeeMedicalHistoryCustom.getDiagnosiLevel()==null?"":employeeMedicalHistoryCustom.getDiagnosiLevel());
                temp.put("remark",employeeMedicalHistoryCustom.getRemark()==null?"":employeeMedicalHistoryCustom.getRemark());
                employeeDiseaseDiagnosiList2.add(temp);
            }
            dataMap.put("employeeDiseaseDiagnosiList",employeeDiseaseDiagnosiList2);
            //工作场所职业病危害因素检测结果

            List  enterpriseOutsideCheckResultAllCustomList2=new ArrayList();

            List<EnterpriseOutsideCheckResultAllCustom> enterpriseOutsideCheckResultAllCustomList = employeeArchivesService.getWorkplaceOutsideCheckByEmployeeId(id);
            for(EnterpriseOutsideCheckResultAllCustom enterpriseOutsideCheckResultAllCustom:enterpriseOutsideCheckResultAllCustomList){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("postName",enterpriseOutsideCheckResultAllCustom.getPostName());
                temp.put("checkDate",enterpriseOutsideCheckResultAllCustom.getCheckDate()==null?"":sdf1.format(enterpriseOutsideCheckResultAllCustom.getCheckDate()));
                temp.put("checkOrganization",enterpriseOutsideCheckResultAllCustom.getCheckOrganization());
                temp.put("harmFactorName",enterpriseOutsideCheckResultAllCustom.getHarmFactorName()==null?"":enterpriseOutsideCheckResultAllCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                temp.put("placeConsistence",enterpriseOutsideCheckResultAllCustom.getIsQualified()==null?"":enterpriseOutsideCheckResultAllCustom.getIsQualified());
                String protectArticlesName="";
                if(enterpriseOutsideCheckResultAllCustom.getProtectArticlesName()!=null){
                    protectArticlesName+="防护用品："+enterpriseOutsideCheckResultAllCustom.getProtectArticlesName();
                }
                if(enterpriseOutsideCheckResultAllCustom.getProtectiveMeasuresName()!=null){
                    protectArticlesName+="防护设施："+enterpriseOutsideCheckResultAllCustom.getProtectiveMeasuresName();
                }
                temp.put("protectArticlesName",protectArticlesName);
                temp.put("remark","");
                enterpriseOutsideCheckResultAllCustomList2.add(temp);
            }
            dataMap.put("enterpriseOutsideCheckResultAllCustomList",enterpriseOutsideCheckResultAllCustomList2);

            //历次职业健康检查结果及处理情况
            List<EmployeeMedicalCustom> employeeMedicalCustomlist = employeeArchivesService.getEmployeeMedicalByEmployeeId(id);
            List  employeeMedicalCustomlist2=new ArrayList();
            File report=null;

            //压缩列表

            for(EmployeeMedicalCustom employeeMedicalCustom:employeeMedicalCustomlist){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("medicalDate",employeeMedicalCustom.getMedicalDate()==null?"":sdf1.format(employeeMedicalCustom.getMedicalDate()));
                temp.put("type",employeeMedicalCustom.getType()==null?"":employeeMedicalCustom.getType());
                temp.put("result",employeeMedicalCustom.getResult()==null?"":employeeMedicalCustom.getResult());
                temp.put("institutionName",employeeMedicalCustom.getInstitutionName()==null?"":employeeMedicalCustom.getInstitutionName());
                temp.put("postName",employeeMedicalCustom.getPostName()==null?"":employeeMedicalCustom.getPostName());
                temp.put("personnelHandling",employeeMedicalCustom.getPersonnelHandling()==null?"":employeeMedicalCustom.getPersonnelHandling());
                temp.put("medicalNotifyNum",employeeMedicalCustom.getMedicalNotifyNum()>0?"已签":"未签");
                temp.put("sceneHanding",employeeMedicalCustom.getSceneHanding()==null?"":employeeMedicalCustom.getSceneHanding());
                employeeMedicalCustomlist2.add(temp);
            }
            dataMap.put("employeeMedicalCustomlist",employeeMedicalCustomlist2);


            //获取培训管理result
            List  employeeTrainingList=new ArrayList();
            condition = new HashMap<String, Object>();
            condition.put("enterpriseId", enterpriseEmployee.getEnterpriseId());
            condition.put("employeeId", id);
            JSONObject result = trainingService.listTrainings(null, null, condition);
            List<EnterpriseTrainingCustom> enterpriseTrainings=(List<EnterpriseTrainingCustom>)result.get("enterpriseTrainings");
            for(EnterpriseTrainingCustom enterpriseTrainingCustom:enterpriseTrainings){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("trainingDate",enterpriseTrainingCustom.getTrainingDate()==null?"":sdf1.format(enterpriseTrainingCustom.getTrainingDate()));
                temp.put("duration",enterpriseTrainingCustom.getDuration());
                temp.put("place",enterpriseTrainingCustom.getPlace());
                temp.put("theme",enterpriseTrainingCustom.getTheme());
                temp.put("lecturer",enterpriseTrainingCustom.getLecturer());

                employeeTrainingList.add(temp);
            }
            dataMap.put("employeeTrainingList",employeeTrainingList);
            //防护用品发放记录
            condition=new HashMap();
            condition.put("employeeId",id);
            condition.put("enterpriseId",enterpriseEmployee.getEnterpriseId());
            condition.put("orderByClause","gmt_create desc");
            Page<EnterpriseProvideProtectArticlesCustom> enterpriseProvideProtectArticlesCustomList = employeeArchivesService.getEmployeeProvideProtectList(condition);
            List  employeeProvideProtectList=new ArrayList();
            for(EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticlesCustom:enterpriseProvideProtectArticlesCustomList){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("postName",enterpriseProvideProtectArticlesCustom.getPostName());
                temp.put("provideDate",enterpriseProvideProtectArticlesCustom.getProvideDate()==null?"":sdf1.format(enterpriseProvideProtectArticlesCustom.getProvideDate()));
                temp.put("provideArticlesName",enterpriseProvideProtectArticlesCustom.getProvideArticlesName());
                temp.put("number",enterpriseProvideProtectArticlesCustom.getNumber());

                employeeProvideProtectList.add(temp);
            }
            dataMap.put("employeeProvideProtectList",employeeProvideProtectList);

            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件

            configuration.setClassForTemplateLoading(EmployeeArchivesController.class,"/templates/ftl");

            //获取模板
            Template template = configuration.getTemplate("employeeArchivesMould.ftl");
            //输出文件

            uploadPath = upload + File.separator+enterpriseEmployee.getName()+"个人健康监护档案"+enterpriseEmployee.getId()+".doc";
            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);

            //关闭流
            out.flush();
            out.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return uploadPath;
    }
    //获得图片的base64码
    public static String getImageBase(String src) throws Exception {
        if (src == null || src == "") {
            return "";
        }
        File file = new File(src);
        if (!file.exists()) {
            return "";
        }
        InputStream in = null;
        byte[] data = null;
        try {
            in = new FileInputStream(file);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }

    public static String getImageBase(InputStream in) throws Exception {
        byte[] data = null;
        try {
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }


    /**
     * @description 培训台账文件下载
     * @param response
     * @param request
     * @param currentUser 当前登录用户
     */
    @RequestMapping("/downloadPersonHealthAccount")
    @ResponseBody
    public void downloadPersonHealthAccount(HttpServletResponse response, QueryRequest request, @CurrentUser EnterpriseUser currentUser){
        File upload = null;
        String uuid = UUID.randomUUID().toString();
        Long enterpriseId = currentUser.getEnterpriseId();
        try {
            // 主文件路径
            upload = new File(uploadTempPath, enterpriseId + File.separator + uuid + File.separator);
            if (!upload.exists()) upload.mkdirs();

            //设置不分页
            request.setPageSize(0);
            // 返回word的变量
            Map<String, Object> dataMap = getPersonHealthAccountMap(enterpriseId,request);
            String uploadPath = upload + File.separator + "从业人员职业健康监护档案台帐.doc";
            exeWord(dataMap,uploadPath,"personHealthAccount.ftl");
            File uploadFile = new File(uploadPath);
            FileInputStream inputFile = new FileInputStream(uploadFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("从业人员职业健康监护档案台帐.doc", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 删除所有文件
        FileUtil.delFolder(upload.getPath());
    }

    /**
     * @description 得到从业人员职业健康监护档案台帐文件的dataMap
     * @param enterpriseId 当前登录企业id
     * @param request
     * @return Map
     */
    private Map getPersonHealthAccountMap(long enterpriseId, QueryRequest request){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        Map<String, Object> condition = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 过滤当前企业
        condition.put("enterpriseId", enterpriseId);
        // 从业人员职业健康监护档案
        Page<EnterpriseEmployeeCustom> page = employeeService.queryEnterpriseEmployeeByCondition(0, 0, condition);
        List employeeArchiveList = new ArrayList();
        for (EnterpriseEmployeeCustom enterpriseEmployeeCustom : page) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("electronArchiveNumber", enterpriseEmployeeCustom.getElectronArchiveNumber());
            temp.put("employeeName", enterpriseEmployeeCustom.getName());
            String postName = enterpriseEmployeeCustom.getPostName();
            temp.put("postName", (postName == null || postName.equals("")) ? "-" : postName);
            // 档案文件默认新增员工的时候就已经建档
            temp.put("archiveFile", "已建档");
            employeeArchiveList.add(temp);
        }
        dataMap.put("employeeArchiveList", employeeArchiveList);
        // 职业健康检查工种及人员名单
        Page<PostEmployeeResultCustom> postEmployeeResultCustomPage =
                this.postService.queryEnterprisePostEmployeeByCondition(condition,request);
        List enterprisePostEmployeeList = new ArrayList();
        for(PostEmployeeResultCustom postEmployeeResultCustom:postEmployeeResultCustomPage){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("postName",postEmployeeResultCustom.getPostName());
            String harmFactorName = postEmployeeResultCustom.getHarmFactorName();
            temp.put("harmFactorName", (harmFactorName == null || harmFactorName.equals("")) ? "-"
                    : harmFactorName.replace("<","&lt;").replace(">","&gt;"));
            temp.put("contactEmployeeName", postEmployeeResultCustom.getEmployeeName());
            enterprisePostEmployeeList.add(temp);
        }
        dataMap.put("enterprisePostEmployeeList",enterprisePostEmployeeList);
        // 职业健康检查结果与分析报告
        JSONObject result = medicalService.listMedicals(null, null, condition);
        List<EmployeeMedicalCustom> employeeMedicals= (List<EmployeeMedicalCustom>) result.get("employeeMedicals");
        List employeeMedicalList = new ArrayList(); // 体检结果list
        List tabooList = new ArrayList(); // 职业禁忌症list
        List occupationalDiseaseList = new ArrayList(); // 职业病list
        for(EmployeeMedicalCustom employeeMedicalCustom:employeeMedicals){
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("employeeName", employeeMedicalCustom.getEmployeeName());
            temp.put("postName", employeeMedicalCustom.getPostName());
            temp.put("medicalDate", sdf.format(employeeMedicalCustom.getMedicalDate()));
            temp.put("institutionName", employeeMedicalCustom.getInstitutionName());
            temp.put("type", employeeMedicalCustom.getType());
            String medicalResult = employeeMedicalCustom.getResult();
            temp.put("result", medicalResult);
            // 如果体检结论是疑似职业病，再根据诊断记录确认是否职业病(如果没有诊断记录填-)
            if (EmployeeMedicalCustom.RESULT_SUSPECTED_OCCUPATIONAL_DISEASE.equals(medicalResult)) {
                if (employeeMedicalCustom.getDiseaseNum().equals(0)) {
                    temp.put("isCertain", "-");
                    temp.put("isTransferred", "-");
                } else {
                    JSONObject result1 = diseaseDiagnosiService.listDiseaseDiagnosis(employeeMedicalCustom.getId());
                    List<EmployeeDiseaseDiagnosi> employeeDiseaseDiagnosis
                            = (List<EmployeeDiseaseDiagnosi>) result1.get("employeeDiseaseDiagnosis");
                    temp.put("isCertain", "否");
                    temp.put("isTransferred", "否");
                    for (EmployeeDiseaseDiagnosi employeeDiseaseDiagnosi : employeeDiseaseDiagnosis) {
                        Integer isCertain = employeeDiseaseDiagnosi.getIsCertain();
                        if (isCertain.equals(1)) {
                            temp.put("isCertain", "是");
                            // 职业病检出名单
                            Map<String, Object> temp1 = new HashMap<String, Object>();
                            temp1.put("employeeName", employeeMedicalCustom.getEmployeeName());
                            temp1.put("diseaseName", employeeDiseaseDiagnosi.getName());
                            Integer isTransferred = employeeDiseaseDiagnosi.getIsTransferred();
                            temp1.put("isTransferred", (isTransferred == null || isTransferred.equals(0)) ? "否" : "是");
                            temp1.put("checkedDate", sdf.format(employeeDiseaseDiagnosi.getDiagnosiDate()));
                            occupationalDiseaseList.add(temp1);
                        }
                        Integer isTransferred = employeeDiseaseDiagnosi.getIsTransferred();
                        if (isTransferred.equals(1)) {
                            temp.put("isTransferred", "是");
                        }
                    }
                }
            } else {
                // 如果体检结论不是疑似职业病，是否职业病填-，是否调离根据实际填写，没有值就填-
                temp.put("isCertain", "-");
                Integer isTransferred = employeeMedicalCustom.getIsTransferred();
                temp.put("isTransferred", isTransferred == null ? "-" : (isTransferred.equals(0) ? "否" : "是"));
            }
            // 职业禁忌症名单
            if (EmployeeMedicalCustom.RESULT_OCCUPATIONAL_CONTRAINDICATION.equals(medicalResult)) {
                Map<String, Object> temp1 = new HashMap<String, Object>();
                temp1.put("employeeName", employeeMedicalCustom.getEmployeeName());
                temp1.put("tabooDescription", employeeMedicalCustom.getProblemDescription());
                Integer isTransferred = employeeMedicalCustom.getIsTransferred();
                temp1.put("isTransferred", (isTransferred == null || isTransferred.equals(0)) ? "否" : "是");
                temp1.put("checkedDate", sdf.format(employeeMedicalCustom.getMedicalDate()));
                tabooList.add(temp1);
            }
            employeeMedicalList.add(temp);
        }
        dataMap.put("employeeMedicalList", employeeMedicalList);
        dataMap.put("occupationalDiseaseList", occupationalDiseaseList);
        dataMap.put("tabooList", tabooList);
        // 体检机构清单
        JSONObject result1 = inspectionInstitutionService.listInspectionInstitutionsByType("体检机构", enterpriseId);
        List<EnterpriseInspectionInstitution> enterpriseInspectionInstitutions
                = (List<EnterpriseInspectionInstitution>)result1.get("enterpriseInspectionInstitutions");
        List medicalInstitutionList = new ArrayList();
        for (EnterpriseInspectionInstitution enterpriseInspectionInstitution : enterpriseInspectionInstitutions) {
            Map<String, Object> temp = new HashMap<String, Object>();
            temp.put("name", enterpriseInspectionInstitution.getName());
            String contacts = enterpriseInspectionInstitution.getContacts();
            temp.put("contacts", (contacts == null || contacts.equals("")) ? "-" : contacts);
            String mobilePhone = enterpriseInspectionInstitution.getMobilePhone();
            temp.put("mobilePhone", (mobilePhone == null || mobilePhone.equals("")) ? "-" : mobilePhone);
            String contract  = enterpriseInspectionInstitution.getContract();
            temp.put("contract", (contract == null || contract.equals("")) ? "-" : "已上传");
            medicalInstitutionList.add(temp);
        }
        dataMap.put("medicalInstitutionList", medicalInstitutionList);
        return dataMap;
    }
}
