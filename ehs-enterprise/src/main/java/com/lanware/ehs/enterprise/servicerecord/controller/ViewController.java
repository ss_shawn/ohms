package com.lanware.ehs.enterprise.servicerecord.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.enterprise.servicerecord.service.ServiceRecordService;
import com.lanware.ehs.pojo.EnterpriseServiceRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 维护记录视图
 */
@Controller("serviceRecordView")
@RequestMapping(Constants.VIEW_PREFIX )
public class ViewController {
    /**
     * 注入serviceRecordService的bean
     */
    @Autowired
    private ServiceRecordService serviceRecordService;

    @RequestMapping("serviceRecord/editServiceRecord")
    /**
     * @description 新增维护记录
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView addServiceRecord() {
        JSONObject data = new JSONObject();
        EnterpriseServiceRecord enterpriseServiceRecord = new EnterpriseServiceRecord();
        data.put("enterpriseServiceRecord", enterpriseServiceRecord);
        return new ModelAndView("serviceRecord/edit", data);
    }

    /**
     * @param id 维护记录id
     * @return org.springframework.web.servlet.ModelAndView
     * @description 编辑维护记录
     */
    @RequestMapping("serviceRecord/editServiceRecord/{id}")
    public ModelAndView editServiceRecord(@PathVariable Long id) {
        JSONObject data = new JSONObject();
        EnterpriseServiceRecord enterpriseServiceRecord = serviceRecordService.getServiceRecordById(id);
        data.put("enterpriseServiceRecord", enterpriseServiceRecord);
        return new ModelAndView("serviceRecord/edit", data);
    }

    @RequestMapping("serviceRecord/serviceEquipment/{recordId}/{type}")
    /**
     * 维护的设备页面
     * @param recordId 记录id
     * @param type 记录类型(维护记录)
     * @return 维护的设备model
     */
    public ModelAndView serviceEquipment(@PathVariable Long recordId, @PathVariable Integer type){
        JSONObject data = new JSONObject();
        data.put("recordId", recordId);
        data.put("type", type);
        return new ModelAndView("serviceRecord/serviceEquipment/list", data);
    }

    @RequestMapping("serviceRecord/selectServiceEquipment/{recordId}/{type}")
    /**
     * @description 选择维护的设备
     * @param recordId 记录id
     * @param type 记录类型(维护记录)
     * @return 尚未维护的设备model
     */
    public ModelAndView selectServiceEquipment(@PathVariable Long recordId, @PathVariable Integer type){
        JSONObject data = new JSONObject();
        data.put("recordId", recordId);
        data.put("type", type);
        return new ModelAndView("serviceRecord/serviceEquipment/select", data);
    }
}
