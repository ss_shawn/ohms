package com.lanware.ehs.enterprise.employee.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.FileUtil;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.employee.service.EmployeeArchivesService;
import com.lanware.ehs.enterprise.employee.service.HarmContactHistoryService;
import com.lanware.ehs.enterprise.employee.service.MedicalHistoryService;
import com.lanware.ehs.enterprise.training.service.TrainingService;
import com.lanware.ehs.pojo.EmployeeDiseaseDiagnosi;
import com.lanware.ehs.pojo.EnterpriseBaseinfo;
import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.*;
import com.lanware.ehs.service.EnterpriseBaseinfoService;
import com.lanware.ehs.service.EnterpriseEmployeeService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/employee")
@Slf4j
@Controller
public class EmployeeArchivesController extends BaseController {
    @Autowired
    private EmployeeArchivesService employeeArchivesService;
    @Autowired
    private EnterpriseEmployeeService enterpriseEmployeeService;
    @Autowired
    private OSSTools ossTools;
    @Autowired
    private EnterpriseBaseinfoService enterpriseBaseinfoService;
    @Autowired
    private HarmContactHistoryService harmContactHistoryService;
    @Autowired
    private MedicalHistoryService medicalHistoryService;
    @Autowired
    private TrainingService trainingService;
    @Value(value="${upload.tempPath}")
    private String uploadTempPath;
    @RequestMapping("/listEmployeeArchivesServiceByEmployeeId")
    @ResponseBody
    /**
     * 根据员工ID获取职业病诊断
     * @param employeeId 员工ID
     * @return 职业病诊断列表
     */
    public ResultFormat listEmployeeArchivesServiceByEmployeeId(Long employeeId) {
        List<EmployeeDiseaseDiagnosi> employeeDiseaseDiagnosiList = employeeArchivesService.getEmployeeDiseaseDiagnosiByEmployeeId(employeeId);
        JSONObject result = new JSONObject();
        result.put("employeeDiseaseDiagnosiList",employeeDiseaseDiagnosiList);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listMedicalByEmployeeId")
    @ResponseBody
    /**
     * 根据员工ID获取职业病诊断
     * @param employeeId 员工ID
     * @return 职业病诊断列表
     */
    public ResultFormat listMedicalByEmployeeId(Long employeeId) {
        List<EmployeeMedicalCustom> employeeMedicalCustomlist = employeeArchivesService.getEmployeeMedicalByEmployeeId(employeeId);
        JSONObject result = new JSONObject();
        result.put("employeeMedicalCustomlist",employeeMedicalCustomlist);
        return ResultFormat.success(result);
    }
    @RequestMapping("/listWorkplaceOutsideCheckByEmployeeId")
    @ResponseBody
    /**
     * 根据员工ID获取工作场所职业病危害因素检测结果
     * @param employeeId 员工ID
     * @return 工作场所职业病危害因素检测结果列表
     */
    public ResultFormat getWorkplaceOutsideCheckByEmployeeId(Long employeeId) {
        List<EnterpriseOutsideCheckResultAllCustom> enterpriseOutsideCheckResultAllCustomList = employeeArchivesService.getWorkplaceOutsideCheckByEmployeeId(employeeId);
        JSONObject result = new JSONObject();
        result.put("enterpriseOutsideCheckResultAllCustomList",enterpriseOutsideCheckResultAllCustomList);
        return ResultFormat.success(result);
    }
    @RequestMapping("/getEmployeeProvideProtectList")
    @ResponseBody
    /**
     * 根据员工ID获取防护用品发放记录
     * @param employeeId 员工ID
     * @return 防护用品发放记录
     */
    public ResultFormat getEmployeeProvideProtectList(Long employeeId,@CurrentUser EnterpriseUser enterpriseUser) {
        Map<String, Object> condition=new HashMap();
        condition.put("employeeId",employeeId);
        condition.put("enterpriseId",enterpriseUser.getEnterpriseId());
        condition.put("orderByClause","gmt_create desc");
        Page<EnterpriseProvideProtectArticlesCustom> enterpriseProvideProtectArticlesCustomList = employeeArchivesService.getEmployeeProvideProtectList(condition);
        Map<String, Object> dataTable = getDataTable(enterpriseProvideProtectArticlesCustomList);
        return ResultFormat.success(dataTable);
    }
    @RequestMapping("/listTrainings")
    @ResponseBody
    /**
     * @description 获取培训管理result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listTrainings(Long employeeId  , @CurrentUser EnterpriseUser enterpriseUse) {
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("enterpriseId", enterpriseUse.getEnterpriseId());
        condition.put("employeeId", employeeId);
        JSONObject result = trainingService.listTrainings(null, null, condition);
        return ResultFormat.success(result);
    }

    //下载个人健康档案
    @RequestMapping("/download")
    @ResponseBody
    public void downloadThreeinquiryDoc(Long id, HttpServletResponse response, @CurrentUser EnterpriseUser enterpriseUse){
        File upload=null;

        List<String> files=new ArrayList<String>();
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            EnterpriseEmployee enterpriseEmployee = enterpriseEmployeeService.selectByPrimaryKey(id);

            EnterpriseBaseinfo enterpriseBaseinfo = enterpriseBaseinfoService.selectByPrimaryKey(enterpriseEmployee.getEnterpriseId());
            //返回word的变量
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("enterpriseName",enterpriseBaseinfo.getName());
            dataMap.put("id", String.format("%06d", id));
            dataMap.put("archiveNumber",enterpriseEmployee.getElectronArchiveNumber());
            dataMap.put("buildTime",enterpriseEmployee.getBuildTime()==null?"":sdf1.format(enterpriseEmployee.getBuildTime()));
            dataMap.put("name",enterpriseEmployee.getName());
            dataMap.put("sex",enterpriseEmployee.getSex()==null?"":(enterpriseEmployee.getSex()==0?"男":"女"));
            dataMap.put("nativePlace",enterpriseEmployee.getNativePlace());
            dataMap.put("isMarried",enterpriseEmployee.getIsMarried()==null?"":(enterpriseEmployee.getIsMarried()==0?"未婚":"已婚"));
            dataMap.put("educationDegree",enterpriseEmployee.getEducationDegree());
            dataMap.put("hobby",enterpriseEmployee.getHobby());
            dataMap.put("attendWorkTime",enterpriseEmployee.getAttendWorkTime()==null?"":sdf1.format(enterpriseEmployee.getAttendWorkTime()));
            dataMap.put("idCard",enterpriseEmployee.getIdCard());
            //主文件路径
            upload = new File(uploadTempPath, enterpriseEmployee.getEnterpriseId()+File.separator+enterpriseEmployee.getId()+File.separator);
            if (!upload.exists()) upload.mkdirs();


            if(enterpriseEmployee.getPhoto()!=null&&!"".equals(enterpriseEmployee.getPhoto())){
                //下载照片
                ossTools.downloadToLocal(enterpriseEmployee.getPhoto(),upload+"photo.jpg");
                //设置照片
                dataMap.put("photo",getImageBase(upload+"photo.jpg"));
                //删除照片
                File photoFile = new File(upload+"photo.jpg");
                photoFile.delete();
            }else{//使用默认图片
                ClassPathResource classPathResource = new ClassPathResource("static/xadmin/images/dsc.png");

                dataMap.put("photo",getImageBase(classPathResource.getInputStream()));
            }

            //职业史及职业病危害接触史
            Map<String, Object> condition = new HashMap<>();
            condition.put("employeeId", id);
            Page<EmployeeHarmContactHistoryCustom> page = harmContactHistoryService.queryEmployeeHarmContactHistoryByCondition(0, 0, condition);
            List harmContactHistoryList=new ArrayList();
            for(EmployeeHarmContactHistoryCustom employeeHarmContactHistoryCustom:page){
                Map<String, Object> temp = new HashMap<String, Object>();
                if(employeeHarmContactHistoryCustom.getStatus()==1){
                    temp.put("startDate",sdf1.format(employeeHarmContactHistoryCustom.getStarttime2())+"—"+(employeeHarmContactHistoryCustom.getEndtime2()==null?"至今":sdf1.format(employeeHarmContactHistoryCustom.getEndtime2())) );
                    temp.put("enterpriseName",employeeHarmContactHistoryCustom.getEnterpriseName2());
                    temp.put("postName",employeeHarmContactHistoryCustom.getPostName2());
                    temp.put("harmFactorName",employeeHarmContactHistoryCustom.getHarmFactorName2()==null?"":employeeHarmContactHistoryCustom.getHarmFactorName2().replace("<","&lt;").replace(">","&gt;"));
                    String protectArticlesName="";
                    if(employeeHarmContactHistoryCustom.getProtectArticlesName2()!=null){
                        protectArticlesName="防护用品："+employeeHarmContactHistoryCustom.getProtectArticlesName2()+"。";
                    }
                    if(employeeHarmContactHistoryCustom.getProtectiveMeasuresName2()!=null){
                        protectArticlesName+="防护设施："+employeeHarmContactHistoryCustom.getProtectiveMeasuresName2()+"。";
                    }
                    temp.put("protectArticlesName",protectArticlesName);
                }else{
                    temp.put("startDate",sdf1.format(employeeHarmContactHistoryCustom.getStarttime())+"—"+(employeeHarmContactHistoryCustom.getEndtime()==null?"至今":sdf1.format(employeeHarmContactHistoryCustom.getEndtime())) );
                    temp.put("enterpriseName",employeeHarmContactHistoryCustom.getEnterpriseName());
                    temp.put("postName",employeeHarmContactHistoryCustom.getPostName());
                    temp.put("harmFactorName",employeeHarmContactHistoryCustom.getHarmFactorName()==null?"":employeeHarmContactHistoryCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                    String protectArticlesName="";
                    if(employeeHarmContactHistoryCustom.getProtectArticlesName()!=null){
                        protectArticlesName="防护用品："+employeeHarmContactHistoryCustom.getProtectArticlesName()+"。";
                    }
                    if(employeeHarmContactHistoryCustom.getProtectiveMeasuresName()!=null){
                        protectArticlesName+="防护设施："+employeeHarmContactHistoryCustom.getProtectiveMeasuresName()+"。";
                    }
                    temp.put("protectArticlesName",protectArticlesName);
                }
                harmContactHistoryList.add(temp);
            }
            dataMap.put("harmContactHistoryList",harmContactHistoryList);

            //职业病史
            condition = new HashMap<>();
            condition.put("employeeId", id);
            condition.put("diseaseType", 0);
            Page<EmployeeMedicalHistoryCustom> employeeMedicalHistoryPage = medicalHistoryService.queryEmployeeMedicalHistoryByCondition(0, 0, condition);
            List  employeeMedicalHistoryList=new ArrayList();
            for(EmployeeMedicalHistoryCustom employeeMedicalHistoryCustom:employeeMedicalHistoryPage){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("diseaseName",employeeMedicalHistoryCustom.getDiseaseName());
                temp.put("diagnosiDate",employeeMedicalHistoryCustom.getDiagnosiDate()==null?"":sdf1.format(employeeMedicalHistoryCustom.getDiagnosiDate()));
                temp.put("diagnosiHosipital",employeeMedicalHistoryCustom.getDiagnosiHosipital());
                temp.put("diagnosiResult",employeeMedicalHistoryCustom.getDiagnosiResult());
                temp.put("remark",employeeMedicalHistoryCustom.getRemark());
                employeeMedicalHistoryList.add(temp);
            }
            dataMap.put("employeeMedicalHistoryList",employeeMedicalHistoryList);
            //职业禁忌症史
            condition = new HashMap<>();
            condition.put("employeeId", id);
            condition.put("diseaseType", 1);
            employeeMedicalHistoryPage = medicalHistoryService.queryEmployeeMedicalHistoryByCondition(0, 0, condition);
            List  employeeMedicalHistoryList2=new ArrayList();
            for(EmployeeMedicalHistoryCustom employeeMedicalHistoryCustom:employeeMedicalHistoryPage){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("diseaseName",employeeMedicalHistoryCustom.getDiseaseName());
                temp.put("diagnosiDate",employeeMedicalHistoryCustom.getDiagnosiDate()==null?"":sdf1.format(employeeMedicalHistoryCustom.getDiagnosiDate()));
                temp.put("diagnosiHosipital",employeeMedicalHistoryCustom.getDiagnosiHosipital());
                temp.put("diagnosiResult",employeeMedicalHistoryCustom.getDiagnosiResult());
                temp.put("remark",employeeMedicalHistoryCustom.getRemark());
                employeeMedicalHistoryList2.add(temp);
            }
            dataMap.put("employeeMedicalHistoryList2",employeeMedicalHistoryList2);
            //职业病诊断
            List<EmployeeDiseaseDiagnosi> employeeDiseaseDiagnosiList = employeeArchivesService.getEmployeeDiseaseDiagnosiByEmployeeId(id);
            List  employeeDiseaseDiagnosiList2=new ArrayList();
            for(EmployeeDiseaseDiagnosi employeeMedicalHistoryCustom:employeeDiseaseDiagnosiList){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("name",employeeMedicalHistoryCustom.getName());
                temp.put("diagnosiDate",employeeMedicalHistoryCustom.getDiagnosiDate()==null?"":sdf1.format(employeeMedicalHistoryCustom.getDiagnosiDate()));
                temp.put("hospital",employeeMedicalHistoryCustom.getHospital());
                temp.put("diagnosiLevel",employeeMedicalHistoryCustom.getDiagnosiLevel()==null?"":employeeMedicalHistoryCustom.getDiagnosiLevel());
                temp.put("remark",employeeMedicalHistoryCustom.getRemark()==null?"":employeeMedicalHistoryCustom.getRemark());
                employeeDiseaseDiagnosiList2.add(temp);
            }
            dataMap.put("employeeDiseaseDiagnosiList",employeeDiseaseDiagnosiList2);
            //工作场所职业病危害因素检测结果

            List  enterpriseOutsideCheckResultAllCustomList2=new ArrayList();

            List<EnterpriseOutsideCheckResultAllCustom> enterpriseOutsideCheckResultAllCustomList = employeeArchivesService.getWorkplaceOutsideCheckByEmployeeId(id);
            for(EnterpriseOutsideCheckResultAllCustom enterpriseOutsideCheckResultAllCustom:enterpriseOutsideCheckResultAllCustomList){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("postName",enterpriseOutsideCheckResultAllCustom.getPostName());
                temp.put("checkDate",enterpriseOutsideCheckResultAllCustom.getCheckDate()==null?"":sdf1.format(enterpriseOutsideCheckResultAllCustom.getCheckDate()));
                temp.put("checkOrganization",enterpriseOutsideCheckResultAllCustom.getCheckOrganization());
                temp.put("harmFactorName",enterpriseOutsideCheckResultAllCustom.getHarmFactorName()==null?"":enterpriseOutsideCheckResultAllCustom.getHarmFactorName().replace("<","&lt;").replace(">","&gt;"));
                temp.put("placeConsistence",enterpriseOutsideCheckResultAllCustom.getIsQualified()==null?"":enterpriseOutsideCheckResultAllCustom.getIsQualified());
                String protectArticlesName="";
                if(enterpriseOutsideCheckResultAllCustom.getProtectArticlesName()!=null){
                    protectArticlesName+="防护用品："+enterpriseOutsideCheckResultAllCustom.getProtectArticlesName();
                }
                if(enterpriseOutsideCheckResultAllCustom.getProtectiveMeasuresName()!=null){
                    protectArticlesName+="防护设施："+enterpriseOutsideCheckResultAllCustom.getProtectiveMeasuresName();
                }
                temp.put("protectArticlesName",protectArticlesName);
                temp.put("remark","");
                enterpriseOutsideCheckResultAllCustomList2.add(temp);
            }
            dataMap.put("enterpriseOutsideCheckResultAllCustomList",enterpriseOutsideCheckResultAllCustomList2);


            //历次职业健康检查结果及处理情况
            List<EmployeeMedicalCustom> employeeMedicalCustomlist = employeeArchivesService.getEmployeeMedicalByEmployeeId(id);
            List  employeeMedicalCustomlist2=new ArrayList();
            File report=null;

            if(employeeMedicalCustomlist.size()>0){
                report = new File(upload+File.separator+"体检报告"+File.separator);
                if (!report.exists()) report.mkdirs();
               // files.add(upload+"\\体检报告\\");
            }
            //压缩列表

             for(EmployeeMedicalCustom employeeMedicalCustom:employeeMedicalCustomlist){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("medicalDate",employeeMedicalCustom.getMedicalDate()==null?"":sdf1.format(employeeMedicalCustom.getMedicalDate()));
                temp.put("type",employeeMedicalCustom.getType()==null?"":employeeMedicalCustom.getType());
                temp.put("result",employeeMedicalCustom.getResult()==null?"":employeeMedicalCustom.getResult());
                temp.put("institutionName",employeeMedicalCustom.getInstitutionName()==null?"":employeeMedicalCustom.getInstitutionName());
                temp.put("postName",employeeMedicalCustom.getPostName()==null?"":employeeMedicalCustom.getPostName());
                temp.put("personnelHandling",employeeMedicalCustom.getPersonnelHandling()==null?"":employeeMedicalCustom.getPersonnelHandling());
                temp.put("medicalNotifyNum",employeeMedicalCustom.getMedicalNotifyNum()>0?"已签":"未签");
                temp.put("sceneHanding",employeeMedicalCustom.getSceneHanding()==null?"":employeeMedicalCustom.getSceneHanding());
                employeeMedicalCustomlist2.add(temp);
                //下载检查报告
                 if(employeeMedicalCustom.getMedicalReport()!=null){
                     String medicalReport=employeeMedicalCustom.getMedicalReport();//取后缀名
                     ossTools.downloadToLocal(medicalReport,report +File.separator+(String)temp.get("medicalDate") +"体检报告."+medicalReport.substring(medicalReport.lastIndexOf(".") + 1));
                     //放入压缩列表
                     files.add(report +File.separator+(String)temp.get("medicalDate") +"体检报告."+medicalReport.substring(medicalReport.lastIndexOf(".") + 1));
                 }
            }
            dataMap.put("employeeMedicalCustomlist",employeeMedicalCustomlist2);

            //获取培训管理result
            List  employeeTrainingList=new ArrayList();
            condition = new HashMap<String, Object>();
            condition.put("enterpriseId", enterpriseUse.getEnterpriseId());
            condition.put("employeeId", id);
            JSONObject result = trainingService.listTrainings(null, null, condition);
            List<EnterpriseTrainingCustom> enterpriseTrainings=(List<EnterpriseTrainingCustom>)result.get("enterpriseTrainings");
            for(EnterpriseTrainingCustom enterpriseTrainingCustom:enterpriseTrainings){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("trainingDate",enterpriseTrainingCustom.getTrainingDate()==null?"":sdf1.format(enterpriseTrainingCustom.getTrainingDate()));
                temp.put("duration",enterpriseTrainingCustom.getDuration());
                temp.put("place",enterpriseTrainingCustom.getPlace());
                temp.put("theme",enterpriseTrainingCustom.getTheme());
                temp.put("lecturer",enterpriseTrainingCustom.getLecturer());

                employeeTrainingList.add(temp);
            }
            dataMap.put("employeeTrainingList",employeeTrainingList);
            //防护用品发放记录
           condition=new HashMap();
            condition.put("employeeId",id);
            condition.put("enterpriseId",enterpriseUse.getEnterpriseId());
            condition.put("orderByClause","gmt_create desc");
            Page<EnterpriseProvideProtectArticlesCustom> enterpriseProvideProtectArticlesCustomList = employeeArchivesService.getEmployeeProvideProtectList(condition);
            List  employeeProvideProtectList=new ArrayList();
            for(EnterpriseProvideProtectArticlesCustom enterpriseProvideProtectArticlesCustom:enterpriseProvideProtectArticlesCustomList){
                Map<String, Object> temp = new HashMap<String, Object>();
                temp.put("postName",enterpriseProvideProtectArticlesCustom.getPostName());
                temp.put("provideDate",enterpriseProvideProtectArticlesCustom.getProvideDate()==null?"":sdf1.format(enterpriseProvideProtectArticlesCustom.getProvideDate()));
                temp.put("provideArticlesName",enterpriseProvideProtectArticlesCustom.getProvideArticlesName());
                temp.put("number",enterpriseProvideProtectArticlesCustom.getNumber());

                employeeProvideProtectList.add(temp);
            }
            dataMap.put("employeeProvideProtectList",employeeProvideProtectList);
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件

            configuration.setClassForTemplateLoading(EmployeeArchivesController.class,"/templates/ftl");

            //获取模板
            Template template = configuration.getTemplate("employeeArchivesMould.ftl");
            //输出文件

            String uploadPath = upload + File.separator+enterpriseEmployee.getName()+"个人健康监护档案.doc";

            File outFile = new File(uploadPath);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
            //生成文件
            template.process(dataMap, out);

            //关闭流
            out.flush();
            out.close();
            //放入压缩列表
            files.add(uploadPath);
            String uploadZipPath = upload +File.separator+enterpriseEmployee.getId()+"个人健康监护档案.zip";
            FileUtil.toZip(files, uploadZipPath, true);

            File uploadZipFile=new File(uploadZipPath);
            FileInputStream inputFile = new FileInputStream(uploadZipFile);
            response.setContentType("text/html");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(enterpriseEmployee.getName()+"个人健康监护档案.zip", "utf-8"));
            OutputStream outputStream = response.getOutputStream();
            int length;
            byte[] buffer = new byte[1024];
            while ((length = inputFile.read(buffer)) != -1){
                outputStream.write(buffer, 0, length);
            }
            inputFile.close();

        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        //删除试试呢
        FileUtil.delFolder(upload.getPath());
    }
    //获得图片的base64码
      public static String getImageBase(String src) throws Exception {
             if (src == null || src == "") {
                    return "";
             }
             File file = new File(src);
             if (!file.exists()) {
                     return "";
             }
             InputStream in = null;
             byte[] data = null;
             try {
                  in = new FileInputStream(file);
                  data = new byte[in.available()];
                  in.read(data);
                  in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BASE64Encoder encoder = new BASE64Encoder();
            return encoder.encode(data);
    }


    public static String getImageBase(InputStream in) throws Exception {
        byte[] data = null;
        try {
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }

}
