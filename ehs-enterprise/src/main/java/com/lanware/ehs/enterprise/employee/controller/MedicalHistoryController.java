package com.lanware.ehs.enterprise.employee.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.employee.service.MedicalHistoryService;
import com.lanware.ehs.pojo.EmployeeMedicalHistory;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EmployeeMedicalHistoryCustom;
import com.lanware.ehs.service.EmployeeMedicalHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/employee/medicalHistory")
@Controller
public class MedicalHistoryController {

    private String basePath = "employee/medicalHistory/";

    private EmployeeMedicalHistoryService employeeMedicalHistoryService;

    private MedicalHistoryService medicalHistoryService;

    @Autowired
    public MedicalHistoryController(EmployeeMedicalHistoryService employeeMedicalHistoryService, MedicalHistoryService medicalHistoryService) {
        this.employeeMedicalHistoryService = employeeMedicalHistoryService;
        this.medicalHistoryService = medicalHistoryService;
    }

    /**
     * 既往病史列表页面
     * @param employeeId 人员id
     * @param diseaseType 疾病类型 0 职业病 1 禁忌症
     * @return 既往病史列表model
     */
    @RequestMapping("")
    public ModelAndView list(Long employeeId, Integer diseaseType){
        JSONObject data = new JSONObject();
        data.put("employeeId", employeeId);
        data.put("diseaseType", diseaseType);
        return new ModelAndView(basePath + "list", data);
    }

    /**
     * 分页查询既往病史
     * @Param employeeId 人员id
     * @Param diseaseType 疾病类型 0 职业病 1 禁忌症
     * @param keyword 关键词
     * @param sortField 排序字段
     * @param sortType 排序方式
     * @return 查询结果
     */
    @RequestMapping("/getList")
    @ResponseBody
    public EhsResult getList(Long employeeId, Integer diseaseType, String keyword, String sortField, String sortType){
        JSONObject data = new JSONObject();
        Map<String, Object> condition = new HashMap<>();
        condition.put("employeeId", employeeId);
        condition.put("diseaseType", diseaseType);
        condition.put("keyword", keyword);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        Page<EmployeeMedicalHistoryCustom> page = medicalHistoryService.queryEmployeeMedicalHistoryByCondition(0, 0, condition);
        data.put("list", page.getResult());
        data.put("total", page.getTotal());
        return EhsResult.ok(data);
    }

    /**
     * 编辑既往病史
     * @param id 既往病史id
     * @param medicalHistoryJSON 既往病史JSON
     * @param tableIndex 表格索引
     * @return 编辑既往病史model
     */
    @RequestMapping("/edit")
    public ModelAndView edit(@CurrentUser EnterpriseUser enterpriseUser, Long id, String medicalHistoryJSON, Integer tableIndex){
        JSONObject data = new JSONObject();
        EmployeeMedicalHistory employeeMedicalHistory = null;
        if (id != null){
            try {
                employeeMedicalHistory = employeeMedicalHistoryService.selectByPrimaryKey(id);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }else if (!StringUtils.isEmpty(medicalHistoryJSON)){
            employeeMedicalHistory = JSONObject.parseObject(medicalHistoryJSON, EmployeeMedicalHistory.class);
        }
        data.put("employeeMedicalHistory", employeeMedicalHistory);
        data.put("tableIndex", tableIndex);
        return new ModelAndView(basePath + "edit", data);
    }

    /**
     * 保存既往病史页面
     * @param medicalHistoryJSON 既往病史JSON
     * @param diagnosiReportFile 诊断报告
     * @return 保存结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public EhsResult save(@CurrentUser EnterpriseUser enterpriseUser, String medicalHistoryJSON, MultipartFile diagnosiReportFile){
        EmployeeMedicalHistory employeeMedicalHistory = JSONObject.parseObject(medicalHistoryJSON, EmployeeMedicalHistory.class);
        try {
            int num = medicalHistoryService.saveEmployeeMedicalHistory(enterpriseUser.getEnterpriseId(), employeeMedicalHistory, diagnosiReportFile);
            if (num == 0){
                return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "保存失败");
            }
        } catch (EhsException e){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
        return EhsResult.ok();
    }

    /**
     * 删除既往病史
     * @param idsJSON id集合JSON
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public EhsResult delete(String idsJSON){
        List<Long> ids = JSONArray.parseArray(idsJSON, Long.class);
        int num = medicalHistoryService.deleteEmployeeMedicalHistory(ids);
        if (num == 0){
            return EhsResult.build(HttpStatus.INTERNAL_SERVER_ERROR.value(), "删除失败");
        }
        return EhsResult.ok();
    }

    /**
     * 查看既往病史
     * @param id 既往病史id
     * @return 查看既往病史model
     */
    @RequestMapping("/view")
    public ModelAndView view(@RequestParam Long id){
        JSONObject data = new JSONObject();
        if (id != null){
            try {
                EmployeeMedicalHistory employeeMedicalHistory = employeeMedicalHistoryService.selectByPrimaryKey(id);
                data.put("employeeMedicalHistory", employeeMedicalHistory);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return new ModelAndView(basePath + "view", data);
    }
}
