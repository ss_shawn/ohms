package com.lanware.ehs.enterprise.user.controller;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.google.common.collect.Maps;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.enterprise.user.service.UserService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.SysUserCustom;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * 用户管理
 */
@Slf4j
@Validated
@RestController
@RequestMapping("user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;


    @GetMapping("{username}")
    public EnterpriseUser getUser(@NotBlank(message = "{required}") @PathVariable String username) {
        return this.userService.findUserDetail(username);
    }

    /**
     * 检查用户名是否存在
     * @param username
     * @param userId
     * @return
     */
    @GetMapping("check/{username}")
    public boolean checkUserName(@NotBlank(message = "{required}") @PathVariable String username, String userId) {
        return this.userService.findByName(username) == null || StringUtils.isNotBlank(userId);
    }

    /**
     * 获取用户数据
     * @param user
     * @param request
     * @return
     */
    @GetMapping("list")
    public EhsResult userList(EnterpriseUserCustom user, QueryRequest request,@CurrentUser EnterpriseUserCustom currentUser) {
        //过滤当前企业下的数据
        user.setEnterpriseId(currentUser.getEnterpriseId());
        user.setIsAdmin(0);
        Map<String, Object> dataTable = getDataTable(this.userService.findUserDetail(user, request));
        return EhsResult.ok(dataTable);
    }

    @PostMapping
    public EhsResult addUser(EnterpriseUserCustom user, @CurrentUser EnterpriseUserCustom currentUser) throws EhsException {
        try {
            user.setEnterpriseId(currentUser.getEnterpriseId());
            this.userService.createUser(user);
            return EhsResult.ok();
        } catch(MailSendException e){
            throw new EhsException("发送邮箱失败,请确认邮箱是否正确！");
        }catch (Exception e) {
            String message = "新增用户失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }


    /**
     * 重置密码
     * @param usernames
     * @return
     * @throws EhsException
     */
    @PostMapping("password/reset/{usernames}")
    public EhsResult resetPassword(@NotBlank(message = "{required}") @PathVariable String usernames) throws EhsException {
        try {
            String[] usernameArr = usernames.split(StringPool.COMMA);
            this.userService.resetPassword(usernameArr);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "重置用户密码失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }


    /**
     * 更改用户状态
     * @param usernames
     * @return
     * @throws EhsException
     */
    @PostMapping("status/{status}/{usernames}")
    public EhsResult updateStatus(@PathVariable Integer status,@NotBlank(message = "{required}") @PathVariable String usernames) throws EhsException {
        try {
            String[] usernameArr = usernames.split(StringPool.COMMA);
            this.userService.updateSatus(status,usernameArr);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "更新用户状态失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }


    /**
     * 修改用户
     * @param user
     * @return
     * @throws EhsException
     */
    @PostMapping("update")
    public EhsResult updateUser(EnterpriseUserCustom user) throws EhsException {
        try {
            if (user.getId() == null)
                throw new EhsException("用户ID为空");
            this.userService.updateUser(user);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "修改用户失败";
            log.error(message, e);
            throw new EhsException(message);
        }
    }


    /**
     * 修改密码
     * @param oldPassword
     * @param newPassword
     * @return
     * @throws EhsException
     */
    @PostMapping("password/update")
    public EhsResult updatePassword(
            @NotBlank(message = "{required}") String oldPassword,
            @NotBlank(message = "{required}") String newPassword) throws EhsException {
        try {
            String userName = ((EnterpriseUserCustom)SecurityUtils.getSubject().getPrincipal()).getUsername();
            EnterpriseUser user = userService.findByName(userName);
            if (!StringUtils.equals(user.getPassword(), MD5Util.encrypt(user.getUsername(), oldPassword))) {
                throw new EhsException("原密码不正确");
            }
            userService.updatePassword(user.getUsername(), newPassword);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "修改密码失败，" + e.getMessage();
            log.error(message, e);
            throw new EhsException(message);
        }
    }

    @GetMapping("password/verify")
    public EhsResult verifyPassword(){
        EnterpriseUserCustom userCustom = (EnterpriseUserCustom)getSubject().getPrincipal();
        String defaultPassword = EnterpriseUserCustom.DEFAULT_PASSWORD;
        if (StringUtils.equals(userCustom.getPassword(), MD5Util.encrypt(userCustom.getUsername(), defaultPassword))) {
            return EhsResult.build(500,"您的登录密码与默认密码一致，请尽快修改您的登录密码！");
        }
        return EhsResult.ok();
    }



}
