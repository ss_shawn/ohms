package com.lanware.ehs.enterprise.user.service;

import javax.mail.MessagingException;

/**
 * 发送邮件服务
 */
public interface MailService {
    /**
     * 发送邮件
     * @param title 标题
     * @param url 邮件内容
     * @param email 目标email
     */
    void sendHTMLMail(String title, String url, String email) throws MessagingException;
}
