package com.lanware.ehs.enterprise.workplace.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.enterprise.workplace.service.HarmfactorService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.WorkplaceHarmFactor;
import com.lanware.ehs.pojo.custom.WorkplaceHarmFactorCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/workplace/harmfactor")
/**
 * @description 职业病危害因素controller
 */
public class HarmfactorController {
    @Autowired
    /** 注入harmfactorService的bean */
    private HarmfactorService harmfactorService;
    
    @RequestMapping("/listHarmfactors")
    @ResponseBody
    /**
     * @description 获取危害因素result
     * @param workplaceId 工作场所id
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listHarmfactors(Long workplaceId) {
        JSONObject result = harmfactorService.listHarmfactors(workplaceId);
        return ResultFormat.success(result);
    }

    @RequestMapping("/listWorkplaceHarmFactors")
    @ResponseBody
    /**
     * @description 获取作业场所危害因素扩展result
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param keyword 模糊查询的关键字
     * @param enterpriseUser 企业用户
     * @param sortField 排序域
     * @param sortType 排序类型
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat listWorkplaceHarmFactors(Integer pageNum, Integer pageSize, String keyword
            , @CurrentUser EnterpriseUser enterpriseUser, String sortField, String sortType) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("keyword", keyword);
        condition.put("enterpriseId", enterpriseUser.getEnterpriseId());
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        JSONObject result = harmfactorService.listWorkplaceHarmFactors(pageNum, pageSize, condition);
        return ResultFormat.success(result);
    }

    @RequestMapping("/getHarmfactors")
    @ResponseBody
    /**
     * @description 获取危害因素result（管理端危害因素获取）
     * @param workplaceId 工作场所id
     * @param harmFactorId 危害因素id
     * @return com.lanware.ehs.common.format.ResultFormat
     */
    public ResultFormat listWorkplaces(Long workplaceId, Long harmFactorId) {
        JSONObject result = harmfactorService.getHarmfactors(workplaceId, harmFactorId);
        return ResultFormat.success(result);
    }

    @RequestMapping("/editHarmFactor/save")
    @ResponseBody
    /**
     * @description 保存职业病危害因素信息
     * @param workplaceHarmFactor 作业场所危害因素pojo
     * @return com.lanware.management.common.format.ResultFormat
     */
    public ResultFormat save(WorkplaceHarmFactor workplaceHarmFactor) {
        if (workplaceHarmFactor.getId() == null) {
            workplaceHarmFactor.setGmtCreate(new Date());
            if (harmfactorService.insertHarmfactor(workplaceHarmFactor) > 0) {
                return ResultFormat.success("添加成功");
            } else {
                return ResultFormat.error("添加失败");
            }
        } else {
            workplaceHarmFactor.setGmtModified(new Date());
            if (harmfactorService.updateHarmfactor(workplaceHarmFactor) > 0) {
                return ResultFormat.success("更新成功");
            } else {
                return ResultFormat.error("更新失败");
            }
        }
    }

    @RequestMapping("/viewHarmfactor")
    /**
     * @description 查看危害因素扩展信息
     * @param id 危害因素id
     * @return org.springframework.web.servlet.ModelAndView
     */
    public ModelAndView viewHarmfactor(Long id) {
        JSONObject data = new JSONObject();
        WorkplaceHarmFactorCustom workplaceHarmFactor = harmfactorService.getHarmfactorCustomById(id);
        data.put("workplaceHarmFactor", workplaceHarmFactor);
        return new ModelAndView("workplace/harmfactor/view", data);
    }

    @RequestMapping("/deleteHarmfactor")
    @ResponseBody
    /**
     * @description 删除危害因素信息
     * @param id 要删除的id
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteHarmfactor(Long id) {
        if (harmfactorService.deleteHarmfactor(id) > 0) {
            return ResultFormat.success("删除成功");
        } else {
            return ResultFormat.error("删除失败");
        }
    }

    @RequestMapping("/deleteHarmfactors")
    @ResponseBody
    /**
     * @description 批量删除危害因素信息
     * @param ids 要删除的数组ids
     * @return com.lanware.management.common.format.ResultFormat 格式化的result
     */
    public ResultFormat deleteHarmfactors(Long[] ids) {
        if (harmfactorService.deleteHarmfactors(ids) == ids.length) {
            return ResultFormat.success("删除成功");
        }else{
            return ResultFormat.error("删除失败");
        }
    }


}
