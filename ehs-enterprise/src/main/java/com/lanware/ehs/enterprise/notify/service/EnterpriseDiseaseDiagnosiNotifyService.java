package com.lanware.ehs.enterprise.notify.service;

import com.github.pagehelper.Page;
import com.lanware.ehs.common.entity.QueryRequest;
import com.lanware.ehs.pojo.EnterpriseDiseaseDiagnosiWaitNotify;
import com.lanware.ehs.pojo.custom.EnterpriseDiseaseDiagnosiNotifyCustom;
import com.lanware.ehs.pojo.custom.EnterpriseDiseaseDiagnosiWaitNotifyCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface EnterpriseDiseaseDiagnosiNotifyService {
    /**
     * @description 根据id查询待职业病诊断结果告知
     * @param id 待职业病诊断结果告知id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseDiseaseDiagnosiWaitNotify对象
     */
     EnterpriseDiseaseDiagnosiWaitNotify getEnterpriseDiseaseDiagnosiWaitNotifyById(Long id);
    /**
     * @description 根据id查询待职业病诊断结果告知
     * @param condition 待职业病诊断结果告知条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseDiseaseDiagnosiWaitNotify对象
     */
     Page<EnterpriseDiseaseDiagnosiWaitNotifyCustom> findEnterpriseDiseaseDiagnosiWaitNotifyList(Map<String, Object> condition, QueryRequest request);
    /**
     * 保存职业病诊断结果告知,
     * @param enterpriseMedicalResultNotifyCustom
     * @return成功与否 1成功 0不成功
     */
    int saveEnterpriseDiseaseDiagnosiNotify(EnterpriseDiseaseDiagnosiNotifyCustom enterpriseMedicalResultNotifyCustom, MultipartFile notifyFile);
    /**
     * 通过id获取职业病诊断结果告知
     * @param id 职业病诊断结果告知id
     * @return职业病诊断结果告知
     */
     EnterpriseDiseaseDiagnosiNotifyCustom getEnterpriseDiseaseDiagnosiNotifyCustomByID(long id);
    /**
     * 删除待职业病诊断结果告知,
     * @param id
     * @return成功与否 1成功 0不成功
     */
     int deleteEnterpriseDiseaseDiagnosiNotify(long id);
    /**
     * 通过待职业病诊断结果告知ID获取待职业病诊断结果告知的人员和岗位信息,放入职业病诊断结果告知返回
     * @param id 待职业病诊断结果告知ID
     * @return 职业病诊断结果告知
     */
     EnterpriseDiseaseDiagnosiNotifyCustom getEnterpriseDiseaseDiagnosiNotifyCustomByWaitID(long id);
    /**
     * @description 根据条件查询职业病诊断结果告知
     * @param condition 查询条件
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseProtectArticles对象
     */
    Page<EnterpriseDiseaseDiagnosiNotifyCustom> findEnterpriseDiseaseDiagnosiNotifyList(Map<String, Object> condition, QueryRequest request);
}
