package com.lanware.ehs.enterprise.account.provideProtectArticlesAccount.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.protect.service.ProvideProtectArticlesService;
import com.lanware.ehs.pojo.EnterpriseEmployee;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.service.EnterpriseEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller("provideProtectArticlesAccount")
public class ViewController {
    @Autowired
    private ProvideProtectArticlesService provideProtectArticlesService;
    @RequestMapping(Constants.VIEW_PREFIX+"/provideProtectArticlesAccount")
    /**
     * @description 返回从业人员防护用品的发放使用管理台帐页面
     * @return java.lang.String 返回页面路径
     */
    public String list(Model model, @CurrentUser EnterpriseUserCustom currentUser) {

        return  "account/provideProtectArticlesAccount/list";
    }

    public  boolean contains(List<String> list,String str){
        for(int i=0;i<list.size();i++){
            if (list.get(i).equals(str)) {
                return true;
            }
        }
        return false;
    }


}
