package com.lanware.ehs.enterprise.yearplan.service;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.pojo.EnterpriseYearPlan;
import com.lanware.ehs.pojo.custom.EnterpriseYearPlanCustom;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @description 年度计划interface
 */
public interface YearplanService {
    /**
     * @description 根据企业id查询年度计划list
     * @param year 年度
     * @param enterpriseId 企业id
     * @return java.util.List<com.lanware.ehs.pojo.EnterpriseYearPlanCustom> 年度计划扩展list
     */
    List<EnterpriseYearPlanCustom> listYearplansByEnterpriseId(String year, Long enterpriseId);

    /**
     * @description 根据id查询年度计划
     * @param id 年度计划id
     * @return com.lanware.management.pojo.EnterpriseYearPlan 返回EnterpriseYearPlan对象
     */
    EnterpriseYearPlan getYearPlanById(Long id);

    /**
     * @description 新增年度计划信息
     * @param enterpriseYearPlan 年度计划pojo
     * @param planFile 计划文件
     * @return int 插入成功的数目
     */
    int insertYearplan(EnterpriseYearPlan enterpriseYearPlan, MultipartFile planFile);

    /**
     * @description 更新年度计划信息
     * @param enterpriseYearPlan 年度计划扩展pojo
     * @param planFile 计划文件
     * @return int 更新成功的数目
     */
    int updateYearplan(EnterpriseYearPlan enterpriseYearPlan, MultipartFile planFile);

    /**
     * @description 根据条件查询年度计划
     * @param condition 查询条件
     * @return EnterpriseYearPlan 年度计划
     */
    EnterpriseYearPlan findYearplanByCondition(Map<String, Object> condition);


    /**
     * @description 根据条件查询年度计划
     * @param condition 查询条件
     * @return EnterpriseYearPlan 年度计划
     */
    List<EnterpriseYearPlan> findYearplansByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件查询年度计划扩展
     * @param condition 查询条件
     * @return EnterpriseYearPlanCustom 年度计划扩展pojo
     */
    EnterpriseYearPlanCustom getYearplanByCondition(Map<String, Object> condition);

    /**
     * @description 根据条件查询年度检测计划扩展
     * @param condition 查询条件
     * @return EnterpriseYearPlanCustom 年度计划扩展pojo
     */
    List<EnterpriseYearPlanCustom> listYearCheckPlans(Map<String, Object> condition);

    JSONObject getCheckPlanHarmFactors(Map<String, Object> condition);

    int insertYearCheckPlan(EnterpriseYearPlanCustom enterpriseYearPlanCustom);

    int updateYearCheckPlan(EnterpriseYearPlanCustom enterpriseYearPlanCustom);

    EnterpriseYearPlanCustom getYearCheckPlanById(Long id);

    int getYearPlanCompletionNum(Map<String, Object> condition);

    void deleteYearPlanById(Long id);
}
