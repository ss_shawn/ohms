package com.lanware.ehs.enterprise.toxicdrill.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.toxicdrill.service.ToxicDrillService;
import com.lanware.ehs.mapper.custom.EnterpriseToxicDrillCustomMapper;
import com.lanware.ehs.pojo.EnterpriseToxicDrill;
import com.lanware.ehs.pojo.EnterpriseToxicDrillExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description 高毒演练记录interface实现类
 */
@Service
public class ToxicDrillServiceImpl implements ToxicDrillService {
    /** 注入enterpriseToxicDrillsCustomMapper的bean */
    @Autowired
    private EnterpriseToxicDrillCustomMapper enterpriseToxicDrillCustomMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;

    /**
     * @description 查询高毒演练记录list
     * @param pageNum 请求的页数
     * @param pageSize 每页数据个数
     * @param condition 查询条件
     * @return com.alibaba.fastjson.JSONObject 返回包含enterpriseToxicDrills和totalNum的result
     */
    @Override
    public JSONObject listToxicDrills(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        JSONObject result = new JSONObject();
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        List<EnterpriseToxicDrill> enterpriseToxicDrills
                = enterpriseToxicDrillCustomMapper.listToxicDrills(condition);
        result.put("enterpriseToxicDrills", enterpriseToxicDrills);
        if (pageNum != null && pageSize != null) {
            PageInfo<EnterpriseToxicDrill> pageInfo = new PageInfo<EnterpriseToxicDrill>(enterpriseToxicDrills);
            result.put("totalNum", pageInfo.getTotal());
        }
        return result;
    }

    /**
     * @description 根据id查询高毒演练记录
     * @param id 高毒演练记录id
     * @return com.lanware.management.pojo.EnterpriseToxicDrills 返回EnterpriseToxicDrills对象
     */
    @Override
    public EnterpriseToxicDrill getToxicDrillById(Long id) {
        return enterpriseToxicDrillCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 新增高毒演练记录
     * @param enterpriseToxicDrill 高毒演练记录pojo
     * @param drillFile 演练记录
     * @param proveFile 证明文件
     * @return int 插入成功的数目
     */
    @Override
    @Transactional
    public int insertToxicDrill(EnterpriseToxicDrill enterpriseToxicDrill
            , MultipartFile drillFile, MultipartFile proveFile) {
        // 新增数据先保存，以便获取id
        enterpriseToxicDrillCustomMapper.insert(enterpriseToxicDrill);
        // 上传并保存演练记录
        if (drillFile != null){
            String drill = "/" + enterpriseToxicDrill.getEnterpriseId() + "/" + ModuleName.TOXICDRILL.getValue()
                    + "/" + enterpriseToxicDrill.getId() + "/" + drillFile.getOriginalFilename();
            try {
                ossTools.uploadStream(drill, drillFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传演练记录失败", e);
            }
            enterpriseToxicDrill.setContent(drill);
        }
        // 上传并保存证明文件
        if (proveFile != null){
            String prove = "/" + enterpriseToxicDrill.getEnterpriseId() + "/" + ModuleName.TOXICDRILL.getValue()
                    + "/" + enterpriseToxicDrill.getId() + "/" + proveFile.getOriginalFilename();
            try {
                ossTools.uploadStream(prove, proveFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传证明文件失败", e);
            }
            enterpriseToxicDrill.setProveFile(prove);
        }
        return enterpriseToxicDrillCustomMapper.updateByPrimaryKeySelective(enterpriseToxicDrill);
    }

    /**
     * @description 更新高毒演练记录
     * @param enterpriseToxicDrill 高毒演练记录pojo
     * @param drillFile 演练记录
     * @param proveFile 证明文件
     * @return int 更新成功的数目
     */
    @Override
    public int updateToxicDrill(EnterpriseToxicDrill enterpriseToxicDrill
            , MultipartFile drillFile, MultipartFile proveFile) {
        // 上传并保存演练记录
        if (drillFile != null) {
            // 上传了新的演练记录
            if (!StringUtils.isEmpty(enterpriseToxicDrill.getContent())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseToxicDrill.getContent());
            }
            String drill = "/" + enterpriseToxicDrill.getEnterpriseId() + "/" + ModuleName.TOXICDRILL.getValue()
                    + "/" + enterpriseToxicDrill.getId() + "/" + drillFile.getOriginalFilename();
            try {
                ossTools.uploadStream(drill, drillFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传演练记录失败", e);
            }
            enterpriseToxicDrill.setContent(drill);
        }
        // 上传并保存证明文件
        if (proveFile != null) {
            // 上传了新的证明文件
            if (!StringUtils.isEmpty(enterpriseToxicDrill.getProveFile())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseToxicDrill.getProveFile());
            }
            String prove = "/" + enterpriseToxicDrill.getEnterpriseId() + "/" + ModuleName.TOXICDRILL.getValue()
                    + "/" + enterpriseToxicDrill.getId() + "/" + proveFile.getOriginalFilename();
            try {
                ossTools.uploadStream(prove, proveFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传证明文件失败", e);
            }
            enterpriseToxicDrill.setProveFile(prove);
        }
        return enterpriseToxicDrillCustomMapper.updateByPrimaryKeySelective(enterpriseToxicDrill);
    }

    /**
     * @description 删除高毒演练记录
     * @param id 要删除的id
     * @param drillFile 要删除的演练记录路径
     * @param proveFile 要删除的证明文件路径
     * @return int 删除成功的数目
     */
    @Override
    public int deleteToxicDrill(Long id, String drillFile, String proveFile) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(drillFile);
        ossTools.deleteOSS(proveFile);
        // 删除数据库中对应的数据
        return enterpriseToxicDrillCustomMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除高毒演练记录
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteToxicDrills(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseToxicDrill enterpriseToxicDrill = enterpriseToxicDrillCustomMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseToxicDrill.getContent());
            ossTools.deleteOSS(enterpriseToxicDrill.getProveFile());
        }
        // 删除数据库中对应的数据
        EnterpriseToxicDrillExample enterpriseToxicDrillExample = new EnterpriseToxicDrillExample();
        EnterpriseToxicDrillExample.Criteria criteria = enterpriseToxicDrillExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseToxicDrillCustomMapper.deleteByExample(enterpriseToxicDrillExample);
    }
}
