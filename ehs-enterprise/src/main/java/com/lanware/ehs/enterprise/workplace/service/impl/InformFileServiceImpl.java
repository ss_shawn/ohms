package com.lanware.ehs.enterprise.workplace.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.enterprise.ModuleName;
import com.lanware.ehs.enterprise.workplace.service.InformFileService;
import com.lanware.ehs.mapper.EnterpriseRelationFileMapper;
import com.lanware.ehs.pojo.EnterpriseRelationFile;
import com.lanware.ehs.pojo.EnterpriseRelationFileExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @description 高毒物品告知卡interface实现类
 */
@Service
public class InformFileServiceImpl implements InformFileService {
    /** 注入enterpriseRelationFileMapper的bean */
    @Autowired
    private EnterpriseRelationFileMapper enterpriseRelationFileMapper;

    @Autowired
    /** 注入ossTools的bean */
    private OSSTools ossTools;
    
    /**
     * @description 查询高毒物品告知卡list
     * @param relationId 关联id
     * @param relationModule 关联模块
     * @return 返回包含enterpriseRelationFiles的result
     */
    @Override
    public JSONObject listInformFiles(Long relationId, String relationModule) {
        JSONObject result = new JSONObject();
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria = enterpriseRelationFileExample.createCriteria();
        criteria.andRelationIdEqualTo(relationId);
        criteria.andRelationModuleEqualTo(relationModule);
        List<EnterpriseRelationFile> enterpriseRelationFiles
                = enterpriseRelationFileMapper.selectByExample(enterpriseRelationFileExample);
        result.put("enterpriseRelationFiles", enterpriseRelationFiles);
        return result;
    }

    /**
     * @description 根据id查询高毒物品告知卡
     * @param id
     * @return EnterpriseRelationFile
     */
    @Override
    public EnterpriseRelationFile getInformFileById(Long id) {
        return enterpriseRelationFileMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional
    public int insertInformFile(EnterpriseRelationFile enterpriseRelationFile
            , MultipartFile informFile, Long enterpriseId) {
        // 新增数据先保存，以便获取id
        enterpriseRelationFileMapper.insert(enterpriseRelationFile);
        // 上传并保存高毒物品告知卡文件
        if (informFile != null){
            String inform = "/" + enterpriseId + "/" + ModuleName.WORKPLACE.getValue()
                    + "/" + enterpriseRelationFile.getRelationId() + "/" + enterpriseRelationFile.getId()
                    + "/" + informFile.getOriginalFilename();
            try {
                ossTools.uploadStream(inform, informFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传高毒物品告知卡文件失败", e);
            }
            enterpriseRelationFile.setFileName(informFile.getOriginalFilename());
            enterpriseRelationFile.setFilePath(inform);
        }
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
    }

    @Override
    public int updateInformFile(EnterpriseRelationFile enterpriseRelationFile
            , MultipartFile informFile, Long enterpriseId) {
        // 上传并保存高毒物品告知卡文件
        if (informFile != null) {
            // 上传了新的高毒物品告知卡文件
            if (!StringUtils.isEmpty(enterpriseRelationFile.getFilePath())) {
                // 删除旧文件
                ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
            }
            String inform = "/" + enterpriseId + "/" + ModuleName.WORKPLACE.getValue()
                    + "/" + enterpriseRelationFile.getRelationId() + "/" + enterpriseRelationFile.getId()
                    + "/" + informFile.getOriginalFilename();
            try {
                ossTools.uploadStream(inform, informFile.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                throw new EhsException("上传高毒物品告知卡文件失败", e);
            }
            enterpriseRelationFile.setFileName(informFile.getOriginalFilename());
            enterpriseRelationFile.setFilePath(inform);
        }
        return enterpriseRelationFileMapper.updateByPrimaryKeySelective(enterpriseRelationFile);
    }

    /**
     * @description 删除高毒物品告知卡
     * @param id 要删除的id
     * @param filePath 要删除的文件路径
     * @return int 删除成功的数目
     */
    @Override
    public int deleteInformFile(Long id, String filePath) {
        // 删除oss服务器上对应的文件
        ossTools.deleteOSS(filePath);
        // 删除数据库中对应的数据
        return enterpriseRelationFileMapper.deleteByPrimaryKey(id);
    }

    /**
     * @description 批量删除高毒物品告知卡
     * @param ids 要删除的数组ids
     * @return int 删除成功的数目
     */
    @Override
    public int deleteInformFiles(Long[] ids) {
        for (Long id : ids) {
            // 删除oss服务器上对应的文件
            EnterpriseRelationFile enterpriseRelationFile = enterpriseRelationFileMapper.selectByPrimaryKey(id);
            ossTools.deleteOSS(enterpriseRelationFile.getFilePath());
        }
        EnterpriseRelationFileExample enterpriseRelationFileExample = new EnterpriseRelationFileExample();
        EnterpriseRelationFileExample.Criteria criteria = enterpriseRelationFileExample.createCriteria();
        criteria.andIdIn(Arrays.asList(ids));
        return enterpriseRelationFileMapper.deleteByExample(enterpriseRelationFileExample);
    }
}
