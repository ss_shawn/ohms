package com.lanware.ehs.others.controller;

import com.lanware.ehs.common.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("othersView")
@RequestMapping(Constants.VIEW_PREFIX + "others")
public class ViewController {

    @GetMapping("/form")
    public String form() {
        return "others/form";
    }

    @GetMapping("/form/group")
    public String formGroup() {
        return "others/formGroup";
    }

    @GetMapping("/tools")
    public String tools() {
        return "others/tools";
    }

    @GetMapping("/icon")
    public String icon() {
        return "others/icon";
    }

    @GetMapping("/others")
    public String others() {
        return "others/others";
    }

}
