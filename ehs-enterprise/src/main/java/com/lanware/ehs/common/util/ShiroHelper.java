package com.lanware.ehs.common.util;

import com.lanware.ehs.shiro.authentication.ShiroRealm;
import org.apache.shiro.authz.AuthorizationInfo;
import org.springframework.stereotype.Component;

@Component
public class ShiroHelper extends ShiroRealm {

    /**
     * 获取当前用户的角色和权限集合
     *
     * @return AuthorizationInfo
     */
    public AuthorizationInfo getCurrentuserAuthorizationInfo() {
        return super.doGetAuthorizationInfo(null);
    }
}
