package com.lanware.ehs.common.controller;

import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.OSSTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class CommonController {

    private OSSTools ossTools;

    @Autowired
    public CommonController(OSSTools ossTools) {
        this.ossTools = ossTools;
    }

    /**
     * 文件下载
     * @param filePath 文件路径
     * @param response response对象
     * @return 失败信息
     */
    @RequestMapping("/downloadFile")
    @ResponseBody
    public ResultFormat downloadFile(@RequestParam("filePath") String filePath, HttpServletResponse response) {
        try {
            ossTools.downloadFile(filePath.substring(filePath.lastIndexOf("/") + 1), filePath, response);
        } catch (IOException e) {
            e.printStackTrace();
            return ResultFormat.error("下载文件失败");
        }
        return null;
    }
}
