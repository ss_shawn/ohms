package com.lanware.ehs.common.util;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Iterator;

/**
 * @description 基于POI的javaee导出Excel工具类(导出.xls表格)
 */
@Component
public class ExportExcelUtil {
    /**
     * @description
     * @param response 响应
     * @param fileName excel文件名
     * @param excelHeader excel表头数组，存放"危害因素#harmFactorName"格式字符串，"危害因素"为excel标题行，"harmFactorName"为对象字段名
     * @param dataList 数据集合，与表头数组中的字段名一致
     * @return HSSFWorkbook
     */
    public static <T> void export(HttpServletResponse response, String fileName, String[] excelHeader
            , Collection<T> dataList) throws Exception {
        // 设置响应头
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        // 创建Workbook对象，对应一个excel
        HSSFWorkbook workbook  = new HSSFWorkbook();
        // 在workbook中添加一个sheet，对应excel中的sheet
        HSSFSheet sheet = workbook.createSheet(fileName);
        // 设置表头样式
        HSSFCellStyle style = workbook.createCellStyle();
        // 创建一个字体样式(红色)
        Font font = workbook.createFont();
        font.setColor(HSSFColor.RED.index);
        // 字体对象放入表头样式对象
        style.setFont(font);
        // 标题数组
        String[] titleArray = new String[excelHeader.length];
        // 字段名数组
        String[] fieldArray = new String[excelHeader.length];
        for(int i = 0; i < excelHeader.length; i++) {
            String[] tempArray = excelHeader[i].split("#");
            titleArray[i] = tempArray[0];
            fieldArray[i] = tempArray[1];
        }
        // 在sheet第一行添加说明，合并后的单元格
        sheet.addMergedRegion(new CellRangeAddress(0,0,0,11));
        HSSFRow explainRow = sheet.createRow(0);
        HSSFCell explainCell = explainRow.createCell(0);
        explainCell.setCellValue("说明：只能填写接触时间(数字，最多1位小数),检测结果(数字，最多2位小数),单位(如：mg/m³),"
                + "如果要添加新数据,请复制粘贴检测结果ID所在行,再进行接触时间、检测结果、单位的填写");
        // 在sheet中第二行添加标题行
        HSSFRow row = sheet.createRow(1);
        // 标题行赋值
        for (int i = 0; i < titleArray.length; i++) {
            HSSFCell titleCell = row.createCell(i);
            titleCell.setCellValue(titleArray[i]);
            // 将接触时间、检测结果、单位标红，提示用户填写数据
            if (i == 9 || i == 10 || i == 11) {
                titleCell.setCellStyle(style);
            }
        }
        // 从sheet第三行开始，遍历数据集合，产生数据行
        Iterator<T> it = dataList.iterator();
        int index = 1;
        while (it.hasNext()) {
            index++;
            row = sheet.createRow(index);
            T t = (T)it.next();
            // 利用反射，根据传过来的字段名数组，动态调用对应的getXxx()方法得到属性值
            for (int i = 0; i < (fieldArray.length-3); i++) {
                HSSFCell dataCell = row.createCell(i);
                String fieldName = fieldArray[i];
                String getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                Class<? extends Object> tCls = t.getClass();
                Method getMethod = tCls.getMethod(getMethodName, new Class[]{});
                Object value = getMethod.invoke(t, new Object[]{});
                if (value != null) {
                    dataCell.setCellValue(value.toString());
                }
            }
        }
        // 自动调整列宽
        for(int i = 0; i < excelHeader.length; i++) {
            sheet.autoSizeColumn((short) i);
        }
        // workbook写入流
        OutputStream os = response.getOutputStream();
        workbook.write(os);
        workbook.close();
        os.flush();
        os.close();
    }
}
