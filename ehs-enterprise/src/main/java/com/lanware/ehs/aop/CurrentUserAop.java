package com.lanware.ehs.aop;

import com.lanware.ehs.common.annotation.CurrentUser;
import com.lanware.ehs.enterprise.user.service.UserService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@Aspect
@Component
public class CurrentUserAop {
	@Autowired
	private UserService userService;

	@Pointcut("execution(public * com.lanware.ehs.enterprise..controller.*.*(..))")
	public void pointcut(){

	}

	@Around("pointcut()")
	public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		Object result = null;
		Object[] args = proceedingJoinPoint.getArgs();
		MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
		Method method = methodSignature.getMethod();
		Annotation[][] annotations = method.getParameterAnnotations();
		//验证是否存在自定义注解
		for(int i = 0; i < args.length; i++){
			for(Annotation annotation : annotations[i]){
				if(annotation.annotationType().equals(CurrentUser.class)){
					EnterpriseUserCustom userCustom = (EnterpriseUserCustom)SecurityUtils.getSubject().getPrincipal();
					userCustom = userService.findByName(userCustom.getUsername());
					args[i] = userCustom;
				}
			}
		}
		result = proceedingJoinPoint.proceed(args);
		return result;
	}
}
