<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
  <w:ignoreSubtree w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2"/>
  <o:DocumentProperties>
    <o:Author>微软用户</o:Author>
    <o:LastAuthor>wym</o:LastAuthor>
    <o:Revision>2</o:Revision>
    <o:TotalTime>0</o:TotalTime>
    <o:Created>2019-10-15T07:49:00Z</o:Created>
    <o:LastSaved>2019-10-15T07:49:00Z</o:LastSaved>
    <o:Pages>1</o:Pages>
    <o:Words>30</o:Words>
    <o:Characters>173</o:Characters>
    <o:Company>微软中国</o:Company>
    <o:Lines>1</o:Lines>
    <o:Paragraphs>1</o:Paragraphs>
    <o:CharactersWithSpaces>202</o:CharactersWithSpaces>
    <o:Version>12</o:Version>
  </o:DocumentProperties>
  <o:CustomDocumentProperties>
    <o:KSOProductBuildVer dt:dt="string">2052-8.1.0.3526</o:KSOProductBuildVer>
  </o:CustomDocumentProperties>
  <w:fonts>
    <w:defaultFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
    <w:font w:name="Times New Roman">
      <w:panose-1 w:val="02020603050405020304"/>
      <w:charset w:val="00"/>
      <w:family w:val="Roman"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="E0002AFF" w:usb-1="C0007841" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="宋体">
      <w:altName w:val="SimSun"/>
      <w:panose-1 w:val="02010600030101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="auto"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="黑体">
      <w:altName w:val="SimHei"/>
      <w:panose-1 w:val="02010609060101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Cambria Math">
      <w:panose-1 w:val="02040503050406030204"/>
      <w:charset w:val="00"/>
      <w:family w:val="Roman"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="E00002FF" w:usb-1="420024FF" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="仿宋_GB2312">
      <w:altName w:val="黑体"/>
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Verdana">
      <w:panose-1 w:val="020B0604030504040204"/>
      <w:charset w:val="00"/>
      <w:family w:val="Swiss"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="A10006FF" w:usb-1="4000205B" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="楷体_GB2312">
      <w:altName w:val="Arial Unicode MS"/>
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="00000000" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="@宋体">
      <w:panose-1 w:val="02010600030101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="auto"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="@黑体">
      <w:panose-1 w:val="02010609060101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="@仿宋_GB2312">
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="@楷体_GB2312">
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="00000000" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
    </w:font>
  </w:fonts>
  <w:lists>
    <w:listDef w:listDefId="0">
      <w:lsid w:val="2746691A"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="7EA04EFC"/>
      <w:lvl w:ilvl="0" w:tplc="2228B75A">
        <w:start w:val="1"/>
        <w:nfc w:val="26"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="1">
      <w:lsid w:val="2EA706A3"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="7CE60A16"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="2">
      <w:lsid w:val="428077C1"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="576C44D2"/>
      <w:lvl w:ilvl="0" w:tplc="AB7C271A">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="3">
      <w:lsid w:val="4370654A"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="9F389E5A"/>
      <w:lvl w:ilvl="0" w:tplc="33A22494">
        <w:start w:val="1"/>
        <w:nfc w:val="26"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1052" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1772" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2492" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3212" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3932" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4652" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5372" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6092" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6812" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="4">
      <w:lsid w:val="47635688"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="7810830C"/>
      <w:lvl w:ilvl="0" w:tplc="AB2675B0">
        <w:start w:val="1"/>
        <w:nfc w:val="26"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="5">
      <w:lsid w:val="625F2313"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="BAE0D7B4"/>
      <w:lvl w:ilvl="0" w:tplc="93D4966A">
        <w:start w:val="1"/>
        <w:nfc w:val="29"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
          <w:b/>
          <w:lang w:val="EN-US"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="6">
      <w:lsid w:val="70E67E38"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="A26216EE"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:h-ansi="Times New Roman" w:cs="Times New Roman" w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="7">
      <w:lsid w:val="72865796"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="6FBCE39A"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="8">
      <w:lsid w:val="74AB1CDB"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="7810830C"/>
      <w:lvl w:ilvl="0" w:tplc="AB2675B0">
        <w:start w:val="1"/>
        <w:nfc w:val="26"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="9">
      <w:lsid w:val="7A8E6FE4"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="148A6DBC"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="10">
      <w:lsid w:val="7DC42279"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="DB828ED4"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:list w:ilfo="1">
      <w:ilst w:val="8"/>
    </w:list>
    <w:list w:ilfo="2">
      <w:ilst w:val="4"/>
    </w:list>
    <w:list w:ilfo="3">
      <w:ilst w:val="5"/>
    </w:list>
    <w:list w:ilfo="4">
      <w:ilst w:val="10"/>
    </w:list>
    <w:list w:ilfo="5">
      <w:ilst w:val="0"/>
    </w:list>
    <w:list w:ilfo="6">
      <w:ilst w:val="1"/>
    </w:list>
    <w:list w:ilfo="7">
      <w:ilst w:val="3"/>
    </w:list>
    <w:list w:ilfo="8">
      <w:ilst w:val="9"/>
    </w:list>
    <w:list w:ilfo="9">
      <w:ilst w:val="7"/>
    </w:list>
    <w:list w:ilfo="10">
      <w:ilst w:val="2"/>
    </w:list>
    <w:list w:ilfo="11">
      <w:ilst w:val="6"/>
    </w:list>
  </w:lists>
  <w:styles>
    <w:versionOfBuiltInStylenames w:val="7"/>
    <w:latentStyles w:defLockedState="off" w:latentStyleCount="267">
      <w:lsdException w:name="Normal"/>
      <w:lsdException w:name="heading 1"/>
      <w:lsdException w:name="heading 2"/>
      <w:lsdException w:name="heading 3"/>
      <w:lsdException w:name="heading 4"/>
      <w:lsdException w:name="heading 5"/>
      <w:lsdException w:name="heading 6"/>
      <w:lsdException w:name="heading 7"/>
      <w:lsdException w:name="heading 8"/>
      <w:lsdException w:name="heading 9"/>
      <w:lsdException w:name="toc 1"/>
      <w:lsdException w:name="toc 2"/>
      <w:lsdException w:name="toc 3"/>
      <w:lsdException w:name="toc 4"/>
      <w:lsdException w:name="toc 5"/>
      <w:lsdException w:name="toc 6"/>
      <w:lsdException w:name="toc 7"/>
      <w:lsdException w:name="toc 8"/>
      <w:lsdException w:name="toc 9"/>
      <w:lsdException w:name="caption"/>
      <w:lsdException w:name="Title"/>
      <w:lsdException w:name="Default Paragraph Font"/>
      <w:lsdException w:name="Subtitle"/>
      <w:lsdException w:name="Strong"/>
      <w:lsdException w:name="Emphasis"/>
      <w:lsdException w:name="Normal (Web)"/>
      <w:lsdException w:name="Table Grid"/>
      <w:lsdException w:name="Placeholder Text"/>
      <w:lsdException w:name="No Spacing"/>
      <w:lsdException w:name="Light Shading"/>
      <w:lsdException w:name="Light List"/>
      <w:lsdException w:name="Light Grid"/>
      <w:lsdException w:name="Medium Shading 1"/>
      <w:lsdException w:name="Medium Shading 2"/>
      <w:lsdException w:name="Medium List 1"/>
      <w:lsdException w:name="Medium List 2"/>
      <w:lsdException w:name="Medium Grid 1"/>
      <w:lsdException w:name="Medium Grid 2"/>
      <w:lsdException w:name="Medium Grid 3"/>
      <w:lsdException w:name="Dark List"/>
      <w:lsdException w:name="Colorful Shading"/>
      <w:lsdException w:name="Colorful List"/>
      <w:lsdException w:name="Colorful Grid"/>
      <w:lsdException w:name="Light Shading Accent 1"/>
      <w:lsdException w:name="Light List Accent 1"/>
      <w:lsdException w:name="Light Grid Accent 1"/>
      <w:lsdException w:name="Medium Shading 1 Accent 1"/>
      <w:lsdException w:name="Medium Shading 2 Accent 1"/>
      <w:lsdException w:name="Medium List 1 Accent 1"/>
      <w:lsdException w:name="Revision"/>
      <w:lsdException w:name="List Paragraph"/>
      <w:lsdException w:name="Quote"/>
      <w:lsdException w:name="Intense Quote"/>
      <w:lsdException w:name="Medium List 2 Accent 1"/>
      <w:lsdException w:name="Medium Grid 1 Accent 1"/>
      <w:lsdException w:name="Medium Grid 2 Accent 1"/>
      <w:lsdException w:name="Medium Grid 3 Accent 1"/>
      <w:lsdException w:name="Dark List Accent 1"/>
      <w:lsdException w:name="Colorful Shading Accent 1"/>
      <w:lsdException w:name="Colorful List Accent 1"/>
      <w:lsdException w:name="Colorful Grid Accent 1"/>
      <w:lsdException w:name="Light Shading Accent 2"/>
      <w:lsdException w:name="Light List Accent 2"/>
      <w:lsdException w:name="Light Grid Accent 2"/>
      <w:lsdException w:name="Medium Shading 1 Accent 2"/>
      <w:lsdException w:name="Medium Shading 2 Accent 2"/>
      <w:lsdException w:name="Medium List 1 Accent 2"/>
      <w:lsdException w:name="Medium List 2 Accent 2"/>
      <w:lsdException w:name="Medium Grid 1 Accent 2"/>
      <w:lsdException w:name="Medium Grid 2 Accent 2"/>
      <w:lsdException w:name="Medium Grid 3 Accent 2"/>
      <w:lsdException w:name="Dark List Accent 2"/>
      <w:lsdException w:name="Colorful Shading Accent 2"/>
      <w:lsdException w:name="Colorful List Accent 2"/>
      <w:lsdException w:name="Colorful Grid Accent 2"/>
      <w:lsdException w:name="Light Shading Accent 3"/>
      <w:lsdException w:name="Light List Accent 3"/>
      <w:lsdException w:name="Light Grid Accent 3"/>
      <w:lsdException w:name="Medium Shading 1 Accent 3"/>
      <w:lsdException w:name="Medium Shading 2 Accent 3"/>
      <w:lsdException w:name="Medium List 1 Accent 3"/>
      <w:lsdException w:name="Medium List 2 Accent 3"/>
      <w:lsdException w:name="Medium Grid 1 Accent 3"/>
      <w:lsdException w:name="Medium Grid 2 Accent 3"/>
      <w:lsdException w:name="Medium Grid 3 Accent 3"/>
      <w:lsdException w:name="Dark List Accent 3"/>
      <w:lsdException w:name="Colorful Shading Accent 3"/>
      <w:lsdException w:name="Colorful List Accent 3"/>
      <w:lsdException w:name="Colorful Grid Accent 3"/>
      <w:lsdException w:name="Light Shading Accent 4"/>
      <w:lsdException w:name="Light List Accent 4"/>
      <w:lsdException w:name="Light Grid Accent 4"/>
      <w:lsdException w:name="Medium Shading 1 Accent 4"/>
      <w:lsdException w:name="Medium Shading 2 Accent 4"/>
      <w:lsdException w:name="Medium List 1 Accent 4"/>
      <w:lsdException w:name="Medium List 2 Accent 4"/>
      <w:lsdException w:name="Medium Grid 1 Accent 4"/>
      <w:lsdException w:name="Medium Grid 2 Accent 4"/>
      <w:lsdException w:name="Medium Grid 3 Accent 4"/>
      <w:lsdException w:name="Dark List Accent 4"/>
      <w:lsdException w:name="Colorful Shading Accent 4"/>
      <w:lsdException w:name="Colorful List Accent 4"/>
      <w:lsdException w:name="Colorful Grid Accent 4"/>
      <w:lsdException w:name="Light Shading Accent 5"/>
      <w:lsdException w:name="Light List Accent 5"/>
      <w:lsdException w:name="Light Grid Accent 5"/>
      <w:lsdException w:name="Medium Shading 1 Accent 5"/>
      <w:lsdException w:name="Medium Shading 2 Accent 5"/>
      <w:lsdException w:name="Medium List 1 Accent 5"/>
      <w:lsdException w:name="Medium List 2 Accent 5"/>
      <w:lsdException w:name="Medium Grid 1 Accent 5"/>
      <w:lsdException w:name="Medium Grid 2 Accent 5"/>
      <w:lsdException w:name="Medium Grid 3 Accent 5"/>
      <w:lsdException w:name="Dark List Accent 5"/>
      <w:lsdException w:name="Colorful Shading Accent 5"/>
      <w:lsdException w:name="Colorful List Accent 5"/>
      <w:lsdException w:name="Colorful Grid Accent 5"/>
      <w:lsdException w:name="Light Shading Accent 6"/>
      <w:lsdException w:name="Light List Accent 6"/>
      <w:lsdException w:name="Light Grid Accent 6"/>
      <w:lsdException w:name="Medium Shading 1 Accent 6"/>
      <w:lsdException w:name="Medium Shading 2 Accent 6"/>
      <w:lsdException w:name="Medium List 1 Accent 6"/>
      <w:lsdException w:name="Medium List 2 Accent 6"/>
      <w:lsdException w:name="Medium Grid 1 Accent 6"/>
      <w:lsdException w:name="Medium Grid 2 Accent 6"/>
      <w:lsdException w:name="Medium Grid 3 Accent 6"/>
      <w:lsdException w:name="Dark List Accent 6"/>
      <w:lsdException w:name="Colorful Shading Accent 6"/>
      <w:lsdException w:name="Colorful List Accent 6"/>
      <w:lsdException w:name="Colorful Grid Accent 6"/>
      <w:lsdException w:name="Subtle Emphasis"/>
      <w:lsdException w:name="Intense Emphasis"/>
      <w:lsdException w:name="Subtle Reference"/>
      <w:lsdException w:name="Intense Reference"/>
      <w:lsdException w:name="Book Title"/>
      <w:lsdException w:name="Bibliography"/>
      <w:lsdException w:name="TOC Heading"/>
    </w:latentStyles>
    <w:style w:type="paragraph" w:default="on" w:styleId="a">
      <w:name w:val="Normal"/>
      <wx:uiName wx:val="正文"/>
      <w:pPr>
        <w:widowControl w:val="off"/>
        <w:jc w:val="both"/>
      </w:pPr>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
        <w:kern w:val="2"/>
        <w:sz w:val="21"/>
        <w:sz-cs w:val="24"/>
        <w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
      </w:rPr>
    </w:style>
    <w:style w:type="character" w:default="on" w:styleId="a0">
      <w:name w:val="Default Paragraph Font"/>
      <wx:uiName wx:val="默认段落字体"/>
    </w:style>
    <w:style w:type="table" w:default="on" w:styleId="a1">
      <w:name w:val="Normal Table"/>
      <wx:uiName wx:val="普通表格"/>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
        <w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
      </w:rPr>
      <w:tblPr>
        <w:tblInd w:w="0" w:type="dxa"/>
        <w:tblCellMar>
          <w:top w:w="0" w:type="dxa"/>
          <w:left w:w="108" w:type="dxa"/>
          <w:bottom w:w="0" w:type="dxa"/>
          <w:right w:w="108" w:type="dxa"/>
        </w:tblCellMar>
      </w:tblPr>
    </w:style>
    <w:style w:type="list" w:default="on" w:styleId="a2">
      <w:name w:val="No List"/>
      <wx:uiName wx:val="无列表"/>
    </w:style>
    <w:style w:type="character" w:styleId="a3">
      <w:name w:val="page number"/>
      <wx:uiName wx:val="页码"/>
      <w:basedOn w:val="a0"/>
    </w:style>
    <w:style w:type="character" w:styleId="a4">
      <w:name w:val="Strong"/>
      <wx:uiName wx:val="要点"/>
      <w:rPr>
        <w:b/>
        <w:b-cs/>
      </w:rPr>
    </w:style>
    <w:style w:type="character" w:styleId="msochangeprop0">
      <w:name w:val="msochangeprop0"/>
      <w:basedOn w:val="a0"/>
    </w:style>
    <w:style w:type="paragraph" w:styleId="a5">
      <w:name w:val="Body Text Indent"/>
      <wx:uiName wx:val="正文文本缩进"/>
      <w:basedOn w:val="a"/>
      <w:pPr>
        <w:ind w:first-line-chars="200" w:first-line="643"/>
      </w:pPr>
      <w:rPr>
        <w:rFonts w:fareast="楷体_GB2312"/>
        <wx:font wx:val="Times New Roman"/>
        <w:b/>
        <w:sz w:val="32"/>
      </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="a6">
      <w:name w:val="header"/>
      <wx:uiName wx:val="页眉"/>
      <w:basedOn w:val="a"/>
      <w:rsid w:val="00342736"/>
      <w:pPr>
        <w:tabs>
          <w:tab w:val="center" w:pos="4153"/>
          <w:tab w:val="right" w:pos="8306"/>
        </w:tabs>
        <w:snapToGrid w:val="off"/>
        <w:jc w:val="center"/>
      </w:pPr>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
        <w:sz w:val="18"/>
        <w:sz-cs w:val="18"/>
      </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="a7">
      <w:name w:val="Normal (Web)"/>
      <wx:uiName wx:val="普通(网站)"/>
      <w:basedOn w:val="a"/>
      <w:pPr>
        <w:widowControl/>
        <w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
        <w:jc w:val="left"/>
      </w:pPr>
      <w:rPr>
        <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
        <wx:font wx:val="宋体"/>
        <w:kern w:val="0"/>
        <w:sz w:val="24"/>
      </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="a8">
      <w:name w:val="footer"/>
      <wx:uiName wx:val="页脚"/>
      <w:basedOn w:val="a"/>
      <w:pPr>
        <w:tabs>
          <w:tab w:val="center" w:pos="4153"/>
          <w:tab w:val="right" w:pos="8306"/>
        </w:tabs>
        <w:snapToGrid w:val="off"/>
        <w:jc w:val="left"/>
      </w:pPr>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
        <w:sz w:val="18"/>
        <w:sz-cs w:val="18"/>
      </w:rPr>
    </w:style>
    <w:style w:type="table" w:styleId="a9">
      <w:name w:val="Table Grid"/>
      <wx:uiName wx:val="网格型"/>
      <w:basedOn w:val="a1"/>
      <w:rsid w:val="0076062C"/>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
      </w:rPr>
      <w:tblPr>
        <w:tblInd w:w="0" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblCellMar>
          <w:top w:w="0" w:type="dxa"/>
          <w:left w:w="108" w:type="dxa"/>
          <w:bottom w:w="0" w:type="dxa"/>
          <w:right w:w="108" w:type="dxa"/>
        </w:tblCellMar>
      </w:tblPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="aa">
      <w:name w:val="List Paragraph"/>
      <wx:uiName wx:val="列出段落"/>
      <w:basedOn w:val="a"/>
      <w:rsid w:val="003F265C"/>
      <w:pPr>
        <w:ind w:first-line-chars="200" w:first-line="420"/>
      </w:pPr>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
      </w:rPr>
    </w:style>
  </w:styles>
  <w:shapeDefaults>
    <o:shapedefaults v:ext="edit" spidmax="3074" fillcolor="#9cbee0" strokecolor="#739cc3">
      <v:fill color="#9cbee0" color2="#bbd5f0" type="gradient">
        <o:fill v:ext="view" type="gradientUnscaled"/>
      </v:fill>
      <v:stroke color="#739cc3" weight="1.25pt"/>
    </o:shapedefaults>
    <o:shapelayout v:ext="edit">
      <o:idmap v:ext="edit" data="1"/>
    </o:shapelayout>
  </w:shapeDefaults>
  <w:docPr>
    <w:view w:val="print"/>
    <w:zoom w:percent="100"/>
    <w:doNotEmbedSystemFonts/>
    <w:bordersDontSurroundHeader/>
    <w:bordersDontSurroundFooter/>
    <w:attachedTemplate w:val="Normal"/>
    <w:stylePaneFormatFilter w:val="3F01"/>
    <w:defaultTabStop w:val="420"/>
    <w:drawingGridVerticalSpacing w:val="156"/>
    <w:displayHorizontalDrawingGridEvery w:val="0"/>
    <w:displayVerticalDrawingGridEvery w:val="2"/>
    <w:punctuationKerning/>
    <w:characterSpacingControl w:val="CompressPunctuation"/>
    <w:webPageEncoding w:val="x-cp20936"/>
    <w:optimizeForBrowser/>
    <w:allowPNG/>
    <w:targetScreenSz w:val="1024x768"/>
    <w:validateAgainstSchema w:val="off"/>
    <w:saveInvalidXML w:val="off"/>
    <w:ignoreMixedContent w:val="off"/>
    <w:alwaysShowPlaceholderText w:val="off"/>
    <w:doNotUnderlineInvalidXML/>
    <w:hdrShapeDefaults>
      <o:shapedefaults v:ext="edit" spidmax="3074" fillcolor="#9cbee0" strokecolor="#739cc3">
        <v:fill color="#9cbee0" color2="#bbd5f0" type="gradient">
          <o:fill v:ext="view" type="gradientUnscaled"/>
        </v:fill>
        <v:stroke color="#739cc3" weight="1.25pt"/>
      </o:shapedefaults>
    </w:hdrShapeDefaults>
    <w:footnotePr>
      <w:footnote w:type="separator">
        <w:p wsp:rsidR="00241DC0" wsp:rsidRDefault="00241DC0">
          <w:r>
            <w:separator/>
          </w:r>
        </w:p>
      </w:footnote>
      <w:footnote w:type="continuation-separator">
        <w:p wsp:rsidR="00241DC0" wsp:rsidRDefault="00241DC0">
          <w:r>
            <w:continuationSeparator/>
          </w:r>
        </w:p>
      </w:footnote>
    </w:footnotePr>
    <w:endnotePr>
      <w:endnote w:type="separator">
        <w:p wsp:rsidR="00241DC0" wsp:rsidRDefault="00241DC0">
          <w:r>
            <w:separator/>
          </w:r>
        </w:p>
      </w:endnote>
      <w:endnote w:type="continuation-separator">
        <w:p wsp:rsidR="00241DC0" wsp:rsidRDefault="00241DC0">
          <w:r>
            <w:continuationSeparator/>
          </w:r>
        </w:p>
      </w:endnote>
    </w:endnotePr>
    <w:compat>
      <w:spaceForUL/>
      <w:balanceSingleByteDoubleByteWidth/>
      <w:doNotLeaveBackslashAlone/>
      <w:ulTrailSpace/>
      <w:doNotExpandShiftReturn/>
      <w:adjustLineHeightInTable/>
      <w:breakWrappedTables/>
      <w:snapToGridInCell/>
      <w:wrapTextWithPunct/>
      <w:useAsianBreakRules/>
      <w:dontGrowAutofit/>
      <w:useFELayout/>
    </w:compat>
    <wsp:rsids>
      <wsp:rsidRoot wsp:val="00172A27"/>
      <wsp:rsid wsp:val="000034C7"/>
      <wsp:rsid wsp:val="00007A83"/>
      <wsp:rsid wsp:val="00053FB0"/>
      <wsp:rsid wsp:val="000628F4"/>
      <wsp:rsid wsp:val="00067C75"/>
      <wsp:rsid wsp:val="00074E77"/>
      <wsp:rsid wsp:val="00141825"/>
      <wsp:rsid wsp:val="00166D53"/>
      <wsp:rsid wsp:val="001A36A8"/>
      <wsp:rsid wsp:val="001B4260"/>
      <wsp:rsid wsp:val="001E7446"/>
      <wsp:rsid wsp:val="00226105"/>
      <wsp:rsid wsp:val="002405F8"/>
      <wsp:rsid wsp:val="00241DC0"/>
      <wsp:rsid wsp:val="002478D8"/>
      <wsp:rsid wsp:val="0025018E"/>
      <wsp:rsid wsp:val="00274EAD"/>
      <wsp:rsid wsp:val="002E3EE0"/>
      <wsp:rsid wsp:val="002E50FC"/>
      <wsp:rsid wsp:val="0032407B"/>
      <wsp:rsid wsp:val="00342736"/>
      <wsp:rsid wsp:val="003A643D"/>
      <wsp:rsid wsp:val="003F265C"/>
      <wsp:rsid wsp:val="00441119"/>
      <wsp:rsid wsp:val="004B1346"/>
      <wsp:rsid wsp:val="004B32A1"/>
      <wsp:rsid wsp:val="004C2F12"/>
      <wsp:rsid wsp:val="004F6D08"/>
      <wsp:rsid wsp:val="00503C55"/>
      <wsp:rsid wsp:val="00530BE7"/>
      <wsp:rsid wsp:val="005A7BDF"/>
      <wsp:rsid wsp:val="005B5625"/>
      <wsp:rsid wsp:val="00637774"/>
      <wsp:rsid wsp:val="00667B09"/>
      <wsp:rsid wsp:val="006746F2"/>
      <wsp:rsid wsp:val="006B3039"/>
      <wsp:rsid wsp:val="00722174"/>
      <wsp:rsid wsp:val="007221BC"/>
      <wsp:rsid wsp:val="00722AD0"/>
      <wsp:rsid wsp:val="00751389"/>
      <wsp:rsid wsp:val="0076062C"/>
      <wsp:rsid wsp:val="007C7E48"/>
      <wsp:rsid wsp:val="007D6C2B"/>
      <wsp:rsid wsp:val="008143DF"/>
      <wsp:rsid wsp:val="00823A31"/>
      <wsp:rsid wsp:val="00845656"/>
      <wsp:rsid wsp:val="00866861"/>
      <wsp:rsid wsp:val="00892A6B"/>
      <wsp:rsid wsp:val="0089510B"/>
      <wsp:rsid wsp:val="008A5B1A"/>
      <wsp:rsid wsp:val="008C1674"/>
      <wsp:rsid wsp:val="008C28CA"/>
      <wsp:rsid wsp:val="008C5E16"/>
      <wsp:rsid wsp:val="00986402"/>
      <wsp:rsid wsp:val="009B0FC8"/>
      <wsp:rsid wsp:val="009B1992"/>
      <wsp:rsid wsp:val="009F2018"/>
      <wsp:rsid wsp:val="009F3423"/>
      <wsp:rsid wsp:val="009F4552"/>
      <wsp:rsid wsp:val="00A351C9"/>
      <wsp:rsid wsp:val="00A64168"/>
      <wsp:rsid wsp:val="00A64221"/>
      <wsp:rsid wsp:val="00A95F4F"/>
      <wsp:rsid wsp:val="00AF7124"/>
      <wsp:rsid wsp:val="00B33F06"/>
      <wsp:rsid wsp:val="00BB6EFE"/>
      <wsp:rsid wsp:val="00BE085B"/>
      <wsp:rsid wsp:val="00C0023D"/>
      <wsp:rsid wsp:val="00C02551"/>
      <wsp:rsid wsp:val="00CB0CDA"/>
      <wsp:rsid wsp:val="00D21066"/>
      <wsp:rsid wsp:val="00D447E1"/>
      <wsp:rsid wsp:val="00D661EB"/>
      <wsp:rsid wsp:val="00D77D97"/>
      <wsp:rsid wsp:val="00DA0C15"/>
      <wsp:rsid wsp:val="00DB2C22"/>
      <wsp:rsid wsp:val="00DC0950"/>
      <wsp:rsid wsp:val="00DC0FFC"/>
      <wsp:rsid wsp:val="00DD76B1"/>
      <wsp:rsid wsp:val="00E2125F"/>
      <wsp:rsid wsp:val="00E327EA"/>
      <wsp:rsid wsp:val="00E37C86"/>
      <wsp:rsid wsp:val="00E51ECB"/>
      <wsp:rsid wsp:val="00E8565B"/>
      <wsp:rsid wsp:val="00EF46C6"/>
      <wsp:rsid wsp:val="00F55A1F"/>
      <wsp:rsid wsp:val="00F65A07"/>
      <wsp:rsid wsp:val="00F74BF3"/>
      <wsp:rsid wsp:val="00F87A7D"/>
      <wsp:rsid wsp:val="00F96426"/>
      <wsp:rsid wsp:val="00FA171D"/>
      <wsp:rsid wsp:val="00FB0B61"/>
      <wsp:rsid wsp:val="00FC23C6"/>
      <wsp:rsid wsp:val="00FE40A0"/>
    </wsp:rsids>
  </w:docPr>
  <w:body>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈠"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈡"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈢"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈣"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈤"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈥"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈦"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈧"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="007C7E48" wsp:rsidRPr="007C7E48" wsp:rsidRDefault="007C7E48" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈨"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00C02551" wsp:rsidP="00C02551">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:ind w:left="640"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:spacing w:val="-20"/>
          <w:w w:val="80"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>(十一)</w:t>
      </w:r>
      <w:r wsp:rsidR="00D661EB">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:spacing w:val="-20"/>
          <w:w w:val="80"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t> </w:t>
      </w:r>
      <w:r wsp:rsidR="00274EAD">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>职业危害</w:t>
      </w:r>
      <w:r wsp:rsidR="00E2125F">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>应急救援和事故台帐</w:t>
      </w:r>
    </w:p>
    <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00530BE7" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>1</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>.</w:t>
      </w:r>
      <w:r wsp:rsidR="00274EAD">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>职业病危害</w:t>
      </w:r>
      <w:r wsp:rsidR="00226105">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>事故应急救援预案；</w:t>
      </w:r>
    </w:p>
    <w:p wsp:rsidR="004F6D08" wsp:rsidRPr="004F6D08" wsp:rsidRDefault="004F6D08" wsp:rsidP="004F6D08">
      <w:pPr>
        <w:widowControl/>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:cs="宋体"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:spacing w:val="8"/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>请于系统中查看</w:t>
      </w:r>
    </w:p>
    <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="00530BE7" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>2</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>.</w:t>
      </w:r>
      <w:r wsp:rsidR="008C5E16">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>使用高毒物品作业的单位应有事故应急救援预案演练记录；</w:t>
      </w:r>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblW w:w="5000" w:type="pct"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="654"/>
        <w:gridCol w:w="1675"/>
        <w:gridCol w:w="1675"/>
        <w:gridCol w:w="3051"/>
        <w:gridCol w:w="1891"/>
      </w:tblGrid>
      <w:tr wsp:rsidR="00DB2C22" wsp:rsidTr="004B1346">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="366" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="00DB2C22" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>序号</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="936" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="0025018E" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>演练时间</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="936" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="0025018E" wsp:rsidP="0025018E">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>演练描述</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1705" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="0025018E" wsp:rsidRDefault="0025018E" wsp:rsidP="0025018E">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>记录文件</w:t>
            </w:r>
          </w:p>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="0025018E" wsp:rsidP="0025018E">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="000171D3">
              <w:rPr>
                <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
                <wx:font wx:val="黑体"/>
                <w:sz w:val="15"/>
                <w:sz-cs w:val="15"/>
              </w:rPr>
              <w:t>（请于系统中下载查看）</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1057" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="0025018E" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>证明</w:t>
            </w:r>
            <w:r wsp:rsidR="00DB2C22">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>文件</w:t>
            </w:r>
          </w:p>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="00DB2C22" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="000171D3">
              <w:rPr>
                <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
                <wx:font wx:val="黑体"/>
                <w:sz w:val="15"/>
                <w:sz-cs w:val="15"/>
              </w:rPr>
              <w:t>（请于系统中下载查看）</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <#list drillList as drill>
      <w:tr wsp:rsidR="00DB2C22" wsp:rsidTr="004B1346">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="366" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="00DB2C22" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${drill_index+1}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="936" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRPr="00986402" wsp:rsidRDefault="00DB2C22" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
              <w:t>${drill.drillDate}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="936" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="00DB2C22" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${drill.description}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1705" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="00DB2C22" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${drill.content}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1057" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DB2C22" wsp:rsidRDefault="00DB2C22" wsp:rsidP="004B1346">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${drill.proveFile}</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      </#list>
    </w:tbl>
    <w:tbl>
      <w:tblPr>
        <w:tblpPr w:leftFromText="180" w:rightFromText="180" w:vertAnchor="text" w:horzAnchor="margin" w:tblpXSpec="center" w:tblpY="652"/>
        <w:tblW w:w="11249" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLayout w:type="Fixed"/>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="675"/>
        <w:gridCol w:w="1278"/>
        <w:gridCol w:w="1417"/>
        <w:gridCol w:w="1701"/>
        <w:gridCol w:w="1703"/>
        <w:gridCol w:w="2266"/>
        <w:gridCol w:w="2209"/>
      </w:tblGrid>
      <w:tr wsp:rsidR="00DC0FFC" wsp:rsidRPr="000171D3" wsp:rsidTr="008C28CA">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="300" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>序号</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="568" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>事故时间</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="630" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>事故原因</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="756" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>事故处理经过</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="757" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>事故责任分析</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1007" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>事故责任人处理情况</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="982" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRPr="000171D3" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体"/>
                <wx:font wx:val="黑体"/>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>事故防范与整改措施</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <#list accidentHandleList as accidentHandle>
      <w:tr wsp:rsidR="00DC0FFC" wsp:rsidRPr="00DE7CB5" wsp:rsidTr="008C28CA">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="300" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRPr="00166A95" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${accidentHandle_index+1}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="568" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRPr="00166A95" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
              <w:t>${accidentHandle.accidentDate}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="630" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRPr="00166A95" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${accidentHandle.accidentReason}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="756" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRPr="00DE7CB5" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${accidentHandle.handleProcess}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="757" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRPr="00DE7CB5" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${accidentHandle.responsibilityAnalysis}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1007" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRPr="00DE7CB5" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${accidentHandle.dutyPersonHandle}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="982" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00DC0FFC" wsp:rsidRPr="00DE7CB5" wsp:rsidRDefault="00DC0FFC" wsp:rsidP="009B1992">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${accidentHandle.measures}</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      </#list>
    </w:tbl>
    <w:p wsp:rsidR="00F96426" wsp:rsidRPr="00F96426" wsp:rsidRDefault="00530BE7" wsp:rsidP="007C7E48">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="Verdana"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>3.</w:t>
      </w:r>
      <w:r wsp:rsidR="00823A31">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="Verdana" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>事故</w:t>
      </w:r>
      <w:r wsp:rsidR="00823A31">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:cs="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:spacing w:val="8"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>调查处理。</w:t>
      </w:r>
    </w:p>
    <w:p wsp:rsidR="00274EAD" wsp:rsidRPr="00F96426" wsp:rsidRDefault="00274EAD" wsp:rsidP="007C7E48">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="Verdana"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:sectPr wsp:rsidR="00274EAD" wsp:rsidRPr="00F96426" wsp:rsidSect="00A95F4F">
      <w:hdr w:type="even">
        <w:p wsp:rsidR="00751389" wsp:rsidRDefault="00751389">
          <w:pPr>
            <w:pStyle w:val="a6"/>
          </w:pPr>
        </w:p>
      </w:hdr>
      <w:hdr w:type="odd">
        <w:p wsp:rsidR="00751389" wsp:rsidRDefault="00751389">
          <w:pPr>
            <w:pStyle w:val="a6"/>
          </w:pPr>
        </w:p>
      </w:hdr>
      <w:ftr w:type="even">
        <wx:pBdrGroup>
          <wx:apo>
            <wx:jc wx:val="right"/>
          </wx:apo>
          <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD">
            <w:pPr>
              <w:pStyle w:val="a8"/>
              <w:framePr w:h="0" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x-align="right" w:y="1"/>
              <w:rPr>
                <w:rStyle w:val="a3"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:fldChar w:fldCharType="begin"/>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rStyle w:val="a3"/>
              </w:rPr>
              <w:instrText>PAGE  </w:instrText>
            </w:r>
            <w:r>
              <w:fldChar w:fldCharType="separate"/>
            </w:r>
            <w:r>
              <w:fldChar w:fldCharType="end"/>
            </w:r>
          </w:p>
        </wx:pBdrGroup>
        <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD">
          <w:pPr>
            <w:pStyle w:val="a8"/>
            <w:ind w:right="360"/>
          </w:pPr>
        </w:p>
      </w:ftr>
      <w:ftr w:type="odd">
        <wx:pBdrGroup>
          <wx:apo>
            <wx:jc wx:val="center"/>
          </wx:apo>
          <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD">
            <w:pPr>
              <w:pStyle w:val="a8"/>
              <w:framePr w:h="0" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x-align="center" w:y="1"/>
              <w:rPr>
                <w:rStyle w:val="a3"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:fldChar w:fldCharType="begin"/>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rStyle w:val="a3"/>
              </w:rPr>
              <w:instrText>PAGE  </w:instrText>
            </w:r>
            <w:r>
              <w:fldChar w:fldCharType="separate"/>
            </w:r>
            <w:r wsp:rsidR="00A95F4F">
              <w:rPr>
                <w:rStyle w:val="a3"/>
                <w:noProof/>
              </w:rPr>
              <w:t>20</w:t>
            </w:r>
            <w:r>
              <w:fldChar w:fldCharType="end"/>
            </w:r>
          </w:p>
        </wx:pBdrGroup>
        <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD">
          <w:pPr>
            <w:pStyle w:val="a8"/>
            <w:ind w:right="360"/>
          </w:pPr>
        </w:p>
      </w:ftr>
      <w:hdr w:type="first">
        <w:p wsp:rsidR="00751389" wsp:rsidRDefault="00751389">
          <w:pPr>
            <w:pStyle w:val="a6"/>
          </w:pPr>
        </w:p>
      </w:hdr>
      <w:ftr w:type="first">
        <w:p wsp:rsidR="00751389" wsp:rsidRDefault="00751389">
          <w:pPr>
            <w:pStyle w:val="a8"/>
          </w:pPr>
        </w:p>
      </w:ftr>
      <w:pgSz w:w="11906" w:h="16838"/>
      <w:pgMar w:top="1474" w:right="1588" w:bottom="1474" w:left="1588" w:header="851" w:footer="992" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:type="lines" w:line-pitch="312"/>
    </w:sectPr>
  </w:body>
</w:wordDocument>
