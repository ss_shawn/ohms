<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
  <w:ignoreSubtree w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2"/>
  <o:DocumentProperties>
    <o:Author>微软用户</o:Author>
    <o:LastAuthor>wym</o:LastAuthor>
    <o:Revision>2</o:Revision>
    <o:TotalTime>0</o:TotalTime>
    <o:Created>2019-10-14T09:02:00Z</o:Created>
    <o:LastSaved>2019-10-14T09:02:00Z</o:LastSaved>
    <o:Pages>1</o:Pages>
    <o:Words>42</o:Words>
    <o:Characters>242</o:Characters>
    <o:Company>微软中国</o:Company>
    <o:Lines>2</o:Lines>
    <o:Paragraphs>1</o:Paragraphs>
    <o:CharactersWithSpaces>283</o:CharactersWithSpaces>
    <o:Version>12</o:Version>
  </o:DocumentProperties>
  <o:CustomDocumentProperties>
    <o:KSOProductBuildVer dt:dt="string">2052-8.1.0.3526</o:KSOProductBuildVer>
  </o:CustomDocumentProperties>
  <w:fonts>
    <w:defaultFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
    <w:font w:name="Times New Roman">
      <w:panose-1 w:val="02020603050405020304"/>
      <w:charset w:val="00"/>
      <w:family w:val="Roman"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="E0002AFF" w:usb-1="C0007841" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="宋体">
      <w:altName w:val="SimSun"/>
      <w:panose-1 w:val="02010600030101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="auto"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="黑体">
      <w:altName w:val="SimHei"/>
      <w:panose-1 w:val="02010609060101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Cambria Math">
      <w:panose-1 w:val="02040503050406030204"/>
      <w:charset w:val="00"/>
      <w:family w:val="Roman"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="E00002FF" w:usb-1="420024FF" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="仿宋_GB2312">
      <w:altName w:val="黑体"/>
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Verdana">
      <w:panose-1 w:val="020B0604030504040204"/>
      <w:charset w:val="00"/>
      <w:family w:val="Swiss"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="A10006FF" w:usb-1="4000205B" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="楷体_GB2312">
      <w:altName w:val="Microsoft YaHei"/>
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="@宋体">
      <w:panose-1 w:val="02010600030101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="auto"/>
      <w:pitch w:val="variable"/>
      <w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="@黑体">
      <w:panose-1 w:val="02010609060101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="@仿宋_GB2312">
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="@楷体_GB2312">
      <w:charset w:val="86"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="fixed"/>
      <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
    </w:font>
  </w:fonts>
  <w:lists>
    <w:listDef w:listDefId="0">
      <w:lsid w:val="2746691A"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="7EA04EFC"/>
      <w:lvl w:ilvl="0" w:tplc="2228B75A">
        <w:start w:val="1"/>
        <w:nfc w:val="26"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="1">
      <w:lsid w:val="2EA706A3"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="7CE60A16"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="2">
      <w:lsid w:val="428077C1"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="576C44D2"/>
      <w:lvl w:ilvl="0" w:tplc="AB7C271A">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="3">
      <w:lsid w:val="4370654A"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="9F389E5A"/>
      <w:lvl w:ilvl="0" w:tplc="33A22494">
        <w:start w:val="1"/>
        <w:nfc w:val="26"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1052" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1772" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2492" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3212" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3932" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4652" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5372" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6092" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6812" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="4">
      <w:lsid w:val="47635688"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="7810830C"/>
      <w:lvl w:ilvl="0" w:tplc="AB2675B0">
        <w:start w:val="1"/>
        <w:nfc w:val="26"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="5">
      <w:lsid w:val="625F2313"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="1E40F4F8"/>
      <w:lvl w:ilvl="0" w:tplc="E8CA1F94">
        <w:start w:val="1"/>
        <w:nfc w:val="29"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
          <w:b/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="6">
      <w:lsid w:val="70E67E38"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="A26216EE"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:h-ansi="Times New Roman" w:cs="Times New Roman" w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="7">
      <w:lsid w:val="72865796"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="6FBCE39A"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="8">
      <w:lsid w:val="74AB1CDB"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="7810830C"/>
      <w:lvl w:ilvl="0" w:tplc="AB2675B0">
        <w:start w:val="1"/>
        <w:nfc w:val="26"/>
        <w:lvlText w:val="%1"/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1000" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1720" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2440" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3160" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4600" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5320" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="6040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6760" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="9">
      <w:lsid w:val="7A8E6FE4"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="148A6DBC"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:listDef w:listDefId="10">
      <w:lsid w:val="7DC42279"/>
      <w:plt w:val="HybridMultilevel"/>
      <w:tmpl w:val="DB828ED4"/>
      <w:lvl w:ilvl="0" w:tplc="0809000F">
        <w:start w:val="1"/>
        <w:lvlText w:val="%1."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="720" w:hanging="360"/>
        </w:pPr>
        <w:rPr>
          <w:rFonts w:hint="default"/>
        </w:rPr>
      </w:lvl>
      <w:lvl w:ilvl="1" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%2."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="1440" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="2" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%3."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="2160" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="3" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%4."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="2880" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="4" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%5."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="3600" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="5" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%6."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="4320" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="6" w:tplc="0809000F" w:tentative="on">
        <w:start w:val="1"/>
        <w:lvlText w:val="%7."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5040" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="7" w:tplc="08090019" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="4"/>
        <w:lvlText w:val="%8."/>
        <w:lvlJc w:val="left"/>
        <w:pPr>
          <w:ind w:left="5760" w:hanging="360"/>
        </w:pPr>
      </w:lvl>
      <w:lvl w:ilvl="8" w:tplc="0809001B" w:tentative="on">
        <w:start w:val="1"/>
        <w:nfc w:val="2"/>
        <w:lvlText w:val="%9."/>
        <w:lvlJc w:val="right"/>
        <w:pPr>
          <w:ind w:left="6480" w:hanging="180"/>
        </w:pPr>
      </w:lvl>
    </w:listDef>
    <w:list w:ilfo="1">
      <w:ilst w:val="8"/>
    </w:list>
    <w:list w:ilfo="2">
      <w:ilst w:val="4"/>
    </w:list>
    <w:list w:ilfo="3">
      <w:ilst w:val="5"/>
    </w:list>
    <w:list w:ilfo="4">
      <w:ilst w:val="10"/>
    </w:list>
    <w:list w:ilfo="5">
      <w:ilst w:val="0"/>
    </w:list>
    <w:list w:ilfo="6">
      <w:ilst w:val="1"/>
    </w:list>
    <w:list w:ilfo="7">
      <w:ilst w:val="3"/>
    </w:list>
    <w:list w:ilfo="8">
      <w:ilst w:val="9"/>
    </w:list>
    <w:list w:ilfo="9">
      <w:ilst w:val="7"/>
    </w:list>
    <w:list w:ilfo="10">
      <w:ilst w:val="2"/>
    </w:list>
    <w:list w:ilfo="11">
      <w:ilst w:val="6"/>
    </w:list>
  </w:lists>
  <w:styles>
    <w:versionOfBuiltInStylenames w:val="7"/>
    <w:latentStyles w:defLockedState="off" w:latentStyleCount="267">
      <w:lsdException w:name="Normal"/>
      <w:lsdException w:name="heading 1"/>
      <w:lsdException w:name="heading 2"/>
      <w:lsdException w:name="heading 3"/>
      <w:lsdException w:name="heading 4"/>
      <w:lsdException w:name="heading 5"/>
      <w:lsdException w:name="heading 6"/>
      <w:lsdException w:name="heading 7"/>
      <w:lsdException w:name="heading 8"/>
      <w:lsdException w:name="heading 9"/>
      <w:lsdException w:name="toc 1"/>
      <w:lsdException w:name="toc 2"/>
      <w:lsdException w:name="toc 3"/>
      <w:lsdException w:name="toc 4"/>
      <w:lsdException w:name="toc 5"/>
      <w:lsdException w:name="toc 6"/>
      <w:lsdException w:name="toc 7"/>
      <w:lsdException w:name="toc 8"/>
      <w:lsdException w:name="toc 9"/>
      <w:lsdException w:name="caption"/>
      <w:lsdException w:name="Title"/>
      <w:lsdException w:name="Default Paragraph Font"/>
      <w:lsdException w:name="Subtitle"/>
      <w:lsdException w:name="Strong"/>
      <w:lsdException w:name="Emphasis"/>
      <w:lsdException w:name="Normal (Web)"/>
      <w:lsdException w:name="Table Grid"/>
      <w:lsdException w:name="Placeholder Text"/>
      <w:lsdException w:name="No Spacing"/>
      <w:lsdException w:name="Light Shading"/>
      <w:lsdException w:name="Light List"/>
      <w:lsdException w:name="Light Grid"/>
      <w:lsdException w:name="Medium Shading 1"/>
      <w:lsdException w:name="Medium Shading 2"/>
      <w:lsdException w:name="Medium List 1"/>
      <w:lsdException w:name="Medium List 2"/>
      <w:lsdException w:name="Medium Grid 1"/>
      <w:lsdException w:name="Medium Grid 2"/>
      <w:lsdException w:name="Medium Grid 3"/>
      <w:lsdException w:name="Dark List"/>
      <w:lsdException w:name="Colorful Shading"/>
      <w:lsdException w:name="Colorful List"/>
      <w:lsdException w:name="Colorful Grid"/>
      <w:lsdException w:name="Light Shading Accent 1"/>
      <w:lsdException w:name="Light List Accent 1"/>
      <w:lsdException w:name="Light Grid Accent 1"/>
      <w:lsdException w:name="Medium Shading 1 Accent 1"/>
      <w:lsdException w:name="Medium Shading 2 Accent 1"/>
      <w:lsdException w:name="Medium List 1 Accent 1"/>
      <w:lsdException w:name="Revision"/>
      <w:lsdException w:name="List Paragraph"/>
      <w:lsdException w:name="Quote"/>
      <w:lsdException w:name="Intense Quote"/>
      <w:lsdException w:name="Medium List 2 Accent 1"/>
      <w:lsdException w:name="Medium Grid 1 Accent 1"/>
      <w:lsdException w:name="Medium Grid 2 Accent 1"/>
      <w:lsdException w:name="Medium Grid 3 Accent 1"/>
      <w:lsdException w:name="Dark List Accent 1"/>
      <w:lsdException w:name="Colorful Shading Accent 1"/>
      <w:lsdException w:name="Colorful List Accent 1"/>
      <w:lsdException w:name="Colorful Grid Accent 1"/>
      <w:lsdException w:name="Light Shading Accent 2"/>
      <w:lsdException w:name="Light List Accent 2"/>
      <w:lsdException w:name="Light Grid Accent 2"/>
      <w:lsdException w:name="Medium Shading 1 Accent 2"/>
      <w:lsdException w:name="Medium Shading 2 Accent 2"/>
      <w:lsdException w:name="Medium List 1 Accent 2"/>
      <w:lsdException w:name="Medium List 2 Accent 2"/>
      <w:lsdException w:name="Medium Grid 1 Accent 2"/>
      <w:lsdException w:name="Medium Grid 2 Accent 2"/>
      <w:lsdException w:name="Medium Grid 3 Accent 2"/>
      <w:lsdException w:name="Dark List Accent 2"/>
      <w:lsdException w:name="Colorful Shading Accent 2"/>
      <w:lsdException w:name="Colorful List Accent 2"/>
      <w:lsdException w:name="Colorful Grid Accent 2"/>
      <w:lsdException w:name="Light Shading Accent 3"/>
      <w:lsdException w:name="Light List Accent 3"/>
      <w:lsdException w:name="Light Grid Accent 3"/>
      <w:lsdException w:name="Medium Shading 1 Accent 3"/>
      <w:lsdException w:name="Medium Shading 2 Accent 3"/>
      <w:lsdException w:name="Medium List 1 Accent 3"/>
      <w:lsdException w:name="Medium List 2 Accent 3"/>
      <w:lsdException w:name="Medium Grid 1 Accent 3"/>
      <w:lsdException w:name="Medium Grid 2 Accent 3"/>
      <w:lsdException w:name="Medium Grid 3 Accent 3"/>
      <w:lsdException w:name="Dark List Accent 3"/>
      <w:lsdException w:name="Colorful Shading Accent 3"/>
      <w:lsdException w:name="Colorful List Accent 3"/>
      <w:lsdException w:name="Colorful Grid Accent 3"/>
      <w:lsdException w:name="Light Shading Accent 4"/>
      <w:lsdException w:name="Light List Accent 4"/>
      <w:lsdException w:name="Light Grid Accent 4"/>
      <w:lsdException w:name="Medium Shading 1 Accent 4"/>
      <w:lsdException w:name="Medium Shading 2 Accent 4"/>
      <w:lsdException w:name="Medium List 1 Accent 4"/>
      <w:lsdException w:name="Medium List 2 Accent 4"/>
      <w:lsdException w:name="Medium Grid 1 Accent 4"/>
      <w:lsdException w:name="Medium Grid 2 Accent 4"/>
      <w:lsdException w:name="Medium Grid 3 Accent 4"/>
      <w:lsdException w:name="Dark List Accent 4"/>
      <w:lsdException w:name="Colorful Shading Accent 4"/>
      <w:lsdException w:name="Colorful List Accent 4"/>
      <w:lsdException w:name="Colorful Grid Accent 4"/>
      <w:lsdException w:name="Light Shading Accent 5"/>
      <w:lsdException w:name="Light List Accent 5"/>
      <w:lsdException w:name="Light Grid Accent 5"/>
      <w:lsdException w:name="Medium Shading 1 Accent 5"/>
      <w:lsdException w:name="Medium Shading 2 Accent 5"/>
      <w:lsdException w:name="Medium List 1 Accent 5"/>
      <w:lsdException w:name="Medium List 2 Accent 5"/>
      <w:lsdException w:name="Medium Grid 1 Accent 5"/>
      <w:lsdException w:name="Medium Grid 2 Accent 5"/>
      <w:lsdException w:name="Medium Grid 3 Accent 5"/>
      <w:lsdException w:name="Dark List Accent 5"/>
      <w:lsdException w:name="Colorful Shading Accent 5"/>
      <w:lsdException w:name="Colorful List Accent 5"/>
      <w:lsdException w:name="Colorful Grid Accent 5"/>
      <w:lsdException w:name="Light Shading Accent 6"/>
      <w:lsdException w:name="Light List Accent 6"/>
      <w:lsdException w:name="Light Grid Accent 6"/>
      <w:lsdException w:name="Medium Shading 1 Accent 6"/>
      <w:lsdException w:name="Medium Shading 2 Accent 6"/>
      <w:lsdException w:name="Medium List 1 Accent 6"/>
      <w:lsdException w:name="Medium List 2 Accent 6"/>
      <w:lsdException w:name="Medium Grid 1 Accent 6"/>
      <w:lsdException w:name="Medium Grid 2 Accent 6"/>
      <w:lsdException w:name="Medium Grid 3 Accent 6"/>
      <w:lsdException w:name="Dark List Accent 6"/>
      <w:lsdException w:name="Colorful Shading Accent 6"/>
      <w:lsdException w:name="Colorful List Accent 6"/>
      <w:lsdException w:name="Colorful Grid Accent 6"/>
      <w:lsdException w:name="Subtle Emphasis"/>
      <w:lsdException w:name="Intense Emphasis"/>
      <w:lsdException w:name="Subtle Reference"/>
      <w:lsdException w:name="Intense Reference"/>
      <w:lsdException w:name="Book Title"/>
      <w:lsdException w:name="Bibliography"/>
      <w:lsdException w:name="TOC Heading"/>
    </w:latentStyles>
    <w:style w:type="paragraph" w:default="on" w:styleId="a">
      <w:name w:val="Normal"/>
      <wx:uiName wx:val="正文"/>
      <w:pPr>
        <w:widowControl w:val="off"/>
        <w:jc w:val="both"/>
      </w:pPr>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
        <w:kern w:val="2"/>
        <w:sz w:val="21"/>
        <w:sz-cs w:val="24"/>
        <w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
      </w:rPr>
    </w:style>
    <w:style w:type="character" w:default="on" w:styleId="a0">
      <w:name w:val="Default Paragraph Font"/>
      <wx:uiName wx:val="默认段落字体"/>
    </w:style>
    <w:style w:type="table" w:default="on" w:styleId="a1">
      <w:name w:val="Normal Table"/>
      <wx:uiName wx:val="普通表格"/>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
        <w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
      </w:rPr>
      <w:tblPr>
        <w:tblInd w:w="0" w:type="dxa"/>
        <w:tblCellMar>
          <w:top w:w="0" w:type="dxa"/>
          <w:left w:w="108" w:type="dxa"/>
          <w:bottom w:w="0" w:type="dxa"/>
          <w:right w:w="108" w:type="dxa"/>
        </w:tblCellMar>
      </w:tblPr>
    </w:style>
    <w:style w:type="list" w:default="on" w:styleId="a2">
      <w:name w:val="No List"/>
      <wx:uiName wx:val="无列表"/>
    </w:style>
    <w:style w:type="character" w:styleId="a3">
      <w:name w:val="page number"/>
      <wx:uiName wx:val="页码"/>
      <w:basedOn w:val="a0"/>
    </w:style>
    <w:style w:type="character" w:styleId="a4">
      <w:name w:val="Strong"/>
      <wx:uiName wx:val="要点"/>
      <w:rPr>
        <w:b/>
        <w:b-cs/>
      </w:rPr>
    </w:style>
    <w:style w:type="character" w:styleId="msochangeprop0">
      <w:name w:val="msochangeprop0"/>
      <w:basedOn w:val="a0"/>
    </w:style>
    <w:style w:type="paragraph" w:styleId="a5">
      <w:name w:val="Body Text Indent"/>
      <wx:uiName wx:val="正文文本缩进"/>
      <w:basedOn w:val="a"/>
      <w:pPr>
        <w:ind w:first-line-chars="200" w:first-line="643"/>
      </w:pPr>
      <w:rPr>
        <w:rFonts w:fareast="楷体_GB2312"/>
        <wx:font wx:val="Times New Roman"/>
        <w:b/>
        <w:sz w:val="32"/>
      </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="a6">
      <w:name w:val="header"/>
      <wx:uiName wx:val="页眉"/>
      <w:basedOn w:val="a"/>
      <w:rsid w:val="00342736"/>
      <w:pPr>
        <w:tabs>
          <w:tab w:val="center" w:pos="4153"/>
          <w:tab w:val="right" w:pos="8306"/>
        </w:tabs>
        <w:snapToGrid w:val="off"/>
        <w:jc w:val="center"/>
      </w:pPr>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
        <w:sz w:val="18"/>
        <w:sz-cs w:val="18"/>
      </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="a7">
      <w:name w:val="Normal (Web)"/>
      <wx:uiName wx:val="普通(网站)"/>
      <w:basedOn w:val="a"/>
      <w:pPr>
        <w:widowControl/>
        <w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
        <w:jc w:val="left"/>
      </w:pPr>
      <w:rPr>
        <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
        <wx:font wx:val="宋体"/>
        <w:kern w:val="0"/>
        <w:sz w:val="24"/>
      </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="a8">
      <w:name w:val="footer"/>
      <wx:uiName wx:val="页脚"/>
      <w:basedOn w:val="a"/>
      <w:pPr>
        <w:tabs>
          <w:tab w:val="center" w:pos="4153"/>
          <w:tab w:val="right" w:pos="8306"/>
        </w:tabs>
        <w:snapToGrid w:val="off"/>
        <w:jc w:val="left"/>
      </w:pPr>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
        <w:sz w:val="18"/>
        <w:sz-cs w:val="18"/>
      </w:rPr>
    </w:style>
    <w:style w:type="table" w:styleId="a9">
      <w:name w:val="Table Grid"/>
      <wx:uiName wx:val="网格型"/>
      <w:basedOn w:val="a1"/>
      <w:rsid w:val="0076062C"/>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
      </w:rPr>
      <w:tblPr>
        <w:tblInd w:w="0" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblCellMar>
          <w:top w:w="0" w:type="dxa"/>
          <w:left w:w="108" w:type="dxa"/>
          <w:bottom w:w="0" w:type="dxa"/>
          <w:right w:w="108" w:type="dxa"/>
        </w:tblCellMar>
      </w:tblPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="aa">
      <w:name w:val="List Paragraph"/>
      <wx:uiName wx:val="列出段落"/>
      <w:basedOn w:val="a"/>
      <w:rsid w:val="003F265C"/>
      <w:pPr>
        <w:ind w:first-line-chars="200" w:first-line="420"/>
      </w:pPr>
      <w:rPr>
        <wx:font wx:val="Times New Roman"/>
      </w:rPr>
    </w:style>
  </w:styles>
  <w:shapeDefaults>
    <o:shapedefaults v:ext="edit" spidmax="3074" fillcolor="#9cbee0" strokecolor="#739cc3">
      <v:fill color="#9cbee0" color2="#bbd5f0" type="gradient">
        <o:fill v:ext="view" type="gradientUnscaled"/>
      </v:fill>
      <v:stroke color="#739cc3" weight="1.25pt"/>
    </o:shapedefaults>
    <o:shapelayout v:ext="edit">
      <o:idmap v:ext="edit" data="1"/>
    </o:shapelayout>
  </w:shapeDefaults>
  <w:docPr>
    <w:view w:val="print"/>
    <w:zoom w:percent="100"/>
    <w:doNotEmbedSystemFonts/>
    <w:bordersDontSurroundHeader/>
    <w:bordersDontSurroundFooter/>
    <w:attachedTemplate w:val="Normal"/>
    <w:stylePaneFormatFilter w:val="3F01"/>
    <w:defaultTabStop w:val="420"/>
    <w:drawingGridVerticalSpacing w:val="156"/>
    <w:displayHorizontalDrawingGridEvery w:val="0"/>
    <w:displayVerticalDrawingGridEvery w:val="2"/>
    <w:punctuationKerning/>
    <w:characterSpacingControl w:val="CompressPunctuation"/>
    <w:webPageEncoding w:val="x-cp20936"/>
    <w:optimizeForBrowser/>
    <w:allowPNG/>
    <w:targetScreenSz w:val="1024x768"/>
    <w:validateAgainstSchema w:val="off"/>
    <w:saveInvalidXML w:val="off"/>
    <w:ignoreMixedContent w:val="off"/>
    <w:alwaysShowPlaceholderText w:val="off"/>
    <w:doNotUnderlineInvalidXML/>
    <w:hdrShapeDefaults>
      <o:shapedefaults v:ext="edit" spidmax="3074" fillcolor="#9cbee0" strokecolor="#739cc3">
        <v:fill color="#9cbee0" color2="#bbd5f0" type="gradient">
          <o:fill v:ext="view" type="gradientUnscaled"/>
        </v:fill>
        <v:stroke color="#739cc3" weight="1.25pt"/>
      </o:shapedefaults>
    </w:hdrShapeDefaults>
    <w:footnotePr>
      <w:footnote w:type="separator">
        <w:p wsp:rsidR="002C4C74" wsp:rsidRDefault="002C4C74">
          <w:r>
            <w:separator/>
          </w:r>
        </w:p>
      </w:footnote>
      <w:footnote w:type="continuation-separator">
        <w:p wsp:rsidR="002C4C74" wsp:rsidRDefault="002C4C74">
          <w:r>
            <w:continuationSeparator/>
          </w:r>
        </w:p>
      </w:footnote>
    </w:footnotePr>
    <w:endnotePr>
      <w:endnote w:type="separator">
        <w:p wsp:rsidR="002C4C74" wsp:rsidRDefault="002C4C74">
          <w:r>
            <w:separator/>
          </w:r>
        </w:p>
      </w:endnote>
      <w:endnote w:type="continuation-separator">
        <w:p wsp:rsidR="002C4C74" wsp:rsidRDefault="002C4C74">
          <w:r>
            <w:continuationSeparator/>
          </w:r>
        </w:p>
      </w:endnote>
    </w:endnotePr>
    <w:compat>
      <w:spaceForUL/>
      <w:balanceSingleByteDoubleByteWidth/>
      <w:doNotLeaveBackslashAlone/>
      <w:ulTrailSpace/>
      <w:doNotExpandShiftReturn/>
      <w:adjustLineHeightInTable/>
      <w:breakWrappedTables/>
      <w:snapToGridInCell/>
      <w:wrapTextWithPunct/>
      <w:useAsianBreakRules/>
      <w:dontGrowAutofit/>
      <w:useFELayout/>
    </w:compat>
    <wsp:rsids>
      <wsp:rsidRoot wsp:val="00172A27"/>
      <wsp:rsid wsp:val="000034C7"/>
      <wsp:rsid wsp:val="00007A83"/>
      <wsp:rsid wsp:val="00053FB0"/>
      <wsp:rsid wsp:val="000628F4"/>
      <wsp:rsid wsp:val="00065B49"/>
      <wsp:rsid wsp:val="00067C75"/>
      <wsp:rsid wsp:val="00141825"/>
      <wsp:rsid wsp:val="00166D53"/>
      <wsp:rsid wsp:val="001A36A8"/>
      <wsp:rsid wsp:val="001B4260"/>
      <wsp:rsid wsp:val="002405F8"/>
      <wsp:rsid wsp:val="002478D8"/>
      <wsp:rsid wsp:val="00274EAD"/>
      <wsp:rsid wsp:val="002C4C74"/>
      <wsp:rsid wsp:val="002E3EE0"/>
      <wsp:rsid wsp:val="002E50FC"/>
      <wsp:rsid wsp:val="0032407B"/>
      <wsp:rsid wsp:val="00342736"/>
      <wsp:rsid wsp:val="003F265C"/>
      <wsp:rsid wsp:val="004C2F12"/>
      <wsp:rsid wsp:val="00503C55"/>
      <wsp:rsid wsp:val="00530BE7"/>
      <wsp:rsid wsp:val="005B5625"/>
      <wsp:rsid wsp:val="00637774"/>
      <wsp:rsid wsp:val="00667B09"/>
      <wsp:rsid wsp:val="006746F2"/>
      <wsp:rsid wsp:val="006B3039"/>
      <wsp:rsid wsp:val="007221BC"/>
      <wsp:rsid wsp:val="00722AD0"/>
      <wsp:rsid wsp:val="00751389"/>
      <wsp:rsid wsp:val="0076062C"/>
      <wsp:rsid wsp:val="007D6C2B"/>
      <wsp:rsid wsp:val="008143DF"/>
      <wsp:rsid wsp:val="00845656"/>
      <wsp:rsid wsp:val="00866861"/>
      <wsp:rsid wsp:val="00892A6B"/>
      <wsp:rsid wsp:val="008A5B1A"/>
      <wsp:rsid wsp:val="008C1674"/>
      <wsp:rsid wsp:val="009B0FC8"/>
      <wsp:rsid wsp:val="009F2018"/>
      <wsp:rsid wsp:val="009F4552"/>
      <wsp:rsid wsp:val="00A351C9"/>
      <wsp:rsid wsp:val="00A64168"/>
      <wsp:rsid wsp:val="00A95F4F"/>
      <wsp:rsid wsp:val="00BB6EFE"/>
      <wsp:rsid wsp:val="00BE085B"/>
      <wsp:rsid wsp:val="00C0023D"/>
      <wsp:rsid wsp:val="00D21066"/>
      <wsp:rsid wsp:val="00D447E1"/>
      <wsp:rsid wsp:val="00E327EA"/>
      <wsp:rsid wsp:val="00E37C86"/>
      <wsp:rsid wsp:val="00E51ECB"/>
      <wsp:rsid wsp:val="00E8565B"/>
      <wsp:rsid wsp:val="00EF46C6"/>
      <wsp:rsid wsp:val="00F65A07"/>
      <wsp:rsid wsp:val="00F87A7D"/>
      <wsp:rsid wsp:val="00FA171D"/>
      <wsp:rsid wsp:val="00FC23C6"/>
      <wsp:rsid wsp:val="00FE40A0"/>
    </wsp:rsids>
  </w:docPr>
  <w:body>
    <w:p wsp:rsidR="003F265C" wsp:rsidRPr="003F265C" wsp:rsidRDefault="003F265C" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈠"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="003F265C" wsp:rsidRPr="003F265C" wsp:rsidRDefault="003F265C" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈡"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="003F265C" wsp:rsidRPr="003F265C" wsp:rsidRDefault="003F265C" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈢"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="003F265C" wsp:rsidRPr="003F265C" wsp:rsidRDefault="003F265C" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈣"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="003F265C" wsp:rsidRPr="003F265C" wsp:rsidRDefault="003F265C" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈤"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="003F265C" wsp:rsidRPr="003F265C" wsp:rsidRDefault="003F265C" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="aa"/>
        <w:widowControl/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈥"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:ind w:first-line-chars="0"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:vanish/>
          <w:kern w:val="0"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:listPr>
          <w:ilvl w:val="0"/>
          <w:ilfo w:val="3"/>
          <wx:t wx:val="㈦"/>
          <wx:font wx:val="Times New Roman"/>
        </w:listPr>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:b/>
          <w:b-cs/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>职业危害日常监测、检测台帐</w:t>
      </w:r>
    </w:p>
    <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00530BE7" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:spacing w:line="600" w:line-rule="exact"/>
        <w:jc w:val="left"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>1</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>.</w:t>
      </w:r>
      <w:r wsp:rsidR="00274EAD">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>职业病危害因素监测、检测点分布示意图；</w:t>
      </w:r>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="6771"/>
        <w:gridCol w:w="2175"/>
      </w:tblGrid>
      <w:tr wsp:rsidR="00530BE7" wsp:rsidTr="007221BC">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="6771" w:type="dxa"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRPr="00F87A7D" wsp:rsidRDefault="00F87A7D" wsp:rsidP="00F87A7D">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00F87A7D">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>描述</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2175" w:type="dxa"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00F87A7D" wsp:rsidRPr="00BE085B" wsp:rsidRDefault="00F87A7D" wsp:rsidP="00F87A7D">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00BE085B">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>示意图</w:t>
            </w:r>
          </w:p>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00F87A7D" wsp:rsidP="00F87A7D">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r wsp:rsidRPr="00BE085B">
              <w:rPr>
                <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
                <wx:font wx:val="黑体"/>
                <w:sz w:val="15"/>
                <w:sz-cs w:val="15"/>
              </w:rPr>
              <w:t>（请于系统中下载查看）</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <#list schematicDiagramList as schematicDiagram>
      <w:tr wsp:rsidR="007221BC" wsp:rsidTr="007221BC">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="6771" w:type="dxa"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="007221BC" wsp:rsidRPr="000034C7" wsp:rsidRDefault="007221BC" wsp:rsidP="00F87A7D">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${schematicDiagram.description}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2175" w:type="dxa"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="007221BC" wsp:rsidRPr="000034C7" wsp:rsidRDefault="007221BC" wsp:rsidP="00F87A7D">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${schematicDiagram.filePath}</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      </#list>
    </w:tbl>
    <w:p wsp:rsidR="00D447E1" wsp:rsidRDefault="00530BE7" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>2</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>.</w:t>
      </w:r>
      <w:r wsp:rsidR="00274EAD">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>工作场所职业病危害因素日常监测记录（用人单位自身监测）</w:t>
      </w:r>
      <w:r wsp:rsidR="00D447E1">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>；</w:t>
      </w:r>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblW w:w="9297" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLayout w:type="Fixed"/>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="1101"/>
        <w:gridCol w:w="1393"/>
        <w:gridCol w:w="1574"/>
        <w:gridCol w:w="1243"/>
        <w:gridCol w:w="1179"/>
        <w:gridCol w:w="1478"/>
        <w:gridCol w:w="1329"/>
      </w:tblGrid>
      <w:tr wsp:rsidR="00530BE7" wsp:rsidTr="000034C7">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1101" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRPr="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00530BE7">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>工作场所</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1393" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRPr="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00530BE7">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>监测点名称</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1574" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRPr="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00530BE7">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>职业危害因素</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1243" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRPr="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00530BE7">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>接触限值</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1179" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRPr="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00530BE7">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>监测周期</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1478" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRPr="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00530BE7">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>监测结果</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1329" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRPr="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00530BE7">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>监测日期</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <#list insideMonitorList as insideMonitor>
      <w:tr wsp:rsidR="00530BE7" wsp:rsidTr="000034C7">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1101" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${insideMonitor.workplaceName}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1393" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${insideMonitor.monitorPointName!''}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1574" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${insideMonitor.harmFactorName}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1243" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
              <w:t>${insideMonitor.limitValue}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1179" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
              <w:t>${insideMonitor.monitorPeriod}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1478" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${insideMonitor.monitorResult}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1329" w:type="dxa"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
              <w:t>${insideMonitor.monitorDate}</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      </#list>
    </w:tbl>
    <w:p wsp:rsidR="00D447E1" wsp:rsidRDefault="00530BE7" wsp:rsidP="00D447E1">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="Verdana"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>3.</w:t>
      </w:r>
      <w:r wsp:rsidR="00274EAD">
        <w:rPr>
          <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:h-ansi="Verdana" w:hint="fareast"/>
          <wx:font wx:val="仿宋_GB2312"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>委托有职业危害检测资质单位进行的职业病危害因素检测委托书和检测报告。</w:t>
      </w:r>
    </w:p>
    <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
          <wx:font wx:val="Times New Roman"/>
          <w:kern w:val="2"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r wsp:rsidRPr="00530BE7">
        <w:rPr>
          <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
          <w:kern w:val="2"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>检测</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
          <w:kern w:val="2"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>服务</w:t>
      </w:r>
      <w:r wsp:rsidRPr="00530BE7">
        <w:rPr>
          <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
          <w:kern w:val="2"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>机构信息表</w:t>
      </w:r>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblW w:w="5000" w:type="pct"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="654"/>
        <w:gridCol w:w="1675"/>
        <w:gridCol w:w="1675"/>
        <w:gridCol w:w="3051"/>
        <w:gridCol w:w="1891"/>
      </w:tblGrid>
      <w:tr wsp:rsidR="00530BE7" wsp:rsidTr="009F2018">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="366" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>序号</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="936" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>机构名称</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="936" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>机构联系人</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1705" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>机构联系电话</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1057" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>合同文件</w:t>
            </w:r>
          </w:p>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="000171D3">
              <w:rPr>
                <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
                <wx:font wx:val="黑体"/>
                <w:sz w:val="15"/>
                <w:sz-cs w:val="15"/>
              </w:rPr>
              <w:t>（请于系统中下载查看）</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <#list checkInstitutionList as checkInstitution>
      <w:tr wsp:rsidR="00530BE7" wsp:rsidTr="009F2018">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="366" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${checkInstitution_index+1}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="936" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${checkInstitution.name}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="936" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${checkInstitution.contacts}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1705" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${checkInstitution.mobilePhone}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1057" w:type="pct"/>
            <w:tcBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tcBorders>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00E327EA">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${checkInstitution.contract}</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      </#list>
    </w:tbl>
    <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00667B09">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:rPr>
          <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
          <wx:font wx:val="Times New Roman"/>
          <w:kern w:val="2"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p wsp:rsidR="00530BE7" wsp:rsidRDefault="00530BE7" wsp:rsidP="00530BE7">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
          <wx:font wx:val="Times New Roman"/>
          <w:kern w:val="2"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
          <w:kern w:val="2"/>
          <w:sz w:val="32"/>
          <w:sz-cs w:val="32"/>
        </w:rPr>
        <w:t>检测记录表</w:t>
      </w:r>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblW w:w="5000" w:type="pct"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="1860"/>
        <w:gridCol w:w="1791"/>
        <w:gridCol w:w="3119"/>
        <w:gridCol w:w="2176"/>
      </w:tblGrid>
      <w:tr wsp:rsidR="00E327EA" wsp:rsidTr="00FE40A0">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1040" w:type="pct"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00E327EA" wsp:rsidRDefault="00E327EA" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r wsp:rsidRPr="00BE085B">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>检测日期</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1001" w:type="pct"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00E327EA" wsp:rsidRDefault="00E327EA" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r wsp:rsidRPr="00BE085B">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>检测机构</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1743" w:type="pct"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00E327EA" wsp:rsidRPr="00BE085B" wsp:rsidRDefault="00E327EA" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00BE085B">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>检测危害因素</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1216" w:type="pct"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00E327EA" wsp:rsidRPr="00BE085B" wsp:rsidRDefault="00E327EA" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:b/>
              </w:rPr>
            </w:pPr>
            <w:r wsp:rsidRPr="00BE085B">
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
                <w:b/>
              </w:rPr>
              <w:t>检测报告书</w:t>
            </w:r>
          </w:p>
          <w:p wsp:rsidR="00E327EA" wsp:rsidRDefault="00E327EA" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
              </w:rPr>
              <w:t>（</w:t>
            </w:r>
            <w:r wsp:rsidRPr="00BE085B">
              <w:rPr>
                <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
                <wx:font wx:val="黑体"/>
                <w:sz w:val="15"/>
                <w:sz-cs w:val="15"/>
              </w:rPr>
              <w:t>请于系统中下载查看</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
                <wx:font wx:val="宋体"/>
              </w:rPr>
              <w:t>）</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <#list checkRecordList as checkRecord>
      <w:tr wsp:rsidR="00FE40A0" wsp:rsidTr="00FE40A0">
        <w:trPr>
          <w:trHeight w:val="567"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1040" w:type="pct"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00FE40A0" wsp:rsidRPr="00FE40A0" wsp:rsidRDefault="00FE40A0" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
              <w:t>${checkRecord.checkDate}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1001" w:type="pct"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00FE40A0" wsp:rsidRPr="00FE40A0" wsp:rsidRDefault="00FE40A0" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${checkRecord.checkOrganizationName}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1743" w:type="pct"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00FE40A0" wsp:rsidRPr="00FE40A0" wsp:rsidRDefault="00FE40A0" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${checkRecord.checkHarmFactorName}</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1216" w:type="pct"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p wsp:rsidR="00FE40A0" wsp:rsidRPr="00FE40A0" wsp:rsidRDefault="00FE40A0" wsp:rsidP="00BE085B">
            <w:pPr>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:hint="fareast"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                <wx:font wx:val="宋体"/></w:rPr>
              <w:t>${checkRecord.checkReport}</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      </#list>
    </w:tbl>
    <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD" wsp:rsidP="00D935F3">
      <w:pPr>
        <w:pStyle w:val="a7"/>
        <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600" w:line-rule="exact"/>
      </w:pPr>
    </w:p>
    <w:sectPr wsp:rsidR="00274EAD" wsp:rsidSect="00D935F3">
      <w:hdr w:type="even">
        <w:p wsp:rsidR="00CB4811" wsp:rsidRDefault="00CB4811">
          <w:pPr>
            <w:pStyle w:val="a6"/>
          </w:pPr>
        </w:p>
      </w:hdr>
      <w:hdr w:type="odd">
        <w:p wsp:rsidR="00CB4811" wsp:rsidRDefault="00CB4811">
          <w:pPr>
            <w:pStyle w:val="a6"/>
          </w:pPr>
        </w:p>
      </w:hdr>
      <w:ftr w:type="even">
        <wx:pBdrGroup>
          <wx:apo>
            <wx:jc wx:val="right"/>
          </wx:apo>
          <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD">
            <w:pPr>
              <w:pStyle w:val="a8"/>
              <w:framePr w:h="0" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x-align="right" w:y="1"/>
              <w:rPr>
                <w:rStyle w:val="a3"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:fldChar w:fldCharType="begin"/>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rStyle w:val="a3"/>
              </w:rPr>
              <w:instrText>PAGE  </w:instrText>
            </w:r>
            <w:r>
              <w:fldChar w:fldCharType="separate"/>
            </w:r>
            <w:r>
              <w:fldChar w:fldCharType="end"/>
            </w:r>
          </w:p>
        </wx:pBdrGroup>
        <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD">
          <w:pPr>
            <w:pStyle w:val="a8"/>
            <w:ind w:right="360"/>
          </w:pPr>
        </w:p>
      </w:ftr>
      <w:ftr w:type="odd">
        <wx:pBdrGroup>
          <wx:apo>
            <wx:jc wx:val="center"/>
          </wx:apo>
          <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD">
            <w:pPr>
              <w:pStyle w:val="a8"/>
              <w:framePr w:h="0" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x-align="center" w:y="1"/>
              <w:rPr>
                <w:rStyle w:val="a3"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:fldChar w:fldCharType="begin"/>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rStyle w:val="a3"/>
              </w:rPr>
              <w:instrText>PAGE  </w:instrText>
            </w:r>
            <w:r>
              <w:fldChar w:fldCharType="separate"/>
            </w:r>
            <w:r wsp:rsidR="00D935F3">
              <w:rPr>
                <w:rStyle w:val="a3"/>
                <w:noProof/>
              </w:rPr>
              <w:t>22</w:t>
            </w:r>
            <w:r>
              <w:fldChar w:fldCharType="end"/>
            </w:r>
          </w:p>
        </wx:pBdrGroup>
        <w:p wsp:rsidR="00274EAD" wsp:rsidRDefault="00274EAD">
          <w:pPr>
            <w:pStyle w:val="a8"/>
            <w:ind w:right="360"/>
          </w:pPr>
        </w:p>
      </w:ftr>
      <w:hdr w:type="first">
        <w:p wsp:rsidR="00CB4811" wsp:rsidRDefault="00CB4811">
          <w:pPr>
            <w:pStyle w:val="a6"/>
          </w:pPr>
        </w:p>
      </w:hdr>
      <w:ftr w:type="first">
        <w:p wsp:rsidR="00CB4811" wsp:rsidRDefault="00CB4811">
          <w:pPr>
            <w:pStyle w:val="a8"/>
          </w:pPr>
        </w:p>
      </w:ftr>
      <w:pgSz w:w="11906" w:h="16838"/>
      <w:pgMar w:top="1474" w:right="1588" w:bottom="1474" w:left="1588" w:header="851" w:footer="992" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:type="lines" w:line-pitch="312"/>
    </w:sectPr>
  </w:body>
</w:wordDocument>
