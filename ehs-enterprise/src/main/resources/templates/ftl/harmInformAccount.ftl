<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
                xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
                xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006"
                xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml"
                xmlns:w10="urn:schemas-microsoft-com:office:word"
                xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
                xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
                xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2"
                xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" w:macrosPresent="no"
                w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve"><w:ignoreSubtree
            w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2"/>
    <o:DocumentProperties>
        <o:Author>Windows 用户</o:Author>
        <o:LastAuthor>Windows 用户</o:LastAuthor>
        <o:Revision>2</o:Revision>
        <o:TotalTime>1</o:TotalTime>
        <o:Created>2019-10-14T02:47:00Z</o:Created>
        <o:LastSaved>2019-10-14T02:47:00Z</o:LastSaved>
        <o:Pages>2</o:Pages>
        <o:Words>80</o:Words>
        <o:Characters>460</o:Characters>
        <o:Lines>3</o:Lines>
        <o:Paragraphs>1</o:Paragraphs>
        <o:CharactersWithSpaces>539</o:CharactersWithSpaces>
        <o:Version>12</o:Version>
    </o:DocumentProperties>
    <w:fonts>
        <w:defaultFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
        <w:font w:name="Times New Roman">
            <w:panose-1 w:val="02020603050405020304"/>
            <w:charset w:val="00"/>
            <w:family w:val="Roman"/>
            <w:pitch w:val="variable"/>
            <w:sig w:usb-0="E0002AFF" w:usb-1="C0007841" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="宋体">
            <w:altName w:val="SimSun"/>
            <w:panose-1 w:val="02010600030101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="auto"/>
            <w:pitch w:val="variable"/>
            <w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="黑体">
            <w:altName w:val="SimHei"/>
            <w:panose-1 w:val="02010609060101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="fixed"/>
            <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Cambria Math">
            <w:panose-1 w:val="02040503050406030204"/>
            <w:charset w:val="00"/>
            <w:family w:val="Roman"/>
            <w:pitch w:val="variable"/>
            <w:sig w:usb-0="E00002FF" w:usb-1="420024FF" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="0000019F"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Calibri">
            <w:panose-1 w:val="020F0502020204030204"/>
            <w:charset w:val="00"/>
            <w:family w:val="Swiss"/>
            <w:pitch w:val="variable"/>
            <w:sig w:usb-0="E10002FF" w:usb-1="4000ACFF" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="0000019F"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="仿宋_GB2312">
            <w:altName w:val="仿宋"/>
            <w:charset w:val="86"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="fixed"/>
            <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="仿宋">
            <w:panose-1 w:val="02010609060101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="fixed"/>
            <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="@宋体">
            <w:panose-1 w:val="02010600030101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="auto"/>
            <w:pitch w:val="variable"/>
            <w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="@黑体">
            <w:panose-1 w:val="02010609060101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="fixed"/>
            <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="@仿宋">
            <w:panose-1 w:val="02010609060101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="fixed"/>
            <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="@仿宋_GB2312">
            <w:charset w:val="86"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="fixed"/>
            <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000"
                   w:csb-1="00000000"/>
        </w:font>
    </w:fonts>
    <w:lists>
        <w:listDef w:listDefId="0">
            <w:lsid w:val="428077C1"/>
            <w:plt w:val="HybridMultilevel"/>
            <w:tmpl w:val="576C44D2"/>
            <w:lvl w:ilvl="0" w:tplc="AB7C271A">
                <w:start w:val="1"/>
                <w:lvlText w:val="%1."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:ind w:left="1000" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="1" w:tplc="08090019">
                <w:start w:val="1"/>
                <w:lvlText w:val="%2."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="1440"/>
                    </w:tabs>
                    <w:ind w:left="1440" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="2" w:tplc="0809001B">
                <w:start w:val="1"/>
                <w:lvlText w:val="%3."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="2160"/>
                    </w:tabs>
                    <w:ind w:left="2160" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="3" w:tplc="0809000F">
                <w:start w:val="1"/>
                <w:lvlText w:val="%4."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="2880"/>
                    </w:tabs>
                    <w:ind w:left="2880" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="4" w:tplc="08090019">
                <w:start w:val="1"/>
                <w:lvlText w:val="%5."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="3600"/>
                    </w:tabs>
                    <w:ind w:left="3600" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="5" w:tplc="0809001B">
                <w:start w:val="1"/>
                <w:lvlText w:val="%6."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="4320"/>
                    </w:tabs>
                    <w:ind w:left="4320" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="6" w:tplc="0809000F">
                <w:start w:val="1"/>
                <w:lvlText w:val="%7."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="5040"/>
                    </w:tabs>
                    <w:ind w:left="5040" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="7" w:tplc="08090019">
                <w:start w:val="1"/>
                <w:lvlText w:val="%8."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="5760"/>
                    </w:tabs>
                    <w:ind w:left="5760" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="8" w:tplc="0809001B">
                <w:start w:val="1"/>
                <w:lvlText w:val="%9."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="6480"/>
                    </w:tabs>
                    <w:ind w:left="6480" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
        </w:listDef>
        <w:listDef w:listDefId="1">
            <w:lsid w:val="625F2313"/>
            <w:plt w:val="HybridMultilevel"/>
            <w:tmpl w:val="1E40F4F8"/>
            <w:lvl w:ilvl="0" w:tplc="E8CA1F94">
                <w:start w:val="1"/>
                <w:nfc w:val="29"/>
                <w:lvlText w:val="%1"/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:ind w:left="1000" w:hanging="360"/>
                </w:pPr>
                <w:rPr>
                    <w:b/>
                </w:rPr>
            </w:lvl>
            <w:lvl w:ilvl="1" w:tplc="08090019">
                <w:start w:val="1"/>
                <w:lvlText w:val="%2."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="1440"/>
                    </w:tabs>
                    <w:ind w:left="1440" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="2" w:tplc="0809001B">
                <w:start w:val="1"/>
                <w:lvlText w:val="%3."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="2160"/>
                    </w:tabs>
                    <w:ind w:left="2160" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="3" w:tplc="0809000F">
                <w:start w:val="1"/>
                <w:lvlText w:val="%4."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="2880"/>
                    </w:tabs>
                    <w:ind w:left="2880" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="4" w:tplc="08090019">
                <w:start w:val="1"/>
                <w:lvlText w:val="%5."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="3600"/>
                    </w:tabs>
                    <w:ind w:left="3600" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="5" w:tplc="0809001B">
                <w:start w:val="1"/>
                <w:lvlText w:val="%6."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="4320"/>
                    </w:tabs>
                    <w:ind w:left="4320" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="6" w:tplc="0809000F">
                <w:start w:val="1"/>
                <w:lvlText w:val="%7."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="5040"/>
                    </w:tabs>
                    <w:ind w:left="5040" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="7" w:tplc="08090019">
                <w:start w:val="1"/>
                <w:lvlText w:val="%8."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="5760"/>
                    </w:tabs>
                    <w:ind w:left="5760" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
            <w:lvl w:ilvl="8" w:tplc="0809001B">
                <w:start w:val="1"/>
                <w:lvlText w:val="%9."/>
                <w:lvlJc w:val="left"/>
                <w:pPr>
                    <w:tabs>
                        <w:tab w:val="list" w:pos="6480"/>
                    </w:tabs>
                    <w:ind w:left="6480" w:hanging="360"/>
                </w:pPr>
            </w:lvl>
        </w:listDef>
        <w:list w:ilfo="1">
            <w:ilst w:val="1"/>
            <w:lvlOverride w:ilvl="0">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="1">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="2">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="3">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="4">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="5">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="6">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="7">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="8">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
        </w:list>
        <w:list w:ilfo="2">
            <w:ilst w:val="0"/>
            <w:lvlOverride w:ilvl="0">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="1">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="2">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="3">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="4">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="5">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="6">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="7">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
            <w:lvlOverride w:ilvl="8">
                <w:startOverride w:val="1"/>
            </w:lvlOverride>
        </w:list>
    </w:lists>
    <w:styles>
        <w:versionOfBuiltInStylenames w:val="7"/>
        <w:latentStyles w:defLockedState="off" w:latentStyleCount="267">
            <w:lsdException w:name="Normal"/>
            <w:lsdException w:name="heading 1"/>
            <w:lsdException w:name="heading 2"/>
            <w:lsdException w:name="heading 3"/>
            <w:lsdException w:name="heading 4"/>
            <w:lsdException w:name="heading 5"/>
            <w:lsdException w:name="heading 6"/>
            <w:lsdException w:name="heading 7"/>
            <w:lsdException w:name="heading 8"/>
            <w:lsdException w:name="heading 9"/>
            <w:lsdException w:name="toc 1"/>
            <w:lsdException w:name="toc 2"/>
            <w:lsdException w:name="toc 3"/>
            <w:lsdException w:name="toc 4"/>
            <w:lsdException w:name="toc 5"/>
            <w:lsdException w:name="toc 6"/>
            <w:lsdException w:name="toc 7"/>
            <w:lsdException w:name="toc 8"/>
            <w:lsdException w:name="toc 9"/>
            <w:lsdException w:name="caption"/>
            <w:lsdException w:name="Title"/>
            <w:lsdException w:name="Default Paragraph Font"/>
            <w:lsdException w:name="Subtitle"/>
            <w:lsdException w:name="Strong"/>
            <w:lsdException w:name="Emphasis"/>
            <w:lsdException w:name="Normal (Web)"/>
            <w:lsdException w:name="Table Grid"/>
            <w:lsdException w:name="Placeholder Text"/>
            <w:lsdException w:name="No Spacing"/>
            <w:lsdException w:name="Light Shading"/>
            <w:lsdException w:name="Light List"/>
            <w:lsdException w:name="Light Grid"/>
            <w:lsdException w:name="Medium Shading 1"/>
            <w:lsdException w:name="Medium Shading 2"/>
            <w:lsdException w:name="Medium List 1"/>
            <w:lsdException w:name="Medium List 2"/>
            <w:lsdException w:name="Medium Grid 1"/>
            <w:lsdException w:name="Medium Grid 2"/>
            <w:lsdException w:name="Medium Grid 3"/>
            <w:lsdException w:name="Dark List"/>
            <w:lsdException w:name="Colorful Shading"/>
            <w:lsdException w:name="Colorful List"/>
            <w:lsdException w:name="Colorful Grid"/>
            <w:lsdException w:name="Light Shading Accent 1"/>
            <w:lsdException w:name="Light List Accent 1"/>
            <w:lsdException w:name="Light Grid Accent 1"/>
            <w:lsdException w:name="Medium Shading 1 Accent 1"/>
            <w:lsdException w:name="Medium Shading 2 Accent 1"/>
            <w:lsdException w:name="Medium List 1 Accent 1"/>
            <w:lsdException w:name="Revision"/>
            <w:lsdException w:name="List Paragraph"/>
            <w:lsdException w:name="Quote"/>
            <w:lsdException w:name="Intense Quote"/>
            <w:lsdException w:name="Medium List 2 Accent 1"/>
            <w:lsdException w:name="Medium Grid 1 Accent 1"/>
            <w:lsdException w:name="Medium Grid 2 Accent 1"/>
            <w:lsdException w:name="Medium Grid 3 Accent 1"/>
            <w:lsdException w:name="Dark List Accent 1"/>
            <w:lsdException w:name="Colorful Shading Accent 1"/>
            <w:lsdException w:name="Colorful List Accent 1"/>
            <w:lsdException w:name="Colorful Grid Accent 1"/>
            <w:lsdException w:name="Light Shading Accent 2"/>
            <w:lsdException w:name="Light List Accent 2"/>
            <w:lsdException w:name="Light Grid Accent 2"/>
            <w:lsdException w:name="Medium Shading 1 Accent 2"/>
            <w:lsdException w:name="Medium Shading 2 Accent 2"/>
            <w:lsdException w:name="Medium List 1 Accent 2"/>
            <w:lsdException w:name="Medium List 2 Accent 2"/>
            <w:lsdException w:name="Medium Grid 1 Accent 2"/>
            <w:lsdException w:name="Medium Grid 2 Accent 2"/>
            <w:lsdException w:name="Medium Grid 3 Accent 2"/>
            <w:lsdException w:name="Dark List Accent 2"/>
            <w:lsdException w:name="Colorful Shading Accent 2"/>
            <w:lsdException w:name="Colorful List Accent 2"/>
            <w:lsdException w:name="Colorful Grid Accent 2"/>
            <w:lsdException w:name="Light Shading Accent 3"/>
            <w:lsdException w:name="Light List Accent 3"/>
            <w:lsdException w:name="Light Grid Accent 3"/>
            <w:lsdException w:name="Medium Shading 1 Accent 3"/>
            <w:lsdException w:name="Medium Shading 2 Accent 3"/>
            <w:lsdException w:name="Medium List 1 Accent 3"/>
            <w:lsdException w:name="Medium List 2 Accent 3"/>
            <w:lsdException w:name="Medium Grid 1 Accent 3"/>
            <w:lsdException w:name="Medium Grid 2 Accent 3"/>
            <w:lsdException w:name="Medium Grid 3 Accent 3"/>
            <w:lsdException w:name="Dark List Accent 3"/>
            <w:lsdException w:name="Colorful Shading Accent 3"/>
            <w:lsdException w:name="Colorful List Accent 3"/>
            <w:lsdException w:name="Colorful Grid Accent 3"/>
            <w:lsdException w:name="Light Shading Accent 4"/>
            <w:lsdException w:name="Light List Accent 4"/>
            <w:lsdException w:name="Light Grid Accent 4"/>
            <w:lsdException w:name="Medium Shading 1 Accent 4"/>
            <w:lsdException w:name="Medium Shading 2 Accent 4"/>
            <w:lsdException w:name="Medium List 1 Accent 4"/>
            <w:lsdException w:name="Medium List 2 Accent 4"/>
            <w:lsdException w:name="Medium Grid 1 Accent 4"/>
            <w:lsdException w:name="Medium Grid 2 Accent 4"/>
            <w:lsdException w:name="Medium Grid 3 Accent 4"/>
            <w:lsdException w:name="Dark List Accent 4"/>
            <w:lsdException w:name="Colorful Shading Accent 4"/>
            <w:lsdException w:name="Colorful List Accent 4"/>
            <w:lsdException w:name="Colorful Grid Accent 4"/>
            <w:lsdException w:name="Light Shading Accent 5"/>
            <w:lsdException w:name="Light List Accent 5"/>
            <w:lsdException w:name="Light Grid Accent 5"/>
            <w:lsdException w:name="Medium Shading 1 Accent 5"/>
            <w:lsdException w:name="Medium Shading 2 Accent 5"/>
            <w:lsdException w:name="Medium List 1 Accent 5"/>
            <w:lsdException w:name="Medium List 2 Accent 5"/>
            <w:lsdException w:name="Medium Grid 1 Accent 5"/>
            <w:lsdException w:name="Medium Grid 2 Accent 5"/>
            <w:lsdException w:name="Medium Grid 3 Accent 5"/>
            <w:lsdException w:name="Dark List Accent 5"/>
            <w:lsdException w:name="Colorful Shading Accent 5"/>
            <w:lsdException w:name="Colorful List Accent 5"/>
            <w:lsdException w:name="Colorful Grid Accent 5"/>
            <w:lsdException w:name="Light Shading Accent 6"/>
            <w:lsdException w:name="Light List Accent 6"/>
            <w:lsdException w:name="Light Grid Accent 6"/>
            <w:lsdException w:name="Medium Shading 1 Accent 6"/>
            <w:lsdException w:name="Medium Shading 2 Accent 6"/>
            <w:lsdException w:name="Medium List 1 Accent 6"/>
            <w:lsdException w:name="Medium List 2 Accent 6"/>
            <w:lsdException w:name="Medium Grid 1 Accent 6"/>
            <w:lsdException w:name="Medium Grid 2 Accent 6"/>
            <w:lsdException w:name="Medium Grid 3 Accent 6"/>
            <w:lsdException w:name="Dark List Accent 6"/>
            <w:lsdException w:name="Colorful Shading Accent 6"/>
            <w:lsdException w:name="Colorful List Accent 6"/>
            <w:lsdException w:name="Colorful Grid Accent 6"/>
            <w:lsdException w:name="Subtle Emphasis"/>
            <w:lsdException w:name="Intense Emphasis"/>
            <w:lsdException w:name="Subtle Reference"/>
            <w:lsdException w:name="Intense Reference"/>
            <w:lsdException w:name="Book Title"/>
            <w:lsdException w:name="Bibliography"/>
            <w:lsdException w:name="TOC Heading"/>
        </w:latentStyles>
        <w:style w:type="paragraph" w:default="on" w:styleId="a">
            <w:name w:val="Normal"/>
            <wx:uiName wx:val="正文"/>
            <w:rsid w:val="00B64BDB"/>
            <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
                <wx:font wx:val="Times New Roman"/>
                <w:kern w:val="2"/>
                <w:sz w:val="21"/>
                <w:sz-cs w:val="24"/>
                <w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:default="on" w:styleId="a0">
            <w:name w:val="Default Paragraph Font"/>
            <wx:uiName wx:val="默认段落字体"/>
        </w:style>
        <w:style w:type="table" w:default="on" w:styleId="a1">
            <w:name w:val="Normal Table"/>
            <wx:uiName wx:val="普通表格"/>
            <w:rPr>
                <wx:font wx:val="Calibri"/>
                <w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
            </w:rPr>
            <w:tblPr>
                <w:tblInd w:w="0" w:type="dxa"/>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:left w:w="108" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                    <w:right w:w="108" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPr>
        </w:style>
        <w:style w:type="list" w:default="on" w:styleId="a2">
            <w:name w:val="No List"/>
            <wx:uiName wx:val="无列表"/>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a3">
            <w:name w:val="header"/>
            <wx:uiName wx:val="页眉"/>
            <w:basedOn w:val="a"/>
            <w:link w:val="Char"/>
            <w:rsid w:val="00B64BDB"/>
            <w:pPr>
                <w:pBdr>
                    <w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
                </w:pBdr>
                <w:tabs>
                    <w:tab w:val="center" w:pos="4153"/>
                    <w:tab w:val="right" w:pos="8306"/>
                </w:tabs>
                <w:snapToGrid w:val="off"/>
                <w:jc w:val="center"/>
            </w:pPr>
            <w:rPr>
                <wx:font wx:val="Times New Roman"/>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="Char">
            <w:name w:val="页眉 Char"/>
            <w:basedOn w:val="a0"/>
            <w:link w:val="a3"/>
            <w:rsid w:val="00B64BDB"/>
            <w:rPr>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a4">
            <w:name w:val="footer"/>
            <wx:uiName wx:val="页脚"/>
            <w:basedOn w:val="a"/>
            <w:link w:val="Char0"/>
            <w:rsid w:val="00B64BDB"/>
            <w:pPr>
                <w:tabs>
                    <w:tab w:val="center" w:pos="4153"/>
                    <w:tab w:val="right" w:pos="8306"/>
                </w:tabs>
                <w:snapToGrid w:val="off"/>
                <w:jc w:val="left"/>
            </w:pPr>
            <w:rPr>
                <wx:font wx:val="Times New Roman"/>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="Char0">
            <w:name w:val="页脚 Char"/>
            <w:basedOn w:val="a0"/>
            <w:link w:val="a4"/>
            <w:rsid w:val="00B64BDB"/>
            <w:rPr>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a5">
            <w:name w:val="Normal (Web)"/>
            <wx:uiName wx:val="普通(网站)"/>
            <w:basedOn w:val="a"/>
            <w:rsid w:val="00B64BDB"/>
            <w:pPr>
                <w:widowControl/>
                <w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
                <w:jc w:val="left"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
                <wx:font wx:val="宋体"/>
                <w:kern w:val="0"/>
                <w:sz w:val="24"/>
            </w:rPr>
        </w:style>
    </w:styles>
    <w:divs>
        <w:div w:id="2044358061">
            <w:bodyDiv w:val="on"/>
            <w:marLeft w:val="0"/>
            <w:marRight w:val="0"/>
            <w:marTop w:val="0"/>
            <w:marBottom w:val="0"/>
            <w:divBdr>
                <w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
                <w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
                <w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
                <w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
            </w:divBdr>
        </w:div>
    </w:divs>
    <w:shapeDefaults>
        <o:shapedefaults v:ext="edit" spidmax="3074"/>
        <o:shapelayout v:ext="edit">
            <o:idmap v:ext="edit" data="2"/>
        </o:shapelayout>
    </w:shapeDefaults>
    <w:docPr>
        <w:view w:val="print"/>
        <w:zoom w:percent="100"/>
        <w:doNotEmbedSystemFonts/>
        <w:bordersDontSurroundHeader/>
        <w:bordersDontSurroundFooter/>
        <w:defaultTabStop w:val="420"/>
        <w:drawingGridVerticalSpacing w:val="156"/>
        <w:displayHorizontalDrawingGridEvery w:val="0"/>
        <w:displayVerticalDrawingGridEvery w:val="2"/>
        <w:punctuationKerning/>
        <w:characterSpacingControl w:val="CompressPunctuation"/>
        <w:optimizeForBrowser/>
        <w:validateAgainstSchema/>
        <w:saveInvalidXML w:val="off"/>
        <w:ignoreMixedContent w:val="off"/>
        <w:alwaysShowPlaceholderText w:val="off"/>
        <w:hdrShapeDefaults>
            <o:shapedefaults v:ext="edit" spidmax="3074"/>
        </w:hdrShapeDefaults>
        <w:footnotePr>
            <w:footnote w:type="separator">
                <w:p wsp:rsidR="009A640B" wsp:rsidRDefault="009A640B" wsp:rsidP="00B64BDB">
                    <w:r>
                        <w:separator/>
                    </w:r>
                </w:p>
            </w:footnote>
            <w:footnote w:type="continuation-separator">
                <w:p wsp:rsidR="009A640B" wsp:rsidRDefault="009A640B" wsp:rsidP="00B64BDB">
                    <w:r>
                        <w:continuationSeparator/>
                    </w:r>
                </w:p>
            </w:footnote>
        </w:footnotePr>
        <w:endnotePr>
            <w:endnote w:type="separator">
                <w:p wsp:rsidR="009A640B" wsp:rsidRDefault="009A640B" wsp:rsidP="00B64BDB">
                    <w:r>
                        <w:separator/>
                    </w:r>
                </w:p>
            </w:endnote>
            <w:endnote w:type="continuation-separator">
                <w:p wsp:rsidR="009A640B" wsp:rsidRDefault="009A640B" wsp:rsidP="00B64BDB">
                    <w:r>
                        <w:continuationSeparator/>
                    </w:r>
                </w:p>
            </w:endnote>
        </w:endnotePr>
        <w:compat>
            <w:spaceForUL/>
            <w:balanceSingleByteDoubleByteWidth/>
            <w:doNotLeaveBackslashAlone/>
            <w:ulTrailSpace/>
            <w:doNotExpandShiftReturn/>
            <w:adjustLineHeightInTable/>
            <w:breakWrappedTables/>
            <w:snapToGridInCell/>
            <w:wrapTextWithPunct/>
            <w:useAsianBreakRules/>
            <w:dontGrowAutofit/>
            <w:useFELayout/>
        </w:compat>
        <wsp:rsids>
            <wsp:rsidRoot wsp:val="00B64BDB"/>
            <wsp:rsid wsp:val="0009310F"/>
            <wsp:rsid wsp:val="001A7DEE"/>
            <wsp:rsid wsp:val="004B6D79"/>
            <wsp:rsid wsp:val="007263B6"/>
            <wsp:rsid wsp:val="009A640B"/>
            <wsp:rsid wsp:val="00B64BDB"/>
            <wsp:rsid wsp:val="00E75FE8"/>
        </wsp:rsids>
    </w:docPr>
    <w:body>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:pStyle w:val="a5"/>
                <w:listPr>
                    <w:ilvl w:val="0"/>
                    <w:ilfo w:val="1"/>
                    <wx:t wx:val="㈠"/>
                    <wx:font wx:val="Times New Roman"/>
                </w:listPr>
                <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600"
                           w:line-rule="exact"/>
                <w:ind w:left="567" w:hanging="567"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:b/>
                    <w:b-cs/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:t>职业危害告知台帐</w:t>
            </w:r>
        </w:p>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:pStyle w:val="a5"/>
                <w:listPr>
                    <w:ilvl w:val="0"/>
                    <w:ilfo w:val="2"/>
                    <wx:t wx:val="1."/>
                    <wx:font wx:val="Times New Roman"/>
                </w:listPr>
                <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600"
                           w:line-rule="exact"/>
                <w:ind w:left="426" w:hanging="426"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:t>岗前告知内容</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblW w:w="5049" w:type="pct"/>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                </w:tblBorders>
                <w:tblLook w:val="04A0"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="602"/>
                <w:gridCol w:w="781"/>
                <w:gridCol w:w="3028"/>
                <w:gridCol w:w="1274"/>
                <w:gridCol w:w="1196"/>
                <w:gridCol w:w="1725"/>
            </w:tblGrid>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="007263B6">
                <w:trPr>
                    <w:trHeight w:val="689"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="350" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>序号</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="454" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>员工</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1759" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>岗位</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="740" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>上岗时间</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="695" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>告知时间</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1002" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>告知书</w:t>
                        </w:r>
                    </w:p>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
                                <wx:font wx:val="仿宋"/>
                                <w:sz w:val="15"/>
                                <w:sz-cs w:val="15"/>
                            </w:rPr>
                            <w:t>（请于系统中下载查看）</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <#list employeeWorkNotifyList as employeeWorkNotify>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="007263B6">
                <w:trPr>
                    <w:trHeight w:val="689"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="350" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${employeeWorkNotify_index+1}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="454" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:ind w:right-chars="-51" w:right="-107"/>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${employeeWorkNotify.employeeName!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1759" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${employeeWorkNotify.postName!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="740" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${employeeWorkNotify.workDate!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="695" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${employeeWorkNotify.notifyDate!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1002" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${employeeWorkNotify.notifyFile!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            </#list>
        </w:tbl>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:spacing w:line="600" w:line-rule="exact"/>
                <w:jc w:val="left"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:color w:val="4472C4"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
        </w:p>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:listPr>
                    <w:ilvl w:val="0"/>
                    <w:ilfo w:val="2"/>
                    <wx:t wx:val="2."/>
                    <wx:font wx:val="Times New Roman"/>
                </w:listPr>
                <w:spacing w:line="600" w:line-rule="exact"/>
                <w:ind w:left="426" w:hanging="568"/>
                <w:jc w:val="left"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:t>作业场所告知内容</w:t>
            </w:r>
        </w:p>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:spacing w:line="600" w:line-rule="exact"/>
                <w:jc w:val="left"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:t>⑴作业场所设置规范、醒目职业病危害警示标识；</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblW w:w="5000" w:type="pct"/>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                </w:tblBorders>
                <w:tblLook w:val="04A0"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="1135"/>
                <w:gridCol w:w="5585"/>
                <w:gridCol w:w="1802"/>
            </w:tblGrid>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="004B6D79">
                <w:trPr>
                    <w:trHeight w:val="645"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="666" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
                                <wx:font wx:val="仿宋"/>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>序号</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="3277" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
                                <wx:font wx:val="仿宋"/>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>作业场所名称</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1057" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>职业病危害警示</w:t>
                        </w:r>
                    </w:p>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
                                <wx:font wx:val="仿宋"/>
                                <w:sz w:val="15"/>
                                <w:sz-cs w:val="15"/>
                            </w:rPr>
                            <w:t>（请于系统中下载查看）</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <#list enterpriseWorkplaceNotifyList as enterpriseWorkplaceNotify>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="004B6D79">
                <w:trPr>
                    <w:trHeight w:val="645"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="666" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseWorkplaceNotify_index+1}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="3277" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:ind w:right-chars="-51" w:right="-107"/>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.workplaceName!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1057" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.dentifyFileNum!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            </#list>
        </w:tbl>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:spacing w:line="600" w:line-rule="exact"/>
                <w:jc w:val="left"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:br/>
                <w:t>⑵高毒物品告知卡；职业病危害因素信息卡（毒物信息卡MSDS）；</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblW w:w="5000" w:type="pct"/>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                </w:tblBorders>
                <w:tblLook w:val="04A0"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="799"/>
                <w:gridCol w:w="3895"/>
                <w:gridCol w:w="1757"/>
                <w:gridCol w:w="2071"/>
            </w:tblGrid>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="00B64BDB">
                <w:trPr>
                    <w:trHeight w:val="645"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="469" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>序号</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2285" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>作业场所名称</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1031" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>高毒物品告知卡</w:t>
                        </w:r>
                    </w:p>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
                                <wx:font wx:val="仿宋"/>
                                <w:sz w:val="15"/>
                                <w:sz-cs w:val="15"/>
                            </w:rPr>
                            <w:t>（请于系统中下载查看）</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1215" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>职业危害因素信息卡</w:t>
                        </w:r>
                    </w:p>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
                                <wx:font wx:val="仿宋"/>
                                <w:sz w:val="15"/>
                                <w:sz-cs w:val="15"/>
                            </w:rPr>
                            <w:t>（请于系统中下载查看）</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <#list enterpriseWorkplaceNotifyList as enterpriseWorkplaceNotify>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="00B64BDB">
                <w:trPr>
                    <w:trHeight w:val="645"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="469" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseWorkplaceNotify_index+1}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2285" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:ind w:right-chars="-51" w:right="-107"/>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.workplaceName!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1031" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.informFileNum!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1215" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseWorkplaceNotify.informationFileNum!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            </#list>
        </w:tbl>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:pStyle w:val="a5"/>
                <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off" w:line="600"
                           w:line-rule="exact"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:t>⑶监测、检测结果公示；</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblW w:w="5000" w:type="pct"/>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                </w:tblBorders>
                <w:tblLook w:val="04A0"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="1733"/>
                <w:gridCol w:w="1733"/>
                <w:gridCol w:w="1735"/>
                <w:gridCol w:w="1519"/>
                <w:gridCol w:w="1802"/>
            </w:tblGrid>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="00B64BDB">
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1017" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>作业场所名称</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1017" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>检测机构</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1018" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>检测时间</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="891" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>公示时间</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1057" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>公示证明文件</w:t>
                        </w:r>
                    </w:p>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
                                <wx:font wx:val="仿宋"/>
                                <w:sz w:val="15"/>
                                <w:sz-cs w:val="15"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
                                <wx:font wx:val="仿宋"/>
                                <w:sz w:val="15"/>
                                <w:sz-cs w:val="15"/>
                            </w:rPr>
                            <w:t>（请于系统中下载查看）</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <#list enterpriseWorkplaceCheckNotifyList as enterpriseWorkplaceNotify>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidRPr="004B6D79" wsp:rsidTr="004B6D79">
                <w:trPr>
                    <w:trHeight w:val="680"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1017" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="004B6D79" wsp:rsidP="004B6D79">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.workplaceName!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1017" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="004B6D79" wsp:rsidP="004B6D79">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.checkOrganization!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1018" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="004B6D79" wsp:rsidP="004B6D79">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.checkDate!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="891" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="004B6D79" wsp:rsidP="004B6D79">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.notifyDate!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1057" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="004B6D79">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseWorkplaceNotify.checkResultProve!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            </#list>
        </w:tbl>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:spacing w:line="600" w:line-rule="exact"/>
                <w:jc w:val="left"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:color w:val="4472C4"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
        </w:p>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:spacing w:line="600" w:line-rule="exact"/>
                <w:jc w:val="left"/>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                    <wx:font wx:val="仿宋_GB2312"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:t>⑷职业健康检查和职业病诊断结果的告知凭证。</w:t>
            </w:r>
        </w:p>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:spacing w:line="600" w:line-rule="exact"/>
                <w:jc w:val="center"/>
                <w:rPr>
                    <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
                    <wx:font wx:val="黑体"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
                    <wx:font wx:val="黑体"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:t>职业健康检查告知</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblW w:w="5000" w:type="pct"/>
                <w:jc w:val="center"/>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                </w:tblBorders>
                <w:tblLook w:val="04A0"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="867"/>
                <w:gridCol w:w="941"/>
                <w:gridCol w:w="1551"/>
                <w:gridCol w:w="1621"/>
                <w:gridCol w:w="1740"/>
                <w:gridCol w:w="1802"/>
            </w:tblGrid>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="00B64BDB">
                <w:trPr>
                    <w:trHeight w:val="682"/>
                    <w:jc w:val="center"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="509" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>序号</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="552" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>员工</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="910" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>体检类型</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="951" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>体检时间</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1021" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>告知时间</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1057" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>告知书</w:t>
                        </w:r>
                    </w:p>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
                                <wx:font wx:val="仿宋"/>
                                <w:sz w:val="15"/>
                                <w:sz-cs w:val="15"/>
                            </w:rPr>
                            <w:t>（请于系统中下载查看）</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <#list enterpriseMedicalResultNotifyList as enterpriseMedicalResultNotify>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="00B64BDB">
                <w:trPr>
                    <w:trHeight w:val="682"/>
                    <w:jc w:val="center"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="509" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseMedicalResultNotify_index+1}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="552" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:ind w:right-chars="-51" w:right="-107"/>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseMedicalResultNotify.employeeName!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="910" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:ind w:right-chars="-51" w:right="-107"/>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseMedicalResultNotify.type!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="951" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseMedicalResultNotify.medicalDate!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1021" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseMedicalResultNotify.notifyDate!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1057" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseMedicalResultNotify.notifyFile!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            </#list>
        </w:tbl>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:rPr>
                    <w:rFonts w:hint="fareast"/>
                </w:rPr>
            </w:pPr>
        </w:p>
        <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB" wsp:rsidP="00B64BDB">
            <w:pPr>
                <w:jc w:val="center"/>
                <w:rPr>
                    <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体"/>
                    <wx:font wx:val="黑体"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
                    <wx:font wx:val="黑体"/>
                    <w:sz w:val="32"/>
                    <w:sz-cs w:val="32"/>
                </w:rPr>
                <w:t>职业病诊断结果告知</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblpPr w:leftFromText="180" w:rightFromText="180" w:vertAnchor="text" w:horzAnchor="margin"
                          w:tblpY="45"/>
                <w:tblW w:w="5000" w:type="pct"/>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                    <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                </w:tblBorders>
                <w:tblLook w:val="04A0"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="644"/>
                <w:gridCol w:w="1225"/>
                <w:gridCol w:w="1812"/>
                <w:gridCol w:w="3039"/>
                <w:gridCol w:w="1802"/>
            </w:tblGrid>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="00B64BDB">
                <w:trPr>
                    <w:trHeight w:val="485"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="378" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>序号</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="719" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>员工</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1063" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>诊断时间</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1783" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>告知时间</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1057" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                                <w:b/>
                            </w:rPr>
                            <w:t>告知书</w:t>
                        </w:r>
                    </w:p>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
                                <wx:font wx:val="仿宋"/>
                                <w:sz w:val="15"/>
                                <w:sz-cs w:val="15"/>
                            </w:rPr>
                            <w:t>（请于系统中下载查看）</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <#list enterpriseDiseaseDiagnosiNotifyList as enterpriseDiseaseDiagnosiNotify>
            <w:tr wsp:rsidR="00B64BDB" wsp:rsidTr="00B64BDB">
                <w:trPr>
                    <w:trHeight w:val="485"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="378" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseDiseaseDiagnosiNotify_index+1}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="719" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:ind w:right-chars="-51" w:right="-107"/>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseDiseaseDiagnosiNotify.employeeName!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1063" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseDiseaseDiagnosiNotify.diagnosiDate!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1783" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:t>${enterpriseDiseaseDiagnosiNotify.notifyDate!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1057" w:type="pct"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p wsp:rsidR="00B64BDB" wsp:rsidRDefault="00B64BDB">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="fareast"/>
                                <wx:font wx:val="宋体"/>
                            </w:rPr>
                            <w:t>${enterpriseDiseaseDiagnosiNotify.notifyFile!''}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            </#list>
        </w:tbl>
        <w:p wsp:rsidR="009A640B" wsp:rsidRDefault="009A640B" wsp:rsidP="00E75FE8"/>
        <w:sectPr wsp:rsidR="009A640B">
            <w:pgSz w:w="11906" w:h="16838"/>
            <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992"
                     w:gutter="0"/>
            <w:cols w:space="425"/>
            <w:docGrid w:type="lines" w:line-pitch="312"/>
        </w:sectPr>
    </w:body></w:wordDocument>