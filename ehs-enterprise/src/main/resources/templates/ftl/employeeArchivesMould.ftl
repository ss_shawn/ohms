<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml"
              pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
                              Target="docProps/app.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
                              Target="docProps/core.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
                              Target="word/document.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties"
                              Target="docProps/custom.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId8"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
                              Target="fontTable.xml"/>
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings"
                              Target="webSettings.xml"/>
                <Relationship Id="rId7" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"
                              Target="media/image2.png"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
                              Target="settings.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
                              Target="styles.xml"/>
                <Relationship Id="rId6" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"
                              Target="media/image1.jpeg"/>
                <Relationship Id="rId5"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes"
                              Target="endnotes.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes"
                              Target="footnotes.xml"/>
                <Relationship Id="rId9" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
                              Target="theme/theme1.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
                <w:body>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>        </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidR="006A7EF6">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                            </w:rPr>
                            <w:t xml:space="preserve">       </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t xml:space="preserve">              </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>档案编号：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>${archiveNumber!''}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E" w:rsidP="00601CBF">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="44"/>
                                <w:szCs w:val="44"/>
                            </w:rPr>
                            <w:t>劳动者个人职业健康监护档案</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E" w:rsidP="00601CBF">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00601CBF" w:rsidRDefault="00601CBF">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="006A7EF6" w:rsidRDefault="006A7EF6">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="006A7EF6" w:rsidRDefault="006A7EF6">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="006A7EF6" w:rsidRDefault="006A7EF6">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="006A7EF6" w:rsidRDefault="006A7EF6">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="480"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>单   位：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>${enterpriseName!''}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00601CBF">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>姓   </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>名：</w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>  </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>${name!''}</w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>    </w:t>
                        </w:r>
                        <w:r w:rsidR="00C25F6A">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">      </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>     </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00601CBF">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>性   </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>别：</w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidR="00C25F6A">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>${sex!''}</w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>    </w:t>
                        </w:r>
                        <w:r w:rsidR="00C25F6A">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">   </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">    </w:t>
                        </w:r>
                        <w:r w:rsidR="00C25F6A">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">    </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00601CBF">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>建档时间：</w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>  </w:t>
                        </w:r>
                        <w:r w:rsidR="00C25F6A">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>${buildTime!''}</w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>  </w:t>
                        </w:r>
                        <w:r w:rsidR="00C25F6A">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">            </w:t>
                        </w:r>
                        <w:r w:rsidR="00C9055E">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>

                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>

                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                        </w:pPr>
                        <w:r>
                            <w:br w:type="page"/>
                        </w:r>
                    </w:p>

                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="44"/>
                                <w:szCs w:val="44"/>
                            </w:rPr>

                            <w:t>目 录</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>

                            <w:t> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="1344"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>1．劳动者个人信息卡（表6-1）</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="1344"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>2．工作场所职业病危害因素检测结果（表6-2）</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="1344"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>3．历次职业健康检查结果及处理情况（表6-3）</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="1344"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>4．历次职业健康体检报告、职业病诊疗等资料</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="1344"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>5．其他职业健康监护资料</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>

                    <w:p w:rsidR="006A7EF6" w:rsidRDefault="006A7EF6">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                        </w:pPr>
                        <w:r>
                            <w:br w:type="page"/>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="006A7EF6" w:rsidRDefault="006A7EF6" w:rsidP="006A7EF6">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>

                            <w:t>劳动者个人信息卡</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>表6-1  </w:t>
                        </w:r>
                        <w:r w:rsidR="006A7EF6">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t xml:space="preserve">                           </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidR="006A7EF6">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>档案号：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${archiveNumber}</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:jc w:val="center"/>
                            <w:tblInd w:w="0" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:left w:w="0" w:type="dxa"/>
                                <w:right w:w="0" w:type="dxa"/>
                            </w:tblCellMar>
                            <w:tblLook w:val="0000"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="1668"/>
                            <w:gridCol w:w="111"/>
                            <w:gridCol w:w="540"/>
                            <w:gridCol w:w="1405"/>
                            <w:gridCol w:w="71"/>
                            <w:gridCol w:w="74"/>
                            <w:gridCol w:w="990"/>
                            <w:gridCol w:w="35"/>
                            <w:gridCol w:w="278"/>
                            <w:gridCol w:w="773"/>
                            <w:gridCol w:w="344"/>
                            <w:gridCol w:w="424"/>
                            <w:gridCol w:w="1375"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1668" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>姓名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2056" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${name!''}</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1170" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>性别</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1051" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${sex!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2143" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00E3463D">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:pict>
                                            <v:shape id="_x0000_i1025" type="#_x0000_t75"
                                                     style="width:99.75pt;height:114pt">
                                                <v:imagedata r:id="rId7" o:title="1564972389(1)"/>
                                            </v:shape>
                                        </w:pict>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1668" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>籍贯</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2056" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${nativePlace!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1170" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>婚姻</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1051" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${isMarried!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2143" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1668" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>文化程度</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2056" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${educationDegree!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1170" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>嗜好</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1051" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t></w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2143" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1668" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>参加工作</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4277" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${attendWorkTime!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2143" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1668" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>身份证号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4277" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${idCard!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2143" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8088" w:type="dxa"/>
                                    <w:gridSpan w:val="13"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>职业史及职业病危害接触史</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2319" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>起止时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1550" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>工作单位</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="990" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>工种</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1430" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>接触职业病危害因素</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1799" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>防护措施</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>

<#list harmContactHistoryList as harmContactHistory>
                        <w:tr w:rsidR="003E3F4A" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2319" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                        </w:rPr>
                                        <w:t>${harmContactHistory.startDate!''}</w:t>
                                    </w:r>
                                </w:p>

                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1550" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" />
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${harmContactHistory.enterpriseName!''}</w:t>
                                    </w:r>

                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="990" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${harmContactHistory.postName!''}</w:t>
                                    </w:r>

                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1430" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" />
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${harmContactHistory.harmFactorName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1799" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRPr="00AB40BD" w:rsidRDefault="003E3F4A"
                                     w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="375" />
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00AB40BD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${harmContactHistory.protectArticlesName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
</#list>
                        <w:tr w:rsidR="00E3463D" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8088" w:type="dxa"/>
                                    <w:gridSpan w:val="13"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00E3463D">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>职业病史</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>

                        <w:tr w:rsidR="00E3463D" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1779" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>疾病名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2016" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>诊断时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1377" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>诊断医院</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1541" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>治疗结果</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1375" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>备注</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>

                        <#list employeeMedicalHistoryList as employeeMedicalHistory>
                        <w:tr w:rsidR="00E3463D" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1779" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalHistory.diseaseName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2016" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00E3463D">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalHistory.diagnosiDate!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1377" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalHistory.diagnosiHosipital!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1541" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalHistory.diagnosiResult!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1375" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalHistory.remark!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
</#list>


                        <w:tr w:rsidR="003E3F4A" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8088" w:type="dxa"/>
                                    <w:gridSpan w:val="13"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="00E3463D" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00E3463D">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>职业禁忌症史</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>

                        <w:tr w:rsidR="003E3F4A" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1779" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>疾病名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2016" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>诊断时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1377" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>诊断医院</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1541" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>治疗结果</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1375" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>备注</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>

                        <#list employeeMedicalHistoryList2 as employeeMedicalHistory>
                            <w:tr w:rsidR="00E3463D" w:rsidTr="00E3463D">
                                <w:trPr>
                                    <w:jc w:val="center"/>
                                </w:trPr>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1779" w:type="dxa"/>
                                        <w:gridSpan w:val="2"/>
                                        <w:tcMar>
                                            <w:top w:w="0" w:type="dxa"/>
                                            <w:left w:w="108" w:type="dxa"/>
                                            <w:bottom w:w="0" w:type="dxa"/>
                                            <w:right w:w="108" w:type="dxa"/>
                                        </w:tcMar>
                                    </w:tcPr>
                                    <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                        <w:pPr>
                                            <w:widowControl/>
                                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                                            <w:jc w:val="center"/>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                                <w:bCs/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                            <w:t>${employeeMedicalHistory.diseaseName!''}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="2016" w:type="dxa"/>
                                        <w:gridSpan w:val="3"/>
                                        <w:tcMar>
                                            <w:top w:w="0" w:type="dxa"/>
                                            <w:left w:w="108" w:type="dxa"/>
                                            <w:bottom w:w="0" w:type="dxa"/>
                                            <w:right w:w="108" w:type="dxa"/>
                                        </w:tcMar>
                                    </w:tcPr>
                                    <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00E3463D">
                                        <w:pPr>
                                            <w:widowControl/>
                                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                                            <w:jc w:val="center"/>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="18"/>
                                                <w:szCs w:val="18"/>
                                            </w:rPr>
                                            <w:t>${employeeMedicalHistory.diagnosiDate!''}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1377" w:type="dxa"/>
                                        <w:gridSpan w:val="4"/>
                                        <w:tcMar>
                                            <w:top w:w="0" w:type="dxa"/>
                                            <w:left w:w="108" w:type="dxa"/>
                                            <w:bottom w:w="0" w:type="dxa"/>
                                            <w:right w:w="108" w:type="dxa"/>
                                        </w:tcMar>
                                    </w:tcPr>
                                    <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                        <w:pPr>
                                            <w:widowControl/>
                                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                                            <w:jc w:val="center"/>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                                <w:bCs/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                            <w:t>${employeeMedicalHistory.diagnosiHosipital!''}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1541" w:type="dxa"/>
                                        <w:gridSpan w:val="3"/>
                                        <w:tcMar>
                                            <w:top w:w="0" w:type="dxa"/>
                                            <w:left w:w="108" w:type="dxa"/>
                                            <w:bottom w:w="0" w:type="dxa"/>
                                            <w:right w:w="108" w:type="dxa"/>
                                        </w:tcMar>
                                    </w:tcPr>
                                    <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                        <w:pPr>
                                            <w:widowControl/>
                                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                                            <w:jc w:val="center"/>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                                <w:bCs/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                            <w:t>${employeeMedicalHistory.diagnosiResult!''}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1375" w:type="dxa"/>
                                        <w:tcMar>
                                            <w:top w:w="0" w:type="dxa"/>
                                            <w:left w:w="108" w:type="dxa"/>
                                            <w:bottom w:w="0" w:type="dxa"/>
                                            <w:right w:w="108" w:type="dxa"/>
                                        </w:tcMar>
                                    </w:tcPr>
                                    <w:p w:rsidR="00E3463D" w:rsidRDefault="00E3463D" w:rsidP="00AA5686">
                                        <w:pPr>
                                            <w:widowControl/>
                                            <w:spacing w:line="360" w:lineRule="atLeast"/>
                                            <w:jc w:val="center"/>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                                <w:b/>
                                                <w:bCs/>
                                                <w:kern w:val="0"/>
                                                <w:sz w:val="24"/>
                                            </w:rPr>
                                            <w:t>${employeeMedicalHistory.remark!''}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                            </w:tr>
                        </#list>

                        <w:tr w:rsidR="003E3F4A" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8088" w:type="dxa"/>
                                    <w:gridSpan w:val="13"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>职业病诊断</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="003E3F4A" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1779" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>职业病名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2016" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>诊断时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1377" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>诊断医院</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1541" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>诊断级别</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1375" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>备注</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <#list employeeDiseaseDiagnosiList as employeeDiseaseDiagnosi>
                        <w:tr w:rsidR="003E3F4A" w:rsidTr="00E3463D">
                            <w:trPr>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1779" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeDiseaseDiagnosi.name!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2016" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                        </w:rPr>
                                        <w:t>${employeeDiseaseDiagnosi.diagnosiDate!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1377" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeDiseaseDiagnosi.hospital!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1541" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeDiseaseDiagnosi.diagnosiLevel!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1375" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="003E3F4A" w:rsidRDefault="003E3F4A" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="360" w:lineRule="atLeast"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeDiseaseDiagnosi.remark!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        </#list>
                    </w:tbl>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                        </w:pPr>
                        <w:r>
                            <w:br w:type="page"/>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>表6-2 工作场所职业病危害因素检测结果</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>劳动者姓名：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${name!''}</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>  </w:t>
                        </w:r>
                        <w:r w:rsidR="006A7EF6">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t xml:space="preserve">                     </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>      档案号：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${archiveNumber!''}</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:jc w:val="center"/>
                            <w:tblInd w:w="0" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:left w:w="0" w:type="dxa"/>
                                <w:right w:w="0" w:type="dxa"/>
                            </w:tblCellMar>
                            <w:tblLook w:val="0000"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="962"/>
                            <w:gridCol w:w="708"/>
                            <w:gridCol w:w="990"/>
                            <w:gridCol w:w="1560"/>
                            <w:gridCol w:w="2171"/>
                            <w:gridCol w:w="1243"/>
                            <w:gridCol w:w="888"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00AB40BD">
                            <w:trPr>
                                <w:trHeight w:val="732"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="962" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>岗位</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="708" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>检测时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="990" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>检测机构</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1560" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>职业病危害因素名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2171" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>职业病危害</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>因素检测结果</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1243" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>防护措施</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="888" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>备注</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <#list enterpriseOutsideCheckResultAllCustomList as enterpriseOutsideCheckResultAllCustom>
                        <w:tr w:rsidR="00AB40BD" w:rsidTr="003C48E3">
                            <w:trPr>
                                <w:trHeight w:val="675"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="962" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00AB40BD" w:rsidRDefault="00AB40BD" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${enterpriseOutsideCheckResultAllCustom.postName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="708" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00AB40BD" w:rsidRDefault="00AB40BD" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${enterpriseOutsideCheckResultAllCustom.checkDate!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="990" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00AB40BD" w:rsidRDefault="00AB40BD" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${enterpriseOutsideCheckResultAllCustom.checkOrganization!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1560" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00AB40BD" w:rsidRDefault="00AB40BD" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${enterpriseOutsideCheckResultAllCustom.harmFactorName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2171" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00AB40BD" w:rsidRDefault="00AB40BD" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${enterpriseOutsideCheckResultAllCustom.placeConsistence!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1243" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00AB40BD" w:rsidRPr="00AB40BD" w:rsidRDefault="00AB40BD"
                                     w:rsidP="003C48E3">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="375" w:lineRule="atLeast"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00AB40BD">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${enterpriseOutsideCheckResultAllCustom.protectArticlesName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="888" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00AB40BD" w:rsidRDefault="00AB40BD" w:rsidP="003E3F4A">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>

                                        <w:t>${enterpriseOutsideCheckResultAllCustom.remark!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        </#list>
                    </w:tbl>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                        </w:pPr>
                        <w:r>
                            <w:br w:type="page"/>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>表6-3 历次职业健康检查结果及处理情况</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>劳动者姓名：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${name!''}</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>  </w:t>
                        </w:r>
                        <w:r w:rsidR="00AF6AF8">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t xml:space="preserve">                 </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>     档案号：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${archiveNumber!''}</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:jc w:val="center"/>
                            <w:tblInd w:w="0" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:left w:w="0" w:type="dxa"/>
                                <w:right w:w="0" w:type="dxa"/>
                            </w:tblCellMar>
                            <w:tblLook w:val="0000"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="792"/>
                            <w:gridCol w:w="736"/>
                            <w:gridCol w:w="990"/>
                            <w:gridCol w:w="1437"/>
                            <w:gridCol w:w="973"/>
                            <w:gridCol w:w="994"/>
                            <w:gridCol w:w="850"/>
                            <w:gridCol w:w="1750"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00AB40BD">
                            <w:trPr>
                                <w:trHeight w:val="957"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="792" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>检查日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="736" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>检查种类</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="990" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>检查</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>结论</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1437" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>检查机构</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="973" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>岗位</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="994" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>人员处理情况</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="850" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>本人</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>签字</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1750" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>现场处理情况</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>

                        <#list employeeMedicalCustomlist as employeeMedicalCustom>
                        <w:tr w:rsidR="00C9055E" w:rsidTr="00AB40BD">
                            <w:trPr>
                                <w:trHeight w:val="705"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="792" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalCustom.medicalDate!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="736" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalCustom.type!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="990" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">${employeeMedicalCustom.result!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1437" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalCustom.institutionName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="973" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalCustom.postName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="994" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="27"/>
                                            <w:szCs w:val="27"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalCustom.personnelHandling!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="850" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalCustom.medicalNotifyNum!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1750" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                </w:tcPr>
                                <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeMedicalCustom.sceneHanding!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        </#list>
                    </w:tbl>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="240" w:lineRule="atLeast"/>
                            <w:ind w:hanging="735"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t>注：1）检查种类是指上岗前、在岗期间、离岗时、应急、离岗后医学随访、复查、医学观察、职业病诊断等；</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="240" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="405"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t>2）检查结论是指未见异常、复查、疑似职业病、职业禁忌证、其他疾患、职业病等；</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="240" w:lineRule="atLeast"/>
                            <w:ind w:hanging="315"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>3）人员处理情况是指调离、暂时脱离工作岗位、复查、医学观察、职业病诊断结果等处理、安置情况及检查、诊断结果；检查结论为未见异常或其他疾患的划“——”；</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="240" w:lineRule="atLeast"/>
                            <w:ind w:firstLine="420"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t>4）现场处理情况是指造成职业损害的作业岗位，现场及个体防护用品整改达标情况，不需整改的可划“——”</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00C9055E" w:rsidRDefault="00C9055E">
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:br w:type="page"/>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>表6-</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>4</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00777BA5">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>培训记录表</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>劳动者姓名：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${name!''}</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>                       档案号：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${archiveNumber!''}</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="9559" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:left w:w="0" w:type="dxa"/>
                                <w:right w:w="0" w:type="dxa"/>
                            </w:tblCellMar>
                            <w:tblLook w:val="0000"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="1526"/>
                            <w:gridCol w:w="1276"/>
                            <w:gridCol w:w="1957"/>
                            <w:gridCol w:w="3827"/>
                            <w:gridCol w:w="973"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00777BA5" w:rsidTr="00777BA5">
                            <w:trPr>
                                <w:trHeight w:val="957"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1526" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>培训时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1276" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>培训时长</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1957" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>培训地点</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3827" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>培训主题</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="973" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>讲授人</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <#list employeeTrainingList as employeeTraining>
                        <w:tr w:rsidR="00777BA5" w:rsidTr="00777BA5">
                            <w:trPr>
                                <w:trHeight w:val="705"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1526" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeTraining.trainingDate!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1276" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeTraining.duration!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1957" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeTraining.place!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3827" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeTraining.theme!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="973" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00777BA5" w:rsidRDefault="00777BA5" w:rsidP="00777BA5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeTraining.lecturer!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        </#list>
                    </w:tbl>
                    <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5"/>
                    <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:br w:type="page"/>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>表6-</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>5</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00EE0CB5">
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="32"/>
                            </w:rPr>
                            <w:t>防护用品发放表</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                        <w:pPr>
                            <w:widowControl/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>劳动者姓名：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${name!''}</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>                       档案号：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                            </w:rPr>
                            <w:t>${archiveNumber!''}</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="8472" w:type="dxa"/>
                            <w:tblInd w:w="-103" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:left w:w="0" w:type="dxa"/>
                                <w:right w:w="0" w:type="dxa"/>
                            </w:tblCellMar>
                            <w:tblLook w:val="0000"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="2376"/>
                            <w:gridCol w:w="1843"/>
                            <w:gridCol w:w="2585"/>
                            <w:gridCol w:w="1668"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00EE0CB5" w:rsidTr="00EE0CB5">
                            <w:trPr>
                                <w:trHeight w:val="957"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2376" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>岗位</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1843" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>发放时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2585" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>防护用品名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1668" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>数量</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>

                        <#list employeeProvideProtectList as employeeProvideProtect>
                        <w:tr w:rsidR="00EE0CB5" w:rsidTr="00EE0CB5">
                            <w:trPr>
                                <w:trHeight w:val="705"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2376" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeProvideProtect.postName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1843" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeProvideProtect.provideDate!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2585" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeProvideProtect.provideArticlesName!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1668" w:type="dxa"/>
                                    <w:tcMar>
                                        <w:top w:w="0" w:type="dxa"/>
                                        <w:left w:w="108" w:type="dxa"/>
                                        <w:bottom w:w="0" w:type="dxa"/>
                                        <w:right w:w="108" w:type="dxa"/>
                                    </w:tcMar>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00EE0CB5" w:rsidRDefault="00EE0CB5" w:rsidP="00EE0CB5">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>

                                            <w:bCs/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                        <w:t>${employeeProvideProtect.number!''}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        </#list>
                    </w:tbl>
                    <w:sectPr w:rsidR="00C9055E">
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992"
                                 w:gutter="0"/>
                        <w:cols w:space="720"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/footnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml">
        <pkg:xmlData>
            <w:footnotes xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
                <w:footnote w:type="separator" w:id="0">
                    <w:p w:rsidR="00E93CF7" w:rsidRDefault="00E93CF7" w:rsidP="00601CBF">
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:footnote>
                <w:footnote w:type="continuationSeparator" w:id="1">
                    <w:p w:rsidR="00E93CF7" w:rsidRDefault="00E93CF7" w:rsidP="00601CBF">
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:footnote>
            </w:footnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/endnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml">
        <pkg:xmlData>
            <w:endnotes xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
                <w:endnote w:type="separator" w:id="0">
                    <w:p w:rsidR="00E93CF7" w:rsidRDefault="00E93CF7" w:rsidP="00601CBF">
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:endnote>
                <w:endnote w:type="continuationSeparator" w:id="1">
                    <w:p w:rsidR="00E93CF7" w:rsidRDefault="00E93CF7" w:rsidP="00601CBF">
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:endnote>
            </w:endnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image2.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${photo!''}</pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme name="Office 主题" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="FFFFFF"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="1F497D"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="EEECE1"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4F81BD"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="C0504D"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="9BBB59"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="8064A2"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="4BACC6"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="F79646"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0000FF"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="800080"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="Cambria"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="ＭＳ ゴシック"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="宋体"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="Calibri"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="ＭＳ 明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="宋体"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="50000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="35000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="37000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="15000"/>
                                            <a:satMod val="350000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="16200000" scaled="1"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="51000"/>
                                            <a:satMod val="130000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="80000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="93000"/>
                                            <a:satMod val="130000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="94000"/>
                                            <a:satMod val="135000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="16200000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="9525" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr">
                                        <a:shade val="95000"/>
                                        <a:satMod val="105000"/>
                                    </a:schemeClr>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                            <a:ln w="25400" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                            <a:ln w="38100" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="20000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="38000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="35000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="35000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                                <a:scene3d>
                                    <a:camera prst="orthographicFront">
                                        <a:rot lat="0" lon="0" rev="0"/>
                                    </a:camera>
                                    <a:lightRig rig="threePt" dir="t">
                                        <a:rot lat="0" lon="0" rev="1200000"/>
                                    </a:lightRig>
                                </a:scene3d>
                                <a:sp3d>
                                    <a:bevelT w="63500" h="25400"/>
                                </a:sp3d>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="40000"/>
                                            <a:satMod val="350000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="40000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="45000"/>
                                            <a:shade val="99000"/>
                                            <a:satMod val="350000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="20000"/>
                                            <a:satMod val="255000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:path path="circle">
                                    <a:fillToRect l="50000" t="-80000" r="50000" b="180000"/>
                                </a:path>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="80000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="30000"/>
                                            <a:satMod val="200000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:path path="circle">
                                    <a:fillToRect l="50000" t="50000" r="50000" b="50000"/>
                                </a:path>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image1.jpeg" pkg:contentType="image/jpeg" pkg:compression="store">
        <pkg:binaryData>/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsK
            CwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQU
            FBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCACuAXYDASIA
            AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
            AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
            ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
            p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
            AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
            BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
            U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
            uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KK
            KACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoooo
            AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigB
            MYoIrN0rxBpmsy3UdhqNteS20rwzxwyqzROrFWVgOQQQRz6VpVKaewbC0UUVQBRRUM08VtE8krrH
            Gg3MznAA9SaBXsSbqd1rH0HxNpfie2ln0nULfUYI5DEZraQOm4dQGHB+o/pWsvegSakrodRRRQUF
            FFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUU
            UUAFFFFABRRRQAUUlLQAwd64v4yfEW2+FHwz8Q+Krkof7PtWaGN+ksx+WJP+BOVH412oI5r4K/4K
            VfFXfNoHw+spvlT/AImuoKpPXlIEP/kRiD6oauKu7HpZbhHjMVCl06+i3PmP4W/GbVPCXiqe7vNQ
            uQl/cNPPdxSFZIpnOWlBGDyT82Pr2wft/wAEftPa7pccI1NYfEFiwBWYEJLt7EOOG49Rz61+aYGT
            gcmvS/hr8ZYvCkR0bWZDLAOLbaQXibP3G9F54z09x0+czfA4lP61gJNTW6WzXe22h9bxJllKNP61
            SajJbruu6XdH6qeDvjd4U8ZbIoL8WN63H2W+xE5PoDna34HPtW94s8e+H/A1kbvX9XtdMhwSomk+
            d/8AdQfMx9gDX5bax8TNSv8AeloFsYj3T5nP4n+grn/7dvnlMk1zJcuepnYuT+J5rPL8xrOKWNik
            +6/VH4piM8UU1Rjd93ov8z7h8fftw2Vt5lt4Q0dr1xwL/UspHn1WNTuYfUr9K+bvHHxb8W/ESRjr
            2t3F3AW3LaIfLt15yMRrhePUgn3rz+11uGU4kBiY+vI/OtCN1kUMpDKehFfbYWphqivSaf5nxWMz
            DG4nSrLTstF/wT7E/ZC186VpGnafISsN8kqjP/PRZXKn8RkfiK+pVbANfF/w/lfTfDegzQMUligh
            lRs5w2AwP519f+HtXi13RbTUIvuzxh8f3T0Yfgcj8K/O8jzT61i8XhJvWM5OP+Ft/kfr1Cj7LCUW
            v5UvnY1BRQKK+yLCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKK
            KKACiiigAooooAKKKKACiiigAoopM0AJRu4PNed+P/j54K+HCyx6rq8c1+nH9n2WJZ8+hUHC/wDA
            iK+bPH/7bHiDV/NtvCthFodseBdXGJrg+4B+RfphvrW0KU57I8jF5nhsH8crvstX/wAA+ufFnjHR
            fA2iXOra9qdtpVhbozvNcyBRgAkgA8sfYcmvxc+Mnxg/4WZ8R/EPimfzJ5dRumlijzgQxD5Y4yfV
            UCLwMHHWuw+PnxE1bVdLY6pql1qWp6gxjMtzMzssY5bGc4HQYGBzxXz5XSqKpvVn0fDuYVatKeJp
            x5VLRdXZb+l/0L91rd1dArvEMZBBSL5QQRggnqQfQnFUKKK0PdlOVR803d+Z6R4E8W/bUXTrx83C
            D91Ix5kHofcfrXZ14NG7ROroxR1OVZTgg+or2LwprieINISbkXEWI5wf72PvD2OM+3T0z8rmWC5H
            7amtHv8A5n5jn+Uqg3iqK917rs+68ma9SQXEluxMblD7HrUdFeBGThrF2Z8OtT6Q8HfEGTT9F0+1
            vbcSJFbxp5kXDcKByDwf0r6e/Z5+IenaxbXWjJeRtKp8+CJztcg/fAB9CAfxNfG0KeXEif3VA4q3
            p101leRyo5jKn7ykgj349Ov4V8ThZxwGOeNprXW6vun/AFc/R6GPqRpqjPVafI/SoUHivjvwf+0J
            4s8LFYrq4Gs2a8GK9JMgHtJ9788/SvcPB/7RPhXxMEiu5jol2esd4QIyfaTp/wB9ba/ScHnuDxdl
            zcsuz0/HY9NTTPVaKihmS4jWSN1eNhlWU5BHqDUtfRJp7FhRRRTAKKKSgBaKSloAKKKKACiiigAo
            oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooARelZuu6PBr+i6hpl0GNtewPb
            yhG2nY6lTgjocHrWl0FGRQCbTuj8bvG1p4g+HPjHWvDd9dObvTbl7ZmkQHeFPyuM54ZcMPYisiLx
            fqaNlpUl9mjAH6Yr6o/4KJfDH+yfFWjeN7WLEGqJ9hvGUcCaMZjY+7Jkf9shXxte3H2a2kcEBui5
            9ajnnHZn9AZdgsnzXLoYqrhYO6973Vut+ncz/GGpHxLqBe6jXdEvloULLt9cDOOuawE0izC/OJ2P
            qsgH/spq3RWXtp/zHmrLMFCPJTpKMVskrJfcZw0RC3+vZV/3Nx/mKkn8LzRXEkazx7UOMuCCT34A
            NXlUswUDJJwBX2l+yv8AsO3njaS28WfEK3lsdBJEtrpD5Se97h5O6R+33m9hgt1UK0nfmZ5OPwuX
            4Sk6tT3fR6v0TPnb4S/sd/EH4uWR1bRtK+1aCkhjN6kiQiVgeVj84puxyCRkAjHXivck/ZZ8eeHd
            MFlF4Ku0tVGNkGyXPudrEk+9fpHpmmWmj6fb2NjaxWdnbxrHFbwIEjjQDAVVHAAHYVcrf2zacWk0
            z8ZzXCRzOXvTlGK2V/xem5+Q/iLwD4j8K3a2+q6HqOnu43xi6tJIi65xkBgM9KzNLszNq9nbzK0Y
            kmRDkYIBYCv1f+I3w+sPiH4dl066IjmX57a5A+aGTsfcHoR3Hvgj4q8UeHbzwvqV/pWqQGG6tgys
            vUHjKsD3B4INfnWb1J4CpeMLxls77eWx85LI1Td1PT0OQooor4nc6djaspvOtlJ+8PlP4f8A1sVZ
            t4HuZVjjUu7HAArO0GJ7i6MK4CvgFicAHt/XivQNO02OxQJEu+RuC2OWP+e1eXVtGbSPscoyyrmC
            UnpBbv8AyPYf2ZdBvrT+0bqS9uGso1ES2/mN5JlPJIXpkDHOP4hXvuevtXOfD3w4PCnhWxsCgE4T
            zJ2HeRuW/Lp9AK6Sv2/KcNLC4OFOfxWu/V62+QsR7P2slSVorRfLr8wzjPauW8e/ELSPhxoiavrJ
            uVsjKIi9vC0pUkEgsB0Hy4ye5A7107fUUMgZSCAQexr2FbqcU1Nwapu0ujauvu0PHR+1t8MioJ1u
            dSR0NhPn/wBBrq/h18Y/C/xRa8Tw/etPNaYMkU0ZjfaejAHqO2a+QP2vdDsdE+LW2wtYbRbmwink
            SBAily8ilsDuQorzPwJ431P4e+J7PXdJmMd1bNyhPyyofvIw7qR/j1Ar144KFSlzwbuz8sqcWYvB
            Y94bFRi4Rdm0mnbvq362P1E69qo6tqcWjaXe6hOJHgtYXmkWJC7lVUsdqjknA4A61gfDP4h6Z8Tf
            Cdrrmludkg2SwscvBKPvI3uP1BB711eMjDc+teTZxdpH6hTqxr01UpO6aun0PG4f2uvhpKpLaxcw
            nONr2Muf0U1oeH/2l/AnirxDZaNpWoXN5e3jiOLbZyKuT6lgMD3ry39uDQ9Pt9A0DUYbKCO/kvGi
            e4SMB2XYxwSOvIHWvYfgBoWn6P8ACfwxJZWUFs9zp8E00kaANI7IGZmPUkknrXXKFJUlNJ6+Z8th
            8ZmNTMZ4Kco2gk20ndp9Lc2nqO8ffHbwn8NNag0zXrm5tbiaITIyWruhUkj7wHbHI69Kwv8AhrX4
            Y/8AQdm/8AJ//iK9V1TS7PVrGW2vrWG7t3GGinjDqfwNflrrUKW2sahDEuyOOeRFUdgGIArTC0ad
            dNO6aOHiHN8fk0ozpuMoyvZNO6t531PvB/2tvhsGjSLVLu4djgLFYy5z6cqP0ruPHHxM0T4eeH7f
            W9Ye4TT55EiEkMDOVLKWXcByBgHk98DvXjP7EOk2TeAdY1BrSFr7+1XiFwYwZAgihIXd1wCzHHua
            +kWRZVIdQwIwVPIIrnrRpwqciT0PdyqtjMbg1iKsoqU1dWTsvXXU8fj/AGuPhk6ZOtXCE/wtYzZ/
            RaSX9rn4ZRxll1q4lI/hWxmyfzUCvmT9rLRbDRfi9dR6faQ2Uc1pDM8cChVLncC2BwM4HT69TWl+
            xnpVnqnxYujeWsN0LfS5ZovOQN5b+ZEu4Z6HazDPua7vqtL2Xtddrnx0eIczeZf2d7l72vZ/fa59
            o+DvFlj438PWet6W0rWF2GaJpozGxAYrnaecZU49Rg1mfEX4qaF8LrO1u9deeKC5cojwwtKARjrj
            p1rrwu0YGAKingS5iaOVVkRhgqwyCPcV5ScebVaH6VJVXS5YySnbe2l/S/6nkS/tb/DFlz/bkw9j
            p8//AMRTJv2uvhnEmV1e4mOcbUsZc/qor40+Mun2+lfFTxXaWkMdvbRahIEiiXaiDdnAA6D2r3T9
            hvQbW8n8V6ldWUEzwG1jt55UVmjJ80uFJ5Gfkr1amFowpe11+8/NcFxDmWLzH+z/AHU7tXs3tfW1
            /I+q/D2u2viXRbPVbPzPst3Es0XmoUbaemVPINaeMimooAwKcDwfavJP1CN0lzbi0UUUiwooooAK
            KKKACiiigAooooAKKKKAPNv2hPhqPiv8JPEHh5I1a+kh8+zZjjE8fzR89skbT7Ma/G/XpGScWzBk
            aM/Op4wfQj1H9a/bfx54usfAXg3WvEeok/YtMtJLqQL1YKpIUe5IAHuRX4leLNZuPEniLUtaulRb
            jUrmW7lEa7UDuxZgB2GT07ZFZVE7XP0vhPE1lh61D7F0/n1X4IyKVFLHCgk+gp8MLTNtUZrVtLVb
            YAjl/wC9XmVsTCgry37H6FhsJPEPTRdzqPhF4Ziv/iV4RtLlFnFzq1pCYmUOuHmRSCO/Wv2qT7g+
            gr8jP2XdOTXfj74Ht2jDbNRjnIxnmIGQHHtsB9q/XQcDFd2HqRqw5on53xvGNHE0aMekb/e/+AOo
            oorqPzYbXjP7S/gO08R+EG1SMCPVrRlSJwOZUJ5jY+nUj0OfU17Nn9a85+OVwYvCVvGP+Wl2gI9g
            rn+YFeBnko08urTkr2Ta9ehUaaqyUJbM+FpI2idkdSjqcFWGCDViysWujuPyxDq3qfQV2HjSw0+f
            UklQ/wCkj/XInRvTJ7H+npWQOgAAAHAA6CvxR47nppwVm/wO3A8PynVcq79xbd3/AJISFRbhRENg
            U5GPX1+te1fBLQB4t8U2c7putbRftMvpuB+Vf++sfgDXi1fRv7LtxHp8V/ZSjE96q3EZPouRt/Js
            /nXVkip1sxpRrvRv72tUvmz6/FVPqmFlCkraWVun9I+ggMDFLRRX9AHwImKWiigD4q/amtYr79ob
            w7bToJIJrezjkRujKbiQEH8DXn/x9+C9z8JfFBECtNoF8xeyuDzt9YnP95f1GD1yB6L+05/ych4W
            /wCuNl/6UyV9T+PPAelfEfwpeaHq0W+3nXKyLw8Mg+66HswP58g5BIr2VXdBQfRrU/KamTQzepjY
            rScZJxfy2fkz4M+BHxkvPhF4rW4JefQ7srHf2g7r2kX/AG1zkeoyO+R+huk6raa5ptrqFhOl1ZXM
            aywzRnKupGQRX5m/EPwDqnw18VXmg6rHiaE7o5gPkniP3ZF9jj8CCDyDXsH7LHx5PgnUo/CuuXB/
            sK9k/wBGnkPy2krHv6Ix6+hOe7GrxVBVY+1p7/mcHDWczy6u8txukb2V+j7ejPR/25/+RN8Of9f7
            f+i2r2L4If8AJIfB/wD2Crb/ANFrXjn7c5DeDPDmDn/Tzz/2zavY/gh/ySHwf/2Crb/0WtcM/wDd
            o+rPtcJ/yPcR/hj+h2p/1b/Svys8Qf8AIe1P/r5l/wDQzX6pn7j/AEr8rPEH/Ie1P/r5l/8AQzXT
            l28vkfN8d/wqHq/0Psv9h3/kmGr/APYYk/8ARMFfRI7V87fsO/8AJMNX/wCwxJ/6Jgr6JHQVw4n+
            NI+04f8A+RXQ/wAJ8G/tk/8AJZJP+vCD+bVf/YkOPizqXGf+JNL/AOjoKo/tk/8AJZJP+vCD+b1y
            HwM1fxno3i26n8DWCajq7WTpJE6BgIfMjLNgkfxBB+Neyo82FS8j8nqVlh+I5VWm7SeiV38kfpDn
            2pD0PFfKv/CfftGf9CtB/wCAyf8Axyk/4T79oz/oVoP/AAGT/wCOV4/1d/zL7z9Q/t6l/wA+an/g
            DPAfjt/yWDxfnr/aEn86+hP2E/8AkB+LB/08Qf8AoLV8v+PbvV73xnrFxr8AttZkuHa6hChQkmeR
            gE4/OvqH9hP/AJAXiz/r5g/9BavXxCthreh+bcPz9pxA5pWu5PXfrufUoGKWiivnj9xCiiigAooo
            oAKKKKACiiigAooooAKKKhlmS3ieR2VEQFizHAAHUk0AfGf/AAUl+Kg0nwho/gK0lH2nVpBfXyjq
            tvE37sH/AHpBkf8AXI18D6d4R1DWdIvtQhtz9jtAGeYjryAQPXg5PpivePiHBdftB/GHXfGepM8f
            h2SfydOiJIaW1j+WIgHBVWA3nvlzjrmuvi0q0h01rBIESzMZiMSrhdpGCMfSvjs44jo4L/ZqHvT6
            9l5ep+15Jg1g8LCElru/V/1Y+UIYEgXCj6n1qStDxBpEmga1eafLndBIVBP8S9VP4gg/jWfXme1d
            Vc7d7n6hSUVBcmx2/wAFbi5sPiVo19aStFcWbtcRyr1RlQ7T+eOtfqr8Ivifa/EjQRK22HVbYBLu
            3Bxg9nX/AGTg/TkfX8sPhHH9n1O91EDc1uixhT0O/Oc/gpr3Twb8WL/wRrttqunQ7bmI4dDIdkqH
            qjDHQ/0B7VxU80qYDGrrTaSa/VeaPg+Jso/tVXgvfitH+j8n+B+jQPXNL+NeG+HP2ptO13T47r+x
            5UDDDLHOHKN6HIFbg/aL8NxxtJcW9/bRqMl3RCo/J/6V9nDPMum7Kqvndfmfi88ox1N8sqTv9/5H
            qbOsaszHCgZJJwAK+bP2gfijbaultpOkSb44pGMl0OjHGMJ7DJ5/L1rL+In7RVt4uMllp88tjpJG
            GDIRJN/vYzgew/HPbyzXNUtLxYWhuI3C7sjdgjp2NfD8QZ8sTTlhMMrxe77+nkfRYDIqtKPt68Xf
            orbepnE5OTyaKE/eDKfMPVeaCCDgjBr84SaPacJR3Ra0uyOoX0MAzhm+YjsO9eseG9Wfw9rNlfQj
            m2kDbR3Xoy/iCR+NcR4LsNqS3bDlvkTPp3P8vyrp645V50a0ZwdnFpr13PBxklUfJ0R9bW1xHeW0
            c8TB4pFDow6FSMg1PivPPgx4h/tTwybKRiZ7BtnPUxnJU/zH4V6FnJPtX9L5di44/CU8TDaSv6Pq
            vvPjqkHCTi+g6iiivSMz4w/ad/5OR8Lf9crL/wBKXr7Nj+4PoK+Mv2nf+TkfC3/XKy/9KXr7NT7g
            +ldtf+HD0Pj8l/33G/4keXfHz4NW3xc8KNFEqw65ZBpLC5PA3d42P91sD6HB7YP57alpl1o2o3Nh
            fQPa3ltI0U0MgwyMDgg1+rXtXzf+1V8B18XadL4u0G2zrdpH/plvGObuJQeQO7qPxIGOSAK2weJ9
            m/Zz2Z5PFWQ/XKbxuGX7yK1XdL9UfN3ir4vXvjP4Y6L4a1UvPfaTd7oLsnJkg2MoV/8AaXgA9xjP
            Iyfur4I/8kh8Hf8AYKtv/Ra1+aVfpb8ER/xaLwd/2Crb/wBFitsdBQglHueTwdiauJxdWVZ3ailf
            yTsjtj9x/pX5WeIP+Q9qf/XzL/6Ga/VM/cf6V+VniD/kPan/ANfMv/oZpZdvL5HRx3/Coer/AEPs
            v9hz/kmGsf8AYYk/9EwV9EjtXzx+w/E6fC/VmdSofV5WViMbh5UIyPxBH4V9DjtXBif40j7Th/8A
            5FdD/CfBv7ZP/JZJP+vCD+bVf/Yj/wCStal/2B5f/R0NUP2yf+SySf8AXhB/Nqv/ALEf/JWtS/7A
            8v8A6Ohr13/unyPy6H/JT/8Ab7PuTFBHymlpD0NfPo/cD81fjt/yWHxf/wBhCT+dfQv7CX/ID8Wf
            9fEH/oLV89/Hb/ksPi//ALCEn8699/YRu0aw8YWwB3xy20hPbDLIB/6Cf0r6DEf7qvRH4jkjS4jl
            f+af6n1bRRRXz5+3hRRRQAUUUUAFFFFABRRRQAUUUUANFZfiPQ4PEuh3ulXLypbXcZhlMLbWKHhg
            D2yMg+xNanGKWk0mrME2mmt0fL/i/wDZVv8AT0aXw1fLfxLyLW6xHL9A4+Vj9dteM674b1Xwxdm1
            1awuLCfnCzoV3e6noR7iv0E9apapotjrlm9rf2cF7bv96KeMOp/A18RjuFcNiLzoNxl96/z/ABPs
            cFxNiaFo11zrvs/8vwPyY+OPh/ZPZ6xEnyuPs8xA7jlT+WR+Aryiv1C+MP7IOieNPDmpW+gzvpN1
            LGWihkJkg8wZK9fmX5sc5PGeK/Mi/wBLutMvLi1uYmiuYJGiliYYZHU4ZSOxBBB+lcNHL8Vg6Sp1
            le3Vapo/ZeH85w+aUXGk9Y7p7q+39I7f4VAiz1YlTtaSEBscHAkyM/iPzrtq5X4aD/inJT63bj/x
            xK6qvlcY715HpVdakja8LeIpfD2oBwS1tIQssfqPUe4re1nXJ9YlBb5IBykanI+ue9cPWlpd8FxD
            K2F/gY9B7GvLqQv7yOCpRi5e0S1NSiiiuYzCnwK8kyJHnzGIVcHHJpYLeW6kCQxtI5/hUZNes/BT
            4KXvjLWZbm8mFlZWihnKje5ZsgKO3QE55xxxXRh8PUxVVUaSvJ7HBi8XQwdKVWs7JFawjaytIYFk
            YiNQM5PJ7mu48MfD3xR4jVWgtGtrY/8ALxdqEXHqMjc34A17p4Y+GHh/wvte2shPcLg/aLn53z6j
            PC/gBXXAACvusFwhH48ZL5L9Wz8pxmexm2qFNerX6HCfD34af8IVPJdSahJeXUkfluAoSMDIPTkk
            8dc/hzXdd+KAOvcUu3rnkV9/hMJRwVJUaEbRXQ+Tq1JVZOc9xOa5fx7rPiTRdDE/hfQI/EWpGUKb
            WS8W3CqQctubg4IA25HXrxg9SB+NGOtdqdnsc1SDnBxUmm+q3Xpe58Q+O/hX8a/H/jZPFF94Xjhv
            4jGIEt7u28uERnKgBpSTzknJ5JPbivpb4WeJPiHqrzW/jbwlBonlxBo722vI5ElbIBXy1dyvHOc4
            4r0Yc46g04cCuipWdSKi4rQ8LBZNDA1pV6daTcndptWb89PyFHSggEEY4paK5j6I+Pv2gf2W9Wu/
            FLa14J037XbX7FrqwjdE8mXqWXcQNrenY57EY+lPhZo174e+HPhvS9Rg+zX1np8ME8RZW2uqAEZU
            kHnPINdUT6UDvXRKtOpBQl0PDwmUYbBYmpiqN057rp8hjuArc44zzX5Vatcpe6pe3EefLlmeRc9c
            FiRX6E/tBfE+0+GvgC/kM6/2tfRPb2EAYby7DBfH91M5J+g718CeEPCOq+ONftdF0e1a7vblsKo+
            6o7sx7KOpNelgI8sZTlsfn/GlVYmvRwdL3pq7st9bW/I+2/2ObGS1+C9rM4+S5vJ5U47Btn80Nei
            fELWvE2iaKk3hbw6niPUZJhG1s92lusSFWPmEuQGwQo2gg/NntVvwD4Tt/Ang7StBtm3w2MCxb8Y
            3t1ZvxYk/jXQDuB3rypzUqjlbqfo2CwsqOBp4ZycZKKV1a6dtbXTR8NfEf4NfGX4neKLjXtW8KRp
            cyIsaxW95bLHGijAUZlJ9Tkk8k9sCrHwr+Evxk+FHij+2tL8IxXMrQNbyw3F7blXjZlJGRKCDlFI
            Pt36V9vDpR+ldX1yfLycqt8/8z59cK4VYj62qs+e973V7/cYfhDUdY1Xw7aXeu6SND1WQN52ni5W
            cREMQPnXg5UBuOmcdqxviVrvjDQ9MgPhDw3F4jvZWZZPOvEgWAAcMQxG/J7AjGK7XsaTuOa4U0ne
            3yPrZUpSpezU2na19L+u1r/I+BfEv7PHxc8V6/qOs33hgG7vpnuJfLvbYKGY5wB5vAHSuz+DPgT4
            z/BvUL+Ww8FwX9tfiMXME9/bgnZu2lWEnyn5m7Ec9K+x+gPrSgYFd8sZOUeRxVv68z5KhwphcPX+
            s06s1O7d7rd79OpmeHb2/wBR0azuNU07+yr+SMNPZecs3kv3XevDfUVqdBSYPal9a4D7OKcUk3cW
            iiikWFFFFABRRRQAUUUUAFFFFABRRRQAmKWiigBpA54r8yP25/hh/wAIL8Y5tWtotmmeIozepgYU
            TDCzr9c7XP8A11r9NwOtfP8A+2t8Mh8QfgpqN3bRCTVNBb+0oCOpRQfOX8Y8tjuUWlJXVj6rhjMf
            7OzKEpP3Ze6/ns/k7HyJ8FfA1nqvw+FzdB1mubqWRJkbkAYTp06oetamq/DjULLL2jLexei/K4/A
            9fwNbPwRBX4W6ECpUhZuvfM8hB/Iiu9trWa8mWC3hknmc4WOJSzMfYCvwfMaspY6qoa+81+Nj9Wr
            46pSrz10Te/qeATQSW8jRyxtFIvVHUgj8DUdfXWlfs66l4uiU61bw6fan/nuN02PZQePxINLr/7H
            NrZRGbw5qPmTryYtSGQx/wBl1Hy+w2n616VLKMfUpOqqT9HZN+iZx/6z4CMvZzlZ+Wq+8+adAtLv
            VdkPl7Wx8skh2hh/U/T/APX2en+CIIsNdyGZv7ifKv8Aj/KrHibwL4g8Fz7dW024sgD8s+N0THtt
            dcqT7ZzV3RtUGoQ7XwJ0HzAfxD+9/jXzVWNWnNwnFxfZ7nRVxbrQ9pRkuV9tfxLltaw2keyGJYl9
            FGK+qPhN4YPhnwfaRyoFurn/AEibIwQW6A/RcD65rwP4a+Gv+Ep8XWVoy7reNvOnyOPLXkg/U4X8
            a+rlGMY7V+g8JYK/Pi5ryX5s/OOIcW244dPzf6D6KKK/Sz4oKKKKACiiigAooooAKKKKAGkVR1iO
            8uNJvY9OmS2v2hcW80q7kSTB2lh3AOMitCkzQS1dWPmcfsi3/i3WX1fx541u9YvJMb0tIQmB/dVm
            yAvsEAFe1+A/hl4b+HFg9p4f0yOxD482bl5ZT/tuck9+M4GeAK6sDPOKOmcVrKrOa5W9Dy8NleDw
            k3Vpw997yer+9jh0paKKyPWCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiig
            AooooAKhmgS5hkikVXjdSrKwyCD1BFTUUAtNUeI+FP2ZdK0KNLWS8lfTLY7LW1i4IjB+UO5yScYz
            j869U0LwtpXhq38rTbCG2GMFlX5m/wB5jyfxNbA70cV5tDL8Nh5SqU4JSbu3u9fNnbXxuIxP8WbY
            opaKSvSOIr3FrDeQPDPEk0LjaySKGVh6EHrXl/in9nXw3rUpu9K36BfqSVe1G6In3jPGPZSK9WUY
            GOKCMjFcWJweHxceSvBSXn+j3R04fFV8M+ajNx/rqup5/wDCv4by+Bk1CW7khnup3Cq8OdojHI68
            gkk5HPQcmvQR60D2pc9arDYalhKSo0VaKIrVp4io6lR3bFooorrMQooooAKKKKACiiigAooooAKK
            KKACkxS0UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFF
            FFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUU
            UAFFFFABRRRQAUUUUAFFFFABRRRQB//Z
        </pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main">
                <w:zoom w:percent="100"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:stylePaneFormatFilter w:val="3F01"/>
                <w:doNotTrackMoves/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:hdrShapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="3074"/>
                </w:hdrShapeDefaults>
                <w:footnotePr>
                    <w:footnote w:id="0"/>
                    <w:footnote w:id="1"/>
                </w:footnotePr>
                <w:endnotePr>
                    <w:endnote w:id="0"/>
                    <w:endnote w:id="1"/>
                </w:endnotePr>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:useNormalStyleForList/>
                    <w:doNotUseIndentAsNumberingTabStop/>
                    <w:useAltKinsokuLineBreakRules/>
                    <w:allowSpaceOfSameStyleInTable/>
                    <w:doNotSuppressIndentation/>
                    <w:doNotAutofitConstrainedTables/>
                    <w:autofitToFirstFixedWidthCell/>
                    <w:displayHangulFixedWidth/>
                    <w:splitPgBreakAndParaMark/>
                    <w:doNotVertAlignCellWithSp/>
                    <w:doNotBreakConstrainedForcedTable/>
                    <w:doNotVertAlignInTxbx/>
                    <w:useAnsiKerningPairs/>
                    <w:cachedColBalance/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="000A18ED"/>
                    <w:rsid w:val="000512CB"/>
                    <w:rsid w:val="000A18ED"/>
                    <w:rsid w:val="00125B05"/>
                    <w:rsid w:val="001E320B"/>
                    <w:rsid w:val="00251742"/>
                    <w:rsid w:val="0029098B"/>
                    <w:rsid w:val="002A3C43"/>
                    <w:rsid w:val="00337F33"/>
                    <w:rsid w:val="0035262F"/>
                    <w:rsid w:val="003C48E3"/>
                    <w:rsid w:val="003E3F4A"/>
                    <w:rsid w:val="004276BC"/>
                    <w:rsid w:val="004B05B5"/>
                    <w:rsid w:val="004D2751"/>
                    <w:rsid w:val="005767C7"/>
                    <w:rsid w:val="005C62BB"/>
                    <w:rsid w:val="00601CBF"/>
                    <w:rsid w:val="006A7EF6"/>
                    <w:rsid w:val="008A35BD"/>
                    <w:rsid w:val="00996EC6"/>
                    <w:rsid w:val="009978A4"/>
                    <w:rsid w:val="009D61E9"/>
                    <w:rsid w:val="00AA5686"/>
                    <w:rsid w:val="00AB40BD"/>
                    <w:rsid w:val="00AF6AF8"/>
                    <w:rsid w:val="00B608A3"/>
                    <w:rsid w:val="00C25F6A"/>
                    <w:rsid w:val="00C43448"/>
                    <w:rsid w:val="00C9055E"/>
                    <w:rsid w:val="00D44BCD"/>
                    <w:rsid w:val="00E3463D"/>
                    <w:rsid w:val="00E93CF7"/>
                    <w:rsid w:val="00EB30B5"/>
                    <w:rsid w:val="00F47F8A"/>
                    <w:rsid w:val="00F53206"/>
                    <w:rsid w:val="0F4A0F6A"/>
                    <w:rsid w:val="27AF23E5"/>
                    <w:rsid w:val="42CD3269"/>
                    <w:rsid w:val="4DE46735"/>
                    <w:rsid w:val="552A13AB"/>
                    <w:rsid w:val="56462356"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="off"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:uiCompat97To2003/>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1"
                                    w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5"
                                    w:accent6="accent6" w:hyperlink="hyperlink"
                                    w:followedHyperlink="followedHyperlink"/>
                <w:doNotIncludeSubdocsInStats/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="3074"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="1"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                     xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002AFF" w:usb1="C0007841" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:altName w:val="SimSun"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="仿宋">
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Cambria">
                    <w:panose1 w:val="02040503050406030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E00002FF" w:usb1="400004FF" w:usb2="00000000" w:usb3="00000000" w:csb0="0000019F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Calibri">
                    <w:panose1 w:val="020F0502020204030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E10002FF" w:usb1="4000ACFF" w:usb2="00000009" w:usb3="00000000" w:csb0="0000019F"
                           w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                           xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
                <w:divs>
                    <w:div w:id="830756635">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                    </w:div>
                    <w:div w:id="1442913944">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                    </w:div>
                </w:divs>
                <w:encoding w:val="x-cp20936"/>
                <w:optimizeForBrowser/>
                <w:allowPNG/>
                <w:targetScreenSz w:val="1024x768"/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>7</TotalTime>
                <Pages>5</Pages>
                <Words>203</Words>
                <Characters>1162</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <Lines>9</Lines>
                <Paragraphs>2</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company>MC SYSTEM</Company>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>1363</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>12.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml"
              pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                               xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/"
                               xmlns:dcmitype="http://purl.org/dc/dcmitype/"
                               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title>附件1 档案编号：</dc:title>
                <dc:creator>MC SYSTEM</dc:creator>
                <cp:lastModifiedBy>Windows 用户</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <dcterms:created xsi:type="dcterms:W3CDTF">2019-08-06T01:03:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2019-08-06T01:03:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                      xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman"
                                      w:cs="Times New Roman"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="0" w:defSemiHidden="0" w:defUnhideWhenUsed="0"
                                w:defQFormat="0" w:count="267">
                    <w:lsdException w:name="Normal" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="Title" w:qFormat="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1"/>
                    <w:lsdException w:name="Subtitle" w:qFormat="1"/>
                    <w:lsdException w:name="Strong" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:qFormat="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:uiPriority="99" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:uiPriority="99"
                                    w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:uiPriority="99" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:uiPriority="99" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:uiPriority="99" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:uiPriority="99" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1" w:uiPriority="99" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="99" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1" w:uiPriority="99" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="99" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="99" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="99" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                    <w:rPr>
                        <w:kern w:val="2"/>
                        <w:sz w:val="21"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:semiHidden/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:semiHidden/>
                    <w:tblPr>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="Char">
                    <w:name w:val="页眉 Char"/>
                    <w:link w:val="a3"/>
                    <w:rPr>
                        <w:kern w:val="2"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="Char0">
                    <w:name w:val="页脚 Char"/>
                    <w:link w:val="a4"/>
                    <w:rPr>
                        <w:kern w:val="2"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a3">
                    <w:name w:val="header"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="Char"/>
                    <w:pPr>
                        <w:pBdr>
                            <w:bottom w:val="single" w:sz="6" w:space="1" w:color="auto"/>
                        </w:pBdr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="center"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                        <w:lang/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a4">
                    <w:name w:val="footer"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="Char0"/>
                    <w:pPr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="left"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                        <w:lang/>
                    </w:rPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/custom.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.custom-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/custom-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <property fmtid="{D5CDD505-2E9C-101B-9397-08002B2CF9AE}" pid="2" name="KSOProductBuildVer">
                    <vt:lpwstr>2052-10.1.0.6877</vt:lpwstr>
                </property>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>