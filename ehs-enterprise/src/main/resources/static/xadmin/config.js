layui.define(function(exports) {
  exports('conf', {
    //容器ID
    container: 'xadmin',
    containerBody: 'xadmin-body',
    v: '2.0',
    //记录xadmin文件夹所在路径
    base: layui.cache.base,
    css: layui.cache.base + 'css/',
    //视图所在路径
    views: '/ehs/views/',
    viewLoadBar: true,
    debug: layui.cache.debug,
    //网站名称
    name: 'ehs',
    //默认视图文件名
    entry: '/index',
    //视图后缀名
    engine: '',
    eventName: 'xadmin-event',
    //本地存储表名
    tableName: 'xadmin',
    //request 基础URL
    requestUrl: './'
  })
});
