package com.lanware.ehs.shiro.authentication;

import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.exception.SysMaintainceException;
import com.lanware.ehs.gov.maintaince.service.MaintainceService;
import com.lanware.ehs.gov.user.service.UserService;
import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.SysMaintain;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 自定义实现 ShiroRealm，包含认证和授权两大模块
 *
 */
@Component
public class ShiroRealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;

    @Autowired
    private MaintainceService maintainceService;

    /**
     * 授权模块，获取用户角色和权限
     *
     * @param principal principal
     * @return AuthorizationInfo 权限信息
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
        return null;
    }

    /**
     * 用户认证
     *
     * @param token AuthenticationToken 身份认证 token
     * @return AuthenticationInfo 身份认证信息
     * @throws AuthenticationException 认证相关异常
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws EhsException {
        // 获取用户输入的用户名和密码
        String userName = (String) token.getPrincipal();
        String password = new String((char[]) token.getCredentials());

        // 通过用户名到数据库查询用户信息

        MonitorUser user = this.userService.findByName(userName);

        if (user == null) {
            throw new UnknownAccountException("用户名或密码错误！");
        }else if (!StringUtils.equals(password, user.getPassword()))
            throw new IncorrectCredentialsException("用户名或密码错误！");
        else if (user.getStatus() == 1)
            throw new LockedAccountException("账号已被锁定,请联系管理员！");

        //查询系统当前是否在维护中
        SysMaintain sysMaintain = maintainceService.find();
        if(sysMaintain != null){
            Long startTime = sysMaintain.getStarttime().getTime();
            Long endTime = sysMaintain.getEndtime().getTime();
            Long currentTime = new Date().getTime();
            //在维护时间段，则抛出维护异常
            if(startTime < currentTime && currentTime < endTime){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                throw new SysMaintainceException("系统正在维护中！预计结束时间："+ format.format(sysMaintain.getEndtime()));
            }
        }
        return new SimpleAuthenticationInfo(user, password, getName());
    }

    /**
     * 清除当前用户权限缓存
     * 使用方法：在需要清除用户权限的地方注入 ShiroRealm,
     * 然后调用其 clearCache方法。
     */
    public void clearCache() {
        PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
        super.clearCache(principals);
    }
}