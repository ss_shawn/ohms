package com.lanware.ehs.gov.front.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.lanware.ehs.common.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * 视图显示Controller
 */
@Controller("systemView")
public class ViewController {
    @GetMapping(Constants.VIEW_PREFIX + "404")
    public String error404() {
        return "error/404";
    }

    @GetMapping(Constants.VIEW_PREFIX + "403")
    public String error403() {
        return "error/403";
    }

    @GetMapping(Constants.VIEW_PREFIX + "500")
    public String error500() {
        return "error/500";
    }

    @RequestMapping(Constants.VIEW_PREFIX + "index")
    public String pageIndex() {
        return "index/index";
    }

    /**
     * 系统布局页面
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "layout")
    public String layout() {
        return "index/layout";
    }

    /**
     * 修改密码
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "password/update")
    public String passwordUpdate() {
        return "user/passwordUpdate";
    }

    /**
     * 辖区数据汇总
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "dataSummary")
    public String dataSummary() {
        return "dataSummary/list";
    }

    /**
     * 企业详细信息汇报
     * @return
     */
    @GetMapping(Constants.VIEW_PREFIX + "detailReport/{enterpriseId}")
    public ModelAndView detailReport(@PathVariable Long enterpriseId) {
        JSONObject data = new JSONObject();
        data.put("enterpriseId", enterpriseId);
        return new ModelAndView("detailReport/list", data);
    }
}
