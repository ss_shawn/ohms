package com.lanware.ehs.gov.maintaince.service;

import com.lanware.ehs.pojo.SysMaintain;

public interface MaintainceService {
    /**
     * 查询系统维护时间
     * @return
     */
    SysMaintain find();
}
