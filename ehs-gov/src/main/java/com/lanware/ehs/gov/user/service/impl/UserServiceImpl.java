package com.lanware.ehs.gov.user.service.impl;

import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.gov.user.service.UserService;
import com.lanware.ehs.mapper.MonitorUserMapper;
import com.lanware.ehs.mapper.custom.MonitorUserCustomMapper;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.MonitorUserCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private MonitorUserCustomMapper userCustomMapper;
    @Override
    public MonitorUser findByName(String username) {
        return userCustomMapper.findByName(username);
    }


    @Override
    @Transactional
    public void updateLoginTime(String username) {
        MonitorUser user = new MonitorUser();
        user.setLastLoginTime(new Date());
        MonitorUserExample example = new MonitorUserExample();
        MonitorUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        userCustomMapper.updateByExampleSelective(user,example);
    }


    @Override
    @Transactional
    public void updatePassword(String username, String password) {
        MonitorUser user = new MonitorUser();
        user.setPassword(MD5Util.encrypt(username, password));
        user.setGmtModified(new Date());
        MonitorUserExample example = new MonitorUserExample();
        MonitorUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        userCustomMapper.updateByExampleSelective(user,example);
    }


}

