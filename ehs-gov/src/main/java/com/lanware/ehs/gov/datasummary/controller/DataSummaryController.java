package com.lanware.ehs.gov.datasummary.controller;

import com.google.common.collect.Maps;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.gov.datasummary.service.DataSummaryService;
import com.lanware.ehs.pojo.MonitorArea;
import com.lanware.ehs.pojo.MonitorUser;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description 辖区数据汇总controller
 */
@RestController
@RequestMapping("dataSummary")
public class DataSummaryController {
    /** 注入dataSummaryService的bean */
    @Autowired
    private DataSummaryService dataSummaryService;

    /**
     * 查询监管区域信息
     * @return
     */
    @GetMapping("getMonitorArea")
    public EhsResult getMonitorArea(){
        MonitorUser monitorUser = (MonitorUser)SecurityUtils.getSubject().getPrincipal();
        MonitorArea monitorArea = dataSummaryService.getMonitorAreaById(monitorUser.getAreaId());
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("monitorArea", monitorArea);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询监管企业数目
     * @return
     */
    @GetMapping("getMonitorEnterpriseNum")
    public EhsResult getMonitorEnterpriseNum(){
        MonitorUser monitorUser = (MonitorUser)SecurityUtils.getSubject().getPrincipal();
        Integer enterpriseNum = dataSummaryService.getMonitorEnterpriseNum(monitorUser.getAreaId());
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("enterpriseNum", enterpriseNum);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询监管企业涉害人员数，女工数，农民工数
     * @return
     */
    @GetMapping("getEnterpriseHarmEmployeeNum")
    public EhsResult getEnterpriseHarmEmployeeNum(){
        MonitorUser monitorUser = (MonitorUser)SecurityUtils.getSubject().getPrincipal();
        Map<String, Integer> harmEmployeeNum = dataSummaryService.getEnterpriseHarmEmployeeNum(monitorUser.getAreaId());
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("harmEmployeeNum", harmEmployeeNum);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询监管企业年度的各种类型数目
     * @param year 年度
     * @return
     */
    @GetMapping("getEnterpriseYearNum")
    public EhsResult getEnterpriseYearNum(Integer year){
        MonitorUser monitorUser = (MonitorUser)SecurityUtils.getSubject().getPrincipal();
        // 查询条件
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("areaId", monitorUser.getAreaId());
        condition.put("year", year);
        Map<String, Integer> enterpriseYearNum = dataSummaryService.getEnterpriseYearNum(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("enterpriseYearNum", enterpriseYearNum);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询监管企业危害因素检测时间分布
     * @param year 年度
     * @return
     */
    @GetMapping("getEnterpriseCheckTimeDistribution")
    public EhsResult getEnterpriseCheckTimeDistribution(Integer year){
        MonitorUser monitorUser = (MonitorUser)SecurityUtils.getSubject().getPrincipal();
        // 查询条件
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("areaId", monitorUser.getAreaId());
        condition.put("year", year);
        Map<String, Integer> checkEnterpriseNum = dataSummaryService.getEnterpriseCheckTimeDistribution(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("checkEnterpriseNum", checkEnterpriseNum);
        return EhsResult.ok(resultMap);
    }
}
