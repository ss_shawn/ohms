package com.lanware.ehs.gov.detailreport.service;

import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.GovernmentThreeSimultaneityCustom;

import java.util.List;
import java.util.Map;

/**
 * @description 企业详细信息汇报interface
 */
public interface DetailReportService {

    GovernmentEnterpriseBaseinfo getEnterpriseBaseInfo(Long enterpriseId);

    GovernmentEnterpriseYear getEnterpriseYearInfo(Map<String,Object> condition);

    List<GovernmentHarmReport> listEnterpriseYearHarmChecks(Map<String,Object> condition);

    GovernmentLastHarmCheck getEnterpriseLastHarmCheck(Long enterpriseId, Integer year);

    GovernmentLastMedical getEnterpriseLastMedical(Map<String,Object> condition);

    GovernmentLastTraining getEnterpriseLastTraining(Map<String,Object> condition);

    List<GovernmentProvideProtectArt> listEnterpriseProvideProtectArts(Map<String,Object> condition);

    GovernmentLastProvide getEnterpriseLastProvide(Map<String,Object> condition);

    List<GovernmentProtectiveMeasures> listEnterpriseProtectiveMeasures(Map<String,Object> condition);

    GovernmentServiceRecord getLastServiceRecord(Long enterpriseId, Integer year);

    GovernmentServiceInspection getServiceInspection(Long enterpriseId, Integer year);

    GovernmentServiceRectified getServiceRectified(Long enterpriseId, Integer year);

    List<GovernmentThreeSimultaneityCustom> listGovernmentThreeSimultaneitys(Map<String,Object> condition);
}
