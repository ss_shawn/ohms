package com.lanware.ehs.gov.front.web.service.impl;

import com.lanware.ehs.gov.front.web.service.EnterpriseBaseInfoService;
import com.lanware.ehs.mapper.custom.GovernmentEnterpriseBaseinfoCustomMapper;
import com.lanware.ehs.pojo.custom.GovernmentEnterpriseBaseinfotCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EnterpriseBaseInfoServiceImpl implements EnterpriseBaseInfoService {
    @Autowired
    private GovernmentEnterpriseBaseinfoCustomMapper baseinfoCustomMapper;
    @Override
    public List<GovernmentEnterpriseBaseinfotCustom> getMonitorEnterpriseByMonitorId(Long monitorId) {
        return baseinfoCustomMapper.getMonitorEnterpriseByMonitorId(monitorId);
    }

    /**
     * @description 根据条件查询该监管区域下的企业
     * @param condition 查询条件
     * @return java.util.List<com.lanware.ehs.pojo.custom.GovernmentEnterpriseBaseinfotCustom>
     */
    @Override
    public List<GovernmentEnterpriseBaseinfotCustom> getMonitorEnterpriseByCondition(Map<String, Object> condition) {
        return baseinfoCustomMapper.getMonitorEnterpriseByCondition(condition);
    }
}
