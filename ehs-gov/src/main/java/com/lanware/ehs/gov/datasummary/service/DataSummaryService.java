package com.lanware.ehs.gov.datasummary.service;

import com.lanware.ehs.pojo.MonitorArea;

import java.util.Map;

/**
 * @description 辖区数据汇总interface
 */
public interface DataSummaryService {

    MonitorArea getMonitorAreaById(Long id);

    Integer getMonitorEnterpriseNum(Long areaId);

    Map<String, Integer> getEnterpriseHarmEmployeeNum(Long areaId);

    Map<String, Integer> getEnterpriseYearNum(Map<String,Object> condition);

    Map<String, Integer> getEnterpriseCheckTimeDistribution(Map<String,Object> condition);
}
