package com.lanware.ehs.gov.user.controller;

import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.gov.user.service.UserService;
import com.lanware.ehs.pojo.EnterpriseUser;
import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.custom.EnterpriseUserCustom;
import com.lanware.ehs.pojo.custom.MonitorUserCustom;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

@Slf4j
@RestController
@RequestMapping("user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;

    @GetMapping("password/verify")
    public EhsResult verifyPassword(){
        MonitorUser user = (MonitorUser)getSubject().getPrincipal();
        String defaultPassword = MonitorUserCustom.DEFAULT_PASSWORD;
        if (StringUtils.equals(user.getPassword(), MD5Util.encrypt(user.getUsername(), defaultPassword))) {
            return EhsResult.build(500,"您的登录密码与默认密码一致，请尽快修改您的登录密码！");
        }
        return EhsResult.ok();
    }

    /**
     * 修改密码
     * @param oldPassword
     * @param newPassword
     * @return
     * @throws EhsException
     */
    @PostMapping("password/update")
    public EhsResult updatePassword(
            @NotBlank(message = "{required}") String oldPassword,
            @NotBlank(message = "{required}") String newPassword) throws EhsException {
        try {
            String userName = ((MonitorUser) SecurityUtils.getSubject().getPrincipal()).getUsername();
            MonitorUser user = userService.findByName(userName);
            if (!StringUtils.equals(user.getPassword(), MD5Util.encrypt(user.getUsername(), oldPassword))) {
                throw new EhsException("原密码不正确");
            }
            userService.updatePassword(user.getUsername(), newPassword);
            return EhsResult.ok();
        } catch (Exception e) {
            String message = "修改密码失败，" + e.getMessage();
            log.error(message, e);
            throw new EhsException(message);
        }
    }

}
