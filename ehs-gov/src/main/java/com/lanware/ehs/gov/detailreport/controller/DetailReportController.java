package com.lanware.ehs.gov.detailreport.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.lanware.ehs.common.format.ResultFormat;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.OSSTools;
import com.lanware.ehs.common.utils.StringUtil;
import com.lanware.ehs.gov.detailreport.service.DetailReportService;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.GovernmentThreeSimultaneityCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description 企业详细信息汇报controller
 */
@RestController
@RequestMapping("detailReport")
public class DetailReportController {
    /** 注入detailReportService的bean */
    @Autowired
    private DetailReportService detailReportService;

    /** 注入ossTools的bean */
    @Autowired
    private OSSTools ossTools;

    /**
     * 文件下载
     * @param filePath 文件路径
     * @param response response对象
     * @return 失败信息
     */
    @RequestMapping("/downloadFile")
    @ResponseBody
    public ResultFormat downloadFile(@RequestParam("filePath") String filePath, HttpServletResponse response) {
        try {
            ossTools.downloadFile(filePath.substring(filePath.lastIndexOf("/") + 1), filePath, response);
        } catch (IOException e) {
            e.printStackTrace();
            return ResultFormat.error("下载文件失败");
        }
        return null;
    }
    
    /**
     * 查询企业的基础信息
     * @param enterpriseId 企业id
     * @return
     */
    @GetMapping("getEnterpriseBaseInfo")
    public EhsResult getEnterpriseBaseInfo(Long enterpriseId){
        GovernmentEnterpriseBaseinfo enterpriseBaseinfo = detailReportService.getEnterpriseBaseInfo(enterpriseId);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("enterpriseBaseinfo", enterpriseBaseinfo);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业年度的各种统计信息
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @GetMapping("getEnterpriseYearInfo")
    public EhsResult getEnterpriseYearInfo(Long enterpriseId, Integer year){
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("enterpriseId", enterpriseId);
        condition.put("year", year);
        GovernmentEnterpriseYear enterpriseYearInfo = detailReportService.getEnterpriseYearInfo(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("enterpriseYearInfo", enterpriseYearInfo);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业年度的危害因素检测信息
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @RequestMapping("listEnterpriseYearHarmChecks")
    public EhsResult listEnterpriseYearHarmChecks(Integer pageNum, Integer pageSize, Long enterpriseId, Integer year
            , String sortField, String sortType){
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        // 查询条件
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("enterpriseId", enterpriseId);
        condition.put("year", year);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        List<GovernmentHarmReport> governmentHarmReports = detailReportService.listEnterpriseYearHarmChecks(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentHarmReports", governmentHarmReports);
        if (pageNum != null && pageSize != null) {
            PageInfo<GovernmentHarmReport> pageInfo = new PageInfo<GovernmentHarmReport>(governmentHarmReports);
            resultMap.put("totalNum", pageInfo.getTotal());
        }
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业最新危害因素检测信息
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @GetMapping("getEnterpriseLastHarmCheck")
    public EhsResult getEnterpriseLastHarmCheck(Long enterpriseId, Integer year){
        GovernmentLastHarmCheck governmentLastHarmCheck = detailReportService.getEnterpriseLastHarmCheck(enterpriseId, year);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentLastHarmCheck", governmentLastHarmCheck);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业员工上次体检信息
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @GetMapping("getEnterpriseLastMedical")
    public EhsResult getEnterpriseLastMedical(Long enterpriseId, Integer year){
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("enterpriseId", enterpriseId);
        condition.put("type", "岗中");
        condition.put("year", year);
        GovernmentLastMedical governmentLastMedical = detailReportService.getEnterpriseLastMedical(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentLastMedical", governmentLastMedical);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业最新培训信息
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @GetMapping("getEnterpriseLastTraining")
    public EhsResult getEnterpriseLastTraining(Long enterpriseId, Integer year){
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("enterpriseId", enterpriseId);
        condition.put("year", year);
        GovernmentLastTraining governmentLastTraining = detailReportService.getEnterpriseLastTraining(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentLastTraining", governmentLastTraining);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业年度的防护用品发放情况
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @RequestMapping("listEnterpriseProvideProtectArts")
    public EhsResult listEnterpriseProvideProtectArts(Integer pageNum, Integer pageSize, Long enterpriseId, Integer year
            , String sortField, String sortType){
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        // 查询条件
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("enterpriseId", enterpriseId);
        condition.put("year", year);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        List<GovernmentProvideProtectArt> governmentProvideProtectArts
                = detailReportService.listEnterpriseProvideProtectArts(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentProvideProtectArts", governmentProvideProtectArts);
        if (pageNum != null && pageSize != null) {
            PageInfo<GovernmentProvideProtectArt> pageInfo = new PageInfo<GovernmentProvideProtectArt>(governmentProvideProtectArts);
            resultMap.put("totalNum", pageInfo.getTotal());
        }
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业最新防护用品发放信息
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @GetMapping("getEnterpriseLastProvide")
    public EhsResult getEnterpriseLastProvide(Long enterpriseId, Integer year){
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("enterpriseId", enterpriseId);
        condition.put("year", year);
        GovernmentLastProvide governmentLastProvide = detailReportService.getEnterpriseLastProvide(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentLastProvide", governmentLastProvide);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业年度的防护设施信息
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @RequestMapping("listEnterpriseProtectiveMeasures")
    public EhsResult listEnterpriseProtectiveMeasures(Integer pageNum, Integer pageSize, Long enterpriseId, Integer year
            , String sortField, String sortType){
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        // 查询条件
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("enterpriseId", enterpriseId);
        condition.put("year", year);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        List<GovernmentProtectiveMeasures> governmentProtectiveMeasures
                = detailReportService.listEnterpriseProtectiveMeasures(condition);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentProtectiveMeasures", governmentProtectiveMeasures);
        if (pageNum != null && pageSize != null) {
            PageInfo<GovernmentProtectiveMeasures> pageInfo = new PageInfo<GovernmentProtectiveMeasures>(governmentProtectiveMeasures);
            resultMap.put("totalNum", pageInfo.getTotal());
        }
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业最新防护设施维修记录
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @GetMapping("getLastServiceRecord")
    public EhsResult getLastServiceRecord(Long enterpriseId, Integer year){
        GovernmentServiceRecord governmentServiceRecord = detailReportService.getLastServiceRecord(enterpriseId, year);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentServiceRecord", governmentServiceRecord);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业最新日常检查记录
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @GetMapping("getServiceInspection")
    public EhsResult getServiceInspection(Long enterpriseId, Integer year){
        GovernmentServiceInspection governmentServiceInspection = detailReportService.getServiceInspection(enterpriseId, year);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentServiceInspection", governmentServiceInspection);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业最新整改记录
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @GetMapping("getServiceRectified")
    public EhsResult getServiceRectified(Long enterpriseId, Integer year){
        GovernmentServiceRectified governmentServiceRectified = detailReportService.getServiceRectified(enterpriseId, year);
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentServiceRectified", governmentServiceRectified);
        return EhsResult.ok(resultMap);
    }

    /**
     * 查询企业年度的三同时信息
     * @param pageNum 请求的页数
     * @param pageSize 每页的数据个数
     * @param enterpriseId 企业id
     * @param year 年度
     * @return
     */
    @RequestMapping("listGovernmentThreeSimultaneitys")
    public EhsResult listGovernmentThreeSimultaneitys(Integer pageNum, Integer pageSize, Long enterpriseId, Integer year
            , String sortField, String sortType){
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum,pageSize);
        }
        // 查询条件
        Map<String,Object> condition = Maps.newHashMap();
        condition.put("enterpriseId", enterpriseId);
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortType)){
            condition.put("sortField", StringUtil.humpToUnderscore(sortField));
            condition.put("sortType", sortType);
        }
        List<GovernmentThreeSimultaneityCustom> governmentThreeSimultaneities
                = detailReportService.listGovernmentThreeSimultaneitys(condition);
        // 计算距离上次更新天数
        for (GovernmentThreeSimultaneityCustom governmentThreeSimultaneityCustom : governmentThreeSimultaneities) {
            if (governmentThreeSimultaneityCustom.getGmtModified() != null) {
                Long timeDiff = new Date().getTime() - governmentThreeSimultaneityCustom.getGmtModified().getTime();
                Long dayDiff = timeDiff / (24 * 60 * 60 * 1000);
                governmentThreeSimultaneityCustom.setDayDiff(dayDiff);
            }else{
                Long timeDiff = new Date().getTime() - governmentThreeSimultaneityCustom.getGmtCreate().getTime();
                Long dayDiff = timeDiff / (24 * 60 * 60 * 1000);
                governmentThreeSimultaneityCustom.setDayDiff(dayDiff);
            }
        }
        Map<String,Object> resultMap = Maps.newHashMap();
        resultMap.put("governmentThreeSimultaneities", governmentThreeSimultaneities);
        if (pageNum != null && pageSize != null) {
            PageInfo<GovernmentThreeSimultaneityCustom> pageInfo = new PageInfo<GovernmentThreeSimultaneityCustom>(governmentThreeSimultaneities);
            resultMap.put("totalNum", pageInfo.getTotal());
        }
        return EhsResult.ok(resultMap);
    }
}
