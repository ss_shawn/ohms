package com.lanware.ehs.gov.detailreport.service.impl;

import com.lanware.ehs.gov.detailreport.service.DetailReportService;
import com.lanware.ehs.mapper.GovernmentLastHarmCheckMapper;
import com.lanware.ehs.mapper.GovernmentServiceInspectionMapper;
import com.lanware.ehs.mapper.GovernmentServiceRecordMapper;
import com.lanware.ehs.mapper.GovernmentServiceRectifiedMapper;
import com.lanware.ehs.mapper.custom.*;
import com.lanware.ehs.pojo.*;
import com.lanware.ehs.pojo.custom.GovernmentThreeSimultaneityCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description 企业详细信息汇报interface实现类
 */
@Service
public class DetailReportServiceImpl implements DetailReportService {
    /** 注入governmentEnterpriseBaseinfoCustomMapper的bean */
    @Autowired
    private GovernmentEnterpriseBaseinfoCustomMapper governmentEnterpriseBaseinfoCustomMapper;
    /** 注入governmentEnterpriseYearCustomMapper的bean */
    @Autowired
    private GovernmentEnterpriseYearCustomMapper governmentEnterpriseYearCustomMapper;
    /** 注入governmentHarmReportCustomMapper的bean */
    @Autowired
    private GovernmentHarmReportCustomMapper governmentHarmReportCustomMapper;
    /** 注入governmentLastHarmCheckMapper的bean */
    @Autowired
    private GovernmentLastHarmCheckMapper governmentLastHarmCheckMapper;
    /** 注入governmentLastMedicalCustomMapper的bean */
    @Autowired
    private GovernmentLastMedicalCustomMapper governmentLastMedicalCustomMapper;
    /** 注入governmentLastTrainingCustomMapper的bean */
    @Autowired
    private GovernmentLastTrainingCustomMapper governmentLastTrainingCustomMapper;
    /** 注入governmentProvideProtectArtCustomMapper的bean */
    @Autowired
    private GovernmentProvideProtectArtCustomMapper governmentProvideProtectArtCustomMapper;
    /** 注入governmentLastProvideCustomMapper的bean */
    @Autowired
    private GovernmentLastProvideCustomMapper governmentLastProvideCustomMapper;
    /** 注入governmentProtectiveMeasuresCustomMapper的bean */
    @Autowired
    private GovernmentProtectiveMeasuresCustomMapper governmentProtectiveMeasuresCustomMapper;
    /** 注入governmentServiceInspectionMapper的bean */
    @Autowired
    private GovernmentServiceInspectionMapper governmentServiceInspectionMapper;
    /** 注入governmentServiceRecordMapper的bean */
    @Autowired
    private GovernmentServiceRecordMapper governmentServiceRecordMapper;
    /** 注入governmentServiceRectifiedMapper的bean */
    @Autowired
    private GovernmentServiceRectifiedMapper governmentServiceRectifiedMapper;
    /** 注入governmentThreeSimultaneityCustomMapper的bean */
    @Autowired
    private GovernmentThreeSimultaneityCustomMapper governmentThreeSimultaneityCustomMapper;

    /**
     * @description 根据企业id查询企业基础信息
     * @param enterpriseId
     * @return com.lanware.ehs.pojo.GovernmentEnterpriseBaseinfo
     */
    @Override
    public GovernmentEnterpriseBaseinfo getEnterpriseBaseInfo(Long enterpriseId) {
        return governmentEnterpriseBaseinfoCustomMapper.selectByPrimaryKey(enterpriseId);
    }

    /**
     * @description 根据条件查询企业年度各类统计信息
     * @param condition
     * @return com.lanware.ehs.pojo.GovernmentEnterpriseBaseinfo
     */
    @Override
    public GovernmentEnterpriseYear getEnterpriseYearInfo(Map<String, Object> condition) {
        return governmentEnterpriseYearCustomMapper.getEnterpriseYearInfo(condition);
    }

    /**
     * @description 查询企业年度的危害因素检测信息
     * @param condition 查询条件(企业id和年度)
     * @return java.util.List<com.lanware.ehs.pojo.GovernmentHarmReport>
     */
    @Override
    public List<GovernmentHarmReport> listEnterpriseYearHarmChecks(Map<String, Object> condition) {
        return governmentHarmReportCustomMapper.listEnterpriseYearHarmChecks(condition);
    }

    /**
     * 查询企业最新危害因素检测信息
     * @param enterpriseId 企业id
     * @param year 年度
     * @return com.lanware.ehs.pojo.GovernmentLastHarmCheck
     */
    @Override
    public GovernmentLastHarmCheck getEnterpriseLastHarmCheck(Long enterpriseId, Integer year) {
        GovernmentLastHarmCheckExample governmentLastHarmCheckExample = new GovernmentLastHarmCheckExample();
        // 按id倒序，取到最新记录
        governmentLastHarmCheckExample.setOrderByClause("id desc");
        GovernmentLastHarmCheckExample.Criteria criteria = governmentLastHarmCheckExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        criteria.andYearEqualTo(year);
        List<GovernmentLastHarmCheck> governmentLastHarmCheckList
                = governmentLastHarmCheckMapper.selectByExample(governmentLastHarmCheckExample);
        if (governmentLastHarmCheckList.size() > 0) {
            return governmentLastHarmCheckList.get(0);
        }
        return null;
    }

    /**
     * @description 查询企业员工上次体检信息
     * @param condition 查询条件
     * @return com.lanware.ehs.pojo.GovernmentLastMedical
     */
    @Override
    public GovernmentLastMedical getEnterpriseLastMedical(Map<String, Object> condition) {
        return governmentLastMedicalCustomMapper.getEnterpriseLastMedical(condition);
    }

    /**
     * @description 查询企业最新培训信息
     * @param condition 查询条件
     * @return com.lanware.ehs.pojo.GovernmentLastTraining
     */
    @Override
    public GovernmentLastTraining getEnterpriseLastTraining(Map<String,Object> condition) {
        return governmentLastTrainingCustomMapper.getEnterpriseLastTraining(condition);
    }

    /**
     * @description 查询企业年度的防护用品发放信息
     * @param condition 查询条件(企业id和年度)
     * @return java.util.List<com.lanware.ehs.pojo.GovernmentProvideProtectArt>
     */
    @Override
    public List<GovernmentProvideProtectArt> listEnterpriseProvideProtectArts(Map<String, Object> condition) {
        return governmentProvideProtectArtCustomMapper.listEnterpriseProvideProtectArts(condition);
    }

    /**
     * @description 查询企业最新防护用品发放信息
     * @param condition 查询条件
     * @return com.lanware.ehs.pojo.GovernmentLastProvide
     */
    @Override
    public GovernmentLastProvide getEnterpriseLastProvide(Map<String, Object> condition) {
        return governmentLastProvideCustomMapper.getEnterpriseLastProvide(condition);
    }

    /**
     * @description 查询企业年度的防护设施信息
     * @param condition 查询条件(企业id和年度)
     * @return java.util.List<com.lanware.ehs.pojo.GovernmentProtectiveMeasures>
     */
    @Override
    public List<GovernmentProtectiveMeasures> listEnterpriseProtectiveMeasures(Map<String, Object> condition) {
        return governmentProtectiveMeasuresCustomMapper.listEnterpriseProtectiveMeasures(condition);
    }

    /**
     * 查询企业最新防护设施维修记录
     * @param enterpriseId 企业id
     * @param year 年度
     * @return com.lanware.ehs.pojo.GovernmentServiceRecord
     */
    @Override
    public GovernmentServiceRecord getLastServiceRecord(Long enterpriseId, Integer year) {
        GovernmentServiceRecordExample governmentServiceRecordExample = new GovernmentServiceRecordExample();
        // 按id倒序，取到最新记录
        governmentServiceRecordExample.setOrderByClause("id desc");
        GovernmentServiceRecordExample.Criteria criteria = governmentServiceRecordExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        criteria.andYearEqualTo(year);
        List<GovernmentServiceRecord> governmentServiceRecordList
                = governmentServiceRecordMapper.selectByExample(governmentServiceRecordExample);
        if (governmentServiceRecordList.size() > 0) {
            return governmentServiceRecordList.get(0);
        }
        return null;
    }

    /**
     * 查询企业最新日常检查记录
     * @param enterpriseId 企业id
     * @param year 年度
     * @return com.lanware.ehs.pojo.GovernmentServiceInspection
     */
    @Override
    public GovernmentServiceInspection getServiceInspection(Long enterpriseId, Integer year) {
        GovernmentServiceInspectionExample governmentServiceInspectionExample = new GovernmentServiceInspectionExample();
        // 按id倒序，取到最新记录
        governmentServiceInspectionExample.setOrderByClause("id desc");
        GovernmentServiceInspectionExample.Criteria criteria = governmentServiceInspectionExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        criteria.andYearEqualTo(year);
        List<GovernmentServiceInspection> governmentServiceInspectionList
                = governmentServiceInspectionMapper.selectByExample(governmentServiceInspectionExample);
        if (governmentServiceInspectionList.size() > 0) {
            return governmentServiceInspectionList.get(0);
        }
        return null;
    }

    /**
     * 查询企业最新整改记录
     * @param enterpriseId 企业id
     * @param year 年度
     * @return com.lanware.ehs.pojo.GovernmentServiceInspection
     */
    @Override
    public GovernmentServiceRectified getServiceRectified(Long enterpriseId, Integer year) {
        GovernmentServiceRectifiedExample governmentServiceRectifiedExample = new GovernmentServiceRectifiedExample();
        // 按id倒序，取到最新记录
        governmentServiceRectifiedExample.setOrderByClause("id desc");
        GovernmentServiceRectifiedExample.Criteria criteria = governmentServiceRectifiedExample.createCriteria();
        criteria.andEnterpriseIdEqualTo(enterpriseId);
        criteria.andYearEqualTo(year);
        List<GovernmentServiceRectified> governmentServiceRectifiedList
                = governmentServiceRectifiedMapper.selectByExample(governmentServiceRectifiedExample);
        if (governmentServiceRectifiedList.size() > 0) {
            return governmentServiceRectifiedList.get(0);
        }
        return null;
    }

    /**
     * @description 查询企业年度的三同时信息
     * @param condition 查询条件(企业id和年度)
     * @return java.util.List<com.lanware.ehs.pojo.GovernmentThreeSimultaneity>
     */
    @Override
    public List<GovernmentThreeSimultaneityCustom> listGovernmentThreeSimultaneitys(Map<String, Object> condition) {
        return governmentThreeSimultaneityCustomMapper.listGovernmentThreeSimultaneitys(condition);
    }
}
