package com.lanware.ehs.gov.front.web.service;

import com.lanware.ehs.pojo.custom.GovernmentEnterpriseBaseinfotCustom;

import java.util.List;
import java.util.Map;

public interface EnterpriseBaseInfoService {
    List<GovernmentEnterpriseBaseinfotCustom> getMonitorEnterpriseByMonitorId(Long monitorId);

    List<GovernmentEnterpriseBaseinfotCustom> getMonitorEnterpriseByCondition(Map<String, Object> condition);
}
