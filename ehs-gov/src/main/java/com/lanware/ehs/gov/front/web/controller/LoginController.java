package com.lanware.ehs.gov.front.web.controller;

import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.exception.EhsException;
import com.lanware.ehs.common.exception.SysMaintainceException;
import com.lanware.ehs.common.utils.CaptchaUtil;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.common.utils.EhsUtil;
import com.lanware.ehs.common.utils.MD5Util;
import com.lanware.ehs.gov.user.service.UserService;
import com.lanware.ehs.pojo.MonitorUser;
import com.wf.captcha.Captcha;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.session.ExpiredSessionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;

/**
 * 登录Controller
 */
@Controller
public class LoginController extends BaseController {
    @Autowired
    private UserService userService;
    /**
     * 请求登录页面
     * @param request
     * @return
     */
    @GetMapping("login")
    @ResponseBody
    public Object login(HttpServletRequest request) {
        if (EhsUtil.isAjaxRequest(request)) {
            throw new ExpiredSessionException();
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("front/login");
            return mav;
        }
    }


    /**
     * 登录
     * @param username
     * @param password
     * @param verifyCode
     * @param rememberMe
     * @param request
     * @return
     * @throws EhsException
     */
    @PostMapping("login")
    @ResponseBody
    public EhsResult login(
            @NotBlank(message = "{required}") String username,
            @NotBlank(message = "{required}") String password,
            @NotBlank(message = "{required}") String verifyCode,
            boolean rememberMe, HttpServletRequest request) throws EhsException {
        if (!CaptchaUtil.verify(verifyCode, request)) {
            throw new EhsException("验证码错误！");
        }
        try {
            password = MD5Util.encrypt(username.toLowerCase(), password);
            UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
            SecurityUtils.getSubject().login(token);

            //更新登录时间
            this.userService.updateLoginTime(username);

            return EhsResult.ok();
        } catch (UnknownAccountException | IncorrectCredentialsException | LockedAccountException | SysMaintainceException e) {
            throw new EhsException(e.getMessage());
        } catch (AuthenticationException e) {
            throw new EhsException("认证失败！");
        }

    }


    @GetMapping("/")
    public String redirectIndex() {
        return "redirect:/index";
    }

    /**
     * 进入系统首页
     * @param model
     * @return
     */
    @GetMapping("index")
    public String index(Model model) {
        MonitorUser user = (MonitorUser)getSubject().getPrincipal();
        user = userService.findByName(user.getUsername());
        model.addAttribute("user",user ); // 获取实时的用户信息
        return "index";
    }

    /**
     * 生成验证码
     * @param request
     * @param response
     * @throws Exception
     */
    @GetMapping("images/captcha")
    @ResponseBody
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CaptchaUtil.outPng(110, 34, 4, Captcha.TYPE_ONLY_NUMBER, request, response);
    }





}
