package com.lanware.ehs.gov.datasummary.service.impl;

import com.lanware.ehs.gov.datasummary.service.DataSummaryService;
import com.lanware.ehs.mapper.custom.*;
import com.lanware.ehs.pojo.MonitorArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @description 辖区数据汇总interface实现类
 */
@Service
public class DataSummaryServiceImpl implements DataSummaryService {
    /** 注入monitorAreaCustomMapper的bean */
    @Autowired
    private MonitorAreaCustomMapper monitorAreaCustomMapper;
    /** 注入monitorEnterpriseCustomMapper的bean */
    @Autowired
    private MonitorEnterpriseCustomMapper monitorEnterpriseCustomMapper;
    /** 注入governmentEnterpriseBaseinfoCustomMapper的bean */
    @Autowired
    private GovernmentEnterpriseBaseinfoCustomMapper governmentEnterpriseBaseinfoCustomMapper;
    /** 注入governmentEnterpriseYearCustomMapper的bean */
    @Autowired
    private GovernmentEnterpriseYearCustomMapper governmentEnterpriseYearCustomMapper;
    /** 注入governmentHarmReportCustomMapper的bean */
    @Autowired
    private GovernmentHarmReportCustomMapper governmentHarmReportCustomMapper;
    /** 注入governmentOutsideCheckCustomMapper的bean */
    @Autowired
    private GovernmentOutsideCheckCustomMapper governmentOutsideCheckCustomMapper;

    /**
     * @description 根据id查询监管区域信息
     * @param id
     * @return com.lanware.ehs.pojo.MonitorArea
     */
    @Override
    public MonitorArea getMonitorAreaById(Long id) {
        return monitorAreaCustomMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 查询监管企业数目
     * @param areaId 监管区域id
     * @return java.lang.Integer
     */
    @Override
    public Integer getMonitorEnterpriseNum(Long areaId) {
        return monitorEnterpriseCustomMapper.getMonitorEnterpriseNum(areaId);
    }

    /**
     * @description 查询监管企业涉害人员数，女工数，农民工数
     * @param areaId 监管区域id
     * @return java.util.Map<java.lang.String,java.lang.Integer>
     */
    @Override
    public Map<String, Integer> getEnterpriseHarmEmployeeNum(Long areaId) {
        return governmentEnterpriseBaseinfoCustomMapper.getEnterpriseHarmEmployeeNum(areaId);
    }

    /**
     * @description 查询监管企业年度的各种类型数目
     * @param condition 查询条件(监管区域id和年度)
     * @return java.util.Map<java.lang.String,java.lang.Integer>
     */
    @Override
    public Map<String, Integer> getEnterpriseYearNum(Map<String, Object> condition) {
        return governmentEnterpriseYearCustomMapper.getEnterpriseYearNum(condition);
    }

    /**
     * @description 查询监管企业危害因素检测时间分布
     * @param condition 查询条件(监管区域id和年度)
     * @return java.util.Map<java.lang.String,java.lang.Integer>
     */
    @Override
    public Map<String, Integer> getEnterpriseCheckTimeDistribution(Map<String, Object> condition) {
        return governmentOutsideCheckCustomMapper.getEnterpriseCheckTimeDistribution(condition);
    }


}
