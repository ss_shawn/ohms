package com.lanware.ehs.gov.user.service;

import com.lanware.ehs.pojo.MonitorUser;

public interface UserService {
    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return 用户
     */
    MonitorUser findByName(String username);

    /**
     * 更新用户登录时间
     *
     * @param username 用户名
     */
    void updateLoginTime(String username);

    /**
     * 修改密码
     *
     * @param username 用户名
     * @param password 新密码
     */
    void updatePassword(String username, String password);
}
