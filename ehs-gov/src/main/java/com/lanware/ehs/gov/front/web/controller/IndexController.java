package com.lanware.ehs.gov.front.web.controller;

import com.google.common.collect.Maps;
import com.lanware.ehs.common.controller.BaseController;
import com.lanware.ehs.common.utils.EhsResult;
import com.lanware.ehs.gov.front.web.service.EnterpriseBaseInfoService;
import com.lanware.ehs.pojo.MonitorUser;
import com.lanware.ehs.pojo.custom.GovernmentEnterpriseBaseinfotCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 */
@RestController
@RequestMapping("index")
public class IndexController extends BaseController {
    @Autowired
    private EnterpriseBaseInfoService baseInfoService;
    /**
     * 获取监管者账号所维护的企业。
     * @return
     */
    @RequestMapping("getMonitorEnterprise")
    public EhsResult getMonitorEnterprise(){
        MonitorUser monitorUser = (MonitorUser)getSubject().getPrincipal();
        List<GovernmentEnterpriseBaseinfotCustom> list = baseInfoService.getMonitorEnterpriseByMonitorId(monitorUser.getId());
        return EhsResult.ok(list);
    }

    /**
     * 根据搜索条件获取监管者账号所维护的企业
     * @param keyword 关键字
     * @param condition 排序&筛选条件
     * @return
     */
    @RequestMapping("getMonitorEnterpriseByCondition")
    public EhsResult getMonitorEnterpriseByCondition(String keyword, String condition){
        MonitorUser monitorUser = (MonitorUser)getSubject().getPrincipal();
        Map<String, Object> condition1 = Maps.newHashMap();
        // 根据排序&筛选条件，新增查询条件
        if ("lowestHealthScore".equals(condition)) {
            condition1.put("sortField", "score");
            condition1.put("sortType", "asc");
        }
        if ("highestHealthScore".equals(condition)) {
            condition1.put("sortField", "score");
            condition1.put("sortType", "desc");
        }
        if ("lastUpdate".equals(condition)) {
            condition1.put("sortField", "last_update_time");
            condition1.put("sortType", "desc");
        }
        if ("highInvolve".equals(condition)) {
            condition1.put("harmDegree", "严重");
        }
        if ("middleInvolve".equals(condition)) {
            condition1.put("harmDegree", "较重");
        }
        if ("lowInvolve".equals(condition)) {
            condition1.put("harmDegree", "一般");
        }
        condition1.put("monitorId", monitorUser.getId());
        condition1.put("keyword", keyword);
        List<GovernmentEnterpriseBaseinfotCustom> list = baseInfoService.getMonitorEnterpriseByCondition(condition1);
        return EhsResult.ok(list);
    }
}
