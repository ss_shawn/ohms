package com.lanware.ehs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 监管端启动类
 */
@ComponentScan("com.lanware.ehs")
@MapperScan("com.lanware.ehs.mapper")
@SpringBootApplication
public class GovApplication {
    public static void main(String[] args) {
        SpringApplication.run(GovApplication.class, args);
    }
}
