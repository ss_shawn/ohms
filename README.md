# ehs 职业卫生智慧管理平台

## 前言

　　`职业卫生智慧管理平台包含三部分管理端、企业端、政府端。管理端主要给平台管理员使用，不涉及权限操作。企业端主要给企业操作人员使用，涉及到用户访问权限。政府端主要用于政府人员查看企业统计数据，不涉及用户访问权限。

## 项目介绍

　　基于SpringBoot和Spring+SpringMVC+Mybatis开发系统架构。

### 组织结构

``` 
ehs
├── ehs-common -- 公共模块
├── ehs-mapper -- 公用Mapper
├── ehs-admin -- 管理端前台thymeleaf模板[端口:8080]
├── ehs-enterprise -- 企业端前台thymeleaf模板[端口:8081]
├── ehs-gov -- 政府端前台thymeleaf模板[端口:8082]
```

### 技术选型

#### 后端技术:
技术 | 名称 | 官网
----|------|----
SpringBoot | 简化开发框架 | [https://spring.io/projects/spring-boot/](https://spring.io/projects/spring-boot/)
Spring Framework | 容器  | [http://projects.spring.io/spring-framework/](http://projects.spring.io/spring-framework/)
SpringMVC | MVC框架  | [http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc](http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc)
Apache Shiro | 安全框架  | [http://shiro.apache.org/](http://shiro.apache.org/)
MyBatis | ORM框架  | [http://www.mybatis.org/mybatis-3/zh/index.html](http://www.mybatis.org/mybatis-3/zh/index.html)
MyBatis Generator | 代码生成  | [http://www.mybatis.org/generator/index.html](http://www.mybatis.org/generator/index.html)
PageHelper | MyBatis物理分页插件  | [http://git.oschina.net/free/Mybatis_PageHelper](http://git.oschina.net/free/Mybatis_PageHelper)
Druid | 数据库连接池  | [https://github.com/alibaba/druid](https://github.com/alibaba/druid)
FluentValidator | 校验框架  | [https://github.com/neoremind/fluent-validator](https://github.com/neoremind/fluent-validator)
Thymeleaf | 模板引擎  | [http://www.thymeleaf.org/](http://www.thymeleaf.org/)
Redis | 分布式缓存数据库  | [https://redis.io/](https://redis.io/)
Quartz | 作业调度框架  | [http://www.quartz-scheduler.org/](http://www.quartz-scheduler.org/)
Log4J | 日志组件  | [http://logging.apache.org/log4j/1.2/](http://logging.apache.org/log4j/1.2/)
Swagger2 | 接口测试框架  | [http://swagger.io/](http://swagger.io/)
AliOSS  | 云存储  | [https://www.aliyun.com/product/oss/](https://www.aliyun.com/product/oss/) 
Protobuf & json | 数据序列化  | [https://github.com/google/protobuf](https://github.com/google/protobuf)
Maven | 项目构建管理  | [http://maven.apache.org/](http://maven.apache.org/)

#### 前端技术:
技术 | 名称 | 官网
----|------|----
jQuery | 函式库  | [http://jquery.com/](http://jquery.com/)
Select2 | 选择框插件  | [https://github.com/select2/select2](https://github.com/select2/select2)
layui | 前端框架  | [https://www.layui.com/](https://www.layui.com/)
React | 界面构建框架  | [https://github.com/facebook/react](https://github.com/facebook/react)
Editor.md | Markdown编辑器  | [https://github.com/pandao/editor.md](https://github.com/pandao/editor.md)


#### 模块介绍





## 环境搭建

#### 开发工具:
- MySql: 数据库
- Tomcat: 应用服务器
- SVN|Git: 版本管理
- IntelliJ IDEA: 开发IDE
- PowerDesigner: 建模工具
- Navicat for MySQL: 数据库客户端

#### 开发环境：
- Jdk8+
- Mysql5.5+
- Redis


### 资源下载

- Maven [http://maven.apache.org/download.cgi](http://maven.apache.org/download.cgi "Maven")
- Redis [https://redis.io/download](https://redis.io/download "Redis")


## 开发指南:


### 编译流程


### 启动顺序（后台）


### 框架规范约定

约定优于配置(convention over configuration)，此框架约定了很多编程规范，下面一一列举：

```
- service类，需要在叫名`service`的包下，并以`Service`结尾，如`CmsArticleServiceImpl`

- controller类，需要在以`controller`结尾的包下，类名以Controller结尾，如`CmsArticleController.java`，并继承`BaseController`

- spring task类，需要在叫名`task`的包下，并以`Task`结尾，如`TestTask.java`

- mapper.xml，需要在名叫`mapper`的包下，并以`Mapper.xml`结尾，如`CmsArticleMapper.xml`

- mapper接口，需要在名叫`mapper`的包下，并以`Mapper`结尾，如`CmsArticleMapper.java`

- model实体类，需要在名叫`model`的包下，命名规则为数据表转驼峰规则，如`SysUser.java`

- 类名：首字母大写驼峰规则；方法名：首字母小写驼峰规则；常量：全大写；变量：首字母小写驼峰规则，尽量非缩写

- 配置文件放到`src/main/resources`目录下

- `RequestMapping`和返回物理试图路径的url尽量写全路径，如：`@RequestMapping("/manage")`、`return "/manage/index"`

- `RequestMapping`指定method

- 模块命名为`项目`-`子项目`-`业务`，如`zyjk-gov-admin`

- 数据表命名为：`子系统`_`表`，如`sys_user`

- 更多规范，参考[[阿里巴巴Java开发手册]

```




